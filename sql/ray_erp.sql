/*
Navicat MySQL Data Transfer

Source Server         : 阿里云数据库
Source Server Version : 50725
Source Host           : rm-uf6hql22bjf3668ebro.mysql.rds.aliyuncs.com:3306
Source Database       : ray_erp

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2020-09-14 14:09:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for base_customer
-- ----------------------------
DROP TABLE IF EXISTS `base_customer`;
CREATE TABLE `base_customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编码',
  `customer_name` varchar(50) DEFAULT NULL COMMENT '客户名称',
  `customer_type` varchar(50) DEFAULT NULL COMMENT '客户类型  客户，供应商，加工户',
  `link_name` varchar(50) DEFAULT NULL COMMENT '联系人',
  `link_moblie` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `address` varchar(500) DEFAULT NULL COMMENT '地址',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_customer_code` (`customer_code`),
  KEY `index_customer_name` (`customer_name`),
  KEY `index_customer_type` (`customer_type`),
  KEY `index_status` (`status`),
  KEY `index_is_deleted` (`is_deleted`),
  KEY `index_tenant_code` (`tenant_code`),
  KEY `index_owner_code` (`owner_code`),
  KEY `index_organization_code` (`organization_code`(255))
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='客户表';

-- ----------------------------
-- Table structure for base_customer_price
-- ----------------------------
DROP TABLE IF EXISTS `base_customer_price`;
CREATE TABLE `base_customer_price` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编码',
  `step_code` varchar(50) DEFAULT NULL COMMENT '工序',
  `price` decimal(18,2) DEFAULT '0.00' COMMENT '加工费用',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_customer_code` (`customer_code`),
  KEY `index_customer_type` (`step_code`),
  KEY `index_status` (`status`),
  KEY `index_is_deleted` (`is_deleted`),
  KEY `index_tenant_code` (`tenant_code`),
  KEY `index_owner_code` (`owner_code`),
  KEY `index_organization_code` (`organization_code`(255))
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8 COMMENT='客户表加工费用配置';

-- ----------------------------
-- Table structure for base_customer_print
-- ----------------------------
DROP TABLE IF EXISTS `base_customer_print`;
CREATE TABLE `base_customer_print` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编码',
  `template` text COMMENT '模板',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT '0' COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_customer_code` (`customer_code`),
  KEY `index_status` (`status`),
  KEY `index_is_deleted` (`is_deleted`),
  KEY `index_tenant_code` (`tenant_code`),
  KEY `index_owner_code` (`owner_code`),
  KEY `index_organization_code` (`organization_code`(255))
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COMMENT='客户-唛头';

-- ----------------------------
-- Table structure for base_material
-- ----------------------------
DROP TABLE IF EXISTS `base_material`;
CREATE TABLE `base_material` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `material_code` varchar(50) DEFAULT NULL COMMENT '物料编码',
  `material_name_en` varchar(50) DEFAULT NULL COMMENT '其他名称',
  `material_name` varchar(50) DEFAULT NULL COMMENT '物料名称',
  `material_type` varchar(50) DEFAULT NULL COMMENT '物料类型',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `weight` varchar(50) DEFAULT NULL COMMENT '重量',
  `weight_diff` decimal(18,2) DEFAULT '0.00',
  `volume` varchar(50) DEFAULT NULL COMMENT '体积',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE,
  KEY `index_material_code` (`material_code`),
  KEY `index_material_name` (`material_name`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='物料/产品';

-- ----------------------------
-- Table structure for base_material_extend
-- ----------------------------
DROP TABLE IF EXISTS `base_material_extend`;
CREATE TABLE `base_material_extend` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `material_code` varchar(50) DEFAULT NULL COMMENT '物料编号',
  `name` varchar(50) DEFAULT NULL COMMENT '属性名称',
  `prop` varchar(50) DEFAULT NULL COMMENT '属性名',
  `value` varchar(500) DEFAULT NULL COMMENT '属性值',
  `company_code` varchar(200) DEFAULT NULL COMMENT '公司编码',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_bms_process_data_business_code` (`material_code`),
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1651758 DEFAULT CHARSET=utf8 COMMENT='物料扩展信息';

-- ----------------------------
-- Table structure for base_material_model
-- ----------------------------
DROP TABLE IF EXISTS `base_material_model`;
CREATE TABLE `base_material_model` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `material_code` varchar(50) DEFAULT NULL COMMENT '物料名称',
  `material_type` varchar(50) DEFAULT NULL COMMENT '物料类型同物料',
  `model_code` varchar(50) DEFAULT NULL COMMENT '型号编码',
  `model_name` varchar(50) DEFAULT NULL COMMENT '型号/规格名称',
  `model_prop` varchar(50) DEFAULT NULL COMMENT '规格属性',
  `name` varchar(50) DEFAULT NULL COMMENT '扩展名字',
  `bar_code` varchar(50) DEFAULT NULL COMMENT '条形码',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COMMENT='物料/产品-规格';

-- ----------------------------
-- Table structure for base_material_template
-- ----------------------------
DROP TABLE IF EXISTS `base_material_template`;
CREATE TABLE `base_material_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `template_code` varchar(50) DEFAULT NULL COMMENT '模板编号',
  `name` varchar(50) DEFAULT NULL COMMENT '属性名称',
  `prop` varchar(50) DEFAULT NULL COMMENT '属性名',
  `company_code` varchar(200) DEFAULT NULL COMMENT '公司编码',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1651610 DEFAULT CHARSET=utf8 COMMENT='商品属性模板定义';

-- ----------------------------
-- Table structure for base_material_type
-- ----------------------------
DROP TABLE IF EXISTS `base_material_type`;
CREATE TABLE `base_material_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type_code` varchar(50) DEFAULT NULL COMMENT '类型编码',
  `type_name` varchar(50) DEFAULT NULL COMMENT '类型名称',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='物料类型';

-- ----------------------------
-- Table structure for base_technology
-- ----------------------------
DROP TABLE IF EXISTS `base_technology`;
CREATE TABLE `base_technology` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `technology_code` varchar(50) DEFAULT NULL COMMENT '工艺编号',
  `model_code` varchar(50) DEFAULT NULL COMMENT '型号编码',
  `step_mothed` varchar(50) DEFAULT NULL COMMENT '工艺方式',
  `remark` varchar(500) DEFAULT NULL COMMENT '工艺说明',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COMMENT='技术工艺';

-- ----------------------------
-- Table structure for base_technology_record
-- ----------------------------
DROP TABLE IF EXISTS `base_technology_record`;
CREATE TABLE `base_technology_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `technology_code` varchar(50) DEFAULT NULL COMMENT '工艺编号',
  `model_type` varchar(50) DEFAULT NULL COMMENT '工艺  原料类型  ',
  `model_code` varchar(50) DEFAULT NULL COMMENT '型号编码',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `use_quantity` decimal(18,2) DEFAULT NULL COMMENT '用量',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT '0' COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COMMENT='技术工艺';

-- ----------------------------
-- Table structure for base_template
-- ----------------------------
DROP TABLE IF EXISTS `base_template`;
CREATE TABLE `base_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `template_code` varchar(50) DEFAULT NULL COMMENT '模板编号',
  `template_type` varchar(50) DEFAULT NULL COMMENT '模板类型',
  `name` varchar(50) DEFAULT NULL COMMENT '属性名称',
  `prop` varchar(50) DEFAULT NULL COMMENT '属性名',
  `company_code` varchar(200) DEFAULT NULL COMMENT '公司编码',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1651612 DEFAULT CHARSET=utf8 COMMENT='属性模板定义';

-- ----------------------------
-- Table structure for base_template_extend
-- ----------------------------
DROP TABLE IF EXISTS `base_template_extend`;
CREATE TABLE `base_template_extend` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `object_code` varchar(50) DEFAULT NULL COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '属性名称',
  `prop` varchar(50) DEFAULT NULL COMMENT '属性名',
  `value` varchar(500) DEFAULT NULL COMMENT '属性值',
  `company_code` varchar(200) DEFAULT NULL COMMENT '公司编码',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_bms_process_data_business_code` (`object_code`),
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1651729 DEFAULT CHARSET=utf8 COMMENT='物料扩展信息';

-- ----------------------------
-- Table structure for fina_advance
-- ----------------------------
DROP TABLE IF EXISTS `fina_advance`;
CREATE TABLE `fina_advance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `advance_code` varchar(50) DEFAULT NULL COMMENT '预付记录',
  `type` varchar(50) DEFAULT NULL COMMENT '业务类型 ',
  `order_no` varchar(50) DEFAULT NULL COMMENT '业务订单号',
  `customer_code` varchar(50) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT '0.00' COMMENT '已支付金额',
  `usable_amount` decimal(18,2) DEFAULT NULL COMMENT '可用金额',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `bank_code` varchar(50) DEFAULT NULL COMMENT '银行账号',
  `bank_name` varchar(50) DEFAULT NULL COMMENT '银行名称',
  `bank_user` varchar(50) DEFAULT NULL COMMENT '账户人',
  `remark` varchar(2000) DEFAULT NULL COMMENT '备注',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COMMENT='预付款';

-- ----------------------------
-- Table structure for fina_advance_detail
-- ----------------------------
DROP TABLE IF EXISTS `fina_advance_detail`;
CREATE TABLE `fina_advance_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单号',
  `advance_code` varchar(50) DEFAULT NULL COMMENT '预付款单号',
  `amount` decimal(18,2) DEFAULT '0.00' COMMENT '已支付金额',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COMMENT='对账单使用预付款记录';

-- ----------------------------
-- Table structure for fina_bill
-- ----------------------------
DROP TABLE IF EXISTS `fina_bill`;
CREATE TABLE `fina_bill` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单号',
  `bill_type` varchar(50) DEFAULT NULL COMMENT '对账单类型 应收 应付',
  `bill_source` varchar(50) DEFAULT NULL COMMENT '账单来源  加工  销售  采购  订单',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `step_code` varchar(50) DEFAULT NULL COMMENT '步骤编码',
  `currency` varchar(50) DEFAULT NULL COMMENT '币种',
  `total_amount` decimal(18,2) DEFAULT NULL COMMENT '总金额',
  `deduction_amount` decimal(18,2) DEFAULT NULL COMMENT '扣款金额',
  `amount` decimal(18,2) DEFAULT NULL COMMENT '对账金额',
  `pay_amount` decimal(18,2) DEFAULT '0.00' COMMENT '已支付金额',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `order_status` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE,
  KEY `index_customer_code` (`customer_code`),
  KEY `index_step_code` (`step_code`),
  KEY `index_bill_no` (`bill_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COMMENT='对账单';

-- ----------------------------
-- Table structure for fina_bill_adjust
-- ----------------------------
DROP TABLE IF EXISTS `fina_bill_adjust`;
CREATE TABLE `fina_bill_adjust` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单号',
  `adjust_no` varchar(50) DEFAULT NULL COMMENT '调整单',
  `adjust_type` varchar(50) DEFAULT NULL COMMENT 't调整方式 增加  减少',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `amount` decimal(18,2) DEFAULT NULL COMMENT '对账金额',
  `remark` varchar(500) DEFAULT NULL COMMENT '调整原因',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE,
  KEY `index_customer_code` (`customer_code`),
  KEY `index_bill_no` (`bill_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='加工单号';

-- ----------------------------
-- Table structure for fina_bill_pay
-- ----------------------------
DROP TABLE IF EXISTS `fina_bill_pay`;
CREATE TABLE `fina_bill_pay` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pay_code` varchar(50) DEFAULT NULL COMMENT '支付编码',
  `pay_type` varchar(50) DEFAULT NULL COMMENT '付款类型',
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单号',
  `amount` decimal(18,2) DEFAULT NULL COMMENT '对账金额',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `bank_code` varchar(50) DEFAULT NULL COMMENT '银行账号',
  `bank_name` varchar(50) DEFAULT NULL COMMENT '银行名称',
  `bank_user` varchar(50) DEFAULT NULL COMMENT '账户人',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE,
  KEY `index_bill_no` (`bill_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COMMENT='对账单';

-- ----------------------------
-- Table structure for prod_business
-- ----------------------------
DROP TABLE IF EXISTS `prod_business`;
CREATE TABLE `prod_business` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `business_code` varchar(50) DEFAULT NULL COMMENT '业务单号',
  `business_type` varchar(50) DEFAULT NULL COMMENT '业务类型',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `step_code` varchar(50) DEFAULT NULL COMMENT '步骤编码',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '入库仓库',
  `price` decimal(18,2) DEFAULT '0.00' COMMENT '单价',
  `total` int(10) DEFAULT NULL COMMENT '卷数',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '总数量',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '生产商品',
  `bill_status` int(4) DEFAULT '0' COMMENT '对账状态',
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单号',
  `order_status` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `order_prod` int(4) DEFAULT NULL COMMENT '是否订单',
  `out_business_code` varchar(50) DEFAULT NULL COMMENT '外部单号',
  `employee` varchar(100) DEFAULT NULL COMMENT '雇员',
  `employeea` varchar(100) DEFAULT NULL COMMENT '雇员',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE,
  KEY `index_order_no` (`order_no`),
  KEY `index_business_code` (`business_code`),
  KEY `index_customer_code` (`customer_code`),
  KEY `index_step_code` (`step_code`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8 COMMENT='加工单号';

-- ----------------------------
-- Table structure for prod_business_back
-- ----------------------------
DROP TABLE IF EXISTS `prod_business_back`;
CREATE TABLE `prod_business_back` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `business_code` varchar(50) DEFAULT NULL COMMENT '业务单号',
  `back_code` varchar(50) DEFAULT NULL COMMENT '退料单',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `step_code` varchar(50) DEFAULT NULL COMMENT '步骤编码',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '入库仓库',
  `price` decimal(18,2) DEFAULT '0.00' COMMENT '单价',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '总数量',
  `total` int(4) DEFAULT '0',
  `order_status` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE,
  KEY `index_order_no` (`order_no`),
  KEY `index_business_code` (`business_code`),
  KEY `index_customer_code` (`customer_code`),
  KEY `index_step_code` (`step_code`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COMMENT='加工退料单';

-- ----------------------------
-- Table structure for prod_business_back_in
-- ----------------------------
DROP TABLE IF EXISTS `prod_business_back_in`;
CREATE TABLE `prod_business_back_in` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `back_code` varchar(50) DEFAULT NULL COMMENT '唯一码',
  `business_code` varchar(50) DEFAULT NULL COMMENT '加工单号',
  `out_code` varchar(50) DEFAULT NULL COMMENT '对应出库单',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '数量',
  `total` int(4) DEFAULT '0',
  `batch_no` varchar(50) DEFAULT '0' COMMENT '批次号',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `serial_number` varchar(50) DEFAULT NULL COMMENT '序列号',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8 COMMENT='加工退料单物料明细';

-- ----------------------------
-- Table structure for prod_business_deduction
-- ----------------------------
DROP TABLE IF EXISTS `prod_business_deduction`;
CREATE TABLE `prod_business_deduction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deduction_code` varchar(50) DEFAULT NULL COMMENT '扣款单号',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `business_code` varchar(50) DEFAULT NULL COMMENT '业务单号',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `amount` decimal(18,2) DEFAULT NULL COMMENT '扣款金额',
  `remark` varchar(500) DEFAULT NULL COMMENT '描述',
  `order_status` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `bill_status` int(4) DEFAULT '0' COMMENT '对账状态',
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单号',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='加工单号-扣款';

-- ----------------------------
-- Table structure for prod_business_in
-- ----------------------------
DROP TABLE IF EXISTS `prod_business_in`;
CREATE TABLE `prod_business_in` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(50) DEFAULT NULL COMMENT '唯一码',
  `order_no` varchar(50) DEFAULT NULL COMMENT '业务订单号',
  `business_code` varchar(50) DEFAULT NULL COMMENT '加工单号',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `quantity` decimal(18,2) DEFAULT '0.00' COMMENT '数量',
  `total` int(4) DEFAULT '0',
  `delete_quantity` decimal(18,2) DEFAULT '0.00' COMMENT '扣减数量',
  `real_quantity` decimal(18,2) DEFAULT '0.00' COMMENT '实际数量',
  `goods_net_weight` decimal(18,2) DEFAULT NULL COMMENT ' 净重',
  `goods_weight` decimal(18,2) DEFAULT NULL COMMENT '毛重',
  `gang_no` varchar(50) DEFAULT NULL COMMENT '缸号',
  `batch_no` varchar(50) DEFAULT '0' COMMENT '批次号',
  `serial_number` varchar(50) DEFAULT NULL COMMENT '序列号',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `out_quantity` varchar(255) DEFAULT '0' COMMENT '对应出库数量',
  `price` decimal(18,2) DEFAULT NULL COMMENT '单价',
  `out_code` varchar(50) DEFAULT NULL COMMENT '对应出库记录',
  `remark` varchar(2000) DEFAULT NULL COMMENT '备注',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=644 DEFAULT CHARSET=utf8 COMMENT='加工单物料明细';

-- ----------------------------
-- Table structure for prod_business_out
-- ----------------------------
DROP TABLE IF EXISTS `prod_business_out`;
CREATE TABLE `prod_business_out` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(50) DEFAULT NULL COMMENT '唯一码',
  `order_no` varchar(255) DEFAULT NULL COMMENT '业务订单号',
  `business_code` varchar(50) DEFAULT NULL COMMENT '加工单号',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `total` int(4) DEFAULT '0',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '数量',
  `gang_no` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT '0' COMMENT '批次号',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `serial_number` varchar(50) DEFAULT NULL COMMENT '序列号',
  `in_code` varchar(50) DEFAULT NULL COMMENT '对应入库记录',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=368 DEFAULT CHARSET=utf8 COMMENT='加工单物料明细';

-- ----------------------------
-- Table structure for prod_business_quick
-- ----------------------------
DROP TABLE IF EXISTS `prod_business_quick`;
CREATE TABLE `prod_business_quick` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `quick_code` varchar(50) DEFAULT NULL COMMENT '业务单号',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `step_code` varchar(50) DEFAULT NULL COMMENT '步骤编码',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '入库仓库',
  `price` decimal(18,2) DEFAULT '0.00' COMMENT '单价',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '生产商品',
  `employee` varchar(100) DEFAULT NULL COMMENT '雇员',
  `employeea` varchar(100) DEFAULT NULL COMMENT '雇员',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE,
  KEY `index_order_no` (`order_no`),
  KEY `index_business_code` (`quick_code`),
  KEY `index_customer_code` (`customer_code`),
  KEY `index_step_code` (`step_code`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8 COMMENT='加工单号-快捷入库';

-- ----------------------------
-- Table structure for prod_business_quick_in
-- ----------------------------
DROP TABLE IF EXISTS `prod_business_quick_in`;
CREATE TABLE `prod_business_quick_in` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(50) DEFAULT NULL COMMENT '唯一码',
  `quick_code` varchar(50) DEFAULT NULL COMMENT '加工单号',
  `order_no` varchar(50) DEFAULT NULL,
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `quantity` decimal(18,2) DEFAULT '0.00' COMMENT '数量',
  `delete_quantity` decimal(18,2) DEFAULT '0.00' COMMENT '扣减数量',
  `real_quantity` decimal(18,2) DEFAULT '0.00' COMMENT '实际数量',
  `goods_weight` decimal(18,2) DEFAULT NULL,
  `goods_net_weight` decimal(18,8) DEFAULT NULL,
  `gang_no` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT '0' COMMENT '批次号',
  `serial_number` varchar(50) DEFAULT NULL COMMENT '序列号',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `out_quantity` varchar(255) DEFAULT '0' COMMENT '对应出库数量',
  `price` decimal(18,2) DEFAULT NULL COMMENT '单价',
  `out_code` varchar(50) DEFAULT NULL COMMENT '对应出库记录',
  `remark` varchar(2000) DEFAULT NULL COMMENT '备注',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=391 DEFAULT CHARSET=utf8 COMMENT='加工单快捷入库明细';

-- ----------------------------
-- Table structure for prod_error_type
-- ----------------------------
DROP TABLE IF EXISTS `prod_error_type`;
CREATE TABLE `prod_error_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `error_code` varchar(50) DEFAULT NULL COMMENT '错误编码',
  `error_name` varchar(50) DEFAULT NULL COMMENT '步骤名称',
  `number` int(4) DEFAULT '0' COMMENT '错误数量',
  `need_length` int(4) DEFAULT NULL COMMENT '是否需要数量',
  `delete_length` decimal(18,2) DEFAULT '0.00' COMMENT '扣减数量',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='验布错误类型';

-- ----------------------------
-- Table structure for prod_order
-- ----------------------------
DROP TABLE IF EXISTS `prod_order`;
CREATE TABLE `prod_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `order_name` varchar(255) DEFAULT NULL COMMENT '订单名称',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `order_status` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '编码',
  `total_amount` decimal(18,2) DEFAULT NULL COMMENT '总金额',
  `send_time` datetime DEFAULT NULL COMMENT '发货时间',
  `pay_type` varchar(50) DEFAULT NULL COMMENT '付款方式',
  `transport` varchar(50) DEFAULT NULL COMMENT '运输方式',
  `transport_fee` decimal(18,2) DEFAULT NULL COMMENT '运输费用',
  `currency` varchar(50) DEFAULT NULL COMMENT '币种',
  `tax` int(4) DEFAULT '0' COMMENT '是否含税',
  `remark` varchar(2000) DEFAULT NULL COMMENT '备注',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='生产订单';

-- ----------------------------
-- Table structure for prod_order_delete_index
-- ----------------------------
DROP TABLE IF EXISTS `prod_order_delete_index`;
CREATE TABLE `prod_order_delete_index` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品信息',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COMMENT='生产订单';

-- ----------------------------
-- Table structure for prod_order_goods
-- ----------------------------
DROP TABLE IF EXISTS `prod_order_goods`;
CREATE TABLE `prod_order_goods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(50) DEFAULT NULL COMMENT '编码',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `customer_code` varchar(50) DEFAULT NULL,
  `goods_code` varchar(50) DEFAULT NULL COMMENT '规格编号',
  `customer_model_prop` varchar(50) DEFAULT NULL COMMENT '客户产品属性',
  `customer_model_name` varchar(50) DEFAULT NULL COMMENT '客户型号名称',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '生产数量',
  `finish_quantity` decimal(18,2) DEFAULT '0.00' COMMENT '已完成数量',
  `total` int(4) DEFAULT '0',
  `send_quantity` decimal(18,2) DEFAULT '0.00',
  `price` decimal(18,2) DEFAULT NULL COMMENT '单价',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `goods_status` varchar(50) DEFAULT NULL COMMENT '商品生产状态',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COMMENT='订单-步骤';

-- ----------------------------
-- Table structure for prod_order_index
-- ----------------------------
DROP TABLE IF EXISTS `prod_order_index`;
CREATE TABLE `prod_order_index` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品信息',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='生产订单';

-- ----------------------------
-- Table structure for prod_order_send
-- ----------------------------
DROP TABLE IF EXISTS `prod_order_send`;
CREATE TABLE `prod_order_send` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `send_code` varchar(255) DEFAULT NULL COMMENT '发货单单号',
  `order_status` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `send_time` datetime DEFAULT NULL COMMENT '发货时间',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '仓库编码',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `bill_status` int(4) DEFAULT '0' COMMENT '对账状态',
  `currency` varchar(50) DEFAULT NULL COMMENT '币种',
  `total` int(4) DEFAULT NULL,
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '总数量',
  `total_amount` decimal(18,2) DEFAULT NULL COMMENT '总金额',
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='生产订单';

-- ----------------------------
-- Table structure for prod_order_step
-- ----------------------------
DROP TABLE IF EXISTS `prod_order_step`;
CREATE TABLE `prod_order_step` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(50) DEFAULT NULL COMMENT '唯一编码',
  `material_code` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `step_code` varchar(50) DEFAULT NULL COMMENT '步骤编号',
  `in_warehouse_code` varchar(50) DEFAULT NULL COMMENT '入库仓库',
  `out_warehouse_code` varchar(50) DEFAULT NULL COMMENT '出库仓库',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='订单-步骤';

-- ----------------------------
-- Table structure for prod_purchase
-- ----------------------------
DROP TABLE IF EXISTS `prod_purchase`;
CREATE TABLE `prod_purchase` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `business_order_no` varchar(50) DEFAULT NULL COMMENT '客户订单号-生产',
  `customer_code` varchar(255) DEFAULT NULL COMMENT '客户',
  `order_status` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `send_time` datetime DEFAULT NULL COMMENT '发货时间',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '仓库编码',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '数量',
  `finish_quantity` decimal(18,2) DEFAULT NULL COMMENT '完成数量',
  `total` int(4) DEFAULT '0' COMMENT '卷数',
  `total_amount` decimal(18,2) DEFAULT NULL,
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单',
  `bill_status` int(4) DEFAULT '0' COMMENT '对账状态',
  `pay_type` varchar(50) DEFAULT NULL COMMENT '付款方式',
  `transport` varchar(50) DEFAULT NULL COMMENT '运输方式',
  `transport_fee` decimal(18,2) DEFAULT NULL COMMENT '运输费用',
  `currency` varchar(50) DEFAULT NULL COMMENT '币种',
  `tax` int(4) DEFAULT NULL COMMENT '是否含税',
  `remark` varchar(2000) DEFAULT NULL COMMENT '备注',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='采购订单';

-- ----------------------------
-- Table structure for prod_purchase_back
-- ----------------------------
DROP TABLE IF EXISTS `prod_purchase_back`;
CREATE TABLE `prod_purchase_back` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `back_code` varchar(255) DEFAULT NULL COMMENT '退货单',
  `order_status` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `out_time` datetime DEFAULT NULL COMMENT '入库时间',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '仓库编码',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `quantity` decimal(18,4) DEFAULT NULL COMMENT '数量',
  `total` int(4) DEFAULT '0',
  `total_amount` decimal(18,4) DEFAULT NULL,
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单',
  `bill_status` int(4) DEFAULT '0' COMMENT '对账状态',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='采购订单-退货';

-- ----------------------------
-- Table structure for prod_purchase_back_record
-- ----------------------------
DROP TABLE IF EXISTS `prod_purchase_back_record`;
CREATE TABLE `prod_purchase_back_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `record_code` varchar(50) DEFAULT NULL COMMENT '记录编号',
  `order_no` varchar(50) DEFAULT NULL COMMENT '销售订单',
  `back_code` varchar(50) DEFAULT NULL COMMENT '退货单',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `quantity` decimal(18,2) DEFAULT '0.00' COMMENT '数量',
  `total` int(10) DEFAULT NULL COMMENT '卷数',
  `price` decimal(18,2) DEFAULT NULL COMMENT '价格',
  `batch_no` varchar(50) DEFAULT '0' COMMENT '批次号',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `serial_number` varchar(50) DEFAULT NULL COMMENT '序列号',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='采购退回记录明细';

-- ----------------------------
-- Table structure for prod_purchase_deduction
-- ----------------------------
DROP TABLE IF EXISTS `prod_purchase_deduction`;
CREATE TABLE `prod_purchase_deduction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deduction_code` varchar(50) DEFAULT NULL COMMENT '扣款单号',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `business_code` varchar(50) DEFAULT NULL COMMENT '业务单号',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `amount` decimal(18,2) DEFAULT NULL COMMENT '扣款金额',
  `remark` varchar(500) DEFAULT NULL COMMENT '描述',
  `order_status` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `bill_status` int(4) DEFAULT '0' COMMENT '对账状态',
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单号',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='采购-扣款';

-- ----------------------------
-- Table structure for prod_purchase_in
-- ----------------------------
DROP TABLE IF EXISTS `prod_purchase_in`;
CREATE TABLE `prod_purchase_in` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `in_code` varchar(255) DEFAULT NULL COMMENT '入库单',
  `order_status` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `in_time` datetime DEFAULT NULL COMMENT '入库时间',
  `out_send_time` date DEFAULT NULL COMMENT '外部对账时间',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '仓库编码',
  `out_business_code` varchar(50) DEFAULT NULL COMMENT '外部单号',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '数量',
  `total` int(4) DEFAULT '0',
  `total_amount` decimal(18,2) DEFAULT NULL,
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单',
  `bill_status` int(4) DEFAULT '0' COMMENT '对账状态',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='采购订单-入库';

-- ----------------------------
-- Table structure for prod_purchase_in_record
-- ----------------------------
DROP TABLE IF EXISTS `prod_purchase_in_record`;
CREATE TABLE `prod_purchase_in_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `record_code` varchar(50) DEFAULT NULL COMMENT '记录编号',
  `order_no` varchar(50) DEFAULT NULL COMMENT '销售订单',
  `in_code` varchar(50) DEFAULT NULL COMMENT '入库',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `customer_code` varchar(50) DEFAULT NULL,
  `quantity` decimal(18,2) DEFAULT '0.00' COMMENT '数量',
  `total` int(10) DEFAULT NULL COMMENT '卷数',
  `price` decimal(18,2) DEFAULT NULL COMMENT '价格',
  `batch_no` varchar(50) DEFAULT '0' COMMENT '批次号',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `serial_number` varchar(50) DEFAULT NULL COMMENT '序列号',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8 COMMENT='采购退回记录明细';

-- ----------------------------
-- Table structure for prod_purchase_record
-- ----------------------------
DROP TABLE IF EXISTS `prod_purchase_record`;
CREATE TABLE `prod_purchase_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(50) DEFAULT NULL COMMENT '唯一编号',
  `order_no` varchar(50) DEFAULT NULL COMMENT '销售订单',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `quantity` decimal(18,2) DEFAULT '0.00' COMMENT '数量',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `price` decimal(18,2) DEFAULT NULL COMMENT '单价',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COMMENT='销售记录明细';

-- ----------------------------
-- Table structure for prod_sale
-- ----------------------------
DROP TABLE IF EXISTS `prod_sale`;
CREATE TABLE `prod_sale` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `customer_code` varchar(255) DEFAULT NULL COMMENT '客户',
  `order_status` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `send_time` datetime DEFAULT NULL COMMENT '发货时间',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '仓库编码',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '数量',
  `finish_quantity` decimal(18,2) DEFAULT NULL,
  `total_amount` decimal(18,2) DEFAULT NULL COMMENT '总金额',
  `total` int(4) DEFAULT '0',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `bill_status` int(4) DEFAULT '0' COMMENT '对账状态',
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单',
  `pay_type` varchar(50) DEFAULT NULL COMMENT '付款方式',
  `transport` varchar(50) DEFAULT NULL COMMENT '运输方式',
  `transport_fee` decimal(18,2) DEFAULT NULL COMMENT '运输费用',
  `currency` varchar(50) DEFAULT NULL COMMENT '币种',
  `tax` int(4) DEFAULT NULL COMMENT '是否含税',
  `remark` varchar(2000) DEFAULT NULL COMMENT '备注',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='销售订单';

-- ----------------------------
-- Table structure for prod_sale_back
-- ----------------------------
DROP TABLE IF EXISTS `prod_sale_back`;
CREATE TABLE `prod_sale_back` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `back_code` varchar(255) DEFAULT NULL COMMENT '退货单',
  `order_status` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `in_time` datetime DEFAULT NULL COMMENT '入库时间',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '仓库编码',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '数量',
  `total` int(4) DEFAULT '0',
  `total_amount` decimal(18,2) DEFAULT NULL,
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `bill_status` int(4) DEFAULT '0' COMMENT '对账状态',
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='销售订单-退货';

-- ----------------------------
-- Table structure for prod_sale_back_record
-- ----------------------------
DROP TABLE IF EXISTS `prod_sale_back_record`;
CREATE TABLE `prod_sale_back_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `record_code` varchar(50) DEFAULT NULL COMMENT '记录编号',
  `order_no` varchar(50) DEFAULT NULL COMMENT '销售订单',
  `back_code` varchar(50) DEFAULT NULL COMMENT '退货单',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `quantity` decimal(18,2) DEFAULT '0.00' COMMENT '数量',
  `total` int(10) DEFAULT NULL COMMENT '卷数',
  `batch_no` varchar(50) DEFAULT '0' COMMENT '批次号',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `serial_number` varchar(50) DEFAULT NULL COMMENT '序列号',
  `price` decimal(18,2) DEFAULT NULL,
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='销售退回记录明细';

-- ----------------------------
-- Table structure for prod_sale_out
-- ----------------------------
DROP TABLE IF EXISTS `prod_sale_out`;
CREATE TABLE `prod_sale_out` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `out_code` varchar(255) DEFAULT NULL COMMENT '出库',
  `order_status` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `out_time` datetime DEFAULT NULL COMMENT '入库时间',
  `customer_code` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '仓库编码',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '数量',
  `total_amount` decimal(18,2) DEFAULT NULL,
  `total` int(4) DEFAULT '0',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `bill_status` int(4) DEFAULT '0' COMMENT '对账状态',
  `bill_no` varchar(50) DEFAULT NULL COMMENT '对账单',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='销售订单-退货';

-- ----------------------------
-- Table structure for prod_sale_out_record
-- ----------------------------
DROP TABLE IF EXISTS `prod_sale_out_record`;
CREATE TABLE `prod_sale_out_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `record_code` varchar(50) DEFAULT NULL COMMENT '记录编号',
  `order_no` varchar(50) DEFAULT NULL COMMENT '销售订单',
  `out_code` varchar(50) DEFAULT NULL COMMENT '退货单',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `quantity` decimal(18,2) DEFAULT '0.00' COMMENT '数量',
  `total` int(10) DEFAULT NULL COMMENT '卷数',
  `batch_no` varchar(50) DEFAULT '0' COMMENT '批次号',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `serial_number` varchar(50) DEFAULT NULL COMMENT '序列号',
  `price` decimal(18,2) DEFAULT NULL,
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='销售退回记录明细';

-- ----------------------------
-- Table structure for prod_sale_record
-- ----------------------------
DROP TABLE IF EXISTS `prod_sale_record`;
CREATE TABLE `prod_sale_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(50) DEFAULT NULL COMMENT '唯一编号',
  `order_no` varchar(50) DEFAULT NULL COMMENT '销售订单',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `quantity` decimal(18,2) DEFAULT '0.00' COMMENT '数量',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `price` decimal(18,2) DEFAULT NULL COMMENT '单价',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COMMENT='销售记录明细';

-- ----------------------------
-- Table structure for prod_send_record
-- ----------------------------
DROP TABLE IF EXISTS `prod_send_record`;
CREATE TABLE `prod_send_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(50) DEFAULT NULL COMMENT '唯一码',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单号',
  `send_code` varchar(50) DEFAULT NULL COMMENT '加工单号',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `total` int(4) DEFAULT NULL,
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '数量',
  `delete_quantity` decimal(18,2) DEFAULT '0.00' COMMENT '扣减数量',
  `real_quantity` decimal(18,2) DEFAULT '0.00' COMMENT '实际数量',
  `goods_weight` decimal(18,2) DEFAULT NULL COMMENT '总量',
  `goods_net_weight` decimal(18,2) DEFAULT NULL,
  `gang_no` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT '0' COMMENT '批次号',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `serial_number` varchar(50) DEFAULT NULL COMMENT '序列号',
  `in_code` varchar(50) DEFAULT NULL COMMENT '业务入库单号',
  `price` decimal(18,2) DEFAULT NULL COMMENT '单价',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 COMMENT='加工单物料明细';

-- ----------------------------
-- Table structure for prod_step
-- ----------------------------
DROP TABLE IF EXISTS `prod_step`;
CREATE TABLE `prod_step` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `step_code` varchar(50) DEFAULT NULL COMMENT '步骤编码',
  `step_name` varchar(50) DEFAULT NULL COMMENT '步骤名称',
  `goods_status` int(50) DEFAULT '0' COMMENT '是否产出成品',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `order_status` int(4) DEFAULT NULL COMMENT '是否订单维度',
  `quick_status` int(4) DEFAULT NULL COMMENT '是否支撑快捷入库',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='生产步骤';

-- ----------------------------
-- Table structure for sys_app
-- ----------------------------
DROP TABLE IF EXISTS `sys_app`;
CREATE TABLE `sys_app` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `app_code` varchar(50) DEFAULT NULL COMMENT '应用编码',
  `app_name` varchar(50) DEFAULT NULL COMMENT '应用名称',
  `app_key` varchar(50) DEFAULT NULL COMMENT '应用key',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='系统应用表';

-- ----------------------------
-- Table structure for sys_company
-- ----------------------------
DROP TABLE IF EXISTS `sys_company`;
CREATE TABLE `sys_company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `company_code` varchar(50) DEFAULT NULL COMMENT '公司编码',
  `company_name` varchar(50) DEFAULT NULL COMMENT '公司名称',
  `parent_company_code` varchar(50) DEFAULT '0' COMMENT '上级公司',
  `credit_code` varchar(50) DEFAULT NULL COMMENT '统一社会信用代码',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机号码',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='公司主表';

-- ----------------------------
-- Table structure for sys_company_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_company_info`;
CREATE TABLE `sys_company_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `company_code` varchar(50) DEFAULT NULL COMMENT '公司编号',
  `name` varchar(50) DEFAULT NULL COMMENT '属性名称',
  `prop` varchar(50) DEFAULT NULL COMMENT '属性名',
  `value` varchar(500) DEFAULT NULL COMMENT '属性值',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_bms_process_data_business_code` (`company_code`),
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1651623 DEFAULT CHARSET=utf8 COMMENT='公司扩展信息';

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `file_code` varchar(50) DEFAULT NULL COMMENT '文件编码',
  `business_code` varchar(50) DEFAULT NULL COMMENT '业务单号',
  `business_type` varchar(50) DEFAULT NULL COMMENT '业务类型',
  `file_name` varchar(50) DEFAULT NULL COMMENT '文件名称',
  `file_url` varchar(300) DEFAULT NULL COMMENT '文件地址',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Table structure for sys_login
-- ----------------------------
DROP TABLE IF EXISTS `sys_login`;
CREATE TABLE `sys_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `login_name` varchar(50) DEFAULT NULL COMMENT '用户名称',
  `password` varchar(50) DEFAULT NULL COMMENT '手机号码',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `company_code` varchar(50) DEFAULT NULL COMMENT '当前登录公司',
  `session_key` varchar(50) DEFAULT NULL COMMENT '登录鉴权',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='用户登录信息表';

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名称',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `device_ip` varchar(50) DEFAULT NULL COMMENT '设备IP',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=591 DEFAULT CHARSET=utf8 COMMENT='用户登录日志';

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_code` varchar(50) DEFAULT NULL COMMENT '菜单编码',
  `menu_name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `menu_type` varchar(50) DEFAULT NULL COMMENT '菜单类型 菜单   按钮，功能',
  `parent_menu_code` varchar(50) DEFAULT '0' COMMENT '上级菜单',
  `menu_url` varchar(500) DEFAULT NULL COMMENT '菜单路径',
  `auth_url` varchar(50) DEFAULT NULL COMMENT '权限路径',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标样式',
  `permission` varchar(50) DEFAULT NULL COMMENT '权限码',
  `component` varchar(255) DEFAULT NULL COMMENT '组件',
  `open_type` varchar(50) DEFAULT NULL COMMENT '打开方式',
  `app_code` varchar(50) DEFAULT NULL COMMENT '所属应用',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=618 DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

-- ----------------------------
-- Table structure for sys_op_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_op_log`;
CREATE TABLE `sys_op_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名称',
  `name` varchar(50) DEFAULT NULL COMMENT '功能名称',
  `op_time` datetime DEFAULT NULL COMMENT '操作时间',
  `params` varchar(2000) DEFAULT NULL COMMENT '设备IP',
  `result` text COMMENT '操作结果',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户操作日志';

-- ----------------------------
-- Table structure for sys_organization
-- ----------------------------
DROP TABLE IF EXISTS `sys_organization`;
CREATE TABLE `sys_organization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `company_code` varchar(50) DEFAULT NULL COMMENT '公司编码',
  `dept_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `dept_name` varchar(50) DEFAULT NULL COMMENT '组织名称',
  `parent_dept_code` varchar(50) DEFAULT '0' COMMENT '上级组织',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Table structure for sys_position
-- ----------------------------
DROP TABLE IF EXISTS `sys_position`;
CREATE TABLE `sys_position` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `position_code` varchar(50) DEFAULT NULL COMMENT '职位编码',
  `position_name` varchar(50) DEFAULT NULL COMMENT '职位名称',
  `data_auth` varchar(50) DEFAULT NULL COMMENT '数据权限',
  `useble` int(50) DEFAULT '1' COMMENT '是否可以选择',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_position_code` (`position_code`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='用户菜单关联';

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_code` varchar(50) DEFAULT NULL COMMENT '角色编码',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `app_code` varchar(50) DEFAULT NULL COMMENT '所属应用',
  `can_delete` int(4) DEFAULT '1' COMMENT '是否删除  1:能 0:不能',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名称',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `sex` varchar(50) DEFAULT NULL COMMENT '性别',
  `id_card` varchar(50) DEFAULT NULL COMMENT '身份证',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='用户基本信息表';

-- ----------------------------
-- Table structure for sys_user_company
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_company`;
CREATE TABLE `sys_user_company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `comapny_code` varchar(50) DEFAULT NULL COMMENT '公司编码',
  `dept_code` varchar(200) DEFAULT NULL COMMENT '组织编码 当用户属于公司 org_code是空',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(255) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='用户公司关联表';

-- ----------------------------
-- Table structure for sys_user_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_info`;
CREATE TABLE `sys_user_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编号',
  `name` varchar(50) DEFAULT NULL COMMENT '属性名称',
  `prop` varchar(50) DEFAULT NULL COMMENT '属性名',
  `value` varchar(500) DEFAULT NULL COMMENT '属性值',
  `company_code` varchar(200) DEFAULT NULL COMMENT '公司编码',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_bms_process_data_business_code` (`user_code`),
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1651599 DEFAULT CHARSET=utf8 COMMENT='用户扩展信息';

-- ----------------------------
-- Table structure for sys_user_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_menu`;
CREATE TABLE `sys_user_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_code` varchar(50) DEFAULT NULL COMMENT '角色编码',
  `menu_code` varchar(50) DEFAULT NULL COMMENT '菜单编码',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=349 DEFAULT CHARSET=utf8 COMMENT='用户菜单关联';

-- ----------------------------
-- Table structure for sys_user_position
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_position`;
CREATE TABLE `sys_user_position` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `company_code` varchar(50) DEFAULT NULL COMMENT '公司',
  `position_code` varchar(50) DEFAULT NULL COMMENT '职位编码',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='用户菜单关联';

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_code` varchar(50) DEFAULT NULL COMMENT '角色编码',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='用户菜单关联';

-- ----------------------------
-- Table structure for sys_work
-- ----------------------------
DROP TABLE IF EXISTS `sys_work`;
CREATE TABLE `sys_work` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `work_code` varchar(50) DEFAULT NULL COMMENT '待办编码',
  `work_title` varchar(50) DEFAULT NULL COMMENT '待办标题',
  `work_content` varchar(200) DEFAULT NULL COMMENT '待办内容',
  `work_start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `work_end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='我的待办任务';

-- ----------------------------
-- Table structure for wms_order_allot
-- ----------------------------
DROP TABLE IF EXISTS `wms_order_allot`;
CREATE TABLE `wms_order_allot` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '入库单',
  `in_warehouse_code` varchar(50) DEFAULT NULL COMMENT '入库仓库',
  `out_warehouse_code` varchar(50) DEFAULT NULL COMMENT '出库仓库',
  `order_type` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `order_time` datetime DEFAULT NULL COMMENT '订单操作时间',
  `order_user_code` varchar(50) DEFAULT NULL COMMENT '订单操作人',
  `order_user_name` varchar(50) DEFAULT NULL COMMENT '订单操作人',
  `order_status` varchar(50) DEFAULT NULL COMMENT '单据状态',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='调拨单';

-- ----------------------------
-- Table structure for wms_order_in
-- ----------------------------
DROP TABLE IF EXISTS `wms_order_in`;
CREATE TABLE `wms_order_in` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '入库单',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '仓库编号',
  `business_code` varchar(50) DEFAULT NULL COMMENT '业务单号',
  `business_order_no` varchar(50) DEFAULT NULL COMMENT '业务订单号',
  `order_type` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `order_time` datetime DEFAULT NULL COMMENT '订单操作时间',
  `order_user_code` varchar(50) DEFAULT NULL COMMENT '订单操作人',
  `order_user_name` varchar(50) DEFAULT NULL COMMENT '订单操作人',
  `order_status` varchar(50) DEFAULT NULL COMMENT '单据状态',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8 COMMENT='入库单';

-- ----------------------------
-- Table structure for wms_order_out
-- ----------------------------
DROP TABLE IF EXISTS `wms_order_out`;
CREATE TABLE `wms_order_out` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '入库单',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '仓库编号',
  `business_code` varchar(50) DEFAULT NULL COMMENT '业务单号',
  `business_order_no` varchar(50) DEFAULT NULL COMMENT '业务订单号',
  `order_type` varchar(50) DEFAULT NULL COMMENT '订单状态',
  `order_time` datetime DEFAULT NULL COMMENT '订单操作时间',
  `order_user_code` varchar(50) DEFAULT NULL COMMENT '订单操作人',
  `order_user_name` varchar(50) DEFAULT NULL COMMENT '订单操作人',
  `order_status` varchar(50) DEFAULT NULL COMMENT '单据状态',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8 COMMENT='出库单';

-- ----------------------------
-- Table structure for wms_order_record
-- ----------------------------
DROP TABLE IF EXISTS `wms_order_record`;
CREATE TABLE `wms_order_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单号',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '数量',
  `total` int(4) DEFAULT '0',
  `batch_no` varchar(50) DEFAULT '0' COMMENT '批次号',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `serial_number` varchar(50) DEFAULT NULL COMMENT '序列号',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1007 DEFAULT CHARSET=utf8 COMMENT='出入库单明细';

-- ----------------------------
-- Table structure for wms_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `wms_warehouse`;
CREATE TABLE `wms_warehouse` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '仓库编码',
  `warehouse_name` varchar(50) DEFAULT NULL COMMENT '仓库名称',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='仓库';

-- ----------------------------
-- Table structure for wms_warehouse_stock
-- ----------------------------
DROP TABLE IF EXISTS `wms_warehouse_stock`;
CREATE TABLE `wms_warehouse_stock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_code` varchar(50) DEFAULT NULL COMMENT '记录唯一单号',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '仓库编码',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `lock_quantity` decimal(18,2) DEFAULT NULL COMMENT '锁数量',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '数量',
  `batch_no` varchar(50) DEFAULT NULL COMMENT '批次号',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE,
  KEY `index_warehouse_code` (`warehouse_code`),
  KEY `index_goods_code` (`goods_code`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 COMMENT='仓库库存';

-- ----------------------------
-- Table structure for wms_warehouse_stock_detail
-- ----------------------------
DROP TABLE IF EXISTS `wms_warehouse_stock_detail`;
CREATE TABLE `wms_warehouse_stock_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_code` varchar(50) DEFAULT NULL COMMENT '记录唯一单号',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '仓库编码',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `lock_quantity` decimal(18,2) DEFAULT NULL COMMENT '锁数量',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '数量',
  `batch_no` varchar(50) DEFAULT '0' COMMENT '批次号',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `total` int(4) DEFAULT '0',
  `serial_number` varchar(50) DEFAULT '' COMMENT '序列号',
  `business_order_no` varchar(50) DEFAULT '0' COMMENT '订单编号',
  `in_time` datetime DEFAULT NULL COMMENT '入库时间',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE,
  KEY `index_create_time` (`create_time`)
) ENGINE=InnoDB AUTO_INCREMENT=419 DEFAULT CHARSET=utf8 COMMENT='仓库库存明细';

-- ----------------------------
-- Table structure for wms_warehouse_stock_log
-- ----------------------------
DROP TABLE IF EXISTS `wms_warehouse_stock_log`;
CREATE TABLE `wms_warehouse_stock_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_code` varchar(50) DEFAULT NULL COMMENT '唯一编号',
  `warehouse_code` varchar(50) DEFAULT NULL COMMENT '仓库编码',
  `order_no` varchar(255) DEFAULT NULL COMMENT '出入库订单',
  `in_out_type` varchar(50) DEFAULT NULL COMMENT '出入库类型',
  `goods_code` varchar(50) DEFAULT NULL COMMENT '商品编码',
  `quantity` decimal(18,2) DEFAULT NULL COMMENT '数量',
  `batch_no` varchar(50) DEFAULT '0' COMMENT '批次号',
  `total` int(4) DEFAULT '0',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `serial_number` varchar(50) DEFAULT NULL COMMENT '序列号',
  `in_time` datetime DEFAULT NULL COMMENT '入库时间',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE,
  KEY `index_create_time` (`create_time`)
) ENGINE=InnoDB AUTO_INCREMENT=973 DEFAULT CHARSET=utf8 COMMENT='仓库库存明细';
