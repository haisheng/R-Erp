/*
Navicat MySQL Data Transfer

Source Server         : 阿里云数据库
Source Server Version : 50725
Source Host           : rm-uf6hql22bjf3668ebro.mysql.rds.aliyuncs.com:3306
Source Database       : ray_erp

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2020-09-14 14:51:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_app
-- ----------------------------
DROP TABLE IF EXISTS `sys_app`;
CREATE TABLE `sys_app` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `app_code` varchar(50) DEFAULT NULL COMMENT '应用编码',
  `app_name` varchar(50) DEFAULT NULL COMMENT '应用名称',
  `app_key` varchar(50) DEFAULT NULL COMMENT '应用key',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='系统应用表';

-- ----------------------------
-- Records of sys_app
-- ----------------------------
INSERT INTO `sys_app` VALUES ('2', 'APP15906275620845638', 'ERP系统', 'ADMIN', '1', '0', '2020-05-28 08:59:20', 'U001', '管理员', '2020-06-23 17:16:29', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');

-- ----------------------------
-- Table structure for sys_company
-- ----------------------------
DROP TABLE IF EXISTS `sys_company`;
CREATE TABLE `sys_company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `company_code` varchar(50) DEFAULT NULL COMMENT '公司编码',
  `company_name` varchar(50) DEFAULT NULL COMMENT '公司名称',
  `parent_company_code` varchar(50) DEFAULT '0' COMMENT '上级公司',
  `credit_code` varchar(50) DEFAULT NULL COMMENT '统一社会信用代码',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机号码',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='公司主表';

-- ----------------------------
-- Records of sys_company
-- ----------------------------
INSERT INTO `sys_company` VALUES ('1', 'CP01', '主体运营公司', '0', null, '13867135741', '1', '0', '2020-05-27 12:23:13', 'UC001', '管理员', '2020-05-31 14:42:50', null, null, 'CP01', 'UC001', 'CP01', '1', '0');
INSERT INTO `sys_company` VALUES ('2', 'CO15907458659568425', 'T纺织有限公司', '0', '', '13800000000', '1', '0', '2020-05-29 17:51:06', 'U001', '管理员', '2020-06-23 17:08:35', 'U001', '管理员', 'CP01', 'U001', 'CP01', '2', '0');
INSERT INTO `sys_company` VALUES ('3', 'CO15907458659434343', 'R纺织科技有限公司', '0', '', '13800000004', '1', '0', '2020-05-31 14:55:03', 'U001', '管理员', '2020-06-21 19:47:46', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company` VALUES ('4', 'CP01|4578', '我的子公司', 'CP01', null, '13800000010', '1', '0', '2020-05-31 21:33:38', 'U001', '管理员', '2020-06-18 18:12:05', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company` VALUES ('9', 'CO15907202006218096', 'R-Erp测试公司', '0', null, '17700000000', '1', '0', '2020-06-21 20:01:25', 'U001', '管理员', '2020-06-21 20:02:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company` VALUES ('10', 'CO15907202006218096|202006266648', '测试子公司', 'CO15907202006218096', '666666', '18888888888', '1', '0', '2020-06-26 21:37:05', 'UC15927410664004948', '默认测试管理员', '2020-06-26 21:37:21', 'UC15927410664004948', '默认测试管理员', 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');

-- ----------------------------
-- Table structure for sys_company_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_company_info`;
CREATE TABLE `sys_company_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `company_code` varchar(50) DEFAULT NULL COMMENT '公司编号',
  `name` varchar(50) DEFAULT NULL COMMENT '属性名称',
  `prop` varchar(50) DEFAULT NULL COMMENT '属性名',
  `value` varchar(500) DEFAULT NULL COMMENT '属性值',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_bms_process_data_business_code` (`company_code`),
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1651623 DEFAULT CHARSET=utf8 COMMENT='公司扩展信息';

-- ----------------------------
-- Records of sys_company_info
-- ----------------------------
INSERT INTO `sys_company_info` VALUES ('1651599', 'CO15907458659568425', '法人信息', 'legalPerson', 'ADMIN', '1', '2020-05-29 17:51:06', 'U001', '管理员', '2020-05-31 14:59:22', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651600', 'CO15907458659568425', '公司地址', 'address', '测试地址', '1', '2020-05-29 17:51:06', 'U001', '管理员', '2020-05-31 14:59:22', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651601', 'CO15907458659434343', '法人信息', 'legalPerson', '测试', '1', '2020-05-31 14:55:04', 'U001', '管理员', '2020-06-18 18:31:47', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651602', 'CO15907458659434343', '公司地址', 'address', '测试', '1', '2020-05-31 14:55:04', 'U001', '管理员', '2020-06-18 18:31:50', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651603', 'CO15907458659568425', '法人信息', 'legalPerson', 'ADMIN1', '1', '2020-05-31 14:59:24', 'U001', '管理员', '2020-06-15 23:30:20', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651604', 'CO15907458659568425', '公司地址', 'address', '测试地址1', '1', '2020-05-31 14:59:24', 'U001', '管理员', '2020-06-15 23:30:20', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651605', 'CP01|4578', '法人信息', 'legalPerson', null, '0', '2020-05-31 21:33:39', 'U001', '管理员', '2020-06-18 18:32:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651606', 'CP01|4578', '公司地址', 'address', null, '0', '2020-05-31 21:33:39', 'U001', '管理员', '2020-06-18 18:32:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651607', 'CO15907458659568425', '法人信息', 'legalPerson', null, '0', '2020-06-15 23:30:20', 'U001', '管理员', '2020-06-15 23:30:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651608', 'CO15907458659568425', '公司地址', 'address', '海宁', '0', '2020-06-15 23:30:20', 'U001', '管理员', '2020-06-15 23:30:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651609', 'CO15907458659434343', '法人信息', 'legalPerson', null, '0', '2020-06-15 23:30:54', 'U001', '管理员', '2020-06-18 18:31:57', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651610', 'CO15907458659434343', '公司地址', 'address', null, '0', '2020-06-15 23:30:54', 'U001', '管理员', '2020-06-18 18:31:59', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651611', 'CO15907202006218096', '法人信息', 'legalPerson', null, '0', '2020-06-18 22:24:14', 'U001', '管理员', '2020-06-21 20:03:31', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651612', 'CO15907202006218096', '公司地址', 'address', null, '0', '2020-06-18 22:24:14', 'U001', '管理员', '2020-06-21 20:03:34', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651621', 'CO15907202006218096|202006266648', '法人信息', 'legalPerson', '李牧之', '0', '2020-06-26 21:37:05', 'UC15927410664004948', '默认测试管理员', '2020-06-26 21:37:05', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_company_info` VALUES ('1651622', 'CO15907202006218096|202006266648', '公司地址', 'address', '666大道', '0', '2020-06-26 21:37:05', 'UC15927410664004948', '默认测试管理员', '2020-06-26 21:37:05', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `file_code` varchar(50) DEFAULT NULL COMMENT '文件编码',
  `business_code` varchar(50) DEFAULT NULL COMMENT '业务单号',
  `business_type` varchar(50) DEFAULT NULL COMMENT '业务类型',
  `file_name` varchar(50) DEFAULT NULL COMMENT '文件名称',
  `file_url` varchar(300) DEFAULT NULL COMMENT '文件地址',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES ('17', 'GC2020071413263916', 'C2020071413267147', 'PAY', '01300535031999137270128786964.jpg', '2020/07/14/e52b0963-ff68-4aec-b82c-db5ae46ba718.01300535031999137270128786964.jpg', '1', '0', '2020-07-14 13:26:45', 'U001', '管理员', '2020-07-14 13:26:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('18', 'GC2020071413302856', 'C2020071413307869', 'PAY', '01300535031999137270128786964.jpg', '2020/07/14/f063db9d-3d72-4ceb-9963-5449552528d5.01300535031999137270128786964.jpg', '1', '0', '2020-07-14 13:30:26', 'U001', '管理员', '2020-07-14 13:30:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('19', 'GC2020071413306570', 'C2020071413307869', 'PAY', '01300535031999137270128786964.jpg', '2020/07/14/198bd58f-199c-4a1f-9092-57edbc9f8b4f.01300535031999137270128786964.jpg', '1', '0', '2020-07-14 13:30:26', 'U001', '管理员', '2020-07-14 13:30:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('20', 'GC2020071417160650', 'C2020071417169169', 'PAY', '01300535031999137270128786964.jpg', '2020/07/14/9100c84a-f1e6-4b86-a048-2921970b987c.01300535031999137270128786964.jpg', '1', '0', '2020-07-14 17:16:49', 'UC15909249151008102', '沈括', '2020-07-14 17:16:49', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_file` VALUES ('21', 'GC2020071917158258', 'CG2020071917102549', 'PURCHASE', '南充用户.xlsx', '2020/07/19/ac3d43f7-2491-4969-b7ac-db05cf21dd19.南充用户.xlsx', '1', '1', '2020-07-19 17:15:11', 'U001', '管理员', '2020-07-19 17:18:32', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('22', 'GC2020071917197646', 'CG2020071917102549', 'PURCHASE', '南充用户.xlsx', '2020/07/19/2fa1dfc9-3454-4ed0-8f24-db66ed016dbd.南充用户.xlsx', '1', '1', '2020-07-19 17:19:13', 'U001', '管理员', '2020-07-19 17:27:56', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('23', 'GC2020071917193335', 'CG2020071917102549', 'PURCHASE', '01300535031999137270128786964.jpg', '2020/07/19/3896bbe8-c371-4f82-8e49-b54eee1b97bb.01300535031999137270128786964.jpg', '1', '1', '2020-07-19 17:19:13', 'U001', '管理员', '2020-07-19 17:27:56', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('24', 'GC2020071917270495', 'CG2020071917102549', 'PURCHASE', '01300535031999137270128786964.jpg', '2020/07/19/f31eaebc-5299-4def-b928-f18155485d0e.01300535031999137270128786964.jpg', '1', '1', '2020-07-19 17:27:56', 'U001', '管理员', '2020-07-19 17:29:31', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('25', 'GC2020071917291215', 'CG2020071917102549', 'PURCHASE', '01300535031999137270128786964.jpg', '2020/07/19/f31eaebc-5299-4def-b928-f18155485d0e.01300535031999137270128786964.jpg', '1', '1', '2020-07-19 17:29:31', 'U001', '管理员', '2020-07-19 17:30:48', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('26', 'GC2020071917301296', 'CG2020071917102549', 'PURCHASE', '01300535031999137270128786964.jpg', '2020/07/19/f31eaebc-5299-4def-b928-f18155485d0e.01300535031999137270128786964.jpg', '1', '1', '2020-07-19 17:30:48', 'U001', '管理员', '2020-07-19 17:35:08', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('27', 'GC2020071917352848', 'CG2020071917102549', 'PURCHASE', '01300535031999137270128786964.jpg', '2020/07/19/f31eaebc-5299-4def-b928-f18155485d0e.01300535031999137270128786964.jpg', '1', '0', '2020-07-19 17:35:08', 'U001', '管理员', '2020-07-19 17:35:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('28', 'GC2020071917420819', 'PI2020071917424761', 'PURCHASE_IN', '01300535031999137270128786964.jpg', '2020/07/19/5da14094-7e0a-4e3f-9e0b-2e470542a969.01300535031999137270128786964.jpg', '1', '0', '2020-07-19 17:42:08', 'U001', '管理员', '2020-07-19 17:44:59', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('29', 'GC2020071917436418', 'PI2020071917432294', 'PURCHASE_IN', '01300535031999137270128786964.jpg', '2020/07/19/5da14094-7e0a-4e3f-9e0b-2e470542a969.01300535031999137270128786964.jpg', '1', '0', '2020-07-19 17:43:52', 'U001', '管理员', '2020-07-19 17:45:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('30', 'GC2020071917446284', 'PI2020071917444993', 'PURCHASE_IN', '01300535031999137270128786964.jpg', '2020/07/19/5da14094-7e0a-4e3f-9e0b-2e470542a969.01300535031999137270128786964.jpg', '1', '1', '2020-07-19 17:44:48', 'U001', '管理员', '2020-07-19 17:49:21', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('31', 'GC2020071917499397', 'PI2020071917444993', 'PURCHASE_IN', '01300535031999137270128786964.jpg', '2020/07/19/5da14094-7e0a-4e3f-9e0b-2e470542a969.01300535031999137270128786964.jpg', '1', '1', '2020-07-19 17:49:21', 'U001', '管理员', '2020-07-19 17:49:49', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('32', 'GC2020071917494762', 'PI2020071917444993', 'PURCHASE_IN', '01300535031999137270128786964.jpg', '2020/07/19/5da14094-7e0a-4e3f-9e0b-2e470542a969.01300535031999137270128786964.jpg', '1', '1', '2020-07-19 17:49:21', 'U001', '管理员', '2020-07-19 17:50:36', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('33', 'GC2020071917499741', 'PI2020071917444993', 'PURCHASE_IN', '01300535031999137270128786964.jpg', '2020/07/19/5da14094-7e0a-4e3f-9e0b-2e470542a969.01300535031999137270128786964.jpg', '1', '1', '2020-07-19 17:49:49', 'U001', '管理员', '2020-07-19 17:50:45', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('34', 'GC2020071917490352', 'PI2020071917444993', 'PURCHASE_IN', '01300535031999137270128786964.jpg', '2020/07/19/5da14094-7e0a-4e3f-9e0b-2e470542a969.01300535031999137270128786964.jpg', '1', '1', '2020-07-19 17:49:49', 'U001', '管理员', '2020-07-19 17:50:45', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('35', 'GC2020071917501807', 'PI2020071917444993', 'PURCHASE_IN', '01300535031999137270128786964.jpg', '2020/07/19/5da14094-7e0a-4e3f-9e0b-2e470542a969.01300535031999137270128786964.jpg', '1', '1', '2020-07-19 17:50:45', 'U001', '管理员', '2020-07-19 17:51:08', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('36', 'GC2020071917505947', 'PI2020071917444993', 'PURCHASE_IN', '01300535031999137270128786964.jpg', '2020/07/19/5da14094-7e0a-4e3f-9e0b-2e470542a969.01300535031999137270128786964.jpg', '1', '1', '2020-07-19 17:50:45', 'U001', '管理员', '2020-07-19 17:51:08', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('37', 'GC2020071917517081', 'PI2020071917444993', 'PURCHASE_IN', '01300535031999137270128786964.jpg', '2020/07/19/5da14094-7e0a-4e3f-9e0b-2e470542a969.01300535031999137270128786964.jpg', '1', '0', '2020-07-19 17:51:08', 'U001', '管理员', '2020-07-19 17:51:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('38', 'GC2020072120068199', 'PI2020072120068932', 'PURCHASE_IN', '01300535031999137270128786964.jpg', '2020/07/21/7a9eb64d-8ff7-4192-8d5e-8cb42fe2cfc3.01300535031999137270128786964.jpg', '1', '0', '2020-07-21 20:06:37', 'U001', '管理员', '2020-07-21 20:06:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('39', 'GC2020072313399911', 'PI2020072313399209', 'PURCHASE_IN', '01300535031999137270128786964.jpg', '2020/07/23/bbec3e2b-06ca-4c3f-ad5a-8111058c8d32.01300535031999137270128786964.jpg', '1', '0', '2020-07-23 13:39:19', 'U001', '管理员', '2020-07-23 13:39:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('40', 'GC2020072715516800', 'GC2020072715327219', 'ADVANCE', '01300535031999137270128786964.jpg', '2020/07/27/1b7d796a-039f-499b-bea0-1c7baa0f398f.01300535031999137270128786964.jpg', '1', '1', '2020-07-27 15:51:51', 'U001', '管理员', '2020-07-27 15:58:22', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('41', 'GC2020072715587224', 'GC2020072715327219', 'ADVANCE', '01300535031999137270128786964.jpg', '2020/07/27/1b7d796a-039f-499b-bea0-1c7baa0f398f.01300535031999137270128786964.jpg', '1', '1', '2020-07-27 15:58:22', 'U001', '管理员', '2020-07-27 16:14:09', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('42', 'GC2020072716141647', 'GC2020072715327219', 'ADVANCE', '01300535031999137270128786964.jpg', '2020/07/27/1b7d796a-039f-499b-bea0-1c7baa0f398f.01300535031999137270128786964.jpg', '1', '1', '2020-07-27 16:14:09', 'U001', '管理员', '2020-07-31 19:57:21', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('43', 'GC2020072716346520', 'GC2020072716347586', 'ADVANCE', '01300535031999137270128786964.jpg', '2020/07/27/c8144dc1-6b95-4f54-bc06-d8bb025035ff.01300535031999137270128786964.jpg', '1', '1', '2020-07-27 16:34:39', 'U001', '管理员', '2020-07-31 19:49:12', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('44', 'GC2020072716357877', 'CG2020072716354431', 'PURCHASE', '01300535031999137270128786964.jpg', '2020/07/27/0fbcf8f7-0ce3-46d6-96e4-9186235bf726.01300535031999137270128786964.jpg', '1', '0', '2020-07-27 16:35:53', 'U001', '管理员', '2020-07-27 16:35:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('45', 'GC2020072716364374', 'PI2020072716363064', 'PURCHASE_IN', '01300535031999137270128786964.jpg', '2020/07/27/dfcb886a-c1b5-40bd-a52f-e729eb8c79ba.01300535031999137270128786964.jpg', '1', '0', '2020-07-27 16:36:20', 'U001', '管理员', '2020-07-27 16:36:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('46', 'GC2020073119493971', 'GC2020072716347586', 'ADVANCE', '01300535031999137270128786964.jpg', '2020/07/27/c8144dc1-6b95-4f54-bc06-d8bb025035ff.01300535031999137270128786964.jpg', '1', '1', '2020-07-31 19:49:12', 'U001', '管理员', '2020-07-31 19:57:13', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('47', 'GC2020073119575542', 'GC2020072716347586', 'ADVANCE', '01300535031999137270128786964.jpg', '2020/07/27/c8144dc1-6b95-4f54-bc06-d8bb025035ff.01300535031999137270128786964.jpg', '1', '1', '2020-07-31 19:57:13', 'U001', '管理员', '2020-07-31 19:57:54', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('48', 'GC2020073119579159', 'GC2020072715327219', 'ADVANCE', '01300535031999137270128786964.jpg', '2020/07/27/1b7d796a-039f-499b-bea0-1c7baa0f398f.01300535031999137270128786964.jpg', '1', '0', '2020-07-31 19:57:21', 'U001', '管理员', '2020-07-31 19:57:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('49', 'GC2020073119571174', 'GC2020072716347586', 'ADVANCE', '01300535031999137270128786964.jpg', '2020/07/27/c8144dc1-6b95-4f54-bc06-d8bb025035ff.01300535031999137270128786964.jpg', '1', '1', '2020-07-31 19:57:54', 'U001', '管理员', '2020-08-18 18:56:19', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_file` VALUES ('50', 'GC2020081818562687', 'GC2020072716347586', 'ADVANCE', '01300535031999137270128786964.jpg', '2020/07/27/c8144dc1-6b95-4f54-bc06-d8bb025035ff.01300535031999137270128786964.jpg', '1', '0', '2020-08-18 18:56:19', 'U001', '管理员', '2020-08-18 18:56:19', null, null, 'CP01', 'U001', 'CP01', null, '0');

-- ----------------------------
-- Table structure for sys_login
-- ----------------------------
DROP TABLE IF EXISTS `sys_login`;
CREATE TABLE `sys_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `login_name` varchar(50) DEFAULT NULL COMMENT '用户名称',
  `password` varchar(50) DEFAULT NULL COMMENT '手机号码',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `company_code` varchar(50) DEFAULT NULL COMMENT '当前登录公司',
  `session_key` varchar(50) DEFAULT NULL COMMENT '登录鉴权',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='用户登录信息表';

-- ----------------------------
-- Records of sys_login
-- ----------------------------
INSERT INTO `sys_login` VALUES ('1', 'U001', '13867135741', '21218CCA77804D2BA1922C33E0151105', '2020-09-12 20:41:31', 'CP01', 'd8c8e53634db4c64be9d42901da451a8', '1', '0', '2020-05-27 12:35:50', 'UC001', '管理员', '2020-09-12 20:41:31', 'U001', '管理员', 'CP01', 'UC001', 'CP01', null, '0');
INSERT INTO `sys_login` VALUES ('2', 'UC15907509667996591', '13800000001', '04fc711301f3c784d66955d98d399afb', null, null, null, '1', '0', '2020-05-29 19:16:07', 'U001', '管理员', '2020-05-29 19:16:07', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login` VALUES ('3', 'UC15907513805251726', '13800000002', '768c1c687efe184ae6dd2420710b8799', '2020-07-19 11:46:50', 'CO15907458659568425', 'e0660dcd1fd4468b8ef1bc2c86fc55f8', '1', '0', '2020-05-29 19:23:01', 'U001', '管理员', '2020-07-19 11:46:50', 'UC15907513805251726', '吴启迪', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login` VALUES ('4', 'UC15907517235671133', '13800000003', 'f7a5c99c58103f6b65c451efd0f81826', null, null, null, '1', '0', '2020-05-29 19:28:44', 'U001', '管理员', '2020-05-29 19:28:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login` VALUES ('5', 'UC15909249151008102', '13800000008', '7f7d5f9f3a660f2b09e3aae62a15e29b', '2020-07-20 08:46:29', 'CO15907458659434343', '4de493b101854bf1a4ce9e620debced0', '1', '0', '2020-05-31 19:35:16', 'U001', '管理员', '2020-07-20 08:46:28', 'UC15909249151008102', '沈括', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login` VALUES ('6', 'UC15909253368333363', '13800000009', '23ff17389acbfd020043268fb49e7048', '2020-07-11 12:53:06', 'CP01', 'fe658cd64e0d420b95feb243f1663257', '1', '0', '2020-05-31 19:42:17', 'U001', '管理员', '2020-07-11 12:53:05', 'UC15909253368333363', '胡金喜', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login` VALUES ('7', 'UC15909322708421901', '13800000011', '2959f1aea8db0f7fbba61f0f8474d0ef', null, null, null, '1', '0', '2020-05-31 21:37:51', 'U001', '管理员', '2020-05-31 21:37:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login` VALUES ('8', 'UC15914288383436686', '13811111101', 'a09fe216278f103e70a7a179e173831c', null, null, null, '1', '0', '2020-06-06 15:33:58', 'U001', '管理员', '2020-07-20 15:36:09', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login` VALUES ('9', 'UC15914292533829386', '12700000123', '3c6fcccf800b9652d2ac85de6c108c86', null, null, null, '1', '0', '2020-06-06 15:40:53', 'U001', '管理员', '2020-06-06 15:40:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login` VALUES ('10', 'UC15925759605520930', '13800000110', '21218cca77804d2ba1922c33e0151105', '2020-07-15 10:09:56', 'CO15907458659434343', '3969576c537740f4a0f29f363216e5d9', '1', '0', '2020-06-19 22:12:40', 'UC15909249151008102', '沈括', '2020-07-15 10:09:56', 'UC15925759605520930', '王浩强', 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login` VALUES ('11', 'UC15927410664004948', '17700000000', '670b14728ad9902aecba32e22fa4f6bd', '2020-09-14 14:08:29', 'CO15907202006218096', '58c0f7b5be5d4ba1b91c71799b26b5bd', '1', '0', '2020-06-21 20:04:26', 'U001', '管理员', '2020-09-14 14:08:28', 'UC15927410664004948', '默认测试管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login` VALUES ('12', 'UC15927411606850587', '17700000001', '04fc711301f3c784d66955d98d399afb', null, null, null, '1', '0', '2020-06-21 20:06:00', 'U001', '管理员', '2020-06-21 20:06:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login` VALUES ('13', 'UC2020070614320764', '18800000008', '7f7d5f9f3a660f2b09e3aae62a15e29b', null, null, null, '1', '0', '2020-07-06 14:32:52', 'UC15909249151008102', '沈括', '2020-07-06 14:32:52', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名称',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `device_ip` varchar(50) DEFAULT NULL COMMENT '设备IP',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=591 DEFAULT CHARSET=utf8 COMMENT='用户登录日志';

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------
INSERT INTO `sys_login_log` VALUES ('1', 'U001', '管理员', '2020-05-29 15:44:45', null, null, '0', '2020-05-29 15:44:45', 'U001', '管理员', '2020-05-29 15:44:45', null, null, null, 'U001', null, null, '0');
INSERT INTO `sys_login_log` VALUES ('2', 'U001', '管理员', '2020-05-29 15:47:11', null, null, '0', '2020-05-29 15:47:11', 'U001', '管理员', '2020-05-29 15:47:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('3', 'U001', '管理员', '2020-05-29 16:24:05', null, null, '0', '2020-05-29 16:24:05', 'U001', '管理员', '2020-05-29 16:24:05', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('4', 'U001', '管理员', '2020-05-29 16:33:25', null, null, '0', '2020-05-29 16:33:25', 'U001', '管理员', '2020-05-29 16:33:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('5', 'U001', '管理员', '2020-05-29 16:35:06', null, null, '0', '2020-05-29 16:35:06', 'U001', '管理员', '2020-05-29 16:35:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('6', 'U001', '管理员', '2020-05-29 16:36:46', null, null, '0', '2020-05-29 16:36:46', 'U001', '管理员', '2020-05-29 16:36:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('7', 'U001', '管理员', '2020-05-29 16:37:21', null, null, '0', '2020-05-29 16:37:21', 'U001', '管理员', '2020-05-29 16:37:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('8', 'U001', '管理员', '2020-05-29 16:37:45', null, null, '0', '2020-05-29 16:37:45', 'U001', '管理员', '2020-05-29 16:37:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('9', 'U001', '管理员', '2020-05-29 16:49:41', null, null, '0', '2020-05-29 16:49:41', 'U001', '管理员', '2020-05-29 16:49:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('10', 'U001', '管理员', '2020-05-29 19:15:28', null, '1', '0', '2020-05-29 19:15:28', 'U001', '管理员', '2020-05-29 19:15:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('11', 'U001', '管理员', '2020-05-29 19:30:45', null, '1', '0', '2020-05-29 19:30:45', 'U001', '管理员', '2020-05-29 19:30:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('12', 'U001', '管理员', '2020-05-30 11:56:08', null, '1', '0', '2020-05-30 11:56:09', 'U001', '管理员', '2020-05-30 11:56:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('13', 'U001', '管理员', '2020-05-30 11:59:20', null, '1', '0', '2020-05-30 11:59:21', 'U001', '管理员', '2020-05-30 11:59:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('14', 'U001', '管理员', '2020-05-30 12:07:40', null, '1', '0', '2020-05-30 12:07:41', 'U001', '管理员', '2020-05-30 12:07:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('15', 'U001', '管理员', '2020-05-30 12:22:58', null, '1', '0', '2020-05-30 12:22:58', 'U001', '管理员', '2020-05-30 12:22:58', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('16', 'U001', '管理员', '2020-05-30 12:43:17', null, '1', '0', '2020-05-30 12:43:17', 'U001', '管理员', '2020-05-30 12:43:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('17', 'U001', '管理员', '2020-05-30 12:47:48', null, '1', '0', '2020-05-30 12:47:49', 'U001', '管理员', '2020-05-30 12:47:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('18', 'U001', '管理员', '2020-05-30 12:56:02', null, '1', '0', '2020-05-30 12:56:02', 'U001', '管理员', '2020-05-30 12:56:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('19', 'U001', '管理员', '2020-05-30 12:57:26', null, '1', '0', '2020-05-30 12:57:27', 'U001', '管理员', '2020-05-30 12:57:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('20', 'U001', '管理员', '2020-05-30 12:58:06', null, '1', '0', '2020-05-30 12:58:06', 'U001', '管理员', '2020-05-30 12:58:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('21', 'U001', '管理员', '2020-05-30 13:00:33', null, '1', '0', '2020-05-30 13:00:34', 'U001', '管理员', '2020-05-30 13:00:34', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('22', 'U001', '管理员', '2020-05-30 13:21:06', null, '1', '0', '2020-05-30 13:21:07', 'U001', '管理员', '2020-05-30 13:21:07', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('23', 'U001', '管理员', '2020-05-30 14:11:58', null, '1', '0', '2020-05-30 14:11:59', 'U001', '管理员', '2020-05-30 14:11:59', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('24', 'U001', '管理员', '2020-05-30 14:45:19', null, '1', '0', '2020-05-30 14:45:20', 'U001', '管理员', '2020-05-30 14:45:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('25', 'U001', '管理员', '2020-05-30 15:23:41', null, '1', '0', '2020-05-30 15:23:42', 'U001', '管理员', '2020-05-30 15:23:42', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('26', 'U001', '管理员', '2020-05-30 18:13:25', null, '1', '0', '2020-05-30 18:13:27', 'U001', '管理员', '2020-05-30 18:13:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('27', 'U001', '管理员', '2020-05-30 19:37:24', null, '1', '0', '2020-05-30 19:37:26', 'U001', '管理员', '2020-05-30 19:37:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('28', 'U001', '管理员', '2020-05-30 21:39:45', null, '1', '0', '2020-05-30 21:39:45', 'U001', '管理员', '2020-05-30 21:39:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('29', 'U001', '管理员', '2020-05-31 08:36:40', null, '1', '0', '2020-05-31 08:36:40', 'U001', '管理员', '2020-05-31 08:36:40', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('30', 'U001', '管理员', '2020-05-31 13:59:04', null, '1', '0', '2020-05-31 13:59:04', 'U001', '管理员', '2020-05-31 13:59:04', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('31', 'U001', '管理员', '2020-05-31 16:07:25', null, '1', '0', '2020-05-31 16:07:25', 'U001', '管理员', '2020-05-31 16:07:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('32', 'U001', '管理员', '2020-05-31 16:49:25', null, '1', '0', '2020-05-31 16:49:25', 'U001', '管理员', '2020-05-31 16:49:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('33', 'U001', '管理员', '2020-05-31 19:04:38', null, '1', '0', '2020-05-31 19:04:39', 'U001', '管理员', '2020-05-31 19:04:39', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('34', 'U001', '管理员', '2020-05-31 21:05:24', null, '1', '0', '2020-05-31 21:05:25', 'U001', '管理员', '2020-05-31 21:05:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('35', 'U001', '管理员', '2020-06-04 13:56:01', null, '1', '0', '2020-06-04 13:56:00', 'U001', '管理员', '2020-06-04 13:56:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('36', 'U001', '管理员', '2020-06-04 15:56:30', null, '1', '0', '2020-06-04 15:56:29', 'U001', '管理员', '2020-06-04 15:56:29', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('37', 'U001', '管理员', '2020-06-05 17:16:45', null, '1', '0', '2020-06-05 17:16:45', 'U001', '管理员', '2020-06-05 17:16:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('38', 'U001', '管理员', '2020-06-06 11:04:28', null, '1', '0', '2020-06-06 11:04:29', 'U001', '管理员', '2020-06-06 11:04:29', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('39', 'U001', '管理员', '2020-06-06 13:30:48', null, '1', '0', '2020-06-06 13:30:49', 'U001', '管理员', '2020-06-06 13:30:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('40', 'U001', '管理员', '2020-06-06 14:05:51', null, '1', '0', '2020-06-06 14:05:52', 'U001', '管理员', '2020-06-06 14:05:52', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('41', 'U001', '管理员', '2020-06-06 16:08:05', null, '1', '0', '2020-06-06 16:08:05', 'U001', '管理员', '2020-06-06 16:08:05', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('42', 'U001', '管理员', '2020-06-06 18:21:29', null, '1', '0', '2020-06-06 18:21:28', 'U001', '管理员', '2020-06-06 18:21:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('43', 'U001', '管理员', '2020-06-07 10:11:55', null, '1', '0', '2020-06-07 10:11:55', 'U001', '管理员', '2020-06-07 10:11:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('44', 'U001', '管理员', '2020-06-07 15:30:28', null, '1', '0', '2020-06-07 15:30:28', 'U001', '管理员', '2020-06-07 15:30:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('45', 'U001', '管理员', '2020-06-07 16:25:46', null, '1', '0', '2020-06-07 16:25:47', 'U001', '管理员', '2020-06-07 16:25:47', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('46', 'U001', '管理员', '2020-06-07 20:07:38', null, '1', '0', '2020-06-07 20:07:38', 'U001', '管理员', '2020-06-07 20:07:38', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('47', 'U001', '管理员', '2020-06-08 16:40:22', null, '1', '0', '2020-06-08 16:40:23', 'U001', '管理员', '2020-06-08 16:40:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('48', 'U001', '管理员', '2020-06-08 18:40:49', null, '1', '0', '2020-06-08 18:40:50', 'U001', '管理员', '2020-06-08 18:40:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('49', 'U001', '管理员', '2020-06-09 08:10:16', null, '1', '0', '2020-06-09 08:10:16', 'U001', '管理员', '2020-06-09 08:10:16', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('50', 'U001', '管理员', '2020-06-09 10:58:15', null, '1', '0', '2020-06-09 10:58:15', 'U001', '管理员', '2020-06-09 10:58:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('51', 'U001', '管理员', '2020-06-09 17:07:07', null, '1', '0', '2020-06-09 17:07:07', 'U001', '管理员', '2020-06-09 17:07:07', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('52', 'U001', '管理员', '2020-06-09 19:07:29', null, '1', '0', '2020-06-09 19:07:28', 'U001', '管理员', '2020-06-09 19:07:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('53', 'U001', '管理员', '2020-06-09 19:23:53', null, '1', '0', '2020-06-09 19:23:53', 'U001', '管理员', '2020-06-09 19:23:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('54', 'U001', '管理员', '2020-06-12 19:51:58', null, '1', '0', '2020-06-12 19:52:00', 'U001', '管理员', '2020-06-12 19:52:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('55', 'U001', '管理员', '2020-06-12 22:16:17', null, '1', '0', '2020-06-12 22:16:19', 'U001', '管理员', '2020-06-12 22:16:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('56', 'U001', '管理员', '2020-06-13 00:16:56', null, '1', '0', '2020-06-13 00:16:58', 'U001', '管理员', '2020-06-13 00:16:58', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('57', 'U001', '管理员', '2020-06-13 08:32:19', null, '1', '0', '2020-06-13 08:32:20', 'U001', '管理员', '2020-06-13 08:32:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('58', 'U001', '管理员', '2020-06-13 10:35:33', null, '1', '0', '2020-06-13 10:35:35', 'U001', '管理员', '2020-06-13 10:35:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('59', 'U001', '管理员', '2020-06-13 12:36:00', null, '1', '0', '2020-06-13 12:36:02', 'U001', '管理员', '2020-06-13 12:36:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('60', 'U001', '管理员', '2020-06-13 14:38:05', null, '1', '0', '2020-06-13 14:38:06', 'U001', '管理员', '2020-06-13 14:38:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('61', 'U001', '管理员', '2020-06-13 16:38:19', null, '1', '0', '2020-06-13 16:38:21', 'U001', '管理员', '2020-06-13 16:38:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('62', 'U001', '管理员', '2020-06-13 18:38:38', null, '1', '0', '2020-06-13 18:38:38', 'U001', '管理员', '2020-06-13 18:38:38', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('63', 'U001', '管理员', '2020-06-13 19:30:15', null, '1', '0', '2020-06-13 19:30:15', 'U001', '管理员', '2020-06-13 19:30:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('64', 'U001', '管理员', '2020-06-13 21:31:13', null, '1', '0', '2020-06-13 21:31:13', 'U001', '管理员', '2020-06-13 21:31:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('65', 'U001', '管理员', '2020-06-14 08:57:09', null, '1', '0', '2020-06-14 08:57:10', 'U001', '管理员', '2020-06-14 08:57:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('66', 'U001', '管理员', '2020-06-14 11:00:15', null, '1', '0', '2020-06-14 11:00:15', 'U001', '管理员', '2020-06-14 11:00:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('67', 'U001', '管理员', '2020-06-14 13:01:34', null, '1', '0', '2020-06-14 13:01:34', 'U001', '管理员', '2020-06-14 13:01:34', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('68', 'U001', '管理员', '2020-06-14 15:01:52', null, '1', '0', '2020-06-14 15:01:52', 'U001', '管理员', '2020-06-14 15:01:52', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('69', 'U001', '管理员', '2020-06-14 17:02:57', null, '1', '0', '2020-06-14 17:02:57', 'U001', '管理员', '2020-06-14 17:02:57', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('70', 'U001', '管理员', '2020-06-14 19:07:03', null, '1', '0', '2020-06-14 19:07:04', 'U001', '管理员', '2020-06-14 19:07:04', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('71', 'U001', '管理员', '2020-06-14 21:08:09', null, '1', '0', '2020-06-14 21:08:09', 'U001', '管理员', '2020-06-14 21:08:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('72', 'U001', '管理员', '2020-06-15 10:48:54', null, '1', '0', '2020-06-15 10:48:55', 'U001', '管理员', '2020-06-15 10:48:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('73', 'U001', '管理员', '2020-06-15 13:32:09', null, '1', '0', '2020-06-15 13:32:09', 'U001', '管理员', '2020-06-15 13:32:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('74', 'U001', '管理员', '2020-06-15 13:32:09', null, '1', '0', '2020-06-15 13:32:09', 'U001', '管理员', '2020-06-15 13:32:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('75', 'U001', '管理员', '2020-06-15 13:33:43', null, '1', '0', '2020-06-15 13:33:43', 'U001', '管理员', '2020-06-15 13:33:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('76', 'U001', '管理员', '2020-06-15 20:19:43', null, '1', '0', '2020-06-15 20:19:43', 'U001', '管理员', '2020-06-15 20:19:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('77', 'U001', '管理员', '2020-06-15 22:22:27', null, '1', '0', '2020-06-15 22:22:28', 'U001', '管理员', '2020-06-15 22:22:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('78', 'U001', '管理员', '2020-06-16 20:57:13', null, '1', '0', '2020-06-16 20:57:13', 'U001', '管理员', '2020-06-16 20:57:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('79', 'U001', '管理员', '2020-06-16 21:17:59', null, '1', '0', '2020-06-16 21:17:59', 'U001', '管理员', '2020-06-16 21:17:59', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('80', 'UC15907513805251726', '测试1', '2020-06-16 22:19:08', null, '1', '0', '2020-06-16 22:19:08', 'UC15907513805251726', '测试1', '2020-06-16 22:19:08', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('81', 'U001', '管理员', '2020-06-16 22:19:40', null, '1', '0', '2020-06-16 22:19:39', 'U001', '管理员', '2020-06-16 22:19:39', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('82', 'UC15907513805251726', '测试1', '2020-06-16 22:25:04', null, '1', '0', '2020-06-16 22:25:03', 'UC15907513805251726', '测试1', '2020-06-16 22:25:03', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('83', 'U001', '管理员', '2020-06-16 22:25:25', null, '1', '0', '2020-06-16 22:25:24', 'U001', '管理员', '2020-06-16 22:25:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('84', 'UC15907513805251726', '测试1', '2020-06-16 22:26:34', null, '1', '0', '2020-06-16 22:26:33', 'UC15907513805251726', '测试1', '2020-06-16 22:26:33', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('85', 'U001', '管理员', '2020-06-16 22:35:04', null, '1', '0', '2020-06-16 22:35:04', 'U001', '管理员', '2020-06-16 22:35:04', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('86', 'U001', '管理员', '2020-06-16 22:52:55', null, '1', '0', '2020-06-16 22:52:55', 'U001', '管理员', '2020-06-16 22:52:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('87', 'U001', '管理员', '2020-06-17 10:51:41', null, '1', '0', '2020-06-17 10:51:40', 'U001', '管理员', '2020-06-17 10:51:40', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('88', 'U001', '管理员', '2020-06-17 15:13:20', null, '1', '0', '2020-06-17 15:13:21', 'U001', '管理员', '2020-06-17 15:13:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('89', 'U001', '管理员', '2020-06-17 16:10:51', null, '1', '0', '2020-06-17 16:10:51', 'U001', '管理员', '2020-06-17 16:10:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('90', 'U001', '管理员', '2020-06-17 17:15:39', null, '1', '0', '2020-06-17 17:15:39', 'U001', '管理员', '2020-06-17 17:15:39', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('91', 'UC15907513805251726', '吴启迪', '2020-06-17 17:48:42', null, '1', '0', '2020-06-17 17:48:43', 'UC15907513805251726', '吴启迪', '2020-06-17 17:48:43', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('92', 'U001', '管理员', '2020-06-17 18:09:59', null, '1', '0', '2020-06-17 18:09:59', 'U001', '管理员', '2020-06-17 18:09:59', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('93', 'U001', '管理员', '2020-06-17 21:14:58', null, '1', '0', '2020-06-17 21:14:57', 'U001', '管理员', '2020-06-17 21:14:57', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('94', 'U001', '管理员', '2020-06-17 21:15:34', null, '1', '0', '2020-06-17 21:15:35', 'U001', '管理员', '2020-06-17 21:15:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('95', 'U001', '管理员', '2020-06-17 23:19:24', null, '1', '0', '2020-06-17 23:19:25', 'U001', '管理员', '2020-06-17 23:19:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('96', 'U001', '管理员', '2020-06-18 10:46:32', null, '1', '0', '2020-06-18 10:46:32', 'U001', '管理员', '2020-06-18 10:46:32', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('97', 'U001', '管理员', '2020-06-18 10:48:25', null, '1', '0', '2020-06-18 10:48:25', 'U001', '管理员', '2020-06-18 10:48:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('98', 'U001', '管理员', '2020-06-18 10:57:51', null, '1', '0', '2020-06-18 10:57:51', 'U001', '管理员', '2020-06-18 10:57:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('99', 'U001', '管理员', '2020-06-18 14:26:04', null, '1', '0', '2020-06-18 14:26:04', 'U001', '管理员', '2020-06-18 14:26:04', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('100', 'U001', '管理员', '2020-06-18 16:44:09', null, '1', '0', '2020-06-18 16:44:08', 'U001', '管理员', '2020-06-18 16:44:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('101', 'U001', '管理员', '2020-06-18 16:54:39', null, '1', '0', '2020-06-18 16:54:40', 'U001', '管理员', '2020-06-18 16:54:40', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('102', 'U001', '管理员', '2020-06-18 17:47:40', null, '1', '0', '2020-06-18 17:47:40', 'U001', '管理员', '2020-06-18 17:47:40', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('103', 'U001', '管理员', '2020-06-18 18:09:54', null, '1', '0', '2020-06-18 18:09:54', 'U001', '管理员', '2020-06-18 18:09:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('104', 'U001', '管理员', '2020-06-18 18:25:33', null, '1', '0', '2020-06-18 18:25:33', 'U001', '管理员', '2020-06-18 18:25:33', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('105', 'U001', '管理员', '2020-06-18 20:26:12', null, '1', '0', '2020-06-18 20:26:12', 'U001', '管理员', '2020-06-18 20:26:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('106', 'UC15909249151008102', '沈括', '2020-06-18 20:27:19', null, '1', '0', '2020-06-18 20:27:18', 'UC15909249151008102', '沈括', '2020-06-18 20:27:18', null, null, 'CP01', 'UC15909249151008102', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('107', 'U001', '管理员', '2020-06-18 20:29:13', null, '1', '0', '2020-06-18 20:29:13', 'U001', '管理员', '2020-06-18 20:29:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('108', 'UC15909249151008102', '沈括', '2020-06-18 20:30:42', null, '1', '0', '2020-06-18 20:30:42', 'UC15909249151008102', '沈括', '2020-06-18 20:30:42', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('109', 'UC15909249151008102', '沈括', '2020-06-18 22:05:34', null, '1', '0', '2020-06-18 22:05:34', 'UC15909249151008102', '沈括', '2020-06-18 22:05:34', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('110', 'U001', '管理员', '2020-06-18 22:12:37', null, '1', '0', '2020-06-18 22:12:37', 'U001', '管理员', '2020-06-18 22:12:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('111', 'UC15909249151008102', '沈括', '2020-06-18 22:28:00', null, '1', '0', '2020-06-18 22:28:00', 'UC15909249151008102', '沈括', '2020-06-18 22:28:00', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('112', 'U001', '管理员', '2020-06-18 22:35:20', null, '1', '0', '2020-06-18 22:35:19', 'U001', '管理员', '2020-06-18 22:35:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('113', 'UC15909249151008102', '沈括', '2020-06-18 22:38:06', null, '1', '0', '2020-06-18 22:38:06', 'UC15909249151008102', '沈括', '2020-06-18 22:38:06', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('114', 'U001', '管理员', '2020-06-18 22:43:08', null, '1', '0', '2020-06-18 22:43:07', 'U001', '管理员', '2020-06-18 22:43:07', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('115', 'UC15909249151008102', '沈括', '2020-06-19 08:19:57', null, '1', '0', '2020-06-19 08:19:57', 'UC15909249151008102', '沈括', '2020-06-19 08:19:57', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('116', 'U001', '管理员', '2020-06-19 08:26:55', null, '1', '0', '2020-06-19 08:26:54', 'U001', '管理员', '2020-06-19 08:26:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('117', 'UC15909249151008102', '沈括', '2020-06-19 08:33:39', null, '1', '0', '2020-06-19 08:33:39', 'UC15909249151008102', '沈括', '2020-06-19 08:33:39', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('118', 'UC15909249151008102', '沈括', '2020-06-19 09:10:00', null, '1', '0', '2020-06-19 09:09:59', 'UC15909249151008102', '沈括', '2020-06-19 09:09:59', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('119', 'U001', '管理员', '2020-06-19 10:15:26', null, '1', '0', '2020-06-19 10:15:26', 'U001', '管理员', '2020-06-19 10:15:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('120', 'U001', '管理员', '2020-06-19 10:56:47', null, '1', '0', '2020-06-19 10:56:46', 'U001', '管理员', '2020-06-19 10:56:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('121', 'U001', '管理员', '2020-06-19 15:57:51', null, '1', '0', '2020-06-19 15:57:51', 'U001', '管理员', '2020-06-19 15:57:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('122', 'U001', '管理员', '2020-06-19 18:18:53', null, '1', '0', '2020-06-19 18:18:54', 'U001', '管理员', '2020-06-19 18:18:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('123', 'U001', '管理员', '2020-06-19 19:20:00', null, '1', '0', '2020-06-19 19:20:01', 'U001', '管理员', '2020-06-19 19:20:01', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('124', 'U001', '管理员', '2020-06-19 20:06:51', null, '1', '0', '2020-06-19 20:06:51', 'U001', '管理员', '2020-06-19 20:06:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('125', 'UC15909249151008102', '沈括', '2020-06-19 20:09:26', null, '1', '0', '2020-06-19 20:09:26', 'UC15909249151008102', '沈括', '2020-06-19 20:09:26', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('126', 'U001', '管理员', '2020-06-19 20:11:12', null, '1', '0', '2020-06-19 20:11:12', 'U001', '管理员', '2020-06-19 20:11:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('127', 'UC15909249151008102', '沈括', '2020-06-19 21:33:19', null, '1', '0', '2020-06-19 21:33:19', 'UC15909249151008102', '沈括', '2020-06-19 21:33:19', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('128', 'UC15925759605520930', '13800000110', '2020-06-19 22:57:19', null, '1', '0', '2020-06-19 22:57:18', 'UC15925759605520930', '13800000110', '2020-06-19 22:57:18', null, null, 'CO15907458659434343', 'UC15925759605520930', 'CO15907458659434343.202006193397', null, '0');
INSERT INTO `sys_login_log` VALUES ('129', 'UC15925759605520930', '13800000110', '2020-06-20 08:15:56', null, '1', '0', '2020-06-20 08:15:57', 'UC15925759605520930', '13800000110', '2020-06-20 08:15:57', null, null, 'CO15907458659434343', 'UC15925759605520930', 'CO15907458659434343.202006193397', null, '0');
INSERT INTO `sys_login_log` VALUES ('130', 'UC15909249151008102', '沈括', '2020-06-20 08:21:09', null, '1', '0', '2020-06-20 08:21:10', 'UC15909249151008102', '沈括', '2020-06-20 08:21:10', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('131', 'UC15925759605520930', '13800000110', '2020-06-20 08:22:16', null, '1', '0', '2020-06-20 08:22:16', 'UC15925759605520930', '13800000110', '2020-06-20 08:22:16', null, null, 'CO15907458659434343', 'UC15925759605520930', 'CO15907458659434343.202006193397', null, '0');
INSERT INTO `sys_login_log` VALUES ('132', 'UC15909249151008102', '沈括', '2020-06-20 08:23:01', null, '1', '0', '2020-06-20 08:23:02', 'UC15909249151008102', '沈括', '2020-06-20 08:23:02', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('133', 'U001', '管理员', '2020-06-20 14:23:45', null, '1', '0', '2020-06-20 14:23:45', 'U001', '管理员', '2020-06-20 14:23:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('134', 'UC15925759605520930', '13800000110', '2020-06-20 14:50:40', null, '1', '0', '2020-06-20 14:50:39', 'UC15925759605520930', '13800000110', '2020-06-20 14:50:39', null, null, 'CO15907458659434343', 'UC15925759605520930', 'CO15907458659434343.202006193397', null, '0');
INSERT INTO `sys_login_log` VALUES ('135', 'U001', '管理员', '2020-06-20 14:51:10', null, '1', '0', '2020-06-20 14:51:10', 'U001', '管理员', '2020-06-20 14:51:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('136', 'UC15909249151008102', '沈括', '2020-06-20 14:55:00', null, '1', '0', '2020-06-20 14:55:00', 'UC15909249151008102', '沈括', '2020-06-20 14:55:00', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('137', 'UC15909249151008102', '沈括', '2020-06-21 09:18:58', null, '1', '0', '2020-06-21 09:18:57', 'UC15909249151008102', '沈括', '2020-06-21 09:18:57', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('138', 'U001', '管理员', '2020-06-21 10:07:25', null, '1', '0', '2020-06-21 10:07:27', 'U001', '管理员', '2020-06-21 10:07:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('139', 'UC15909249151008102', '沈括', '2020-06-21 10:27:42', null, '1', '0', '2020-06-21 10:27:44', 'UC15909249151008102', '沈括', '2020-06-21 10:27:44', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('140', 'U001', '管理员', '2020-06-21 12:32:08', null, '1', '0', '2020-06-21 12:32:09', 'U001', '管理员', '2020-06-21 12:32:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('141', 'UC15909249151008102', '沈括', '2020-06-21 12:47:20', null, '1', '0', '2020-06-21 12:47:19', 'UC15909249151008102', '沈括', '2020-06-21 12:47:19', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('142', 'UC15909249151008102', '沈括', '2020-06-21 14:34:39', null, '1', '0', '2020-06-21 14:34:39', 'UC15909249151008102', '沈括', '2020-06-21 14:34:39', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('143', 'UC15909249151008102', '沈括', '2020-06-21 14:36:37', null, '1', '0', '2020-06-21 14:36:36', 'UC15909249151008102', '沈括', '2020-06-21 14:36:36', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('144', 'U001', '管理员', '2020-06-21 15:35:26', null, '1', '0', '2020-06-21 15:35:25', 'U001', '管理员', '2020-06-21 15:35:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('145', 'UC15909249151008102', '沈括', '2020-06-21 15:37:33', null, '1', '0', '2020-06-21 15:37:33', 'UC15909249151008102', '沈括', '2020-06-21 15:37:33', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('146', 'UC15909249151008102', '沈括', '2020-06-21 16:02:37', null, '1', '0', '2020-06-21 16:02:37', 'UC15909249151008102', '沈括', '2020-06-21 16:02:37', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('147', 'UC15909249151008102', '沈括', '2020-06-21 19:07:09', null, '1', '0', '2020-06-21 19:07:08', 'UC15909249151008102', '沈括', '2020-06-21 19:07:08', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('148', 'UC15909249151008102', '沈括', '2020-06-21 19:44:28', null, '1', '0', '2020-06-21 19:44:28', 'UC15909249151008102', '沈括', '2020-06-21 19:44:28', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('149', 'UC15909249151008102', '沈括', '2020-06-21 19:48:13', null, '1', '0', '2020-06-21 19:48:13', 'UC15909249151008102', '沈括', '2020-06-21 19:48:13', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('150', 'U001', '管理员', '2020-06-21 19:55:24', null, '1', '0', '2020-06-21 19:55:24', 'U001', '管理员', '2020-06-21 19:55:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('151', 'UC15909249151008102', '沈括', '2020-06-21 20:01:27', null, '1', '0', '2020-06-21 20:01:26', 'UC15909249151008102', '沈括', '2020-06-21 20:01:26', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('152', 'U001', '管理员', '2020-06-21 20:01:49', null, '1', '0', '2020-06-21 20:01:49', 'U001', '管理员', '2020-06-21 20:01:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('153', 'U001', '管理员', '2020-06-21 20:39:41', null, '1', '0', '2020-06-21 20:39:40', 'U001', '管理员', '2020-06-21 20:39:40', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('154', 'UC15909249151008102', '沈括', '2020-06-21 20:40:24', null, '1', '0', '2020-06-21 20:40:24', 'UC15909249151008102', '沈括', '2020-06-21 20:40:24', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('155', 'UC15927410664004948', '默认测试管理员', '2020-06-21 20:49:38', null, '1', '0', '2020-06-21 20:49:38', 'UC15927410664004948', '默认测试管理员', '2020-06-21 20:49:38', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('156', 'UC15909249151008102', '沈括', '2020-06-21 21:20:20', null, '1', '0', '2020-06-21 21:20:19', 'UC15909249151008102', '沈括', '2020-06-21 21:20:19', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('157', 'UC15927410664004948', '默认测试管理员', '2020-06-21 21:23:04', null, '1', '0', '2020-06-21 21:23:04', 'UC15927410664004948', '默认测试管理员', '2020-06-21 21:23:04', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('158', 'UC15927410664004948', '默认测试管理员', '2020-06-21 21:26:18', null, '1', '0', '2020-06-21 21:26:17', 'UC15927410664004948', '默认测试管理员', '2020-06-21 21:26:17', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('159', 'UC15927410664004948', '默认测试管理员', '2020-06-21 22:46:33', null, '1', '0', '2020-06-21 22:46:32', 'UC15927410664004948', '默认测试管理员', '2020-06-21 22:46:32', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('160', 'UC15927410664004948', '默认测试管理员', '2020-06-21 22:50:58', null, '1', '0', '2020-06-21 22:50:58', 'UC15927410664004948', '默认测试管理员', '2020-06-21 22:50:58', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('161', 'UC15927410664004948', '默认测试管理员', '2020-06-22 09:02:51', null, '1', '0', '2020-06-22 09:02:51', 'UC15927410664004948', '默认测试管理员', '2020-06-22 09:02:51', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('162', 'UC15927410664004948', '默认测试管理员', '2020-06-22 09:27:44', null, '1', '0', '2020-06-22 09:27:44', 'UC15927410664004948', '默认测试管理员', '2020-06-22 09:27:44', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('163', 'UC15927410664004948', '默认测试管理员', '2020-06-22 09:59:00', null, '1', '0', '2020-06-22 09:58:59', 'UC15927410664004948', '默认测试管理员', '2020-06-22 09:58:59', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('164', 'UC15927410664004948', '默认测试管理员', '2020-06-22 11:40:57', null, '1', '0', '2020-06-22 11:40:56', 'UC15927410664004948', '默认测试管理员', '2020-06-22 11:40:56', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('165', 'UC15927410664004948', '默认测试管理员', '2020-06-22 15:05:43', null, '1', '0', '2020-06-22 15:05:42', 'UC15927410664004948', '默认测试管理员', '2020-06-22 15:05:42', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('166', 'UC15927410664004948', '默认测试管理员', '2020-06-22 20:24:54', null, '1', '0', '2020-06-22 20:24:54', 'UC15927410664004948', '默认测试管理员', '2020-06-22 20:24:54', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('167', 'U001', '管理员', '2020-06-22 20:52:41', null, '1', '0', '2020-06-22 20:52:41', 'U001', '管理员', '2020-06-22 20:52:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('168', 'U001', '管理员', '2020-06-22 21:03:22', null, '1', '0', '2020-06-22 21:03:23', 'U001', '管理员', '2020-06-22 21:03:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('169', 'UC15909249151008102', '沈括', '2020-06-23 11:06:04', null, '1', '0', '2020-06-23 11:06:03', 'UC15909249151008102', '沈括', '2020-06-23 11:06:03', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('170', 'U001', '管理员', '2020-06-23 11:17:32', null, '1', '0', '2020-06-23 11:17:31', 'U001', '管理员', '2020-06-23 11:17:31', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('171', 'U001', '管理员', '2020-06-23 11:31:26', null, '1', '0', '2020-06-23 11:31:27', 'U001', '管理员', '2020-06-23 11:31:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('172', 'UC15927410664004948', '默认测试管理员', '2020-06-23 15:18:33', null, '1', '0', '2020-06-23 15:18:33', 'UC15927410664004948', '默认测试管理员', '2020-06-23 15:18:33', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('173', 'U001', '管理员', '2020-06-23 16:35:59', null, '1', '0', '2020-06-23 16:36:00', 'U001', '管理员', '2020-06-23 16:36:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('174', 'UC15927410664004948', '默认测试管理员', '2020-06-23 19:50:46', null, '1', '0', '2020-06-23 19:50:46', 'UC15927410664004948', '默认测试管理员', '2020-06-23 19:50:46', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('175', 'U001', '管理员', '2020-06-23 20:13:28', null, '1', '0', '2020-06-23 20:13:28', 'U001', '管理员', '2020-06-23 20:13:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('176', 'U001', '管理员', '2020-06-24 08:30:49', null, '1', '0', '2020-06-24 08:30:49', 'U001', '管理员', '2020-06-24 08:30:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('177', 'UC15927410664004948', '默认测试管理员', '2020-06-24 08:34:27', null, '1', '0', '2020-06-24 08:34:27', 'UC15927410664004948', '默认测试管理员', '2020-06-24 08:34:27', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('178', 'UC15927410664004948', '默认测试管理员', '2020-06-24 09:02:36', null, '1', '0', '2020-06-24 09:02:36', 'UC15927410664004948', '默认测试管理员', '2020-06-24 09:02:36', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('179', 'UC15927410664004948', '默认测试管理员', '2020-06-24 09:40:45', null, '1', '0', '2020-06-24 09:40:44', 'UC15927410664004948', '默认测试管理员', '2020-06-24 09:40:44', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('180', 'U001', '管理员', '2020-06-24 10:33:44', null, '1', '0', '2020-06-24 10:33:43', 'U001', '管理员', '2020-06-24 10:33:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('181', 'UC15909249151008102', '沈括', '2020-06-24 10:34:47', null, '1', '0', '2020-06-24 10:34:46', 'UC15909249151008102', '沈括', '2020-06-24 10:34:46', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('182', 'UC15909249151008102', '沈括', '2020-06-24 13:53:44', null, '1', '0', '2020-06-24 13:53:44', 'UC15909249151008102', '沈括', '2020-06-24 13:53:44', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('183', 'UC15927410664004948', '默认测试管理员', '2020-06-24 14:30:48', null, '1', '0', '2020-06-24 14:30:47', 'UC15927410664004948', '默认测试管理员', '2020-06-24 14:30:47', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('184', 'U001', '管理员', '2020-06-24 15:29:56', null, '1', '0', '2020-06-24 15:29:55', 'U001', '管理员', '2020-06-24 15:29:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('185', 'UC15909249151008102', '沈括', '2020-06-24 15:30:35', null, '1', '0', '2020-06-24 15:30:34', 'UC15909249151008102', '沈括', '2020-06-24 15:30:34', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('186', 'UC15909249151008102', '沈括', '2020-06-24 16:28:04', null, '1', '0', '2020-06-24 16:28:04', 'UC15909249151008102', '沈括', '2020-06-24 16:28:04', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('187', 'UC15909249151008102', '沈括', '2020-06-24 17:57:18', null, '1', '0', '2020-06-24 17:57:18', 'UC15909249151008102', '沈括', '2020-06-24 17:57:18', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('188', 'UC15909249151008102', '沈括', '2020-06-25 09:31:30', null, '1', '0', '2020-06-25 09:31:29', 'UC15909249151008102', '沈括', '2020-06-25 09:31:29', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('189', 'UC15909249151008102', '沈括', '2020-06-25 09:35:49', null, '1', '0', '2020-06-25 09:35:49', 'UC15909249151008102', '沈括', '2020-06-25 09:35:49', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('190', 'UC15909249151008102', '沈括', '2020-06-25 11:15:39', null, '1', '0', '2020-06-25 11:15:38', 'UC15909249151008102', '沈括', '2020-06-25 11:15:38', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('191', 'UC15927410664004948', '默认测试管理员', '2020-06-25 11:29:56', null, '1', '0', '2020-06-25 11:29:55', 'UC15927410664004948', '默认测试管理员', '2020-06-25 11:29:55', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('192', 'UC15909249151008102', '沈括', '2020-06-25 11:39:47', null, '1', '0', '2020-06-25 11:39:48', 'UC15909249151008102', '沈括', '2020-06-25 11:39:48', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('193', 'UC15909249151008102', '沈括', '2020-06-25 13:55:47', null, '1', '0', '2020-06-25 13:55:48', 'UC15909249151008102', '沈括', '2020-06-25 13:55:48', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('194', 'UC15909249151008102', '沈括', '2020-06-25 14:35:39', null, '1', '0', '2020-06-25 14:35:39', 'UC15909249151008102', '沈括', '2020-06-25 14:35:39', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('195', 'UC15909249151008102', '沈括', '2020-06-25 14:43:55', null, '1', '0', '2020-06-25 14:43:55', 'UC15909249151008102', '沈括', '2020-06-25 14:43:55', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('196', 'UC15909249151008102', '沈括', '2020-06-26 10:50:39', null, '1', '0', '2020-06-26 10:50:39', 'UC15909249151008102', '沈括', '2020-06-26 10:50:39', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('197', 'UC15909249151008102', '沈括', '2020-06-26 10:54:51', null, '1', '0', '2020-06-26 10:54:51', 'UC15909249151008102', '沈括', '2020-06-26 10:54:51', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('198', 'UC15909249151008102', '沈括', '2020-06-26 11:34:12', null, '1', '0', '2020-06-26 11:34:12', 'UC15909249151008102', '沈括', '2020-06-26 11:34:12', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('199', 'UC15909249151008102', '沈括', '2020-06-26 12:49:48', null, '1', '0', '2020-06-26 12:49:48', 'UC15909249151008102', '沈括', '2020-06-26 12:49:48', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('200', 'UC15909249151008102', '沈括', '2020-06-26 12:54:08', null, '1', '0', '2020-06-26 12:54:08', 'UC15909249151008102', '沈括', '2020-06-26 12:54:08', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('201', 'U001', '管理员', '2020-06-26 14:46:24', null, '1', '0', '2020-06-26 14:46:23', 'U001', '管理员', '2020-06-26 14:46:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('202', 'UC15909249151008102', '沈括', '2020-06-26 14:51:54', null, '1', '0', '2020-06-26 14:51:54', 'UC15909249151008102', '沈括', '2020-06-26 14:51:54', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('203', 'UC15909249151008102', '沈括', '2020-06-26 16:23:13', null, '1', '0', '2020-06-26 16:23:13', 'UC15909249151008102', '沈括', '2020-06-26 16:23:13', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('204', 'UC15909249151008102', '沈括', '2020-06-26 16:52:10', null, '1', '0', '2020-06-26 16:52:09', 'UC15909249151008102', '沈括', '2020-06-26 16:52:09', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('205', 'UC15927410664004948', '默认测试管理员', '2020-06-26 20:14:42', null, '1', '0', '2020-06-26 20:14:41', 'UC15927410664004948', '默认测试管理员', '2020-06-26 20:14:41', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('206', 'UC15927410664004948', '默认测试管理员', '2020-06-26 21:33:37', null, '1', '0', '2020-06-26 21:33:36', 'UC15927410664004948', '默认测试管理员', '2020-06-26 21:33:36', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('207', 'UC15909249151008102', '沈括', '2020-06-27 08:39:41', null, '1', '0', '2020-06-27 08:39:40', 'UC15909249151008102', '沈括', '2020-06-27 08:39:40', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('208', 'UC15909249151008102', '沈括', '2020-06-27 09:38:12', null, '1', '0', '2020-06-27 09:38:12', 'UC15909249151008102', '沈括', '2020-06-27 09:38:12', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('209', 'UC15927410664004948', '默认测试管理员', '2020-06-27 14:15:22', null, '1', '0', '2020-06-27 14:15:22', 'UC15927410664004948', '默认测试管理员', '2020-06-27 14:15:22', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('210', 'U001', '管理员', '2020-06-27 17:59:35', null, '1', '0', '2020-06-27 17:59:36', 'U001', '管理员', '2020-06-27 17:59:36', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('211', 'U001', '管理员', '2020-06-27 20:01:19', null, '1', '0', '2020-06-27 20:01:19', 'U001', '管理员', '2020-06-27 20:01:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('212', 'UC15927410664004948', '默认测试管理员', '2020-06-28 11:52:01', null, '1', '0', '2020-06-28 11:52:00', 'UC15927410664004948', '默认测试管理员', '2020-06-28 11:52:00', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('213', 'UC15927410664004948', '默认测试管理员', '2020-06-28 14:44:08', null, '1', '0', '2020-06-28 14:44:07', 'UC15927410664004948', '默认测试管理员', '2020-06-28 14:44:07', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('214', 'UC15909249151008102', '沈括', '2020-06-28 16:50:26', null, '1', '0', '2020-06-28 16:50:25', 'UC15909249151008102', '沈括', '2020-06-28 16:50:25', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('215', 'U001', '管理员', '2020-06-29 11:04:08', null, '1', '0', '2020-06-29 11:04:11', 'U001', '管理员', '2020-06-29 11:04:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('216', 'UC15927410664004948', '默认测试管理员', '2020-06-29 12:12:17', null, '1', '0', '2020-06-29 12:12:16', 'UC15927410664004948', '默认测试管理员', '2020-06-29 12:12:16', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('217', 'U001', '管理员', '2020-06-29 13:32:02', null, '1', '0', '2020-06-29 13:32:06', 'U001', '管理员', '2020-06-29 13:32:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('218', 'UC15927410664004948', '默认测试管理员', '2020-06-29 14:05:21', null, '1', '0', '2020-06-29 14:05:20', 'UC15927410664004948', '默认测试管理员', '2020-06-29 14:05:20', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('219', 'UC15927410664004948', '默认测试管理员', '2020-06-29 15:52:27', null, '1', '0', '2020-06-29 15:52:26', 'UC15927410664004948', '默认测试管理员', '2020-06-29 15:52:26', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('220', 'U001', '管理员', '2020-06-29 16:02:27', null, '1', '0', '2020-06-29 16:02:30', 'U001', '管理员', '2020-06-29 16:02:30', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('221', 'UC15927410664004948', '默认测试管理员', '2020-06-29 16:07:52', null, '1', '0', '2020-06-29 16:07:51', 'UC15927410664004948', '默认测试管理员', '2020-06-29 16:07:51', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('222', 'U001', '管理员', '2020-06-29 19:22:26', null, '1', '0', '2020-06-29 19:22:29', 'U001', '管理员', '2020-06-29 19:22:29', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('223', 'U001', '管理员', '2020-06-30 08:52:32', null, '1', '0', '2020-06-30 08:52:31', 'U001', '管理员', '2020-06-30 08:52:31', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('224', 'U001', '管理员', '2020-06-30 10:55:56', null, '1', '0', '2020-06-30 10:55:55', 'U001', '管理员', '2020-06-30 10:55:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('225', 'UC15927410664004948', '默认测试管理员', '2020-06-30 11:24:48', null, '1', '0', '2020-06-30 11:24:47', 'UC15927410664004948', '默认测试管理员', '2020-06-30 11:24:47', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('226', 'UC15927410664004948', '默认测试管理员', '2020-06-30 11:27:00', null, '1', '0', '2020-06-30 11:26:59', 'UC15927410664004948', '默认测试管理员', '2020-06-30 11:26:59', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('227', 'UC15927410664004948', '默认测试管理员', '2020-06-30 11:34:16', null, '1', '0', '2020-06-30 11:34:16', 'UC15927410664004948', '默认测试管理员', '2020-06-30 11:34:16', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('228', 'U001', '管理员', '2020-06-30 14:08:50', null, '1', '0', '2020-06-30 14:08:49', 'U001', '管理员', '2020-06-30 14:08:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('229', 'UC15927410664004948', '默认测试管理员', '2020-06-30 14:29:32', null, '1', '0', '2020-06-30 14:29:32', 'UC15927410664004948', '默认测试管理员', '2020-06-30 14:29:32', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('230', 'U001', '管理员', '2020-06-30 16:17:11', null, '1', '0', '2020-06-30 16:17:11', 'U001', '管理员', '2020-06-30 16:17:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('231', 'UC15927410664004948', '默认测试管理员', '2020-06-30 16:55:05', null, '1', '0', '2020-06-30 16:55:05', 'UC15927410664004948', '默认测试管理员', '2020-06-30 16:55:05', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('232', 'U001', '管理员', '2020-07-01 08:44:30', null, '1', '0', '2020-07-01 08:44:30', 'U001', '管理员', '2020-07-01 08:44:30', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('233', 'U001', '管理员', '2020-07-01 09:02:17', null, '1', '0', '2020-07-01 09:02:16', 'U001', '管理员', '2020-07-01 09:02:16', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('234', 'U001', '管理员', '2020-07-01 11:05:11', null, '1', '0', '2020-07-01 11:05:10', 'U001', '管理员', '2020-07-01 11:05:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('235', 'UC15927410664004948', '默认测试管理员', '2020-07-01 12:57:58', null, '1', '0', '2020-07-01 12:57:58', 'UC15927410664004948', '默认测试管理员', '2020-07-01 12:57:58', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('236', 'U001', '管理员', '2020-07-01 13:32:54', null, '1', '0', '2020-07-01 13:32:55', 'U001', '管理员', '2020-07-01 13:32:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('237', 'UC15927410664004948', '默认测试管理员', '2020-07-02 15:09:20', null, '1', '0', '2020-07-02 15:09:20', 'UC15927410664004948', '默认测试管理员', '2020-07-02 15:09:20', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('238', 'U001', '管理员', '2020-07-03 10:39:51', null, '1', '0', '2020-07-03 10:39:50', 'U001', '管理员', '2020-07-03 10:39:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('239', 'U001', '管理员', '2020-07-03 11:09:51', null, '1', '0', '2020-07-03 11:09:51', 'U001', '管理员', '2020-07-03 11:09:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('240', 'UC15927410664004948', '默认测试管理员', '2020-07-03 14:32:26', null, '1', '0', '2020-07-03 14:32:25', 'UC15927410664004948', '默认测试管理员', '2020-07-03 14:32:25', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('241', 'U001', '管理员', '2020-07-03 17:19:57', null, '1', '0', '2020-07-03 17:20:02', 'U001', '管理员', '2020-07-03 17:20:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('242', 'U001', '管理员', '2020-07-04 08:28:35', null, '1', '0', '2020-07-04 08:28:35', 'U001', '管理员', '2020-07-04 08:28:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('243', 'UC15909249151008102', '沈括', '2020-07-04 08:29:01', null, '1', '0', '2020-07-04 08:29:00', 'UC15909249151008102', '沈括', '2020-07-04 08:29:00', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('244', 'U001', '管理员', '2020-07-04 08:41:44', null, '1', '0', '2020-07-04 08:41:43', 'U001', '管理员', '2020-07-04 08:41:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('245', 'UC15909249151008102', '沈括', '2020-07-04 08:43:34', null, '1', '0', '2020-07-04 08:43:33', 'UC15909249151008102', '沈括', '2020-07-04 08:43:33', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('246', 'UC15909249151008102', '沈括', '2020-07-04 08:48:44', null, '1', '0', '2020-07-04 08:48:50', 'UC15909249151008102', '沈括', '2020-07-04 08:48:50', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('247', 'UC15909249151008102', '沈括', '2020-07-04 09:06:08', null, '1', '0', '2020-07-04 09:06:08', 'UC15909249151008102', '沈括', '2020-07-04 09:06:08', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('248', 'UC15909249151008102', '沈括', '2020-07-04 10:34:59', null, '1', '0', '2020-07-04 10:35:05', 'UC15909249151008102', '沈括', '2020-07-04 10:35:05', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('249', 'U001', '管理员', '2020-07-04 11:57:24', null, '1', '0', '2020-07-04 11:57:31', 'U001', '管理员', '2020-07-04 11:57:31', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('250', 'UC15909249151008102', '沈括', '2020-07-04 12:00:38', null, '1', '0', '2020-07-04 12:00:45', 'UC15909249151008102', '沈括', '2020-07-04 12:00:45', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('251', 'UC15909249151008102', '沈括', '2020-07-04 12:44:01', null, '1', '0', '2020-07-04 12:44:00', 'UC15909249151008102', '沈括', '2020-07-04 12:44:00', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('252', 'UC15927410664004948', '默认测试管理员', '2020-07-04 12:45:21', null, '1', '0', '2020-07-04 12:45:21', 'UC15927410664004948', '默认测试管理员', '2020-07-04 12:45:21', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('253', 'UC15909249151008102', '沈括', '2020-07-04 14:49:34', null, '1', '0', '2020-07-04 14:49:34', 'UC15909249151008102', '沈括', '2020-07-04 14:49:34', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('254', 'U001', '管理员', '2020-07-04 14:51:52', null, '1', '0', '2020-07-04 14:51:59', 'U001', '管理员', '2020-07-04 14:51:59', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('255', 'U001', '管理员', '2020-07-04 16:52:55', null, '1', '0', '2020-07-04 16:53:01', 'U001', '管理员', '2020-07-04 16:53:01', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('256', 'U001', '管理员', '2020-07-04 18:39:11', null, '1', '0', '2020-07-04 18:39:11', 'U001', '管理员', '2020-07-04 18:39:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('257', 'U001', '管理员', '2020-07-04 20:39:33', null, '1', '0', '2020-07-04 20:39:33', 'U001', '管理员', '2020-07-04 20:39:33', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('258', 'U001', '管理员', '2020-07-05 08:24:29', null, '1', '0', '2020-07-05 08:24:29', 'U001', '管理员', '2020-07-05 08:24:29', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('259', 'U001', '管理员', '2020-07-05 10:45:41', null, '1', '0', '2020-07-05 10:45:42', 'U001', '管理员', '2020-07-05 10:45:42', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('260', 'U001', '管理员', '2020-07-05 12:54:22', null, '1', '0', '2020-07-05 12:54:23', 'U001', '管理员', '2020-07-05 12:54:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('261', 'U001', '管理员', '2020-07-05 14:55:12', null, '1', '0', '2020-07-05 14:55:13', 'U001', '管理员', '2020-07-05 14:55:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('262', 'U001', '管理员', '2020-07-05 16:55:43', null, '1', '0', '2020-07-05 16:55:44', 'U001', '管理员', '2020-07-05 16:55:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('263', 'U001', '管理员', '2020-07-05 19:52:36', null, '1', '0', '2020-07-05 19:52:37', 'U001', '管理员', '2020-07-05 19:52:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('264', 'U001', '管理员', '2020-07-05 22:16:50', null, '1', '0', '2020-07-05 22:16:50', 'U001', '管理员', '2020-07-05 22:16:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('265', 'UC15909249151008102', '沈括', '2020-07-05 22:20:47', null, '1', '0', '2020-07-05 22:20:46', 'UC15909249151008102', '沈括', '2020-07-05 22:20:46', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('266', 'UC15909253368333363', '胡金喜', '2020-07-05 22:32:13', null, '1', '0', '2020-07-05 22:32:12', 'UC15909253368333363', '胡金喜', '2020-07-05 22:32:12', null, null, 'CP01', 'UC15909253368333363', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('267', 'UC15909253368333363', '胡金喜', '2020-07-05 23:00:51', null, '1', '0', '2020-07-05 23:00:51', 'UC15909253368333363', '胡金喜', '2020-07-05 23:00:51', null, null, 'CP01', 'UC15909253368333363', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('268', 'UC15927410664004948', '默认测试管理员', '2020-07-06 09:51:24', null, '1', '0', '2020-07-06 09:51:24', 'UC15927410664004948', '默认测试管理员', '2020-07-06 09:51:24', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('269', 'UC15927410664004948', '默认测试管理员', '2020-07-06 09:54:10', null, '1', '0', '2020-07-06 09:54:09', 'UC15927410664004948', '默认测试管理员', '2020-07-06 09:54:09', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('270', 'UC15909249151008102', '沈括', '2020-07-06 14:27:43', null, '1', '0', '2020-07-06 14:27:42', 'UC15909249151008102', '沈括', '2020-07-06 14:27:42', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('271', 'U001', '管理员', '2020-07-06 14:29:19', null, '1', '0', '2020-07-06 14:29:18', 'U001', '管理员', '2020-07-06 14:29:18', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('272', 'UC15909249151008102', '沈括', '2020-07-06 14:29:54', null, '1', '0', '2020-07-06 14:29:54', 'UC15909249151008102', '沈括', '2020-07-06 14:29:54', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('273', 'U001', '管理员', '2020-07-06 15:18:40', null, '1', '0', '2020-07-06 15:18:39', 'U001', '管理员', '2020-07-06 15:18:39', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('274', 'U001', '管理员', '2020-07-06 15:27:46', null, '1', '0', '2020-07-06 15:27:46', 'U001', '管理员', '2020-07-06 15:27:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('275', 'U001', '管理员', '2020-07-07 10:47:29', null, '1', '0', '2020-07-07 10:47:29', 'U001', '管理员', '2020-07-07 10:47:29', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('276', 'U001', '管理员', '2020-07-07 13:38:32', null, '1', '0', '2020-07-07 13:38:31', 'U001', '管理员', '2020-07-07 13:38:31', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('277', 'U001', '管理员', '2020-07-07 15:57:48', null, '1', '0', '2020-07-07 15:57:47', 'U001', '管理员', '2020-07-07 15:57:47', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('278', 'U001', '管理员', '2020-07-07 16:04:55', null, '1', '0', '2020-07-07 16:04:54', 'U001', '管理员', '2020-07-07 16:04:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('279', 'U001', '管理员', '2020-07-07 16:31:38', null, '1', '0', '2020-07-07 16:31:37', 'U001', '管理员', '2020-07-07 16:31:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('280', 'U001', '管理员', '2020-07-07 16:42:28', null, '1', '0', '2020-07-07 16:42:28', 'U001', '管理员', '2020-07-07 16:42:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('281', 'UC15909249151008102', '沈括', '2020-07-07 16:43:59', null, '1', '0', '2020-07-07 16:43:59', 'UC15909249151008102', '沈括', '2020-07-07 16:43:59', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('282', 'UC15927410664004948', '默认测试管理员', '2020-07-07 17:31:22', null, '1', '0', '2020-07-07 17:31:21', 'UC15927410664004948', '默认测试管理员', '2020-07-07 17:31:21', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('283', 'UC15927410664004948', '默认测试管理员', '2020-07-07 18:33:47', null, '1', '0', '2020-07-07 18:33:47', 'UC15927410664004948', '默认测试管理员', '2020-07-07 18:33:47', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('284', 'UC15909249151008102', '沈括', '2020-07-07 19:04:03', null, '1', '0', '2020-07-07 19:04:03', 'UC15909249151008102', '沈括', '2020-07-07 19:04:03', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('285', 'UC15909249151008102', '沈括', '2020-07-07 19:28:07', null, '1', '0', '2020-07-07 19:28:06', 'UC15909249151008102', '沈括', '2020-07-07 19:28:06', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('286', 'U001', '管理员', '2020-07-07 20:38:13', null, '1', '0', '2020-07-07 20:38:12', 'U001', '管理员', '2020-07-07 20:38:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('287', 'UC15909249151008102', '沈括', '2020-07-07 20:39:55', null, '1', '0', '2020-07-07 20:39:55', 'UC15909249151008102', '沈括', '2020-07-07 20:39:55', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('288', 'UC15909249151008102', '沈括', '2020-07-07 21:23:56', null, '1', '0', '2020-07-07 21:23:55', 'UC15909249151008102', '沈括', '2020-07-07 21:23:55', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('289', 'U001', '管理员', '2020-07-08 08:20:12', null, '1', '0', '2020-07-08 08:20:11', 'U001', '管理员', '2020-07-08 08:20:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('290', 'UC15909249151008102', '沈括', '2020-07-08 08:20:48', null, '1', '0', '2020-07-08 08:20:46', 'UC15909249151008102', '沈括', '2020-07-08 08:20:46', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('291', 'U001', '管理员', '2020-07-08 10:04:58', null, '1', '0', '2020-07-08 10:04:57', 'U001', '管理员', '2020-07-08 10:04:57', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('292', 'UC15909249151008102', '沈括', '2020-07-08 10:24:38', null, '1', '0', '2020-07-08 10:24:36', 'UC15909249151008102', '沈括', '2020-07-08 10:24:36', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('293', 'U001', '管理员', '2020-07-08 10:57:51', null, '1', '0', '2020-07-08 10:57:49', 'U001', '管理员', '2020-07-08 10:57:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('294', 'U001', '管理员', '2020-07-08 13:17:45', null, '1', '0', '2020-07-08 13:17:43', 'U001', '管理员', '2020-07-08 13:17:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('295', 'UC15909249151008102', '沈括', '2020-07-08 13:52:42', null, '1', '0', '2020-07-08 13:52:40', 'UC15909249151008102', '沈括', '2020-07-08 13:52:40', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('296', 'U001', '管理员', '2020-07-08 14:26:00', null, '1', '0', '2020-07-08 14:26:00', 'U001', '管理员', '2020-07-08 14:26:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('297', 'UC15909249151008102', '沈括', '2020-07-08 16:01:03', null, '1', '0', '2020-07-08 16:01:02', 'UC15909249151008102', '沈括', '2020-07-08 16:01:02', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('298', 'U001', '管理员', '2020-07-08 16:54:09', null, '1', '0', '2020-07-08 16:54:08', 'U001', '管理员', '2020-07-08 16:54:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('299', 'U001', '管理员', '2020-07-08 17:04:16', null, '1', '0', '2020-07-08 17:04:14', 'U001', '管理员', '2020-07-08 17:04:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('300', 'UC15909249151008102', '沈括', '2020-07-08 17:35:00', null, '1', '0', '2020-07-08 17:35:00', 'UC15909249151008102', '沈括', '2020-07-08 17:35:00', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('301', 'UC15909249151008102', '沈括', '2020-07-08 17:36:48', null, '1', '0', '2020-07-08 17:36:48', 'UC15909249151008102', '沈括', '2020-07-08 17:36:48', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('302', 'UC15927410664004948', '默认测试管理员', '2020-07-08 17:53:42', null, '1', '0', '2020-07-08 17:53:42', 'UC15927410664004948', '默认测试管理员', '2020-07-08 17:53:42', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('303', 'U001', '管理员', '2020-07-08 19:24:53', null, '1', '0', '2020-07-08 19:24:53', 'U001', '管理员', '2020-07-08 19:24:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('304', 'UC15909249151008102', '沈括', '2020-07-08 19:37:46', null, '1', '0', '2020-07-08 19:37:44', 'UC15909249151008102', '沈括', '2020-07-08 19:37:44', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('305', 'U001', '管理员', '2020-07-08 19:43:21', null, '1', '0', '2020-07-08 19:43:19', 'U001', '管理员', '2020-07-08 19:43:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('306', 'U001', '管理员', '2020-07-08 20:49:54', null, '1', '0', '2020-07-08 20:49:53', 'U001', '管理员', '2020-07-08 20:49:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('307', 'UC15909249151008102', '沈括', '2020-07-08 20:50:09', null, '1', '0', '2020-07-08 20:50:08', 'UC15909249151008102', '沈括', '2020-07-08 20:50:08', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('308', 'UC15909249151008102', '沈括', '2020-07-09 08:31:50', null, '1', '0', '2020-07-09 08:31:49', 'UC15909249151008102', '沈括', '2020-07-09 08:31:49', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('309', 'U001', '管理员', '2020-07-09 08:36:45', null, '1', '0', '2020-07-09 08:36:45', 'U001', '管理员', '2020-07-09 08:36:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('310', 'U001', '管理员', '2020-07-09 15:07:05', null, '1', '0', '2020-07-09 15:07:04', 'U001', '管理员', '2020-07-09 15:07:04', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('311', 'UC15909249151008102', '沈括', '2020-07-09 15:28:31', null, '1', '0', '2020-07-09 15:28:32', 'UC15909249151008102', '沈括', '2020-07-09 15:28:32', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('312', 'U001', '管理员', '2020-07-10 08:05:22', null, '1', '0', '2020-07-10 08:05:21', 'U001', '管理员', '2020-07-10 08:05:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('313', 'UC15909249151008102', '沈括', '2020-07-10 08:17:51', null, '1', '0', '2020-07-10 08:17:51', 'UC15909249151008102', '沈括', '2020-07-10 08:17:51', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('314', 'U001', '管理员', '2020-07-10 10:24:10', null, '1', '0', '2020-07-10 10:24:09', 'U001', '管理员', '2020-07-10 10:24:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('315', 'U001', '管理员', '2020-07-10 11:06:58', null, '1', '0', '2020-07-10 11:06:57', 'U001', '管理员', '2020-07-10 11:06:57', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('316', 'U001', '管理员', '2020-07-10 13:31:09', null, '1', '0', '2020-07-10 13:31:09', 'U001', '管理员', '2020-07-10 13:31:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('317', 'U001', '管理员', '2020-07-10 14:15:43', null, '1', '0', '2020-07-10 14:15:43', 'U001', '管理员', '2020-07-10 14:15:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('318', 'UC15909249151008102', '沈括', '2020-07-10 14:16:27', null, '1', '0', '2020-07-10 14:16:27', 'UC15909249151008102', '沈括', '2020-07-10 14:16:27', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('319', 'U001', '管理员', '2020-07-10 15:41:26', null, '1', '0', '2020-07-10 15:41:26', 'U001', '管理员', '2020-07-10 15:41:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('320', 'U001', '管理员', '2020-07-10 16:16:50', null, '1', '0', '2020-07-10 16:16:49', 'U001', '管理员', '2020-07-10 16:16:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('321', 'UC15927410664004948', '默认测试管理员', '2020-07-10 17:01:29', null, '1', '0', '2020-07-10 17:01:29', 'UC15927410664004948', '默认测试管理员', '2020-07-10 17:01:29', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('322', 'UC15927410664004948', '默认测试管理员', '2020-07-10 17:44:21', null, '1', '0', '2020-07-10 17:44:20', 'UC15927410664004948', '默认测试管理员', '2020-07-10 17:44:20', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('323', 'UC15927410664004948', '默认测试管理员', '2020-07-10 21:12:30', null, '1', '0', '2020-07-10 21:12:30', 'UC15927410664004948', '默认测试管理员', '2020-07-10 21:12:30', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('324', 'UC15909249151008102', '沈括', '2020-07-11 10:16:43', null, '1', '0', '2020-07-11 10:16:42', 'UC15909249151008102', '沈括', '2020-07-11 10:16:42', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('325', 'UC15909249151008102', '沈括', '2020-07-11 11:27:07', null, '1', '0', '2020-07-11 11:27:07', 'UC15909249151008102', '沈括', '2020-07-11 11:27:07', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('326', 'UC15909249151008102', '沈括', '2020-07-11 12:06:25', null, '1', '0', '2020-07-11 12:06:25', 'UC15909249151008102', '沈括', '2020-07-11 12:06:25', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('327', 'UC15909253368333363', '胡金喜', '2020-07-11 12:07:01', null, '1', '0', '2020-07-11 12:07:00', 'UC15909253368333363', '胡金喜', '2020-07-11 12:07:00', null, null, 'CP01', 'UC15909253368333363', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('328', 'UC15909249151008102', '沈括', '2020-07-11 12:14:19', null, '1', '0', '2020-07-11 12:14:18', 'UC15909249151008102', '沈括', '2020-07-11 12:14:18', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('329', 'UC15909253368333363', '胡金喜', '2020-07-11 12:53:06', null, '1', '0', '2020-07-11 12:53:05', 'UC15909253368333363', '胡金喜', '2020-07-11 12:53:05', null, null, 'CP01', 'UC15909253368333363', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('330', 'U001', '管理员', '2020-07-11 14:24:42', null, '1', '0', '2020-07-11 14:24:44', 'U001', '管理员', '2020-07-11 14:24:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('331', 'UC15927410664004948', '默认测试管理员', '2020-07-11 15:01:18', null, '1', '0', '2020-07-11 15:01:17', 'UC15927410664004948', '默认测试管理员', '2020-07-11 15:01:17', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('332', 'UC15927410664004948', '默认测试管理员', '2020-07-11 16:15:11', null, '1', '0', '2020-07-11 16:15:10', 'UC15927410664004948', '默认测试管理员', '2020-07-11 16:15:10', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('333', 'U001', '管理员', '2020-07-11 16:28:15', null, '1', '0', '2020-07-11 16:28:14', 'U001', '管理员', '2020-07-11 16:28:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('334', 'U001', '管理员', '2020-07-11 18:41:13', null, '1', '0', '2020-07-11 18:41:13', 'U001', '管理员', '2020-07-11 18:41:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('335', 'UC15927410664004948', '默认测试管理员', '2020-07-11 19:12:14', null, '1', '0', '2020-07-11 19:12:13', 'UC15927410664004948', '默认测试管理员', '2020-07-11 19:12:13', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('336', 'U001', '管理员', '2020-07-11 19:32:47', null, '1', '0', '2020-07-11 19:32:47', 'U001', '管理员', '2020-07-11 19:32:47', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('337', 'UC15909249151008102', '沈括', '2020-07-11 20:17:32', null, '1', '0', '2020-07-11 20:17:32', 'UC15909249151008102', '沈括', '2020-07-11 20:17:32', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('338', 'UC15909249151008102', '沈括', '2020-07-11 20:22:34', null, '1', '0', '2020-07-11 20:22:35', 'UC15909249151008102', '沈括', '2020-07-11 20:22:35', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('339', 'U001', '管理员', '2020-07-11 22:24:04', null, '1', '0', '2020-07-11 22:24:04', 'U001', '管理员', '2020-07-11 22:24:04', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('340', 'UC15909249151008102', '沈括', '2020-07-11 22:24:44', null, '1', '0', '2020-07-11 22:24:44', 'UC15909249151008102', '沈括', '2020-07-11 22:24:44', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('341', 'U001', '管理员', '2020-07-12 08:25:17', null, '1', '0', '2020-07-12 08:25:18', 'U001', '管理员', '2020-07-12 08:25:18', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('342', 'UC15909249151008102', '沈括', '2020-07-12 09:59:40', null, '1', '0', '2020-07-12 09:59:40', 'UC15909249151008102', '沈括', '2020-07-12 09:59:40', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('343', 'U001', '管理员', '2020-07-12 10:56:13', null, '1', '0', '2020-07-12 10:56:14', 'U001', '管理员', '2020-07-12 10:56:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('344', 'UC15909249151008102', '沈括', '2020-07-12 11:23:52', null, '1', '0', '2020-07-12 11:23:51', 'UC15909249151008102', '沈括', '2020-07-12 11:23:51', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('345', 'UC15909249151008102', '沈括', '2020-07-12 13:24:46', null, '1', '0', '2020-07-12 13:24:45', 'UC15909249151008102', '沈括', '2020-07-12 13:24:45', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('346', 'UC15909249151008102', '沈括', '2020-07-12 19:37:05', null, '1', '0', '2020-07-12 19:37:05', 'UC15909249151008102', '沈括', '2020-07-12 19:37:05', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('347', 'UC15909249151008102', '沈括', '2020-07-12 20:55:37', null, '1', '0', '2020-07-12 20:55:37', 'UC15909249151008102', '沈括', '2020-07-12 20:55:37', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('348', 'UC15909249151008102', '沈括', '2020-07-12 20:56:38', null, '1', '0', '2020-07-12 20:56:38', 'UC15909249151008102', '沈括', '2020-07-12 20:56:38', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('349', 'UC15909249151008102', '沈括', '2020-07-12 21:30:33', null, '1', '0', '2020-07-12 21:30:33', 'UC15909249151008102', '沈括', '2020-07-12 21:30:33', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('350', 'UC15909249151008102', '沈括', '2020-07-12 21:43:10', null, '1', '0', '2020-07-12 21:43:10', 'UC15909249151008102', '沈括', '2020-07-12 21:43:10', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('351', 'UC15927410664004948', '默认测试管理员', '2020-07-13 10:56:39', null, '1', '0', '2020-07-13 10:56:39', 'UC15927410664004948', '默认测试管理员', '2020-07-13 10:56:39', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('352', 'UC15927410664004948', '默认测试管理员', '2020-07-13 15:54:46', null, '1', '0', '2020-07-13 15:54:46', 'UC15927410664004948', '默认测试管理员', '2020-07-13 15:54:46', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('353', 'U001', '管理员', '2020-07-14 10:25:21', null, '1', '0', '2020-07-14 10:25:21', 'U001', '管理员', '2020-07-14 10:25:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('354', 'U001', '管理员', '2020-07-14 10:46:37', null, '1', '0', '2020-07-14 10:46:37', 'U001', '管理员', '2020-07-14 10:46:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('355', 'U001', '管理员', '2020-07-14 10:51:42', null, '1', '0', '2020-07-14 10:51:41', 'U001', '管理员', '2020-07-14 10:51:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('356', 'U001', '管理员', '2020-07-14 10:57:41', null, '1', '0', '2020-07-14 10:57:41', 'U001', '管理员', '2020-07-14 10:57:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('357', 'U001', '管理员', '2020-07-14 10:59:11', null, '1', '0', '2020-07-14 10:59:11', 'U001', '管理员', '2020-07-14 10:59:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('358', 'UC15927410664004948', '默认测试管理员', '2020-07-14 11:09:46', null, '1', '0', '2020-07-14 11:09:46', 'UC15927410664004948', '默认测试管理员', '2020-07-14 11:09:46', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('359', 'U001', '管理员', '2020-07-14 12:39:36', null, '1', '0', '2020-07-14 12:39:35', 'U001', '管理员', '2020-07-14 12:39:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('360', 'U001', '管理员', '2020-07-14 14:44:15', null, '1', '0', '2020-07-14 14:44:15', 'U001', '管理员', '2020-07-14 14:44:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('361', 'U001', '管理员', '2020-07-14 15:35:05', null, '1', '0', '2020-07-14 15:35:04', 'U001', '管理员', '2020-07-14 15:35:04', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('362', 'UC15909249151008102', '沈括', '2020-07-14 15:36:02', null, '1', '0', '2020-07-14 15:36:02', 'UC15909249151008102', '沈括', '2020-07-14 15:36:02', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('363', 'UC15927410664004948', '默认测试管理员', '2020-07-14 16:34:10', null, '1', '0', '2020-07-14 16:34:09', 'UC15927410664004948', '默认测试管理员', '2020-07-14 16:34:09', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('364', 'U001', '管理员', '2020-07-14 17:24:56', null, '1', '0', '2020-07-14 17:24:55', 'U001', '管理员', '2020-07-14 17:24:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('365', 'UC15909249151008102', '沈括', '2020-07-14 17:25:25', null, '1', '0', '2020-07-14 17:25:24', 'UC15909249151008102', '沈括', '2020-07-14 17:25:24', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('366', 'U001', '管理员', '2020-07-14 18:41:47', null, '1', '0', '2020-07-14 18:41:48', 'U001', '管理员', '2020-07-14 18:41:48', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('367', 'UC15909249151008102', '沈括', '2020-07-14 19:25:44', null, '1', '0', '2020-07-14 19:25:44', 'UC15909249151008102', '沈括', '2020-07-14 19:25:44', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('368', 'UC15927410664004948', '默认测试管理员', '2020-07-14 19:53:58', null, '1', '0', '2020-07-14 19:53:58', 'UC15927410664004948', '默认测试管理员', '2020-07-14 19:53:58', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('369', 'UC15927410664004948', '默认测试管理员', '2020-07-14 21:37:58', null, '1', '0', '2020-07-14 21:37:58', 'UC15927410664004948', '默认测试管理员', '2020-07-14 21:37:58', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('370', 'UC15927410664004948', '默认测试管理员', '2020-07-14 22:43:41', null, '1', '0', '2020-07-14 22:43:40', 'UC15927410664004948', '默认测试管理员', '2020-07-14 22:43:40', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('371', 'UC15909249151008102', '沈括', '2020-07-15 08:29:14', null, '1', '0', '2020-07-15 08:29:14', 'UC15909249151008102', '沈括', '2020-07-15 08:29:14', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('372', 'UC15925759605520930', '王浩强', '2020-07-15 10:04:28', null, '1', '0', '2020-07-15 10:04:28', 'UC15925759605520930', '王浩强', '2020-07-15 10:04:28', null, null, 'CO15907458659434343', 'UC15925759605520930', 'CO15907458659434343.202006193397', null, '0');
INSERT INTO `sys_login_log` VALUES ('373', 'UC15925759605520930', '王浩强', '2020-07-15 10:06:19', null, '1', '0', '2020-07-15 10:06:18', 'UC15925759605520930', '王浩强', '2020-07-15 10:06:18', null, null, 'CO15907458659434343', 'UC15925759605520930', 'CO15907458659434343.202006193397', null, '0');
INSERT INTO `sys_login_log` VALUES ('374', 'UC15925759605520930', '王浩强', '2020-07-15 10:09:56', null, '1', '0', '2020-07-15 10:09:56', 'UC15925759605520930', '王浩强', '2020-07-15 10:09:56', null, null, 'CO15907458659434343', 'UC15925759605520930', 'CO15907458659434343.202006193397', null, '0');
INSERT INTO `sys_login_log` VALUES ('375', 'U001', '管理员', '2020-07-15 10:14:16', null, '1', '0', '2020-07-15 10:14:16', 'U001', '管理员', '2020-07-15 10:14:16', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('376', 'UC15907513805251726', '吴启迪', '2020-07-15 10:15:34', null, '1', '0', '2020-07-15 10:15:33', 'UC15907513805251726', '吴启迪', '2020-07-15 10:15:33', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('377', 'UC15927410664004948', '默认测试管理员', '2020-07-15 15:03:21', null, '1', '0', '2020-07-15 15:03:21', 'UC15927410664004948', '默认测试管理员', '2020-07-15 15:03:21', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('378', 'UC15907513805251726', '吴启迪', '2020-07-15 15:04:44', null, '1', '0', '2020-07-15 15:04:44', 'UC15907513805251726', '吴启迪', '2020-07-15 15:04:44', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('379', 'U001', '管理员', '2020-07-15 15:51:31', null, '1', '0', '2020-07-15 15:51:31', 'U001', '管理员', '2020-07-15 15:51:31', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('380', 'UC15927410664004948', '默认测试管理员', '2020-07-15 15:53:12', null, '1', '0', '2020-07-15 15:53:12', 'UC15927410664004948', '默认测试管理员', '2020-07-15 15:53:12', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('381', 'UC15907513805251726', '吴启迪', '2020-07-15 15:56:05', null, '1', '0', '2020-07-15 15:56:04', 'UC15907513805251726', '吴启迪', '2020-07-15 15:56:04', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('382', 'U001', '管理员', '2020-07-15 16:06:00', null, '1', '0', '2020-07-15 16:06:00', 'U001', '管理员', '2020-07-15 16:06:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('383', 'U001', '管理员', '2020-07-15 17:33:52', null, '1', '0', '2020-07-15 17:33:51', 'U001', '管理员', '2020-07-15 17:33:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('384', 'UC15927410664004948', '默认测试管理员', '2020-07-15 17:54:03', null, '1', '0', '2020-07-15 17:54:03', 'UC15927410664004948', '默认测试管理员', '2020-07-15 17:54:03', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('385', 'UC15907513805251726', '吴启迪', '2020-07-15 19:16:11', null, '1', '0', '2020-07-15 19:16:11', 'UC15907513805251726', '吴启迪', '2020-07-15 19:16:11', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('386', 'U001', '管理员', '2020-07-15 19:28:52', null, '1', '0', '2020-07-15 19:28:51', 'U001', '管理员', '2020-07-15 19:28:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('387', 'UC15907513805251726', '吴启迪', '2020-07-15 19:30:00', null, '1', '0', '2020-07-15 19:30:00', 'UC15907513805251726', '吴启迪', '2020-07-15 19:30:00', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('388', 'UC15927410664004948', '默认测试管理员', '2020-07-15 22:46:22', null, '1', '0', '2020-07-15 22:46:22', 'UC15927410664004948', '默认测试管理员', '2020-07-15 22:46:22', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('389', 'UC15907513805251726', '吴启迪', '2020-07-16 08:06:25', null, '1', '0', '2020-07-16 08:06:24', 'UC15907513805251726', '吴启迪', '2020-07-16 08:06:24', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('390', 'UC15907513805251726', '吴启迪', '2020-07-16 08:16:42', null, '1', '0', '2020-07-16 08:16:42', 'UC15907513805251726', '吴启迪', '2020-07-16 08:16:42', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('391', 'UC15927410664004948', '默认测试管理员', '2020-07-16 18:27:24', null, '1', '0', '2020-07-16 18:27:23', 'UC15927410664004948', '默认测试管理员', '2020-07-16 18:27:23', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('392', 'UC15927410664004948', '默认测试管理员', '2020-07-17 10:41:22', null, '1', '0', '2020-07-17 10:41:22', 'UC15927410664004948', '默认测试管理员', '2020-07-17 10:41:22', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('393', 'UC15907513805251726', '吴启迪', '2020-07-17 11:06:50', null, '1', '0', '2020-07-17 11:06:49', 'UC15907513805251726', '吴启迪', '2020-07-17 11:06:49', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('394', 'UC15907513805251726', '吴启迪', '2020-07-17 12:15:23', null, '1', '0', '2020-07-17 12:15:24', 'UC15907513805251726', '吴启迪', '2020-07-17 12:15:24', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('395', 'U001', '管理员', '2020-07-17 14:26:29', null, '1', '0', '2020-07-17 14:26:29', 'U001', '管理员', '2020-07-17 14:26:29', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('396', 'U001', '管理员', '2020-07-17 14:37:53', null, '1', '0', '2020-07-17 14:37:53', 'U001', '管理员', '2020-07-17 14:37:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('397', 'UC15907513805251726', '吴启迪', '2020-07-17 15:39:33', null, '1', '0', '2020-07-17 15:39:33', 'UC15907513805251726', '吴启迪', '2020-07-17 15:39:33', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('398', 'U001', '管理员', '2020-07-17 16:52:07', null, '1', '0', '2020-07-17 16:52:07', 'U001', '管理员', '2020-07-17 16:52:07', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('399', 'UC15927410664004948', '默认测试管理员', '2020-07-17 17:28:55', null, '1', '0', '2020-07-17 17:28:55', 'UC15927410664004948', '默认测试管理员', '2020-07-17 17:28:55', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('400', 'UC15909249151008102', '沈括', '2020-07-17 17:36:39', null, '1', '0', '2020-07-17 17:36:38', 'UC15909249151008102', '沈括', '2020-07-17 17:36:38', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('401', 'U001', '管理员', '2020-07-17 19:21:14', null, '1', '0', '2020-07-17 19:21:15', 'U001', '管理员', '2020-07-17 19:21:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('402', 'U001', '管理员', '2020-07-17 19:47:51', null, '1', '0', '2020-07-17 19:47:51', 'U001', '管理员', '2020-07-17 19:47:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('403', 'U001', '管理员', '2020-07-17 21:30:53', null, '1', '0', '2020-07-17 21:30:53', 'U001', '管理员', '2020-07-17 21:30:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('404', 'U001', '管理员', '2020-07-17 23:08:17', null, '1', '0', '2020-07-17 23:08:18', 'U001', '管理员', '2020-07-17 23:08:18', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('405', 'UC15927410664004948', '默认测试管理员', '2020-07-17 23:19:32', null, '1', '0', '2020-07-17 23:19:31', 'UC15927410664004948', '默认测试管理员', '2020-07-17 23:19:31', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('406', 'UC15927410664004948', '默认测试管理员', '2020-07-18 17:48:22', null, '1', '0', '2020-07-18 17:48:21', 'UC15927410664004948', '默认测试管理员', '2020-07-18 17:48:21', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('407', 'U001', '管理员', '2020-07-19 11:37:03', null, '1', '0', '2020-07-19 11:37:03', 'U001', '管理员', '2020-07-19 11:37:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('408', 'U001', '管理员', '2020-07-19 11:38:02', null, '1', '0', '2020-07-19 11:38:02', 'U001', '管理员', '2020-07-19 11:38:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('409', 'UC15907513805251726', '吴启迪', '2020-07-19 11:46:50', null, '1', '0', '2020-07-19 11:46:50', 'UC15907513805251726', '吴启迪', '2020-07-19 11:46:50', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_login_log` VALUES ('410', 'U001', '管理员', '2020-07-19 14:47:01', null, '1', '0', '2020-07-19 14:47:00', 'U001', '管理员', '2020-07-19 14:47:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('411', 'U001', '管理员', '2020-07-19 16:29:53', null, '1', '0', '2020-07-19 16:29:54', 'U001', '管理员', '2020-07-19 16:29:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('412', 'U001', '管理员', '2020-07-20 08:30:22', null, '1', '0', '2020-07-20 08:30:22', 'U001', '管理员', '2020-07-20 08:30:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('413', 'UC15909249151008102', '沈括', '2020-07-20 08:46:29', null, '1', '0', '2020-07-20 08:46:28', 'UC15909249151008102', '沈括', '2020-07-20 08:46:28', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_login_log` VALUES ('414', 'U001', '管理员', '2020-07-20 08:51:18', null, '1', '0', '2020-07-20 08:51:18', 'U001', '管理员', '2020-07-20 08:51:18', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('415', 'U001', '管理员', '2020-07-20 10:53:02', null, '1', '0', '2020-07-20 10:53:02', 'U001', '管理员', '2020-07-20 10:53:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('416', 'UC15927410664004948', '默认测试管理员', '2020-07-20 11:17:21', null, '1', '0', '2020-07-20 11:17:21', 'UC15927410664004948', '默认测试管理员', '2020-07-20 11:17:21', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('417', 'U001', '管理员', '2020-07-20 13:06:40', null, '1', '0', '2020-07-20 13:06:40', 'U001', '管理员', '2020-07-20 13:06:40', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('418', 'UC15927410664004948', '默认测试管理员', '2020-07-20 14:07:03', null, '1', '0', '2020-07-20 14:07:03', 'UC15927410664004948', '默认测试管理员', '2020-07-20 14:07:03', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('419', 'UC15927410664004948', '默认测试管理员', '2020-07-20 14:16:21', null, '1', '0', '2020-07-20 14:16:20', 'UC15927410664004948', '默认测试管理员', '2020-07-20 14:16:20', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('420', 'UC15927410664004948', '默认测试管理员', '2020-07-20 14:37:09', null, '1', '0', '2020-07-20 14:37:09', 'UC15927410664004948', '默认测试管理员', '2020-07-20 14:37:09', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('421', 'U001', '管理员', '2020-07-20 15:07:13', null, '1', '0', '2020-07-20 15:07:14', 'U001', '管理员', '2020-07-20 15:07:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('422', 'U001', '管理员', '2020-07-20 15:56:53', null, '1', '0', '2020-07-20 15:56:53', 'U001', '管理员', '2020-07-20 15:56:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('423', 'U001', '管理员', '2020-07-20 16:01:46', null, '1', '0', '2020-07-20 16:01:46', 'U001', '管理员', '2020-07-20 16:01:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('424', 'U001', '管理员', '2020-07-20 16:02:15', null, '1', '0', '2020-07-20 16:02:15', 'U001', '管理员', '2020-07-20 16:02:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('425', 'U001', '管理员', '2020-07-20 18:45:27', null, '1', '0', '2020-07-20 18:45:26', 'U001', '管理员', '2020-07-20 18:45:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('426', 'U001', '管理员', '2020-07-20 19:02:05', null, '1', '0', '2020-07-20 19:02:05', 'U001', '管理员', '2020-07-20 19:02:05', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('427', 'U001', '管理员', '2020-07-21 08:20:09', null, '1', '0', '2020-07-21 08:20:09', 'U001', '管理员', '2020-07-21 08:20:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('428', 'U001', '管理员', '2020-07-21 09:16:10', null, '1', '0', '2020-07-21 09:16:12', 'U001', '管理员', '2020-07-21 09:16:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('429', 'U001', '管理员', '2020-07-21 13:11:23', null, '1', '0', '2020-07-21 13:11:22', 'U001', '管理员', '2020-07-21 13:11:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('430', 'U001', '管理员', '2020-07-21 14:27:10', null, '1', '0', '2020-07-21 14:27:11', 'U001', '管理员', '2020-07-21 14:27:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('431', 'U001', '管理员', '2020-07-21 18:52:32', null, '1', '0', '2020-07-21 18:52:31', 'U001', '管理员', '2020-07-21 18:52:31', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('432', 'U001', '管理员', '2020-07-21 18:55:15', null, '1', '0', '2020-07-21 18:55:18', 'U001', '管理员', '2020-07-21 18:55:18', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('433', 'U001', '管理员', '2020-07-21 20:05:04', null, '1', '0', '2020-07-21 20:05:07', 'U001', '管理员', '2020-07-21 20:05:07', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('434', 'U001', '管理员', '2020-07-21 21:56:37', null, '1', '0', '2020-07-21 21:56:37', 'U001', '管理员', '2020-07-21 21:56:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('435', 'U001', '管理员', '2020-07-22 08:38:54', null, '1', '0', '2020-07-22 08:38:54', 'U001', '管理员', '2020-07-22 08:38:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('436', 'U001', '管理员', '2020-07-22 09:08:59', null, '1', '0', '2020-07-22 09:08:58', 'U001', '管理员', '2020-07-22 09:08:58', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('437', 'U001', '管理员', '2020-07-22 10:43:05', null, '1', '0', '2020-07-22 10:43:05', 'U001', '管理员', '2020-07-22 10:43:05', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('438', 'U001', '管理员', '2020-07-22 13:19:10', null, '1', '0', '2020-07-22 13:19:10', 'U001', '管理员', '2020-07-22 13:19:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('439', 'U001', '管理员', '2020-07-22 15:22:10', null, '1', '0', '2020-07-22 15:22:11', 'U001', '管理员', '2020-07-22 15:22:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('440', 'UC15927410664004948', '默认测试管理员', '2020-07-22 15:55:24', null, '1', '0', '2020-07-22 15:55:23', 'UC15927410664004948', '默认测试管理员', '2020-07-22 15:55:23', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('441', 'U001', '管理员', '2020-07-22 16:40:51', null, '1', '0', '2020-07-22 16:40:50', 'U001', '管理员', '2020-07-22 16:40:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('442', 'U001', '管理员', '2020-07-22 19:18:42', null, '1', '0', '2020-07-22 19:18:41', 'U001', '管理员', '2020-07-22 19:18:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('443', 'U001', '管理员', '2020-07-22 19:33:28', null, '1', '0', '2020-07-22 19:33:29', 'U001', '管理员', '2020-07-22 19:33:29', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('444', 'U001', '管理员', '2020-07-23 08:26:17', null, '1', '0', '2020-07-23 08:26:17', 'U001', '管理员', '2020-07-23 08:26:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('445', 'U001', '管理员', '2020-07-23 09:35:11', null, '1', '0', '2020-07-23 09:35:12', 'U001', '管理员', '2020-07-23 09:35:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('446', 'U001', '管理员', '2020-07-23 11:22:02', null, '1', '0', '2020-07-23 11:22:02', 'U001', '管理员', '2020-07-23 11:22:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('447', 'U001', '管理员', '2020-07-23 13:29:25', null, '1', '0', '2020-07-23 13:29:25', 'U001', '管理员', '2020-07-23 13:29:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('448', 'U001', '管理员', '2020-07-23 15:35:55', null, '1', '0', '2020-07-23 15:35:54', 'U001', '管理员', '2020-07-23 15:35:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('449', 'U001', '管理员', '2020-07-23 16:53:30', null, '1', '0', '2020-07-23 16:53:30', 'U001', '管理员', '2020-07-23 16:53:30', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('450', 'U001', '管理员', '2020-07-23 17:36:16', null, '1', '0', '2020-07-23 17:36:15', 'U001', '管理员', '2020-07-23 17:36:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('451', 'U001', '管理员', '2020-07-23 17:36:25', null, '1', '0', '2020-07-23 17:36:24', 'U001', '管理员', '2020-07-23 17:36:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('452', 'U001', '管理员', '2020-07-24 09:52:42', null, '1', '0', '2020-07-24 09:52:41', 'U001', '管理员', '2020-07-24 09:52:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('453', 'U001', '管理员', '2020-07-24 13:33:32', null, '1', '0', '2020-07-24 13:33:31', 'U001', '管理员', '2020-07-24 13:33:31', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('454', 'U001', '管理员', '2020-07-24 14:16:59', null, '1', '0', '2020-07-24 14:17:00', 'U001', '管理员', '2020-07-24 14:17:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('455', 'UC15927410664004948', '默认测试管理员', '2020-07-24 14:32:16', null, '1', '0', '2020-07-24 14:32:16', 'UC15927410664004948', '默认测试管理员', '2020-07-24 14:32:16', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('456', 'UC15927410664004948', '默认测试管理员', '2020-07-24 14:35:46', null, '1', '0', '2020-07-24 14:35:45', 'UC15927410664004948', '默认测试管理员', '2020-07-24 14:35:45', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('457', 'U001', '管理员', '2020-07-24 16:26:15', null, '1', '0', '2020-07-24 16:26:15', 'U001', '管理员', '2020-07-24 16:26:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('458', 'U001', '管理员', '2020-07-24 16:47:52', null, '1', '0', '2020-07-24 16:47:52', 'U001', '管理员', '2020-07-24 16:47:52', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('459', 'U001', '管理员', '2020-07-24 17:14:34', null, '1', '0', '2020-07-24 17:14:35', 'U001', '管理员', '2020-07-24 17:14:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('460', 'UC15927410664004948', '默认测试管理员', '2020-07-24 17:37:36', null, '1', '0', '2020-07-24 17:37:36', 'UC15927410664004948', '默认测试管理员', '2020-07-24 17:37:36', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('461', 'U001', '管理员', '2020-07-24 19:33:48', null, '1', '0', '2020-07-24 19:33:49', 'U001', '管理员', '2020-07-24 19:33:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('462', 'U001', '管理员', '2020-07-25 10:05:08', null, '1', '0', '2020-07-25 10:05:08', 'U001', '管理员', '2020-07-25 10:05:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('463', 'U001', '管理员', '2020-07-25 10:27:12', null, '1', '0', '2020-07-25 10:27:13', 'U001', '管理员', '2020-07-25 10:27:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('464', 'U001', '管理员', '2020-07-25 13:22:19', null, '1', '0', '2020-07-25 13:22:21', 'U001', '管理员', '2020-07-25 13:22:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('465', 'U001', '管理员', '2020-07-25 13:24:36', null, '1', '0', '2020-07-25 13:24:36', 'U001', '管理员', '2020-07-25 13:24:36', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('466', 'U001', '管理员', '2020-07-25 13:49:53', null, '1', '0', '2020-07-25 13:49:53', 'U001', '管理员', '2020-07-25 13:49:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('467', 'U001', '管理员', '2020-07-25 16:47:21', null, '1', '0', '2020-07-25 16:47:24', 'U001', '管理员', '2020-07-25 16:47:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('468', 'U001', '管理员', '2020-07-25 19:29:46', null, '1', '0', '2020-07-25 19:29:46', 'U001', '管理员', '2020-07-25 19:29:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('469', 'U001', '管理员', '2020-07-27 14:44:38', null, '1', '0', '2020-07-27 14:44:39', 'U001', '管理员', '2020-07-27 14:44:39', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('470', 'U001', '管理员', '2020-07-27 14:55:41', null, '1', '0', '2020-07-27 14:55:41', 'U001', '管理员', '2020-07-27 14:55:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('471', 'U001', '管理员', '2020-07-27 16:55:56', null, '1', '0', '2020-07-27 16:55:57', 'U001', '管理员', '2020-07-27 16:55:57', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('472', 'U001', '管理员', '2020-07-28 10:13:32', null, '1', '0', '2020-07-28 10:13:34', 'U001', '管理员', '2020-07-28 10:13:34', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('473', 'U001', '管理员', '2020-07-28 14:55:08', null, '1', '0', '2020-07-28 14:55:10', 'U001', '管理员', '2020-07-28 14:55:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('474', 'U001', '管理员', '2020-07-28 16:23:08', null, '1', '0', '2020-07-28 16:23:10', 'U001', '管理员', '2020-07-28 16:23:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('475', 'U001', '管理员', '2020-07-28 17:27:21', null, '1', '0', '2020-07-28 17:27:20', 'U001', '管理员', '2020-07-28 17:27:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('476', 'U001', '管理员', '2020-07-28 18:23:24', null, '1', '0', '2020-07-28 18:23:25', 'U001', '管理员', '2020-07-28 18:23:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('477', 'UC15927410664004948', '默认测试管理员', '2020-07-28 18:45:41', null, '1', '0', '2020-07-28 18:45:40', 'UC15927410664004948', '默认测试管理员', '2020-07-28 18:45:40', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('478', 'U001', '管理员', '2020-07-29 08:33:32', null, '1', '0', '2020-07-29 08:33:35', 'U001', '管理员', '2020-07-29 08:33:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('479', 'U001', '管理员', '2020-07-29 11:00:32', null, '1', '0', '2020-07-29 11:00:35', 'U001', '管理员', '2020-07-29 11:00:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('480', 'U001', '管理员', '2020-07-29 15:21:33', null, '1', '0', '2020-07-29 15:21:35', 'U001', '管理员', '2020-07-29 15:21:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('481', 'U001', '管理员', '2020-07-29 18:37:53', null, '1', '0', '2020-07-29 18:37:53', 'U001', '管理员', '2020-07-29 18:37:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('482', 'U001', '管理员', '2020-07-29 18:40:02', null, '1', '0', '2020-07-29 18:40:05', 'U001', '管理员', '2020-07-29 18:40:05', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('483', 'UC15927410664004948', '默认测试管理员', '2020-07-29 23:25:14', null, '1', '0', '2020-07-29 23:25:13', 'UC15927410664004948', '默认测试管理员', '2020-07-29 23:25:13', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('484', 'U001', '管理员', '2020-07-30 08:15:11', null, '1', '0', '2020-07-30 08:15:10', 'U001', '管理员', '2020-07-30 08:15:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('485', 'U001', '管理员', '2020-07-30 09:16:24', null, '1', '0', '2020-07-30 09:16:28', 'U001', '管理员', '2020-07-30 09:16:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('486', 'UC15927410664004948', '默认测试管理员', '2020-07-30 09:58:28', null, '1', '0', '2020-07-30 09:58:28', 'UC15927410664004948', '默认测试管理员', '2020-07-30 09:58:28', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('487', 'UC15927410664004948', '默认测试管理员', '2020-07-30 10:11:29', null, '1', '0', '2020-07-30 10:11:28', 'UC15927410664004948', '默认测试管理员', '2020-07-30 10:11:28', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('488', 'U001', '管理员', '2020-07-30 11:19:22', null, '1', '0', '2020-07-30 11:19:26', 'U001', '管理员', '2020-07-30 11:19:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('489', 'U001', '管理员', '2020-07-30 13:37:59', null, '1', '0', '2020-07-30 13:37:59', 'U001', '管理员', '2020-07-30 13:37:59', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('490', 'U001', '管理员', '2020-07-30 15:49:33', null, '1', '0', '2020-07-30 15:49:33', 'U001', '管理员', '2020-07-30 15:49:33', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('491', 'U001', '管理员', '2020-07-31 08:56:48', null, '1', '0', '2020-07-31 08:56:47', 'U001', '管理员', '2020-07-31 08:56:47', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('492', 'U001', '管理员', '2020-07-31 09:00:07', null, '1', '0', '2020-07-31 09:00:06', 'U001', '管理员', '2020-07-31 09:00:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('493', 'UC15927410664004948', '默认测试管理员', '2020-07-31 12:25:43', null, '1', '0', '2020-07-31 12:25:42', 'UC15927410664004948', '默认测试管理员', '2020-07-31 12:25:42', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('494', 'UC15927410664004948', '默认测试管理员', '2020-07-31 12:33:02', null, '1', '0', '2020-07-31 12:33:02', 'UC15927410664004948', '默认测试管理员', '2020-07-31 12:33:02', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('495', 'U001', '管理员', '2020-07-31 14:14:16', null, '1', '0', '2020-07-31 14:14:15', 'U001', '管理员', '2020-07-31 14:14:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('496', 'U001', '管理员', '2020-07-31 14:15:54', null, '1', '0', '2020-07-31 14:15:53', 'U001', '管理员', '2020-07-31 14:15:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('497', 'U001', '管理员', '2020-07-31 16:16:42', null, '1', '0', '2020-07-31 16:16:41', 'U001', '管理员', '2020-07-31 16:16:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('498', 'U001', '管理员', '2020-07-31 19:45:21', null, '1', '0', '2020-07-31 19:45:20', 'U001', '管理员', '2020-07-31 19:45:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('499', 'U001', '管理员', '2020-08-01 09:58:07', null, '1', '0', '2020-08-01 09:58:06', 'U001', '管理员', '2020-08-01 09:58:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('500', 'U001', '管理员', '2020-08-01 11:42:14', null, '1', '0', '2020-08-01 11:42:13', 'U001', '管理员', '2020-08-01 11:42:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('501', 'U001', '管理员', '2020-08-01 12:30:54', null, '1', '0', '2020-08-01 12:30:53', 'U001', '管理员', '2020-08-01 12:30:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('502', 'U001', '管理员', '2020-08-01 14:23:36', null, '1', '0', '2020-08-01 14:23:35', 'U001', '管理员', '2020-08-01 14:23:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('503', 'U001', '管理员', '2020-08-01 16:46:04', null, '1', '0', '2020-08-01 16:46:03', 'U001', '管理员', '2020-08-01 16:46:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('504', 'U001', '管理员', '2020-08-01 20:32:53', null, '1', '0', '2020-08-01 20:32:54', 'U001', '管理员', '2020-08-01 20:32:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('505', 'U001', '管理员', '2020-08-01 20:57:20', null, '1', '0', '2020-08-01 20:57:20', 'U001', '管理员', '2020-08-01 20:57:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('506', 'UC15927410664004948', '默认测试管理员', '2020-08-01 22:08:54', null, '1', '0', '2020-08-01 22:08:54', 'UC15927410664004948', '默认测试管理员', '2020-08-01 22:08:54', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('507', 'UC15927410664004948', '默认测试管理员', '2020-08-02 08:18:54', null, '1', '0', '2020-08-02 08:18:53', 'UC15927410664004948', '默认测试管理员', '2020-08-02 08:18:53', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('508', 'U001', '管理员', '2020-08-02 12:53:05', null, '1', '0', '2020-08-02 12:53:05', 'U001', '管理员', '2020-08-02 12:53:05', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('509', 'UC15927410664004948', '默认测试管理员', '2020-08-02 14:42:07', null, '1', '0', '2020-08-02 14:42:07', 'UC15927410664004948', '默认测试管理员', '2020-08-02 14:42:07', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('510', 'U001', '管理员', '2020-08-02 14:57:12', null, '1', '0', '2020-08-02 14:57:12', 'U001', '管理员', '2020-08-02 14:57:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('511', 'U001', '管理员', '2020-08-02 15:19:56', null, '1', '0', '2020-08-02 15:19:57', 'U001', '管理员', '2020-08-02 15:19:57', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('512', 'U001', '管理员', '2020-08-02 19:19:48', null, '1', '0', '2020-08-02 19:19:49', 'U001', '管理员', '2020-08-02 19:19:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('513', 'U001', '管理员', '2020-08-03 13:22:46', null, '1', '0', '2020-08-03 13:22:45', 'U001', '管理员', '2020-08-03 13:22:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('514', 'U001', '管理员', '2020-08-03 15:57:54', null, '1', '0', '2020-08-03 15:57:53', 'U001', '管理员', '2020-08-03 15:57:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('515', 'U001', '管理员', '2020-08-03 19:45:09', null, '1', '0', '2020-08-03 19:45:08', 'U001', '管理员', '2020-08-03 19:45:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('516', 'U001', '管理员', '2020-08-04 08:04:12', null, '1', '0', '2020-08-04 08:04:12', 'U001', '管理员', '2020-08-04 08:04:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('517', 'U001', '管理员', '2020-08-04 08:34:17', null, '1', '0', '2020-08-04 08:34:16', 'U001', '管理员', '2020-08-04 08:34:16', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('518', 'U001', '管理员', '2020-08-04 11:18:18', null, '1', '0', '2020-08-04 11:18:18', 'U001', '管理员', '2020-08-04 11:18:18', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('519', 'U001', '管理员', '2020-08-04 13:28:10', null, '1', '0', '2020-08-04 13:28:10', 'U001', '管理员', '2020-08-04 13:28:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('520', 'U001', '管理员', '2020-08-04 15:28:46', null, '1', '0', '2020-08-04 15:28:45', 'U001', '管理员', '2020-08-04 15:28:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('521', 'UC15927410664004948', '默认测试管理员', '2020-08-04 17:18:42', null, '1', '0', '2020-08-04 17:18:41', 'UC15927410664004948', '默认测试管理员', '2020-08-04 17:18:41', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('522', 'U001', '管理员', '2020-08-04 18:15:38', null, '1', '0', '2020-08-04 18:15:37', 'U001', '管理员', '2020-08-04 18:15:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('523', 'UC15927410664004948', '默认测试管理员', '2020-08-05 07:15:53', null, '1', '0', '2020-08-05 07:15:53', 'UC15927410664004948', '默认测试管理员', '2020-08-05 07:15:53', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('524', 'UC15927410664004948', '默认测试管理员', '2020-08-05 13:14:34', null, '1', '0', '2020-08-05 13:14:33', 'UC15927410664004948', '默认测试管理员', '2020-08-05 13:14:33', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('525', 'U001', '管理员', '2020-08-05 15:33:57', null, '1', '0', '2020-08-05 15:33:57', 'U001', '管理员', '2020-08-05 15:33:57', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('526', 'U001', '管理员', '2020-08-05 18:25:20', null, '1', '0', '2020-08-05 18:25:19', 'U001', '管理员', '2020-08-05 18:25:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('527', 'U001', '管理员', '2020-08-05 18:58:21', null, '1', '0', '2020-08-05 18:58:21', 'U001', '管理员', '2020-08-05 18:58:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('528', 'UC15927410664004948', '默认测试管理员', '2020-08-06 10:51:41', null, '1', '0', '2020-08-06 10:51:40', 'UC15927410664004948', '默认测试管理员', '2020-08-06 10:51:40', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('529', 'UC15927410664004948', '默认测试管理员', '2020-08-06 11:10:54', null, '1', '0', '2020-08-06 11:10:54', 'UC15927410664004948', '默认测试管理员', '2020-08-06 11:10:54', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('530', 'UC15927410664004948', '默认测试管理员', '2020-08-07 13:52:21', null, '1', '0', '2020-08-07 13:52:21', 'UC15927410664004948', '默认测试管理员', '2020-08-07 13:52:21', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('531', 'UC15927410664004948', '默认测试管理员', '2020-08-07 15:20:21', null, '1', '0', '2020-08-07 15:20:20', 'UC15927410664004948', '默认测试管理员', '2020-08-07 15:20:20', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('532', 'U001', '管理员', '2020-08-08 07:54:01', null, '1', '0', '2020-08-08 07:54:03', 'U001', '管理员', '2020-08-08 07:54:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('533', 'U001', '管理员', '2020-08-08 08:49:05', null, '1', '0', '2020-08-08 08:49:06', 'U001', '管理员', '2020-08-08 08:49:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('534', 'UC15927410664004948', '默认测试管理员', '2020-08-08 12:45:18', null, '1', '0', '2020-08-08 12:45:17', 'UC15927410664004948', '默认测试管理员', '2020-08-08 12:45:17', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('535', 'U001', '管理员', '2020-08-09 16:35:11', null, '1', '0', '2020-08-09 16:35:14', 'U001', '管理员', '2020-08-09 16:35:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('536', 'UC15927410664004948', '默认测试管理员', '2020-08-11 12:04:17', null, '1', '0', '2020-08-11 12:04:16', 'UC15927410664004948', '默认测试管理员', '2020-08-11 12:04:16', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('537', 'UC15927410664004948', '默认测试管理员', '2020-08-13 15:24:19', null, '1', '0', '2020-08-13 15:24:19', 'UC15927410664004948', '默认测试管理员', '2020-08-13 15:24:19', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('538', 'UC15927410664004948', '默认测试管理员', '2020-08-13 16:52:30', null, '1', '0', '2020-08-13 16:52:29', 'UC15927410664004948', '默认测试管理员', '2020-08-13 16:52:29', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('539', 'UC15927410664004948', '默认测试管理员', '2020-08-14 15:27:10', null, '1', '0', '2020-08-14 15:27:10', 'UC15927410664004948', '默认测试管理员', '2020-08-14 15:27:10', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('540', 'U001', '管理员', '2020-08-14 16:00:51', null, '1', '0', '2020-08-14 16:00:51', 'U001', '管理员', '2020-08-14 16:00:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('541', 'UC15927410664004948', '默认测试管理员', '2020-08-15 11:12:50', null, '1', '0', '2020-08-15 11:12:49', 'UC15927410664004948', '默认测试管理员', '2020-08-15 11:12:49', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('542', 'UC15927410664004948', '默认测试管理员', '2020-08-15 11:41:01', null, '1', '0', '2020-08-15 11:41:00', 'UC15927410664004948', '默认测试管理员', '2020-08-15 11:41:00', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('543', 'UC15927410664004948', '默认测试管理员', '2020-08-17 15:59:28', null, '1', '0', '2020-08-17 15:59:27', 'UC15927410664004948', '默认测试管理员', '2020-08-17 15:59:27', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('544', 'U001', '管理员', '2020-08-17 18:07:58', null, '1', '0', '2020-08-17 18:07:57', 'U001', '管理员', '2020-08-17 18:07:57', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('545', 'U001', '管理员', '2020-08-18 16:13:49', null, '1', '0', '2020-08-18 16:13:49', 'U001', '管理员', '2020-08-18 16:13:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('546', 'U001', '管理员', '2020-08-18 18:42:10', null, '1', '0', '2020-08-18 18:42:11', 'U001', '管理员', '2020-08-18 18:42:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('547', 'U001', '管理员', '2020-08-19 18:49:01', null, '1', '0', '2020-08-19 18:49:00', 'U001', '管理员', '2020-08-19 18:49:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('548', 'U001', '管理员', '2020-08-19 19:16:21', null, '1', '0', '2020-08-19 19:16:20', 'U001', '管理员', '2020-08-19 19:16:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('549', 'U001', '管理员', '2020-08-20 15:11:45', null, '1', '0', '2020-08-20 15:11:44', 'U001', '管理员', '2020-08-20 15:11:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('550', 'UC15927410664004948', '默认测试管理员', '2020-08-20 17:03:42', null, '1', '0', '2020-08-20 17:03:41', 'UC15927410664004948', '默认测试管理员', '2020-08-20 17:03:41', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('551', 'U001', '管理员', '2020-08-20 17:12:02', null, '1', '0', '2020-08-20 17:12:02', 'U001', '管理员', '2020-08-20 17:12:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('552', 'U001', '管理员', '2020-08-20 19:34:16', null, '1', '0', '2020-08-20 19:34:17', 'U001', '管理员', '2020-08-20 19:34:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('553', 'U001', '管理员', '2020-08-21 10:59:37', null, '1', '0', '2020-08-21 10:59:37', 'U001', '管理员', '2020-08-21 10:59:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('554', 'U001', '管理员', '2020-08-21 11:03:50', null, '1', '0', '2020-08-21 11:03:51', 'U001', '管理员', '2020-08-21 11:03:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('555', 'U001', '管理员', '2020-08-21 15:01:29', null, '1', '0', '2020-08-21 15:01:29', 'U001', '管理员', '2020-08-21 15:01:29', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('556', 'UC15927410664004948', '默认测试管理员', '2020-08-22 10:24:58', null, '1', '0', '2020-08-22 10:24:57', 'UC15927410664004948', '默认测试管理员', '2020-08-22 10:24:57', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('557', 'U001', '管理员', '2020-08-22 11:01:42', null, '1', '0', '2020-08-22 11:01:41', 'U001', '管理员', '2020-08-22 11:01:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('558', 'U001', '管理员', '2020-08-22 11:05:36', null, '1', '0', '2020-08-22 11:05:36', 'U001', '管理员', '2020-08-22 11:05:36', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('559', 'U001', '管理员', '2020-08-22 14:27:06', null, '1', '0', '2020-08-22 14:27:06', 'U001', '管理员', '2020-08-22 14:27:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('560', 'U001', '管理员', '2020-08-22 16:23:38', null, '1', '0', '2020-08-22 16:23:37', 'U001', '管理员', '2020-08-22 16:23:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('561', 'U001', '管理员', '2020-08-22 16:27:28', null, '1', '0', '2020-08-22 16:27:28', 'U001', '管理员', '2020-08-22 16:27:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('562', 'U001', '管理员', '2020-08-22 16:33:27', null, '1', '0', '2020-08-22 16:33:27', 'U001', '管理员', '2020-08-22 16:33:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('563', 'U001', '管理员', '2020-08-22 18:30:10', null, '1', '0', '2020-08-22 18:30:10', 'U001', '管理员', '2020-08-22 18:30:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('564', 'U001', '管理员', '2020-08-22 21:03:27', null, '1', '0', '2020-08-22 21:03:27', 'U001', '管理员', '2020-08-22 21:03:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('565', 'U001', '管理员', '2020-08-23 20:46:42', null, '1', '0', '2020-08-23 20:46:41', 'U001', '管理员', '2020-08-23 20:46:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('566', 'U001', '管理员', '2020-08-24 14:50:00', null, '1', '0', '2020-08-24 14:49:59', 'U001', '管理员', '2020-08-24 14:49:59', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('567', 'U001', '管理员', '2020-08-24 15:08:04', null, '1', '0', '2020-08-24 15:08:05', 'U001', '管理员', '2020-08-24 15:08:05', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('568', 'U001', '管理员', '2020-08-24 15:59:19', null, '1', '0', '2020-08-24 15:59:20', 'U001', '管理员', '2020-08-24 15:59:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('569', 'U001', '管理员', '2020-08-25 17:09:41', null, '1', '0', '2020-08-25 17:09:40', 'U001', '管理员', '2020-08-25 17:09:40', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('570', 'U001', '管理员', '2020-08-25 17:13:09', null, '1', '0', '2020-08-25 17:13:09', 'U001', '管理员', '2020-08-25 17:13:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('571', 'U001', '管理员', '2020-08-26 14:52:50', null, '1', '0', '2020-08-26 14:52:49', 'U001', '管理员', '2020-08-26 14:52:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('572', 'UC15927410664004948', '默认测试管理员', '2020-08-26 21:24:47', null, '1', '0', '2020-08-26 21:24:46', 'UC15927410664004948', '默认测试管理员', '2020-08-26 21:24:46', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('573', 'U001', '管理员', '2020-09-05 18:47:14', null, '1', '0', '2020-09-05 18:47:16', 'U001', '管理员', '2020-09-05 18:47:16', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('574', 'U001', '管理员', '2020-09-05 20:48:14', null, '1', '0', '2020-09-05 20:48:15', 'U001', '管理员', '2020-09-05 20:48:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('575', 'U001', '管理员', '2020-09-05 21:16:49', null, '1', '0', '2020-09-05 21:16:50', 'U001', '管理员', '2020-09-05 21:16:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('576', 'U001', '管理员', '2020-09-06 15:50:51', null, '1', '0', '2020-09-06 15:50:53', 'U001', '管理员', '2020-09-06 15:50:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('577', 'U001', '管理员', '2020-09-07 17:20:07', null, '1', '0', '2020-09-07 17:20:06', 'U001', '管理员', '2020-09-07 17:20:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('578', 'U001', '管理员', '2020-09-07 19:36:00', null, '1', '0', '2020-09-07 19:36:00', 'U001', '管理员', '2020-09-07 19:36:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('579', 'U001', '管理员', '2020-09-08 08:22:01', null, '1', '0', '2020-09-08 08:22:01', 'U001', '管理员', '2020-09-08 08:22:01', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('580', 'U001', '管理员', '2020-09-08 10:24:05', null, '1', '0', '2020-09-08 10:24:05', 'U001', '管理员', '2020-09-08 10:24:05', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('581', 'U001', '管理员', '2020-09-08 14:50:02', null, '1', '0', '2020-09-08 14:50:02', 'U001', '管理员', '2020-09-08 14:50:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('582', 'UC15927410664004948', '默认测试管理员', '2020-09-10 22:33:18', null, '1', '0', '2020-09-10 22:33:17', 'UC15927410664004948', '默认测试管理员', '2020-09-10 22:33:17', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('583', 'U001', '管理员', '2020-09-12 13:44:51', null, '1', '0', '2020-09-12 13:44:51', 'U001', '管理员', '2020-09-12 13:44:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('584', 'U001', '管理员', '2020-09-12 19:05:20', null, '1', '0', '2020-09-12 19:05:20', 'U001', '管理员', '2020-09-12 19:05:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('585', 'U001', '管理员', '2020-09-12 20:34:11', null, '1', '0', '2020-09-12 20:34:11', 'U001', '管理员', '2020-09-12 20:34:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('586', 'U001', '管理员', '2020-09-12 20:41:31', null, '1', '0', '2020-09-12 20:41:31', 'U001', '管理员', '2020-09-12 20:41:31', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_login_log` VALUES ('587', 'UC15927410664004948', '默认测试管理员', '2020-09-12 23:14:19', null, '1', '0', '2020-09-12 23:14:18', 'UC15927410664004948', '默认测试管理员', '2020-09-12 23:14:18', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('588', 'UC15927410664004948', '默认测试管理员', '2020-09-14 11:40:22', null, '1', '0', '2020-09-14 11:40:22', 'UC15927410664004948', '默认测试管理员', '2020-09-14 11:40:22', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('589', 'UC15927410664004948', '默认测试管理员', '2020-09-14 13:10:04', null, '1', '0', '2020-09-14 13:10:04', 'UC15927410664004948', '默认测试管理员', '2020-09-14 13:10:04', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_login_log` VALUES ('590', 'UC15927410664004948', '默认测试管理员', '2020-09-14 14:08:29', null, '1', '0', '2020-09-14 14:08:28', 'UC15927410664004948', '默认测试管理员', '2020-09-14 14:08:28', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_code` varchar(50) DEFAULT NULL COMMENT '菜单编码',
  `menu_name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `menu_type` varchar(50) DEFAULT NULL COMMENT '菜单类型 菜单   按钮，功能',
  `parent_menu_code` varchar(50) DEFAULT '0' COMMENT '上级菜单',
  `menu_url` varchar(500) DEFAULT NULL COMMENT '菜单路径',
  `auth_url` varchar(50) DEFAULT NULL COMMENT '权限路径',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标样式',
  `permission` varchar(50) DEFAULT NULL COMMENT '权限码',
  `component` varchar(255) DEFAULT NULL COMMENT '组件',
  `open_type` varchar(50) DEFAULT NULL COMMENT '打开方式',
  `app_code` varchar(50) DEFAULT NULL COMMENT '所属应用',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=618 DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', 'MC15907524736113995', '应用管理', 'MENU', 'MC9999', '/admin/app', null, 'el-icon-picture-outline-round', null, 'views/admin/app/index', null, 'APP15906275620845638', '1', '0', '2020-05-29 19:41:14', 'U001', '管理员', '2020-07-10 17:26:09', null, null, 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('3', 'MC6', '应用新增', 'BTN', 'MC15907524736113995', null, null, null, 'app_add', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 13:48:18', 'U001', '管理员', '2020-05-30 13:51:25', null, null, 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('4', 'MC7', '应用编辑', 'BTN', 'MC15907524736113995', null, null, null, 'app_edit', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 13:48:19', 'U001', '管理员', '2020-05-30 13:51:25', null, null, 'CP01', 'U001', 'CP01', '2', '0');
INSERT INTO `sys_menu` VALUES ('5', 'MC8', '应用查看', 'BTN', 'MC15907524736113995', null, null, null, 'app_view', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 13:48:20', 'U001', '管理员', '2020-05-30 13:51:25', null, null, 'CP01', 'U001', 'CP01', '3', '0');
INSERT INTO `sys_menu` VALUES ('6', 'MC9', '应用删除', 'BTN', 'MC15907524736113995', null, null, null, 'app_delete', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 13:48:20', 'U001', '管理员', '2020-05-30 13:51:25', null, null, 'CP01', 'U001', 'CP01', '4', '0');
INSERT INTO `sys_menu` VALUES ('7', 'MC10', '应用菜单', 'FUNCTION', 'MC15907524736113995', '/admin/app/menu', null, null, 'app_menu', 'views/admin/app/menu/index', null, 'APP15906275620845638', '1', '0', '2020-05-30 13:48:21', 'U001', '管理员', '2020-05-31 15:15:18', null, null, 'CP01', 'U001', 'CP01', '7', '0');
INSERT INTO `sys_menu` VALUES ('8', 'MC11', '应用角色', 'FUNCTION', 'MC15907524736113995', '/admin/app/role', null, null, 'app_role', 'views/admin/app/role/index', null, 'APP15906275620845638', '1', '0', '2020-05-30 13:48:59', 'U001', '管理员', '2020-05-31 15:15:25', null, null, 'CP01', 'U001', 'CP01', '8', '0');
INSERT INTO `sys_menu` VALUES ('9', 'MC12', '应用开启', 'BTN', 'MC15907524736113995', null, null, null, 'app_open', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 13:49:01', 'U001', '管理员', '2020-05-30 15:02:31', null, null, 'CP01', 'U001', 'CP01', '5', '0');
INSERT INTO `sys_menu` VALUES ('10', 'MC12', '应用关闭', 'BTN', 'MC15907524736113995', null, null, null, 'app_close', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 14:43:16', 'U001', '管理员', '2020-05-30 18:00:05', null, null, 'CP01', 'U001', 'CP01', '6', '0');
INSERT INTO `sys_menu` VALUES ('11', 'MC1001', '菜单新增', 'BTN', 'MC10', null, null, null, 'menu_add', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 17:57:26', 'U001', '管理员', '2020-05-30 18:18:42', null, null, 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('12', 'MC1002', '菜单编辑', 'BTN', 'MC10', null, null, null, 'menu_edit', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 17:57:27', 'U001', '管理员', '2020-05-30 18:18:43', null, null, 'CP01', 'U001', 'CP01', '2', '0');
INSERT INTO `sys_menu` VALUES ('13', 'MC1003', '菜单查看', 'BTN', 'MC10', null, null, null, 'menu_view', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 17:57:28', 'U001', '管理员', '2020-05-30 18:18:43', null, null, 'CP01', 'U001', 'CP01', '3', '0');
INSERT INTO `sys_menu` VALUES ('14', 'MC1004', '菜单删除', 'BTN', 'MC10', null, null, null, 'menu_delete', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 17:57:30', 'U001', '管理员', '2020-05-30 18:18:44', null, null, 'CP01', 'U001', 'CP01', '4', '0');
INSERT INTO `sys_menu` VALUES ('15', 'MC1005', '菜单启用', 'BTN', 'MC10', null, null, null, 'menu_open', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 17:57:47', 'U001', '管理员', '2020-05-30 18:18:45', null, null, 'CP01', 'U001', 'CP01', '5', '0');
INSERT INTO `sys_menu` VALUES ('16', 'MC1006', '菜单关闭', 'BTN', 'MC10', null, null, null, 'menu_close', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 17:57:51', 'U001', '管理员', '2020-05-30 18:18:48', null, null, 'CP01', 'U001', 'CP01', '6', '0');
INSERT INTO `sys_menu` VALUES ('17', 'MC15908351493366943', '公司管理', 'MENU', 'MC9999', '/admin/company', null, 'el-icon-menu', null, 'views/admin/company/index', null, 'APP15906275620845638', '1', '0', '2020-05-30 18:39:11', 'U001', '管理员', '2020-07-10 17:26:24', 'U001', '管理员', 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('18', 'MC1101', '角色添加', 'BTN', 'MC11', null, null, null, 'manager_role_add', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 19:07:02', 'U001', '管理员', '2020-05-30 20:23:06', null, null, 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('19', 'MC1102', '角色编辑', 'BTN', 'MC11', null, null, null, 'manager_role_edit', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 19:07:08', 'U001', '管理员', '2020-05-30 20:23:09', null, null, 'CP01', 'U001', 'CP01', '2', '0');
INSERT INTO `sys_menu` VALUES ('20', 'MC1103', '角色查看', 'BTN', 'MC11', null, null, null, 'manager_role_view', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 19:07:08', 'U001', '管理员', '2020-05-30 20:23:11', null, null, 'CP01', 'U001', 'CP01', '3', '0');
INSERT INTO `sys_menu` VALUES ('21', 'MC1104', '角色删除', 'BTN', 'MC11', null, null, null, 'manager_role_delete', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 19:07:09', 'U001', '管理员', '2020-05-30 20:23:13', null, null, 'CP01', 'U001', 'CP01', '4', '0');
INSERT INTO `sys_menu` VALUES ('22', 'MC1105', '角色授权', 'BTN', 'MC11', null, null, null, 'manager_role_auth', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 19:07:11', 'U001', '管理员', '2020-05-30 20:23:15', null, null, 'CP01', 'U001', 'CP01', '5', '0');
INSERT INTO `sys_menu` VALUES ('23', 'MC1106', '角色开启', 'BTN', 'MC11', null, null, null, 'manager_role_open', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 19:07:37', 'U001', '管理员', '2020-05-30 20:23:18', null, null, 'CP01', 'U001', 'CP01', '6', '0');
INSERT INTO `sys_menu` VALUES ('24', 'MC1107', '角色关闭', 'BTN', 'MC11', null, null, null, 'manager_role_close', null, null, 'APP15906275620845638', '1', '0', '2020-05-30 19:08:24', 'U001', '管理员', '2020-05-30 20:23:24', null, null, 'CP01', 'U001', 'CP01', '7', '0');
INSERT INTO `sys_menu` VALUES ('25', 'MC15908450124067007', '系统管理', 'MENU', '0', null, null, 'el-icon-s-tools', null, null, null, 'APP15906275620845638', '1', '0', '2020-05-30 21:23:32', 'U001', '管理员', '2020-07-10 17:21:37', null, null, 'CP01', 'U001', 'CP01', '21', '0');
INSERT INTO `sys_menu` VALUES ('26', 'MC15908451170658422', '用户管理', 'MENU', 'MC15908450124067007', '/admin/user', null, 'el-icon-user-solid', null, 'views/admin/user/index', null, 'APP15906275620845638', '1', '0', '2020-05-30 21:25:17', 'U001', '管理员', '2020-07-10 17:26:41', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('27', 'MC15908451836374862', '角色配置', 'MENU', 'MC15908450124067007', '/admin/role', null, 'el-icon-star-on', null, 'views/admin/role/index', null, 'APP15906275620845638', '1', '0', '2020-05-30 21:26:23', 'U001', '管理员', '2020-07-10 17:26:51', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('231', 'MC110611', '用户开启', 'BTN', 'MC15908451170658422', '', '', '', 'user_open', '', '', 'APP15906275620845638', '1', '0', '2020-05-30 19:07:37', 'U001', '管理员', '2020-07-10 17:27:03', '', '', 'CP01', 'U001', 'CP01', '6', '0');
INSERT INTO `sys_menu` VALUES ('241', 'MC110711', '用户关闭', 'BTN', 'MC15908451170658422', '', '', '', 'user_close', '', '', 'APP15906275620845638', '1', '0', '2020-05-30 19:08:24', 'U001', '管理员', '2020-07-10 17:26:58', '', '', 'CP01', 'U001', 'CP01', '7', '0');
INSERT INTO `sys_menu` VALUES ('242', 'MC1500101', '公司新增', 'BTN', 'MC15908351493366943', null, null, null, 'manager_company_add', null, null, 'APP15906275620845638', '1', '0', '2020-05-31 14:44:05', 'U001', '管理员', '2020-05-31 14:49:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('243', 'MC1500102', '公司编辑', 'BTN', 'MC15908351493366943', null, null, null, 'manager_company_edit', null, null, 'APP15906275620845638', '1', '0', '2020-05-31 14:44:06', 'U001', '管理员', '2020-05-31 14:49:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('244', 'MC1500103', '公司查看', 'BTN', 'MC15908351493366943', null, null, null, 'manager_company_view', null, null, 'APP15906275620845638', '1', '0', '2020-05-31 14:44:08', 'U001', '管理员', '2020-05-31 14:49:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('245', 'MC1500104', '公司删除', 'BTN', 'MC15908351493366943', null, null, null, 'manager_company_delete', null, null, 'APP15906275620845638', '1', '0', '2020-05-31 14:44:09', 'U001', '管理员', '2020-05-31 14:49:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('246', 'MC1500105', '公司开启', 'BTN', 'MC15908351493366943', null, null, null, 'manager_company_open', null, null, 'APP15906275620845638', '1', '0', '2020-05-31 14:44:11', 'U001', '管理员', '2020-05-31 14:49:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('247', 'MC1500106', '公司关闭', 'BTN', 'MC15908351493366943', null, null, null, 'manager_company_close', null, null, 'APP15906275620845638', '1', '0', '2020-05-31 14:44:22', 'U001', '管理员', '2020-05-31 14:49:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('248', 'MC1500107', '公司用户', 'FUNCTION', 'MC15908351493366943', '/admin/company/user', null, 'el-icon-user', 'manager_company_user', 'views/admin/company/user/index', null, 'APP15906275620845638', '1', '0', '2020-05-31 14:46:48', 'U001', '管理员', '2020-07-10 17:27:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('249', 'MC15909130927808478', '数据权限', 'MENU', 'MC9999', '/admin/data', null, 'el-icon-finished', '', 'views/admin/data/index', null, 'APP15906275620845638', '1', '0', '2020-05-31 16:18:13', 'U001', '管理员', '2020-07-10 17:27:34', 'U001', '管理员', 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('250', 'MC15909233289603421', '用户新增', 'BTN', 'MC1500107', null, null, null, 'manager_user_add', null, null, 'APP15906275620845638', '1', '0', '2020-05-31 19:08:50', 'U001', '管理员', '2020-05-31 19:08:50', null, null, 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('251', 'MC15909233761609664', '用户查看', 'BTN', 'MC1500107', null, null, null, 'manager_user_view', null, null, 'APP15906275620845638', '1', '0', '2020-05-31 19:09:37', 'U001', '管理员', '2020-05-31 19:09:37', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('252', 'MC15909235998502878', '角色设置', 'BTN', 'MC1500107', null, null, null, 'manager_user_role', null, null, 'APP15906275620845638', '1', '0', '2020-05-31 19:13:20', 'U001', '管理员', '2020-05-31 19:13:20', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('253', 'MC15909317203776339', '子公司管理', 'MENU', 'MC15908450124067007', '/admin/child', null, 'el-icon-price-tag', null, 'views/admin/child/index', null, 'APP15906275620845638', '1', '0', '2020-05-31 21:28:41', 'U001', '管理员', '2020-07-10 17:27:51', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('254', 'MC15909329142621828', '用户详情', 'BTN', 'MC15908451170658422', null, null, null, 'user_view', null, null, 'APP15906275620845638', '1', '0', '2020-05-31 21:48:35', 'U001', '管理员', '2020-05-31 21:48:35', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('255', 'MC110811', '重置密码', 'BTN', 'MC15908451170658422', null, null, null, 'user_password', null, null, 'APP15906275620845638', '1', '0', '2020-05-31 22:32:55', 'U001', '管理员', '2020-05-31 22:33:43', null, null, 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('256', 'MC9999', '运营管理', 'MENU', '0', null, null, 'el-icon-s-home', null, null, null, 'APP15906275620845638', '1', '0', '2020-06-01 08:50:18', 'U001', '管理员', '2020-07-10 17:21:27', null, null, 'CP01', 'U001', 'CP01', '20', '0');
INSERT INTO `sys_menu` VALUES ('257', 'MC15914128374246222', '部门管理', 'MENU', 'MC15908450124067007', '/admin/dept', null, 'el-icon-coordinate', null, 'views/admin/dept/index', null, 'APP15906275620845638', '1', '0', '2020-06-06 11:07:18', 'U001', '管理员', '2020-07-10 17:25:55', 'U001', '管理员', 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('258', 'MC15001071', '子公司新增', 'BTN', 'MC15909317203776339', '', null, null, 'child_add', '', null, 'APP15906275620845638', '1', '0', '2020-06-06 12:31:33', 'U001', '管理员', '2020-06-06 12:39:07', null, null, 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('259', 'MC15001072', '子公司编辑', 'BTN', 'MC15909317203776339', '', null, null, 'child_edit', '', null, 'APP15906275620845638', '1', '0', '2020-06-06 12:31:35', 'U001', '管理员', '2020-06-06 12:39:04', null, null, 'CP01', 'U001', 'CP01', '2', '0');
INSERT INTO `sys_menu` VALUES ('260', 'MC15001073', '子公司查看', 'BTN', 'MC15909317203776339', '', null, null, 'child_view', '', null, 'APP15906275620845638', '1', '0', '2020-06-06 12:31:35', 'U001', '管理员', '2020-06-06 12:39:02', null, null, 'CP01', 'U001', 'CP01', '3', '0');
INSERT INTO `sys_menu` VALUES ('261', 'MC15001074', '子公司删除', 'BTN', 'MC15909317203776339', '', null, null, 'child_delete', '', null, 'APP15906275620845638', '1', '0', '2020-06-06 12:31:36', 'U001', '管理员', '2020-06-06 12:39:00', null, null, 'CP01', 'U001', 'CP01', '4', '0');
INSERT INTO `sys_menu` VALUES ('262', 'MC15001075', '子公司开启', 'BTN', 'MC15909317203776339', '', null, null, 'child_open', '', null, 'APP15906275620845638', '1', '0', '2020-06-06 12:31:37', 'U001', '管理员', '2020-06-06 12:38:57', null, null, 'CP01', 'U001', 'CP01', '5', '0');
INSERT INTO `sys_menu` VALUES ('263', 'MC15001076', '子公司关闭', 'BTN', 'MC15909317203776339', '', null, null, 'child_close', '', null, 'APP15906275620845638', '1', '0', '2020-06-06 12:31:37', 'U001', '管理员', '2020-06-06 15:50:46', null, null, 'CP01', 'U001', 'CP01', '6', '0');
INSERT INTO `sys_menu` VALUES ('264', 'MC15001077', '子公司用户', 'FUNCTION', 'MC15909317203776339', '/admin/child/user', null, null, 'child_user', 'views/admin/child/user/index', null, 'APP15906275620845638', '1', '0', '2020-06-06 12:31:39', 'U001', '管理员', '2020-06-06 12:38:52', null, null, 'CP01', 'U001', 'CP01', '7', '0');
INSERT INTO `sys_menu` VALUES ('265', 'MC150010777', '部门新增', 'BTN', 'MC15914128374246222', null, null, null, 'dept_add', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 12:40:28', 'U001', '管理员', '2020-06-06 12:45:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('266', 'MC150010766', '部门编辑', 'BTN', 'MC15914128374246222', null, null, null, 'dept_edit', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 12:40:29', 'U001', '管理员', '2020-06-06 12:45:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('267', 'MC150010755', '部门查看', 'BTN', 'MC15914128374246222', null, null, null, 'dept_view', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 12:40:29', 'U001', '管理员', '2020-06-06 12:45:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('268', 'MC150010744', '部门删除', 'BTN', 'MC15914128374246222', null, null, null, 'dept_delete', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 12:40:31', 'U001', '管理员', '2020-06-06 12:45:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('269', 'MC150010733', '部门开启', 'BTN', 'MC15914128374246222', null, null, null, 'dept_open', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 12:40:32', 'U001', '管理员', '2020-06-06 12:45:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('270', 'MC150010722', '部门关闭', 'BTN', 'MC15914128374246222', null, null, null, 'dept_close', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 12:40:34', 'U001', '管理员', '2020-06-06 12:45:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('271', 'MC150010721', '部门用户', 'FUNCTION', 'MC15914128374246222', '/admin/dept/user', null, null, 'dept_user', 'views/admin/dept/user/index', '', 'APP15906275620845638', '1', '0', '2020-06-06 12:40:59', 'U001', '管理员', '2020-06-06 12:45:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('272', 'MC150010770001', '用户禁用', 'BTN', 'MC150010721', null, null, null, 'dept_user_close', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 15:51:59', 'U001', '管理员', '2020-06-06 15:58:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('273', 'MC150010770002', '用户开启', 'BTN', 'MC150010721', null, null, null, 'dept_user_open', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 15:51:59', 'U001', '管理员', '2020-06-06 15:58:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('274', 'MC150010770003', '用户新增', 'BTN', 'MC150010721', null, null, null, 'dept_user_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 15:52:00', 'U001', '管理员', '2020-06-06 15:58:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('276', 'MC150010770005', '角色设置', 'BTN', 'MC150010721', null, null, null, 'dept_user_role', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 15:52:05', 'U001', '管理员', '2020-06-06 15:58:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('277', 'MC1500107700051', '角色设置', 'BTN', 'MC15001077', '', '', '', 'child_user_role', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 15:52:05', 'U001', '管理员', '2020-06-06 15:58:21', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('278', 'MC1500107700031', '用户新增', 'BTN', 'MC15001077', '', '', '', 'child_user_add', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 15:52:00', 'U001', '管理员', '2020-06-06 15:58:22', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('279', 'MC1500107700021', '用户开启', 'BTN', 'MC15001077', '', '', '', 'child_user_open', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 15:51:59', 'U001', '管理员', '2020-06-06 15:58:23', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('280', 'MC1500107700011', '用户禁用', 'BTN', 'MC15001077', '', '', '', 'child_user_close', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 15:51:59', 'U001', '管理员', '2020-06-06 15:58:25', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('281', 'MC15914313350266378', '基础管理', 'MENU', '0', null, null, 'el-icon-help', null, null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:15:35', 'U001', '管理员', '2020-07-10 17:21:24', null, null, 'CP01', 'U001', 'CP01', '18', '0');
INSERT INTO `sys_menu` VALUES ('282', 'MC15914315858413972', '仓库管理', 'MENU', '0', null, null, 'el-icon-office-building', null, null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:19:45', 'U001', '管理员', '2020-07-10 17:20:53', null, null, 'CP01', 'U001', 'CP01', '19', '0');
INSERT INTO `sys_menu` VALUES ('283', 'MC15914316095982777', '生产管理', 'MENU', '0', null, null, 'el-icon-set-up', null, null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:20:09', 'U001', '管理员', '2020-07-10 17:21:13', null, null, 'CP01', 'U001', 'CP01', '5', '0');
INSERT INTO `sys_menu` VALUES ('284', 'MC15914318899150370', '客户管理', 'MENU', 'MC15914313350266378', '/base/customer', null, 'el-icon-s-custom', null, 'views/base/customer/index', null, 'APP15906275620845638', '1', '0', '2020-06-06 16:24:49', 'U001', '管理员', '2020-07-10 17:12:31', 'U001', '管理员', 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('285', 'MC15914315', '客户关闭', 'BTN', 'MC15914318899150370', null, null, null, 'customer_close', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:48:30', 'U001', '管理员', '2020-06-06 17:01:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('286', 'MC15914314', '客户开启', 'BTN', 'MC15914318899150370', null, null, null, 'customer_open', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:48:31', 'U001', '管理员', '2020-06-06 17:01:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('287', 'MC15914313', '客户查看', 'BTN', 'MC15914318899150370', null, null, null, 'customer_view', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:48:32', 'U001', '管理员', '2020-06-06 17:01:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('288', 'MC15914312', '客户编辑', 'BTN', 'MC15914318899150370', null, null, null, 'customer_edit', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:48:33', 'U001', '管理员', '2020-06-06 17:01:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('289', 'MC15914311', '客户新增', 'BTN', 'MC15914318899150370', null, null, null, 'customer_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:48:40', 'U001', '管理员', '2020-06-06 17:01:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('290', 'MC1590913095', '数权关闭', 'BTN', 'MC15909130927808478', null, null, null, 'data_close', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:50:42', 'U001', '管理员', '2020-06-06 16:58:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('291', 'MC1590913094', '数权开启', 'BTN', 'MC15909130927808478', null, null, null, 'data_open', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:50:43', 'U001', '管理员', '2020-06-06 16:58:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('292', 'MC1590913093', '数权查看', 'BTN', 'MC15909130927808478', null, null, null, 'data_view', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:50:43', 'U001', '管理员', '2020-06-06 16:58:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('293', 'MC1590913092', '数权编辑', 'BTN', 'MC15909130927808478', null, null, null, 'data_edit', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:50:44', 'U001', '管理员', '2020-06-06 16:58:40', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('294', 'MC1590913091', '数权新增', 'BTN', 'MC15909130927808478', null, null, null, 'data_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:50:47', 'U001', '管理员', '2020-06-06 16:58:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('295', 'MC15901', '角色新增', 'BTN', 'MC15908451836374862', null, null, null, 'role_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:52:12', 'U001', '管理员', '2020-06-06 16:59:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('296', 'MC15902', '角色编辑', 'BTN', 'MC15908451836374862', null, null, null, 'role_edit', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:52:14', 'U001', '管理员', '2020-06-06 16:59:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('297', 'MC15903', '角色查看', 'BTN', 'MC15908451836374862', null, null, null, 'role_view', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:52:15', 'U001', '管理员', '2020-06-06 16:59:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('298', 'MC15904', '角色删除', 'BTN', 'MC15908451836374862', null, null, null, 'role_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:52:15', 'U001', '管理员', '2020-06-06 16:59:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('299', 'MC15905', '角色授权', 'BTN', 'MC15908451836374862', null, null, null, 'role_auth', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:52:18', 'U001', '管理员', '2020-06-06 17:12:52', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('300', 'MC15906', '角色开启', 'BTN', 'MC15908451836374862', null, null, null, 'role_open', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:52:24', 'U001', '管理员', '2020-06-06 16:59:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('301', 'MC15907', '角色关闭', 'BTN', 'MC15908451836374862', null, null, null, 'role_close', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 16:53:35', 'U001', '管理员', '2020-06-06 16:59:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('302', 'MC15914347262962224', '供应商管理', 'MENU', 'MC15914313350266378', '/base/supplier', null, 'el-icon-female', null, 'views/base/supplier/index', null, 'APP15906275620845638', '1', '0', '2020-06-06 17:12:06', 'U001', '管理员', '2020-07-10 17:28:02', null, null, 'CP01', 'U001', 'CP01', '2', '0');
INSERT INTO `sys_menu` VALUES ('303', 'MC15914347513001853', '加工户管理', 'MENU', 'MC15914313350266378', '/base/handle', null, 'el-icon-s-shop', null, 'views/base/handle/index', null, 'APP15906275620845638', '1', '0', '2020-06-06 17:12:31', 'U001', '管理员', '2020-07-10 17:25:33', null, null, 'CP01', 'U001', 'CP01', '3', '0');
INSERT INTO `sys_menu` VALUES ('304', 'MC159143471', '加工户关闭', 'BTN', 'MC15914347513001853', null, null, null, 'handle_close', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 16:48:30', 'U001', '管理员', '2020-06-06 17:18:44', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('305', 'MC159143472', '加工户开启', 'BTN', 'MC15914347513001853', null, null, null, 'handle_open', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 16:48:31', 'U001', '管理员', '2020-06-06 17:18:40', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('306', 'MC159143473', '加工户查看', 'BTN', 'MC15914347513001853', null, null, null, 'handle_view', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 16:48:32', 'U001', '管理员', '2020-06-06 17:18:38', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('307', 'MC159143474', '加工户编辑', 'BTN', 'MC15914347513001853', null, null, null, 'handle_edit', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 16:48:33', 'U001', '管理员', '2020-06-06 17:18:31', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('308', 'MC159143475', '加工户新增', 'BTN', 'MC15914347513001853', null, null, null, 'handle_add', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 16:48:40', 'U001', '管理员', '2020-06-06 17:18:29', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('309', 'MC1591434751', '供应商新增', 'BTN', 'MC15914347262962224', '', '', '', 'supplier_add', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 16:48:40', 'U001', '管理员', '2020-06-06 17:18:15', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('310', 'MC1591434742', '供应商编辑', 'BTN', 'MC15914347262962224', '', '', '', 'supplier_edit', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 16:48:33', 'U001', '管理员', '2020-06-06 17:18:12', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('311', 'MC1591434733', '供应商查看', 'BTN', 'MC15914347262962224', '', '', '', 'supplier_view', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 16:48:32', 'U001', '管理员', '2020-06-06 17:18:11', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('312', 'MC1591434724', '供应商开启', 'BTN', 'MC15914347262962224', '', '', '', 'supplier_open', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 16:48:31', 'U001', '管理员', '2020-06-06 17:18:09', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('313', 'MC1591434715', '供应商关闭', 'BTN', 'MC15914347262962224', '', '', '', 'supplier_close', '', '', 'APP15906275620845638', '1', '0', '2020-06-06 16:48:30', 'U001', '管理员', '2020-06-06 17:17:55', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('314', 'MC15914376418604103', '默认按钮', 'BTN', 'MC10', null, null, null, 'menu_btn', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 18:00:41', 'U001', '管理员', '2020-06-06 18:00:41', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('315', 'MC15914377245922261', '类型管理', 'MENU', 'MC15914313350266378', '/base/type', null, 'el-icon-postcard', null, 'views/base/type/index', null, 'APP15906275620845638', '1', '0', '2020-06-06 18:02:04', 'U001', '管理员', '2020-07-10 17:28:15', 'U001', '管理员', 'CP01', 'U001', 'CP01', '4', '0');
INSERT INTO `sys_menu` VALUES ('316', 'MC15914378160443006', '属性管理', 'MENU', 'MC15914313350266378', '/base/template', null, 'el-icon-circle-plus', null, 'views/base/template/index', null, 'APP15906275620845638', '1', '0', '2020-06-06 18:03:36', 'U001', '管理员', '2020-07-10 17:25:18', null, null, 'CP01', 'U001', 'CP01', '5', '0');
INSERT INTO `sys_menu` VALUES ('317', 'MC15914396277851780', '类型新增', 'BTN', 'MC15914377245922261', null, null, null, 'type_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 18:33:48', null, null, '2020-07-19 11:40:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('318', 'MC15914396277864836', '类型编辑', 'BTN', 'MC15914377245922261', null, null, null, 'type_edit', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 18:33:48', null, null, '2020-07-19 11:40:36', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('319', 'MC15914396277867698', '类型查看', 'BTN', 'MC15914377245922261', null, null, null, 'type_view', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 18:33:48', null, null, '2020-07-19 11:40:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('320', 'MC15914396277866900', '类型删除', 'BTN', 'MC15914377245922261', null, null, null, 'type_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 18:33:49', null, null, '2020-07-19 11:40:39', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('321', 'MC15914396277878832', '类型开启', 'BTN', 'MC15914377245922261', null, null, null, 'type_open', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 18:33:49', null, null, '2020-07-19 11:40:40', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('322', 'MC15914396277877159', '类型关闭', 'BTN', 'MC15914377245922261', null, null, null, 'type_close', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 18:33:49', null, null, '2020-07-19 11:40:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('323', 'MC15914396355918880', '属性新增', 'BTN', 'MC15914378160443006', null, null, null, 'template_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 18:33:55', null, null, '2020-07-20 12:26:56', null, null, 'CP01', null, 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('324', 'MC15914396355927442', '属性编辑', 'BTN', 'MC15914378160443006', null, null, null, 'template_edit', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 18:33:55', null, null, '2020-07-20 12:26:56', null, null, 'CP01', null, 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('325', 'MC15914396356030248', '属性查看', 'BTN', 'MC15914378160443006', null, null, null, 'template_view', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 18:33:55', null, null, '2020-07-20 12:26:56', null, null, 'CP01', null, 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('327', 'MC15914396356049376', '属性开启', 'BTN', 'MC15914378160443006', null, null, null, 'template_open', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 18:33:55', null, null, '2020-07-20 12:26:56', null, null, 'CP01', null, 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('328', 'MC15914396356050761', '属性关闭', 'BTN', 'MC15914378160443006', null, null, null, 'template_close', null, null, 'APP15906275620845638', '1', '0', '2020-06-06 18:33:55', null, null, '2020-07-20 12:26:56', null, null, 'CP01', null, 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('329', 'MC15914960797205706', '物料管理', 'MENU', 'MC15914313350266378', '/base/material', null, 'el-icon-s-cooperation', null, 'views/base/material/index', null, 'APP15906275620845638', '1', '0', '2020-06-07 10:14:40', 'U001', '管理员', '2020-07-10 17:25:06', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('330', 'MC15914975002110092', '新增', 'BTN', 'MC15914960797205706', null, null, null, 'material_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 10:38:20', 'U001', '管理员', '2020-06-07 10:39:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('331', 'MC15914975002126324', '编辑', 'BTN', 'MC15914960797205706', null, null, null, 'material_edit', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 10:38:21', 'U001', '管理员', '2020-06-07 10:39:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('332', 'MC15914975002125159', '查看', 'BTN', 'MC15914960797205706', null, null, null, 'material_view', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 10:38:21', 'U001', '管理员', '2020-08-03 16:07:58', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('333', 'MC15914975002133170', '色号管理', 'FUNCTION', 'MC15914960797205706', '/admin/material/model', null, null, 'material_model', 'views/base/material/model/index', null, 'APP15906275620845638', '1', '0', '2020-06-07 10:38:21', 'U001', '管理员', '2020-07-20 11:01:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('334', 'MC15914975002136386', '开启', 'BTN', 'MC15914960797205706', null, null, null, 'material_open', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 10:38:21', 'U001', '管理员', '2020-06-07 10:39:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('335', 'MC15914975002149169', '关闭', 'BTN', 'MC15914960797205706', null, null, null, 'material_close', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 10:38:21', 'U001', '管理员', '2020-06-07 10:39:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('336', 'MC15915162486452432', '新增', 'BTN', 'MC15914975002133170', null, null, null, 'model_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 15:50:50', 'U001', '管理员', '2020-06-07 15:51:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('337', 'MC15915162486495390', '编辑', 'BTN', 'MC15914975002133170', null, null, null, 'model_edit', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 15:50:50', 'U001', '管理员', '2020-06-07 15:51:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('338', 'MC15915162486491770', '查看', 'BTN', 'MC15914975002133170', null, null, null, 'model_view', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 15:50:50', 'U001', '管理员', '2020-06-07 15:51:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('340', 'MC15915162486507564', '开启', 'BTN', 'MC15914975002133170', null, null, null, 'model_open', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 15:50:50', 'U001', '管理员', '2020-06-07 15:51:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('341', 'MC15915162486510267', '关闭', 'BTN', 'MC15914975002133170', null, null, null, 'model_close', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 15:50:51', 'U001', '管理员', '2020-06-07 15:51:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('342', 'MC15915178876094091', '仓库配置', 'MENU', 'MC15914315858413972', '/wms/warehouse', null, 'el-icon-c-scale-to-original', null, 'views/wms/warehouse/index', null, 'APP15906275620845638', '1', '0', '2020-06-07 16:18:08', 'U001', '管理员', '2020-07-10 17:24:51', null, null, 'CP01', 'U001', 'CP01', '6', '0');
INSERT INTO `sys_menu` VALUES ('343', 'MC15915179468335964', '出库管理', 'MENU', 'MC15914315858413972', '/wms/order/out', null, 'el-icon-top', null, 'views/wms/out/index', null, 'APP15906275620845638', '1', '0', '2020-06-07 16:19:07', 'U001', '管理员', '2020-07-10 17:24:40', null, null, 'CP01', 'U001', 'CP01', '2', '0');
INSERT INTO `sys_menu` VALUES ('344', 'MC15915179885058020', '入库管理', 'MENU', 'MC15914315858413972', '/wms/order/in', null, 'el-icon-bottom', null, 'views/wms/in/index', null, 'APP15906275620845638', '1', '0', '2020-06-07 16:19:49', 'U001', '管理员', '2020-07-10 17:24:36', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('345', 'MC15915180351375063', '库存管理', 'MENU', 'MC15914315858413972', '/wms/stock', null, 'el-icon-s-ticket', null, 'views/wms/stock/index', null, 'APP15906275620845638', '1', '0', '2020-06-07 16:20:36', 'U001', '管理员', '2020-07-10 17:24:17', null, null, 'CP01', 'U001', 'CP01', '3', '0');
INSERT INTO `sys_menu` VALUES ('346', 'MC15915189355825373', '新增', 'BTN', 'MC15915178876094091', null, null, null, 'warehouse_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 16:35:36', 'U001', '管理员', '2020-06-07 16:36:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('347', 'MC15915189355837642', '编辑', 'BTN', 'MC15915178876094091', null, null, null, 'warehouse_edit', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 16:35:36', 'U001', '管理员', '2020-06-07 16:36:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('348', 'MC15915189355842974', '查看', 'BTN', 'MC15915178876094091', null, null, null, 'warehouse_view', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 16:35:36', 'U001', '管理员', '2020-06-07 16:36:16', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('350', 'MC15915189355855177', '开启', 'BTN', 'MC15915178876094091', null, null, null, 'warehouse_open', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 16:35:36', 'U001', '管理员', '2020-06-07 16:36:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('351', 'MC15915189355854014', '关闭', 'BTN', 'MC15915178876094091', null, null, null, 'warehouse_close', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 16:35:36', 'U001', '管理员', '2020-06-07 16:36:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('352', 'MC15915232094752126', '采购管理', 'MENU', 'MC15921316489411035', '/prod/purchase', null, 'el-icon-download', null, 'views/prod/purchase/index', null, 'APP15906275620845638', '1', '0', '2020-06-07 17:46:49', 'U001', '管理员', '2020-07-10 17:23:59', null, null, 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('353', 'MC15915321692402826', '采购新增', 'FUNCTION', 'MC15915232094752126', '/prod/purchase/add', null, null, 'purchase_add', 'views/prod/purchase/add/index', null, 'APP15906275620845638', '1', '0', '2020-06-07 20:16:09', 'U001', '管理员', '2020-08-22 16:34:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('354', 'MC15915321692412802', '采购编辑', 'FUNCTION', 'MC15915232094752126', '/prod/purchase/edit', null, null, 'purchase_edit', 'views/prod/purchase/edit/index', null, 'APP15906275620845638', '1', '0', '2020-06-07 20:16:09', 'U001', '管理员', '2020-06-08 18:53:38', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('355', 'MC15915321692422274', '采购查看', 'FUNCTION', 'MC15915232094752126', '/prod/purchase/view', null, null, 'purchase_view', 'views/prod/purchase/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-07 20:16:09', 'U001', '管理员', '2020-06-08 18:53:57', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('356', 'MC15915321692424233', '删除', 'BTN', 'MC15915232094752126', null, null, null, 'purchase_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 20:16:09', 'U001', '管理员', '2020-06-07 20:16:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('357', 'MC15915321692437765', '通过', 'BTN', 'MC15915232094752126', null, null, null, 'purchase_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 20:16:09', 'U001', '管理员', '2020-06-07 20:17:01', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('358', 'MC15915321692442326', '取消', 'BTN', 'MC15915232094752126', null, null, null, 'purchase_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-07 20:16:09', 'U001', '管理员', '2020-06-07 20:17:07', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('359', 'MC15916716796507619', '销售管理', 'MENU', 'MC15921316489411035', '/prod/sale', null, 'el-icon-upload2', null, 'views/prod/sale/index', null, 'APP15906275620845638', '1', '0', '2020-06-09 11:01:19', 'U001', '管理员', '2020-07-10 17:23:57', null, null, 'CP01', 'U001', 'CP01', '2', '0');
INSERT INTO `sys_menu` VALUES ('360', 'MC15916716869404703', '销售新增', 'FUNCTION', 'MC15916716796507619', '/prod/sale/add', null, null, 'sale_add', 'views/prod/sale/add/index', null, 'APP15906275620845638', '1', '0', '2020-06-09 11:01:27', 'U001', '管理员', '2020-06-09 11:03:33', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('361', 'MC15916716869411013', '销售编辑', 'FUNCTION', 'MC15916716796507619', '/prod/sale/edit', null, null, 'sale_edit', 'views/prod/sale/edit/index', null, 'APP15906275620845638', '1', '0', '2020-06-09 11:01:27', 'U001', '管理员', '2020-06-09 11:03:48', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('362', 'MC15916716869413817', '销售查看', 'FUNCTION', 'MC15916716796507619', '/prod/sale/view', null, null, 'sale_view', 'views/prod/sale/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-09 11:01:27', 'U001', '管理员', '2020-06-09 11:03:39', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('363', 'MC15916716869415256', '删除', 'BTN', 'MC15916716796507619', null, null, null, 'sale_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-09 11:01:27', 'U001', '管理员', '2020-06-09 11:02:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('364', 'MC15916716869422895', '开启', 'BTN', 'MC15916716796507619', null, null, null, 'sale_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-09 11:01:27', 'U001', '管理员', '2020-06-09 11:02:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('365', 'MC15916716869421760', '关闭', 'BTN', 'MC15916716796507619', null, null, null, 'sale_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-09 11:01:27', 'U001', '管理员', '2020-06-09 11:02:29', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('366', 'MC15917008822490563', '入库', 'BTN', 'MC15915179885058020', null, null, null, 'order_in_in', null, null, 'APP15906275620845638', '1', '0', '2020-06-09 19:08:02', 'U001', '管理员', '2020-06-09 19:08:59', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('367', 'MC15917008822514155', '取消', 'BTN', 'MC15915179885058020', null, null, null, 'order_in_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-09 19:08:02', 'U001', '管理员', '2020-06-09 19:09:05', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('368', 'MC15917008822518563', '入库详情', 'FUNCTION', 'MC15915179885058020', '/wms/in/view', null, null, 'order_in_view', 'views/wms/in/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-09 19:08:02', 'U001', '管理员', '2020-06-12 20:02:11', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('372', 'MC15917009549252282', '出库', 'BTN', 'MC15915179468335964', null, null, null, 'order_out_out', null, null, 'APP15906275620845638', '1', '0', '2020-06-09 19:09:15', 'U001', '管理员', '2020-06-09 19:10:04', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('373', 'MC15917009549269061', '取消', 'BTN', 'MC15915179468335964', null, null, null, 'order_out_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-09 19:09:15', 'U001', '管理员', '2020-06-09 19:10:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('374', 'MC15917009549274710', '出库详情', 'FUNCTION', 'MC15915179468335964', '/wms/out/view', null, null, 'order_out_view', 'views/wms/out/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-09 19:09:15', 'U001', '管理员', '2020-07-01 10:51:52', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('378', 'MC15917037605031945', '快速入库', 'MENU', 'MC15914316095982777', '/room/quick/in', null, 'el-icon-setting', null, 'views/prod/room/quick/index', null, 'APP15906275620845638', '1', '0', '2020-06-09 19:56:00', 'U001', '管理员', '2020-07-28 14:56:41', 'U001', '管理员', 'CP01', 'U001', 'CP01', '-1', '0');
INSERT INTO `sys_menu` VALUES ('379', 'MC15919653933238573', '库存明细', 'FUNCTION', 'MC15915180351375063', '/wms/stock/detail', null, '', 'wms_stock_detail', 'views/wms/stock/detail/index', null, 'APP15906275620845638', '1', '0', '2020-06-12 20:36:35', 'U001', '管理员', '2020-06-12 20:36:35', null, null, 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('380', 'MC15919654354046452', '库存日志', 'FUNCTION', 'MC15915180351375063', '/wms/stock/log', null, null, 'wms_stock_log', 'views/wms/stock/log/index', null, 'APP15906275620845638', '1', '0', '2020-06-12 20:37:17', 'U001', '管理员', '2020-06-12 20:37:17', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('381', 'MC15919800812908111', '订单管理', 'MENU', 'MC15921316489411035', '/prod/order', null, 'el-icon-s-order', null, 'views/prod/order/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 00:41:23', 'U001', '管理员', '2020-07-10 17:23:17', 'U001', '管理员', 'CP01', 'U001', 'CP01', '3', '0');
INSERT INTO `sys_menu` VALUES ('382', 'MC15920115067041821', '订单新增', 'FUNCTION', 'MC15919800812908111', '/prod/order/add', null, null, 'order_add', 'views/prod/order/add/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 09:25:09', 'U001', '管理员', '2020-07-01 10:51:30', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('383', 'MC15920115067050841', '订单编辑', 'FUNCTION', 'MC15919800812908111', '/prod/order/edit', null, null, 'order_edit', 'views/prod/order/edit/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 09:25:09', 'U001', '管理员', '2020-07-01 10:51:34', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('384', 'MC15920115067051144', '订单查看', 'FUNCTION', 'MC15919800812908111', '/prod/order/view', null, null, 'order_view', 'views/prod/order/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 09:25:09', 'U001', '管理员', '2020-07-01 10:51:45', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('385', 'MC15920115067056351', '删除', 'BTN', 'MC15919800812908111', null, null, null, 'order_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 09:25:09', 'U001', '管理员', '2020-06-13 10:27:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('386', 'MC15920115067064690', '排期', 'BTN', 'MC15919800812908111', null, null, null, 'order_prod', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 09:25:09', 'U001', '管理员', '2020-06-13 10:31:48', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('387', 'MC15920115067077920', '完成', 'BTN', 'MC15919800812908111', null, null, null, 'order_finish', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 09:25:09', 'U001', '管理员', '2020-06-13 10:30:40', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('388', 'MC15920115067048746', '审核', 'BTN', 'MC15919800812908111', null, null, null, 'order_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 10:29:55', 'U001', '管理员', '2020-06-13 10:36:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('389', 'MC15920115067041234', '进入生产', 'FUNCTION', 'MC15919800812908111', '/prod/room', null, null, 'order_room', 'views/prod/room/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 10:31:06', 'U001', '管理员', '2020-06-13 13:48:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('390', 'MC15920115067274665', '工序', 'FUNCTION', 'MC15914960797205706', '/prod/material/step', null, null, 'material_step', 'views/base/material/step/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 10:32:25', 'U001', '管理员', '2020-08-03 17:20:04', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('391', 'MC15920185456085838', '工序配置', 'MENU', 'MC15914316095982777', '/prod/step', null, 'el-icon-turn-off', null, 'views/prod/step/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 11:22:27', 'U001', '管理员', '2020-07-10 17:22:31', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('392', 'MC15920185513191809', '新增', 'BTN', 'MC15920185456085838', null, null, null, 'step_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 11:22:33', 'U001', '管理员', '2020-06-13 11:33:18', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('393', 'MC15920185513205158', '编辑', 'BTN', 'MC15920185456085838', null, null, null, 'step_edit', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 11:22:33', 'U001', '管理员', '2020-06-13 11:33:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('394', 'MC15920185513207648', '查看', 'BTN', 'MC15920185456085838', null, null, null, 'step_view', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 11:22:33', 'U001', '管理员', '2020-06-13 11:33:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('396', 'MC15920185513201150', '开启', 'BTN', 'MC15920185456085838', null, null, null, 'step_open', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 11:22:33', 'U001', '管理员', '2020-06-13 11:33:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('397', 'MC15920185513203661', '关闭', 'BTN', 'MC15920185456085838', null, null, null, 'step_close', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 11:22:33', 'U001', '管理员', '2020-06-13 11:33:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('398', 'MC15920194069281756', '新增', 'BTN', 'MC15920115067274665', null, null, null, 'order_step_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 11:36:49', 'U001', '管理员', '2020-06-13 11:47:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('399', 'MC15920194069299345', '编辑', 'BTN', 'MC15920115067274665', null, null, null, 'order_step_edit', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 11:36:49', 'U001', '管理员', '2020-06-13 11:47:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('400', 'MC15920194069303363', '查看', 'BTN', 'MC15920115067274665', null, null, null, 'order_step_view', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 11:36:49', 'U001', '管理员', '2020-06-13 11:47:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('401', 'MC15920194069309352', '删除', 'BTN', 'MC15920115067274665', null, null, null, 'order_step_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 11:36:49', 'U001', '管理员', '2020-06-13 11:47:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('402', 'MC15920194069310445', '开启', 'BTN', 'MC15920115067274665', null, null, null, 'order_step_open', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 11:36:49', 'U001', '管理员', '2020-06-13 11:47:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('403', 'MC15920194069328818', '关闭', 'BTN', 'MC15920115067274665', null, null, null, 'order_step_close', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 11:36:49', 'U001', '管理员', '2020-06-13 11:47:05', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('404', 'MC15920221446840895', '生产车间', 'MENU', 'MC15914316095982777', '/prod/room', null, 'el-icon-box', null, 'views/prod/room/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 12:22:26', 'U001', '管理员', '2020-07-10 17:22:17', 'U001', '管理员', 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('405', 'MC15920253458676691', '生产工序', 'FUNCTION', 'MC15920221446840895', '/prod/business/step', null, null, 'business_step', 'views/prod/room/step/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 13:15:48', 'U001', '管理员', '2020-06-29 11:11:18', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('406', 'MC15920267763306406', '入库新增', 'FUNCTION', 'MC15920253458678475', '/prod/business/in', null, null, 'business_in_add', 'views/prod/room/in/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 13:39:38', 'U001', '管理员', '2020-06-29 11:09:42', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('407', 'MC15920267763314983', '出库编辑', 'FUNCTION', 'MC15920253458678475', '/prod/business/edit/out', null, null, 'business_out_edit', 'views/prod/room/edit/out/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 13:39:38', 'U001', '管理员', '2020-07-05 08:36:56', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('408', 'MC15920267763311136', '加工单详情', 'FUNCTION', 'MC15920253458678475', '/prod/business/view', null, null, 'business_view', 'views/prod/room/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 13:39:38', 'U001', '管理员', '2020-07-01 10:52:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('409', 'MC15920267763319955', '删除', 'BTN', 'MC15920253458678475', null, null, null, 'business_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 13:39:38', 'U001', '管理员', '2020-06-29 11:09:42', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('410', 'MC15920267763314662', '通过', 'BTN', 'MC15920253458678475', null, null, null, 'business_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 13:39:38', 'U001', '管理员', '2020-06-29 11:09:42', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('411', 'MC15920267763327980', '取消', 'BTN', 'MC15920253458678475', null, null, null, 'business_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 13:39:38', 'U001', '管理员', '2020-06-29 11:09:42', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('412', 'MC15920267763306487', '出库新增', 'FUNCTION', 'MC15920253458678475', '/prod/business/out', null, null, 'business_out_add', 'views/prod/room/out/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 13:41:20', 'U001', '管理员', '2020-06-29 11:09:42', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('413', 'MC15920498177618661', '订单发货', 'MENU', 'MC15921316489411035', '/prod/order/send', null, 'el-icon-shopping-cart-2', null, 'views/prod/send/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 20:03:37', 'U001', '管理员', '2020-07-10 17:19:36', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('414', 'MC15920522992062998', '发货新增', 'FUNCTION', 'MC15920498177618661', '/prod/order/send/add', null, null, 'send_add', 'views/prod/send/add/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 20:44:59', 'U001', '管理员', '2020-07-01 10:52:17', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('415', 'MC15920522992097016', '发货编辑', 'FUNCTION', 'MC15920498177618661', '/prod/order/send/edit', null, null, 'send_edit', 'views/prod/send/edit/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 20:44:59', 'U001', '管理员', '2020-07-01 10:52:29', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('416', 'MC15920522992109187', '发货详情', 'FUNCTION', 'MC15920498177618661', '/prod/order/send/view', null, null, 'send_view', 'views/prod/send/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-13 20:44:59', 'U001', '管理员', '2020-07-01 10:52:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('417', 'MC15920522992103836', '删除', 'BTN', 'MC15920498177618661', null, null, null, 'send_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 20:44:59', 'U001', '管理员', '2020-06-13 20:45:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('418', 'MC15920522992110418', '通过', 'BTN', 'MC15920498177618661', null, null, null, 'send_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 20:44:59', 'U001', '管理员', '2020-06-13 20:45:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('419', 'MC15920522992115783', '取消', 'BTN', 'MC15920498177618661', null, null, null, 'send_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-13 20:44:59', 'U001', '管理员', '2020-06-13 20:45:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('420', 'MC15921152571761824', '采购退货', 'FUNCTION', 'MC15915232094752126', '/prod/purchase/back', null, null, 'purchase_back', 'views/prod/purchase/back/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 14:14:17', 'U001', '管理员', '2020-06-14 14:14:17', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('421', 'MC15921164457537340', '采购退货新增', 'FUNCTION', 'MC15921152571761824', '/prod/purchase/back/add', null, null, 'purchase_back_add', 'views/prod/purchase/back/add/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 14:34:06', 'U001', '管理员', '2020-07-01 10:52:47', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('422', 'MC15921164457541357', '采购退货编辑', 'FUNCTION', 'MC15921152571761824', '/prod/purchase/back/edit', null, null, 'purchase_back_edit', 'views/prod/purchase/back/edit/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 14:34:06', 'U001', '管理员', '2020-07-01 10:52:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('423', 'MC15921164457557435', '采购退货详情', 'FUNCTION', 'MC15921152571761824', '/prod/purchase/back/view', null, null, 'purchase_back_view', 'views/prod/purchase/back/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 14:34:06', 'U001', '管理员', '2020-07-01 10:53:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('424', 'MC15921164457561106', '删除', 'BTN', 'MC15921152571761824', null, null, null, 'purchase_back_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-14 14:34:06', 'U001', '管理员', '2020-06-14 14:34:52', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('425', 'MC15921164457562033', '通过', 'BTN', 'MC15921152571761824', null, null, null, 'purchase_back_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-14 14:34:06', 'U001', '管理员', '2020-06-14 14:35:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('426', 'MC15921164457578070', '取消', 'BTN', 'MC15921152571761824', null, null, null, 'purchase_back_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-14 14:34:06', 'U001', '管理员', '2020-06-14 14:35:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('427', 'MC15921205758877845', '销售退货', 'FUNCTION', 'MC15916716796507619', '/prod/sale/back', null, null, 'sale_back', 'views/prod/sale/back/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 15:42:56', 'U001', '管理员', '2020-06-14 15:43:26', 'U001', '管理员', 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('428', 'MC15921205929516074', '销售退货新增', 'FUNCTION', 'MC15921205758877845', '/prod/sale/back/add', null, null, 'sale_back_add', 'views/prod/sale/back/add/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 15:43:13', 'U001', '管理员', '2020-07-01 10:53:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('429', 'MC15921205929516152', '销售退货编辑', 'FUNCTION', 'MC15921205758877845', '/prod/sale/back/edit', null, null, 'sale_back_edit', 'views/prod/sale/back/edit/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 15:43:13', 'U001', '管理员', '2020-07-01 10:53:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('430', 'MC15921205929524036', '销售退货详情', 'FUNCTION', 'MC15921205758877845', '/prod/sale/back/view', null, null, 'sale_back_view', 'views/prod/sale/back/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 15:43:13', 'U001', '管理员', '2020-07-01 10:53:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('431', 'MC15921205929521456', '删除', 'BTN', 'MC15921205758877845', null, null, null, 'sale_back_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-14 15:43:13', 'U001', '管理员', '2020-06-14 15:54:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('432', 'MC15921205929538857', '通过', 'BTN', 'MC15921205758877845', null, null, null, 'sale_back_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-14 15:43:13', 'U001', '管理员', '2020-06-14 15:54:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('433', 'MC15921205929539894', '取消', 'BTN', 'MC15921205758877845', null, null, null, 'sale_back_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-14 15:43:13', 'U001', '管理员', '2020-06-14 15:54:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('434', 'MC15921223012273018', '退货管理', 'MENU', '0', null, null, 'el-icon-truck', null, null, null, 'APP15906275620845638', '1', '0', '2020-06-14 16:11:41', 'U001', '管理员', '2020-07-10 17:19:14', 'U001', '管理员', 'CP01', 'U001', 'CP01', '9', '0');
INSERT INTO `sys_menu` VALUES ('435', 'MC15921223851143081', '采购退货', 'MENU', 'MC15921223012273018', '/prod/back/purchase', null, 'el-icon-truck', null, 'views/prod/back/purchase/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 16:13:05', 'U001', '管理员', '2020-07-10 17:19:08', null, null, 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('437', 'MC15921252103797314', '采购退货编辑', 'FUNCTION', 'MC15921223851143081', '/prod/purchase/back/edit', null, null, 'm_purchase_back_edit', 'views/prod/purchase/back/edit/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 17:00:10', 'U001', '管理员', '2020-07-01 10:53:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('438', 'MC15921252103803582', '采购退货详情', 'FUNCTION', 'MC15921223851143081', '/prod/purchase/back/view', null, null, 'm_purchase_back_view', 'views/prod/purchase/back/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 17:00:10', 'U001', '管理员', '2020-07-01 10:53:56', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('439', 'MC15921252103800066', '删除', 'BTN', 'MC15921223851143081', null, null, null, 'm_purchase_back_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-14 17:00:11', 'U001', '管理员', '2020-06-14 17:02:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('440', 'MC15921252103800038', '通过', 'BTN', 'MC15921223851143081', null, null, null, 'm_purchase_back_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-14 17:00:11', 'U001', '管理员', '2020-06-14 17:02:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('441', 'MC15921252103804886', '取消', 'BTN', 'MC15921223851143081', null, null, null, 'm_purchase_back_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-14 17:00:11', 'U001', '管理员', '2020-06-14 17:02:36', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('442', 'MC15921252329366024', '销售退货', 'FUNCTION', 'MC15921223851143081', '/prod/back/sale', null, 'el-icon-truck', null, 'views/prod/back/sale/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 17:00:33', 'U001', '管理员', '2020-07-10 17:19:01', null, null, 'CP01', 'U001', 'CP01', '2', '0');
INSERT INTO `sys_menu` VALUES ('443', 'MC15921257981917036', '销售退货', 'MENU', 'MC15921223012273018', '/prod/back/sale', null, 'el-icon-truck', null, 'views/prod/back/sale/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 17:09:58', 'U001', '管理员', '2020-07-10 17:18:58', null, null, 'CP01', 'U001', 'CP01', '2', '0');
INSERT INTO `sys_menu` VALUES ('445', 'MC15921258234308176', '销售退货编辑', 'FUNCTION', 'MC15921257981917036', '/prod/sale/back/edit', null, null, 'm_sale_back_edit', 'views/prod/sale/back/edit/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 17:10:23', 'U001', '管理员', '2020-07-01 10:54:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('446', 'MC15921258234306022', '销售退货详情', 'FUNCTION', 'MC15921257981917036', '/prod/sale/back/view', null, null, 'm_sale_back_view', 'views/prod/sale/back/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-14 17:10:24', 'U001', '管理员', '2020-07-01 10:54:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('447', 'MC15921258234319367', '删除', 'BTN', 'MC15921257981917036', null, null, null, 'm_sale_back_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-14 17:10:24', 'U001', '管理员', '2020-06-14 17:12:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('448', 'MC15921258234310156', '通过', 'BTN', 'MC15921257981917036', null, null, null, 'm_sale_back_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-14 17:10:24', 'U001', '管理员', '2020-06-14 17:12:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('449', 'MC15921258234319162', '取消', 'BTN', 'MC15921257981917036', null, null, null, 'm_sale_back_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-14 17:10:24', 'U001', '管理员', '2020-06-14 17:12:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('450', 'MC15921316489411035', '经营管理', 'MENU', '0', null, null, 'el-icon-link', null, null, null, 'APP15906275620845638', '1', '0', '2020-06-14 18:47:30', 'U001', '管理员', '2020-07-10 17:18:40', null, null, 'CP01', 'U001', 'CP01', '7', '0');
INSERT INTO `sys_menu` VALUES ('451', 'MC15921893585870021', '首页', 'FUNCTION', '0', null, null, null, null, null, null, 'APP15906275620845638', '1', '0', '2020-06-15 10:49:20', 'U001', '管理员', '2020-06-15 10:49:36', 'U001', '管理员', 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('452', 'MC15921899765822035', '对账管理', 'MENU', '0', null, null, 'el-icon-connection', null, null, null, 'APP15906275620845638', '1', '0', '2020-06-15 10:59:38', 'U001', '管理员', '2020-07-10 17:18:26', null, null, 'CP01', 'U001', 'CP01', '11', '0');
INSERT INTO `sys_menu` VALUES ('453', 'MC15921900582042021', '加工单', 'MENU', 'MC15914316095982777', '/prod/business/hand', null, 'el-icon-scissors', null, 'views/prod/business/index', null, 'APP15906275620845638', '1', '0', '2020-06-15 11:00:59', 'U001', '管理员', '2020-07-10 17:18:12', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('454', 'MC15922237358272458', '库存明细', 'MENU', 'MC15914315858413972', '/wms/detail', null, 'el-icon-s-grid', null, 'views/wms/detail/index', null, 'APP15906275620845638', '1', '0', '2020-06-15 20:22:16', 'U001', '管理员', '2020-07-10 17:17:57', null, null, 'CP01', 'U001', 'CP01', '4', '0');
INSERT INTO `sys_menu` VALUES ('455', 'MC15922252780760728', '订单变更', 'BTN', 'MC15922237358272458', null, null, null, 'detail_change', null, null, 'APP15906275620845638', '1', '0', '2020-06-15 20:47:59', 'U001', '管理员', '2020-06-15 20:48:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('461', 'MC15922347608569902', '加工对账', 'MENU', 'MC15921899765822035', '/finance/business', null, 'el-icon-bank-card', null, 'views/finance/business/index', null, 'APP15906275620845638', '1', '0', '2020-06-15 23:26:01', 'U001', '管理员', '2020-07-10 17:16:44', 'U001', '管理员', 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('462', 'MC15922347849784041', '销售对账', 'MENU', 'MC15921899765822035', '/finance/sale', null, 'el-icon-bank-card', null, 'views/finance/sale/index', null, 'APP15906275620845638', '1', '0', '2020-06-15 23:26:25', 'U001', '管理员', '2020-07-10 17:16:39', null, null, 'CP01', 'U001', 'CP01', '2', '0');
INSERT INTO `sys_menu` VALUES ('463', 'MC15922348302271666', '采购对账', 'MENU', 'MC15921899765822035', '/finance/purchase', null, 'el-icon-bank-card', null, 'views/finance/purchase/index', null, 'APP15906275620845638', '1', '0', '2020-06-15 23:27:11', 'U001', '管理员', '2020-07-10 17:16:36', null, null, 'CP01', 'U001', 'CP01', '3', '0');
INSERT INTO `sys_menu` VALUES ('464', 'MC15922348497253818', '付款单', 'MENU', 'MC15921899765822035', '/finance/bill/out', null, 'el-icon-money', null, 'views/finance/bill/out/index', null, 'APP15906275620845638', '1', '0', '2020-06-15 23:27:30', 'U001', '管理员', '2020-07-10 17:16:35', null, null, 'CP01', 'U001', 'CP01', '6', '0');
INSERT INTO `sys_menu` VALUES ('465', 'MC15922348671757139', '收款单', 'MENU', 'MC15921899765822035', '/finance/bill/in', null, 'el-icon-money', null, 'views/finance/bill/in/index', null, 'APP15906275620845638', '1', '0', '2020-06-15 23:27:48', 'U001', '管理员', '2020-07-10 17:16:22', null, null, 'CP01', 'U001', 'CP01', '5', '0');
INSERT INTO `sys_menu` VALUES ('467', 'MC15909235998434566', '用户编辑', 'BTN', 'MC1500107', null, null, null, 'manager_user_edit', null, null, 'APP15906275620845638', '1', '0', '2020-06-17 15:08:37', 'U001', '管理员', '2020-06-17 15:09:29', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('468', 'MC15923843262001622', '平台用户', 'MENU', 'MC9999', '/platform/user', null, 'el-icon-user', null, 'views/admin/platform/user/index', null, 'APP15906275620845638', '1', '0', '2020-06-17 16:58:46', 'U001', '管理员', '2020-07-10 17:17:38', null, null, 'CP01', 'U001', 'CP01', '5', '0');
INSERT INTO `sys_menu` VALUES ('469', 'MC15923845032869274', '新增', 'BTN', 'MC15923843262001622', null, null, null, 'platform_user_password', null, null, 'APP15906275620845638', '1', '0', '2020-06-17 17:01:44', 'U001', '管理员', '2020-06-17 17:03:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('470', 'MC15923845032862383', '编辑', 'BTN', 'MC15923843262001622', null, null, null, 'platform_user_edit', null, null, 'APP15906275620845638', '1', '0', '2020-06-17 17:01:44', 'U001', '管理员', '2020-06-17 17:02:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('471', 'MC15923845032871880', '查看', 'BTN', 'MC15923843262001622', null, null, null, 'platform_user_view', null, null, 'APP15906275620845638', '1', '0', '2020-06-17 17:01:44', 'U001', '管理员', '2020-06-17 17:02:39', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('475', 'MC15909329142622135', '用户编辑', 'BTN', 'MC15908451170658422', null, null, null, 'user_edit', null, null, 'APP15906275620845638', '1', '0', '2020-06-17 17:30:43', 'U001', '管理员', '2020-06-17 17:31:40', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('476', 'MC15924022844179029', '加工单扣款', 'FUNCTION', 'MC15921900582042021', '/prod/business/deduction', null, null, 'm_business_deduction', 'views/prod/business/deduction/index', null, 'APP15906275620845638', '1', '0', '2020-06-17 21:58:05', 'U001', '管理员', '2020-07-01 10:54:15', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('477', 'MC15924026744948821', '扣款单', 'MENU', 'MC15914316095982777', '/prod/deduction', null, 'el-icon-coin', null, 'views/prod/deduction/index', null, 'APP15906275620845638', '1', '0', '2020-06-17 22:04:35', 'U001', '管理员', '2020-07-10 17:17:18', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('479', 'MC15924028940811300', '出库编辑', 'BTN', 'MC15921900582042021', '/prod/business/edit/out', null, null, 'm_business_edit_out', 'views/prod/room/edit/out/index', null, 'APP15906275620845638', '1', '0', '2020-06-17 22:08:14', 'U001', '管理员', '2020-07-05 08:51:52', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('480', 'MC15924028940820346', '查看', 'BTN', 'MC15921900582042021', null, null, null, 'm_business_view', null, null, 'APP15906275620845638', '1', '0', '2020-06-17 22:08:14', 'U001', '管理员', '2020-06-17 22:11:58', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('481', 'MC15924028940830763', '删除', 'BTN', 'MC15921900582042021', null, null, null, 'm_business_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-17 22:08:14', 'U001', '管理员', '2020-06-17 22:11:31', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('482', 'MC15924028940832961', '通过', 'BTN', 'MC15921900582042021', null, null, null, 'm_business_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-17 22:08:14', 'U001', '管理员', '2020-06-17 22:11:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('483', 'MC15924028940848340', '取消', 'BTN', 'MC15921900582042021', null, null, null, 'm_business_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-17 22:08:15', 'U001', '管理员', '2020-06-17 22:11:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('484', 'MC15924032084191967', '新增', 'BTN', 'MC15924022844179029', null, null, null, 'business_deduction_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-17 22:13:29', 'U001', '管理员', '2020-06-17 22:13:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('485', 'MC15924032084203046', '删除', 'BTN', 'MC15924022844179029', null, null, null, 'business_deduction_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-17 22:13:29', 'U001', '管理员', '2020-06-17 22:14:30', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('486', 'MC15924032084215220', '删除', 'BTN', 'MC15924026744948821', null, null, null, 'deduction_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-17 22:13:29', 'U001', '管理员', '2020-06-17 22:14:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('490', 'MC15924055245110185', '生成对账单', 'BTN', 'MC15922347608569902', null, null, null, 'business_finance_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-17 22:52:05', 'U001', '管理员', '2020-06-17 22:52:05', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('492', 'MC15925329887220065', '调整单', 'FUNCTION', 'MC15922348671757139', '/fina/bill/adjust', null, null, 'bill_in_adjust', 'views/finance/bill/adjust/index', null, 'APP15906275620845638', '1', '0', '2020-06-19 10:16:28', 'U001', '管理员', '2020-06-19 10:46:56', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('493', 'MC15925329887230038', '对账单收款详情', 'FUNCTION', 'MC15922348671757139', '/fina/bill/view', null, null, 'bill_in_view', 'views/finance/bill/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-19 10:16:28', 'U001', '管理员', '2020-07-01 10:54:56', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('494', 'MC15925329887238500', '通过', 'BTN', 'MC15922348671757139', null, null, null, 'bill_in_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-19 10:16:28', 'U001', '管理员', '2020-06-19 10:17:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('495', 'MC15925329887239786', '取消', 'BTN', 'MC15922348671757139', null, null, null, 'bill_in_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-19 10:16:28', 'U001', '管理员', '2020-06-19 10:17:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('496', 'MC15925329887248610', '完成', 'BTN', 'MC15922348671757139', null, null, null, 'bill_in_finish', null, null, 'APP15906275620845638', '1', '0', '2020-06-19 10:16:28', 'U001', '管理员', '2020-06-19 10:18:05', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('498', 'MC15925342831144490', '调整单', 'FUNCTION', 'MC15922348497253818', '/fina/bill/adjust', null, null, 'bill_out_adjust', 'views/finance/bill/adjust/index', null, 'APP15906275620845638', '1', '0', '2020-06-19 10:38:03', 'U001', '管理员', '2020-06-19 10:46:57', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('499', 'MC15925342831158596', '对账单付款详情', 'FUNCTION', 'MC15922348497253818', '/fina/bill/view', null, null, 'bill_out_view', 'views/finance/bill/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-19 10:38:03', 'U001', '管理员', '2020-07-01 10:54:59', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('500', 'MC15925342831151023', '通过', 'BTN', 'MC15922348497253818', null, null, null, 'bill_out_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-19 10:38:03', 'U001', '管理员', '2020-06-19 10:40:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('501', 'MC15925342831166390', '取消', 'BTN', 'MC15922348497253818', null, null, null, 'bill_out_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-19 10:38:03', 'U001', '管理员', '2020-06-19 10:40:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('502', 'MC15925342831176058', '完成', 'BTN', 'MC15922348497253818', null, null, null, 'bill_out_finish', null, null, 'APP15906275620845638', '1', '0', '2020-06-19 10:38:03', 'U001', '管理员', '2020-06-19 10:40:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('503', 'MC15925624419880442', '新增', 'BTN', 'MC15925342831144490', null, null, null, 'adjuest_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-19 18:27:23', 'U001', '管理员', '2020-06-19 18:28:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('504', 'MC15925624419897014', '删除', 'BTN', 'MC15925342831144490', null, null, null, 'adjuest_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-19 18:27:23', 'U001', '管理员', '2020-06-19 18:28:34', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('505', 'MC15925624419898222', '新增', 'BTN', 'MC15925329887220065', null, null, null, 'adjuest_add', null, null, 'APP15906275620845638', '1', '0', '2020-06-19 18:27:23', 'U001', '管理员', '2020-06-19 18:28:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('506', 'MC15925624419906850', '删除', 'BTN', 'MC15925329887220065', null, null, null, 'adjuest_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-19 18:27:23', 'U001', '管理员', '2020-06-19 18:28:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('509', 'MC15926359011796169', '今日加工数量', 'BTN', 'MC15921893585870021', null, null, null, 'odayBusinessQuantity', null, null, 'APP15906275620845638', '1', '0', '2020-06-20 14:51:41', 'U001', '管理员', '2020-06-20 14:52:39', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('510', 'MC15926359011808718', '本月加工数量', 'BTN', 'MC15921893585870021', null, null, null, 'monthBusinessQuantity', null, null, 'APP15906275620845638', '1', '0', '2020-06-20 14:51:41', 'U001', '管理员', '2020-06-20 14:53:01', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('511', 'MC15926359011818364', '今日加工单', 'BTN', 'MC15921893585870021', null, null, null, 'businessGroup', null, null, 'APP15906275620845638', '1', '0', '2020-06-20 14:51:41', 'U001', '管理员', '2020-06-20 14:53:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('512', 'MC15926359011813018', '今日出入库', 'BTN', 'MC15921893585870021', null, null, null, 'wmsOrder', null, null, 'APP15906275620845638', '1', '0', '2020-06-20 14:51:41', 'U001', '管理员', '2020-06-20 14:53:36', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('515', 'MC15927146956194844', '加工完成', 'BTN', 'MC15920253458678475', null, null, null, 'business_finish', null, null, 'APP15906275620845638', '1', '0', '2020-06-21 12:44:55', 'U001', '管理员', '2020-06-29 11:09:43', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('516', 'MC15927147609593844', '开启加工单', 'BTN', 'MC15920253458678475', null, null, null, 'business_open', null, null, 'APP15906275620845638', '1', '0', '2020-06-21 12:46:00', 'U001', '管理员', '2020-06-29 11:09:43', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('517', 'MC15928305611622971', '加工退料', 'FUNCTION', 'MC15921900582042021', '/prod/business/back', null, null, 'm_business_back', 'views/prod/business/back/index', null, 'APP15906275620845638', '1', '0', '2020-06-22 20:56:01', 'U001', '管理员', '2020-07-01 10:55:08', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('518', 'MC15928823479311469', '加工退料新增', 'FUNCTION', 'MC15928305611622971', '/prod/business/back/add', null, null, 'business_back_add', 'views/prod/business/back/add/index', null, 'APP15906275620845638', '1', '0', '2020-06-23 11:19:07', 'U001', '管理员', '2020-07-01 10:55:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('520', 'MC15928823479326616', '加工退料详情', 'FUNCTION', 'MC15928305611622971', '/prod/business/back/view', null, null, 'business_back_view', 'views/prod/business/back/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-23 11:19:08', 'U001', '管理员', '2020-07-01 10:55:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('521', 'MC15928823479325682', '删除', 'BTN', 'MC15928305611622971', null, null, null, 'business_back_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-23 11:19:08', 'U001', '管理员', '2020-06-23 11:21:07', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('522', 'MC15928823479320857', '通过', 'BTN', 'MC15928305611622971', null, null, null, 'business_back_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-23 11:19:08', 'U001', '管理员', '2020-06-23 11:21:33', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('523', 'MC15928823479322281', '取消', 'BTN', 'MC15928305611622971', null, null, null, 'business_back_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-23 11:19:08', 'U001', '管理员', '2020-06-23 11:21:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('524', 'MC15932607554425045', '订单采购', 'FUNCTION', 'MC15919800812908111', '/prod/purchase', null, null, 'order_purchase', 'views/prod/purchase/index', null, 'APP15906275620845638', '1', '0', '2020-06-27 20:25:56', 'U001', '管理员', '2020-07-01 10:55:32', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('525', 'MC15920253458678475', '订单生产', 'FUNCTION', 'MC15920253458676691', '/prod/business', null, null, 'business_prod', 'views/prod/room/business/index', null, 'APP15906275620845638', '1', '0', '2020-06-29 09:32:23', 'U001', '管理员', '2020-07-01 10:55:41', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('526', 'MC15934039718447633', '采购入库', 'FUNCTION', 'MC15915232094752126', '/prod/purchase/in', null, null, 'purchase_in', 'views/prod/purchase/in/index', null, 'APP15906275620845638', '1', '0', '2020-06-29 12:12:55', 'U001', '管理员', '2020-07-01 10:56:01', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('527', 'MC15934044040795979', '采购入库新增', 'FUNCTION', 'MC15934039718447633', '/prod/purchase/in/add', null, null, 'purchase_in_add', 'views/prod/purchase/in/add/index', null, 'APP15906275620845638', '1', '0', '2020-06-29 12:20:07', 'U001', '管理员', '2020-07-01 10:56:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('528', 'MC15934044040804990', '采购入库编辑', 'FUNCTION', 'MC15934039718447633', '/prod/purchase/in/edit', null, null, 'purchase_in_edit', 'views/prod/purchase/in/edit/index', null, 'APP15906275620845638', '1', '0', '2020-06-29 12:20:07', 'U001', '管理员', '2020-07-01 10:56:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('529', 'MC15934044040806399', '采购入库详情', 'FUNCTION', 'MC15934039718447633', '/prod/purchase/in/view', null, null, 'purchase_in_view', 'views/prod/purchase/in/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-29 12:20:07', 'U001', '管理员', '2020-07-01 10:56:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('530', 'MC15934044040810715', '删除', 'BTN', 'MC15934039718447633', null, null, null, 'purchase_in_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-29 12:20:07', 'U001', '管理员', '2020-06-29 12:21:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('531', 'MC15934044040812606', '通过', 'BTN', 'MC15934039718447633', null, null, null, 'purchase_in_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-29 12:20:07', 'U001', '管理员', '2020-06-29 12:21:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('532', 'MC15934044040826457', '取消', 'BTN', 'MC15934039718447633', null, null, null, 'purchase_in_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-29 12:20:07', 'U001', '管理员', '2020-06-29 12:21:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('533', 'MC2020063009096992', '完成', 'BTN', 'MC15915232094752126', null, null, null, 'purchase_finish', null, null, 'APP15906275620845638', '1', '0', '2020-06-30 09:09:24', 'U001', '管理员', '2020-06-30 09:09:24', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('534', 'MC2020063009092171', '完成', 'BTN', 'MC15916716796507619', null, null, null, 'sale_finish', null, null, 'APP15906275620845638', '1', '0', '2020-06-30 09:09:49', 'U001', '管理员', '2020-06-30 10:34:19', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('535', 'MC2020063009113782', '销售出库', 'FUNCTION', 'MC15916716796507619', '/prod/sale/out', null, null, 'sale_out', 'views/prod/sale/out/index', null, 'APP15906275620845638', '1', '0', '2020-06-30 09:11:02', 'U001', '管理员', '2020-07-01 10:56:22', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('536', 'MC2020063009114959', '销售出库新增', 'FUNCTION', 'MC2020063009113782', '/prod/sale/out/add', null, null, 'sale_out_add', 'views/prod/sale/out/add/index', null, 'APP15906275620845638', '1', '0', '2020-06-30 09:11:34', 'U001', '管理员', '2020-07-01 10:56:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('537', 'MC2020063009115641', '销售出库编辑', 'FUNCTION', 'MC2020063009113782', '/prod/sale/out/edit', null, null, 'sale_out_edit', 'views/prod/sale/out/edit/index', null, 'APP15906275620845638', '1', '0', '2020-06-30 09:11:34', 'U001', '管理员', '2020-07-01 10:56:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('538', 'MC2020063009111428', '销售出库详情', 'FUNCTION', 'MC2020063009113782', '/prod/sale/out/view', null, null, 'sale_out_view', 'views/prod/sale/out/view/index', null, 'APP15906275620845638', '1', '0', '2020-06-30 09:11:34', 'U001', '管理员', '2020-07-01 10:56:34', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('539', 'MC2020063009114699', '删除', 'BTN', 'MC2020063009113782', null, null, null, 'sale_out_delete', null, null, 'APP15906275620845638', '1', '0', '2020-06-30 09:11:34', 'U001', '管理员', '2020-06-30 09:12:33', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('540', 'MC2020063009110795', '通过', 'BTN', 'MC2020063009113782', null, null, null, 'sale_out_pass', null, null, 'APP15906275620845638', '1', '0', '2020-06-30 09:11:34', 'U001', '管理员', '2020-06-30 09:12:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('541', 'MC2020063009110683', '取消', 'BTN', 'MC2020063009113782', null, null, null, 'sale_out_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-06-30 09:11:34', 'U001', '管理员', '2020-06-30 09:12:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('542', 'MC2020070416305326', '订单对账', 'MENU', 'MC15921899765822035', '/finance/order', null, 'el-icon-coin', null, 'views/finance/order/index', null, 'APP15906275620845638', '1', '0', '2020-07-04 16:31:02', 'U001', '管理员', '2020-07-10 17:17:14', null, null, 'CP01', 'U001', 'CP01', '4', '0');
INSERT INTO `sys_menu` VALUES ('543', 'MC2020070417220548', '生成对账单', 'BTN', 'MC15922347849784041', null, null, null, 'sale_finance_add', null, null, 'APP15906275620845638', '1', '0', '2020-07-04 17:22:59', 'U001', '管理员', '2020-07-04 17:24:11', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('544', 'MC2020070417238247', '生成对账单', 'BTN', 'MC15922348302271666', null, null, null, 'purchase_finance_add', null, null, 'APP15906275620845638', '1', '0', '2020-07-04 17:23:25', 'U001', '管理员', '2020-07-04 17:23:25', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('545', 'MC2020070417238156', '生成对账单', 'BTN', 'MC2020070416305326', null, null, null, 'order_finance_add', null, null, 'APP15906275620845638', '1', '0', '2020-07-04 17:23:44', 'U001', '管理员', '2020-07-04 17:23:44', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('546', 'MC2020070419091430', '唛头', 'BTN', 'MC15914318899150370', '', null, null, 'customer_title', '', null, 'APP15906275620845638', '1', '0', '2020-07-04 19:09:32', 'U001', '管理员', '2020-07-04 20:30:50', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('547', 'MC15920267763314983', '入库编辑', 'FUNCTION', 'MC15920253458678475', '/prod/business/edit/in', '', '', 'business_in_edit', 'views/prod/room/edit/in/index', '', 'APP15906275620845638', '1', '0', '2020-06-13 13:39:38', 'U001', '管理员', '2020-07-05 08:36:56', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('548', 'MC15924028940811300', '入库编辑', 'BTN', 'MC15921900582042021', '/prod/business/edit/in', '', '', 'm_business_edit_in', 'views/prod/room/edit/in/index', '', 'APP15906275620845638', '1', '0', '2020-06-17 22:08:14', 'U001', '管理员', '2020-07-05 08:51:04', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('549', 'MC2020070512212828', '加工入库', 'MENU', '0', '/prod/quick', null, 'el-icon-crop', null, 'views/prod/quick/index', null, 'APP15906275620845638', '1', '0', '2020-07-05 12:21:40', 'U001', '管理员', '2020-07-10 17:17:03', 'U001', '管理员', 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('550', 'MC2020070514571879', '验布', 'FUNCTION', 'MC2020070512212828', '/prod/quick/view', null, null, 'business_quick', 'views/prod/quick/view/index', null, 'APP15906275620845638', '1', '0', '2020-07-05 14:57:18', 'U001', '管理员', '2020-07-05 14:57:18', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('551', 'MC15925342831144432', '付款单', 'FUNCTION', 'MC15922348497253818', '/fina/bill/pay', '', '', 'bill_out_pay', 'views/finance/bill/pay/index', '', 'APP15906275620845638', '1', '0', '2020-06-19 10:38:03', 'U001', '管理员', '2020-07-05 22:19:37', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('552', 'MC15925329887220078', '付款单', 'FUNCTION', 'MC15922348671757139', '/fina/bill/pay', '', '', 'bill_in_pay', 'views/finance/bill/pay/index', '', 'APP15906275620845638', '1', '0', '2020-06-19 10:16:28', 'U001', '管理员', '2020-07-05 22:19:22', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('553', 'MC120811', '职位变更', 'BTN', 'MC15908451170658422', '', '', '', 'user_position', '', '', 'APP15906275620845638', '1', '0', '2020-05-31 22:32:55', 'U001', '管理员', '2020-05-31 22:33:43', '', '', 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('554', 'MC15926359011793435', '对账统计', 'BTN', 'MC15921893585870021', '', '', '', 'billQuantity', '', '', 'APP15906275620845638', '1', '0', '2020-06-20 14:51:41', 'U001', '管理员', '2020-06-20 14:52:39', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('555', 'MC2020070810225471', '收款汇总', 'MENU', 'MC15921899765822035', '/fina/bill/count/in', null, 'el-icon-s-data', null, 'views/finance/count/in/index', null, 'APP15906275620845638', '1', '0', '2020-07-08 10:22:51', 'U001', '管理员', '2020-07-10 17:15:59', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('556', 'MC2020070810230903', '付款汇总', 'MENU', 'MC15921899765822035', '/fina/bill/count/out', null, 'el-icon-s-data', null, 'views/finance/count/out/index', null, 'APP15906275620845638', '1', '0', '2020-07-08 10:23:33', 'U001', '管理员', '2020-07-10 17:15:56', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('557', 'MC2020070811353519', '模板配置', 'MENU', 'MC15914313350266378', '/base/gtemplate', null, 'el-icon-document', null, 'views/base/gtemplate/index', null, 'APP15906275620845638', '1', '0', '2020-07-08 11:35:19', 'U001', '管理员', '2020-07-10 17:15:05', 'U001', '管理员', 'CP01', 'U001', 'CP01', '18', '0');
INSERT INTO `sys_menu` VALUES ('558', 'MC2020070811352500', '新增', 'BTN', 'MC2020070811353519', null, null, null, 'gtemplate_add', null, null, 'APP15906275620845638', '1', '0', '2020-07-08 11:35:36', 'U001', '管理员', '2020-07-08 11:37:34', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('559', 'MC2020070811358723', '编辑', 'BTN', 'MC2020070811353519', null, null, null, 'gtemplate_edit', null, null, 'APP15906275620845638', '1', '0', '2020-07-08 11:35:36', 'U001', '管理员', '2020-07-08 11:37:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('560', 'MC2020070811350468', '查看', 'BTN', 'MC2020070811353519', null, null, null, 'gtemplate_view', null, null, 'APP15906275620845638', '1', '0', '2020-07-08 11:35:36', 'U001', '管理员', '2020-07-08 11:37:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('562', 'MC2020070811354276', '开启', 'BTN', 'MC2020070811353519', null, null, null, 'gtemplate_open', null, null, 'APP15906275620845638', '1', '0', '2020-07-08 11:35:36', 'U001', '管理员', '2020-07-08 11:38:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('563', 'MC2020070811354124', '关闭', 'BTN', 'MC2020070811353519', null, null, null, 'gtemplate_close', null, null, 'APP15906275620845638', '1', '0', '2020-07-08 11:35:36', 'U001', '管理员', '2020-07-08 11:38:07', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('564', 'MC2020070817011822', '报表管理', 'MENU', '0', null, null, 'el-icon-s-data', null, null, null, 'APP15906275620845638', '1', '0', '2020-07-08 17:01:14', 'U001', '管理员', '2020-07-10 17:11:44', null, null, 'CP01', 'U001', 'CP01', '23', '0');
INSERT INTO `sys_menu` VALUES ('565', 'MC2020070817030493', '员工产量', 'MENU', 'MC2020070817011822', '/report/business/quantity', null, 'el-icon-s-operation', null, 'views/report/business/quantity/index', null, 'APP15906275620845638', '1', '0', '2020-07-08 17:03:46', 'U001', '管理员', '2020-07-10 17:13:05', null, null, 'CP01', 'U001', 'CP01', '1', '0');
INSERT INTO `sys_menu` VALUES ('567', 'MC2020070915196946', '订单进度', 'MENU', 'MC2020070817011822', '/report/order/view', null, 'el-icon-s-unfold', null, 'views/report/order/view/index', null, 'APP15906275620845638', '1', '0', '2020-07-09 15:19:31', 'U001', '管理员', '2020-07-10 17:12:38', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('568', 'MC2020071118420217', '客户产品', 'MENU', 'MC2020070817011822', '/report/order/goods', null, 'el-icon-s-unfold', null, 'views/report/order/goods/index', null, 'APP15906275620845638', '1', '0', '2020-07-11 18:42:47', 'U001', '管理员', '2020-07-11 18:59:05', null, null, 'CP01', 'U001', 'CP01', '3', '0');
INSERT INTO `sys_menu` VALUES ('569', 'MC2020071209384806', '采购扣款', 'FUNCTION', 'MC15915232094752126', '/prod/purchase/deduction', null, null, 'purchase_deduction', 'views/prod/purchase/deduction/index', null, 'APP15906275620845638', '1', '0', '2020-07-12 09:38:56', 'U001', '管理员', '2020-07-12 09:38:56', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('570', 'MC2020071209438390', '新增扣款', 'BTN', 'MC2020071209384806', null, null, null, 'purchase_deduction_add', null, null, 'APP15906275620845638', '1', '0', '2020-07-12 09:43:10', 'U001', '管理员', '2020-07-12 09:43:10', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('571', 'MC2020071209439126', '删除扣款', 'BTN', 'MC2020071209384806', null, null, null, 'purchase_deduction_delete', null, null, 'APP15906275620845638', '1', '0', '2020-07-12 09:43:40', 'U001', '管理员', '2020-07-12 09:43:40', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('572', 'MC2020071414267723', '序列号', 'MENU', 'MC2020070817011822', '/report/business/number', null, 'el-icon-s-operation', null, 'views/report/business/number/index', null, 'APP15906275620845638', '1', '0', '2020-07-14 14:26:38', 'U001', '管理员', '2020-07-14 14:26:56', null, null, 'CP01', 'U001', 'CP01', '5', '0');
INSERT INTO `sys_menu` VALUES ('573', 'MC2020071714284721', '异常配置', 'MENU', 'MC15914316095982777', '/prod/error', null, 'el-icon-s-operation', null, 'views/prod/error/index', null, 'APP15906275620845638', '1', '0', '2020-07-17 14:28:34', 'U001', '管理员', '2020-07-17 15:37:49', null, null, 'CP01', 'U001', 'CP01', '9', '0');
INSERT INTO `sys_menu` VALUES ('574', 'MC2020071714287021', '新增', 'BTN', 'MC2020071714284721', null, null, null, 'error_add', null, null, 'APP15906275620845638', '1', '0', '2020-07-17 14:28:57', 'U001', '管理员', '2020-07-17 14:30:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('575', 'MC2020071714280856', '编辑', 'BTN', 'MC2020071714284721', null, null, null, 'error_edit', null, null, 'APP15906275620845638', '1', '0', '2020-07-17 14:28:57', 'U001', '管理员', '2020-07-17 14:30:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('576', 'MC2020071714289709', '查看', 'BTN', 'MC2020071714284721', null, null, null, 'error_view', null, null, 'APP15906275620845638', '1', '0', '2020-07-17 14:28:57', 'U001', '管理员', '2020-07-17 14:30:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('577', 'MC2020071714281238', '删除', 'BTN', 'MC2020071714284721', null, null, null, 'error_delete', null, null, 'APP15906275620845638', '1', '0', '2020-07-17 14:28:57', 'U001', '管理员', '2020-07-17 14:30:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('578', 'MC2020071714281876', '开启', 'BTN', 'MC2020071714284721', null, null, null, 'error_open', null, null, 'APP15906275620845638', '1', '0', '2020-07-17 14:28:57', 'U001', '管理员', '2020-07-17 14:30:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('579', 'MC2020071714281226', '关闭', 'BTN', 'MC2020071714284721', null, null, null, 'error_close', null, null, 'APP15906275620845638', '1', '0', '2020-07-17 14:28:57', 'U001', '管理员', '2020-07-17 14:30:39', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('580', 'MC2020072310571467', '我的待办', 'MENU', '0', '/sys/work', null, 'el-icon-s-operation', null, 'views/admin/work/index', '', 'APP15906275620845638', '1', '0', '2020-07-23 10:57:06', 'U001', '管理员', '2020-07-23 11:15:51', 'U001', '管理员', 'CP01', 'U001', 'CP01', '28', '0');
INSERT INTO `sys_menu` VALUES ('581', 'MC2020072310576862', '新增', 'BTN', 'MC2020072310571467', null, null, null, 'work_add', null, null, 'APP15906275620845638', '1', '0', '2020-07-23 10:57:28', 'U001', '管理员', '2020-07-23 11:01:39', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('582', 'MC2020072310574166', '编辑', 'BTN', 'MC2020072310571467', null, null, null, 'work_edit', null, null, 'APP15906275620845638', '1', '0', '2020-07-23 10:57:28', 'U001', '管理员', '2020-07-23 11:01:42', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('583', 'MC2020072310572410', '查看', 'BTN', 'MC2020072310571467', null, null, null, 'work_view', null, null, 'APP15906275620845638', '1', '0', '2020-07-23 10:57:28', 'U001', '管理员', '2020-07-23 11:01:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('584', 'MC2020072310577709', '删除', 'BTN', 'MC2020072310571467', null, null, null, 'work_delete', null, null, 'APP15906275620845638', '1', '0', '2020-07-23 10:57:28', 'U001', '管理员', '2020-07-23 11:01:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('585', 'MC2020072310578120', '开启', 'BTN', 'MC2020072310571467', null, null, null, 'work_open', null, null, 'APP15906275620845638', '1', '0', '2020-07-23 10:57:28', 'U001', '管理员', '2020-07-23 11:01:52', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('586', 'MC2020072310572905', '关闭', 'BTN', 'MC2020072310571467', null, null, null, 'work_close', null, null, 'APP15906275620845638', '1', '0', '2020-07-23 10:57:28', 'U001', '管理员', '2020-07-23 11:01:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('587', 'MC2020072714480934', '预付款', 'MENU', '0', null, null, 'el-icon-pie-chart', null, null, null, 'APP15906275620845638', '1', '0', '2020-07-27 14:48:37', 'U001', '管理员', '2020-07-27 15:24:48', null, null, 'CP01', 'U001', 'CP01', '12', '0');
INSERT INTO `sys_menu` VALUES ('588', 'MC2020072714498612', '订单预付款', 'MENU', 'MC2020072714480934', '/fina/advance/order', null, 'el-icon-files', null, 'views/finance/advance/order/index', null, 'APP15906275620845638', '1', '0', '2020-07-27 14:49:26', 'U001', '管理员', '2020-07-27 15:24:50', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('589', 'MC2020072715255693', '新增', 'BTN', 'MC2020072714498612', null, null, null, 'advance_order_add', null, null, 'APP15906275620845638', '1', '0', '2020-07-27 15:25:22', 'U001', '管理员', '2020-07-27 15:25:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('590', 'MC2020072715252387', '编辑', 'BTN', 'MC2020072714498612', null, null, null, 'advance_order_edit', null, null, 'APP15906275620845638', '1', '0', '2020-07-27 15:25:22', 'U001', '管理员', '2020-07-27 15:25:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('591', 'MC2020072715256263', '查看', 'BTN', 'MC2020072714498612', null, null, null, 'advance_order_view', null, null, 'APP15906275620845638', '1', '0', '2020-07-27 15:25:22', 'U001', '管理员', '2020-07-27 15:25:52', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('592', 'MC2020072715255567', '删除', 'BTN', 'MC2020072714498612', null, null, null, 'advance_order_delete', null, null, 'APP15906275620845638', '1', '0', '2020-07-27 15:25:22', 'U001', '管理员', '2020-07-27 15:25:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('595', 'MC2020072717258929', '取消', 'BTN', 'MC15919800812908111', null, null, null, 'order_cancel', null, null, 'APP15906275620845638', '1', '0', '2020-07-27 17:25:15', 'U001', '管理员', '2020-07-27 17:25:15', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('596', 'MC15924028943847563', '单价', 'BTN', 'MC15921900582042021', null, null, null, 'm_business_price', null, null, 'APP15906275620845638', '1', '0', '2020-07-29 08:45:05', 'U001', '管理员', '2020-07-29 08:45:57', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('597', 'MC2020063009034235', '开启', 'BTN', 'MC15915232094752126', '', '', '', 'purchase_open', '', '', 'APP15906275620845638', '1', '0', '2020-06-30 09:09:24', 'U001', '管理员', '2020-06-30 09:09:24', '', '', 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('598', 'MC2020063009093422', '开启', 'BTN', 'MC15916716796507619', '', '', '', 'sale_open', '', '', 'APP15906275620845638', '1', '0', '2020-06-30 09:09:49', 'U001', '管理员', '2020-06-30 10:34:19', '', '', 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('599', 'MC2020073009059709', '测试', 'MENU', '0', '/test/table', null, null, null, 'views/test/table/index', null, 'APP15906275620845638', '0', '1', '2020-07-30 09:05:40', 'U001', '管理员', '2020-07-30 11:08:43', 'U001', '管理员', 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('600', 'MC15924028940836453', '打印', 'FUNCTION', 'MC15921900582042021', '/prod/business/print', '', '', 'm_business_print', 'views/prod/business/print/index', '', 'APP15906275620845638', '1', '0', '2020-06-17 22:08:14', 'U001', '管理员', '2020-08-01 16:48:24', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('601', 'MC2020080916512106', '唛头图片', 'MENU', 'MC15914313350266378', '/base/logo', null, 'el-icon-picture', null, 'views/base/logo/index', null, 'APP15906275620845638', '1', '0', '2020-08-09 16:51:18', 'U001', '管理员', '2020-08-09 17:34:51', null, null, 'CP01', 'U001', 'CP01', '9', '0');
INSERT INTO `sys_menu` VALUES ('602', 'MC2020072714493228', '采购预付款', 'MENU', 'MC2020072714480934', '/fina/advance/purchase', '', 'el-icon-files', '', 'views/finance/advance/purchase/index', '', 'APP15906275620845638', '1', '0', '2020-07-27 14:49:26', 'U001', '管理员', '2020-07-27 15:24:50', '', '', 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('603', 'MC2020081719254186', '新增', 'BTN', 'MC2020072714493228', null, null, null, 'advance_purchase_add', null, null, 'APP15906275620845638', '1', '0', '2020-08-17 19:25:41', 'U001', '管理员', '2020-08-17 19:26:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('604', 'MC2020081719251588', '编辑', 'BTN', 'MC2020072714493228', null, null, null, 'advance_purchase_edit', null, null, 'APP15906275620845638', '1', '0', '2020-08-17 19:25:41', 'U001', '管理员', '2020-08-17 19:26:47', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('605', 'MC2020081719258477', '查看', 'BTN', 'MC2020072714493228', null, null, null, 'advance_purchase_view', null, null, 'APP15906275620845638', '1', '0', '2020-08-17 19:25:41', 'U001', '管理员', '2020-08-17 19:26:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('606', 'MC2020081719259804', '删除', 'BTN', 'MC2020072714493228', null, null, null, 'advance_purchase_delete', null, null, 'APP15906275620845638', '1', '0', '2020-08-17 19:25:41', 'U001', '管理员', '2020-08-17 19:26:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('609', 'MC15915321692403245', '价格', 'FUNCTION', 'MC15915232094752126', '/prod/purchase/price', null, null, 'purchase_price', 'views/prod/purchase/price/index', null, 'APP15906275620845638', '1', '0', '2020-08-20 19:31:59', 'U001', '管理员', '2020-08-20 19:35:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('610', 'MC2020082415038095', '采购入库', 'MENU', 'MC2020070817011822', '/report/purchase/in', null, 'el-icon-s-unfold', null, 'views/report/purchase/in/index', '6', 'APP15906275620845638', '1', '0', '2020-08-24 15:03:33', 'U001', '管理员', '2020-08-24 15:08:41', null, null, 'CP01', 'U001', 'CP01', '0', '0');
INSERT INTO `sys_menu` VALUES ('613', 'MC15925329887248111', '結算', 'BTN', 'MC15922348671757139', '', '', '', 'bill_in_settlement', '', '', 'APP15906275620845638', '1', '0', '2020-06-19 10:16:28', 'U001', '管理员', '2020-09-07 19:11:19', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('614', 'MC15925329887248113', '結清', 'BTN', 'MC15922348671757139', '', '', '', 'bill_in_settle', '', '', 'APP15906275620845638', '1', '0', '2020-06-19 10:16:28', 'U001', '管理员', '2020-09-07 19:11:27', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('615', 'MC15925329887248111', '结算', 'BTN', 'MC15922348497253818', '', '', '', 'bill_out_settlement', '', '', 'APP15906275620845638', '1', '0', '2020-06-19 10:16:28', 'U001', '管理员', '2020-09-07 19:16:20', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('616', 'MC15925329887248623', '结清', 'BTN', 'MC15922348497253818', '', '', '', 'bill_out_settle', '', '', 'APP15906275620845638', '1', '0', '2020-06-19 10:16:28', 'U001', '管理员', '2020-09-07 19:16:23', '', '', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_menu` VALUES ('617', 'MC2020091220371930', '订单生产(订单)', 'FUNCTION', 'MC15920253458676691', 'prod/order/business', null, null, 'business_prod', 'views/prod/room/order/business/index', null, 'APP15906275620845638', '1', '0', '2020-09-12 20:37:20', 'U001', '管理员', '2020-09-12 20:42:45', null, null, 'CP01', 'U001', 'CP01', '0', '0');

-- ----------------------------
-- Table structure for sys_op_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_op_log`;
CREATE TABLE `sys_op_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名称',
  `name` varchar(50) DEFAULT NULL COMMENT '功能名称',
  `op_time` datetime DEFAULT NULL COMMENT '操作时间',
  `params` varchar(2000) DEFAULT NULL COMMENT '设备IP',
  `result` text COMMENT '操作结果',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户操作日志';

-- ----------------------------
-- Records of sys_op_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_organization
-- ----------------------------
DROP TABLE IF EXISTS `sys_organization`;
CREATE TABLE `sys_organization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `company_code` varchar(50) DEFAULT NULL COMMENT '公司编码',
  `dept_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `dept_name` varchar(50) DEFAULT NULL COMMENT '组织名称',
  `parent_dept_code` varchar(50) DEFAULT '0' COMMENT '上级组织',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of sys_organization
-- ----------------------------
INSERT INTO `sys_organization` VALUES ('1', 'CP01', 'CP01.001', '研发部', 'CP01', '1', '0', '2020-06-06 14:19:08', 'U001', '管理员', '2020-07-20 15:24:50', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_organization` VALUES ('2', 'CP01', 'CP01.001.001', '研发1组', 'CP01.001', '1', '0', '2020-06-06 14:55:25', 'U001', '管理员', '2020-06-17 17:47:17', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_organization` VALUES ('3', 'CP01', 'CP01.001.202006091750', '测试组', 'CP01.001', '1', '0', '2020-06-09 19:52:30', 'U001', '管理员', '2020-06-17 17:47:49', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_organization` VALUES ('4', 'CO15907458659568425', 'CO15907458659568425.202006160933', '生产车间', 'CO15907458659568425', '1', '0', '2020-06-16 22:28:06', 'UC15907513805251726', '测试1', '2020-06-16 22:28:06', null, null, 'CO15907458659568425', 'UC15907513805251726', 'CO15907458659568425', null, '0');
INSERT INTO `sys_organization` VALUES ('5', 'CO15907458659434343', 'CO15907458659434343.202006193397', '业务部', 'CO15907458659434343', '1', '0', '2020-06-19 20:15:28', 'UC15909249151008102', '沈括', '2020-06-19 20:15:28', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_organization` VALUES ('6', 'CO15907458659434343', 'CO15907458659434343.202006192713', '总经理办公室', 'CO15907458659434343', '1', '0', '2020-06-19 20:15:44', 'UC15909249151008102', '沈括', '2020-06-19 20:15:44', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_organization` VALUES ('7', 'CO15907458659434343', 'CO15907458659434343.202006190171', '生产部', 'CO15907458659434343', '1', '0', '2020-06-19 20:15:51', 'UC15909249151008102', '沈括', '2020-06-19 20:15:51', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_organization` VALUES ('8', 'CO15907458659434343', 'CO15907458659434343.202006199653', '仓管部', 'CO15907458659434343', '1', '0', '2020-06-19 20:16:00', 'UC15909249151008102', '沈括', '2020-06-19 20:16:00', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_organization` VALUES ('9', 'CO15907202006218096', 'CO15907202006218096.202006213065', '生产车间', 'CO15907202006218096', '0', '0', '2020-06-21 20:59:51', 'UC15927410664004948', '默认测试管理员', '2020-06-22 09:04:22', 'UC15927410664004948', '默认测试管理员', 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_organization` VALUES ('10', 'CO15907202006218096', 'CO15907202006218096.202006219688', '业务部', 'CO15907202006218096', '1', '0', '2020-06-21 21:00:00', 'UC15927410664004948', '默认测试管理员', '2020-06-21 21:00:00', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');

-- ----------------------------
-- Table structure for sys_position
-- ----------------------------
DROP TABLE IF EXISTS `sys_position`;
CREATE TABLE `sys_position` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `position_code` varchar(50) DEFAULT NULL COMMENT '职位编码',
  `position_name` varchar(50) DEFAULT NULL COMMENT '职位名称',
  `data_auth` varchar(50) DEFAULT NULL COMMENT '数据权限',
  `useble` int(50) DEFAULT '1' COMMENT '是否可以选择',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_position_code` (`position_code`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='用户菜单关联';

-- ----------------------------
-- Records of sys_position
-- ----------------------------
INSERT INTO `sys_position` VALUES ('2', 'ADMIN', '超级管理员', 'ALL', '0', '1', '0', '2020-05-27 15:15:22', 'U001', '管理员', '2020-05-29 19:32:01', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_position` VALUES ('3', 'PO15905719115748897', '公司管理员', 'COMPANY', '1', '1', '0', '2020-05-27 17:31:50', 'U001', '管理员', '2020-05-29 19:32:04', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_position` VALUES ('6', 'PO15909156557699431', '部门', 'ORG', '1', '1', '0', '2020-05-31 17:00:56', 'U001', '管理员', '2020-05-31 19:27:00', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_position` VALUES ('7', 'PO15909164323563155', '个人', 'PERSON', '1', '1', '0', '2020-05-31 17:13:53', 'U001', '管理员', '2020-05-31 17:13:53', null, null, 'CP01', 'U001', 'CP01', null, '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_code` varchar(50) DEFAULT NULL COMMENT '角色编码',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `app_code` varchar(50) DEFAULT NULL COMMENT '所属应用',
  `can_delete` int(4) DEFAULT '1' COMMENT '是否删除  1:能 0:不能',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('3', 'RC15908395234096057', '默认角色', 'APP15906275620845638', '1', '1', '0', '2020-05-30 19:52:05', 'U001', '管理员', '2020-05-30 22:00:40', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_role` VALUES ('4', 'RC15908468490378516', '仓管员', 'APP15906275620845638', '1', '1', '0', '2020-05-30 21:54:09', 'U001', '管理员', '2020-06-15 23:33:32', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_role` VALUES ('5', 'RC15909313876831863', '财务', 'APP15906275620845638', '1', '1', '1', '2020-05-31 21:23:08', 'U001', '管理员', '2020-07-20 15:29:42', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_role` VALUES ('6', 'RC15914341140759909', 'BOSS', 'APP15906275620845638', '1', '1', '0', '2020-06-06 17:01:54', 'U001', '管理员', '2020-06-15 23:33:47', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_role` VALUES ('7', 'RC15923163039369960', '管理员', 'APP15906275620845638', '1', '1', '0', '2020-06-16 22:05:03', 'U001', '管理员', '2020-06-16 22:05:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_role` VALUES ('8', 'RC15924835338893088', '仓配', 'APP15906275620845638', '1', '1', '0', '2020-06-18 20:32:13', 'UC15909249151008102', '沈括', '2020-06-18 20:32:13', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_role` VALUES ('9', 'RC15924835338897665', '业务员', 'APP15906275620845638', '1', '1', '0', '2020-06-18 20:32:13', 'UC15909249151008102', '沈括', '2020-06-19 09:03:52', 'UC15909249151008102', '沈括', 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_role` VALUES ('10', 'RC15924898278157941', '内部角色', 'APP15906275620845638', '1', '1', '0', '2020-06-18 22:17:07', 'U001', '管理员', '2020-06-19 20:11:10', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_role` VALUES ('11', 'RC15927460495237915', '画句号吧', 'APP15906275620845638', '1', '1', '0', '2020-06-21 21:27:29', 'UC15927410664004948', '默认测试管理员', '2020-06-21 21:27:29', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_role` VALUES ('12', 'RC15934176876604822', '11', 'APP15906275620845638', '1', '1', '0', '2020-06-29 16:01:27', 'UC15927410664004948', '默认测试管理员', '2020-06-29 16:01:27', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_role` VALUES ('13', 'RC15934176876636650', '11', 'APP15906275620845638', '1', '1', '0', '2020-06-29 16:01:27', 'UC15927410664004948', '默认测试管理员', '2020-06-29 16:01:27', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');
INSERT INTO `sys_role` VALUES ('14', 'RC2020070522267928', '验布员', 'APP15906275620845638', '1', '1', '0', '2020-07-05 22:26:07', 'UC15909249151008102', '沈括', '2020-07-05 22:26:07', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名称',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `sex` varchar(50) DEFAULT NULL COMMENT '性别',
  `id_card` varchar(50) DEFAULT NULL COMMENT '身份证',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='用户基本信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'U001', '管理员', '13867135741', null, null, null, null, '1', '0', '2020-05-27 12:34:14', 'U001', '管理员', '2020-05-27 12:34:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user` VALUES ('2', 'UC15907509667996591', '席金喜', '13800000001', null, null, null, null, '1', '0', '2020-05-29 19:16:07', 'U001', '管理员', '2020-05-29 19:16:07', 'UC15909249151008102', '沈括', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user` VALUES ('3', 'UC15907513805251726', '吴启迪', '13800000002', '1', null, '0', null, '1', '0', '2020-05-29 19:23:00', 'U001', '管理员', '2020-05-29 19:23:00', 'UC15907513805251726', '吴启迪', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user` VALUES ('4', 'UC15907517235671133', '称得上', '13800000003', null, null, null, null, '1', '0', '2020-05-29 19:28:44', 'U001', '管理员', '2020-05-29 19:28:44', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user` VALUES ('5', 'UC15909249151008102', '沈括', '13800000008', null, null, null, null, '1', '0', '2020-05-31 19:35:16', 'U001', '管理员', '2020-05-31 19:35:16', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user` VALUES ('6', 'UC15909253368333363', '胡金喜', '13800000009', null, null, null, null, '1', '0', '2020-05-31 19:42:17', 'U001', '管理员', '2020-05-31 19:42:17', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user` VALUES ('7', 'UC15909322708421901', '奥巴马', '13800000011', null, null, null, null, '1', '0', '2020-05-31 21:37:51', 'U001', '管理员', '2020-05-31 21:37:51', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user` VALUES ('8', 'UC15914288383436686', '张三', '13811111101', null, null, null, null, '1', '0', '2020-06-06 15:33:58', 'U001', '管理员', '2020-06-06 15:33:58', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user` VALUES ('9', 'UC15914292533829386', '鹿晗', '12700000123', null, null, null, null, '1', '0', '2020-06-06 15:40:53', 'U001', '管理员', '2020-06-06 15:40:53', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user` VALUES ('10', 'UC15925759605520930', '王浩强', '13800000110', null, '2020-07-27', '1', null, '1', '0', '2020-06-19 22:12:40', 'UC15909249151008102', '沈括', '2020-06-19 22:12:40', 'UC15925759605520930', '王浩强', 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user` VALUES ('11', 'UC15927410664004948', '默认测试管理员', '17700000000', null, null, null, null, '1', '0', '2020-06-21 20:04:26', 'U001', '管理员', '2020-06-21 20:04:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user` VALUES ('12', 'UC15927411606850587', '17700000001', '17700000001', null, null, null, null, '1', '0', '2020-06-21 20:06:00', 'U001', '管理员', '2020-06-21 20:06:00', 'UC15927410664004948', '默认测试管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user` VALUES ('13', 'UC2020070614320764', '18800000008', '18800000008', null, null, null, null, '1', '0', '2020-07-06 14:32:52', 'UC15909249151008102', '沈括', '2020-07-06 14:32:52', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');

-- ----------------------------
-- Table structure for sys_user_company
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_company`;
CREATE TABLE `sys_user_company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `comapny_code` varchar(50) DEFAULT NULL COMMENT '公司编码',
  `dept_code` varchar(200) DEFAULT NULL COMMENT '组织编码 当用户属于公司 org_code是空',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(255) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='用户公司关联表';

-- ----------------------------
-- Records of sys_user_company
-- ----------------------------
INSERT INTO `sys_user_company` VALUES ('1', 'U001', 'CP01', 'CP01', '1', '0', '2020-05-29 15:46:48', null, null, '2020-05-29 15:46:48', null, null, null, null, null, null, '0');
INSERT INTO `sys_user_company` VALUES ('2', 'UC15907513805251726', 'CO15907458659568425', 'CO15907458659568425', '1', '0', '2020-05-29 19:23:01', 'U001', '管理员', '2020-05-29 19:29:01', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_company` VALUES ('3', 'UC15907517235671133', 'CO15907458659568425', 'CO15907458659568425', '1', '0', '2020-05-29 19:28:44', 'U001', '管理员', '2020-05-29 19:28:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_company` VALUES ('4', 'U001', 'CO15907458659568425', 'CO15907458659568425', '1', '0', '2020-05-29 19:29:45', 'U001', '管理员', '2020-05-29 19:29:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_company` VALUES ('5', 'UC15909249151008102', 'CP01', 'CP01', '0', '0', '2020-05-31 19:35:16', 'U001', '管理员', '2020-06-18 20:30:28', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_company` VALUES ('6', 'UC15909253368333363', 'CP01', 'CP01', '1', '0', '2020-05-31 19:42:18', 'U001', '管理员', '2020-05-31 22:36:56', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_company` VALUES ('7', 'UC15909249151008102', 'CO15907458659434343', 'CO15907458659434343', '1', '0', '2020-05-31 21:21:34', 'U001', '管理员', '2020-06-18 18:12:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_company` VALUES ('8', 'UC15909322708421901', 'CP01|4578', 'CP01|4578', '1', '0', '2020-05-31 21:37:52', 'U001', '管理员', '2020-06-18 18:12:42', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_company` VALUES ('9', 'UC15914288383436686', 'CP01', 'CP01.001', '1', '0', '2020-06-06 15:33:58', 'U001', '管理员', '2020-07-20 15:36:19', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_company` VALUES ('10', 'UC15914292533829386', 'CP01', 'CP01.001.001', '1', '0', '2020-06-06 15:40:53', 'U001', '管理员', '2020-06-06 15:40:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_company` VALUES ('11', 'UC15909322708421901', 'CO15907458659434343', 'CO15907458659434343.202006193397', '0', '0', '2020-06-19 22:11:33', 'UC15909249151008102', '沈括', '2020-06-19 22:12:17', 'UC15909249151008102', '沈括', 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_company` VALUES ('12', 'UC15925759605520930', 'CO15907458659434343', 'CO15907458659434343.202006193397', '1', '0', '2020-06-19 22:12:40', 'UC15909249151008102', '沈括', '2020-06-19 22:12:40', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_company` VALUES ('13', 'UC15927410664004948', 'CO15907202006218096', 'CO15907202006218096', '1', '0', '2020-06-21 20:04:26', 'U001', '管理员', '2020-06-21 20:06:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_company` VALUES ('14', 'UC15927411606850587', 'CO15907202006218096', 'CO15907202006218096', '1', '0', '2020-06-21 20:06:00', 'U001', '管理员', '2020-06-21 20:06:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_company` VALUES ('15', 'UC15909253368333363', 'CO15907458659434343', 'CO15907458659434343.202006190171', '1', '0', '2020-07-05 22:28:33', 'UC15909249151008102', '沈括', '2020-07-05 22:31:16', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_company` VALUES ('16', 'UC15907509667996591', 'CO15907458659434343', 'CO15907458659434343.202006190171', '1', '0', '2020-07-05 22:29:49', 'UC15909249151008102', '沈括', '2020-07-05 22:31:20', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_company` VALUES ('17', 'UC2020070614320764', 'CO15907458659434343', 'CO15907458659434343.202006190171', '1', '0', '2020-07-06 14:32:53', 'UC15909249151008102', '沈括', '2020-07-06 14:32:53', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');

-- ----------------------------
-- Table structure for sys_user_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_info`;
CREATE TABLE `sys_user_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编号',
  `name` varchar(50) DEFAULT NULL COMMENT '属性名称',
  `prop` varchar(50) DEFAULT NULL COMMENT '属性名',
  `value` varchar(500) DEFAULT NULL COMMENT '属性值',
  `company_code` varchar(200) DEFAULT NULL COMMENT '公司编码',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_bms_process_data_business_code` (`user_code`),
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1651599 DEFAULT CHARSET=utf8 COMMENT='用户扩展信息';

-- ----------------------------
-- Records of sys_user_info
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_menu`;
CREATE TABLE `sys_user_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_code` varchar(50) DEFAULT NULL COMMENT '角色编码',
  `menu_code` varchar(50) DEFAULT NULL COMMENT '菜单编码',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=349 DEFAULT CHARSET=utf8 COMMENT='用户菜单关联';

-- ----------------------------
-- Records of sys_user_menu
-- ----------------------------
INSERT INTO `sys_user_menu` VALUES ('1', 'RC15908395234096057', 'MC15908351493366943', '1', '0', '2020-05-30 21:21:35', 'U001', '管理员', '2020-05-30 21:21:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('2', 'RC15908395234096057', 'MC15907524736113995', '1', '0', '2020-05-30 21:22:15', 'U001', '管理员', '2020-05-30 21:22:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('3', 'RC15908395234096057', 'MC6', '1', '0', '2020-05-30 21:22:16', 'U001', '管理员', '2020-05-30 21:22:16', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('4', 'RC15908395234096057', 'MC7', '1', '0', '2020-05-30 21:22:16', 'U001', '管理员', '2020-05-30 21:22:16', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('5', 'RC15908395234096057', 'MC8', '1', '0', '2020-05-30 21:22:17', 'U001', '管理员', '2020-05-30 21:22:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('6', 'RC15914341140759909', 'MC9999', '1', '0', '2020-06-06 17:02:02', 'U001', '管理员', '2020-06-06 17:02:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('7', 'RC15923163039369960', 'MC15921893585870021', '1', '0', '2020-06-16 22:05:11', 'U001', '管理员', '2020-06-16 22:05:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('8', 'RC15923163039369960', 'MC15908450124067007', '1', '0', '2020-06-16 22:05:16', 'U001', '管理员', '2020-06-16 22:05:16', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('9', 'RC15923163039369960', 'MC15914315858413972', '1', '0', '2020-06-16 22:05:19', 'U001', '管理员', '2020-06-16 22:05:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('10', 'RC15923163039369960', 'MC15914313350266378', '1', '0', '2020-06-16 22:05:19', 'U001', '管理员', '2020-06-16 22:05:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('11', 'RC15923163039369960', 'MC15914316095982777', '1', '0', '2020-06-16 22:05:19', 'U001', '管理员', '2020-06-16 22:05:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('12', 'RC15923163039369960', 'MC15921316489411035', '1', '0', '2020-06-16 22:05:20', 'U001', '管理员', '2020-06-16 22:05:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('13', 'RC15923163039369960', 'MC15921223012273018', '1', '0', '2020-06-16 22:05:21', 'U001', '管理员', '2020-06-16 22:05:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('14', 'RC15923163039369960', 'MC15921899765822035', '1', '0', '2020-06-16 22:05:21', 'U001', '管理员', '2020-06-16 22:05:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('15', 'RC15923163039369960', 'MC9999', '1', '1', '2020-06-16 22:09:25', 'U001', '管理员', '2020-06-16 22:10:07', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('16', 'RC15923163039369960', 'MC15908351493366943', '1', '1', '2020-06-16 22:09:25', 'U001', '管理员', '2020-06-16 22:10:12', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('17', 'RC15923163039369960', 'MC15909130927808478', '1', '1', '2020-06-16 22:09:26', 'U001', '管理员', '2020-06-16 22:10:12', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('18', 'RC15923163039369960', 'MC15907524736113995', '1', '1', '2020-06-16 22:09:27', 'U001', '管理员', '2020-06-16 22:10:13', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('19', 'RC15923163039369960', 'MC15908451170658422', '1', '0', '2020-06-16 22:10:01', 'U001', '管理员', '2020-06-16 22:10:01', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('20', 'RC15923163039369960', 'MC15908451836374862', '1', '0', '2020-06-16 22:10:02', 'U001', '管理员', '2020-06-16 22:10:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('21', 'RC15923163039369960', 'MC15909317203776339', '1', '0', '2020-06-16 22:10:02', 'U001', '管理员', '2020-06-16 22:10:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('22', 'RC15923163039369960', 'MC15914128374246222', '1', '0', '2020-06-16 22:10:03', 'U001', '管理员', '2020-06-16 22:10:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('23', 'RC15923163039369960', 'MC15909329142621828', '1', '0', '2020-06-16 22:10:21', 'U001', '管理员', '2020-06-16 22:10:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('24', 'RC15923163039369960', 'MC110811', '1', '0', '2020-06-16 22:10:22', 'U001', '管理员', '2020-06-16 22:10:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('25', 'RC15923163039369960', 'MC110611', '1', '0', '2020-06-16 22:10:23', 'U001', '管理员', '2020-06-16 22:10:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('26', 'RC15923163039369960', 'MC110711', '1', '0', '2020-06-16 22:10:23', 'U001', '管理员', '2020-06-16 22:10:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('27', 'RC15923163039369960', 'MC15901', '1', '0', '2020-06-16 22:10:33', 'U001', '管理员', '2020-06-16 22:10:33', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('28', 'RC15923163039369960', 'MC15902', '1', '0', '2020-06-16 22:10:34', 'U001', '管理员', '2020-06-16 22:10:34', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('29', 'RC15923163039369960', 'MC15903', '1', '0', '2020-06-16 22:10:35', 'U001', '管理员', '2020-06-16 22:10:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('30', 'RC15923163039369960', 'MC15904', '1', '0', '2020-06-16 22:10:36', 'U001', '管理员', '2020-06-16 22:10:36', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('31', 'RC15923163039369960', 'MC15905', '1', '0', '2020-06-16 22:10:36', 'U001', '管理员', '2020-06-16 22:10:36', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('32', 'RC15923163039369960', 'MC15906', '1', '0', '2020-06-16 22:10:37', 'U001', '管理员', '2020-06-16 22:10:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('33', 'RC15923163039369960', 'MC15907', '1', '0', '2020-06-16 22:10:39', 'U001', '管理员', '2020-06-16 22:10:39', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('34', 'RC15923163039369960', 'MC15001071', '1', '0', '2020-06-16 22:11:25', 'U001', '管理员', '2020-06-16 22:11:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('35', 'RC15923163039369960', 'MC15001072', '1', '0', '2020-06-16 22:11:25', 'U001', '管理员', '2020-06-16 22:11:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('36', 'RC15923163039369960', 'MC15001073', '1', '0', '2020-06-16 22:11:27', 'U001', '管理员', '2020-06-16 22:11:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('37', 'RC15923163039369960', 'MC15001074', '1', '0', '2020-06-16 22:11:27', 'U001', '管理员', '2020-06-16 22:11:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('38', 'RC15923163039369960', 'MC15001075', '1', '0', '2020-06-16 22:11:28', 'U001', '管理员', '2020-06-16 22:11:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('39', 'RC15923163039369960', 'MC15001076', '1', '0', '2020-06-16 22:11:29', 'U001', '管理员', '2020-06-16 22:11:29', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('40', 'RC15923163039369960', 'MC15001077', '1', '0', '2020-06-16 22:11:30', 'U001', '管理员', '2020-06-16 22:11:30', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('41', 'RC15923163039369960', 'MC1500107700051', '1', '0', '2020-06-16 22:11:34', 'U001', '管理员', '2020-06-16 22:11:34', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('42', 'RC15923163039369960', 'MC1500107700031', '1', '0', '2020-06-16 22:11:34', 'U001', '管理员', '2020-06-16 22:11:34', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('43', 'RC15923163039369960', 'MC1500107700021', '1', '0', '2020-06-16 22:11:35', 'U001', '管理员', '2020-06-16 22:11:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('44', 'RC15923163039369960', 'MC1500107700011', '1', '0', '2020-06-16 22:11:35', 'U001', '管理员', '2020-06-16 22:11:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('45', 'RC15923163039369960', 'MC150010777', '1', '0', '2020-06-16 22:11:46', 'U001', '管理员', '2020-06-16 22:11:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('46', 'RC15923163039369960', 'MC150010766', '1', '0', '2020-06-16 22:11:46', 'U001', '管理员', '2020-06-16 22:11:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('47', 'RC15923163039369960', 'MC150010755', '1', '0', '2020-06-16 22:11:47', 'U001', '管理员', '2020-06-16 22:11:47', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('48', 'RC15923163039369960', 'MC150010744', '1', '0', '2020-06-16 22:11:47', 'U001', '管理员', '2020-06-16 22:11:47', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('49', 'RC15923163039369960', 'MC150010733', '1', '0', '2020-06-16 22:11:48', 'U001', '管理员', '2020-06-16 22:11:48', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('50', 'RC15923163039369960', 'MC150010722', '1', '0', '2020-06-16 22:11:49', 'U001', '管理员', '2020-06-16 22:11:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('51', 'RC15923163039369960', 'MC150010721', '1', '0', '2020-06-16 22:11:50', 'U001', '管理员', '2020-06-16 22:11:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('52', 'RC15923163039369960', 'MC150010770001', '1', '0', '2020-06-16 22:11:54', 'U001', '管理员', '2020-06-16 22:11:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('53', 'RC15923163039369960', 'MC150010770002', '1', '0', '2020-06-16 22:11:54', 'U001', '管理员', '2020-06-16 22:11:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('54', 'RC15923163039369960', 'MC150010770003', '1', '0', '2020-06-16 22:11:55', 'U001', '管理员', '2020-06-16 22:11:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('55', 'RC15923163039369960', 'MC150010770005', '1', '0', '2020-06-16 22:11:55', 'U001', '管理员', '2020-06-16 22:11:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('56', 'RC15923163039369960', 'MC15914960797205706', '1', '0', '2020-06-16 22:12:06', 'U001', '管理员', '2020-06-16 22:12:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('57', 'RC15923163039369960', 'MC15914318899150370', '1', '0', '2020-06-16 22:12:07', 'U001', '管理员', '2020-06-16 22:12:07', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('58', 'RC15923163039369960', 'MC15914347262962224', '1', '0', '2020-06-16 22:12:07', 'U001', '管理员', '2020-06-16 22:12:07', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('59', 'RC15923163039369960', 'MC15914347513001853', '1', '0', '2020-06-16 22:12:08', 'U001', '管理员', '2020-06-16 22:12:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('60', 'RC15923163039369960', 'MC15914377245922261', '1', '0', '2020-06-16 22:12:08', 'U001', '管理员', '2020-06-16 22:12:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('61', 'RC15923163039369960', 'MC15914378160443006', '1', '0', '2020-06-16 22:12:09', 'U001', '管理员', '2020-06-16 22:12:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('62', 'RC15923163039369960', 'MC15914975002110092', '1', '0', '2020-06-16 22:12:16', 'U001', '管理员', '2020-06-16 22:12:16', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('63', 'RC15923163039369960', 'MC15914975002126324', '1', '0', '2020-06-16 22:12:17', 'U001', '管理员', '2020-06-16 22:12:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('64', 'RC15923163039369960', 'MC15914975002125159', '1', '0', '2020-06-16 22:12:17', 'U001', '管理员', '2020-06-16 22:12:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('65', 'RC15923163039369960', 'MC15914975002133170', '1', '0', '2020-06-16 22:12:18', 'U001', '管理员', '2020-06-16 22:12:18', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('66', 'RC15923163039369960', 'MC15914975002136386', '1', '0', '2020-06-16 22:12:18', 'U001', '管理员', '2020-06-16 22:12:18', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('67', 'RC15923163039369960', 'MC15914975002149169', '1', '0', '2020-06-16 22:12:19', 'U001', '管理员', '2020-06-16 22:12:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('68', 'RC15923163039369960', 'MC15914315', '1', '0', '2020-06-16 22:12:28', 'U001', '管理员', '2020-06-16 22:12:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('69', 'RC15923163039369960', 'MC15914313', '1', '0', '2020-06-16 22:12:29', 'U001', '管理员', '2020-06-16 22:12:29', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('70', 'RC15923163039369960', 'MC15914314', '1', '0', '2020-06-16 22:12:29', 'U001', '管理员', '2020-06-16 22:12:29', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('71', 'RC15923163039369960', 'MC15914312', '1', '0', '2020-06-16 22:12:30', 'U001', '管理员', '2020-06-16 22:12:30', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('72', 'RC15923163039369960', 'MC15914311', '1', '0', '2020-06-16 22:12:31', 'U001', '管理员', '2020-06-16 22:12:31', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('73', 'RC15923163039369960', 'MC1591434751', '1', '0', '2020-06-16 22:12:36', 'U001', '管理员', '2020-06-16 22:12:36', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('74', 'RC15923163039369960', 'MC1591434733', '1', '0', '2020-06-16 22:12:37', 'U001', '管理员', '2020-06-16 22:12:37', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('75', 'RC15923163039369960', 'MC1591434742', '1', '0', '2020-06-16 22:12:38', 'U001', '管理员', '2020-06-16 22:12:38', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('76', 'RC15923163039369960', 'MC1591434724', '1', '0', '2020-06-16 22:12:38', 'U001', '管理员', '2020-06-16 22:12:38', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('77', 'RC15923163039369960', 'MC1591434715', '1', '0', '2020-06-16 22:12:39', 'U001', '管理员', '2020-06-16 22:12:39', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('78', 'RC15923163039369960', 'MC159143471', '1', '0', '2020-06-16 22:12:43', 'U001', '管理员', '2020-06-16 22:12:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('79', 'RC15923163039369960', 'MC159143472', '1', '0', '2020-06-16 22:12:44', 'U001', '管理员', '2020-06-16 22:12:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('80', 'RC15923163039369960', 'MC159143473', '1', '0', '2020-06-16 22:12:45', 'U001', '管理员', '2020-06-16 22:12:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('81', 'RC15923163039369960', 'MC159143474', '1', '0', '2020-06-16 22:12:46', 'U001', '管理员', '2020-06-16 22:12:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('82', 'RC15923163039369960', 'MC159143475', '1', '0', '2020-06-16 22:12:47', 'U001', '管理员', '2020-06-16 22:12:47', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('83', 'RC15923163039369960', 'MC15914396277851780', '1', '0', '2020-06-16 22:13:00', 'U001', '管理员', '2020-06-16 22:13:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('84', 'RC15923163039369960', 'MC15914396277867698', '1', '0', '2020-06-16 22:13:00', 'U001', '管理员', '2020-06-16 22:13:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('85', 'RC15923163039369960', 'MC15914396277864836', '1', '0', '2020-06-16 22:13:00', 'U001', '管理员', '2020-06-16 22:13:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('86', 'RC15923163039369960', 'MC15914396277877159', '1', '0', '2020-06-16 22:13:00', 'U001', '管理员', '2020-06-16 22:13:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('87', 'RC15923163039369960', 'MC15914396277878832', '1', '0', '2020-06-16 22:13:01', 'U001', '管理员', '2020-06-16 22:13:01', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('88', 'RC15923163039369960', 'MC15914396277866900', '1', '0', '2020-06-16 22:13:01', 'U001', '管理员', '2020-06-16 22:13:01', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('89', 'RC15923163039369960', 'MC15914396355918880', '1', '0', '2020-06-16 22:13:05', 'U001', '管理员', '2020-06-16 22:13:05', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('90', 'RC15923163039369960', 'MC15914396355927442', '1', '0', '2020-06-16 22:13:06', 'U001', '管理员', '2020-06-16 22:13:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('91', 'RC15923163039369960', 'MC15914396356030248', '1', '0', '2020-06-16 22:13:06', 'U001', '管理员', '2020-06-16 22:13:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('92', 'RC15923163039369960', 'MC15914396356049376', '1', '0', '2020-06-16 22:13:07', 'U001', '管理员', '2020-06-16 22:13:07', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('93', 'RC15923163039369960', 'MC15914396356050761', '1', '0', '2020-06-16 22:13:08', 'U001', '管理员', '2020-06-16 22:13:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('94', 'RC15923163039369960', 'MC15915179885058020', '1', '0', '2020-06-16 22:13:19', 'U001', '管理员', '2020-06-16 22:13:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('95', 'RC15923163039369960', 'MC15915179468335964', '1', '0', '2020-06-16 22:13:20', 'U001', '管理员', '2020-06-16 22:13:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('96', 'RC15923163039369960', 'MC15915180351375063', '1', '0', '2020-06-16 22:13:21', 'U001', '管理员', '2020-06-16 22:13:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('97', 'RC15923163039369960', 'MC15922237358272458', '1', '0', '2020-06-16 22:13:21', 'U001', '管理员', '2020-06-16 22:13:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('98', 'RC15923163039369960', 'MC15915178876094091', '1', '0', '2020-06-16 22:13:21', 'U001', '管理员', '2020-06-16 22:13:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('99', 'RC15923163039369960', 'MC15922349398474926', '1', '0', '2020-06-16 22:13:22', 'U001', '管理员', '2020-06-16 22:13:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('100', 'RC15923163039369960', 'MC15917008822514155', '1', '0', '2020-06-16 22:13:26', 'U001', '管理员', '2020-06-16 22:13:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('101', 'RC15923163039369960', 'MC15917008822490563', '1', '0', '2020-06-16 22:13:26', 'U001', '管理员', '2020-06-16 22:13:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('102', 'RC15923163039369960', 'MC15917008822518563', '1', '0', '2020-06-16 22:13:26', 'U001', '管理员', '2020-06-16 22:13:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('103', 'RC15923163039369960', 'MC15917009549252282', '1', '0', '2020-06-16 22:13:31', 'U001', '管理员', '2020-06-16 22:13:31', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('104', 'RC15923163039369960', 'MC15917009549269061', '1', '0', '2020-06-16 22:13:33', 'U001', '管理员', '2020-06-16 22:13:33', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('105', 'RC15923163039369960', 'MC15917009549274710', '1', '0', '2020-06-16 22:13:33', 'U001', '管理员', '2020-06-16 22:13:33', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('106', 'RC15923163039369960', 'MC15919654354046452', '1', '0', '2020-06-16 22:14:03', 'U001', '管理员', '2020-06-16 22:14:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('107', 'RC15923163039369960', 'MC15919653933238573', '1', '0', '2020-06-16 22:14:03', 'U001', '管理员', '2020-06-16 22:14:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('108', 'RC15923163039369960', 'MC15922252780760728', '1', '0', '2020-06-16 22:14:08', 'U001', '管理员', '2020-06-16 22:14:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('109', 'RC15923163039369960', 'MC15915189355825373', '1', '0', '2020-06-16 22:14:17', 'U001', '管理员', '2020-06-16 22:14:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('110', 'RC15923163039369960', 'MC15915189355842974', '1', '0', '2020-06-16 22:14:18', 'U001', '管理员', '2020-06-16 22:14:18', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('111', 'RC15923163039369960', 'MC15915189355837642', '1', '0', '2020-06-16 22:14:18', 'U001', '管理员', '2020-06-16 22:14:18', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('112', 'RC15923163039369960', 'MC15915189355855177', '1', '0', '2020-06-16 22:14:19', 'U001', '管理员', '2020-06-16 22:14:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('113', 'RC15923163039369960', 'MC15915189355854014', '1', '0', '2020-06-16 22:14:19', 'U001', '管理员', '2020-06-16 22:14:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('114', 'RC15923163039369960', 'MC15920185456085838', '1', '0', '2020-06-16 22:14:43', 'U001', '管理员', '2020-06-16 22:14:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('115', 'RC15923163039369960', 'MC15920221446840895', '1', '0', '2020-06-16 22:14:44', 'U001', '管理员', '2020-06-16 22:14:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('116', 'RC15923163039369960', 'MC15921900582042021', '1', '0', '2020-06-16 22:14:44', 'U001', '管理员', '2020-06-16 22:14:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('117', 'RC15923163039369960', 'MC15917037605031945', '1', '1', '2020-06-16 22:15:21', 'U001', '管理员', '2020-06-16 22:15:22', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('118', 'RC15923163039369960', 'MC15920185513191809', '1', '0', '2020-06-16 22:15:25', 'U001', '管理员', '2020-06-16 22:15:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('119', 'RC15923163039369960', 'MC15920185513205158', '1', '0', '2020-06-16 22:15:26', 'U001', '管理员', '2020-06-16 22:15:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('120', 'RC15923163039369960', 'MC15920185513207648', '1', '0', '2020-06-16 22:15:27', 'U001', '管理员', '2020-06-16 22:15:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('121', 'RC15923163039369960', 'MC15920185513201150', '1', '0', '2020-06-16 22:15:27', 'U001', '管理员', '2020-06-16 22:15:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('122', 'RC15923163039369960', 'MC15920185513203661', '1', '0', '2020-06-16 22:15:28', 'U001', '管理员', '2020-06-16 22:15:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('123', 'RC15923163039369960', 'MC15920253458676691', '1', '0', '2020-06-16 22:15:34', 'U001', '管理员', '2020-06-16 22:15:34', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('124', 'RC15923163039369960', 'MC15920267763306406', '1', '0', '2020-06-16 22:15:42', 'U001', '管理员', '2020-06-16 22:15:42', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('125', 'RC15923163039369960', 'MC15920267763314983', '1', '0', '2020-06-16 22:15:45', 'U001', '管理员', '2020-06-16 22:15:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('126', 'RC15923163039369960', 'MC15920267763311136', '1', '0', '2020-06-16 22:15:46', 'U001', '管理员', '2020-06-16 22:15:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('127', 'RC15923163039369960', 'MC15920267763319955', '1', '0', '2020-06-16 22:15:46', 'U001', '管理员', '2020-06-16 22:15:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('128', 'RC15923163039369960', 'MC15920267763314662', '1', '0', '2020-06-16 22:15:46', 'U001', '管理员', '2020-06-16 22:15:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('129', 'RC15923163039369960', 'MC15920267763327980', '1', '0', '2020-06-16 22:15:47', 'U001', '管理员', '2020-06-16 22:15:47', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('130', 'RC15923163039369960', 'MC15920267763306487', '1', '0', '2020-06-16 22:15:47', 'U001', '管理员', '2020-06-16 22:15:47', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('131', 'RC15923163039369960', 'MC15920498177618661', '1', '0', '2020-06-16 22:15:58', 'U001', '管理员', '2020-06-16 22:15:58', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('132', 'RC15923163039369960', 'MC15915232094752126', '1', '0', '2020-06-16 22:15:59', 'U001', '管理员', '2020-06-16 22:15:59', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('133', 'RC15923163039369960', 'MC15919800812908111', '1', '0', '2020-06-16 22:16:00', 'U001', '管理员', '2020-06-16 22:16:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('134', 'RC15923163039369960', 'MC15916716796507619', '1', '0', '2020-06-16 22:16:00', 'U001', '管理员', '2020-06-16 22:16:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('135', 'RC15923163039369960', 'MC15920522992062998', '1', '0', '2020-06-16 22:16:02', 'U001', '管理员', '2020-06-16 22:16:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('136', 'RC15923163039369960', 'MC15920522992097016', '1', '0', '2020-06-16 22:16:03', 'U001', '管理员', '2020-06-16 22:16:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('137', 'RC15923163039369960', 'MC15920522992109187', '1', '0', '2020-06-16 22:16:03', 'U001', '管理员', '2020-06-16 22:16:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('138', 'RC15923163039369960', 'MC15920522992103836', '1', '0', '2020-06-16 22:16:04', 'U001', '管理员', '2020-06-16 22:16:04', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('139', 'RC15923163039369960', 'MC15920522992110418', '1', '0', '2020-06-16 22:16:06', 'U001', '管理员', '2020-06-16 22:16:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('140', 'RC15923163039369960', 'MC15920522992115783', '1', '0', '2020-06-16 22:16:06', 'U001', '管理员', '2020-06-16 22:16:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('141', 'RC15923163039369960', 'MC15915321692402826', '1', '0', '2020-06-16 22:16:12', 'U001', '管理员', '2020-06-16 22:16:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('142', 'RC15923163039369960', 'MC15915321692412802', '1', '0', '2020-06-16 22:16:12', 'U001', '管理员', '2020-06-16 22:16:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('143', 'RC15923163039369960', 'MC15915321692422274', '1', '0', '2020-06-16 22:16:13', 'U001', '管理员', '2020-06-16 22:16:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('144', 'RC15923163039369960', 'MC15915321692424233', '1', '0', '2020-06-16 22:16:13', 'U001', '管理员', '2020-06-16 22:16:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('145', 'RC15923163039369960', 'MC15915321692437765', '1', '0', '2020-06-16 22:16:15', 'U001', '管理员', '2020-06-16 22:16:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('146', 'RC15923163039369960', 'MC15915321692442326', '1', '0', '2020-06-16 22:16:15', 'U001', '管理员', '2020-06-16 22:16:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('147', 'RC15923163039369960', 'MC15921152571761824', '1', '0', '2020-06-16 22:16:15', 'U001', '管理员', '2020-06-16 22:16:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('148', 'RC15923163039369960', 'MC15920115067041821', '1', '0', '2020-06-16 22:16:21', 'U001', '管理员', '2020-06-16 22:16:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('149', 'RC15923163039369960', 'MC15920115067050841', '1', '0', '2020-06-16 22:16:21', 'U001', '管理员', '2020-06-16 22:16:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('150', 'RC15923163039369960', 'MC15920115067051144', '1', '0', '2020-06-16 22:16:22', 'U001', '管理员', '2020-06-16 22:16:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('151', 'RC15923163039369960', 'MC15920115067056351', '1', '0', '2020-06-16 22:16:23', 'U001', '管理员', '2020-06-16 22:16:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('152', 'RC15923163039369960', 'MC15920115067064690', '1', '0', '2020-06-16 22:16:23', 'U001', '管理员', '2020-06-16 22:16:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('153', 'RC15923163039369960', 'MC15920115067077920', '1', '0', '2020-06-16 22:16:24', 'U001', '管理员', '2020-06-16 22:16:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('154', 'RC15923163039369960', 'MC15920115067048746', '1', '0', '2020-06-16 22:16:26', 'U001', '管理员', '2020-06-16 22:16:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('155', 'RC15923163039369960', 'MC15920115067041234', '1', '0', '2020-06-16 22:16:28', 'U001', '管理员', '2020-06-16 22:16:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('156', 'RC15923163039369960', 'MC15920115067274665', '1', '0', '2020-06-16 22:16:28', 'U001', '管理员', '2020-06-16 22:16:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('157', 'RC15923163039369960', 'MC15921164457537340', '1', '0', '2020-06-16 22:16:41', 'U001', '管理员', '2020-06-16 22:16:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('158', 'RC15923163039369960', 'MC15921164457562033', '1', '0', '2020-06-16 22:16:41', 'U001', '管理员', '2020-06-16 22:16:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('159', 'RC15923163039369960', 'MC15921164457541357', '1', '0', '2020-06-16 22:16:41', 'U001', '管理员', '2020-06-16 22:16:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('160', 'RC15923163039369960', 'MC15921164457561106', '1', '0', '2020-06-16 22:16:41', 'U001', '管理员', '2020-06-16 22:16:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('161', 'RC15923163039369960', 'MC15921164457557435', '1', '0', '2020-06-16 22:16:41', 'U001', '管理员', '2020-06-16 22:16:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('162', 'RC15923163039369960', 'MC15921164457578070', '1', '0', '2020-06-16 22:16:41', 'U001', '管理员', '2020-06-16 22:16:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('163', 'RC15923163039369960', 'MC15916716869404703', '1', '0', '2020-06-16 22:16:48', 'U001', '管理员', '2020-06-16 22:16:48', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('164', 'RC15923163039369960', 'MC15916716869411013', '1', '0', '2020-06-16 22:16:49', 'U001', '管理员', '2020-06-16 22:16:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('165', 'RC15923163039369960', 'MC15916716869422895', '1', '0', '2020-06-16 22:16:54', 'U001', '管理员', '2020-06-16 22:16:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('166', 'RC15923163039369960', 'MC15916716869421760', '1', '0', '2020-06-16 22:16:54', 'U001', '管理员', '2020-06-16 22:16:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('167', 'RC15923163039369960', 'MC15916716869413817', '1', '0', '2020-06-16 22:16:54', 'U001', '管理员', '2020-06-16 22:16:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('168', 'RC15923163039369960', 'MC15916716869415256', '1', '0', '2020-06-16 22:16:54', 'U001', '管理员', '2020-06-16 22:16:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('169', 'RC15923163039369960', 'MC15921205758877845', '1', '0', '2020-06-16 22:16:54', 'U001', '管理员', '2020-06-16 22:16:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('170', 'RC15923163039369960', 'MC15921205929516074', '1', '0', '2020-06-16 22:16:55', 'U001', '管理员', '2020-06-16 22:16:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('171', 'RC15923163039369960', 'MC15921205929516152', '1', '0', '2020-06-16 22:16:55', 'U001', '管理员', '2020-06-16 22:16:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('172', 'RC15923163039369960', 'MC15921205929524036', '1', '0', '2020-06-16 22:16:56', 'U001', '管理员', '2020-06-16 22:16:56', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('173', 'RC15923163039369960', 'MC15921205929521456', '1', '0', '2020-06-16 22:16:56', 'U001', '管理员', '2020-06-16 22:16:56', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('174', 'RC15923163039369960', 'MC15921205929538857', '1', '0', '2020-06-16 22:16:57', 'U001', '管理员', '2020-06-16 22:16:57', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('175', 'RC15923163039369960', 'MC15921205929539894', '1', '0', '2020-06-16 22:16:58', 'U001', '管理员', '2020-06-16 22:16:58', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('176', 'RC15923163039369960', 'MC15921223851143081', '1', '0', '2020-06-16 22:17:09', 'U001', '管理员', '2020-06-16 22:17:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('177', 'RC15923163039369960', 'MC15921257981917036', '1', '0', '2020-06-16 22:17:10', 'U001', '管理员', '2020-06-16 22:17:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('178', 'RC15923163039369960', 'MC15921252103797314', '1', '0', '2020-06-16 22:17:15', 'U001', '管理员', '2020-06-16 22:17:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('179', 'RC15923163039369960', 'MC15921252103803582', '1', '0', '2020-06-16 22:17:15', 'U001', '管理员', '2020-06-16 22:17:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('180', 'RC15923163039369960', 'MC15921252103800066', '1', '0', '2020-06-16 22:17:15', 'U001', '管理员', '2020-06-16 22:17:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('181', 'RC15923163039369960', 'MC15921252103800038', '1', '0', '2020-06-16 22:17:15', 'U001', '管理员', '2020-06-16 22:17:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('182', 'RC15923163039369960', 'MC15921252103804886', '1', '0', '2020-06-16 22:17:16', 'U001', '管理员', '2020-06-16 22:17:16', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('183', 'RC15923163039369960', 'MC15921252329366024', '1', '0', '2020-06-16 22:17:17', 'U001', '管理员', '2020-06-16 22:17:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('184', 'RC15923163039369960', 'MC15921258234308176', '1', '0', '2020-06-16 22:17:23', 'U001', '管理员', '2020-06-16 22:17:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('185', 'RC15923163039369960', 'MC15921258234306022', '1', '0', '2020-06-16 22:17:23', 'U001', '管理员', '2020-06-16 22:17:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('186', 'RC15923163039369960', 'MC15921258234319367', '1', '0', '2020-06-16 22:17:24', 'U001', '管理员', '2020-06-16 22:17:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('187', 'RC15923163039369960', 'MC15921258234310156', '1', '0', '2020-06-16 22:17:25', 'U001', '管理员', '2020-06-16 22:17:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('188', 'RC15923163039369960', 'MC15921258234319162', '1', '0', '2020-06-16 22:17:25', 'U001', '管理员', '2020-06-16 22:17:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('189', 'RC15909313876831863', 'MC15921899765822035', '1', '0', '2020-06-16 22:17:48', 'U001', '管理员', '2020-06-16 22:17:48', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('190', 'RC15909313876831863', 'MC15921893585870021', '1', '0', '2020-06-16 22:17:50', 'U001', '管理员', '2020-06-16 22:17:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('191', 'RC15908468490378516', 'MC15914315858413972', '1', '0', '2020-06-16 22:18:09', 'U001', '管理员', '2020-06-16 22:18:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('192', 'RC15908468490378516', 'MC15915179885058020', '1', '0', '2020-06-16 22:18:10', 'U001', '管理员', '2020-06-16 22:18:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('193', 'RC15908468490378516', 'MC15915179468335964', '1', '0', '2020-06-16 22:18:11', 'U001', '管理员', '2020-06-16 22:18:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('194', 'RC15908468490378516', 'MC15915180351375063', '1', '0', '2020-06-16 22:18:12', 'U001', '管理员', '2020-06-16 22:18:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('195', 'RC15908468490378516', 'MC15922237358272458', '1', '0', '2020-06-16 22:18:13', 'U001', '管理员', '2020-06-16 22:18:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('196', 'RC15908468490378516', 'MC15915178876094091', '1', '0', '2020-06-16 22:18:19', 'U001', '管理员', '2020-06-16 22:18:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('197', 'RC15908468490378516', 'MC15922349398474926', '1', '0', '2020-06-16 22:18:20', 'U001', '管理员', '2020-06-16 22:18:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('198', 'RC15923163039369960', 'MC15922347608569902', '1', '0', '2020-06-18 20:24:21', 'U001', '管理员', '2020-06-18 20:24:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('199', 'RC15923163039369960', 'MC15922347849784041', '1', '0', '2020-06-18 20:24:21', 'U001', '管理员', '2020-06-18 20:24:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('200', 'RC15923163039369960', 'MC15922348302271666', '1', '0', '2020-06-18 20:24:22', 'U001', '管理员', '2020-06-18 20:24:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('201', 'RC15923163039369960', 'MC15922348497253818', '1', '0', '2020-06-18 20:24:23', 'U001', '管理员', '2020-06-18 20:24:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('202', 'RC15923163039369960', 'MC15922348671757139', '1', '0', '2020-06-18 20:24:24', 'U001', '管理员', '2020-06-18 20:24:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('203', 'RC15923163039369960', 'MC15924055245110185', '1', '0', '2020-06-18 20:24:27', 'U001', '管理员', '2020-06-18 20:24:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('204', 'RC15924835338893088', 'MC15921893585870021', '1', '0', '2020-06-18 20:50:03', 'UC15909249151008102', '沈括', '2020-06-18 20:50:03', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('205', 'RC15923163039369960', 'MC15915162486452432', '1', '0', '2020-06-18 22:37:22', 'U001', '管理员', '2020-06-18 22:37:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('206', 'RC15923163039369960', 'MC15915162486495390', '1', '0', '2020-06-18 22:37:23', 'U001', '管理员', '2020-06-18 22:37:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('207', 'RC15923163039369960', 'MC15915162486491770', '1', '0', '2020-06-18 22:37:24', 'U001', '管理员', '2020-06-18 22:37:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('208', 'RC15923163039369960', 'MC15915162486507564', '1', '0', '2020-06-18 22:37:24', 'U001', '管理员', '2020-06-18 22:37:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('209', 'RC15923163039369960', 'MC15915162486510267', '1', '0', '2020-06-18 22:37:25', 'U001', '管理员', '2020-06-18 22:37:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('210', 'RC15923163039369960', 'MC15924026744948821', '1', '0', '2020-06-18 22:37:38', 'U001', '管理员', '2020-06-18 22:37:38', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('211', 'RC15923163039369960', 'MC15924022844179029', '1', '0', '2020-06-18 22:37:43', 'U001', '管理员', '2020-06-18 22:37:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('212', 'RC15923163039369960', 'MC15924028940811300', '1', '0', '2020-06-18 22:37:44', 'U001', '管理员', '2020-06-18 22:37:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('213', 'RC15923163039369960', 'MC15924028940820346', '1', '0', '2020-06-18 22:37:44', 'U001', '管理员', '2020-06-18 22:37:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('214', 'RC15923163039369960', 'MC15924028940830763', '1', '0', '2020-06-18 22:37:45', 'U001', '管理员', '2020-06-18 22:37:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('215', 'RC15923163039369960', 'MC15924028940832961', '1', '0', '2020-06-18 22:37:45', 'U001', '管理员', '2020-06-18 22:37:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('216', 'RC15923163039369960', 'MC15924028940848340', '1', '0', '2020-06-18 22:37:46', 'U001', '管理员', '2020-06-18 22:37:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('217', 'RC15923163039369960', 'MC15920194069281756', '1', '0', '2020-06-18 22:43:48', 'U001', '管理员', '2020-06-18 22:43:48', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('218', 'RC15923163039369960', 'MC15920194069299345', '1', '0', '2020-06-18 22:43:49', 'U001', '管理员', '2020-06-18 22:43:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('219', 'RC15923163039369960', 'MC15920194069303363', '1', '0', '2020-06-18 22:43:49', 'U001', '管理员', '2020-06-18 22:43:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('220', 'RC15923163039369960', 'MC15920194069309352', '1', '0', '2020-06-18 22:43:50', 'U001', '管理员', '2020-06-18 22:43:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('221', 'RC15923163039369960', 'MC15920194069310445', '1', '0', '2020-06-18 22:43:50', 'U001', '管理员', '2020-06-18 22:43:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('222', 'RC15923163039369960', 'MC15920194069328818', '1', '0', '2020-06-18 22:43:52', 'U001', '管理员', '2020-06-18 22:43:52', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('223', 'RC15923163039369960', 'MC15925342831144490', '1', '0', '2020-06-19 20:08:15', 'U001', '管理员', '2020-06-19 20:08:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('224', 'RC15923163039369960', 'MC15925342831158596', '1', '0', '2020-06-19 20:08:15', 'U001', '管理员', '2020-06-19 20:08:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('225', 'RC15923163039369960', 'MC15925342831151023', '1', '0', '2020-06-19 20:08:16', 'U001', '管理员', '2020-06-19 20:08:16', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('226', 'RC15923163039369960', 'MC15925342831166390', '1', '0', '2020-06-19 20:08:17', 'U001', '管理员', '2020-06-19 20:08:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('227', 'RC15923163039369960', 'MC15925342831176058', '1', '0', '2020-06-19 20:08:18', 'U001', '管理员', '2020-06-19 20:08:18', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('228', 'RC15923163039369960', 'MC15925329887220065', '1', '0', '2020-06-19 20:08:22', 'U001', '管理员', '2020-06-19 20:08:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('229', 'RC15923163039369960', 'MC15925329887230038', '1', '0', '2020-06-19 20:08:23', 'U001', '管理员', '2020-06-19 20:08:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('230', 'RC15923163039369960', 'MC15925329887238500', '1', '0', '2020-06-19 20:08:23', 'U001', '管理员', '2020-06-19 20:08:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('231', 'RC15923163039369960', 'MC15925329887239786', '1', '0', '2020-06-19 20:08:24', 'U001', '管理员', '2020-06-19 20:08:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('232', 'RC15923163039369960', 'MC15925329887248610', '1', '0', '2020-06-19 20:08:24', 'U001', '管理员', '2020-06-19 20:08:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('233', 'RC15923163039369960', 'MC15924032084215220', '1', '0', '2020-06-19 20:11:44', 'U001', '管理员', '2020-06-19 20:11:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('234', 'RC15923163039369960', 'MC15924032084191967', '1', '0', '2020-06-19 20:12:52', 'U001', '管理员', '2020-06-19 20:12:52', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('235', 'RC15923163039369960', 'MC15924032084203046', '1', '0', '2020-06-19 20:12:53', 'U001', '管理员', '2020-06-19 20:12:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('236', 'RC15924835338897665', 'MC15921316489411035', '1', '0', '2020-06-19 22:38:15', 'UC15909249151008102', '沈括', '2020-06-19 22:38:15', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('237', 'RC15924835338897665', 'MC15919800812908111', '1', '0', '2020-06-19 22:38:21', 'UC15909249151008102', '沈括', '2020-06-19 22:38:21', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('238', 'RC15924835338897665', 'MC15916716796507619', '1', '0', '2020-06-19 22:38:33', 'UC15909249151008102', '沈括', '2020-06-19 22:38:33', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('239', 'RC15924835338897665', 'MC15916716869404703', '1', '0', '2020-06-19 22:38:37', 'UC15909249151008102', '沈括', '2020-06-19 22:38:37', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('240', 'RC15924835338897665', 'MC15916716869411013', '1', '0', '2020-06-19 22:38:38', 'UC15909249151008102', '沈括', '2020-06-19 22:38:38', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('241', 'RC15924835338897665', 'MC15916716869413817', '1', '0', '2020-06-19 22:38:39', 'UC15909249151008102', '沈括', '2020-06-19 22:38:39', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('242', 'RC15924835338897665', 'MC15916716869415256', '1', '0', '2020-06-19 22:38:40', 'UC15909249151008102', '沈括', '2020-06-19 22:38:40', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('243', 'RC15924835338897665', 'MC15916716869422895', '1', '1', '2020-06-19 22:38:41', 'UC15909249151008102', '沈括', '2020-06-19 22:56:25', 'UC15909249151008102', '沈括', 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('244', 'RC15924835338897665', 'MC15916716869421760', '1', '1', '2020-06-19 22:38:44', 'UC15909249151008102', '沈括', '2020-06-19 22:56:30', 'UC15909249151008102', '沈括', 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('245', 'RC15924835338897665', 'MC15921205758877845', '1', '0', '2020-06-19 22:38:45', 'UC15909249151008102', '沈括', '2020-06-19 22:38:45', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('246', 'RC15924835338897665', 'MC15916716869422895', '1', '1', '2020-06-19 22:38:57', 'UC15909249151008102', '沈括', '2020-06-19 22:56:25', 'UC15909249151008102', '沈括', 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('247', 'RC15924835338897665', 'MC15916716869422895', '1', '1', '2020-06-19 22:39:07', 'UC15909249151008102', '沈括', '2020-06-19 22:56:25', 'UC15909249151008102', '沈括', 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('248', 'RC15924835338897665', 'MC15916716869422895', '1', '1', '2020-06-19 22:56:25', 'UC15909249151008102', '沈括', '2020-06-19 22:56:25', 'UC15909249151008102', '沈括', 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('249', 'RC15924835338897665', 'MC15916716869422895', '1', '1', '2020-06-19 22:56:27', 'UC15909249151008102', '沈括', '2020-06-19 22:56:28', 'UC15909249151008102', '沈括', 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('250', 'RC15924835338897665', 'MC15916716869422895', '1', '0', '2020-06-19 22:56:29', 'UC15909249151008102', '沈括', '2020-06-19 22:56:29', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('251', 'RC15924835338897665', 'MC15920115067041821', '1', '0', '2020-06-19 22:56:45', 'UC15909249151008102', '沈括', '2020-06-19 22:56:45', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('252', 'RC15924835338897665', 'MC15920115067050841', '1', '0', '2020-06-19 22:56:45', 'UC15909249151008102', '沈括', '2020-06-19 22:56:45', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('253', 'RC15924835338897665', 'MC15920115067051144', '1', '0', '2020-06-19 22:56:46', 'UC15909249151008102', '沈括', '2020-06-19 22:56:46', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('254', 'RC15924835338897665', 'MC15920115067056351', '1', '0', '2020-06-19 22:56:48', 'UC15909249151008102', '沈括', '2020-06-19 22:56:48', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('255', 'RC15924835338897665', 'MC15914313350266378', '1', '0', '2020-06-20 08:21:35', 'UC15909249151008102', '沈括', '2020-06-20 08:21:35', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('256', 'RC15924835338897665', 'MC15914318899150370', '1', '0', '2020-06-20 08:21:38', 'UC15909249151008102', '沈括', '2020-06-20 08:21:38', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('257', 'RC15924835338897665', 'MC15914315', '1', '0', '2020-06-20 08:21:42', 'UC15909249151008102', '沈括', '2020-06-20 08:21:42', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('258', 'RC15924835338897665', 'MC15914314', '1', '0', '2020-06-20 08:21:42', 'UC15909249151008102', '沈括', '2020-06-20 08:21:42', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('259', 'RC15924835338897665', 'MC15914313', '1', '0', '2020-06-20 08:21:45', 'UC15909249151008102', '沈括', '2020-06-20 08:21:45', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('260', 'RC15924835338897665', 'MC15914312', '1', '0', '2020-06-20 08:21:46', 'UC15909249151008102', '沈括', '2020-06-20 08:21:46', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('261', 'RC15924835338897665', 'MC15914311', '1', '0', '2020-06-20 08:21:48', 'UC15909249151008102', '沈括', '2020-06-20 08:21:48', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('262', 'RC15923163039369960', 'MC15926359011796169', '1', '0', '2020-06-20 14:54:45', 'U001', '管理员', '2020-06-20 14:54:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('263', 'RC15923163039369960', 'MC15926359011808718', '1', '0', '2020-06-20 14:54:46', 'U001', '管理员', '2020-06-20 14:54:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('264', 'RC15923163039369960', 'MC15926359011818364', '1', '0', '2020-06-20 14:54:46', 'U001', '管理员', '2020-06-20 14:54:46', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('265', 'RC15923163039369960', 'MC15926359011813018', '1', '0', '2020-06-20 14:54:47', 'U001', '管理员', '2020-06-20 14:54:47', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('266', 'RC15923163039369960', 'MC15927146956194844', '1', '0', '2020-06-21 12:46:59', 'U001', '管理员', '2020-06-21 12:46:59', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('267', 'RC15923163039369960', 'MC15927147609593844', '1', '0', '2020-06-21 12:46:59', 'U001', '管理员', '2020-06-21 12:46:59', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('268', 'RC15923163039369960', 'MC15925624419880442', '1', '0', '2020-06-21 15:37:02', 'U001', '管理员', '2020-06-21 15:37:02', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('269', 'RC15923163039369960', 'MC15925624419897014', '1', '0', '2020-06-21 15:37:03', 'U001', '管理员', '2020-06-21 15:37:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('270', 'RC15923163039369960', 'MC15925624419898222', '1', '0', '2020-06-21 15:37:13', 'U001', '管理员', '2020-06-21 15:37:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('271', 'RC15923163039369960', 'MC15925624419906850', '1', '0', '2020-06-21 15:37:14', 'U001', '管理员', '2020-06-21 15:37:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('272', 'RC15923163039369960', 'MC15928305611622971', '1', '0', '2020-06-24 10:34:20', 'U001', '管理员', '2020-06-24 10:34:20', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('273', 'RC15923163039369960', 'MC15928823479311469', '1', '0', '2020-06-24 10:34:25', 'U001', '管理员', '2020-06-24 10:34:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('274', 'RC15923163039369960', 'MC15928823479326616', '1', '0', '2020-06-24 10:34:25', 'U001', '管理员', '2020-06-24 10:34:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('275', 'RC15923163039369960', 'MC15928823479325682', '1', '0', '2020-06-24 10:34:26', 'U001', '管理员', '2020-06-24 10:34:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('276', 'RC15923163039369960', 'MC15928823479320857', '1', '0', '2020-06-24 10:34:26', 'U001', '管理员', '2020-06-24 10:34:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('277', 'RC15923163039369960', 'MC15928823479322281', '1', '0', '2020-06-24 10:34:27', 'U001', '管理员', '2020-06-24 10:34:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('278', 'RC15923163039369960', 'MC15920253458678475', '1', '0', '2020-07-01 08:48:41', 'U001', '管理员', '2020-07-01 08:48:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('279', 'RC15923163039369960', 'MC15934039718447633', '1', '0', '2020-07-01 08:49:08', 'U001', '管理员', '2020-07-01 08:49:08', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('280', 'RC15923163039369960', 'MC2020063009096992', '1', '0', '2020-07-01 08:49:09', 'U001', '管理员', '2020-07-01 08:49:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('281', 'RC15923163039369960', 'MC15934044040795979', '1', '0', '2020-07-01 08:49:13', 'U001', '管理员', '2020-07-01 08:49:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('282', 'RC15923163039369960', 'MC15934044040804990', '1', '0', '2020-07-01 08:49:14', 'U001', '管理员', '2020-07-01 08:49:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('283', 'RC15923163039369960', 'MC15934044040806399', '1', '0', '2020-07-01 08:49:14', 'U001', '管理员', '2020-07-01 08:49:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('284', 'RC15923163039369960', 'MC15934044040810715', '1', '0', '2020-07-01 08:49:15', 'U001', '管理员', '2020-07-01 08:49:15', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('285', 'RC15923163039369960', 'MC15934044040812606', '1', '0', '2020-07-01 08:49:16', 'U001', '管理员', '2020-07-01 08:49:16', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('286', 'RC15923163039369960', 'MC15934044040826457', '1', '0', '2020-07-01 08:49:17', 'U001', '管理员', '2020-07-01 08:49:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('287', 'RC15923163039369960', 'MC2020063009092171', '1', '0', '2020-07-01 08:49:27', 'U001', '管理员', '2020-07-01 08:49:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('288', 'RC15923163039369960', 'MC2020063009113782', '1', '0', '2020-07-01 08:49:29', 'U001', '管理员', '2020-07-01 08:49:29', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('289', 'RC15923163039369960', 'MC2020063009114959', '1', '0', '2020-07-01 08:49:33', 'U001', '管理员', '2020-07-01 08:49:33', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('290', 'RC15923163039369960', 'MC2020063009115641', '1', '0', '2020-07-01 08:49:33', 'U001', '管理员', '2020-07-01 08:49:33', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('291', 'RC15923163039369960', 'MC2020063009111428', '1', '0', '2020-07-01 08:49:34', 'U001', '管理员', '2020-07-01 08:49:34', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('292', 'RC15923163039369960', 'MC2020063009114699', '1', '0', '2020-07-01 08:49:35', 'U001', '管理员', '2020-07-01 08:49:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('293', 'RC15923163039369960', 'MC2020063009110795', '1', '0', '2020-07-01 08:49:35', 'U001', '管理员', '2020-07-01 08:49:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('294', 'RC15923163039369960', 'MC2020063009110683', '1', '0', '2020-07-01 08:49:36', 'U001', '管理员', '2020-07-01 08:49:36', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('295', 'RC15923163039369960', 'MC15932607554425045', '1', '0', '2020-07-04 08:43:06', 'U001', '管理员', '2020-07-04 08:43:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('296', 'RC15923163039369960', 'MC2020070416305326', '1', '0', '2020-07-04 17:24:41', 'U001', '管理员', '2020-07-04 17:24:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('297', 'RC15923163039369960', 'MC2020070417238156', '1', '0', '2020-07-04 17:24:44', 'U001', '管理员', '2020-07-04 17:24:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('298', 'RC15923163039369960', 'MC2020070417220548', '1', '0', '2020-07-04 17:24:50', 'U001', '管理员', '2020-07-04 17:24:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('299', 'RC15923163039369960', 'MC2020070417238247', '1', '0', '2020-07-04 17:24:51', 'U001', '管理员', '2020-07-04 17:24:51', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('300', 'RC15923163039369960', 'MC2020070512212828', '1', '0', '2020-07-05 22:17:03', 'U001', '管理员', '2020-07-05 22:17:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('301', 'RC15923163039369960', 'MC2020070514571879', '1', '0', '2020-07-05 22:17:06', 'U001', '管理员', '2020-07-05 22:17:06', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('302', 'RC15923163039369960', 'MC9999', '1', '1', '2020-07-05 22:17:17', 'U001', '管理员', '2020-07-06 14:29:35', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('303', 'RC15923163039369960', 'MC15925329887220078', '1', '0', '2020-07-05 22:20:09', 'U001', '管理员', '2020-07-05 22:20:09', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('304', 'RC15923163039369960', 'MC15925342831144432', '1', '0', '2020-07-05 22:20:17', 'U001', '管理员', '2020-07-05 22:20:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('305', 'RC2020070522267928', 'MC2020070512212828', '1', '0', '2020-07-05 22:27:50', 'UC15909249151008102', '沈括', '2020-07-05 22:27:50', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('306', 'RC2020070522267928', 'MC2020070514571879', '1', '0', '2020-07-05 22:27:51', 'UC15909249151008102', '沈括', '2020-07-05 22:27:51', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_menu` VALUES ('307', 'RC15923163039369960', 'MC120811', '1', '0', '2020-07-07 16:43:12', 'U001', '管理员', '2020-07-07 16:43:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('308', 'RC15923163039369960', 'MC15909329142622135', '1', '0', '2020-07-07 16:43:28', 'U001', '管理员', '2020-07-07 16:43:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('309', 'RC15923163039369960', 'MC15926359011793435', '1', '0', '2020-07-08 10:07:27', 'U001', '管理员', '2020-07-08 10:07:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('310', 'RC15923163039369960', 'MC2020070810225471', '1', '0', '2020-07-08 10:23:52', 'U001', '管理员', '2020-07-08 10:23:52', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('311', 'RC15923163039369960', 'MC2020070810230903', '1', '0', '2020-07-08 10:23:53', 'U001', '管理员', '2020-07-08 10:23:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('312', 'RC15923163039369960', 'MC2020070811353519', '1', '0', '2020-07-08 13:52:11', 'U001', '管理员', '2020-07-08 13:52:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('313', 'RC15923163039369960', 'MC2020070811352500', '1', '0', '2020-07-08 13:52:11', 'U001', '管理员', '2020-07-08 13:52:11', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('314', 'RC15923163039369960', 'MC2020070811358723', '1', '0', '2020-07-08 13:52:12', 'U001', '管理员', '2020-07-08 13:52:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('315', 'RC15923163039369960', 'MC2020070811350468', '1', '0', '2020-07-08 13:52:12', 'U001', '管理员', '2020-07-08 13:52:12', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('316', 'RC15923163039369960', 'MC2020070811354276', '1', '0', '2020-07-08 13:52:13', 'U001', '管理员', '2020-07-08 13:52:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('317', 'RC15923163039369960', 'MC2020070811354124', '1', '0', '2020-07-08 13:52:13', 'U001', '管理员', '2020-07-08 13:52:13', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('318', 'RC15923163039369960', 'MC2020070419091430', '1', '0', '2020-07-08 14:27:35', 'U001', '管理员', '2020-07-08 14:27:35', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('319', 'RC15923163039369960', 'MC2020070817011822', '1', '0', '2020-07-08 17:04:31', 'U001', '管理员', '2020-07-08 17:04:31', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('320', 'RC15923163039369960', 'MC2020070817030493', '1', '0', '2020-07-08 17:04:32', 'U001', '管理员', '2020-07-08 17:04:32', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('321', 'RC15923163039369960', 'MC2020070915196946', '1', '0', '2020-07-09 15:27:50', 'U001', '管理员', '2020-07-09 15:27:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('322', 'RC15923163039369960', 'MC2020071118420217', '1', '0', '2020-07-14 15:35:27', 'U001', '管理员', '2020-07-14 15:35:27', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('323', 'RC15923163039369960', 'MC2020071414267723', '1', '0', '2020-07-14 17:25:10', 'U001', '管理员', '2020-07-14 17:25:10', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('324', 'RC15923163039369960', 'MC2020071714284721', '1', '0', '2020-07-17 15:37:19', 'U001', '管理员', '2020-07-17 15:37:19', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('325', 'RC15923163039369960', 'MC2020071714287021', '1', '0', '2020-07-17 15:37:22', 'U001', '管理员', '2020-07-17 15:37:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('326', 'RC15923163039369960', 'MC2020071714280856', '1', '0', '2020-07-17 15:37:23', 'U001', '管理员', '2020-07-17 15:37:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('327', 'RC15923163039369960', 'MC2020071714289709', '1', '0', '2020-07-17 15:37:23', 'U001', '管理员', '2020-07-17 15:37:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('328', 'RC15923163039369960', 'MC2020071714281238', '1', '0', '2020-07-17 15:37:24', 'U001', '管理员', '2020-07-17 15:37:24', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('329', 'RC15923163039369960', 'MC2020071714281876', '1', '0', '2020-07-17 15:37:25', 'U001', '管理员', '2020-07-17 15:37:25', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('330', 'RC15923163039369960', 'MC2020071714281226', '1', '0', '2020-07-17 15:37:26', 'U001', '管理员', '2020-07-17 15:37:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('331', 'RC15923163039369960', 'MC2020072714480934', '1', '0', '2020-07-27 17:26:44', 'U001', '管理员', '2020-07-27 17:26:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('332', 'RC15923163039369960', 'MC2020072714498612', '1', '0', '2020-07-27 17:26:45', 'U001', '管理员', '2020-07-27 17:26:45', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('333', 'RC15923163039369960', 'MC2020072715255693', '1', '0', '2020-07-27 17:26:48', 'U001', '管理员', '2020-07-27 17:26:48', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('334', 'RC15923163039369960', 'MC2020072715252387', '1', '0', '2020-07-27 17:26:49', 'U001', '管理员', '2020-07-27 17:26:49', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('335', 'RC15923163039369960', 'MC2020072715256263', '1', '0', '2020-07-27 17:26:50', 'U001', '管理员', '2020-07-27 17:26:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('336', 'RC15923163039369960', 'MC2020072715255567', '1', '0', '2020-07-27 17:26:50', 'U001', '管理员', '2020-07-27 17:26:50', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('337', 'RC15923163039369960', 'MC2020072717258929', '1', '0', '2020-07-27 17:27:04', 'U001', '管理员', '2020-07-27 17:27:04', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('338', 'RC15923163039369960', 'MC2020071209384806', '1', '0', '2020-07-30 11:08:03', 'U001', '管理员', '2020-07-30 11:08:03', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('339', 'RC15923163039369960', 'MC2020071209438390', '1', '0', '2020-07-30 11:08:04', 'U001', '管理员', '2020-07-30 11:08:04', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('340', 'RC15923163039369960', 'MC2020071209439126', '1', '0', '2020-07-30 11:08:05', 'U001', '管理员', '2020-07-30 11:08:05', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('341', 'RC15923163039369960', 'MC2020063009034235', '1', '0', '2020-07-30 11:08:07', 'U001', '管理员', '2020-07-30 11:08:07', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('342', 'RC15923163039369960', 'MC2020072310571467', '1', '0', '2020-08-17 19:27:30', 'U001', '管理员', '2020-08-17 19:27:30', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('343', 'RC15923163039369960', 'MC2020072310576862', '1', '0', '2020-08-17 19:27:34', 'U001', '管理员', '2020-08-17 19:27:34', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('344', 'RC15923163039369960', 'MC2020072714493228', '1', '0', '2020-08-17 19:27:41', 'U001', '管理员', '2020-08-17 19:27:41', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('345', 'RC15923163039369960', 'MC2020081719254186', '1', '0', '2020-08-17 19:27:42', 'U001', '管理员', '2020-08-17 19:27:42', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('346', 'RC15923163039369960', 'MC2020081719251588', '1', '0', '2020-08-17 19:27:43', 'U001', '管理员', '2020-08-17 19:27:43', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('347', 'RC15923163039369960', 'MC2020081719258477', '1', '0', '2020-08-17 19:27:44', 'U001', '管理员', '2020-08-17 19:27:44', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_menu` VALUES ('348', 'RC15923163039369960', 'MC2020081719259804', '1', '0', '2020-08-17 19:27:45', 'U001', '管理员', '2020-08-17 19:27:45', null, null, 'CP01', 'U001', 'CP01', null, '0');

-- ----------------------------
-- Table structure for sys_user_position
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_position`;
CREATE TABLE `sys_user_position` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `company_code` varchar(50) DEFAULT NULL COMMENT '公司',
  `position_code` varchar(50) DEFAULT NULL COMMENT '职位编码',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='用户菜单关联';

-- ----------------------------
-- Records of sys_user_position
-- ----------------------------
INSERT INTO `sys_user_position` VALUES ('1', 'U001', 'CP01', 'ADMIN', '1', '0', '2020-05-29 15:46:08', null, null, '2020-05-29 15:46:22', null, null, null, null, null, null, '0');
INSERT INTO `sys_user_position` VALUES ('2', 'UC15907509667996591', 'CO15907458659568425', 'PO15905719115748897', '1', '0', '2020-05-29 19:16:07', 'U001', '管理员', '2020-06-16 22:24:23', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_position` VALUES ('3', 'UC15907513805251726', 'CO15907458659568425', 'PO15905719115748897', '1', '0', '2020-05-29 19:23:01', 'U001', '管理员', '2020-06-17 17:27:12', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_position` VALUES ('4', 'UC15907517235671133', 'CO15907458659568425', 'PO15905719115748897', '1', '0', '2020-05-29 19:28:44', 'U001', '管理员', '2020-06-16 22:24:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_position` VALUES ('5', 'U001', 'CO15907458659568425', 'PO15905719115748897', '1', '0', '2020-05-29 19:29:45', 'U001', '管理员', '2020-05-29 19:32:21', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_position` VALUES ('6', 'UC15909249151008102', 'CP01', 'PO15905719115748897', '1', '0', '2020-05-31 19:35:17', 'U001', '管理员', '2020-05-31 19:35:17', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_position` VALUES ('11', 'UC15909253368333363', 'CP01', 'PO15909156557699431', '1', '0', '2020-05-31 19:42:18', 'U001', '管理员', '2020-07-07 15:37:52', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_position` VALUES ('12', 'UC15909249151008102', 'CO15907458659434343', 'PO15905719115748897', '1', '0', '2020-05-31 21:21:34', 'U001', '管理员', '2020-06-18 18:32:28', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_position` VALUES ('13', 'UC15909322708421901', 'CP01|4578', 'PO15905719115748897', '1', '0', '2020-05-31 21:37:52', 'U001', '管理员', '2020-06-18 18:31:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_position` VALUES ('14', 'UC15914288383436686', 'CP01.001', 'PO15909164323563155', '1', '0', '2020-06-06 15:33:58', 'U001', '管理员', '2020-06-06 15:33:58', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_position` VALUES ('15', 'UC15914292533829386', 'CP01.001.001', 'PO15905719115748897', '1', '0', '2020-06-06 15:40:53', 'U001', '管理员', '2020-06-06 15:40:53', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_position` VALUES ('16', 'UC15914288383436686', 'CP01', 'PO15909156557699431', '1', '0', '2020-06-17 17:41:22', 'U001', '管理员', '2020-06-17 17:41:22', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_position` VALUES ('17', 'UC15914292533829386', 'CP01', 'PO15909156557699431', '1', '0', '2020-06-17 17:41:28', 'U001', '管理员', '2020-07-07 15:36:19', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_position` VALUES ('18', 'UC15909322708421901', 'CO15907458659434343', 'PO15909164323563155', '1', '0', '2020-06-19 22:11:33', 'UC15909249151008102', '沈括', '2020-07-06 14:31:21', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_position` VALUES ('20', 'UC15927410664004948', 'CO15907202006218096', 'PO15905719115748897', '1', '0', '2020-06-21 20:04:26', 'U001', '管理员', '2020-06-21 20:04:26', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_position` VALUES ('23', 'UC15909253368333363', 'CO15907458659434343', 'PO15909156557699431', '1', '0', '2020-07-05 22:29:13', 'UC15909249151008102', '沈括', '2020-07-06 14:31:25', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_position` VALUES ('25', 'UC2020070614320764', 'CO15907458659434343', 'PO15909164323563155', '1', '0', '2020-07-06 14:32:53', 'UC15909249151008102', '沈括', '2020-07-06 14:32:53', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_position` VALUES ('26', 'UC15925759605520930', 'CO15907458659434343', 'PO15909156557699431', '1', '0', '2020-07-07 16:44:24', 'UC15909249151008102', '沈括', '2020-07-07 16:44:24', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_position` VALUES ('27', 'UC15907509667996591', 'CO15907458659434343', 'PO15909156557699431', '1', '0', '2020-07-07 16:44:30', 'UC15909249151008102', '沈括', '2020-07-07 16:44:30', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_position` VALUES ('28', 'UC15927411606850587', 'CO15907202006218096', 'PO15905719115748897', '1', '0', '2020-07-15 18:28:01', 'UC15927410664004948', '默认测试管理员', '2020-07-15 18:28:01', null, null, 'CO15907202006218096', 'UC15927410664004948', 'CO15907202006218096', null, '0');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_code` varchar(50) DEFAULT NULL COMMENT '角色编码',
  `user_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='用户菜单关联';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', 'RC15908395234096057', 'U001', '1', '0', '2020-05-31 21:18:55', 'U001', '管理员', '2020-05-31 21:18:55', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_role` VALUES ('2', 'RC15908468490378516', 'UC15914288383436686', '1', '1', '2020-06-06 16:05:45', 'U001', '管理员', '2020-06-06 16:05:46', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_role` VALUES ('3', 'RC15923163039369960', 'UC15907513805251726', '1', '0', '2020-06-16 22:26:04', 'U001', '管理员', '2020-06-16 22:26:04', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_role` VALUES ('4', 'RC15908468490378516', 'UC15907517235671133', '1', '0', '2020-06-16 22:26:14', 'U001', '管理员', '2020-06-16 22:26:14', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_role` VALUES ('5', 'RC15923163039369960', 'UC15909249151008102', '1', '0', '2020-06-18 20:29:54', 'U001', '管理员', '2020-06-18 20:29:54', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_role` VALUES ('6', 'RC15924835338897665', 'UC15925759605520930', '1', '0', '2020-06-19 22:37:49', 'UC15909249151008102', '沈括', '2020-06-19 22:37:49', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_role` VALUES ('7', 'RC15923163039369960', 'UC15927410664004948', '1', '0', '2020-06-21 20:40:01', 'U001', '管理员', '2020-06-21 20:40:01', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_user_role` VALUES ('8', 'RC2020070522267928', 'UC15909253368333363', '1', '0', '2020-07-05 22:31:49', 'UC15909249151008102', '沈括', '2020-07-05 22:31:49', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');
INSERT INTO `sys_user_role` VALUES ('9', 'RC2020070522267928', 'UC15907509667996591', '1', '0', '2020-07-05 22:31:55', 'UC15909249151008102', '沈括', '2020-07-05 22:31:55', null, null, 'CO15907458659434343', 'UC15909249151008102', 'CO15907458659434343', null, '0');

-- ----------------------------
-- Table structure for sys_work
-- ----------------------------
DROP TABLE IF EXISTS `sys_work`;
CREATE TABLE `sys_work` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `work_code` varchar(50) DEFAULT NULL COMMENT '待办编码',
  `work_title` varchar(50) DEFAULT NULL COMMENT '待办标题',
  `work_content` varchar(200) DEFAULT NULL COMMENT '待办内容',
  `work_start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `work_end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `status` int(4) DEFAULT '1' COMMENT '状态  1:启用 0:禁用',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除 1:删除 0:未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user_code` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user_code` varchar(50) DEFAULT NULL COMMENT '更新用户编码',
  `update_user_name` varchar(50) DEFAULT NULL COMMENT '更新用户名称',
  `tenant_code` varchar(50) DEFAULT NULL COMMENT '租户编码',
  `owner_code` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `organization_code` varchar(500) DEFAULT NULL COMMENT '组织编码',
  `index_sort` int(11) DEFAULT NULL COMMENT '排序',
  `update_version` int(11) DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_is_deleted` (`is_deleted`) USING BTREE,
  KEY `index_tenant_code` (`tenant_code`) USING BTREE,
  KEY `index_owner_code` (`owner_code`) USING BTREE,
  KEY `index_organization_code` (`organization_code`(255)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='我的待办任务';

-- ----------------------------
-- Records of sys_work
-- ----------------------------
INSERT INTO `sys_work` VALUES ('38', 'C2020072312086255', '11', '1', '2020-07-21 16:00:00', '2020-07-28 16:00:00', '1', '1', '2020-07-23 12:08:00', 'U001', '管理员', '2020-07-23 13:07:31', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_work` VALUES ('39', 'C2020072313084086', '测试', '1', '2020-07-23 16:00:00', '2020-07-23 16:00:00', '1', '0', '2020-07-23 13:08:00', 'U001', '管理员', '2020-07-23 13:08:00', null, null, 'CP01', 'U001', 'CP01', null, '0');
INSERT INTO `sys_work` VALUES ('40', 'C2020072313165972', '111', '111', '2020-07-23 00:00:00', '2020-07-29 16:00:00', '1', '0', '2020-07-23 13:16:01', 'U001', '管理员', '2020-07-23 13:16:57', 'U001', '管理员', 'CP01', 'U001', 'CP01', null, '0');
