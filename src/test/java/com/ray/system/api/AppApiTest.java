package com.ray.system.api;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.support.MockSupport;
import com.ray.system.table.params.app.AppCreateParams;
import com.ray.system.table.params.app.AppEditParams;
import com.ray.system.table.params.app.AppQueryParams;
import com.ray.system.table.vo.app.AppVO;
import com.ray.test.annotation.Mock;
import com.ray.test.annotation.MockData;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

@Slf4j
public class AppApiTest extends MockSupport {

    @Mock(implClass = AppApi.class)
    private AppApi AppApi;

    @Test
    @MockData
    public void pageApps() throws Exception {
        AppQueryParams AppQueryParams = new AppQueryParams();
        CommonPage<AppQueryParams, Page<AppVO>> queryParams = new CommonPage<>();
        queryParams.setEntity(AppQueryParams);
        queryParams.setPage(new Page<>());
        Result<IPage<AppVO>> result = AppApi.pageApps(queryParams);
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void queryApps() throws Exception {
        AppQueryParams AppCreateParams = new AppQueryParams();
        Result<List<AppVO>> result = AppApi.queryApps(AppCreateParams);
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void createApp() throws Exception {
        AppCreateParams AppCreateParams = new AppCreateParams();
        AppCreateParams.setAppName("测试应用");
        AppCreateParams.setAppKey("TEST01");
        Result<String> result = AppApi.createApp(AppCreateParams);
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void editApp() throws Exception {
        AppEditParams AppEditParams = new AppEditParams();
        AppEditParams.setAppCode("APP15906274897403754");
        AppEditParams.setAppName("测试应用");
        AppEditParams.setAppKey("TEST1");
        Result<String> result = AppApi.editApp(AppEditParams);
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void deleteApp() throws Exception {
        Result<String> result = AppApi.openApp("APP15906274897403754");
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void openApp() throws Exception {
        Result<String> result = AppApi.openApp("APP15906274897403754");
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void closeApp() throws Exception {
        Result<String> result = AppApi.closeApp("APP15906274897403754");
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void viewApp() throws Exception {
        Result<AppVO> result = AppApi.viewApp("APP15906274897403754");
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

}