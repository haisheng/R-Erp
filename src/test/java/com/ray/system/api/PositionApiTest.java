package com.ray.system.api;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.support.MockSupport;
import com.ray.system.table.params.position.PositionCreateParams;
import com.ray.system.table.params.position.PositionEditParams;
import com.ray.system.table.params.position.PositionQueryParams;
import com.ray.system.table.vo.position.PositionVO;
import com.ray.test.annotation.Mock;
import com.ray.test.annotation.MockData;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import java.util.List;

import static org.junit.Assert.*;

@Slf4j
public class PositionApiTest extends MockSupport {

    @Mock(implClass = PositionApi.class)
    private PositionApi positionApi;

    @Test
    @MockData
    public void pagePositions() throws Exception {
        PositionQueryParams positionQueryParams = new PositionQueryParams();
        CommonPage<PositionQueryParams,Page<PositionVO>> queryParams = new CommonPage<>();
        queryParams.setEntity(positionQueryParams);
        queryParams.setPage(new Page<>());
        Result<IPage<PositionVO>> result= positionApi.pagePositions(queryParams);
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void queryPositions() throws Exception {
        PositionQueryParams positionCreateParams = new PositionQueryParams();
        Result<List<PositionVO>> result= positionApi.queryPositions(positionCreateParams);
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void createPosition() throws Exception {
        PositionCreateParams positionCreateParams = new PositionCreateParams();
        positionCreateParams.setDataAuth("ALL");
        positionCreateParams.setPositionName("测试管理员");
        Result<String> result= positionApi.createPosition(positionCreateParams);
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void editPosition() throws Exception {
        PositionEditParams positionEditParams = new PositionEditParams();
        positionEditParams.setPositionCode("PO15905719115748897");
        positionEditParams.setDataAuth("COMPANY");
        positionEditParams.setPositionName("测试管理员");
        Result<String> result= positionApi.editPosition(positionEditParams);
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void deletePosition() throws Exception {
        Result<String> result= positionApi.openPosition("PO15905719115748897");
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void openPosition() throws Exception {
        Result<String> result= positionApi.openPosition("PO15905719115748897");
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void closePosition() throws Exception {
        Result<String> result= positionApi.closePosition("PO15905719115748897");
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

    @Test
    @MockData
    public void viewPosition() throws Exception {
        Result<PositionVO> result= positionApi.viewPosition("PO15905719115748897");
        log.info(JSON.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
    }

}