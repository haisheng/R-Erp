package com.ray.support;

import com.ray.ErpApplication;
import com.ray.test.runner.RMockAutoSpringJUnit4ClassRunner;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.util.LogInUserUtil;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author bo shen
 * @Description:
 * @Class: com.tf56.bms.support.MockSupport
 * @Package tf56.antcolonyums
 * @date 2019/12/23 10:43
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@RunWith(RMockAutoSpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ErpApplication.class)
@Transactional
@Rollback
public class MockSupport {

    //当前用户
    protected LoginUser cruuentUser = null;

    @Before
    public void init() throws Exception {
        LoginUser loginUser = BeanCreate.newBean(LoginUser.class);
        loginUser.setId(1);
        loginUser.setAppCode("ADMIN");
        loginUser.setUserCode("U001");
        loginUser.setUserName("管理员");
        loginUser.setCompanyCode("CP01");
        loginUser.setCompanyName("主体运营公司");
        loginUser.setDeptCode("CP01");
        loginUser.setDeptName("主体运营公司");
        cruuentUser = loginUser;
        LogInUserUtil.add(loginUser);
    }
}
