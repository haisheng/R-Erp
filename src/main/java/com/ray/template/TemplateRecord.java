package com.ray.template;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 模板纪录
 * @Class: TemplateRecord
 * @Package com.tf56.bms.template
 * @date 2020/2/11 11:02
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class TemplateRecord {
    /**
     * 模板编号
     **/
    private String templateCode;

    /**
     * 属性名称
     **/
    private String propertyName;

    /**
     * 属性
     **/
    private String property;

    /**
     * 属性值
     **/
    private String propertyValue;
    /**
     * 是否大数据
     **/
    private boolean bigData;
}
