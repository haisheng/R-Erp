package com.ray.template;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author bo shen
 * @Description: 模板
 * @Class: Template
 * @Package com.tf56.bms.template
 * @date 2020/2/11 10:58
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Template {
    /**模板编号**/
    String code() default "";
}
