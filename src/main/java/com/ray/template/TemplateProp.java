package com.ray.template;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author bo shen
 * @Description: 模板
 * @Class: Template
 * @Package com.tf56.bms.template
 * @date 2020/2/11 10:58
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface TemplateProp {
    /**属性名称**/
    String name() default "";
    /***属性值**/
    String property() default "";
    /***是否大数据**/
    boolean bigData() default false;
    /***数据格式**/
    Class<?> dataClass() default String.class;
    /***数据格式**/
    Class<?> entityClass() default String.class;
    /***时间格式**/
    String dateFormat() default "yyyy-MM-dd hh:mm:ss";
}
