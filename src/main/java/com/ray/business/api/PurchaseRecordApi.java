package com.ray.business.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.table.dto.CustomerQueryDTO;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.builder.PurchaseGoodsBuilder;
import com.ray.business.service.ProdPurchaseRecordService;
import com.ray.business.table.dto.PurchaseQueryDTO;
import com.ray.business.table.entity.ProdPurchaseRecord;
import com.ray.business.table.params.purchase.PurchaseQueryParams;
import com.ray.business.table.vo.PurchaseGoodsVO;
import com.ray.common.SysMsgCodeConstant;
import com.ray.system.builder.CommonPageBuilder;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.Assert;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 采购相关接口
 * @Class: PurchaseApi
 * @Package com.ray.business.api
 * @date 2020/6/7 17:52
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class PurchaseRecordApi {

    @Autowired
    private ProdPurchaseRecordService prodPurchaseRecordService;
    @Autowired
    private GoodsService goodsService;

    /**
     * 查询采购订单列表信息 分页  非删除的
     *
     * @param queryParams
     * @return
     */
    public Result<IPage<PurchaseGoodsVO>> pagePurchaseGoods(CommonPage<PurchaseQueryParams, Page<PurchaseGoodsVO>> queryParams) {
        Assert.notNull(queryParams, SysMsgCodeConstant.Error.ERR10000001);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        Map<String, MaterialModelVO> modelMap = new HashMap<>();
        CommonPageBuilder<PurchaseQueryDTO, ProdPurchaseRecord> commonPageBuilder = new CommonPageBuilder<>(PurchaseQueryDTO.class);
        commonPageBuilder.appendEntity(queryParams.getEntity()).appendQuery(queryParams.getQuery()).appendPage(queryParams.getPage());
        IPage<ProdPurchaseRecord> page = prodPurchaseRecordService.page(commonPageBuilder.bulid(), loginUser);
        List<ProdPurchaseRecord> orders = page.getRecords();
        //结果对象
        IPage<PurchaseGoodsVO> pageList = new Page<>();
        pageList.setTotal(page.getTotal());
        pageList.setCurrent(page.getCurrent());
        pageList.setSize(page.getSize());
        //查询到结果 数据转换
        if (ObjectUtil.isNotNull(orders)) {
            pageList.setRecords(orders.stream().map(sysPurchase -> {
                MaterialModelVO materialModelVO = modelMap.get(sysPurchase.getGoodsCode());
                if (ObjectUtil.isNull(materialModelVO)) {
                    materialModelVO = goodsService.queryGoodsByCode(sysPurchase.getGoodsCode(), loginUser);
                    modelMap.put(sysPurchase.getGoodsCode(),materialModelVO);
                }
                return new PurchaseGoodsBuilder().append(sysPurchase).append(materialModelVO).bulid();
            }).collect(Collectors.toList()));
        } else {
            pageList.setRecords(new ArrayList<>());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, pageList);
    }


    /**
     * 查询采购订单列表信息 分页  非删除的
     *
     * @param queryParams
     * @return
     */
    public Result<List<PurchaseGoodsVO>> listPurchasesGoods(PurchaseQueryParams queryParams) {
        Assert.notNull(queryParams, SysMsgCodeConstant.Error.ERR10000001);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        Map<String, MaterialModelVO> modelMap = new HashMap<>();
        PurchaseQueryDTO queryDTO = new PurchaseQueryDTO();
        BeanUtil.copyProperties(queryParams, queryDTO);
        List<ProdPurchaseRecord> orders = prodPurchaseRecordService.list(queryDTO, loginUser);
        List<PurchaseGoodsVO> list = orders.stream().map(sysPurchase -> {
            MaterialModelVO materialModelVO = modelMap.get(sysPurchase.getGoodsCode());
            if (ObjectUtil.isNull(materialModelVO)) {
                materialModelVO = goodsService.queryGoodsByCode(sysPurchase.getGoodsCode(), loginUser);
                modelMap.put(sysPurchase.getGoodsCode(),materialModelVO);
            }
            return new PurchaseGoodsBuilder().append(sysPurchase).append(materialModelVO).bulid();
        }).collect(Collectors.toList());
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, list);
    }

}
