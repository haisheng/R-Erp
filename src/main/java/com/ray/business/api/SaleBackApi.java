package com.ray.business.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.service.BaseCustomerService;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.builder.SaleBackBuilder;
import com.ray.business.builder.SaleBackRecordBuilder;
import com.ray.business.builder.SaleGoodsBuilder;
import com.ray.business.check.SaleBackCheck;
import com.ray.business.check.SaleCheck;
import com.ray.business.enums.PurchaseInStatusEnum;
import com.ray.business.enums.SaleStatusEnum;
import com.ray.business.service.ProdSaleBackRecordService;
import com.ray.business.service.ProdSaleBackService;
import com.ray.business.service.ProdSaleRecordService;
import com.ray.business.service.ProdSaleService;
import com.ray.business.table.dto.SaleBackQueryDTO;
import com.ray.business.table.entity.ProdSale;
import com.ray.business.table.entity.ProdSaleBack;
import com.ray.business.table.entity.ProdSaleBackRecord;
import com.ray.business.table.entity.ProdSaleRecord;
import com.ray.business.table.params.sale.back.SaleBackCreateParams;
import com.ray.business.table.params.sale.back.SaleBackEditParams;
import com.ray.business.table.params.sale.back.SaleBackQueryParams;
import com.ray.business.table.vo.SaleBackVO;
import com.ray.business.table.vo.SaleGoodsVO;
import com.ray.common.SysMsgCodeConstant;
import com.ray.system.builder.CommonPageBuilder;
import com.ray.system.enums.FileTypeEnum;
import com.ray.system.service.SysFileService;
import com.ray.system.table.entity.SysFile;
import com.ray.util.FileRecordUtil;
import com.ray.validate.support.utils.ValidateUtil;
import com.ray.wms.builder.GoodsBuilder;
import com.ray.wms.service.WmsWarehouseService;
import com.ray.wms.service.compose.InService;
import com.ray.wms.table.dto.GoodsDTO;
import com.ray.wms.table.dto.OrderCreateDTO;
import com.ray.wms.table.entity.WmsWarehouse;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.config.SysFileConfig;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.Assert;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description:
 * @Class: SaleBackBackApi
 * @Package com.ray.business.api
 * @date 2020/6/8 13:39
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class SaleBackApi {

    @Autowired
    private ProdSaleBackService prodSaleBackService;
    @Autowired
    private ProdSaleBackRecordService prodSaleBackRecordService;
    @Autowired
    private BaseCustomerService baseCustomerService;
    @Autowired
    private WmsWarehouseService wmsWarehouseService;
    @Autowired
    private InService inService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ProdSaleService prodSaleService;
    @Autowired
    private ProdSaleRecordService prodSaleRecordService;
    @Autowired
    private SysFileService sysFileService;
    @Autowired
    private SysFileConfig sysFileConfig;


    /**
     * 查询采购订单列表信息 分页  非删除的
     *
     * @param queryParams
     * @return
     */
    public Result<IPage<SaleBackVO>> pageSaleBacks(CommonPage<SaleBackQueryParams, Page<SaleBackVO>> queryParams) {
        Assert.notNull(queryParams, SysMsgCodeConstant.Error.ERR10000001);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        Map<String, WmsWarehouse> wmsWarehouseMap = new HashMap<>();
        Map<String, BaseCustomer> baseCustomerMap = new HashMap<>();
        CommonPageBuilder<SaleBackQueryDTO, ProdSaleBack> commonPageBuilder = new CommonPageBuilder<>(SaleBackQueryDTO.class);
        commonPageBuilder.appendEntity(queryParams.getEntity()).appendQuery(queryParams.getQuery()).appendPage(queryParams.getPage());
        IPage<ProdSaleBack> page = prodSaleBackService.page(commonPageBuilder.bulid(), loginUser);
        List<ProdSaleBack> orders = page.getRecords();
        //结果对象
        IPage<SaleBackVO> pageList = new Page<>();
        pageList.setTotal(page.getTotal());
        pageList.setCurrent(page.getCurrent());
        pageList.setSize(page.getSize());
        //查询到结果 数据转换
        if (ObjectUtil.isNotNull(orders)) {
            pageList.setRecords(orders.stream().map(sysSaleBack -> {
                SaleBackVO orderVO = new SaleBackVO();
                BeanUtil.copyProperties(sysSaleBack, orderVO);
                //查询客信息
                BaseCustomer baseCustomer = baseCustomerMap.get(sysSaleBack.getCustomerCode());
                if (ObjectUtil.isEmpty(baseCustomer)) {
                    baseCustomer = baseCustomerService.queryCustomerByCustomerCode(sysSaleBack.getCustomerCode(), loginUser);
                    baseCustomerMap.put(sysSaleBack.getCustomerCode(), baseCustomer);
                }
                orderVO.setCustomerName(baseCustomer.getCustomerName());
                //查询仓库信息
                WmsWarehouse wmsWarehouse = wmsWarehouseMap.get(sysSaleBack.getWarehouseCode());
                if (ObjectUtil.isEmpty(wmsWarehouse)) {
                    wmsWarehouse = wmsWarehouseService.queryWarehouseByWarehouseCode(sysSaleBack.getWarehouseCode(), loginUser);
                    wmsWarehouseMap.put(wmsWarehouse.getWarehouseCode(), wmsWarehouse);
                }
                orderVO.setWarehouseName(wmsWarehouse.getWarehouseName());
                return orderVO;
            }).collect(Collectors.toList()));
        } else {
            pageList.setRecords(new ArrayList<>());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, pageList);
    }


    /**
     * 创建订单
     *
     * @param createParams 创建对象
     * @return Result
     */
    @Transactional
    public Result<String> createOrder(SaleBackCreateParams createParams) {
        ValidateUtil.validate(createParams);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //查询采购订单数据
        ProdSale prodSale = prodSaleService.querySaleBySaleCode(createParams.getOrderNo(), loginUser);
        new SaleCheck(prodSale).checkNull("销售退货单不存在").checkCanPass("订单未审核或已取消");
        SaleBackBuilder orderBuilder = new SaleBackBuilder();
        orderBuilder.append(createParams).append(prodSale).appendStatus(SaleStatusEnum.UN_CHECK.getValue()).appendCreate(loginUser);

        //总数
        final List<BigDecimal> quantity = new ArrayList<>();
        //金额统计
        final List<BigDecimal> total = new ArrayList<>();
        //保存商品信息
        List<ProdSaleBackRecord> records = createParams.getGoods().stream().map(saleGoodsParams -> {
            //查询商品是否存在
            ProdSaleRecord prodSaleRecord = prodSaleRecordService.queryRecord(saleGoodsParams,prodSale.getOrderNo(),loginUser);
            if(ObjectUtil.isNull(prodSaleRecord)){
                log.info("商品{}不在订单中",JSON.toJSONString(saleGoodsParams));
                throw BusinessExceptionFactory.newException("商品不存在");
            }
            //销售退回订单数量
            quantity.add(saleGoodsParams.getQuantity());
            //计算单个商品总价
            total.add(saleGoodsParams.getQuantity().multiply(NumberUtil.null2Zero(prodSaleRecord.getPrice())));
            //已退回数量
            BigDecimal backQuantity = prodSaleBackRecordService.queryBackQuantity(createParams.getOrderNo(),null,saleGoodsParams,loginUser);
            //判断退回商品数量
            if(prodSaleRecord.getQuantity().subtract(backQuantity).compareTo(saleGoodsParams.getQuantity()) <0){
                log.info("商品{}可退数量:{},实际数量:{}",JSON.toJSONString(prodSaleRecord),
                        prodSaleRecord.getQuantity().subtract(backQuantity),saleGoodsParams.getQuantity());
                throw BusinessExceptionFactory.newException("可退回数量不足");
            }
            return new SaleBackRecordBuilder().append(saleGoodsParams).appendOrderNo(prodSale.getOrderNo())
                    .appendRecordCode(prodSaleRecord.getCode())
                    .appendPrice(prodSaleRecord.getPrice()).appendCode(orderBuilder.getCode()).appendCreate(loginUser).bulid();
        }).collect(Collectors.toList());
        //保存总数
        orderBuilder.append(quantity.stream().reduce(BigDecimal::add).get());
        //保存总价
        orderBuilder.appendTotal(total.stream().reduce(BigDecimal::add).get());
        //保存订单信息
        if (!prodSaleBackService.save(orderBuilder.bulid())) {
            log.info("保存订单接口异常,参数:{}", JSON.toJSONString(orderBuilder.bulid()));
            throw BusinessExceptionFactory.newException("订单操作异常");
        }
        //保存数据
        prodSaleBackRecordService.saveBatch(records);
        //保存文件
        sysFileService.saveFile(orderBuilder.getCode(),FileTypeEnum.SALE_BACK, FileRecordUtil.toFileDTO(createParams.getFiles()),loginUser);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, orderBuilder.getCode());
    }

    /**
     * 编辑订单
     *
     * @param editParams 编辑对象
     * @return Result
     */
    @Transactional
    public Result<String> editOrder(SaleBackEditParams editParams) {
        ValidateUtil.validate(editParams);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        ProdSaleBack prodSaleBack = prodSaleBackService.querySaleBackBySaleBackCode(editParams.getBackCode(), loginUser);
        new SaleBackCheck(prodSaleBack).checkNull("采购退货单步不存在").checkCanEdit("采购退货单已审核");
        ProdSale prodSale = prodSaleService.querySaleBySaleCode(prodSaleBack.getOrderNo(), loginUser);
        new SaleCheck(prodSale).checkNull("采购单不存在").checkCanPass("订单未审核或已取消");
        SaleBackBuilder orderBuilder = new SaleBackBuilder();
        orderBuilder.append(editParams).appendEdit(loginUser);

        //删除数据
        prodSaleBackRecordService.deleteOrderGoods(editParams.getBackCode(), loginUser);
        //金额统计
        final List<BigDecimal> total = new ArrayList<>();
        //总数
        final List<BigDecimal> quantity = new ArrayList<>();
        //保存商品信息
        List<ProdSaleBackRecord> records = editParams.getGoods().stream().map(saleGoodsParams -> {
            //查询商品是否存在
            ProdSaleRecord prodSaleRecord = prodSaleRecordService.queryRecord(saleGoodsParams,prodSale.getOrderNo(),loginUser);
            if(ObjectUtil.isNull(prodSaleRecord)){
                log.info("商品{}不在订单中",JSON.toJSONString(saleGoodsParams));
                throw BusinessExceptionFactory.newException("商品不存在");
            }
            //已退回数量
            BigDecimal backQuantity = prodSaleBackRecordService.queryBackQuantity(editParams.getOrderNo(),prodSaleBack.getBackCode(),saleGoodsParams,loginUser);
            //判断退回商品数量
            if(prodSaleRecord.getQuantity().subtract(backQuantity).compareTo(saleGoodsParams.getQuantity()) <0){
                log.info("商品{}可退数量:{},实际数量:{}",JSON.toJSONString(prodSaleRecord),
                        prodSaleRecord.getQuantity().subtract(backQuantity),saleGoodsParams.getQuantity());
                throw BusinessExceptionFactory.newException("可退回数量不足");
            }
            //销售退回订单数量
            quantity.add(saleGoodsParams.getQuantity());
            //计算单个商品总价
            total.add(saleGoodsParams.getQuantity().multiply(NumberUtil.null2Zero(prodSaleRecord.getPrice())));
            return new SaleBackRecordBuilder().append(saleGoodsParams).appendOrderNo(prodSale.getOrderNo())
                    .appendRecordCode(prodSaleRecord.getCode())
                    .appendPrice(prodSaleRecord.getPrice()).appendCode(orderBuilder.getCode()).appendCreate(loginUser).bulid();
        }).collect(Collectors.toList());
        //保存总数
        orderBuilder.append(quantity.stream().reduce(BigDecimal::add).get());
        //保存总价
        orderBuilder.appendTotal(total.stream().reduce(BigDecimal::add).get());
        //编辑订单信息
        if (!prodSaleBackService.edit(orderBuilder.bulid(), loginUser)) {
            log.info("编辑订单接口异常,参数:{}", JSON.toJSONString(orderBuilder.bulid()));
            throw BusinessExceptionFactory.newException("订单操作异常");
        }
        //保存数据
        prodSaleBackRecordService.saveBatch(records);
        //删除文件
        sysFileService.deleteFile(orderBuilder.getCode(), FileTypeEnum.SALE_BACK.getValue(),loginUser);
        //保存文件
        sysFileService.saveFile(orderBuilder.getCode(),FileTypeEnum.SALE_BACK, FileRecordUtil.toFileDTO(editParams.getFiles()),loginUser);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, orderBuilder.getCode());
    }


    /**
     * 审核通过
     *
     * @param backCode
     * @return
     */
    @Transactional
    public Result<String> passOrder(String backCode) {
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        ProdSaleBack prodSaleBack = prodSaleBackService.querySaleBackBySaleBackCode(backCode, loginUser);
        new SaleBackCheck(prodSaleBack).checkNull("销售退货单不存在").checkCanCheck("订单已经审核");
        SaleBackBuilder orderBuilder = new SaleBackBuilder();
        orderBuilder.appendCode(backCode).appendStatus(PurchaseInStatusEnum.UN_IN.getValue()).appendEdit(loginUser);
        //编辑订单信息
        if (!prodSaleBackService.edit(orderBuilder.bulid(), loginUser)) {
            log.info("审核订单接口异常,参数:{}", JSON.toJSONString(orderBuilder.bulid()));
            throw BusinessExceptionFactory.newException("订单操作异常");
        }
        OrderCreateDTO orderCreateDTO = new OrderCreateDTO();
        orderCreateDTO.setBusinessCode(backCode);
        orderCreateDTO.setWarehouseCode(prodSaleBack.getWarehouseCode());
        //查询订单商品
        List<ProdSaleBackRecord> records = prodSaleBackRecordService.list(backCode, loginUser);

        List<GoodsDTO> goodsDTOS = records.stream().map(prodSaleBackRecord -> {
            return new GoodsBuilder().append(prodSaleBackRecord).bulid();
        }).collect(Collectors.toList());
        orderCreateDTO.setGoodsDTOS(goodsDTOS);
        //创建订单
        inService.createOrderIn(orderCreateDTO, loginUser);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, backCode);
    }

    /**
     * 删除
     *
     * @param backCode
     * @return
     */
    @Transactional
    public Result<String> deleteOrder(String backCode) {
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        ProdSaleBack prodSaleBack = prodSaleBackService.querySaleBackBySaleBackCode(backCode, loginUser);
        new SaleBackCheck(prodSaleBack).checkNull("销售退货单不存在").checkCanCheck("销售退货不能删除");
        SaleBackBuilder orderBuilder = new SaleBackBuilder();
        orderBuilder.appendCode(backCode).appendEdit(loginUser).delete();
        //编辑订单信息
        if (!prodSaleBackService.edit(orderBuilder.bulid(), loginUser)) {
            log.info("编辑订单接口异常,参数:{}", JSON.toJSONString(orderBuilder.bulid()));
            throw BusinessExceptionFactory.newException("订单操作异常");
        }
        //删除数据
        prodSaleBackRecordService.deleteOrderGoods(backCode, loginUser);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, backCode);
    }


    /**
     * 取消
     *
     * @param backCode
     * @return
     */
    @Transactional
    public Result<String> cancelOrder(String backCode) {
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        ProdSaleBack prodSaleBack = prodSaleBackService.querySaleBackBySaleBackCode(backCode, loginUser);
        new SaleBackCheck(prodSaleBack).checkNull("销售退货单不存在").checkCanCancel("订单不能取消");
        SaleBackBuilder orderBuilder = new SaleBackBuilder();
        orderBuilder.appendCode(backCode).appendStatus(PurchaseInStatusEnum.CANCEL.getValue()).appendEdit(loginUser);
        //编辑订单信息
        if (!prodSaleBackService.edit(orderBuilder.bulid(), loginUser)) {
            log.info("编辑订单接口异常,参数:{}", JSON.toJSONString(orderBuilder.bulid()));
            throw BusinessExceptionFactory.newException("订单操作异常");
        }
        //删除数据
        prodSaleBackRecordService.deleteOrderGoods(backCode, loginUser);
        //作废出库订单
        inService.invalidOrderBusinessCode(backCode, loginUser);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, backCode);
    }


    /**
     * 订单详情
     *
     * @param orderNo
     * @return
     */
    public Result<SaleBackVO> viewOrder(String orderNo) {
        LoginUser loginUser = LogInUserUtil.get();
        ProdSaleBack prodSaleBack = prodSaleBackService.querySaleBackBySaleBackCode(orderNo, loginUser);
        new SaleBackCheck(prodSaleBack).checkNull("销售退货单不存在");
        SaleBackVO saleVO = new SaleBackVO();
        BeanUtil.copyProperties(prodSaleBack, saleVO);
        //查询客信息
        BaseCustomer baseCustomer = baseCustomerService.queryCustomerByCustomerCode(prodSaleBack.getCustomerCode(), loginUser);
        if (ObjectUtil.isNotNull(baseCustomer)) {
            saleVO.setCustomerName(baseCustomer.getCustomerName());
        }
        //查询仓库信息
        WmsWarehouse wmsWarehouse = wmsWarehouseService.queryWarehouseByWarehouseCode(prodSaleBack.getWarehouseCode(), loginUser);
        if (ObjectUtil.isNotNull(wmsWarehouse)) {
            saleVO.setWarehouseName(wmsWarehouse.getWarehouseName());
        }
        //查询
        List<ProdSaleBackRecord> prodSaleBackRecords = prodSaleBackRecordService.list(orderNo, loginUser);
        Map<String, MaterialModelVO> modelMap = new HashMap<>();
        List<SaleGoodsVO> saleGoodsVOS = prodSaleBackRecords.stream().map(prodSaleBackRecord -> {
            MaterialModelVO materialModelVO = modelMap.get(prodSaleBackRecord.getGoodsCode());
            if (ObjectUtil.isNull(materialModelVO)) {
                materialModelVO = goodsService.queryGoodsByCode(prodSaleBackRecord.getGoodsCode(), loginUser);
                modelMap.put(prodSaleBackRecord.getGoodsCode(),materialModelVO);
            }
            return new SaleGoodsBuilder().append(prodSaleBackRecord).append(materialModelVO).bulid();
        }).collect(Collectors.toList());
        saleVO.setGoods(saleGoodsVOS);
        //查询图片信息
        List<SysFile> files = sysFileService.queryFiles(orderNo, FileTypeEnum.SALE_BACK.getValue(),loginUser);
        saleVO.setFiles(FileRecordUtil.toFileVO(files,sysFileConfig));
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, saleVO);
    }
}
