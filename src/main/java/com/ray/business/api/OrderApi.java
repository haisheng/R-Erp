package com.ray.business.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.check.CustomerCheck;
import com.ray.base.service.BaseCustomerPrintService;
import com.ray.base.service.BaseCustomerService;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.base.table.entity.BaseCustomerPrint;
import com.ray.base.table.vo.customer.CustomerPrintVO;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.builder.OrderBuilder;
import com.ray.business.builder.OrderGoodsBuilder;
import com.ray.business.builder.OrderGoodsVOBuilder;
import com.ray.business.builder.OrderVOBuilder;
import com.ray.business.check.OrderCheck;
import com.ray.business.enums.OrderStatusEnum;
import com.ray.business.service.ProdBusinessService;
import com.ray.business.service.ProdOrderGoodsService;
import com.ray.business.service.ProdOrderService;
import com.ray.business.service.ProdOrderStepService;
import com.ray.business.table.dto.OrderGoodsQueryDTO;
import com.ray.business.table.dto.OrderQueryDTO;
import com.ray.business.table.entity.ProdOrder;
import com.ray.business.table.entity.ProdOrderGoods;
import com.ray.business.table.params.order.OrderCreateParams;
import com.ray.business.table.params.order.OrderEditParams;
import com.ray.business.table.params.order.OrderQueryParams;
import com.ray.business.table.params.order.goods.OrderGoodsQueryParams;
import com.ray.business.table.vo.OrderGoodsCountVO;
import com.ray.business.table.vo.OrderGoodsVO;
import com.ray.business.table.vo.OrderVO;
import com.ray.common.SysMsgCodeConstant;
import com.ray.system.builder.CommonPageBuilder;
import com.ray.system.service.SysUserService;
import com.ray.system.table.entity.SysUser;
import com.ray.validate.support.utils.ValidateUtil;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.Assert;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 订单相关服务
 * @Class: OrderApi
 * @Package com.ray.base.api
 * @date 2020/5/27 11:15
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class OrderApi {

    @Autowired
    private ProdOrderService prodOrderService;
    @Autowired
    private BaseCustomerService baseCustomerService;
    @Autowired
    private ProdOrderGoodsService prodOrderGoodsService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private BaseCustomerPrintService customerPrintService;
    @Autowired
    private ProdBusinessService prodBusinessService;


    /**
     * 查询订单列表信息 分页  非删除的
     *
     * @param queryParams
     * @return
     */
    public Result<IPage<OrderVO>> pageOrders(CommonPage<OrderQueryParams, Page<OrderVO>> queryParams) {
        Assert.notNull(queryParams, SysMsgCodeConstant.Error.ERR10000001);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        Map<String, BaseCustomer> baseCustomerMap = new HashMap<>();
        CommonPageBuilder<OrderQueryDTO, ProdOrder> commonPageBuilder = new CommonPageBuilder<>(OrderQueryDTO.class);
        commonPageBuilder.appendEntity(queryParams.getEntity()).appendQuery(queryParams.getQuery()).appendPage(queryParams.getPage());
        IPage<ProdOrder> page = prodOrderService.page(commonPageBuilder.bulid(), loginUser);
        List<ProdOrder> orders = page.getRecords();
        //结果对象
        IPage<OrderVO> pageList = new Page<>();
        pageList.setTotal(page.getTotal());
        pageList.setCurrent(page.getCurrent());
        pageList.setSize(page.getSize());
        //查询到结果 数据转换
        if (ObjectUtil.isNotEmpty(orders)) {
            pageList.setRecords(orders.stream().map(sysOrder -> {
                //查询客信息
                BaseCustomer baseCustomer = baseCustomerMap.get(sysOrder.getCustomerCode());
                if (ObjectUtil.isEmpty(baseCustomer)) {
                    baseCustomer = baseCustomerService.queryCustomerByCustomerCode(sysOrder.getCustomerCode(), loginUser);
                    baseCustomerMap.put(sysOrder.getCustomerCode(), baseCustomer);
                }
                //查询用户信息
                //  SysUser sysUser = sysUserService.queryUserByUserCode(sysOrder.getOwnerCode(), loginUser);
                OrderVOBuilder orderVOBuilder = new OrderVOBuilder();
                orderVOBuilder.append(baseCustomer).append(sysOrder);
                return orderVOBuilder.bulid();
            }).collect(Collectors.toList()));
        } else {
            pageList.setRecords(new ArrayList<>());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, pageList);
    }

    /**
     * 查询订单列表信息--启用的订单
     *
     * @param queryParams
     * @return
     */
    public Result<List<OrderVO>> queryOrders(OrderQueryParams queryParams) {
        Assert.notNull(queryParams, SysMsgCodeConstant.Error.ERR10000001);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        OrderQueryDTO queryDTO = new OrderQueryDTO();
        BeanUtil.copyProperties(queryParams, queryDTO);
        queryDTO.setStatus(YesOrNoEnum.YES.getValue());
        List<ProdOrder> orders = prodOrderService.list(queryDTO, loginUser);
        Map<String, BaseCustomer> baseCustomerMap = new HashMap<>();

        //查询对象
        List<OrderVO> list = new ArrayList<>();
        if (ObjectUtil.isNotNull(orders)) {
            list = orders.stream().map(sysOrder -> {
                //查询客信息
                BaseCustomer baseCustomer = baseCustomerMap.get(sysOrder.getCustomerCode());
                if (ObjectUtil.isEmpty(baseCustomer)) {
                    baseCustomer = baseCustomerService.queryCustomerByCustomerCode(sysOrder.getCustomerCode(), loginUser);
                    baseCustomerMap.put(sysOrder.getCustomerCode(), baseCustomer);
                }
                //构建对象
                OrderVOBuilder orderVOBuilder = new OrderVOBuilder();
                orderVOBuilder.append(sysOrder).append(baseCustomer);
                return orderVOBuilder.bulid();
            }).collect(Collectors.toList());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, list);
    }

    /**
     * 查询订单列表信息--启用的订单
     *
     * @param queryParams
     * @return
     */
    public Result<List<OrderVO>> queryMyOrders(OrderQueryParams queryParams) {
        Assert.notNull(queryParams, SysMsgCodeConstant.Error.ERR10000001);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        OrderQueryDTO queryDTO = new OrderQueryDTO();
        BeanUtil.copyProperties(queryParams, queryDTO);
        queryDTO.setStatus(YesOrNoEnum.YES.getValue());
        List<ProdOrder> orders = prodOrderService.listMy(queryDTO, loginUser);
        //查询对象
        List<OrderVO> list = new ArrayList<>();
        if (ObjectUtil.isNotNull(orders)) {
            list = orders.stream().map(sysOrder -> {
                OrderVO orderVO = new OrderVO();
                BeanUtil.copyProperties(sysOrder, orderVO);
                return orderVO;
            }).collect(Collectors.toList());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, list);
    }


    /**
     * 创建订单
     *
     * @param createParams 创建对象
     * @return Result
     */
    @Transactional
    public Result<String> createOrder(OrderCreateParams createParams) {
        ValidateUtil.validate(createParams);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //查询客户数据
        BaseCustomer baseCustomer = baseCustomerService.queryCustomerByCustomerCode(createParams.getCustomerCode(), loginUser);
        new CustomerCheck(baseCustomer).checkNull("客户不存在");
        OrderBuilder orderBuilder = new OrderBuilder();
        orderBuilder.append(createParams).appendStatus(OrderStatusEnum.CHECKING.getValue()).appendCreate(loginUser)
                .appendOwner(createParams.getOwnerCode());
        //数量统计
        final List<BigDecimal> quantity = new ArrayList<>();
        //金额统计
        final List<BigDecimal> total = new ArrayList<>();
        List<String> existCodes = new ArrayList<>();
        //保存订单商品
        List<ProdOrderGoods> orderGoods = createParams.getGoods().stream().map(orderGoodsParams -> {
            quantity.add(orderGoodsParams.getQuantity());
            if (existCodes.contains(orderGoodsParams.getGoodsCode())) {
                throw BusinessExceptionFactory.newException("商品重复");
            }
            //计算单个商品总价
            total.add(orderGoodsParams.getQuantity().multiply(NumberUtil.null2Zero(orderGoodsParams.getPrice())));
            existCodes.add(orderGoodsParams.getGoodsCode());
            OrderGoodsBuilder orderGoodsBuilder = new OrderGoodsBuilder();
            orderGoodsBuilder.append(orderGoodsParams).appendOrderNo(orderBuilder.getCode()).appendCustomerCode(baseCustomer.getCustomerCode()).appendCreate(loginUser);
            return orderGoodsBuilder.bulid();
        }).collect(Collectors.toList());
        //保存总数
        orderBuilder.append(quantity.stream().reduce(BigDecimal::add).get());
        //保存总价
        orderBuilder.appendTotal(total.stream().reduce(BigDecimal::add).get());
        //保存订单信息
        if (!prodOrderService.save(orderBuilder.bulid())) {
            log.info("保存订单接口异常,参数:{}", JSON.toJSONString(orderBuilder.bulid()));
            throw BusinessExceptionFactory.newException("订单保存异常");
        }
        //保存商品
        prodOrderGoodsService.saveBatch(orderGoods);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, orderBuilder.getCode());
    }

    /**
     * 编辑订单
     *
     * @param editParams 编辑对象
     * @return Result
     */
    @Transactional
    public Result<String> editOrder(OrderEditParams editParams) {
        ValidateUtil.validate(editParams);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //查询客户数据
        BaseCustomer baseCustomer = baseCustomerService.queryCustomerByCustomerCode(editParams.getCustomerCode(), loginUser);
        new CustomerCheck(baseCustomer).checkNull("客户不存在");
        ProdOrder prodOrder = prodOrderService.queryOrderByOrderCode(editParams.getOrderNo(), loginUser);
        new OrderCheck(prodOrder).checkNull("订单不存在").checkCanEdit("订单不能编辑");
        OrderBuilder orderBuilder = new OrderBuilder();
        orderBuilder.append(editParams).appendEdit(loginUser).appendOwner(editParams.getOwnerCode());
        //数量统计
        final List<BigDecimal> quantity = new ArrayList<>();
        //金额统计
        final List<BigDecimal> total = new ArrayList<>();
        List<String> existCodes = new ArrayList<>();
        //保存订单商品
        List<ProdOrderGoods> orderGoods = editParams.getGoods().stream().map(orderGoodsParams -> {
            if (existCodes.contains(orderGoodsParams.getGoodsCode())) {
                throw BusinessExceptionFactory.newException("商品重复");
            }
            existCodes.add(orderGoodsParams.getGoodsCode());
            quantity.add(orderGoodsParams.getQuantity());
            //计算单个商品总价
            total.add(orderGoodsParams.getQuantity().multiply(NumberUtil.null2Zero(orderGoodsParams.getPrice())));
            //构建商品信息
            OrderGoodsBuilder orderGoodsBuilder = new OrderGoodsBuilder();
            orderGoodsBuilder.append(orderGoodsParams).appendOrderNo(orderBuilder.getCode()).appendCustomerCode(baseCustomer.getCustomerCode()).appendCreate(loginUser);
            return orderGoodsBuilder.bulid();
        }).collect(Collectors.toList());
        //删除订单商品
        prodOrderGoodsService.deleteOrderGoods(editParams.getOrderNo(), loginUser);
        //保存总数
        orderBuilder.append(quantity.stream().reduce(BigDecimal::add).get());
        //保存总价
        orderBuilder.appendTotal(total.stream().reduce(BigDecimal::add).get());
        //编辑订单信息
        if (!prodOrderService.edit(orderBuilder.bulid(), loginUser)) {
            log.info("编辑订单接口异常,参数:{}", JSON.toJSONString(orderBuilder.bulid()));
            throw BusinessExceptionFactory.newException("编辑订单异常");
        }
        //保存商品
        prodOrderGoodsService.saveBatch(orderGoods);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, orderBuilder.getCode());
    }


    /**
     * 删除订单
     *
     * @param orderCode 订单编码
     * @return Result
     */
    @Transactional
    public Result<String> deleteOrder(String orderCode) {
        ValidateUtil.hasLength(orderCode, "参数[orderCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        ProdOrder sysOrder = prodOrderService.queryOrderByOrderCode(orderCode, loginUser);
        new OrderCheck(sysOrder).checkNull("订单不存在").checkCanCheck("订单不能删除");
        OrderBuilder orderBuilder = new OrderBuilder();
        orderBuilder.appendCode(orderCode).appendEdit(loginUser).delete();
        //删除订单信息
        if (!prodOrderService.edit(orderBuilder.bulid(), loginUser)) {
            log.info("删除订单接口异常,参数:{}", JSON.toJSONString(orderBuilder.bulid()));
            throw BusinessExceptionFactory.newException("删除订单异常");
        }
        //删除订单商品
        prodOrderGoodsService.deleteOrderGoods(orderCode, loginUser);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, orderBuilder.getCode());
    }


    /**
     * 审核订单
     *
     * @param orderCode 订单编码
     * @return Result
     */
    @Transactional
    public Result<String> passOrder(String orderCode) {
        ValidateUtil.hasLength(orderCode, "参数[orderCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        ProdOrder sysOrder = prodOrderService.queryOrderByOrderCode(orderCode, loginUser);
        new OrderCheck(sysOrder).checkNull("订单不存在").checkCanCheck("订单已经审核");
        //判断是否有工序
     /*   if (prodOrderStepService.countByOrderNo(orderCode, loginUser) == 0) {
            throw BusinessExceptionFactory.newException("工序未配置");
        }*/
        OrderBuilder orderBuilder = new OrderBuilder();
        orderBuilder.appendCode(orderCode).appendEdit(loginUser).appendStatus(OrderStatusEnum.PASS.getValue());
        //开启订单信息
        if (!prodOrderService.edit(orderBuilder.bulid(), loginUser)) {
            log.info("审核订单接口异常,参数:{}", JSON.toJSONString(orderBuilder.bulid()));
            throw BusinessExceptionFactory.newException("订单审核异常");
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, orderBuilder.getCode());
    }


    /**
     * 取消订单
     *
     * @param orderCode 订单编码
     * @return Result
     */
    @Transactional
    public Result<String> cancelOrder(String orderCode) {
        ValidateUtil.hasLength(orderCode, "参数[orderCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        ProdOrder sysOrder = prodOrderService.queryOrderByOrderCode(orderCode, loginUser);
        new OrderCheck(sysOrder).checkNull("订单不存在").checkCanCancel("订单不能取消");
        OrderBuilder orderBuilder = new OrderBuilder();
        orderBuilder.appendCode(orderCode).appendEdit(loginUser).appendStatus(OrderStatusEnum.CANCEL.getValue());
        //开启订单信息
        if (!prodOrderService.edit(orderBuilder.bulid(), loginUser)) {
            log.info("取消订单接口异常,参数:{}", JSON.toJSONString(orderBuilder.bulid()));
            throw BusinessExceptionFactory.newException("取消审核异常");
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, orderBuilder.getCode());
    }


    /**
     * 安排生产订单
     *
     * @param orderCode 订单编码
     * @return Result
     */
    @Transactional
    public Result<String> openOrder(String orderCode) {
        ValidateUtil.hasLength(orderCode, "参数[orderCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        ProdOrder sysOrder = prodOrderService.queryOrderByOrderCode(orderCode, loginUser);
        new OrderCheck(sysOrder).checkNull(SysMsgCodeConstant.Error.ERR10000002).checkCanProd(SysMsgCodeConstant.Error.ERR10000025);
        OrderBuilder orderBuilder = new OrderBuilder();
        //默认审核中
        orderBuilder.appendCode(orderCode).appendEdit(loginUser).appendStatus(OrderStatusEnum.PROD.getValue());
        //开启订单信息
        if (!prodOrderService.edit(orderBuilder.bulid(), loginUser)) {
            log.info("安排订单接口异常,参数:{}", JSON.toJSONString(orderBuilder.bulid()));
            throw BusinessExceptionFactory.newException("安排订单异常");
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, orderBuilder.getCode());
    }

    /**
     * 完成订单
     *
     * @param orderCode 订单编码
     * @return Result
     */
    @Transactional
    public Result<String> closeOrder(String orderCode) {
        ValidateUtil.hasLength(orderCode, "参数[orderCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        ProdOrder sysOrder = prodOrderService.queryOrderByOrderCode(orderCode, loginUser);
        new OrderCheck(sysOrder).checkNull(SysMsgCodeConstant.Error.ERR10000002);
        OrderBuilder orderBuilder = new OrderBuilder();
        orderBuilder.appendCode(orderCode).appendEdit(loginUser).appendStatus(OrderStatusEnum.FINISH.getValue());
        //关闭订单信息
        if (!prodOrderService.edit(orderBuilder.bulid(), loginUser)) {
            log.info("完成订单接口异常,参数:{}", JSON.toJSONString(orderBuilder.bulid()));
            throw BusinessExceptionFactory.newException("完成订单");
        }
        //完成全部加工单号
        prodBusinessService.finishOrderByOrderNo(orderCode, loginUser);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, orderBuilder.getCode());
    }

    /**
     * 订单详情
     *
     * @param orderCode 订单编码
     * @return Result
     */
    public Result<OrderVO> viewOrder(String orderCode, Boolean showTotal) {
        ValidateUtil.hasLength(orderCode, "参数[orderCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        ProdOrder sysOrder = prodOrderService.queryOrderByOrderCode(orderCode, loginUser);
        new OrderCheck(sysOrder).checkNull(SysMsgCodeConstant.Error.ERR10000002);
        BaseCustomer baseCustomer = baseCustomerService.queryCustomerByCustomerCode(sysOrder.getCustomerCode(), loginUser);
        //查询用户信息
        SysUser sysUser = sysUserService.queryUserByUserCode(sysOrder.getOwnerCode(), loginUser);
        OrderVOBuilder orderVOBuilder = new OrderVOBuilder();
        orderVOBuilder.append(sysOrder).append(baseCustomer).append(sysUser);
        OrderVO orderVO = orderVOBuilder.bulid();
        //卷数
        List<Integer> totals = new ArrayList<>();
        //数量
        List<BigDecimal> qiantitys = new ArrayList<>();
        //获取商品列表
        List<OrderGoodsVO> goodsVOS = prodOrderGoodsService.list(orderCode, loginUser).stream().map(prodOrderGoods -> {
            OrderGoodsVOBuilder orderStepBuilder = new OrderGoodsVOBuilder();
            MaterialModelVO materialModelVO = goodsService.queryGoodsByCode(prodOrderGoods.getGoodsCode(), loginUser);
            orderStepBuilder.append(materialModelVO);
            orderStepBuilder.append(prodOrderGoods);
            totals.add(prodOrderGoods.getTotal());
            qiantitys.add(prodOrderGoods.getFinishQuantity());
            return orderStepBuilder.bulid();
        }).collect(Collectors.toList());
        //合计总数
        if (ObjectUtil.isNotNull(showTotal) && showTotal) {
            goodsVOS.add(new OrderGoodsVOBuilder().appendTotalRow(totals, qiantitys).bulid());
        }
        orderVO.setGoods(goodsVOS);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, orderVO);
    }

    /**
     * 订单信息
     *
     * @param orderNo
     * @return
     */
    public Result<CustomerPrintVO> viewCustomerPrint(String orderNo) {
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        ProdOrder sysOrder = prodOrderService.queryOrderByOrderCode(orderNo, loginUser);
        new OrderCheck(sysOrder).checkNull(SysMsgCodeConstant.Error.ERR10000002);
        CustomerPrintVO customerVO = new CustomerPrintVO();
        BaseCustomerPrint customerPrint = customerPrintService.queryCustomerByCustomerCode(orderNo, loginUser);
        customerVO.setCustomerCode(orderNo);
        if (ObjectUtil.isNull(customerPrint)) {
            customerPrint = customerPrintService.queryCustomerByCustomerCode(sysOrder.getCustomerCode(), loginUser);
            customerVO.setCustomerCode(sysOrder.getCustomerCode());
        }
        if (ObjectUtil.isNotNull(customerPrint)) {
            customerVO.setTemplate(customerPrint.getTemplate());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, customerVO);
    }

    /**
     * 查询订单信息
     *
     * @param orderNo
     * @return
     */
    public Result<List<OrderGoodsCountVO>> countOrder(String orderNo) {
        Assert.hasLength(orderNo, "参数[orderNo]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取订单商品信息
        List<ProdOrderGoods> orderGoods = prodOrderGoodsService.list(orderNo, loginUser);
        List<OrderGoodsCountVO> goodsCountVOS = orderGoods.stream().map(orderGood -> {
            OrderGoodsCountVO orderGoodsCountVO = new OrderGoodsCountVO();
            orderGoodsCountVO.put("goodsCode", orderGood.getGoodsCode());
            orderGoodsCountVO.put("quantity", orderGood.getQuantity());
            orderGoodsCountVO.put("finishQuantity", orderGood.getFinishQuantity());
            orderGoodsCountVO.put("sendQuantity", orderGood.getSendQuantity());
            //查询详情信息
            MaterialModelVO materialModelVO = goodsService.queryGoodsByCode(orderGood.getGoodsCode(), loginUser);
            if (ObjectUtil.isNotNull(materialModelVO)) {
                orderGoodsCountVO.put("goodsName", materialModelVO.getMaterialName());
                orderGoodsCountVO.put("modelName", materialModelVO.getModelName());
                orderGoodsCountVO.put("modelProp", materialModelVO.getModelProp());
                orderGoodsCountVO.put("unit", materialModelVO.getUnit());
            }
            //查询统计数据
            prodBusinessService.countInQuantity(orderNo, orderGood.getGoodsCode(), loginUser).forEach(businessInCountQuantityDTO -> {
                orderGoodsCountVO.put(businessInCountQuantityDTO.getStepCode()+"1", String.valueOf(businessInCountQuantityDTO.getQuantity()));
            });
            //查询统计数据
            prodBusinessService.countAllQuantity(orderNo, orderGood.getGoodsCode(), loginUser).forEach(businessInCountQuantityDTO -> {
                orderGoodsCountVO.put(businessInCountQuantityDTO.getStepCode()+"0", String.valueOf(businessInCountQuantityDTO.getQuantity()));
            });
            return orderGoodsCountVO;
        }).collect(Collectors.toList());
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, goodsCountVOS);

    }

    public Result<IPage<OrderGoodsVO>> customerGoods(@Valid CommonPage<OrderGoodsQueryParams, Page<OrderGoodsVO>> queryParams) {
        Assert.notNull(queryParams, SysMsgCodeConstant.Error.ERR10000001);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        Map<String, BaseCustomer> baseCustomerMap = new HashMap<>();
        CommonPageBuilder<OrderGoodsQueryDTO, ProdOrderGoods> commonPageBuilder = new CommonPageBuilder<>(OrderGoodsQueryDTO.class);
        commonPageBuilder.appendEntity(queryParams.getEntity()).appendQuery(queryParams.getQuery()).appendPage(queryParams.getPage());
        IPage<ProdOrderGoods> page = prodOrderGoodsService.page(commonPageBuilder.bulid(), loginUser);
        List<ProdOrderGoods> orders = page.getRecords();
        //结果对象
        IPage<OrderGoodsVO> pageList = new Page<>();
        pageList.setTotal(page.getTotal());
        pageList.setCurrent(page.getCurrent());
        pageList.setSize(page.getSize());
        //查询到结果 数据转换
        if (ObjectUtil.isNotEmpty(orders)) {
            pageList.setRecords(orders.stream().map(orderGoods -> {
                OrderGoodsVOBuilder orderGoodsVOBuilder = new OrderGoodsVOBuilder();
                MaterialModelVO materialModelVO = goodsService.queryGoodsByCode(orderGoods.getGoodsCode(), loginUser);
                orderGoodsVOBuilder.append(orderGoods).append(materialModelVO);
                return orderGoodsVOBuilder.bulid();
            }).collect(Collectors.toList()));
        } else {
            pageList.setRecords(new ArrayList<>());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, pageList);
    }
}
