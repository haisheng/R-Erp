package com.ray.business.enums;

import lombok.Getter;
import org.springframework.util.StringUtils;

/**
 * @author bo shen
 * @Description: 对账单类型类型
 * @Class: DeductionStatusEnum
 * @Package com.ray.business.enums
 * @date 2020/5/27 10:50
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Getter
public enum DeductionStatusEnum {
    UN_CHECK("UN_CHECK", "未审核"),
    PASS("PASS", "审核通过"),
    FINANCE("FINANCE", "已对账");

    DeductionStatusEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    /***值***/
    private String value;
    /***名称描述***/
    private String name;

    /**
     * 获取枚举对象
     *
     * @param value 值
     * @return
     */
    public static DeductionStatusEnum getEnum(String value) {
        for (DeductionStatusEnum entity : DeductionStatusEnum.values()) {
            if (StringUtils.pathEquals(value, entity.value)) {
                return entity;
            }
        }
        return null;
    }

    /**
     * value是否在列表中
     *
     * @param value
     * @return
     */
    public static boolean valueInEnum(String value) {
        for (DeductionStatusEnum entity : DeductionStatusEnum.values()) {
            if (StringUtils.pathEquals(value, entity.value)) {
                return true;
            }
        }
        return false;
    }
}
