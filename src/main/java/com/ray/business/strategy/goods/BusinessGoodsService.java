package com.ray.business.strategy.goods;

import cn.hutool.core.util.ObjectUtil;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.service.compose.TechnologyService;
import com.ray.base.table.entity.BaseTechnologyRecord;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.builder.BusinessGoodsVOBuilder;
import com.ray.business.check.OrderStepCheck;
import com.ray.business.check.StepCheck;
import com.ray.business.service.ProdBusinessOutService;
import com.ray.business.service.ProdBusinessService;
import com.ray.business.service.ProdOrderStepService;
import com.ray.business.service.ProdStepService;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdBusinessOut;
import com.ray.business.table.entity.ProdOrderStep;
import com.ray.business.table.entity.ProdStep;
import com.ray.business.table.params.business.quick.BusinessQuickGoodsQueryParams;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.common.check.AbstractCheck;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.util.CodeSplitUtil;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.Assert;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 订单商品数据
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "businessGoods", strategy = "BUSINESS", desc = "加工信息")
public class BusinessGoodsService implements Strategy<BusinessQuickGoodsQueryParams, LoginUser, BusinessGoodsVO> {

    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ProdOrderStepService prodOrderStepService;
    @Autowired
    private ProdBusinessService prodBusinessService;
    @Autowired
    private ProdBusinessOutService prodBusinessOutService;
    @Autowired
    private ProdStepService prodStepService;

    @Override
    public BusinessGoodsVO execute(BusinessQuickGoodsQueryParams queryParams, LoginUser loginUser) {
        ProdOrderStep prodOrderStep = prodOrderStepService.queryStepByStepCode(CodeSplitUtil.getFirst(queryParams.getGoodsCode())
                , queryParams.getStepCode(), loginUser);
        new OrderStepCheck(prodOrderStep).checkNull("订单配置步骤数据不存在");
        //查询步骤
        ProdStep prodStep = prodStepService.queryStepByStepCode(queryParams.getStepCode(), loginUser);
        new StepCheck(prodStep).checkNull("工序不存在");
        //查询订单商品
        MaterialModelVO goodsModelVo = goodsService.queryGoodsByCode(queryParams.getGoodsCode(), loginUser);
        new AbstractCheck<>(goodsModelVo).checkNull("订单商品不存在");
        //查询订单对应的采购订单
        List<ProdBusiness> prodBusinesses = prodBusinessService.listByOrderNo(queryParams.getOrderNo(), queryParams.getStepCode(),
                queryParams.getCustomerCode(), queryParams.getGoodsCode(), loginUser);
        if (ObjectUtil.isNotEmpty(prodBusinesses)) {
            //查询
            List<String> businessCodes = prodBusinesses.stream().map(ProdBusiness::getBusinessCode).collect(Collectors.toList());
            //查询出库单信息
            ProdBusinessOut prodBusinessOut = prodBusinessOutService.queryByNumber(businessCodes, queryParams.getSerialNumber(), loginUser);
            if (ObjectUtil.isNull(prodBusinessOut)) {
                throw BusinessExceptionFactory.newException("无法查询到该序列号的商品");
            }
            //查询商品详情
            MaterialModelVO modelVO = null;
            Boolean appendUnit = false;
            if (prodStep.getGoodsStatus() == YesOrNoEnum.YES.getValue().intValue()) {
                modelVO = goodsModelVo;
                appendUnit = true;
            } else {
                //工序是产品变化
                modelVO = goodsService.queryGoodsByCode(prodBusinessOut.getGoodsCode(), loginUser);
            }
            //商品构建
            BusinessGoodsVOBuilder businessGoodsVOBuilder = new BusinessGoodsVOBuilder()
                    .append(modelVO).append(prodBusinessOut);
            if (appendUnit) {
                businessGoodsVOBuilder.appendUnit(modelVO.getUnit());
            }
            return businessGoodsVOBuilder.bulid();
        }
        return null;
    }
}
