package com.ray.business.strategy.goods;

import cn.hutool.core.util.ObjectUtil;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.service.compose.TechnologyService;
import com.ray.base.table.entity.BaseTechnologyRecord;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.builder.BusinessGoodsVOBuilder;
import com.ray.business.service.compose.BusinessService;
import com.ray.business.table.params.business.quick.BusinessQuickGoodsQueryParams;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 订单商品数据
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "businessGoods", strategy = "MB", desc = "面布信息")
public class MbGoodsService implements Strategy<BusinessQuickGoodsQueryParams, LoginUser, BusinessGoodsVO> {

    @Autowired
    private GoodsService goodsService;
    @Autowired
    private TechnologyService technologyService;
    @Autowired
    private BusinessService businessService;

    @Override
    public BusinessGoodsVO execute(BusinessQuickGoodsQueryParams queryParams, LoginUser loginUser) {
        //查询面布信息
        BaseTechnologyRecord baseTechnologyRecord = technologyService.queryRecord(queryParams.getGoodsCode(),"面布",loginUser);
        if(ObjectUtil.isNull(baseTechnologyRecord)){
            throw BusinessExceptionFactory.newException("技术工艺中未配置面布");
        }
        MaterialModelVO materialModelVO = goodsService.queryGoodsByCode(baseTechnologyRecord.getModelCode(),loginUser);
        BusinessGoodsVOBuilder businessGoodsVOBuilder = new BusinessGoodsVOBuilder();
        businessGoodsVOBuilder.append(materialModelVO).appendNumber(businessService.getNumber());
        return businessGoodsVOBuilder.bulid();
    }
}
