package com.ray.business.strategy.goods;

import com.ray.base.service.compose.GoodsService;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.builder.BusinessGoodsVOBuilder;
import com.ray.business.table.params.business.quick.BusinessQuickGoodsQueryParams;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 订单商品数据
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "businessGoods", strategy = "ORDER", desc = "订单信息")
public class OrderProdGoodsService implements Strategy<BusinessQuickGoodsQueryParams, LoginUser, BusinessGoodsVO> {

    @Autowired
    private GoodsService goodsService;

    @Override
    public BusinessGoodsVO execute(BusinessQuickGoodsQueryParams businessQuickGoodsQueryParams, LoginUser loginUser) {
        MaterialModelVO materialModelVO = goodsService.queryGoodsByCode(businessQuickGoodsQueryParams.getGoodsCode(),loginUser);
        BusinessGoodsVOBuilder businessGoodsVOBuilder = new BusinessGoodsVOBuilder();
        businessGoodsVOBuilder.append(materialModelVO);
        return businessGoodsVOBuilder.bulid();
    }
}
