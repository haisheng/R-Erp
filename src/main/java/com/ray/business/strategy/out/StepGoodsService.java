package com.ray.business.strategy.out;

import cn.hutool.core.util.ObjectUtil;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.builder.BusinessGoodsVOBuilder;
import com.ray.business.service.ProdBusinessInService;
import com.ray.business.service.ProdBusinessService;
import com.ray.business.service.ProdOrderStepService;
import com.ray.business.table.dto.BusinessQueryDTO;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdBusinessIn;
import com.ray.business.table.entity.ProdOrderStep;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.common.SysMsgCodeConstant;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.util.CodeSplitUtil;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.util.Assert;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 订单商品数据
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "outGoods", strategy = "step", desc = "工艺商品物料查询")
public class StepGoodsService implements Strategy<BusinessQueryDTO, LoginUser, List<BusinessGoodsVO>> {

    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ProdOrderStepService prodOrderStepService;
    @Autowired
    private ProdBusinessInService prodBusinessInService;
    @Autowired
    private ProdBusinessService prodBusinessService;

    @Override
    public List<BusinessGoodsVO> execute(BusinessQueryDTO businessQueryDTO, LoginUser loginUser) {
        Assert.notNull(businessQueryDTO, SysMsgCodeConstant.Error.ERR10000001);
        Assert.notNull(businessQueryDTO.getOrderStep(), SysMsgCodeConstant.Error.ERR10000001);
        ProdOrderStep thisStep = businessQueryDTO.getOrderStep();
        //查询步骤编码
        ProdOrderStep perOrderStep = prodOrderStepService.queryStepByIndexSort(CodeSplitUtil.getFirst(businessQueryDTO.getGoodsCode()),
                thisStep.getIndexSort()-1,loginUser);
        //查询订单对应业务单号
        List<ProdBusiness> prodBusinesses = prodBusinessService.listInBusiness(businessQueryDTO.getOrderNo(),
                perOrderStep.getStepCode(),businessQueryDTO.getGoodsCode(), loginUser);
        if(ObjectUtil.isNotNull(perOrderStep) || ObjectUtil.isNotNull(prodBusinesses)){
            //查询
            List<String> businessCodes = prodBusinesses.stream().map(ProdBusiness::getBusinessCode).collect(Collectors.toList());
            List<BusinessGoodsVO> prodPurchaseRecords = prodBusinessInService.list(businessCodes, loginUser)
                    .stream().map(prodBusinessIn -> {
                        //查询商品详情
                        MaterialModelVO modelVO = goodsService.queryGoodsByCode(prodBusinessIn.getGoodsCode(),loginUser);
                        BusinessGoodsVO businessGoodsVO = new BusinessGoodsVOBuilder()
                                .append(modelVO).append(prodBusinessIn).bulid();
                        return  businessGoodsVO;
                    }).collect(Collectors.toList());
            return prodPurchaseRecords;
        }
        //没有上个步骤返回空
        return new ArrayList<>();
    }
}
