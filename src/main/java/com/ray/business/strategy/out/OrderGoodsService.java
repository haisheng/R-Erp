package com.ray.business.strategy.out;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.service.BaseTechnologyRecordService;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.service.compose.TechnologyService;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.builder.BusinessGoodsVOBuilder;
import com.ray.business.service.*;
import com.ray.business.table.dto.BusinessQueryDTO;
import com.ray.business.table.entity.*;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.CommonValue;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.util.Assert;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.math.BigDecimal;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 订单商品数据
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "outGoods", strategy = "order", desc = "商品物料查询")
public class OrderGoodsService implements Strategy<BusinessQueryDTO, LoginUser, List<BusinessGoodsVO>> {

    @Autowired
    private ProdPurchaseService prodPurchaseService;
    @Autowired
    private ProdPurchaseInRecordService prodPurchaseInRecordService;
    @Autowired
    private ProdPurchaseBackRecordService prodPurchaseBackRecordService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ProdBusinessService prodBusinessService;
    @Autowired
    private ProdBusinessOutService prodBusinessOutService;
    @Autowired
    private ProdPurchaseBackService prodPurchaseBackService;
    @Autowired
    private ProdPurchaseInService prodPurchaseInService;
    @Autowired
    private TechnologyService technologyService;

    @Override
    public List<BusinessGoodsVO> execute(BusinessQueryDTO businessQueryDTO, LoginUser loginUser) {
        Assert.hasLength(businessQueryDTO.getGoodsCode(), "原料信息不存在");
        //查询订单对应的采购订单
        List<ProdPurchase> prodPurchases = prodPurchaseService.listByBusinessOrderNo(businessQueryDTO.getOrderNo(), loginUser);
        //查询对应的出库单数量
        List<ProdBusiness> list = prodBusinessService.listUsedByOrderNo(businessQueryDTO.getOrderNo(), businessQueryDTO.getStepCode(), loginUser);
        //查询已出库数量
        List<String> businessCodes = list.parallelStream().map(ProdBusiness::getBusinessCode).collect(Collectors.toList());
        //查询订单原料
        List<String> modelCodes = technologyService.queryTechnologyRecordByModelCode(businessQueryDTO.getGoodsCode(), loginUser).stream().map(baseTechnologyRecord -> {
            return baseTechnologyRecord.getModelCode();
        }).collect(Collectors.toList());
        //没有技术工艺原料
        if (ObjectUtil.isEmpty(modelCodes)) {
            //没有采购订单返回空
            return new ArrayList<>();
        }
        //查询出库物料
        final Map<String, BigDecimal> bigDecimalMap = prodBusinessOutService.list(businessCodes, loginUser).stream().collect(Collectors.groupingBy(prodBusinessOut -> {
                    return String.format("%s|%s|%s|%s", prodBusinessOut.getGoodsCode(), prodBusinessOut.getBatchNo(),
                            prodBusinessOut.getSerialNumber(), prodBusinessOut.getUnit());
                },
                Collectors.reducing(BigDecimal.ZERO, ProdBusinessOut::getQuantity, BigDecimal::add)));
        if (ObjectUtil.isNotEmpty(prodPurchases)) {
            List<String> orderNos = prodPurchases.stream().map(ProdPurchase::getOrderNo).collect(Collectors.toList());
            //查询采购单对应的退回单
            List<ProdPurchaseBack> prodPurchaseBacks = prodPurchaseBackService.listFinishByOrderNos(orderNos, loginUser);
            List<String> backCodes = prodPurchaseBacks.parallelStream().map(ProdPurchaseBack::getBackCode).collect(Collectors.toList());
            //查询生效的入库单
            List<ProdPurchaseIn> prodPurchaseIns = prodPurchaseInService.listFinishByOrderNos(orderNos, loginUser);
            List<String> inCodes = prodPurchaseIns.parallelStream().map(ProdPurchaseIn::getInCode).collect(Collectors.toList());
            //查询订单对应的采购数量
            List<BusinessGoodsVO> prodPurchaseRecords = prodPurchaseInRecordService.list(orderNos, inCodes, modelCodes, loginUser)
                    .stream().map(prodPurchaseRecord -> {
                        //查询商品详情
                        MaterialModelVO modelVO = goodsService.queryGoodsByCode(prodPurchaseRecord.getGoodsCode(), loginUser);
                        //查询订单退回数量
                        BigDecimal backQuantity = prodPurchaseBackRecordService.queryBackQuantity(prodPurchaseRecord.getGoodsCode(), prodPurchaseRecord.getUnit()
                                , backCodes, loginUser);
                        //查询已出库数量
                        String key = String.format("%s|%s|%s|%s", prodPurchaseRecord.getGoodsCode(), prodPurchaseRecord.getBatchNo(),
                                prodPurchaseRecord.getSerialNumber(), prodPurchaseRecord.getUnit());
                        BigDecimal outQuantity = NumberUtil.null2Zero(bigDecimalMap.get(key));
                        BusinessGoodsVO businessGoodsVO = new BusinessGoodsVOBuilder()
                                .append(modelVO).append(prodPurchaseRecord).sub(backQuantity.add(outQuantity)).bulid();
                        return businessGoodsVO;
                    }).filter(businessGoodsVO -> {
                        return businessGoodsVO.getQuantity().compareTo(new BigDecimal(0)) > 0;
                    }).collect(Collectors.toList());
            return prodPurchaseRecords;
        }
        //没有采购订单返回空
        return new ArrayList<>();
    }
}
