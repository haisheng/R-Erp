package com.ray.business.strategy.order.delete;

import cn.hutool.core.util.StrUtil;
import com.ray.business.builder.BusinessBuilder;
import com.ray.business.builder.BusinessOutBuilder;
import com.ray.business.check.BusinessInCheck;
import com.ray.business.service.ProdBusinessInService;
import com.ray.business.service.ProdBusinessOutService;
import com.ray.business.service.ProdBusinessService;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdBusinessIn;
import com.ray.business.table.entity.ProdBusinessOut;
import com.ray.business.table.params.business.BusinessEditParams;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 订单商品数据
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "deleteBusiness", strategy = "out", desc = "出库业务删除")
public class OutOrderDeleteService implements Strategy<ProdBusiness, LoginUser, String> {

    @Autowired
    private ProdBusinessService prodBusinessService;
    @Autowired
    private ProdBusinessInService prodBusinessInService;
    @Autowired
    private ProdBusinessOutService prodBusinessOutService;

    @Override
    public String execute(ProdBusiness prodBusiness, LoginUser loginUser) {
        BusinessBuilder businessBuilder = new BusinessBuilder();
        businessBuilder.appendCode(prodBusiness.getBusinessCode()).appendEdit(loginUser).delete();
        //更新
        prodBusinessService.edit(businessBuilder.bulid(), loginUser);
        //还原对应出库的状态
        prodBusinessOutService.listAll(prodBusiness.getBusinessCode(), loginUser).forEach(prodBusinessOut -> {
            //还原数据
            if (StrUtil.isNotBlank(prodBusinessOut.getInCode())) {
                prodBusinessInService.cancelBusinessIn(prodBusinessOut.getInCode(), loginUser);
            }
        });
        //删除原有的记录
        prodBusinessOutService.deleteByBusinessCode(prodBusiness.getBusinessCode(), loginUser);
        return businessBuilder.getCode();
    }
}
