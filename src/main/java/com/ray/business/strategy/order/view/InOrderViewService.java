package com.ray.business.strategy.order.view;

import cn.hutool.core.util.ObjectUtil;
import com.ray.base.service.BaseCustomerService;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.builder.BusinessBuilder;
import com.ray.business.builder.BusinessGoodsVOBuilder;
import com.ray.business.builder.BusinessVOBuilder;
import com.ray.business.enums.BusinessStatusEnum;
import com.ray.business.service.ProdBusinessInService;
import com.ray.business.service.ProdBusinessService;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdBusinessIn;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.business.table.vo.BusinessVO;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.system.service.SysUserService;
import com.ray.system.table.entity.SysUser;
import com.ray.wms.builder.GoodsBuilder;
import com.ray.wms.service.WmsWarehouseService;
import com.ray.wms.service.compose.InService;
import com.ray.wms.table.dto.GoodsDTO;
import com.ray.wms.table.dto.OrderCreateDTO;
import com.ray.wms.table.entity.WmsWarehouse;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 订单商品数据
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "viewBusiness", strategy = "in", desc = "业务详情")
public class InOrderViewService implements Strategy<ProdBusiness, LoginUser, BusinessVO> {

    @Autowired
    private ProdBusinessInService prodBusinessInService;
    @Autowired
    private BaseCustomerService baseCustomerService;
    @Autowired
    private WmsWarehouseService wmsWarehouseService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private SysUserService sysUserService;


    @Override
    public BusinessVO execute(ProdBusiness prodBusiness, LoginUser loginUser) {
        BusinessVOBuilder businessVOBuilder = new BusinessVOBuilder();
        businessVOBuilder.append(prodBusiness);
        //查询客户信息
        BaseCustomer baseCustomer = baseCustomerService.queryCustomerByCustomerCode(prodBusiness.getCustomerCode(),loginUser);
        businessVOBuilder.append(baseCustomer);
        WmsWarehouse wmsWarehouse = wmsWarehouseService.queryWarehouseByWarehouseCode(prodBusiness.getWarehouseCode(),loginUser);
        businessVOBuilder.append(wmsWarehouse);
        Map<String, MaterialModelVO> modelMap = new HashMap<>();
        List<BusinessGoodsVO> goodsVOS = prodBusinessInService.listAll(prodBusiness.getBusinessCode(),loginUser)
                .stream().map(prodBusinessIn -> {
                    BusinessGoodsVOBuilder businessGoodsVOBuilder = new BusinessGoodsVOBuilder();
                    MaterialModelVO materialModelVO = modelMap.get(prodBusinessIn.getGoodsCode());
                    if (ObjectUtil.isNull(materialModelVO)) {
                        materialModelVO = goodsService.queryGoodsByCode(prodBusinessIn.getGoodsCode(), loginUser);
                        modelMap.put(prodBusinessIn.getGoodsCode(),materialModelVO);
                    }
                    businessGoodsVOBuilder.append(materialModelVO);
                    businessGoodsVOBuilder.append(prodBusinessIn);
                    return businessGoodsVOBuilder.bulid();
                }).collect(Collectors.toList());
        businessVOBuilder.append(goodsVOS);
        //查询人员信息
        SysUser user = sysUserService.queryUserByUserCode(prodBusiness.getEmployee(),loginUser);
        businessVOBuilder.appendEmployee(user);
        //查询人员信息
        SysUser usera = sysUserService.queryUserByUserCode(prodBusiness.getEmployeea(),loginUser);
        businessVOBuilder.appendEmployeeA(usera);
        //查询加工商品信息
        MaterialModelVO  goods = goodsService.queryGoodsByCode(prodBusiness.getGoodsCode(), loginUser);
        businessVOBuilder.append(goods);
        return businessVOBuilder.bulid();
    }
}
