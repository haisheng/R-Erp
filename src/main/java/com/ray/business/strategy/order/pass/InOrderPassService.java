package com.ray.business.strategy.order.pass;

import cn.hutool.core.util.StrUtil;
import com.ray.business.builder.BusinessBuilder;
import com.ray.business.enums.BusinessStatusEnum;
import com.ray.business.service.ProdBusinessInService;
import com.ray.business.service.ProdBusinessOutService;
import com.ray.business.service.ProdBusinessService;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdBusinessIn;
import com.ray.business.table.entity.ProdPurchaseRecord;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.wms.builder.GoodsBuilder;
import com.ray.wms.service.compose.InService;
import com.ray.wms.table.dto.GoodsDTO;
import com.ray.wms.table.dto.OrderCreateDTO;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 订单商品数据
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "passBusiness", strategy = "in", desc = "审核业务")
public class InOrderPassService implements Strategy<ProdBusiness, LoginUser, String> {

    @Autowired
    private ProdBusinessService prodBusinessService;
    @Autowired
    private ProdBusinessInService prodBusinessInService;
    @Autowired
    private InService inService;

    @Override
    public String execute(ProdBusiness prodBusiness, LoginUser loginUser) {
        BusinessBuilder businessBuilder = new BusinessBuilder();
        businessBuilder.appendCode(prodBusiness.getBusinessCode()).appendEdit(loginUser).appendStatus(BusinessStatusEnum.UN_IN.getValue());
        //更新
        prodBusinessService.edit(businessBuilder.bulid(), loginUser);
        OrderCreateDTO orderCreateDTO = new OrderCreateDTO();
        orderCreateDTO.setBusinessOrderNo(prodBusiness.getOrderNo());
        orderCreateDTO.setBusinessCode(prodBusiness.getBusinessCode());
        orderCreateDTO.setWarehouseCode(prodBusiness.getWarehouseCode());
        //查询订单商品
        List<ProdBusinessIn> records = prodBusinessInService.listAll(prodBusiness.getBusinessCode(), loginUser);
        List<GoodsDTO> goodsDTOS = records.stream().map(prodBusinessIn -> {
            return new GoodsBuilder().append(prodBusinessIn).appendOrderNo(prodBusiness.getOrderNo()).bulid();
        }).collect(Collectors.toList());
        orderCreateDTO.setGoodsDTOS(goodsDTOS);
        //创建订单
        inService.createOrderIn(orderCreateDTO, loginUser);
        return businessBuilder.getCode();
    }
}
