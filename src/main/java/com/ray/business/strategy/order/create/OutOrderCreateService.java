package com.ray.business.strategy.order.create;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.ray.business.builder.BusinessBuilder;
import com.ray.business.builder.BusinessOutBuilder;
import com.ray.business.check.BusinessInCheck;
import com.ray.business.enums.BusinessStatusEnum;
import com.ray.business.enums.BusinessTypeEnum;
import com.ray.business.service.ProdBusinessInService;
import com.ray.business.service.ProdBusinessOutService;
import com.ray.business.service.ProdBusinessService;
import com.ray.business.table.entity.ProdBusinessIn;
import com.ray.business.table.entity.ProdBusinessOut;
import com.ray.business.table.params.business.BusinessCreateParams;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 订单商品数据
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "createBusiness", strategy = "out", desc = "工艺新增")
public class OutOrderCreateService implements Strategy<BusinessCreateParams, LoginUser, String> {

    @Autowired
    private ProdBusinessService prodBusinessService;
    @Autowired
    private ProdBusinessInService prodBusinessInService;
    @Autowired
    private ProdBusinessOutService prodBusinessOutService;

    @Override
    public String execute(BusinessCreateParams createParams, LoginUser loginUser) {
        BusinessBuilder businessBuilder = new BusinessBuilder();
        businessBuilder.append(createParams).appendType(BusinessTypeEnum.OUT.getValue())
                .appendStatus(BusinessStatusEnum.UN_CHECK.getValue()).appendStatus(YesOrNoEnum.NO.getValue()).appendCreate(loginUser);
        final List<BigDecimal> quantity = new ArrayList<>();
        //保存物料明细
        List<String> keys = new ArrayList<>();
        //保存物料明细
        List<ProdBusinessOut> outList = createParams.getGoods().stream().map(businessGoodsParams -> {
            if (keys.contains(businessGoodsParams.getKey())) {
                log.info("物料重复:{}", JSON.toJSONString(businessGoodsParams));
                throw BusinessExceptionFactory.newException("物料重复");
            }
            keys.add(businessGoodsParams.getKey());
            BusinessOutBuilder businessOutBuilder = new BusinessOutBuilder().append(businessGoodsParams)
                    .appendBusinessCode(businessBuilder.getCode()).appendOrderNo(createParams.getOrderNo()).appendCreate(loginUser);
            //判断是否关联唯一单号
            if (StrUtil.isNotBlank(businessGoodsParams.getCode())) {
                //查询数据信息
                ProdBusinessIn businessIn = prodBusinessInService.getUsableGoodsByCode(businessGoodsParams.getCode(), loginUser);
                new BusinessInCheck(businessIn).checkNull("数据不存在");
                //更新使用状态
                if (!prodBusinessInService.useBusinessIn(businessGoodsParams.getCode(), businessIn.getUpdateVersion(), loginUser)) {
                    log.info("更新记录失败:{}", businessGoodsParams.getCode());
                    throw BusinessExceptionFactory.newException("更新记录异常");
                }
                businessOutBuilder.appendInCode(businessIn.getCode()).appendGangNo(businessIn.getGangNo());
            }
            quantity.add(businessGoodsParams.getQuantity());
            return businessOutBuilder.bulid();
        }).collect(Collectors.toList());
        //保存总数
        businessBuilder.append(quantity.stream().reduce(BigDecimal::add).get());
        //保存
        prodBusinessService.save(businessBuilder.bulid());
        //保存数据
        prodBusinessOutService.saveBatch(outList);
        return businessBuilder.getCode();
    }
}
