package com.ray.business.strategy.order.cancel;

import cn.hutool.core.util.StrUtil;
import com.ray.business.builder.BusinessBuilder;
import com.ray.business.enums.BusinessStatusEnum;
import com.ray.business.service.ProdBusinessInService;
import com.ray.business.service.ProdBusinessOutService;
import com.ray.business.service.ProdBusinessService;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.wms.service.compose.OutService;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 订单商品数据
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "cancelBusiness", strategy = "out", desc = "出库业务取消")
public class OutOrderCancelService implements Strategy<ProdBusiness, LoginUser, String> {

    @Autowired
    private ProdBusinessService prodBusinessService;
    @Autowired
    private ProdBusinessInService prodBusinessInService;
    @Autowired
    private ProdBusinessOutService prodBusinessOutService;
    @Autowired
    private OutService outService;

    @Override
    public String execute( ProdBusiness prodBusiness, LoginUser loginUser) {
        BusinessBuilder businessBuilder = new BusinessBuilder();
        businessBuilder.appendCode(prodBusiness.getBusinessCode()).appendEdit(loginUser).appendStatus(BusinessStatusEnum.CANCEL.getValue());
        //更新
        prodBusinessService.edit(businessBuilder.bulid(), loginUser);
        //还原对应出库的状态
        prodBusinessOutService.listAll(prodBusiness.getBusinessCode(), loginUser).forEach(prodBusinessOut -> {
            //还原数据
            if (StrUtil.isNotBlank(prodBusinessOut.getInCode())) {
                prodBusinessInService.cancelBusinessIn(prodBusinessOut.getInCode(), loginUser);
            }
        });
        //删除原有的记录
      //  prodBusinessOutService.deleteByBusinessCode(businessCode, loginUser);
        //时效出入库
        outService.invalidOrderBusinessCode(prodBusiness.getBusinessCode(),loginUser);
        return businessBuilder.getCode();
    }
}
