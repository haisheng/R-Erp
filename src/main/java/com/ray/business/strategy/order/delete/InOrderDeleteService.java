package com.ray.business.strategy.order.delete;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.table.entity.BaseMaterialModel;
import com.ray.business.builder.BusinessBuilder;
import com.ray.business.builder.OrderDeleteIndexBuilder;
import com.ray.business.service.ProdBusinessInService;
import com.ray.business.service.ProdBusinessOutService;
import com.ray.business.service.ProdBusinessService;
import com.ray.business.service.ProdOrderDeleteIndexService;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdOrderDeleteIndex;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.util.CodeSplitUtil;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bo shen
 * @Description: 订单商品数据
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "deleteBusiness", strategy = "in", desc = "删除业务")
public class InOrderDeleteService implements Strategy<ProdBusiness, LoginUser, String> {

    @Autowired
    private ProdBusinessService prodBusinessService;
    @Autowired
    private ProdBusinessInService prodBusinessInService;
    @Autowired
    private ProdBusinessOutService prodBusinessOutService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ProdOrderDeleteIndexService prodOrderDeleteIndexService;

    @Override
    public String execute(ProdBusiness prodBusiness, LoginUser loginUser) {
        List<ProdOrderDeleteIndex> deleteIndexList = new ArrayList<>();
        BusinessBuilder businessBuilder = new BusinessBuilder();
        businessBuilder.appendCode(prodBusiness.getBusinessCode()).appendEdit(loginUser).delete();
        //更新
        prodBusinessService.edit(businessBuilder.bulid(), loginUser);
        //还原对应出库的状态
        prodBusinessInService.listAll(prodBusiness.getBusinessCode(), loginUser).forEach(prodBusinessIn -> {
            //还原数据
            if (StrUtil.isNotBlank(prodBusinessIn.getOutCode())) {
                prodBusinessOutService.cancelBusinessOut(prodBusinessIn.getOutCode(), loginUser);
            }
            //组装列表
            if(ObjectUtil.isNotNull(prodBusinessIn.getIndexSort())){
                deleteIndexList.add(new OrderDeleteIndexBuilder().appendOrderNo(prodBusiness.getOrderNo())
                        .appendGoodsCode(CodeSplitUtil.getFirst(prodBusiness.getGoodsCode()))
                        .appendCreate(loginUser).appendIndex(prodBusinessIn.getIndexSort()).bulid());
            }
        });
        //保存删除的序号
        if(ObjectUtil.isNotEmpty(deleteIndexList)){
            prodOrderDeleteIndexService.saveBatch(deleteIndexList);
        }
        //删除原有的记录
        prodBusinessInService.deleteByBusinessCode(prodBusiness.getBusinessCode(), loginUser);
        return businessBuilder.getCode();
    }
}
