package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdOrderDeleteIndex;
import com.ray.business.table.entity.ProdOrderIndex;
import com.ray.business.table.params.send.OrderSendEditParams;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Sale 构造器
 * @Class: SaleBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class OrderDeleteIndexBuilder extends AbstractBuilder<ProdOrderDeleteIndex, OrderDeleteIndexBuilder> {

    public OrderDeleteIndexBuilder() {
        super(new ProdOrderDeleteIndex());
        super.setBuilder(this);
    }

    @Override
    public ProdOrderDeleteIndex bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    /**
     * 添加状态
     *
     * @return
     */
    public OrderDeleteIndexBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public OrderDeleteIndexBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }
    /**
     * 添加编码
     *
     * @return
     */
    public OrderDeleteIndexBuilder appendGoodsCode(String goodsCode) {
        entity.setGoodsCode(goodsCode);
        return this;
    }


    @Override
    public String getCode() {
        return entity.getOrderNo();
    }

    public OrderDeleteIndexBuilder appendIndex(int index) {
        entity.setIndexSort(index);
        return this;
    }
}
