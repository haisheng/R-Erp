package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.business.table.entity.ProdBusinessBack;
import com.ray.business.table.entity.ProdStep;
import com.ray.business.table.vo.BusinessBackVO;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.common.builder.AbstractBuilder;
import com.ray.wms.table.entity.WmsWarehouse;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j

public class BusinessBackVOBuilder extends AbstractBuilder<BusinessBackVO, BusinessBackVOBuilder> {

    public BusinessBackVOBuilder() {
        super(new BusinessBackVO());
        super.setBuilder(this);
    }

    @Override
    public BusinessBackVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    @Override
    public String getCode() {
        return entity.getBusinessCode();
    }


    public BusinessBackVOBuilder append(ProdBusinessBack prodBusiness) {
        if (ObjectUtil.isNotNull(prodBusiness)) {
            BeanUtil.copyProperties(prodBusiness, entity);
            entity.setAmount(NumberUtil.null2Zero(prodBusiness.getPrice()).multiply(NumberUtil.null2Zero(prodBusiness.getQuantity())));
        }
        return this;
    }

    public BusinessBackVOBuilder append(WmsWarehouse warehouse) {
        if (ObjectUtil.isNotNull(warehouse)) {
            entity.setWarehouseName(warehouse.getWarehouseName());
        }
        return this;
    }

    public BusinessBackVOBuilder append(BaseCustomer customer) {
        if (ObjectUtil.isNotNull(customer)) {
            entity.setCustomerName(customer.getCustomerName());
        }
        return this;
    }


    public BusinessBackVOBuilder append(ProdStep step) {
        if (ObjectUtil.isNotNull(step)) {
            entity.setStepName(step.getStepName());
        }
        return this;
    }

    public BusinessBackVOBuilder append(List<BusinessGoodsVO> goodsVOS) {
        entity.setGoods(goodsVOS);
        return this;
    }

    public BusinessBackVOBuilder appendCustomerName(String customerName) {
        entity.setCustomerName(customerName);
        return this;
    }

}
