package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.business.table.entity.ProdPurchaseRecord;
import com.ray.business.table.params.purchase.PurchaseCreateParams;
import com.ray.business.table.params.purchase.PurchaseEditParams;
import com.ray.business.table.params.purchase.PurchaseGoodsParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * @author bo shen
 * @Description: Purchase 构造器
 * @Class: PurchaseBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class PurchaseRecordBuilder extends AbstractBuilder<ProdPurchaseRecord, PurchaseRecordBuilder> {

    public PurchaseRecordBuilder() {
        super(new ProdPurchaseRecord());
        super.setBuilder(this);
        entity.setCode(CodeCreateUtil.getCode(Perifx.RECORD_CODE));
    }

    @Override
    public ProdPurchaseRecord bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public PurchaseRecordBuilder append(PurchaseGoodsParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }


    /**
     *  添加编码
     * @return
     */
    public PurchaseRecordBuilder appendCode(String code) {
        entity.setOrderNo(code);
        return this;
    }


    @Override
    public String getCode() {
        return entity.getOrderNo();
    }



}
