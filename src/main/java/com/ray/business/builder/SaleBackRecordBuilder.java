package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdSaleBackRecord;
import com.ray.business.table.params.sale.SaleGoodsParams;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Sale 构造器
 * @Class: SaleBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class SaleBackRecordBuilder extends AbstractBuilder<ProdSaleBackRecord, SaleBackRecordBuilder> {

    public SaleBackRecordBuilder() {
        super(new ProdSaleBackRecord());
        super.setBuilder(this);
    }

    @Override
    public ProdSaleBackRecord bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public SaleBackRecordBuilder append(SaleGoodsParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }


    /**
     *  添加编码
     * @return
     */
    public SaleBackRecordBuilder appendCode(String code) {
        entity.setBackCode(code);
        return this;
    }

    /**
     *  添加编码
     * @return
     */
    public SaleBackRecordBuilder appendOrderNo(String code) {
        entity.setOrderNo(code);
        return this;
    }


    @Override
    public String getCode() {
        return entity.getBackCode();
    }


    public SaleBackRecordBuilder appendPrice(BigDecimal price) {
        entity.setPrice(price);
        return this;
    }

    public SaleBackRecordBuilder appendRecordCode(String code) {
        entity.setRecordCode(code);
        return this;
    }
}
