package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdErrorType;
import com.ray.business.table.params.error.ErrorTypeCreateParams;
import com.ray.business.table.params.error.ErrorTypeEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Step 构造器
 * @Class: ProdErrorType
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class ErrorTypeBuilder extends AbstractBuilder<ProdErrorType, ErrorTypeBuilder> {

    public ErrorTypeBuilder() {
        super(new ProdErrorType());
        super.setBuilder(this);
        entity.setErrorCode(CodeCreateUtil.getCode(Perifx.CODE));
    }

    @Override
    public ProdErrorType bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public ErrorTypeBuilder append(ErrorTypeCreateParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    /**
     *  添加状态
     * @return
     */
    public ErrorTypeBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }
    /**
     *  添加编码
     * @return
     */
    public ErrorTypeBuilder appendCode(String code) {
        entity.setErrorCode(code);
        return this;
    }
    /**
     * 构建创建信息
     * @param editParams
     * @return
     */
    public ErrorTypeBuilder append(ErrorTypeEditParams editParams) {
        if(ObjectUtil.isNotNull(editParams)){
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getErrorCode();
    }



}
