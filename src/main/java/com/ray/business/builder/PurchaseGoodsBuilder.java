package com.ray.business.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.table.entity.ProdPurchaseBackRecord;
import com.ray.business.table.entity.ProdPurchaseInRecord;
import com.ray.business.table.entity.ProdPurchaseRecord;
import com.ray.business.table.vo.PurchaseGoodsVO;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: PurchaseGoodsVO 构造器
 * @Class: PurchaseGoodsBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class PurchaseGoodsBuilder extends AbstractBuilder<PurchaseGoodsVO, PurchaseGoodsBuilder> {

    public PurchaseGoodsBuilder() {
        super(new PurchaseGoodsVO());
        super.setBuilder(this);
    }

    @Override
    public PurchaseGoodsVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public PurchaseGoodsBuilder append(ProdPurchaseRecord prodPurchaseRecord) {
        if (ObjectUtil.isNotNull(prodPurchaseRecord)) {
            entity.setGoodsCode(prodPurchaseRecord.getGoodsCode());
            entity.setUnit(prodPurchaseRecord.getUnit());
            entity.setPrice(prodPurchaseRecord.getPrice());
            entity.setQuantity(prodPurchaseRecord.getQuantity());
            entity.setCode(prodPurchaseRecord.getCode());
        }
        return this;
    }

    public PurchaseGoodsBuilder append(MaterialModelVO materialModelVO) {
        if (ObjectUtil.isNotNull(materialModelVO)) {
            entity.setGoodsName(materialModelVO.getModelName());
            entity.setMaterialType(materialModelVO.getMaterialType());
            entity.setMaterialTypeName(materialModelVO.getMaterialTypeName());
            entity.setMaterialCode(materialModelVO.getMaterialCode());
            entity.setMaterialName(materialModelVO.getMaterialName());
            entity.setMaterialNameEn(materialModelVO.getMaterialNameEn());
            //entity.setUnit(materialModelVO.getUnit());
            entity.setGoodsCode(materialModelVO.getModelCode());
            entity.setModelName(materialModelVO.getModelName());
            entity.setModelProp(materialModelVO.getModelProp());
            entity.setWeight(materialModelVO.getWeight());
            entity.setVolume(materialModelVO.getVolume());
        }
        return this;
    }

    public PurchaseGoodsBuilder append(ProdPurchaseBackRecord prodPurchaseBackRecord) {
        if (ObjectUtil.isNotNull(prodPurchaseBackRecord)) {
            entity.setBatchNo(prodPurchaseBackRecord.getBatchNo());
            entity.setGoodsCode(prodPurchaseBackRecord.getGoodsCode());
            entity.setPrice(prodPurchaseBackRecord.getPrice());
            entity.setUnit(prodPurchaseBackRecord.getUnit());
            entity.setQuantity(prodPurchaseBackRecord.getQuantity());
            entity.setSerialNumber(prodPurchaseBackRecord.getSerialNumber());
            entity.setTotal(prodPurchaseBackRecord.getTotal());

        }
        return this;
    }

    public PurchaseGoodsBuilder append(ProdPurchaseInRecord prodPurchaseInRecord) {
        if (ObjectUtil.isNotNull(prodPurchaseInRecord)) {
            entity.setBatchNo(prodPurchaseInRecord.getBatchNo());
            entity.setGoodsCode(prodPurchaseInRecord.getGoodsCode());
            entity.setPrice(prodPurchaseInRecord.getPrice());
            entity.setQuantity(prodPurchaseInRecord.getQuantity());
            entity.setSerialNumber(prodPurchaseInRecord.getSerialNumber());
            entity.setUnit(prodPurchaseInRecord.getUnit());
            entity.setTotal(prodPurchaseInRecord.getTotal());
            entity.setOrderNo(prodPurchaseInRecord.getOrderNo());
            entity.setBizCode(prodPurchaseInRecord.getOrderNo());
        }
        return this;
    }

    public PurchaseGoodsBuilder appendTotalAmount(BigDecimal totalAmount) {
        entity.setTotalAmount(totalAmount);
        return this;
    }
}
