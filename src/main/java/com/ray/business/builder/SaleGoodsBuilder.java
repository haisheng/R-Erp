package com.ray.business.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.table.entity.ProdSaleBackRecord;
import com.ray.business.table.entity.ProdSaleOutRecord;
import com.ray.business.table.entity.ProdSaleRecord;
import com.ray.business.table.vo.SaleGoodsVO;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: SaleGoodsVO 构造器
 * @Class: SaleGoodsBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class SaleGoodsBuilder extends AbstractBuilder<SaleGoodsVO, SaleGoodsBuilder> {

    public SaleGoodsBuilder() {
        super(new SaleGoodsVO());
        super.setBuilder(this);
    }

    @Override
    public SaleGoodsVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public SaleGoodsBuilder append(ProdSaleRecord prodSaleRecord) {
        if (ObjectUtil.isNotNull(prodSaleRecord)) {
            entity.setGoodsCode(prodSaleRecord.getGoodsCode());
            entity.setPrice(prodSaleRecord.getPrice());
            entity.setQuantity(prodSaleRecord.getQuantity());
            entity.setUnit(prodSaleRecord.getUnit());
        }
        return this;
    }

    public SaleGoodsBuilder append(MaterialModelVO materialModelVO) {
        if (ObjectUtil.isNotNull(materialModelVO)) {
            entity.setGoodsName(materialModelVO.getModelName());
            entity.setMaterialType(materialModelVO.getMaterialType());
            entity.setMaterialTypeName(materialModelVO.getMaterialTypeName());
            entity.setMaterialCode(materialModelVO.getMaterialCode());
            entity.setMaterialName(materialModelVO.getMaterialName());
            entity.setMaterialNameEn(materialModelVO.getMaterialNameEn());
            entity.setUnit(materialModelVO.getUnit());
            entity.setModelName(materialModelVO.getModelName());
            entity.setModelProp(materialModelVO.getModelProp());
            entity.setWeight(materialModelVO.getWeight());
            entity.setVolume(materialModelVO.getVolume());
        }
        return this;
    }

    public SaleGoodsBuilder append(ProdSaleBackRecord prodSaleBackRecord) {
        if (ObjectUtil.isNotNull(prodSaleBackRecord)) {
            entity.setBatchNo(prodSaleBackRecord.getBatchNo());
            entity.setGoodsCode(prodSaleBackRecord.getGoodsCode());
            entity.setPrice(prodSaleBackRecord.getPrice());
            entity.setQuantity(prodSaleBackRecord.getQuantity());
            entity.setUnit(prodSaleBackRecord.getUnit());
            entity.setSerialNumber(prodSaleBackRecord.getSerialNumber());
            entity.setTotal(prodSaleBackRecord.getTotal());
        }
        return this;
    }

    public SaleGoodsBuilder append(ProdSaleOutRecord prodSaleOutRecord) {
        if (ObjectUtil.isNotNull(prodSaleOutRecord)) {
            entity.setBatchNo(prodSaleOutRecord.getBatchNo());
            entity.setUnit(prodSaleOutRecord.getUnit());
            entity.setGoodsCode(prodSaleOutRecord.getGoodsCode());
            entity.setPrice(prodSaleOutRecord.getPrice());
            entity.setQuantity(prodSaleOutRecord.getQuantity());
            entity.setSerialNumber(prodSaleOutRecord.getSerialNumber());
            entity.setTotal(prodSaleOutRecord.getTotal());
            entity.setBizCode(prodSaleOutRecord.getOrderNo());
        }
        return this;
    }

    public SaleGoodsBuilder appendTotalAmount(BigDecimal total) {
        entity.setTotalAmount(total);
        return  this;
    }
}
