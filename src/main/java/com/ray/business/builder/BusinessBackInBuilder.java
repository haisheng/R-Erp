package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.business.table.entity.ProdBusinessBackIn;
import com.ray.business.table.entity.ProdBusinessIn;
import com.ray.business.table.params.business.BusinessGoodsParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * @author bo shen
 * @Description: Business 构造器
 * @Class: BusinessBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BusinessBackInBuilder extends AbstractBuilder<ProdBusinessBackIn, BusinessBackInBuilder> {

    public BusinessBackInBuilder() {
        super(new ProdBusinessBackIn());
        super.setBuilder(this);
    }

    @Override
    public ProdBusinessBackIn bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    /**
     * 添加编码
     *
     * @return
     */
    public BusinessBackInBuilder appendBackCode(String code) {
        entity.setBackCode(code);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param goodsParams
     * @return
     */
    public BusinessBackInBuilder append(BusinessGoodsParams goodsParams) {
        if (ObjectUtil.isNotNull(goodsParams)) {
            BeanUtil.copyProperties(goodsParams, entity);
            //没有批次号默认一个批次号
            if (StrUtil.isBlank(entity.getBatchNo())) {
                entity.setBatchNo(DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN));
            }
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getBusinessCode();
    }


    public BusinessBackInBuilder appendOutCode(String code) {
        entity.setOutCode(code);
        return this;
    }

    public BusinessBackInBuilder appendBusinessCode(String businessCode) {
        entity.setBusinessCode(businessCode);
        return this;
    }
}
