package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.business.table.entity.ProdPurchaseBackRecord;
import com.ray.business.table.entity.ProdPurchaseInRecord;
import com.ray.business.table.params.purchase.PurchaseGoodsParams;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author bo shen
 * @Description: Purchase 构造器
 * @Class: PurchaseBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class PurchaseInRecordBuilder extends AbstractBuilder<ProdPurchaseInRecord, PurchaseInRecordBuilder> {

    public PurchaseInRecordBuilder() {
        super(new ProdPurchaseInRecord());
        super.setBuilder(this);
    }

    @Override
    public ProdPurchaseInRecord bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public PurchaseInRecordBuilder append(PurchaseGoodsParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
            if (StrUtil.isBlank(createParams.getBatchNo())) {
                entity.setBatchNo(DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN));
            }
        }
        return this;
    }


    /**
     * 添加编码
     *
     * @return
     */
    public PurchaseInRecordBuilder appendCode(String code) {
        entity.setInCode(code);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public PurchaseInRecordBuilder appendOrderNo(String code) {
        entity.setOrderNo(code);
        return this;
    }


    @Override
    public String getCode() {
        return entity.getInCode();
    }


    public PurchaseInRecordBuilder appendPrice(BigDecimal price) {
        entity.setPrice(price);
        return this;
    }

    public PurchaseInRecordBuilder appendRecordCode(String code) {
        entity.setRecordCode(code);
        return this;
    }

    public PurchaseInRecordBuilder appendCustomerCode(String customerCode) {
        entity.setCustomerCode(customerCode);
        return this;
    }
}
