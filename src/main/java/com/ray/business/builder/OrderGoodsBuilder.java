package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdOrderGoods;
import com.ray.business.table.params.order.goods.OrderGoodsParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class OrderGoodsBuilder extends AbstractBuilder<ProdOrderGoods, OrderGoodsBuilder> {

    public OrderGoodsBuilder() {
        super(new ProdOrderGoods());
        super.setBuilder(this);
        entity.setCode(CodeCreateUtil.getCode(Perifx.ORDER_CODE));
        log.info("获得编码:{}", entity.getCode());
    }

    @Override
    public ProdOrderGoods bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public OrderGoodsBuilder append(OrderGoodsParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public OrderGoodsBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public OrderGoodsBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }


    public OrderGoodsBuilder appendStatus(String status) {
        entity.setGoodsStatus(status);
        return this;
    }

    @Override
    public String getCode() {
        return entity.getCode();
    }


    /**
     * 添加编码
     *
     * @return
     */
    public OrderGoodsBuilder appendCode(String code) {
        entity.setCode(code);
        return this;
    }

    public OrderGoodsBuilder appendCustomerCode(String customerCode) {
        entity.setCustomerCode(customerCode);
        return this;
    }
}
