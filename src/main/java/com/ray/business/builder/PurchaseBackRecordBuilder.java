package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdPurchaseBackRecord;
import com.ray.business.table.entity.ProdPurchaseRecord;
import com.ray.business.table.params.purchase.PurchaseGoodsParams;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Purchase 构造器
 * @Class: PurchaseBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class PurchaseBackRecordBuilder extends AbstractBuilder<ProdPurchaseBackRecord, PurchaseBackRecordBuilder> {

    public PurchaseBackRecordBuilder() {
        super(new ProdPurchaseBackRecord());
        super.setBuilder(this);
    }

    @Override
    public ProdPurchaseBackRecord bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public PurchaseBackRecordBuilder append(PurchaseGoodsParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }


    /**
     *  添加编码
     * @return
     */
    public PurchaseBackRecordBuilder appendCode(String code) {
        entity.setBackCode(code);
        return this;
    }

    /**
     *  添加编码
     * @return
     */
    public PurchaseBackRecordBuilder appendOrderNo(String code) {
        entity.setOrderNo(code);
        return this;
    }


    @Override
    public String getCode() {
        return entity.getBackCode();
    }


    public PurchaseBackRecordBuilder appendPrice(BigDecimal price) {
        entity.setPrice(price);
        return this;
    }

    public PurchaseBackRecordBuilder appendRecordCode(String code) {
        entity.setRecordCode(code);
        return this;
    }
}
