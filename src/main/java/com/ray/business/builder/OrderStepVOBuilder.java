package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdOrderStep;
import com.ray.business.table.entity.ProdStep;
import com.ray.business.table.vo.OrderStepVO;
import com.ray.common.builder.AbstractBuilder;
import com.ray.wms.table.entity.WmsWarehouse;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class OrderStepVOBuilder extends AbstractBuilder<OrderStepVO, OrderStepVOBuilder> {

    public OrderStepVOBuilder() {
        super(new OrderStepVO());
        super.setBuilder(this);
    }

    @Override
    public OrderStepVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }



    @Override
    public String getCode() {
        return entity.getCode();
    }


    public OrderStepVOBuilder append(ProdOrderStep sysOrderStep) {
        if(ObjectUtil.isNotNull(sysOrderStep)){
            BeanUtil.copyProperties(sysOrderStep,entity);
        }
        return  this;
    }

    public OrderStepVOBuilder appendIn(WmsWarehouse inWarehouse) {
        if(ObjectUtil.isNotNull(inWarehouse)){
           entity.setInWarehouseName(inWarehouse.getWarehouseName());
        }
        return  this;
    }

    public OrderStepVOBuilder appendOut(WmsWarehouse outWarehouse) {
        if(ObjectUtil.isNotNull(outWarehouse)){
            entity.setOutWarehouseName(outWarehouse.getWarehouseName());
        }
        return  this;
    }

    public OrderStepVOBuilder append(ProdStep step) {
        if(ObjectUtil.isNotNull(step)){
            entity.setStepName(step.getStepName());
            entity.setOrderStatus(step.getOrderStatus());
            entity.setQuickStatus(step.getQuickStatus());
        }
        return  this;
    }
}
