package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.table.entity.*;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BusinessGoodsVOBuilder extends AbstractBuilder<BusinessGoodsVO, BusinessGoodsVOBuilder> {

    public BusinessGoodsVOBuilder() {
        super(new BusinessGoodsVO());
        super.setBuilder(this);
    }

    @Override
    public BusinessGoodsVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    @Override
    public String getCode() {
        return entity.getBusinessCode();
    }


    public BusinessGoodsVOBuilder append(ProdPurchaseRecord prodPurchaseRecord) {
        if (ObjectUtil.isNotNull(prodPurchaseRecord)) {
            BeanUtil.copyProperties(prodPurchaseRecord, entity);
            entity.setCode(null);
            entity.setLinkCode(null);
        }
        return this;
    }

    public BusinessGoodsVOBuilder append(MaterialModelVO modelVO) {
        if (ObjectUtil.isNotNull(modelVO)) {
            BeanUtil.copyProperties(modelVO, entity,"quantity");
            entity.setGoodsName(modelVO.getModelName());
        }
        return this;
    }

    public BusinessGoodsVOBuilder sub(BigDecimal backQuantity) {
        if (ObjectUtil.isNotNull(backQuantity) && ObjectUtil.isNotNull(entity.getQuantity())) {
            entity.setQuantity(entity.getQuantity().subtract(backQuantity));
        }
        return this;
    }

    public BusinessGoodsVOBuilder append(ProdBusinessIn prodBusinessIn) {
        if (ObjectUtil.isNotNull(prodBusinessIn)) {
            BeanUtil.copyProperties(prodBusinessIn, entity);
            entity.setLinkCode(prodBusinessIn.getOutCode());
            entity.setBizCode(prodBusinessIn.getBusinessCode());
        }
        return this;
    }

    public BusinessGoodsVOBuilder append(ProdBusinessOut prodBusinessOut) {
        if (ObjectUtil.isNotNull(prodBusinessOut)) {
            BeanUtil.copyProperties(prodBusinessOut, entity);
            entity.setLinkCode(prodBusinessOut.getInCode());
        }
        return this;
    }

    public BusinessGoodsVOBuilder append(ProdBusinessBackIn prodBusinessBackIn) {
        if (ObjectUtil.isNotNull(prodBusinessBackIn)) {
            BeanUtil.copyProperties(prodBusinessBackIn, entity);
            entity.setLinkCode(prodBusinessBackIn.getOutCode());
        }
        return this;
    }

    public BusinessGoodsVOBuilder append(ProdPurchaseInRecord prodPurchaseRecord) {
        if (ObjectUtil.isNotNull(prodPurchaseRecord)) {
            BeanUtil.copyProperties(prodPurchaseRecord, entity);
            entity.setLinkCode(prodPurchaseRecord.getRecordCode());
        }
        return this;
    }

    public BusinessGoodsVOBuilder append(ProdBusinessQuickIn prodBusinessQuickIn) {
        if (ObjectUtil.isNotNull(prodBusinessQuickIn)) {
            BeanUtil.copyProperties(prodBusinessQuickIn, entity);
            entity.setLinkCode(prodBusinessQuickIn.getOutCode());
        }
        return this;
    }

    public BusinessGoodsVOBuilder appendNumber(String number) {
        entity.setSerialNumber(number);
        return this;
    }

    public BusinessGoodsVOBuilder appendGoodsCode(String modelCode) {
        entity.setGoodsCode(modelCode);
        return this;
    }

    public BusinessGoodsVOBuilder appendUnit(String unit) {
        entity.setUnit(unit);
        return this;
    }

    public BusinessGoodsVOBuilder append(ProdStep prodStep) {
        if (ObjectUtil.isNotNull(prodStep)) {
            entity.setStepCode(prodStep.getStepCode());
            entity.setStepName(prodStep.getStepName());
        }
        return this;
    }

    public BusinessGoodsVOBuilder append(BaseCustomer customer) {
        if (ObjectUtil.isNotNull(customer)) {
            entity.setCustomerCode(customer.getCustomerCode());
            entity.setCustomerName(customer.getCustomerName());
        }
        return this;
    }

    public BusinessGoodsVOBuilder appendType(String type) {
        entity.setType(type);
        return this;
    }

    public BusinessGoodsVOBuilder appendTotalAmount(BigDecimal totalAmount) {
        entity.setTotalAmount(totalAmount);
        return this;
    }

    public BusinessGoodsVOBuilder appendPrice(BigDecimal price) {
        entity.setPrice(price);
        return this;
    }
}
