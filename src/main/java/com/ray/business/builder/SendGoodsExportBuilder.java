package com.ray.business.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.table.entity.ProdOrderGoods;
import com.ray.business.table.entity.ProdSendRecord;
import com.ray.business.table.vo.SendGoodsExportVO;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: SaleGoodsVO 构造器
 * @Class: SaleGoodsBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class SendGoodsExportBuilder extends AbstractBuilder<SendGoodsExportVO, SendGoodsExportBuilder> {

    public SendGoodsExportBuilder() {
        super(new SendGoodsExportVO());
        super.setBuilder(this);
    }

    @Override
    public SendGoodsExportVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public SendGoodsExportBuilder append(ProdSendRecord sendRecord) {
        if (ObjectUtil.isNotNull(sendRecord)) {
            entity.setBatchNo(sendRecord.getBatchNo());
            entity.setQuantity(sendRecord.getQuantity());
            entity.setSerialNumber(sendRecord.getSerialNumber());
            entity.setGoodsNetWeight(sendRecord.getGoodsNetWeight());
            entity.setGoodsWeight(sendRecord.getGoodsWeight());
            entity.setTotal(sendRecord.getTotal());
        }
        return this;
    }

    public SendGoodsExportBuilder append(MaterialModelVO materialModelVO) {
        if (ObjectUtil.isNotNull(materialModelVO)) {
            entity.setMaterialName(materialModelVO.getMaterialName());
            entity.setUnit(materialModelVO.getUnit());
            entity.setModelProp(materialModelVO.getModelProp());
            entity.setWeight(materialModelVO.getWeight());
        }
        return this;
    }

    public SendGoodsExportBuilder append(ProdOrderGoods orderGoods) {
        if (ObjectUtil.isNotNull(orderGoods)) {
            entity.setPrice(orderGoods.getPrice());
            entity.setCustomerModelName(orderGoods.getCustomerModelName());
            entity.setCustomerModelProp(orderGoods.getCustomerModelProp());
        }
        return this;
    }
}
