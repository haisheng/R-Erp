package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.business.table.entity.ProdBusinessOut;
import com.ray.business.table.params.business.BusinessGoodsParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: Business 构造器
 * @Class: BusinessBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BusinessOutBuilder extends AbstractBuilder<ProdBusinessOut, BusinessOutBuilder> {

    public BusinessOutBuilder() {
        super(new ProdBusinessOut());
        super.setBuilder(this);
        entity.setCode(CodeCreateUtil.getCode(Perifx.RECORD_CODE));
    }

    @Override
    public ProdBusinessOut bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    /**
     * 添加编码
     *
     * @return
     */
    public BusinessOutBuilder appendBusinessCode(String code) {
        entity.setBusinessCode(code);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param goodsParams
     * @return
     */
    public BusinessOutBuilder append(BusinessGoodsParams goodsParams) {
        if (ObjectUtil.isNotNull(goodsParams)) {
            BeanUtil.copyProperties(goodsParams, entity);
        }
        entity.setCode(CodeCreateUtil.getCode(Perifx.RECORD_CODE));
        return this;
    }

    @Override
    public String getCode() {
        return entity.getBusinessCode();
    }


    public BusinessOutBuilder appendInCode(String code) {
        entity.setInCode(code);
        return this;
    }

    public BusinessOutBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }

    public BusinessOutBuilder appendGangNo(String gangNo) {
        if(StrUtil.isNotBlank(gangNo)){
            entity.setGangNo(gangNo);
        }
        return this;
    }
}
