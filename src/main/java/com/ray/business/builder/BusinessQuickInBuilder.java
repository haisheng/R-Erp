package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.business.table.entity.ProdBusinessQuick;
import com.ray.business.table.entity.ProdBusinessQuickIn;
import com.ray.business.table.params.business.quick.BusinessQuickInParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * @author bo shen
 * @Description: Business 构造器
 * @Class: BusinessBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BusinessQuickInBuilder extends AbstractBuilder<ProdBusinessQuickIn, BusinessQuickInBuilder> {

    public BusinessQuickInBuilder() {
        super(new ProdBusinessQuickIn());
        super.setBuilder(this);
        entity.setCode(CodeCreateUtil.getCode(Perifx.RECORD_CODE));
    }

    @Override
    public ProdBusinessQuickIn bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    /**
     * 构建创建信息
     *
     * @param goodsParams
     * @return
     */
    public BusinessQuickInBuilder append(BusinessQuickInParams goodsParams) {
        if (ObjectUtil.isNotNull(goodsParams)) {
            BeanUtil.copyProperties(goodsParams, entity);
            //没有批次号默认一个批次号
            if (StrUtil.isBlank(entity.getBatchNo())) {
                entity.setBatchNo(DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN));
            }
        }
        entity.setCode(CodeCreateUtil.getCode(Perifx.RECORD_CODE));
        return this;
    }

    @Override
    public String getCode() {
        return entity.getCode();
    }


    public BusinessQuickInBuilder appendOutCode(String code) {
        entity.setOutCode(code);
        return this;
    }

    public BusinessQuickInBuilder appendCode(String code) {
        entity.setCode(code);
        return this;
    }

    public BusinessQuickInBuilder append(ProdBusinessQuick prodBusinessQuick) {
        if (ObjectUtil.isNotNull(prodBusinessQuick)) {
            entity.setOrderNo(prodBusinessQuick.getOrderNo());
        }
        return this;
    }
}
