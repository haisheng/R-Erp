package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdOrder;
import com.ray.business.table.entity.ProdOrderSend;
import com.ray.business.table.params.send.OrderSendCreateParams;
import com.ray.business.table.params.send.OrderSendEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Sale 构造器
 * @Class: SaleBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class OrderSendBuilder extends AbstractBuilder<ProdOrderSend, OrderSendBuilder> {

    public OrderSendBuilder() {
        super(new ProdOrderSend());
        super.setBuilder(this);
        entity.setSendCode(CodeCreateUtil.getCode(Perifx.SEND_CODE));
        log.info("获得发货编码:{}", entity.getSendCode());
    }

    @Override
    public ProdOrderSend bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public OrderSendBuilder append(OrderSendCreateParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    /**
     *  添加状态
     * @return
     */
    public OrderSendBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }
    /**
     *  添加编码
     * @return
     */
    public OrderSendBuilder appendCode(String code) {
        entity.setSendCode(code);
        return this;
    }

    /**
     *  添加编码
     * @return
     */
    public OrderSendBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }
    /**
     * 构建创建信息
     * @param editParams
     * @return
     */
    public OrderSendBuilder append(OrderSendEditParams editParams) {
        if(ObjectUtil.isNotNull(editParams)){
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getSendCode();
    }


    public OrderSendBuilder appendStatus(String status) {
        entity.setOrderStatus(status);
        return this;
    }

    public OrderSendBuilder appendQuantity(BigDecimal quantity) {
        entity.setQuantity(quantity);
        return this;
    }

    public OrderSendBuilder appendTotal(BigDecimal bigDecimal) {
        entity.setTotalAmount(bigDecimal);
        return this;
    }

    public OrderSendBuilder append(ProdOrder prodOrder) {
        if(ObjectUtil.isNotNull(prodOrder)){
            entity.setCustomerCode(prodOrder.getCustomerCode());
            entity.setCurrency(prodOrder.getCurrency());
        }
        return this;
    }

    public OrderSendBuilder appenJuanTotal(int sum) {
        entity.setTotal(sum);
        return this;
    }
}
