package com.ray.business.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.table.entity.ProdOrderGoods;
import com.ray.business.table.entity.ProdSendRecord;
import com.ray.business.table.vo.SendGoodsVO;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: SaleGoodsVO 构造器
 * @Class: SaleGoodsBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class SendGoodsBuilder extends AbstractBuilder<SendGoodsVO, SendGoodsBuilder> {

    public SendGoodsBuilder() {
        super(new SendGoodsVO());
        super.setBuilder(this);
    }

    @Override
    public SendGoodsVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public SendGoodsBuilder append(ProdSendRecord sendRecord) {
        if (ObjectUtil.isNotNull(sendRecord)) {
            entity.setBatchNo(sendRecord.getBatchNo());
            entity.setGoodsCode(sendRecord.getGoodsCode());
            entity.setQuantity(sendRecord.getQuantity());
            entity.setSerialNumber(sendRecord.getSerialNumber());
            entity.setLinkCode(sendRecord.getInCode());
            entity.setIndexSort(sendRecord.getIndexSort());
            entity.setGoodsNetWeight(sendRecord.getGoodsNetWeight());
            entity.setGoodsWeight(sendRecord.getGoodsWeight());
            entity.setTotal(sendRecord.getTotal());
            entity.setBizCode(sendRecord.getSendCode());
        }
        return this;
    }

    public SendGoodsBuilder append(MaterialModelVO materialModelVO) {
        if (ObjectUtil.isNotNull(materialModelVO)) {
            entity.setGoodsName(materialModelVO.getModelName());
            entity.setMaterialType(materialModelVO.getMaterialType());
            entity.setMaterialTypeName(materialModelVO.getMaterialTypeName());
            entity.setMaterialCode(materialModelVO.getMaterialCode());
            entity.setMaterialName(materialModelVO.getMaterialName());
            entity.setMaterialNameEn(materialModelVO.getMaterialNameEn());
            entity.setUnit(materialModelVO.getUnit());
            entity.setModelProp(materialModelVO.getModelProp());
            entity.setModelCode(materialModelVO.getModelCode());
            entity.setGoodsCode(materialModelVO.getModelCode());
            entity.setWeight(materialModelVO.getWeight());
            entity.setVolume(materialModelVO.getVolume());
        }
        return this;
    }

    public SendGoodsBuilder append(ProdOrderGoods orderGoods) {
        if (ObjectUtil.isNotNull(orderGoods)) {
            entity.setPrice(orderGoods.getPrice());
            entity.setCustomerModelName(orderGoods.getCustomerModelName());
            entity.setCustomerModelProp(orderGoods.getCustomerModelProp());
        }
        return this;
    }

    public SendGoodsBuilder appendTotalAmount(BigDecimal multiply) {
        entity.setTotalAmount(multiply);
        return this;
    }
}
