package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdBusinessDeduction;
import com.ray.business.table.params.deduction.DeductionCreateParams;
import com.ray.business.table.params.deduction.DeductionEditParams;
import com.ray.business.table.params.order.OrderCreateParams;
import com.ray.business.table.params.order.OrderEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class DeductionBuilder extends AbstractBuilder<ProdBusinessDeduction, DeductionBuilder> {

    public DeductionBuilder() {
        super(new ProdBusinessDeduction());
        super.setBuilder(this);
        entity.setDeductionCode(CodeCreateUtil.getCode(Perifx.CODE));
        log.info("获得扣款编码:{}", entity.getOrderNo());
    }

    @Override
    public ProdBusinessDeduction bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public DeductionBuilder append(DeductionCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public DeductionBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public DeductionBuilder appendCode(String code) {
        entity.setDeductionCode(code);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public DeductionBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public DeductionBuilder appendBusinessCode(String businessCode) {
        entity.setBusinessCode(businessCode);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public DeductionBuilder append(DeductionEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getDeductionCode();
    }


    public DeductionBuilder appendStatus(String status) {
        entity.setOrderStatus(status);
        return this;
    }

    public DeductionBuilder append(ProdBusiness prodBusiness) {
        if (ObjectUtil.isNotNull(prodBusiness)) {
            entity.setBusinessCode(prodBusiness.getBusinessCode());
            entity.setCustomerCode(prodBusiness.getCustomerCode());
            entity.setOrderNo(prodBusiness.getOrderNo());
        }
        return this;
    }
}
