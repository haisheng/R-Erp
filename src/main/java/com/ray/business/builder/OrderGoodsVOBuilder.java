package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.table.entity.ProdOrderGoods;
import com.ray.business.table.vo.OrderGoodsVO;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;
import org.omg.PortableInterceptor.INACTIVE;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class OrderGoodsVOBuilder extends AbstractBuilder<OrderGoodsVO, OrderGoodsVOBuilder> {

    public OrderGoodsVOBuilder() {
        super(new OrderGoodsVO());
        super.setBuilder(this);
    }

    @Override
    public OrderGoodsVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    @Override
    public String getCode() {
        return entity.getCode();
    }


    public OrderGoodsVOBuilder append(ProdOrderGoods sysOrderGoods) {
        if (ObjectUtil.isNotNull(sysOrderGoods)) {
            BeanUtil.copyProperties(sysOrderGoods, entity);
        }
        return this;
    }

    public OrderGoodsVOBuilder append(MaterialModelVO materialModelVO) {
        if (ObjectUtil.isNotNull(materialModelVO)) {
            BeanUtil.copyProperties(materialModelVO, entity);
            entity.setGoodsName(materialModelVO.getModelName());
        }
        return this;
    }

    public OrderGoodsVOBuilder appendTotalRow(List<Integer> totals, List<BigDecimal> qiantitys) {
        entity.setTotal(totals.stream().mapToInt(Integer::intValue).sum());
        entity.setFinishQuantity(qiantitys.stream().reduce(BigDecimal::add).get());
        return this;
    }
}
