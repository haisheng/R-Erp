package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdSale;
import com.ray.business.table.entity.ProdSaleOut;
import com.ray.business.table.params.sale.out.SaleOutCreateParams;
import com.ray.business.table.params.sale.out.SaleOutEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Purchase 构造器
 * @Class: PurchaseBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class SaleOutBuilder extends AbstractBuilder<ProdSaleOut, SaleOutBuilder> {

    public SaleOutBuilder() {
        super(new ProdSaleOut());
        super.setBuilder(this);
        entity.setOutCode(CodeCreateUtil.getCode(Perifx.SALE_OUT_CODE));
        log.info("获得销售出库订单编码:{}", entity.getOutCode());
    }

    @Override
    public ProdSaleOut bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public SaleOutBuilder append(SaleOutCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public SaleOutBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public SaleOutBuilder appendCode(String code) {
        entity.setOutCode(code);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public SaleOutBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public SaleOutBuilder append(SaleOutEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getOutCode();
    }


    public SaleOutBuilder appendStatus(String status) {
        entity.setOrderStatus(status);
        return this;
    }

    public SaleOutBuilder append(ProdSale prodSale) {
        if (ObjectUtil.isNotNull(prodSale)) {
            entity.setCustomerCode(prodSale.getCustomerCode());
            entity.setWarehouseCode(prodSale.getWarehouseCode());
        }
        return this;
    }

    public SaleOutBuilder append(BigDecimal quantity) {
        entity.setQuantity(quantity);
        return this;
    }

    public SaleOutBuilder appendTotal(BigDecimal total) {
        entity.setTotalAmount(total);
        return this;
    }

    public SaleOutBuilder appendJuanTotal(Integer total) {
        entity.setTotal(total);
        return this;
    }

    public BigDecimal getQuantity() {
        return entity.getQuantity();
    }

    public BigDecimal getAmount() {
        return entity.getTotalAmount();
    }

    public Integer getTotal() {
        return entity.getTotal();
    }
}
