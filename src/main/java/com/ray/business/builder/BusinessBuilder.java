package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.params.business.BusinessCreateParams;
import com.ray.business.table.params.business.BusinessEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Business 构造器
 * @Class: BusinessBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BusinessBuilder extends AbstractBuilder<ProdBusiness, BusinessBuilder> {

    public BusinessBuilder() {
        super(new ProdBusiness());
        super.setBuilder(this);
        entity.setBusinessCode(CodeCreateUtil.getCode(Perifx.BUSINESS_CODE));
        log.info("获得加工单号编码:{}", entity.getBusinessCode());
    }

    @Override
    public ProdBusiness bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public BusinessBuilder append(BusinessCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public BusinessBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加类型
     *
     * @return
     */
    public BusinessBuilder appendType(String type) {
        entity.setBusinessType(type);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public BusinessBuilder appendCode(String code) {
        entity.setBusinessCode(code);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public BusinessBuilder append(BusinessEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getBusinessCode();
    }


    public BusinessBuilder appendStatus(String status) {
        entity.setOrderStatus(status);
        return this;
    }

    public BusinessBuilder append(BigDecimal quantity) {
        entity.setQuantity(quantity);
        return this;
    }
}
