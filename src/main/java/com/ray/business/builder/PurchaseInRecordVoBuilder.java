package com.ray.business.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.business.table.entity.ProdPurchaseInRecord;
import com.ray.business.table.vo.PurchaseInRecordVO;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: PurchaseGoodsVO 构造器
 * @Class: PurchaseGoodsBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class PurchaseInRecordVoBuilder extends AbstractBuilder<PurchaseInRecordVO, PurchaseInRecordVoBuilder> {

    public PurchaseInRecordVoBuilder() {
        super(new PurchaseInRecordVO());
        super.setBuilder(this);
    }

    @Override
    public PurchaseInRecordVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public PurchaseInRecordVoBuilder append(MaterialModelVO materialModelVO) {
        if (ObjectUtil.isNotNull(materialModelVO)) {
            entity.setGoodsName(materialModelVO.getModelName());
            entity.setMaterialType(materialModelVO.getMaterialType());
            entity.setMaterialTypeName(materialModelVO.getMaterialTypeName());
            entity.setMaterialCode(materialModelVO.getMaterialCode());
            entity.setMaterialName(materialModelVO.getMaterialName());
            entity.setMaterialNameEn(materialModelVO.getMaterialNameEn());
            //entity.setUnit(materialModelVO.getUnit());
            entity.setGoodsCode(materialModelVO.getModelCode());
            entity.setModelName(materialModelVO.getModelName());
            entity.setModelProp(materialModelVO.getModelProp());
            entity.setWeight(materialModelVO.getWeight());
            entity.setVolume(materialModelVO.getVolume());
        }
        return this;
    }


    public PurchaseInRecordVoBuilder append(ProdPurchaseInRecord prodPurchaseInRecord) {
        if (ObjectUtil.isNotNull(prodPurchaseInRecord)) {
            entity.setBatchNo(prodPurchaseInRecord.getBatchNo());
            entity.setGoodsCode(prodPurchaseInRecord.getGoodsCode());
            entity.setPrice(prodPurchaseInRecord.getPrice());
            entity.setQuantity(prodPurchaseInRecord.getQuantity());
            entity.setSerialNumber(prodPurchaseInRecord.getSerialNumber());
            entity.setUnit(prodPurchaseInRecord.getUnit());
            entity.setTotal(prodPurchaseInRecord.getTotal());
            entity.setOrderNo(prodPurchaseInRecord.getOrderNo());
            entity.setInCode(prodPurchaseInRecord.getInCode());
            entity.setCreateTime(prodPurchaseInRecord.getCreateTime());
        }
        return this;
    }

    public PurchaseInRecordVoBuilder appendTotalAmount(BigDecimal totalAmount) {
        entity.setTotalAmount(totalAmount);
        return this;
    }

    public PurchaseInRecordVoBuilder append(BaseCustomer baseCustomer) {
        if (ObjectUtil.isNotNull(baseCustomer)) {
            entity.setCustomerName(baseCustomer.getCustomerName());
        }
        return this;
    }

    public PurchaseInRecordVoBuilder append(ProdPurchase prodPurchase) {
        if (ObjectUtil.isNotNull(prodPurchase)) {
            entity.setBillStatus(prodPurchase.getBillStatus());
            entity.setBillNo(prodPurchase.getBillNo());
        }
        return this;
    }
}
