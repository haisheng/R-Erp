package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdBusinessQuick;
import com.ray.business.table.entity.ProdOrderStep;
import com.ray.business.table.params.business.BusinessEditParams;
import com.ray.business.table.params.business.quick.BusinessQuickQueryParams;
import com.ray.business.table.params.business.quick.BusinessQuickSubmitParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Business 构造器
 * @Class: BusinessBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BusinessQuickBuilder extends AbstractBuilder<ProdBusinessQuick, BusinessQuickBuilder> {

    public BusinessQuickBuilder() {
        super(new ProdBusinessQuick());
        super.setBuilder(this);
        entity.setQuickCode(CodeCreateUtil.getCode(Perifx.CODE));
    }

    @Override
    public ProdBusinessQuick bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public BusinessQuickBuilder append(BusinessQuickQueryParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public BusinessQuickBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }


    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public BusinessQuickBuilder append(BusinessEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getQuickCode();
    }

    public BusinessQuickBuilder append(ProdOrderStep prodOrderStep) {
        if (ObjectUtil.isNotNull(prodOrderStep)) {
            entity.setWarehouseCode(prodOrderStep.getInWarehouseCode());
        }
        return this;
    }

    public BusinessQuickBuilder appendCode(String quickCode) {
        entity.setQuickCode(quickCode);
        return this;
    }

    public BusinessQuickBuilder append(BusinessQuickSubmitParams submitParams) {
        if (ObjectUtil.isNotNull(submitParams)) {
            BeanUtil.copyProperties(submitParams, entity);
        }
        return this;
    }
}
