package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdOrder;
import com.ray.business.table.entity.ProdOrderIndex;
import com.ray.business.table.params.send.OrderSendCreateParams;
import com.ray.business.table.params.send.OrderSendEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Sale 构造器
 * @Class: SaleBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class OrderIndexBuilder extends AbstractBuilder<ProdOrderIndex, OrderIndexBuilder> {

    public OrderIndexBuilder() {
        super(new ProdOrderIndex());
        super.setBuilder(this);
    }

    @Override
    public ProdOrderIndex bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    /**
     * 添加状态
     *
     * @return
     */
    public OrderIndexBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public OrderIndexBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }
    /**
     * 添加编码
     *
     * @return
     */
    public OrderIndexBuilder appendGoodsCode(String goodsCode) {
        entity.setGoodsCode(goodsCode);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public OrderIndexBuilder append(OrderSendEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getOrderNo();
    }

    public OrderIndexBuilder appendIndex(int index) {
        entity.setIndexSort(index);
        return this;
    }
}
