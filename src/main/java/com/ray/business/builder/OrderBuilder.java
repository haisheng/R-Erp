package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.business.table.entity.ProdOrder;
import com.ray.business.table.params.order.OrderCreateParams;
import com.ray.business.table.params.order.OrderEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class OrderBuilder extends AbstractBuilder<ProdOrder, OrderBuilder> {

    public OrderBuilder() {
        super(new ProdOrder());
        super.setBuilder(this);
        entity.setOrderNo(CodeCreateUtil.getCode(Perifx.ORDER_CODE));
        log.info("获得订单编码:{}", entity.getOrderNo());
    }

    @Override
    public ProdOrder bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public OrderBuilder append(OrderCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public OrderBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public OrderBuilder appendCode(String code) {
        entity.setOrderNo(code);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public OrderBuilder append(OrderEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getOrderNo();
    }


    public OrderBuilder appendStatus(String status) {
        entity.setOrderStatus(status);
        return this;
    }

    public OrderBuilder append(BigDecimal quantity) {
        entity.setQuantity(quantity);
        return this;
    }

    public OrderBuilder appendTotal(BigDecimal total) {
        entity.setTotalAmount(total);
        return this;
    }

    public OrderBuilder appendOwner(String ownerCode) {
        if (StrUtil.isNotBlank(ownerCode)) {
            entity.setOwnerCode(ownerCode);
        }
        return this;
    }
}
