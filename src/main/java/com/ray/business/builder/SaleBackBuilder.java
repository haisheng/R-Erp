package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdSale;
import com.ray.business.table.entity.ProdSaleBack;
import com.ray.business.table.params.sale.back.SaleBackCreateParams;
import com.ray.business.table.params.sale.back.SaleBackEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Sale 构造器
 * @Class: SaleBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class SaleBackBuilder extends AbstractBuilder<ProdSaleBack, SaleBackBuilder> {

    public SaleBackBuilder() {
        super(new ProdSaleBack());
        super.setBuilder(this);
        entity.setBackCode(CodeCreateUtil.getCode(Perifx.SALE_BACK_CODE));
        log.info("获得销售退货订单编码:{}", entity.getBackCode());
    }

    @Override
    public ProdSaleBack bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public SaleBackBuilder append(SaleBackCreateParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    /**
     *  添加状态
     * @return
     */
    public SaleBackBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }
    /**
     *  添加编码
     * @return
     */
    public SaleBackBuilder appendCode(String code) {
        entity.setBackCode(code);
        return this;
    }

    /**
     *  添加编码
     * @return
     */
    public SaleBackBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }
    /**
     * 构建创建信息
     * @param editParams
     * @return
     */
    public SaleBackBuilder append(SaleBackEditParams editParams) {
        if(ObjectUtil.isNotNull(editParams)){
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getBackCode();
    }


    public SaleBackBuilder appendStatus(String status) {
        entity.setOrderStatus(status);
        return this;
    }

    public SaleBackBuilder append(ProdSale prodSale) {
        if(ObjectUtil.isNotNull(prodSale)){
            entity.setCustomerCode(prodSale.getCustomerCode());
            entity.setWarehouseCode(prodSale.getWarehouseCode());
        }
        return this;
    }

    public SaleBackBuilder append(BigDecimal quantity) {
        entity.setQuantity(quantity);
        return this;
    }

    public SaleBackBuilder appendTotal(BigDecimal total) {
        entity.setTotalAmount(total);
        return this;
    }
}
