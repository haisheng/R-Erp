package com.ray.business.builder;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.table.entity.ProdBusinessIn;
import com.ray.business.table.entity.ProdBusinessQuickIn;
import com.ray.business.table.entity.ProdOrderGoods;
import com.ray.business.table.entity.ProdSendRecord;
import com.ray.common.builder.AbstractBuilder;
import com.ray.common.dto.PrintDataDTO;

import java.math.RoundingMode;

/**
 * @author bo shen
 * @Description:
 * @Class: PrintDataBuilder
 * @Package com.ray.business.builder
 * @date 2020/7/6 9:09
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class PrintDataBuilder extends AbstractBuilder<PrintDataDTO, PrintDataBuilder> {

    public PrintDataBuilder() {
        super(new PrintDataDTO());
        super.setBuilder(this);
    }

    @Override
    public PrintDataDTO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    public PrintDataBuilder append(MaterialModelVO materialModelVO) {
        if (ObjectUtil.isNotNull(materialModelVO)) {
            BeanUtil.copyProperties(materialModelVO, entity);
        }
        return this;
    }

    public PrintDataBuilder append(ProdOrderGoods orderGoods) {
        if (ObjectUtil.isNotNull(orderGoods)) {
            entity.setCustomerModelName(orderGoods.getCustomerModelName());
            entity.setCustomerModelProp(orderGoods.getCustomerModelProp());
        }
        return this;
    }

    public PrintDataBuilder append(ProdBusinessQuickIn quickIn) {
        if (ObjectUtil.isNotNull(quickIn)) {
            entity.setUnit(quickIn.getUnit());
            if (ObjectUtil.isNotNull(quickIn.getQuantity())) {
                entity.setQuantity(quickIn.getQuantity().setScale(2, RoundingMode.HALF_UP));
            }
            entity.setSerialNumber(quickIn.getSerialNumber());
            entity.setDeleteQuantity(quickIn.getDeleteQuantity());
            entity.setRealQuantity(quickIn.getRealQuantity());
            entity.setGoodsWeight(quickIn.getGoodsWeight());
            entity.setGoodsNetWeight(quickIn.getGoodsNetWeight());
            entity.setGangNo(quickIn.getGangNo());
            entity.setIndexSort(quickIn.getIndexSort());
        }
        return this;
    }

    public PrintDataBuilder append(ProdSendRecord sendRecord) {
        if (ObjectUtil.isNotNull(sendRecord)) {
            entity.setUnit(sendRecord.getUnit());
            entity.setQuantity(sendRecord.getQuantity());
            entity.setSerialNumber(sendRecord.getSerialNumber());
            entity.setDeleteQuantity(sendRecord.getDeleteQuantity());
            entity.setRealQuantity(sendRecord.getRealQuantity());
            entity.setGoodsWeight(sendRecord.getGoodsWeight());
            entity.setGoodsNetWeight(sendRecord.getGoodsNetWeight());
            entity.setGangNo(sendRecord.getGangNo());
            entity.setIndexSort(sendRecord.getIndexSort());
        }
        return this;
    }

    public PrintDataBuilder append(ProdBusinessIn businessIn) {
        if (ObjectUtil.isNotNull(businessIn)) {
            entity.setUnit(businessIn.getUnit());
            entity.setQuantity(businessIn.getQuantity());
            entity.setSerialNumber(businessIn.getSerialNumber());
            entity.setDeleteQuantity(businessIn.getDeleteQuantity());
            entity.setRealQuantity(businessIn.getRealQuantity());
            entity.setGoodsWeight(businessIn.getGoodsWeight());
            entity.setGoodsNetWeight(businessIn.getGoodsNetWeight());
            entity.setGangNo(businessIn.getGangNo());
            entity.setIndexSort(businessIn.getIndexSort());
        }
        return this;
    }
}
