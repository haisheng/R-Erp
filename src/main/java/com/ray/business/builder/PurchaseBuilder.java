package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.business.table.params.purchase.PurchaseCreateParams;
import com.ray.business.table.params.purchase.PurchaseEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Purchase 构造器
 * @Class: PurchaseBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class PurchaseBuilder extends AbstractBuilder<ProdPurchase, PurchaseBuilder> {

    public PurchaseBuilder() {
        super(new ProdPurchase());
        super.setBuilder(this);
        entity.setOrderNo(CodeCreateUtil.getCode(Perifx.PURCHASE_CODE));
        log.info("获得采购订单编码:{}", entity.getOrderNo());
    }

    @Override
    public ProdPurchase bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public PurchaseBuilder append(PurchaseCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public PurchaseBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public PurchaseBuilder appendCode(String code) {
        entity.setOrderNo(code);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public PurchaseBuilder append(PurchaseEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getOrderNo();
    }


    public PurchaseBuilder appendStatus(String status) {
        entity.setOrderStatus(status);
        return this;
    }

    public PurchaseBuilder append(BigDecimal quantity) {
        entity.setQuantity(quantity);
        return this;
    }

    public PurchaseBuilder appendTotal(BigDecimal total) {
        entity.setTotalAmount(total);
        return this;
    }
}
