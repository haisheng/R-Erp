package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.table.entity.*;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.business.table.vo.BusinessQuickVO;
import com.ray.business.table.vo.BusinessVO;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysUser;
import com.ray.wms.table.entity.WmsWarehouse;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BusinessQuickVOBuilder extends AbstractBuilder<BusinessQuickVO, BusinessQuickVOBuilder> {

    public BusinessQuickVOBuilder() {
        super(new BusinessQuickVO());
        super.setBuilder(this);
    }

    @Override
    public BusinessQuickVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    @Override
    public String getCode() {
        return entity.getQuickCode();
    }


    public BusinessQuickVOBuilder append(ProdBusinessQuick prodBusinessQuick) {
        if (ObjectUtil.isNotNull(prodBusinessQuick)) {
            BeanUtil.copyProperties(prodBusinessQuick, entity);
        }
        return this;
    }


    public BusinessQuickVOBuilder append(BaseCustomer customer) {
        if (ObjectUtil.isNotNull(customer)) {
            entity.setCustomerName(customer.getCustomerName());
        }
        return this;
    }


    public BusinessQuickVOBuilder append(ProdStep step) {
        if (ObjectUtil.isNotNull(step)) {
            entity.setStepName(step.getStepName());
        }
        return this;
    }

    public BusinessQuickVOBuilder append(List<BusinessGoodsVO> goodsVOS) {
        entity.setGoods(goodsVOS);
        return this;
    }

    public BusinessQuickVOBuilder appendCustomerName(String customerName) {
        entity.setCustomerName(customerName);
        return this;
    }


    public BusinessQuickVOBuilder appendEmployee(SysUser user) {
        if (ObjectUtil.isNotNull(user)) {
            entity.setEmployee(user.getUserCode());
            entity.setEmployeeName(user.getUserName());
        }
        return this;
    }

    public BusinessQuickVOBuilder appendEmployeeA(SysUser user) {
        if (ObjectUtil.isNotNull(user)) {
            entity.setEmployeea(user.getUserCode());
            entity.setEmployeeName(String.format("%s,%s", entity.getEmployeeName(), user.getUserName()));
        }
        return this;
    }

    public BusinessQuickVOBuilder append(MaterialModelVO materialModelVO) {
        if (ObjectUtil.isNotNull(materialModelVO)) {
            entity.setModelName(materialModelVO.getModelName());
            entity.setModelProp(materialModelVO.getModelProp());
            entity.setGoodsName(materialModelVO.getMaterialName());
        }
        return this;
    }
}
