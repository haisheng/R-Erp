package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdSaleRecord;
import com.ray.business.table.params.sale.SaleGoodsParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Sale 构造器
 * @Class: SaleBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class SaleRecordBuilder extends AbstractBuilder<ProdSaleRecord, SaleRecordBuilder> {

    public SaleRecordBuilder() {
        super(new ProdSaleRecord());
        super.setBuilder(this);
        entity.setCode(CodeCreateUtil.getCode(Perifx.RECORD_CODE));
    }

    @Override
    public ProdSaleRecord bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public SaleRecordBuilder append(SaleGoodsParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }


    /**
     *  添加编码
     * @return
     */
    public SaleRecordBuilder appendCode(String code) {
        entity.setOrderNo(code);
        return this;
    }


    @Override
    public String getCode() {
        return entity.getOrderNo();
    }



}
