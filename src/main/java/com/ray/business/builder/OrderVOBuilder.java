package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.business.table.entity.ProdOrder;
import com.ray.business.table.params.order.OrderCreateParams;
import com.ray.business.table.params.order.OrderEditParams;
import com.ray.business.table.vo.OrderGoodsVO;
import com.ray.business.table.vo.OrderVO;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysUser;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class OrderVOBuilder extends AbstractBuilder<OrderVO, OrderVOBuilder> {

    public OrderVOBuilder() {
        super(new OrderVO());
        super.setBuilder(this);
    }

    @Override
    public OrderVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param order
     * @return
     */
    public OrderVOBuilder append(ProdOrder order) {
        if (ObjectUtil.isNotNull(order)) {
            BeanUtil.copyProperties(order, entity);
        }
        return this;
    }

    public OrderVOBuilder append(BaseCustomer baseCustomer) {
        if (ObjectUtil.isNotNull(baseCustomer)) {
            entity.setCustomerCode(baseCustomer.getCustomerCode());
            entity.setCustomerName(baseCustomer.getCustomerName());
        }
        return this;
    }

    public OrderVOBuilder append(List<OrderGoodsVO> goods) {
        entity.setGoods(goods);
        return this;
    }

    public OrderVOBuilder append(SysUser sysUser) {
        if (ObjectUtil.isNotNull(sysUser)) {
            entity.setOwnerCode(sysUser.getUserCode());
            entity.setOwnerName(sysUser.getUserName());
        }
        return this;
    }
}
