package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.business.table.entity.ProdPurchaseBack;
import com.ray.business.table.params.purchase.back.PurchaseBackCreateParams;
import com.ray.business.table.params.purchase.back.PurchaseBackEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Purchase 构造器
 * @Class: PurchaseBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class PurchaseBackBuilder extends AbstractBuilder<ProdPurchaseBack, PurchaseBackBuilder> {

    public PurchaseBackBuilder() {
        super(new ProdPurchaseBack());
        super.setBuilder(this);
        entity.setBackCode(CodeCreateUtil.getCode(Perifx.PURCHASE_BACK_CODE));
        log.info("获得采购退货订单编码:{}", entity.getBackCode());
    }

    @Override
    public ProdPurchaseBack bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public PurchaseBackBuilder append(PurchaseBackCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public PurchaseBackBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public PurchaseBackBuilder appendCode(String code) {
        entity.setBackCode(code);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public PurchaseBackBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public PurchaseBackBuilder append(PurchaseBackEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getBackCode();
    }


    public PurchaseBackBuilder appendStatus(String status) {
        entity.setOrderStatus(status);
        return this;
    }

    public PurchaseBackBuilder append(ProdPurchase prodPurchase) {
        if (ObjectUtil.isNotNull(prodPurchase)) {
            entity.setCustomerCode(prodPurchase.getCustomerCode());
            entity.setWarehouseCode(prodPurchase.getWarehouseCode());
        }
        return this;
    }

    public PurchaseBackBuilder append(BigDecimal quantity) {
        entity.setQuantity(quantity);
        return this;
    }

    public PurchaseBackBuilder appendTotal(BigDecimal total) {
        entity.setTotalAmount(total);
        return this;
    }

    public PurchaseBackBuilder appendJuanTotal(int sum) {
        entity.setTotal(sum);
        return this;
    }
}
