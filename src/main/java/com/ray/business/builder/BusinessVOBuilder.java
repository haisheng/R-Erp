package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.table.entity.*;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.business.table.vo.BusinessVO;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysUser;
import com.ray.wms.table.entity.WmsWarehouse;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BusinessVOBuilder extends AbstractBuilder<BusinessVO, BusinessVOBuilder> {

    public BusinessVOBuilder() {
        super(new BusinessVO());
        super.setBuilder(this);
    }

    @Override
    public BusinessVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    @Override
    public String getCode() {
        return entity.getBusinessCode();
    }


    public BusinessVOBuilder append(ProdBusiness prodBusiness) {
        if (ObjectUtil.isNotNull(prodBusiness)) {
            BeanUtil.copyProperties(prodBusiness, entity);
            entity.setAmount(NumberUtil.null2Zero(prodBusiness.getPrice()).multiply(NumberUtil.null2Zero(prodBusiness.getQuantity())));
        }
        return this;
    }

    public BusinessVOBuilder append(WmsWarehouse warehouse) {
        if (ObjectUtil.isNotNull(warehouse)) {
            entity.setWarehouseName(warehouse.getWarehouseName());
        }
        return this;
    }

    public BusinessVOBuilder append(BaseCustomer customer) {
        if (ObjectUtil.isNotNull(customer)) {
            entity.setCustomerName(customer.getCustomerName());
        }
        return this;
    }


    public BusinessVOBuilder append(ProdStep step) {
        if (ObjectUtil.isNotNull(step)) {
            entity.setStepName(step.getStepName());
        }
        return this;
    }

    public BusinessVOBuilder append(List<BusinessGoodsVO> goodsVOS) {
        entity.setGoods(goodsVOS);
        return this;
    }

    public BusinessVOBuilder appendCustomerName(String customerName) {
        entity.setCustomerName(customerName);
        return this;
    }

    public BusinessVOBuilder appendDeductionAmount(BigDecimal amount) {
        entity.setDeductionAmount(amount);
        return this;
    }

    public BusinessVOBuilder append(ProdPurchase prodPurchase) {
        if (ObjectUtil.isNotNull(prodPurchase)) {
            entity.setBillNo(prodPurchase.getBillNo());
            entity.setQuantity(prodPurchase.getFinishQuantity());
            entity.setAmount(prodPurchase.getTotalAmount());
            entity.setBusinessCode(prodPurchase.getOrderNo());
            entity.setCreateUserCode(prodPurchase.getCreateUserCode());
            entity.setCreateUserName(prodPurchase.getCreateUserName());
            entity.setCreateTime(prodPurchase.getCreateTime());
        }
        return this;
    }


    public BusinessVOBuilder append(ProdSale prodSale) {
        if (ObjectUtil.isNotNull(prodSale)) {
            entity.setBillNo(prodSale.getBillNo());
            entity.setQuantity(prodSale.getFinishQuantity());
            entity.setBusinessCode(prodSale.getOrderNo());
            entity.setAmount(prodSale.getTotalAmount());
            entity.setCreateUserCode(prodSale.getCreateUserCode());
            entity.setCreateUserName(prodSale.getCreateUserName());
            entity.setCreateTime(prodSale.getCreateTime());
        }
        return this;
    }

    public BusinessVOBuilder append(ProdOrderSend prodSale) {
        if (ObjectUtil.isNotNull(prodSale)) {
            entity.setBillNo(prodSale.getBillNo());
            entity.setQuantity(prodSale.getQuantity());
            entity.setBusinessCode(prodSale.getOrderNo());
            entity.setAmount(prodSale.getTotalAmount());
            entity.setCreateUserCode(prodSale.getCreateUserCode());
            entity.setCreateUserName(prodSale.getCreateUserName());
            entity.setCreateTime(prodSale.getCreateTime());
        }
        return this;
    }

    public BusinessVOBuilder appendEmployee(SysUser user) {
        if (ObjectUtil.isNotNull(user)) {
            entity.setEmployee(user.getUserCode());
            entity.setEmployeeName(user.getUserName());
        }
        return this;
    }

    public BusinessVOBuilder appendEmployeeA(SysUser user) {
        if (ObjectUtil.isNotNull(user)) {
            entity.setEmployeea(user.getUserCode());
            entity.setEmployeeName(String.format("%s,%s", entity.getEmployeeName(), user.getUserName()));
        }
        return this;
    }

    public BusinessVOBuilder append(MaterialModelVO materialModelVO) {
        if (ObjectUtil.isNotNull(materialModelVO)) {
            entity.setModelName(materialModelVO.getModelName());
            entity.setModelProp(materialModelVO.getModelProp());
            entity.setGoodsName(materialModelVO.getMaterialName());
        }
        return this;
    }

    public BusinessVOBuilder append(ProdOrderStep prodOrderStep) {
        if (ObjectUtil.isNotNull(prodOrderStep)) {
          entity.setOrderStepCode(prodOrderStep.getCode());
        }
        return this;
    }
}
