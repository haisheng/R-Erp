package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.business.table.entity.ProdPurchaseIn;
import com.ray.business.table.params.purchase.in.PurchaseInCreateParams;
import com.ray.business.table.params.purchase.in.PurchaseInEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Purchase 构造器
 * @Class: PurchaseBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class PurchaseInBuilder extends AbstractBuilder<ProdPurchaseIn, PurchaseInBuilder> {

    public PurchaseInBuilder() {
        super(new ProdPurchaseIn());
        super.setBuilder(this);
        entity.setInCode(CodeCreateUtil.getCode(Perifx.PURCHASE_IN_CODE));
        log.info("获得采购入库订单编码:{}", entity.getInCode());
    }

    @Override
    public ProdPurchaseIn bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public PurchaseInBuilder append(PurchaseInCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public PurchaseInBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public PurchaseInBuilder appendCode(String code) {
        entity.setInCode(code);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public PurchaseInBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public PurchaseInBuilder append(PurchaseInEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getInCode();
    }


    public PurchaseInBuilder appendStatus(String status) {
        entity.setOrderStatus(status);
        return this;
    }

    public PurchaseInBuilder append(ProdPurchase prodPurchase) {
        if (ObjectUtil.isNotNull(prodPurchase)) {
            entity.setCustomerCode(prodPurchase.getCustomerCode());
            entity.setWarehouseCode(prodPurchase.getWarehouseCode());
        }
        return this;
    }

    public PurchaseInBuilder append(BigDecimal quantity) {
        entity.setQuantity(quantity);
        return this;
    }

    public PurchaseInBuilder appendTotal(BigDecimal total) {
        entity.setTotalAmount(total);
        return this;
    }

    public PurchaseInBuilder appendJuanTotal(Integer total) {
        entity.setTotal(total);
        return this;
    }

    public BigDecimal getQuantity() {
        return entity.getQuantity();
    }

    public BigDecimal getAmount() {
        return entity.getTotalAmount();
    }

    public Integer getTotal() {
        return entity.getTotal();
    }
}
