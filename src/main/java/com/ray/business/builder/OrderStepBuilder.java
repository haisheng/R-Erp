package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdOrderStep;
import com.ray.business.table.params.order.step.OrderStepCreateParams;
import com.ray.business.table.params.order.step.OrderStepEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class OrderStepBuilder extends AbstractBuilder<ProdOrderStep, OrderStepBuilder> {

    public OrderStepBuilder() {
        super(new ProdOrderStep());
        super.setBuilder(this);
        entity.setCode(CodeCreateUtil.getCode(Perifx.CODE));
        log.info("获得编码:{}", entity.getCode());
    }

    @Override
    public ProdOrderStep bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public OrderStepBuilder append(OrderStepCreateParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    /**
     *  添加状态
     * @return
     */
    public OrderStepBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }
    /**
     *  添加编码
     * @return
     */
    public OrderStepBuilder appendCode(String code) {
        entity.setCode(code);
        return this;
    }
    /**
     * 构建创建信息
     * @param editParams
     * @return
     */
    public OrderStepBuilder append(OrderStepEditParams editParams) {
        if(ObjectUtil.isNotNull(editParams)){
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getCode();
    }



}
