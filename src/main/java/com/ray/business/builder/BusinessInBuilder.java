package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.business.table.entity.ProdBusinessIn;
import com.ray.business.table.params.business.BusinessEditParams;
import com.ray.business.table.params.business.BusinessGoodsParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;
import org.omg.CORBA.OBJ_ADAPTER;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @author bo shen
 * @Description: Business 构造器
 * @Class: BusinessBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BusinessInBuilder extends AbstractBuilder<ProdBusinessIn, BusinessInBuilder> {

    public BusinessInBuilder() {
        super(new ProdBusinessIn());
        super.setBuilder(this);
        entity.setCode(CodeCreateUtil.getCode(Perifx.RECORD_CODE));
    }

    @Override
    public ProdBusinessIn bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    /**
     * 添加编码
     *
     * @return
     */
    public BusinessInBuilder appendBusinessCode(String code) {
        entity.setBusinessCode(code);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param goodsParams
     * @return
     */
    public BusinessInBuilder append(BusinessGoodsParams goodsParams) {
        if (ObjectUtil.isNotNull(goodsParams)) {
            BeanUtil.copyProperties(goodsParams, entity);
            //没有批次号默认一个批次号
            if(StrUtil.isBlank(entity.getBatchNo())){
                entity.setBatchNo(DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN));
            }
            if(ObjectUtil.isNotNull(goodsParams.getRealQuantity())){
                entity.setRealQuantity(goodsParams.getQuantity());
            }
        }
        entity.setCode(CodeCreateUtil.getCode(Perifx.RECORD_CODE));
        return this;
    }

    @Override
    public String getCode() {
        return entity.getBusinessCode();
    }


    public BusinessInBuilder appendOutCode(String code) {
        entity.setOutCode(code);
        return this;
    }

    public BusinessInBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }

    public BusinessInBuilder appendGangNo(String gangNo) {
        if(StrUtil.isNotBlank(gangNo)){
            entity.setGangNo(gangNo);
        }
        return this;
    }
}
