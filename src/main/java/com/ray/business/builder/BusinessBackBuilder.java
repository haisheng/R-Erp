package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdBusinessBack;
import com.ray.business.table.params.business.BusinessCreateParams;
import com.ray.business.table.params.business.BusinessEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Business 构造器
 * @Class: BusinessBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BusinessBackBuilder extends AbstractBuilder<ProdBusinessBack, BusinessBackBuilder> {

    public BusinessBackBuilder() {
        super(new ProdBusinessBack());
        super.setBuilder(this);
        entity.setBackCode(CodeCreateUtil.getCode(Perifx.BUSINESS_BACK_CODE));
        log.info("获得工单号编码:{}", entity.getBackCode());
    }

    @Override
    public ProdBusinessBack bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public BusinessBackBuilder append(ProdBusiness createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            entity.setBusinessCode(createParams.getBusinessCode());
            entity.setCustomerCode(createParams.getCustomerCode());
            entity.setWarehouseCode(createParams.getWarehouseCode());
            entity.setStepCode(createParams.getStepCode());
            entity.setPrice(createParams.getPrice());
            entity.setOrderNo(createParams.getOrderNo());
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public BusinessBackBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }


    /**
     * 添加编码
     *
     * @return
     */
    public BusinessBackBuilder appendCode(String code) {
        entity.setBackCode(code);
        return this;
    }


    @Override
    public String getCode() {
        return entity.getBackCode();
    }


    public BusinessBackBuilder appendStatus(String status) {
        entity.setOrderStatus(status);
        return this;
    }

    public BusinessBackBuilder append(BigDecimal quantity) {
        entity.setQuantity(quantity);
        return this;
    }

    public BusinessBackBuilder appendJuan(Integer juans) {
        entity.setTotal(juans);
        return this;
    }
}
