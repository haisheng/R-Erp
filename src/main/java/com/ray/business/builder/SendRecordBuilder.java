package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdBusinessIn;
import com.ray.business.table.entity.ProdOrderGoods;
import com.ray.business.table.entity.ProdSendRecord;
import com.ray.business.table.params.sale.SaleGoodsParams;
import com.ray.business.table.params.send.SendGoodsParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Sale 构造器
 * @Class: SaleBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class SendRecordBuilder extends AbstractBuilder<ProdSendRecord, SendRecordBuilder> {

    public SendRecordBuilder() {
        super(new ProdSendRecord());
        super.setBuilder(this);
        entity.setCode(CodeCreateUtil.getCode(Perifx.RECORD_CODE));
    }

    @Override
    public ProdSendRecord bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public SendRecordBuilder append(SendGoodsParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public SendRecordBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public SendRecordBuilder appendSendCode(String sendCode) {
        entity.setSendCode(sendCode);
        return this;
    }


    @Override
    public String getCode() {
        return entity.getOrderNo();
    }


    public SendRecordBuilder appendInCode(String code) {
        entity.setInCode(code);
        return this;
    }

    public SendRecordBuilder appendPrice(ProdOrderGoods prodOrderGoods) {
        if (ObjectUtil.isNotNull(prodOrderGoods)) {
           entity.setPrice(prodOrderGoods.getPrice());
        }
        return this;
    }

    public SendRecordBuilder append(ProdBusinessIn businessIn) {
        if (ObjectUtil.isNotNull(businessIn)) {
            entity.setDeleteQuantity(businessIn.getDeleteQuantity());
            entity.setRealQuantity(businessIn.getRealQuantity());
            entity.setGoodsWeight(businessIn.getGoodsWeight());
            entity.setGoodsNetWeight(businessIn.getGoodsNetWeight());
            entity.setGangNo(businessIn.getGangNo());
        }
        return this;
    }
}
