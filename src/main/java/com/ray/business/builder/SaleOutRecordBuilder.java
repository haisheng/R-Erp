package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdPurchaseInRecord;
import com.ray.business.table.entity.ProdSaleOutRecord;
import com.ray.business.table.params.purchase.PurchaseGoodsParams;
import com.ray.business.table.params.sale.SaleGoodsParams;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: ProdSaleOutRecord 构造器
 * @Class: ProdSaleOutRecord
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class SaleOutRecordBuilder extends AbstractBuilder<ProdSaleOutRecord, SaleOutRecordBuilder> {

    public SaleOutRecordBuilder() {
        super(new ProdSaleOutRecord());
        super.setBuilder(this);
    }

    @Override
    public ProdSaleOutRecord bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public SaleOutRecordBuilder append(SaleGoodsParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }


    /**
     * 添加编码
     *
     * @return
     */
    public SaleOutRecordBuilder appendCode(String code) {
        entity.setOutCode(code);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public SaleOutRecordBuilder appendOrderNo(String code) {
        entity.setOrderNo(code);
        return this;
    }


    @Override
    public String getCode() {
        return entity.getOutCode();
    }


    public SaleOutRecordBuilder appendPrice(BigDecimal price) {
        entity.setPrice(price);
        return this;
    }

    public SaleOutRecordBuilder appendRecordCode(String code) {
        entity.setRecordCode(code);
        return this;
    }
}
