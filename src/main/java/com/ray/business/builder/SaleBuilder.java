package com.ray.business.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdSale;
import com.ray.business.table.params.sale.SaleCreateParams;
import com.ray.business.table.params.sale.SaleEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Sale 构造器
 * @Class: SaleBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class SaleBuilder extends AbstractBuilder<ProdSale, SaleBuilder> {

    public SaleBuilder() {
        super(new ProdSale());
        super.setBuilder(this);
        entity.setOrderNo(CodeCreateUtil.getCode(Perifx.SALE_CODE));
        log.info("获得销售订单编码:{}", entity.getOrderNo());
    }

    @Override
    public ProdSale bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public SaleBuilder append(SaleCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public SaleBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public SaleBuilder appendCode(String code) {
        entity.setOrderNo(code);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public SaleBuilder append(SaleEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getOrderNo();
    }


    public SaleBuilder appendStatus(String status) {
        entity.setOrderStatus(status);
        return this;
    }

    public SaleBuilder append(BigDecimal quantity) {
        entity.setQuantity(quantity);
        return this;
    }

    public SaleBuilder appendTotal(BigDecimal total) {
        entity.setTotalAmount(total);
        return this;
    }
}
