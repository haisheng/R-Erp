package com.ray.business.check;

import cn.hutool.core.util.StrUtil;
import com.ray.business.enums.OrderStatusEnum;
import com.ray.business.enums.PurchaseStatusEnum;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: PurchaseCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class PurchaseCheck extends AbstractCheck<ProdPurchase> {


    public PurchaseCheck(ProdPurchase entity) {
        super(entity);
    }

    @Override
    public PurchaseCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public PurchaseCheck checkCanEdit(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), PurchaseStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public PurchaseCheck checkFinish(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), PurchaseStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public PurchaseCheck checkPass(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), PurchaseStatusEnum.FINISH.getValue()) &&
                !StrUtil.equals(entity.getOrderStatus(), PurchaseStatusEnum.CHECKED.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
    /**
     * 校验
     *
     * @param messageCode
     */
    public PurchaseCheck checkCanFinish(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), PurchaseStatusEnum.CHECKED.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


    /**
     * 校验
     *
     * @param messageCode
     */
    public PurchaseCheck checkCanCancel(String messageCode) {
        if ((!StrUtil.equals(entity.getOrderStatus(), PurchaseStatusEnum.FINISH.getValue()) &&
                !StrUtil.equals(entity.getOrderStatus(), PurchaseStatusEnum.CHECKED.getValue())
        ) || entity.getBillStatus() == YesOrNoEnum.YES.getValue()) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public PurchaseCheck checkCanCheck(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), PurchaseStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public PurchaseCheck canBill(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), PurchaseStatusEnum.FINISH.getValue()) || entity.getBillStatus() == YesOrNoEnum.YES.getValue()) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public PurchaseCheck checkCanIn(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), PurchaseStatusEnum.CHECKED.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


    public PurchaseCheck canDeduction(String messageCode) {
        if (entity.getBillStatus() == YesOrNoEnum.YES.getValue().intValue()
                || !StrUtil.equals(entity.getOrderStatus(), PurchaseStatusEnum.CHECKED.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public PurchaseCheck canDeleteDeduction(String messageCode) {
        if (entity.getBillStatus() == YesOrNoEnum.YES.getValue().intValue()) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public PurchaseCheck checkCanOpen(String messageCode) {
        if (entity.getBillStatus() == YesOrNoEnum.YES.getValue().intValue()
                || !StrUtil.equals(entity.getOrderStatus(), PurchaseStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public PurchaseCheck checkCanAdvance(String messageCode) {
        if(StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.FINISH.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
