package com.ray.business.check;

import cn.hutool.core.util.StrUtil;
import com.ray.business.enums.SaleStatusEnum;
import com.ray.business.table.entity.ProdSale;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: PurchaseCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class SaleCheck extends AbstractCheck<ProdSale> {


    public SaleCheck(ProdSale entity) {
        super(entity);
    }

    @Override
    public SaleCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public SaleCheck checkCanEdit(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), SaleStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public SaleCheck checkFinish(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), SaleStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public SaleCheck checkCanFinish(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), SaleStatusEnum.CHECKED.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


    /**
     * 校验
     *
     * @param messageCode
     */
    public SaleCheck checkCanCancel(String messageCode) {
        if ((!StrUtil.equals(entity.getOrderStatus(), SaleStatusEnum.FINISH.getValue()) &&
                !StrUtil.equals(entity.getOrderStatus(), SaleStatusEnum.CHECKED.getValue())) || entity.getBillStatus() == YesOrNoEnum.YES.getValue()) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public SaleCheck checkCanCheck(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), SaleStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public SaleCheck canBill(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), SaleStatusEnum.FINISH.getValue()) || entity.getBillStatus() == YesOrNoEnum.YES.getValue()) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public SaleCheck checkCanIn(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), SaleStatusEnum.CHECKED.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public SaleCheck checkCanPass(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), SaleStatusEnum.CHECKED.getValue()) &&
                !StrUtil.equals(entity.getOrderStatus(), SaleStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public SaleCheck checkCanOpen(String messageCode) {
        if (entity.getBillStatus() == YesOrNoEnum.YES.getValue().intValue()
                || !StrUtil.equals(entity.getOrderStatus(), SaleStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
