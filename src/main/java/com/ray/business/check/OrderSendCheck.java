package com.ray.business.check;

import cn.hutool.core.util.StrUtil;
import com.ray.business.enums.OrderSendStatusEnum;
import com.ray.business.enums.SaleStatusEnum;
import com.ray.business.table.entity.ProdOrderSend;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: OrderSendCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class OrderSendCheck extends AbstractCheck<ProdOrderSend> {


    public OrderSendCheck(ProdOrderSend entity) {
        super(entity);
    }

    @Override
    public OrderSendCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public OrderSendCheck checkCanEdit(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), OrderSendStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public OrderSendCheck checkFinish(String messageCode) {
        if (StrUtil.equals(entity.getOrderStatus(), OrderSendStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


    /**
     * 校验
     *
     * @param messageCode
     */
    public OrderSendCheck checkCanCancel(String messageCode) {
        if ((!StrUtil.equals(entity.getOrderStatus(), OrderSendStatusEnum.FINISH.getValue()) &&
                !StrUtil.equals(entity.getOrderStatus(), OrderSendStatusEnum.UN_OUT.getValue())) || entity.getBillStatus() == YesOrNoEnum.YES.getValue()) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


    public OrderSendCheck canBill(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), OrderSendStatusEnum.FINISH.getValue()) || entity.getBillStatus() == YesOrNoEnum.YES.getValue()) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public OrderSendCheck checkCanCheck(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), OrderSendStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public OrderSendCheck checkCanDelete(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), OrderSendStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
