package com.ray.business.check;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.business.table.entity.ProdErrorType;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: StepCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class ErrorTypeCheck extends AbstractCheck<ProdErrorType> {


    public ErrorTypeCheck(ProdErrorType entity) {
        super(entity);
    }

    @Override
    public ErrorTypeCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }
    /**
     * 校验
     * @param errorCode
     * @param messageCode
     */
    public ErrorTypeCheck checkErrprName(String errorCode, String messageCode) {
        //存在 并且appCode相等
        if(ObjectUtil.isNotNull(entity) && !StrUtil.equals(errorCode,entity.getErrorCode())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

}
