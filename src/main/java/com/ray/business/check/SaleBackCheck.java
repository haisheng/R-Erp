package com.ray.business.check;

import cn.hutool.core.util.StrUtil;
import com.ray.business.enums.PurchaseInStatusEnum;
import com.ray.business.table.entity.ProdSaleBack;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: PurchaseCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class SaleBackCheck extends AbstractCheck<ProdSaleBack> {


    public SaleBackCheck(ProdSaleBack entity) {
        super(entity);
    }

    @Override
    public SaleBackCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public SaleBackCheck checkCanEdit(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), PurchaseInStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public SaleBackCheck checkFinish(String messageCode) {
        if (StrUtil.equals(entity.getOrderStatus(), PurchaseInStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


    /**
     * 校验
     *
     * @param messageCode
     */
    public SaleBackCheck checkCanCancel(String messageCode) {
        if ((!StrUtil.equals(entity.getOrderStatus(), PurchaseInStatusEnum.FINISH.getValue())
                && !StrUtil.equals(entity.getOrderStatus(), PurchaseInStatusEnum.UN_IN.getValue())) || entity.getBillStatus() == YesOrNoEnum.YES.getValue()) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public SaleBackCheck checkCanCheck(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), PurchaseInStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
