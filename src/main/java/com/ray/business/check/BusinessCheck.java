package com.ray.business.check;

import cn.hutool.core.util.StrUtil;
import com.ray.business.enums.BusinessStatusEnum;
import com.ray.business.enums.BusinessTypeEnum;
import com.ray.business.enums.OrderStatusEnum;
import com.ray.business.enums.PurchaseStatusEnum;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: StepCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class BusinessCheck extends AbstractCheck<ProdBusiness> {


    public BusinessCheck(ProdBusiness entity) {
        super(entity);
    }

    @Override
    public BusinessCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public BusinessCheck checkCanEdit(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BusinessStatusEnum.UN_CHECK.getValue()) &&
                !StrUtil.equals(entity.getOrderStatus(), BusinessStatusEnum.CANCEL.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public BusinessCheck checkFinish(String messageCode) {
        if (StrUtil.equals(entity.getOrderStatus(), BusinessStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
    /**
     * 校验
     *
     * @param messageCode
     */
    public BusinessCheck checkNotFinish(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BusinessStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


    /**
     * 校验
     *
     * @param messageCode
     */
    public BusinessCheck canBill(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BusinessStatusEnum.FINISH.getValue()) || entity.getBillStatus() == YesOrNoEnum.YES.getValue()) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public BusinessCheck checkCanCancel(String messageCode) {
        if ((!StrUtil.equals(entity.getOrderStatus(), BusinessStatusEnum.FINISH.getValue())
                && !StrUtil.equals(entity.getOrderStatus(), BusinessStatusEnum.UN_IN.getValue()))
                || entity.getBillStatus() == YesOrNoEnum.YES.getValue()) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public BusinessCheck checkCanCheck(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BusinessStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public BusinessCheck canDeduction(String messageCode) {
        if (!StrUtil.equals(entity.getBusinessType(), BusinessTypeEnum.IN.getValue()) || entity.getBillStatus() == YesOrNoEnum.YES.getValue().intValue()
                || !StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public BusinessCheck canDeleteDeduction(String messageCode) {
        if (entity.getBillStatus() == YesOrNoEnum.YES.getValue().intValue()) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public BusinessCheck checkCanDelete(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BusinessStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public BusinessCheck checkOutType(String messageCode) {
        if (!StrUtil.equals(entity.getBusinessType(), BusinessTypeEnum.OUT.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public BusinessCheck checkCanBack(String messageCode) {
        if (StrUtil.equals(entity.getBusinessType(), BusinessTypeEnum.IN.getValue()) || entity.getBillStatus() == YesOrNoEnum.YES.getValue().intValue()
                || !StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public BusinessCheck checkBusinessType(String businessType, String messageCode) {
        if (StrUtil.isNotBlank(businessType) && !StrUtil.equals(entity.getBusinessType(), businessType)) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
