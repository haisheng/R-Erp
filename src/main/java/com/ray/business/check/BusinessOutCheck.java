package com.ray.business.check;

import cn.hutool.core.util.StrUtil;
import com.ray.business.table.entity.ProdBusinessOut;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: StepCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class BusinessOutCheck extends AbstractCheck<ProdBusinessOut> {


    public BusinessOutCheck(ProdBusinessOut entity) {
        super(entity);
    }

    @Override
    public BusinessOutCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }

    public BusinessOutCheck checkQuantity(BigDecimal quantity, String message) {
        if(entity.getQuantity().compareTo(quantity) != 0){
            throw BusinessExceptionFactory.newException(message);
        }
        return this;
    }

    public BusinessOutCheck checkBackQuantity(BigDecimal quantity, String message) {
        if(entity.getQuantity().compareTo(quantity) < 0){
            throw BusinessExceptionFactory.newException(message);
        }
        return this;
    }

    public BusinessOutCheck checkBusinessCode( String businessCode, String message) {
        if(!StrUtil.equals(businessCode,entity.getBusinessCode())){
            throw BusinessExceptionFactory.newException(message);
        }
        return this;
    }
}
