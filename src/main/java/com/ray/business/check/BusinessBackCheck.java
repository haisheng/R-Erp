package com.ray.business.check;

import cn.hutool.core.util.StrUtil;
import com.ray.business.enums.BusinessBackStatusEnum;
import com.ray.business.table.entity.ProdBusinessBack;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: StepCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class BusinessBackCheck extends AbstractCheck<ProdBusinessBack> {


    public BusinessBackCheck(ProdBusinessBack entity) {
        super(entity);
    }

    @Override
    public BusinessBackCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public BusinessBackCheck checkCanEdit(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BusinessBackStatusEnum.UN_CHECK.getValue()) &&
                !StrUtil.equals(entity.getOrderStatus(), BusinessBackStatusEnum.CANCEL.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public BusinessBackCheck checkFinish(String messageCode) {
        if (StrUtil.equals(entity.getOrderStatus(), BusinessBackStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public BusinessBackCheck checkNotFinish(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BusinessBackStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


    /**
     * 校验
     *
     * @param messageCode
     */
    public BusinessBackCheck checkCanCancel(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BusinessBackStatusEnum.FINISH.getValue()) &&
                !StrUtil.equals(entity.getOrderStatus(), BusinessBackStatusEnum.UN_IN.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public BusinessBackCheck checkCanCheck(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BusinessBackStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


    public BusinessBackCheck checkCanDelete(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BusinessBackStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


}
