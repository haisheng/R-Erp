package com.ray.business.check;

import cn.hutool.core.util.StrUtil;
import com.ray.business.enums.DeductionStatusEnum;
import com.ray.business.table.entity.ProdPurchaseDeduction;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: PurchaseDeductionCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class PurchaseDeductionCheck extends AbstractCheck<ProdPurchaseDeduction> {


    public PurchaseDeductionCheck(ProdPurchaseDeduction entity) {
        super(entity);
    }

    @Override
    public PurchaseDeductionCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }



    public PurchaseDeductionCheck checkCanFinance(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), DeductionStatusEnum.PASS.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public PurchaseDeductionCheck checkCanCheck(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), DeductionStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public PurchaseDeductionCheck checkEdit(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), DeductionStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
