package com.ray.business.check;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.business.enums.OrderStatusEnum;
import com.ray.business.table.entity.ProdOrder;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: OrderCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class OrderCheck extends AbstractCheck<ProdOrder> {


    public OrderCheck(ProdOrder entity) {
        super(entity);
    }

    @Override
    public OrderCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }
    /**
     * 校验
     * @param orderNo
     * @param messageCode
     */
    public OrderCheck checkOrderName(String orderNo, String messageCode) {
        //存在 并且appCode相等
        if(ObjectUtil.isNotNull(entity) && !StrUtil.equals(orderNo,entity.getOrderNo())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public OrderCheck checkCanProd(String messageCode) {
        if(!StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.PASS.getValue()) &&
                !StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.FINISH.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public OrderCheck checkCanCheck(String messageCode) {
        if(!StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.CHECKING.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public OrderCheck checkCanCancel(String messageCode) {
        if(StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.CHECKING.getValue()) ||
                StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.FINISH.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public OrderCheck checkProd(String messageCode) {
        if(!StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.PROD.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public OrderCheck checkFinish(String messageCode) {
        if(!StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.FINISH.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public OrderCheck checkCanAdvance(String messageCode) {
        if(StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.FINISH.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public OrderCheck checkCanEdit(String messageCode) {
        if(StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.FINISH.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
