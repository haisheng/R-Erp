package com.ray.business.check;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.business.table.entity.ProdOrderGoods;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: OrderCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class OrderGoodsCheck extends AbstractCheck<ProdOrderGoods> {


    public OrderGoodsCheck(ProdOrderGoods entity) {
        super(entity);
    }

    @Override
    public OrderGoodsCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }
    /**
     * 校验
     * @param code
     * @param messageCode
     */
    public OrderGoodsCheck checkSame(String code, String messageCode) {
        //存在 并且appCode相等
        if(ObjectUtil.isNotNull(entity) && !StrUtil.equals(code,entity.getCode())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

}
