package com.ray.business.check;

import cn.hutool.core.util.StrUtil;
import com.ray.business.enums.DeductionStatusEnum;
import com.ray.business.table.entity.ProdBusinessDeduction;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: BusinessDeductionCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class DeductionCheck extends AbstractCheck<ProdBusinessDeduction> {


    public DeductionCheck(ProdBusinessDeduction entity) {
        super(entity);
    }

    @Override
    public DeductionCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }



    public DeductionCheck checkCanFinance(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), DeductionStatusEnum.PASS.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public DeductionCheck checkCanCheck(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), DeductionStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public DeductionCheck checkEdit(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), DeductionStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
