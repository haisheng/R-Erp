package com.ray.business.check;

import cn.hutool.core.util.StrUtil;
import com.ray.business.enums.SaleOutStatusEnum;
import com.ray.business.enums.SaleStatusEnum;
import com.ray.business.table.entity.ProdSaleOut;
import com.ray.business.table.entity.ProdSaleOut;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: PurchaseCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class SaleOutCheck extends AbstractCheck<ProdSaleOut> {


    public SaleOutCheck(ProdSaleOut entity) {
        super(entity);
    }

    @Override
    public SaleOutCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public SaleOutCheck checkCanEdit(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), SaleOutStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public SaleOutCheck checkFinish(String messageCode) {
        if (StrUtil.equals(entity.getOrderStatus(), SaleOutStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


    /**
     * 校验
     *
     * @param messageCode
     */
    public SaleOutCheck checkCanCancel(String messageCode) {
        if ((!StrUtil.equals(entity.getOrderStatus(), SaleOutStatusEnum.FINISH.getValue()) &&
                !StrUtil.equals(entity.getOrderStatus(), SaleOutStatusEnum.UN_OUT.getValue()))) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public SaleOutCheck checkCanCheck(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), SaleStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
