package com.ray.business.check;

import com.ray.business.table.entity.ProdBusinessIn;
import com.ray.common.check.AbstractCheck;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: StepCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class BusinessInCheck extends AbstractCheck<ProdBusinessIn> {


    public BusinessInCheck(ProdBusinessIn entity) {
        super(entity);
    }

    @Override
    public BusinessInCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }

}
