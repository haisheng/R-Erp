package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.business.api.StepApi;
import com.ray.business.table.params.step.StepCreateParams;
import com.ray.business.table.params.step.StepEditParams;
import com.ray.business.table.params.step.StepQueryParams;
import com.ray.business.table.vo.StepVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-工序管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/step")
@Api("生产服务-工序配置")
@Validated
public class StepController {

    @Autowired
    private StepApi stepApi;

    @ApiOperation("查询工序列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<StepVO>> page(@RequestBody @Valid CommonPage<StepQueryParams, Page<StepVO>> queryParams) {
        return stepApi.pageSteps(queryParams);
    }

    @ApiOperation("查询工序列表")
    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<StepVO>> list(@RequestBody @Valid StepQueryParams queryParams) {
        return stepApi.querySteps(queryParams);
    }


    @ApiOperation("工序新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid StepCreateParams createParams) {
        return stepApi.createStep(createParams);
    }

    @ApiOperation("工序编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid StepEditParams editParams) {
        return stepApi.editStep(editParams);
    }

    @ApiOperation("工序删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "工序编码", required = true) @RequestParam(required = true) String stepCode) {
        return stepApi.deleteStep(stepCode);
    }

    @ApiOperation("工序详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<StepVO> view(@ApiParam(value = "工序编码", required = true) @RequestParam(required = true) String stepCode) {
        return stepApi.viewStep(stepCode);
    }

    @ApiOperation("工序开启")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "工序编码", required = true) @RequestParam(required = true) String stepCode) {
        return stepApi.openStep(stepCode);
    }


    @ApiOperation("工序关闭")
    @PostMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "工序编码", required = true) @RequestParam(required = true) String stepCode) {
        return stepApi.closeStep(stepCode);
    }


}
