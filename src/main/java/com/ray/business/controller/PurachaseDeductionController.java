package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.business.api.DeductionApi;
import com.ray.business.api.PurchaseDeductionApi;
import com.ray.business.table.params.deduction.DeductionCreateParams;
import com.ray.business.table.params.deduction.DeductionQueryParams;
import com.ray.business.table.vo.DeductionVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-扣款管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/purchase/deduction")
@Api("生产服务-扣款")
@Validated
public class PurachaseDeductionController {

    @Autowired
    private PurchaseDeductionApi purchaseDeductionApi;

    @ApiOperation("查询扣款列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<DeductionVO>> page(@RequestBody @Valid CommonPage<DeductionQueryParams, Page<DeductionVO>> queryParams) {
        return purchaseDeductionApi.pagePurchaseDeductions(queryParams);
    }

    @ApiOperation("查询扣款列表")
    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<DeductionVO>> list(@RequestBody @Valid DeductionQueryParams queryParams) {
        return purchaseDeductionApi.queryPurchaseDeductions(queryParams);
    }


    @ApiOperation("扣款新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid DeductionCreateParams createParams) {
        return purchaseDeductionApi.createPurchaseDeduction(createParams);
    }


    @ApiOperation("扣款删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "扣款编码", required = true) @RequestParam(required = true) String deductionCode) {
        return purchaseDeductionApi.deletePurchaseDeduction(deductionCode);
    }


}
