package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.business.api.OrderStepApi;
import com.ray.business.table.params.order.step.OrderStepCreateParams;
import com.ray.business.table.params.order.step.OrderStepEditParams;
import com.ray.business.table.params.order.step.OrderStepQueryParams;
import com.ray.business.table.vo.OrderStepVO;
import com.ray.business.table.vo.StepVO;
import com.ray.util.CodeSplitUtil;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-订单工序管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/order/step")
@Api("业务服务-订单工序")
@Validated
public class OrderStepController {

    @Autowired
    private OrderStepApi orderStepApi;

    @ApiOperation("查询订单工序列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<OrderStepVO>> page(@RequestBody @Valid CommonPage<OrderStepQueryParams, Page<OrderStepVO>> queryParams) {
        return orderStepApi.pageOrderSteps(queryParams);
    }

    @ApiOperation("查询订单工序列表查询")
    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<OrderStepVO>> list(@RequestBody @Valid OrderStepQueryParams queryParams) {
        queryParams.setMaterialCode(CodeSplitUtil.getFirst(queryParams.getMaterialCode()));
        return orderStepApi.queryOrderSteps(queryParams);
    }


    @ApiOperation("订单工序新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid OrderStepCreateParams createParams) {
        return orderStepApi.createOrderStep(createParams);
    }

    @ApiOperation("订单工序编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid OrderStepEditParams editParams) {
        return orderStepApi.editOrderStep(editParams);
    }

    @ApiOperation("订单工序删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "订单工序编码", required = true) @RequestParam(required = true) String code) {
        return orderStepApi.deleteOrderStep(code);
    }

    @ApiOperation("订单工序详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<OrderStepVO> view(@ApiParam(value = "订单工序编码", required = true) @RequestParam(required = true) String code) {
        return orderStepApi.viewOrderStep(code);
    }

    
    @ApiOperation("关闭订单工序")
    @PostMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> finish(@ApiParam(value = "订单工序编码", required = true) @RequestParam(required = true) String code) {
        return orderStepApi.closeOrderStep(code);
    }

    @ApiOperation("开启订单工序")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "订单工序编码", required = true) @RequestParam(required = true) String code) {
        return orderStepApi.openOrderStep(code);
    }

    @ApiOperation("查询订单工序列表查询")
    @PostMapping(value = "/all", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<StepVO>> page(@ApiParam(value = "订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return orderStepApi.queryOrderAllSteps(orderNo);
    }

}
