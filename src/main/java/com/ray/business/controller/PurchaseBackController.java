package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.business.api.PurchaseBackApi;
import com.ray.business.api.PurchaseInApi;
import com.ray.business.api.PurchaseRecordApi;
import com.ray.business.table.params.purchase.PurchaseQueryParams;
import com.ray.business.table.params.purchase.back.PurchaseBackCreateParams;
import com.ray.business.table.params.purchase.back.PurchaseBackEditParams;
import com.ray.business.table.params.purchase.back.PurchaseBackQueryParams;
import com.ray.business.table.params.purchase.in.PurchaseInQueryParams;
import com.ray.business.table.vo.PurchaseBackVO;
import com.ray.business.table.vo.PurchaseGoodsVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import javax.validation.Valid;

/**
 * <p>
 * 系统基础服务-采购退货订单管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/purchase/back")
@Api("业务服务-采购退货订单")
@Validated
public class PurchaseBackController {

    @Autowired
    private PurchaseBackApi purchaseBackApi;
    @Autowired
    private PurchaseInApi purchaseInApi;

    @ApiOperation("查询采购退货订单列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<PurchaseBackVO>> page(@RequestBody @Valid CommonPage<PurchaseBackQueryParams, Page<PurchaseBackVO>> queryParams) {
        return purchaseBackApi.pagePurchaseBacks(queryParams);
    }


    @ApiOperation("查询采购退货订单列表查询")
    @PostMapping(value = "/listGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<PurchaseGoodsVO>> listGoods(@RequestBody @Valid PurchaseInQueryParams queryParams) {
        return purchaseInApi.listGoods(queryParams);
    }

    @ApiOperation("采购退货订单新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid PurchaseBackCreateParams createParams) {
        return purchaseBackApi.createOrder(createParams);
    }

    @ApiOperation("采购退货订单编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid PurchaseBackEditParams editParams) {
        return purchaseBackApi.editOrder(editParams);
    }

    @ApiOperation("采购退货订单删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "采购退货订单编码", required = true) @RequestParam(required = true) String backCode) {
        return purchaseBackApi.deleteOrder(backCode);
    }

    @ApiOperation("采购退货订单详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<PurchaseBackVO> view(@ApiParam(value = "采购退货订单编码", required = true) @RequestParam(required = true) String backCode) {
        return purchaseBackApi.viewOrder(backCode);
    }

    @ApiOperation("采购退货订单通过")
    @PostMapping(value = "/pass", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> pass(@ApiParam(value = "采购退货订单编码", required = true) @RequestParam(required = true) String backCode) {
        return purchaseBackApi.passOrder(backCode);
    }


    @ApiOperation("采购退货订单取消")
    @PostMapping(value = "/cancel", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> cancel(@ApiParam(value = "采购退货订单编码", required = true) @RequestParam(required = true) String backCode) {
        return purchaseBackApi.cancelOrder(backCode);
    }


}
