package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.table.vo.customer.CustomerVO;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.api.BusinessApi;
import com.ray.business.table.params.business.*;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.business.table.vo.BusinessVO;
import com.ray.business.table.vo.UserBusinessQuantityVO;
import com.ray.outside.DataService;
import com.ray.template.TemplateRecord;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.util.IpAddressUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-加工单管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/business")
@Api("业务服务-加工单")
@Validated
public class BusinessController {

    @Autowired
    private BusinessApi businessApi;
    @Autowired
    private DataService dataService;

    @ApiOperation("查询加工单列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<BusinessVO>> page(@RequestBody @Valid CommonPage<BusinessQueryParams, Page<BusinessVO>> queryParams) {
        return businessApi.pageBusinesss(queryParams);
    }

    @ApiOperation("入库客户列表")
    @PostMapping(value = "/listCustomer", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<CustomerVO>> page(@RequestBody @Valid BusinessQueryParams queryParams) {
        return businessApi.listCustomer(queryParams);
    }

    @ApiOperation("入库商品列表查询")
    @PostMapping(value = "/listInGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BusinessGoodsVO>> listInGoods(@RequestBody BusinessQueryParams queryParams) {
        return businessApi.listInGoods(queryParams);
    }

    @ApiOperation("出库商品列表查询")
    @PostMapping(value = "/listOutGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BusinessGoodsVO>> listOutGoods(@RequestBody BusinessQueryParams queryParams) {
        return businessApi.listOutGoods(queryParams);
    }

    @ApiOperation("出库商品列表查询-退回")
    @PostMapping(value = "/listBackOutGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BusinessGoodsVO>> listBackOutGoods(@RequestBody BusinessQueryParams queryParams) {
        return businessApi.listBackOutGoods(queryParams);
    }


    @ApiOperation("查询规格列表查询")
    @PostMapping(value = "/listGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<MaterialModelVO>> listGoods(@RequestBody @Valid BusinessQueryParams queryParams) {
        return businessApi.listGoods(queryParams);
    }


    @ApiOperation("加工单入库新增")
    @PostMapping(value = "/inAdd", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> inAdd(@RequestBody @Valid BusinessCreateParams createParams) {
        return businessApi.createBusinessIn(createParams);
    }

    @ApiOperation("加工单出库新增")
    @PostMapping(value = "/outAdd", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> outAdd(@RequestBody @Valid BusinessCreateParams createParams) {
        return businessApi.createBusinessOut(createParams);
    }

    @ApiOperation("加工单编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid BusinessEditParams editParams) {
        return businessApi.editBusiness(editParams);
    }

    @ApiOperation("加工单删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "加工单编码", required = true) @RequestParam(required = true) String businessCode) {
        return businessApi.deleteBusiness(businessCode);
    }

    @ApiOperation("加工单详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<BusinessVO> view(@ApiParam(value = "加工单编码", required = true) @RequestParam(required = true) String businessCode,
                                   @ApiParam(value = "加工单编码", required = false) String businessType) {
        return businessApi.viewBusiness(businessCode,businessType);
    }

    @ApiOperation("加工单通过")
    @PostMapping(value = "/pass", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> pass(@ApiParam(value = "加工单编码", required = true) @RequestParam(required = true) String businessCode) {
        return businessApi.passBusiness(businessCode);
    }

    @ApiOperation("加工单完成")
    @PostMapping(value = "/finish", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> finish(@ApiParam(value = "加工单编码", required = true) @RequestParam(required = true) String businessCode) {
        return businessApi.finishBusiness(businessCode);
    }

    @ApiOperation("加工单打开")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "加工单编码", required = true) @RequestParam(required = true) String businessCode) {
        return businessApi.openBusiness(businessCode);
    }

    @ApiOperation("取消加工单")
    @PostMapping(value = "/cancel", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> cancel(@ApiParam(value = "加工单编码", required = true) @RequestParam(required = true) String businessCode) {
        return businessApi.cancelBusiness(businessCode);
    }

    @ApiOperation("查询加工单列表查询")
    @PostMapping(value = "/quick", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BusinessVO>> listQuickBusiness(@RequestBody @Valid BusinessQueryParams queryParams) {
        return businessApi.listQuickBusiness(queryParams);
    }
    @ApiOperation("查询产量")
    @PostMapping(value = "/businessQuantity", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<UserBusinessQuantityVO>> businessQuantityCount(@RequestBody @Valid BusinessQuantityQueryParams queryParams) {
        return businessApi.businessQuantityCount(queryParams);
    }

    @ApiOperation("变更加工单价")
    @PostMapping(value = "/changePrice", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> changePrice(@RequestBody @Valid BusinessPriceChangeParams changeParams) {
        return businessApi.changePrice(changeParams);
    }

    @PostMapping(value = "/getNumber", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> getNumber() {
        return businessApi.getNumber();
    }

    @PostMapping(value = "/getNumbers", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<String>> getNumbers(@ApiParam(value = "数量", required = true) @RequestParam(required = true)  Integer quantity) {
        return businessApi.getNumbers(quantity);
    }

    @PostMapping(value = "/getLength", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<Object> getLength(HttpServletRequest request) {
        return dataService.getDataNumber(IpAddressUtil.getClientIp(request));
    }

    @PostMapping(value = "/getBusinessGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BusinessGoodsVO>> getBusinessGoods(@RequestBody @Valid NumberQueryParams queryParams) {
        return businessApi.getBusinessGoods(queryParams);
    }

    @PostMapping(value = "/businessIns", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BusinessGoodsVO>> businessIns(@RequestBody @Valid BusinessQueryParams queryParams) {
        return businessApi.businessInGoods(queryParams.getBusinessCode());
    }

    @PostMapping(value = "/printBusiness", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<List<TemplateRecord>>> printBusiness(@ApiParam(value = "加工单编码", required = true) @RequestParam(required = true) String businessCode) {
        return businessApi.printBusinessData(businessCode);
    }

    @PostMapping(value = "/printData", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<TemplateRecord>> printData(@ApiParam(value = "入库记录单号", required = true) @RequestParam(required = true) String code) {
        return businessApi.printData(code);
    }

}
