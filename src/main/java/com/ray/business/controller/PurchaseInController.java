package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.business.api.PurchaseInApi;
import com.ray.business.api.PurchaseRecordApi;
import com.ray.business.table.params.purchase.PurchaseQueryParams;
import com.ray.business.table.params.purchase.in.PurchaseInCreateParams;
import com.ray.business.table.params.purchase.in.PurchaseInEditParams;
import com.ray.business.table.params.purchase.in.PurchaseInQueryParams;
import com.ray.business.table.params.purchase.in.PurchaseInRecordQueryParams;
import com.ray.business.table.vo.PurchaseInRecordVO;
import com.ray.business.table.vo.PurchaseInVO;
import com.ray.business.table.vo.PurchaseGoodsVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-采购入库订单管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/purchase/in")
@Api("业务服务-采购入库订单")
@Validated
public class PurchaseInController {

    @Autowired
    private PurchaseInApi purchaseInApi;
    @Autowired
    private PurchaseRecordApi purchaseRecordApi;


    @ApiOperation("查询采购入库订单列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<PurchaseInVO>> page(@RequestBody @Valid CommonPage<PurchaseInQueryParams, Page<PurchaseInVO>> queryParams) {
        return purchaseInApi.pagePurchaseIns(queryParams);
    }

    @ApiOperation("查询采购退货订单列表查询")
    @PostMapping(value = "/listGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<PurchaseGoodsVO>> listGoods(@RequestBody @Valid PurchaseQueryParams queryParams) {
        return purchaseRecordApi.listPurchasesGoods(queryParams);
    }

    @ApiOperation("采购入库订单新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid PurchaseInCreateParams createParams) {
        return purchaseInApi.createOrder(createParams);
    }

    @ApiOperation("采购入库订单编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid PurchaseInEditParams editParams) {
        return purchaseInApi.editOrder(editParams);
    }

    @ApiOperation("采购入库订单删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "采购入库订单编码", required = true) @RequestParam(required = true) String inCode) {
        return purchaseInApi.deleteOrder(inCode);
    }

    @ApiOperation("采购入库订单详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<PurchaseInVO> view(@ApiParam(value = "采购入库订单编码", required = true) @RequestParam(required = true) String inCode) {
        return purchaseInApi.viewOrder(inCode);
    }

    @ApiOperation("采购入库订单通过")
    @PostMapping(value = "/pass", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> pass(@ApiParam(value = "采购入库订单编码", required = true) @RequestParam(required = true) String inCode) {
        return purchaseInApi.passOrder(inCode);
    }


    @ApiOperation("采购入库订单取消")
    @PostMapping(value = "/cancel", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> cancel(@ApiParam(value = "采购入库订单编码", required = true) @RequestParam(required = true) String inCode) {
        return purchaseInApi.cancelOrder(inCode);
    }


    @ApiOperation("查询入库的记录")
    @PostMapping(value = "/pageRecord", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<PurchaseInRecordVO>> pageRecord(@RequestBody @Valid CommonPage<PurchaseInRecordQueryParams, Page<PurchaseInRecordVO>> queryParams) {
        return purchaseInApi.pagePurchaseInRecords(queryParams);
    }


}
