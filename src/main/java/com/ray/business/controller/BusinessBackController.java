package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.table.vo.customer.CustomerVO;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.api.BusinessApi;
import com.ray.business.api.BusinessBackApi;
import com.ray.business.table.params.business.BusinessCreateParams;
import com.ray.business.table.params.business.BusinessEditParams;
import com.ray.business.table.params.business.BusinessQueryParams;
import com.ray.business.table.params.business.back.BusinessBackCreateParams;
import com.ray.business.table.params.business.back.BusinessBackQueryParams;
import com.ray.business.table.vo.BusinessBackVO;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.business.table.vo.BusinessVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-退料单管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/business/back")
@Api("业务服务-退料单")
@Validated
public class BusinessBackController {

    @Autowired
    private BusinessBackApi businessBackApi;

    @ApiOperation("查询退料单列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<BusinessBackVO>> page(@RequestBody @Valid CommonPage<BusinessBackQueryParams, Page<BusinessBackVO>> queryParams) {
        return businessBackApi.pageBusinessBacks(queryParams);
    }


    @ApiOperation("出库商品列表查询-退回")
    @PostMapping(value = "/listBackInGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BusinessGoodsVO>> listBackInGoods(@RequestBody BusinessBackQueryParams queryParams) {
        return businessBackApi.listBackOutGoods(queryParams);
    }


    @ApiOperation("退料单出库新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid BusinessBackCreateParams createParams) {
        return businessBackApi.createBusinessBack(createParams);
    }


    @ApiOperation("退料单删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "退料编码", required = true) @RequestParam(required = true) String backCode) {
        return businessBackApi.deleteBusinessBack(backCode);
    }

    @ApiOperation("退料单详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<BusinessBackVO> view(@ApiParam(value = "退料编码", required = true) @RequestParam(required = true) String backCode) {
        return businessBackApi.viewBusinessBack(backCode);
    }

    @ApiOperation("退料单通过")
    @PostMapping(value = "/pass", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> pass(@ApiParam(value = "退料单编码", required = true) @RequestParam(required = true) String backCode) {
        return businessBackApi.passBusinessBack(backCode);
    }


    @ApiOperation("取消退料单")
    @PostMapping(value = "/cancel", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> cancel(@ApiParam(value = "退料单编码", required = true) @RequestParam(required = true) String backCode) {
        return businessBackApi.cancelBusinessBack(backCode);
    }
}
