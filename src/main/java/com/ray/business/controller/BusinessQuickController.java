package com.ray.business.controller;


import com.ray.business.api.BusinessQuickApi;
import com.ray.business.api.BusinessQuickInApi;
import com.ray.business.table.params.business.NumberQueryParams;
import com.ray.business.table.params.business.quick.*;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.business.table.vo.BusinessQuickVO;
import com.ray.business.table.vo.QuickSearchGoodsVO;
import com.ray.template.TemplateRecord;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-加工单管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/business/quick")
@Api("业务服务-加工单")
@Validated
public class BusinessQuickController {

    @Autowired
    private BusinessQuickApi businessQuickApi;

    @Autowired
    private BusinessQuickInApi businessQuickInApi;


    @ApiOperation("入库客户列表")
    @PostMapping(value = "/listRecord", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BusinessGoodsVO>> listInRecord(@RequestBody @Valid BusinessQuickInQueryParams quickQueryParams) {
        return businessQuickInApi.listInRecord(quickQueryParams.getQuickCode());
    }


    @ApiOperation("快捷入库新增")
    @PostMapping(value = "/in/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid BusinessQuickInParams createParams) {
        return businessQuickInApi.saveQuickIn(createParams);
    }

    @ApiOperation("快捷入库记录删除")
    @PostMapping(value = "/in/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> inDelete(@ApiParam(value = "快捷入库单记录编号", required = true) @RequestParam(required = true) String code) {
        return businessQuickInApi.deleteQuickIn(code);
    }

    @ApiOperation("快捷入库记录删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "快捷入库单", required = true) @RequestParam(required = true) String quickCode) {
        return businessQuickApi.deleteBusinessQuick(quickCode);
    }


    @ApiOperation("快捷入库详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<BusinessQuickVO> view(@RequestBody @Valid BusinessQuickQueryParams quickQueryParams) {
        return businessQuickApi.viewBusinessQuick(quickQueryParams);
    }

    @ApiOperation("提交快捷入库")
    @PostMapping(value = "/submit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> submit(@RequestBody @Valid BusinessQuickSubmitParams submitParams) {
        return businessQuickApi.submitBusinessQuick(submitParams);
    }


    @ApiOperation("商品信息")
    @PostMapping(value = "/viewGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<BusinessGoodsVO> viewGoods(@RequestBody @Valid BusinessQuickGoodsQueryParams quickQueryParams) {
        return businessQuickApi.viewGoods(quickQueryParams);
    }


    @ApiOperation("打印信息")
    @PostMapping(value = "/printData", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<TemplateRecord>> printData(@ApiParam(value = "快捷入库单记录编号", required = true) @RequestParam(required = true) String code) {
        return businessQuickInApi.printData(code);
    }

    @ApiOperation("快速检索")
    @PostMapping(value = "/search", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<QuickSearchGoodsVO> search(@RequestBody @Valid NumberQueryParams quickQueryParams) {
        return businessQuickApi.search(quickQueryParams);
    }

}
