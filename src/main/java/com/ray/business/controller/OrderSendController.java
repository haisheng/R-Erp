package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.api.OrderSendApi;
import com.ray.business.service.compose.SendDataExportService;
import com.ray.business.table.params.send.OrderSendCreateParams;
import com.ray.business.table.params.send.OrderSendEditParams;
import com.ray.business.table.params.send.OrderSendQueryParams;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.business.table.vo.OrderSendVO;
import com.ray.business.table.vo.SendGoodsExportVO;
import com.ray.export.request.ExportQueryRequest;
import com.ray.export.support.exports.DefaultAsyncVerification;
import com.ray.export.support.exports.DefaultPageVerification;
import com.ray.export.support.result.DefaultResult;
import com.ray.export.template.ExportTemplate;
import com.ray.template.TemplateRecord;
import com.ray.wms.table.vo.order.GoodsVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-发货单管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/order/send")
@Api("业务服务-发货")
@Validated
public class OrderSendController {

    @Autowired
    private OrderSendApi orderSendApi;

    @ApiOperation("查询发货单列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<OrderSendVO>> page(@RequestBody @Valid CommonPage<OrderSendQueryParams, Page<OrderSendVO>> queryParams) {
        return orderSendApi.pageOrderSends(queryParams);
    }


    @ApiOperation("出库商品列表查询")
    @PostMapping(value = "/listOutGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BusinessGoodsVO>> listOutGoods(@RequestBody OrderSendQueryParams queryParams) {
        return orderSendApi.listOutGoods(queryParams);
    }

    @ApiOperation("查询规格列表查询")
    @PostMapping(value = "/listGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<GoodsVO>> listGoods(@RequestBody @Valid CommonPage<OrderSendQueryParams, Page<MaterialModelVO>> queryParams) {
        return orderSendApi.listStockGoods(queryParams);
    }

    @ApiOperation("发货单新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid OrderSendCreateParams createParams) {
        return orderSendApi.createOrder(createParams);
    }


    @ApiOperation("发货单编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid OrderSendEditParams editParams) {
        return orderSendApi.editOrder(editParams);
    }

    @ApiOperation("发货单删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "发货单编码", required = true) @RequestParam(required = true) String sendCode) {
        return orderSendApi.deleteOrder(sendCode);
    }

    @ApiOperation("发货单详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<OrderSendVO> view(@ApiParam(value = "发货单编码", required = true) @RequestParam(required = true) String sendCode) {
        return orderSendApi.viewOrder(sendCode);
    }

    @ApiOperation("发货单通过")
    @PostMapping(value = "/pass", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> pass(@ApiParam(value = "发货单编码", required = true) @RequestParam(required = true) String sendCode) {
        return orderSendApi.passOrder(sendCode);
    }


    @ApiOperation("取消发货单")
    @PostMapping(value = "/cancel", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> cancel(@ApiParam(value = "发货单编码", required = true) @RequestParam(required = true) String sendCode) {
        return orderSendApi.cancelOrder(sendCode);
    }


    @ApiOperation("打印信息")
    @PostMapping(value = "/printData", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<List<TemplateRecord>>> printData(@ApiParam(value = "发货单编码", required = true) @RequestParam(required = true) String sendCode) {
        return orderSendApi.printData(sendCode);
    }

    /**
     * 到处发货单记录
     * @param queryRequest
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/exportSendData", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ExportTemplate(templateCode = "到处发货单记录", dataClazz = SendGoodsExportVO.class, dataCount = SendDataExportService.class, pageVerification = DefaultPageVerification.class,
            asyncVerification = DefaultAsyncVerification.class, dataExport = SendDataExportService.class,
            result = DefaultResult.class, success = SendDataExportService.class, error = SendDataExportService.class)
    public void exportSendData(@RequestBody ExportQueryRequest queryRequest, HttpServletRequest request, HttpServletResponse response) {
    }
}
