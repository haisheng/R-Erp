package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.table.params.material.model.MaterialModelQueryParams;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.api.PurchaseApi;
import com.ray.business.table.params.purchase.PurchaseCreateParams;
import com.ray.business.table.params.purchase.PurchaseEditParams;
import com.ray.business.table.params.purchase.PurchaseGoodsPriceParams;
import com.ray.business.table.params.purchase.PurchaseQueryParams;
import com.ray.business.table.vo.PurchaseGoodsVO;
import com.ray.business.table.vo.PurchaseVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-采购订单管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/purchase")
@Api("业务服务-采购订单")
@Validated
public class PurchaseController {

    @Autowired
    private PurchaseApi purchaseApi;

    @ApiOperation("查询采购订单列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<PurchaseVO>> page(@RequestBody @Valid CommonPage<PurchaseQueryParams, Page<PurchaseVO>> queryParams) {
        return purchaseApi.pagePurchases(queryParams);
    }

    @ApiOperation("查询需要订单列表查询")
    @PostMapping(value = "/listInGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<MaterialModelVO>> listInGoods(@RequestBody @Valid CommonPage<MaterialModelQueryParams, Page<MaterialModelVO>> queryParams) {
        return purchaseApi.listInGoods(queryParams);
    }


    @ApiOperation("采购订单新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid PurchaseCreateParams createParams) {
        return purchaseApi.createOrder(createParams);
    }

    @ApiOperation("采购订单编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid PurchaseEditParams editParams) {
        return purchaseApi.editOrder(editParams);
    }

    @ApiOperation("采购订单删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "采购订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return purchaseApi.deleteOrder(orderNo);
    }

    @ApiOperation("采购订单详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<PurchaseVO> view(@ApiParam(value = "采购订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return purchaseApi.viewOrder(orderNo);
    }

    @ApiOperation("采购订单通过")
    @PostMapping(value = "/pass", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> pass(@ApiParam(value = "采购订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return purchaseApi.passOrder(orderNo);
    }


    @ApiOperation("采购订单取消")
    @PostMapping(value = "/cancel", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> cancel(@ApiParam(value = "采购订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return purchaseApi.cancelOrder(orderNo);
    }

    @ApiOperation("采购订单完成")
    @PostMapping(value = "/finish", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> finish(@ApiParam(value = "采购订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return purchaseApi.finishOrder(orderNo);
    }

    @ApiOperation("采购订单完成")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "采购订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return purchaseApi.openOrder(orderNo);
    }

    @ApiOperation("修改商品单价")
    @PostMapping(value = "/editPrice", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> editPrice(@RequestBody @Valid PurchaseGoodsPriceParams priceParams) {
        return purchaseApi.editPrice(priceParams);
    }


    @ApiOperation("采购商品列表")
    @PostMapping(value = "/goodsList", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<PurchaseGoodsVO>> goodsList(@RequestBody @Valid PurchaseQueryParams queryParams) {
        return purchaseApi.goodsList(queryParams.getOrderNo());
    }


}
