package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.table.vo.customer.CustomerPrintVO;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.api.OrderApi;
import com.ray.business.table.params.order.OrderCreateParams;
import com.ray.business.table.params.order.OrderEditParams;
import com.ray.business.table.params.order.OrderNoQueryParams;
import com.ray.business.table.params.order.OrderQueryParams;
import com.ray.business.table.params.order.goods.OrderGoodsQueryParams;
import com.ray.business.table.vo.OrderGoodsCountVO;
import com.ray.business.table.vo.OrderGoodsVO;
import com.ray.business.table.vo.OrderVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-订单管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/order")
@Api("业务服务-订单")
@Validated
public class OrderController {

    @Autowired
    private OrderApi orderApi;

    @ApiOperation("查询订单列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<OrderVO>> page(@RequestBody @Valid CommonPage<OrderQueryParams, Page<OrderVO>> queryParams) {
        return orderApi.pageOrders(queryParams);
    }


    @ApiOperation("订单新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid OrderCreateParams createParams) {
        return orderApi.createOrder(createParams);
    }

    @ApiOperation("订单编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid OrderEditParams editParams) {
        return orderApi.editOrder(editParams);
    }

    @ApiOperation("订单删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return orderApi.deleteOrder(orderNo);
    }

    @ApiOperation("订单取消")
    @PostMapping(value = "/cancel", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> cancel(@ApiParam(value = "订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return orderApi.cancelOrder(orderNo);
    }

    @ApiOperation("订单详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<OrderVO> view(@ApiParam(value = "订单编码", required = true) @RequestParam(required = true) String orderNo,
                                @ApiParam(value = "是否显示统计", required = false) @RequestParam(required = false) Boolean showTotal) {
        return orderApi.viewOrder(orderNo,showTotal);
    }

    @ApiOperation("订单通过")
    @PostMapping(value = "/pass", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> pass(@ApiParam(value = "订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return orderApi.passOrder(orderNo);
    }


    @ApiOperation("完成订单")
    @PostMapping(value = "/finish", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> finish(@ApiParam(value = "订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return orderApi.closeOrder(orderNo);
    }

    @ApiOperation("开启订单")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return orderApi.openOrder(orderNo);
    }

    @ApiOperation("客户模板新增")
    @PostMapping(value = "/template/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<CustomerPrintVO> templateView(@ApiParam(value = "订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return orderApi.viewCustomerPrint(orderNo);
    }

    @ApiOperation("订单统计")
    @PostMapping(value = "/countOrder", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<OrderGoodsCountVO>> countOrder(@RequestBody @Valid OrderNoQueryParams queryParams) {
        return orderApi.countOrder(queryParams.getOrderNo());
    }

    @ApiOperation("订单统计")
    @PostMapping(value = "/customerGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<OrderGoodsVO>> customerGoods(@RequestBody @Valid CommonPage<OrderGoodsQueryParams, Page<OrderGoodsVO>>  queryParams) {
        return orderApi.customerGoods(queryParams);
    }


}
