package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.business.api.OrderGoodsApi;
import com.ray.business.table.params.order.goods.OrderGoodsQueryParams;
import com.ray.business.table.vo.OrderGoodsVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 系统基础服务-订单商品管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/order/goods")
@Api("业务服务-订单商品")
@Validated
public class OrderGoodsController {

    @Autowired
    private OrderGoodsApi orderGoodsApi;

    @ApiOperation("查询订单商品列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<OrderGoodsVO>> page(@RequestBody @Valid CommonPage<OrderGoodsQueryParams, Page<OrderGoodsVO>> queryParams) {
        return orderGoodsApi.pageOrderGoodss(queryParams);
    }

    @ApiOperation("订单商品删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "订单商品编码", required = true) @RequestParam(required = true) String code) {
        return orderGoodsApi.deleteOrderGoods(code);
    }

    @ApiOperation("订单商品详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<OrderGoodsVO> view(@ApiParam(value = "订单商品编码", required = true) @RequestParam(required = true) String code) {
        return orderGoodsApi.viewOrderGoods(code);
    }

    @ApiOperation("订单商品详情")
    @PostMapping(value = "/orderGoodsView", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<OrderGoodsVO> view(@ApiParam(value = "订单编号", required = true) @RequestParam(required = true) String orderNo,
                                     @ApiParam(value = "商品编码", required = true) @RequestParam(required = true) String goodsCode) {
        return orderGoodsApi.viewOrderGoods(goodsCode,orderNo);
    }

    
    @ApiOperation("完成订单商品")
    @PostMapping(value = "/finish", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> finish(@ApiParam(value = "订单商品编码", required = true) @RequestParam(required = true) String code) {
        return orderGoodsApi.closeOrderGoods(code);
    }

    @ApiOperation("开启订单商品")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "订单商品编码", required = true) @RequestParam(required = true) String code) {
        return orderGoodsApi.openOrderGoods(code);
    }


}
