package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.business.api.SaleBackApi;
import com.ray.business.api.SaleOutApi;
import com.ray.business.api.SaleRecordApi;
import com.ray.business.table.params.sale.SaleQueryParams;
import com.ray.business.table.params.sale.back.SaleBackCreateParams;
import com.ray.business.table.params.sale.back.SaleBackEditParams;
import com.ray.business.table.params.sale.back.SaleBackQueryParams;
import com.ray.business.table.params.sale.out.SaleOutQueryParams;
import com.ray.business.table.vo.SaleBackVO;
import com.ray.business.table.vo.SaleGoodsVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import javax.validation.Valid;

/**
 * <p>
 * 系统基础服务-销售退货订单管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/sale/back")
@Api("业务服务-销售退货订单")
@Validated
public class SaleBackController {

    @Autowired
    private SaleBackApi saleBackApi;
    @Autowired
    private SaleOutApi saleOurApi;

    @ApiOperation("查询销售退货订单列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<SaleBackVO>> page(@RequestBody @Valid CommonPage<SaleBackQueryParams, Page<SaleBackVO>> queryParams) {
        return saleBackApi.pageSaleBacks(queryParams);
    }

    @ApiOperation("查询销售退货商品列表查询")
    @PostMapping(value = "/listGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<SaleGoodsVO>> listGoods(@RequestBody @Valid SaleOutQueryParams queryParams) {
        return saleOurApi.listGoods(queryParams);
    }

    @ApiOperation("销售退货订单新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid SaleBackCreateParams createParams) {
        return saleBackApi.createOrder(createParams);
    }

    @ApiOperation("销售退货订单编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid SaleBackEditParams editParams) {
        return saleBackApi.editOrder(editParams);
    }

    @ApiOperation("销售退货订单删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "销售退货订单编码", required = true) @RequestParam(required = true) String backCode) {
        return saleBackApi.deleteOrder(backCode);
    }

    @ApiOperation("销售退货订单详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<SaleBackVO> view(@ApiParam(value = "销售退货订单编码", required = true) @RequestParam(required = true) String backCode) {
        return saleBackApi.viewOrder(backCode);
    }

    @ApiOperation("销售退货订单通过")
    @PostMapping(value = "/pass", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> pass(@ApiParam(value = "销售退货订单编码", required = true) @RequestParam(required = true) String backCode) {
        return saleBackApi.passOrder(backCode);
    }


    @ApiOperation("销售退货订单取消")
    @PostMapping(value = "/cancel", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> cancel(@ApiParam(value = "销售退货订单编码", required = true) @RequestParam(required = true) String backCode) {
        return saleBackApi.cancelOrder(backCode);
    }


}
