package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.business.api.ErrorApi;
import com.ray.business.table.params.error.ErrorTypeCreateParams;
import com.ray.business.table.params.error.ErrorTypeEditParams;
import com.ray.business.table.params.error.ErrorTypeQueryParams;
import com.ray.business.table.vo.ErrorTypeVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-异常管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/error")
@Api("生产服务-错误配置")
@Validated
public class ErrorController {

    @Autowired
    private ErrorApi errorApi;

    @ApiOperation("查询异常列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<ErrorTypeVO>> page(@RequestBody @Valid CommonPage<ErrorTypeQueryParams, Page<ErrorTypeVO>> queryParams) {
        return errorApi.pageErrorTypes(queryParams);
    }

    @ApiOperation("查询异常列表")
    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<ErrorTypeVO>> list(@RequestBody @Valid ErrorTypeQueryParams queryParams) {
        return errorApi.queryErrorTypes(queryParams);
    }


    @ApiOperation("异常新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid ErrorTypeCreateParams createParams) {
        return errorApi.createErrorType(createParams);
    }

    @ApiOperation("异常编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid ErrorTypeEditParams editParams) {
        return errorApi.editErrorType(editParams);
    }

    @ApiOperation("异常删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "异常编码", required = true) @RequestParam(required = true) String errorCode) {
        return errorApi.deleteErrorType(errorCode);
    }

    @ApiOperation("异常详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<ErrorTypeVO> view(@ApiParam(value = "异常编码", required = true) @RequestParam(required = true) String errorCode) {
        return errorApi.viewErrorType(errorCode);
    }

    @ApiOperation("异常开启")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "异常编码", required = true) @RequestParam(required = true) String errorCode) {
        return errorApi.openErrorType(errorCode);
    }


    @ApiOperation("异常关闭")
    @PostMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "异常编码", required = true) @RequestParam(required = true) String errorCode) {
        return errorApi.closeErrorType(errorCode);
    }


}
