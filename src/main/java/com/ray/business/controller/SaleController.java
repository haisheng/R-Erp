package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.business.api.SaleApi;
import com.ray.business.table.params.sale.SaleCreateParams;
import com.ray.business.table.params.sale.SaleEditParams;
import com.ray.business.table.params.sale.SaleQueryParams;
import com.ray.business.table.vo.SaleVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 系统基础服务-销售订单管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/sale")
@Api("业务服务-销售订单")
@Validated
public class SaleController {

    @Autowired
    private SaleApi saleApi;

    @ApiOperation("查询销售订单列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<SaleVO>> page(@RequestBody @Valid CommonPage<SaleQueryParams, Page<SaleVO>> queryParams) {
        return saleApi.pageSales(queryParams);
    }


    @ApiOperation("销售订单新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid SaleCreateParams createParams) {
        return saleApi.createOrder(createParams);
    }

    @ApiOperation("销售订单编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid SaleEditParams editParams) {
        return saleApi.editOrder(editParams);
    }

    @ApiOperation("销售订单删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "销售订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return saleApi.deleteOrder(orderNo);
    }

    @ApiOperation("销售订单详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<SaleVO> view(@ApiParam(value = "销售订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return saleApi.viewOrder(orderNo);
    }

    @ApiOperation("销售订单通过")
    @PostMapping(value = "/pass", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> pass(@ApiParam(value = "销售订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return saleApi.passOrder(orderNo);
    }

    @ApiOperation("销售订单完成")
    @PostMapping(value = "/finish", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> finish(@ApiParam(value = "销售订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return saleApi.finishOrder(orderNo);
    }

    @ApiOperation("销售订单完成")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "销售订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return saleApi.openOrder(orderNo);
    }



    @ApiOperation("销售订单取消")
    @PostMapping(value = "/cancel", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> cancel(@ApiParam(value = "销售订单编码", required = true) @RequestParam(required = true) String orderNo) {
        return saleApi.cancelOrder(orderNo);
    }


}
