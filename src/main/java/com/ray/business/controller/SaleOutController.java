package com.ray.business.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.business.api.SaleOutApi;
import com.ray.business.table.params.sale.out.SaleOutCreateParams;
import com.ray.business.table.params.sale.out.SaleOutEditParams;
import com.ray.business.table.params.sale.out.SaleOutQueryParams;
import com.ray.business.table.vo.SaleOutVO;
import com.ray.wms.table.vo.order.GoodsVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-销售出库订单管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/prod/sale/out")
@Api("业务服务-销售出库订单")
@Validated
public class SaleOutController {

    @Autowired
    private SaleOutApi saleOutApi;


    @ApiOperation("查询销售出库订单列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<SaleOutVO>> page(@RequestBody @Valid CommonPage<SaleOutQueryParams, Page<SaleOutVO>> queryParams) {
        return saleOutApi.pageSaleOuts(queryParams);
    }

    @ApiOperation("查询库存详情列表查询")
    @PostMapping(value = "/listGoods", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<GoodsVO>> listGoods(@RequestBody @Valid SaleOutQueryParams queryParams) {
        return saleOutApi.listOutGoods(queryParams);
    }
    

    @ApiOperation("销售出库订单新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid SaleOutCreateParams createParams) {
        return saleOutApi.createOrder(createParams);
    }

    @ApiOperation("销售出库订单编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid SaleOutEditParams editParams) {
        return saleOutApi.editOrder(editParams);
    }

    @ApiOperation("销售出库订单删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "销售出库订单编码", required = true) @RequestParam(required = true) String outCode) {
        return saleOutApi.deleteOrder(outCode);
    }

    @ApiOperation("销售出库订单详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<SaleOutVO> view(@ApiParam(value = "销售出库订单编码", required = true) @RequestParam(required = true) String outCode) {
        return saleOutApi.viewOrder(outCode);
    }

    @ApiOperation("销售出库订单通过")
    @PostMapping(value = "/pass", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> pass(@ApiParam(value = "销售出库订单编码", required = true) @RequestParam(required = true) String outCode) {
        return saleOutApi.passOrder(outCode);
    }


    @ApiOperation("销售出库订单取消")
    @PostMapping(value = "/cancel", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> cancel(@ApiParam(value = "销售出库订单编码", required = true) @RequestParam(required = true) String outCode) {
        return saleOutApi.cancelOrder(outCode);
    }


}
