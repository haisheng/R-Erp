package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdPurchaseIn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 采购订单-入库 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-27
 */
public interface ProdPurchaseInMapper extends BaseMapper<ProdPurchaseIn> {

}
