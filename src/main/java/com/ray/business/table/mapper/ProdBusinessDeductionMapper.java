package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdBusinessDeduction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 加工单号-扣款 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface ProdBusinessDeductionMapper extends BaseMapper<ProdBusinessDeduction> {

}
