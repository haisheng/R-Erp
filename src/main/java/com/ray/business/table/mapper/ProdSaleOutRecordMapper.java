package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdSaleOutRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 销售退回记录明细 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-27
 */
public interface ProdSaleOutRecordMapper extends BaseMapper<ProdSaleOutRecord> {

}
