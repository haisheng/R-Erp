package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdSaleOut;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 销售订单-退货 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-27
 */
public interface ProdSaleOutMapper extends BaseMapper<ProdSaleOut> {

}
