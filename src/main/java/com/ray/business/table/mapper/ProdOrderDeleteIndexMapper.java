package com.ray.business.table.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ray.business.table.entity.ProdOrderDeleteIndex;
import com.ray.business.table.entity.ProdOrderIndex;

/**
 * <p>
 * 生产订单 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-07-25
 */
public interface ProdOrderDeleteIndexMapper extends BaseMapper<ProdOrderDeleteIndex> {

}
