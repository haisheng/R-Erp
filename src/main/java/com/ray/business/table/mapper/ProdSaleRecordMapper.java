package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdSaleRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 销售记录明细 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface ProdSaleRecordMapper extends BaseMapper<ProdSaleRecord> {

}
