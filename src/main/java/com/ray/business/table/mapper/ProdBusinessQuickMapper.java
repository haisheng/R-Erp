package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdBusinessQuick;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 加工单号-快捷入库 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-07-05
 */
public interface ProdBusinessQuickMapper extends BaseMapper<ProdBusinessQuick> {

}
