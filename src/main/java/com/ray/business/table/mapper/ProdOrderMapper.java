package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 生产订单 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface ProdOrderMapper extends BaseMapper<ProdOrder> {

}
