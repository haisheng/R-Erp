package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdBusinessQuickIn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 加工单快捷入库明细 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-07-05
 */
public interface ProdBusinessQuickInMapper extends BaseMapper<ProdBusinessQuickIn> {

}
