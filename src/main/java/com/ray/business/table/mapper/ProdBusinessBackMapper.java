package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdBusinessBack;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 加工退料单 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-22
 */
public interface ProdBusinessBackMapper extends BaseMapper<ProdBusinessBack> {

}
