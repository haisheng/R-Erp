package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdSaleBackRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 销售退回记录明细 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface ProdSaleBackRecordMapper extends BaseMapper<ProdSaleBackRecord> {

}
