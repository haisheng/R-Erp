package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdPurchaseBack;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 采购订单-退货 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface ProdPurchaseBackMapper extends BaseMapper<ProdPurchaseBack> {

}
