package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdOrderIndex;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 生产订单 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-07-25
 */
public interface ProdOrderIndexMapper extends BaseMapper<ProdOrderIndex> {

}
