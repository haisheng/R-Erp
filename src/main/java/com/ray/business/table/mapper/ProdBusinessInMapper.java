package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdBusinessIn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 加工单物料明细 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface ProdBusinessInMapper extends BaseMapper<ProdBusinessIn> {

    List<Integer> listIndexSort(ProdBusinessIn entity);
}
