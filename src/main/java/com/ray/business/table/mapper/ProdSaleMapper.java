package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdSale;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 销售订单 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface ProdSaleMapper extends BaseMapper<ProdSale> {

}
