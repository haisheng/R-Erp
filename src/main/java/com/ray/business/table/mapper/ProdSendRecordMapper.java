package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdSendRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 加工单物料明细 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface ProdSendRecordMapper extends BaseMapper<ProdSendRecord> {

}
