package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdPurchase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 采购订单 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface ProdPurchaseMapper extends BaseMapper<ProdPurchase> {

}
