package com.ray.business.table.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ray.business.table.entity.ProdPurchaseDeduction;

/**
 * <p>
 * 加工单号-扣款 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface ProdPurchaseDeductionMapper extends BaseMapper<ProdPurchaseDeduction> {

}
