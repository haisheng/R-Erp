package com.ray.business.table.mapper;

import com.ray.business.table.dto.BusinessInCountQuantityDTO;
import com.ray.business.table.dto.BusinessQuantityDTO;
import com.ray.business.table.dto.GroupBusinessDTO;
import com.ray.business.table.dto.UserBusinessQuantityDTO;
import com.ray.business.table.entity.ProdBusiness;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ray.common.CountParams;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 加工单号 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface ProdBusinessMapper extends BaseMapper<ProdBusiness> {

    /**
     * 分组统计
     *
     * @param countParams
     * @return
     */
    List<GroupBusinessDTO> countGroupBusiness(CountParams countParams);


    /**
     * 分组统计order
     *
     * @param countParams
     * @return
     */
    List<GroupBusinessDTO> orderGroupBusiness(CountParams countParams);
    /**
     * 分组统计总和
     *
     * @param countParams
     * @return
     */
    List<BusinessQuantityDTO> businessQuantity(CountParams countParams);

    /**
     * 产量统计
     * @param userBusinessQuantityDTO
     * @return
     */
    List<BusinessQuantityDTO> quantityCount(UserBusinessQuantityDTO userBusinessQuantityDTO);

    /**
     * 工序产量
     * @param orderNo
     * @param companyCode
     * @return
     */
    List<BusinessInCountQuantityDTO> countInQuantity(@Param("orderNo") String orderNo, @Param("goodsCode") String goodsCode, @Param("companyCode") String companyCode);

    /**
     * 工序产量
     * @param orderNo
     * @param companyCode
     * @return
     */
    List<BusinessInCountQuantityDTO> countAllQuantity(@Param("orderNo") String orderNo, @Param("goodsCode") String goodsCode, @Param("companyCode") String companyCode);

}
