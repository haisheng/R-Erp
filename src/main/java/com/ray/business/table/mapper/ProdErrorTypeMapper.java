package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdErrorType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 验布错误类型 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-07-17
 */
public interface ProdErrorTypeMapper extends BaseMapper<ProdErrorType> {

}
