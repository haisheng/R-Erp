package com.ray.business.table.mapper;

import com.ray.business.table.entity.ProdOrderGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单-步骤 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface ProdOrderGoodsMapper extends BaseMapper<ProdOrderGoods> {

}
