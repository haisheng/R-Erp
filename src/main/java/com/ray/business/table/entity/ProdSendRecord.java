package com.ray.business.table.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 加工单物料明细
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("prod_send_record")
public class ProdSendRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 唯一吗
     */
    private String code;
    /**
     * 加工单号
     */
    private String sendCode;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 商品编码
     */
    private String goodsCode;

    /**
     * 数量
     */
    private BigDecimal quantity;

    /**
     * 卷数
     */
    private Integer total;

    /**
     * 扣减数量
     */
    private BigDecimal deleteQuantity;

    /**
     * 实际数量
     */
    private BigDecimal realQuantity;

    /**
     * 重量
     */
    private BigDecimal goodsWeight;

    /**
     * 净重
     */
    private BigDecimal goodsNetWeight;
    /**
     * 缸号
     */
    private String gangNo;

    /**
     * 批次号
     */
    private String batchNo;

    /**
     * 单位
     */
    private String unit;

    /**
     * 序列号
     */
    private String serialNumber;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 状态  1:启用 0:禁用
     */
    private Integer status;

    /**
     * 关联单号
     */
    private String inCode;

    /**
     * 是否删除 1:删除 0:未删除
     */
    @TableLogic
    private Integer isDeleted;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建人编码
     */
    private String createUserCode;

    /**
     * 创建人名称
     */
    private String createUserName;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新用户编码
     */
    private String updateUserCode;

    /**
     * 更新用户名称
     */
    private String updateUserName;

    /**
     * 租户编码
     */
    private String tenantCode;

    /**
     * 用户编码
     */
    private String ownerCode;

    /**
     * 组织编码
     */
    private String organizationCode;

    /**
     * 排序
     */
    private Integer indexSort;

    /**
     * 版本号
     */
    @Version
    private Integer updateVersion;


}
