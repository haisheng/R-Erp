package com.ray.business.table.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 采购订单
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("prod_purchase")
public class ProdPurchase implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 客户订单编号
     */
    private String businessOrderNo;

    /**
     * 客户
     */
    private String customerCode;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 数量
     */
    private BigDecimal quantity;

    /**
     * 完成数量
     */
    private BigDecimal finishQuantity;

    /**
     * 卷数
     */
    private Integer total;

    /**
     * 总金额
     */
    private BigDecimal totalAmount;

    /**
     * 发货时间
     */
    private LocalDateTime sendTime;

    /**
     * 仓库编码
     */
    private String warehouseCode;

    /**
     * 状态  1:启用 0:禁用
     */
    private Integer status;

    /**
     * 对账状态
     */
    private Integer billStatus;

    /**
     * 对账单号
     */
    private String billNo;

    /**
     * 付款方式
     */
    private String payType;

    /**
     * 运输方式
     */
    private String transport;

    /**
     * 运输费用
     */
    private BigDecimal transportFee;


    /**
     * 币种
     */
    private String currency;


    /**
     * 是否含税
     */
    private Integer tax;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否删除 1:删除 0:未删除
     */
    @TableLogic
    private Integer isDeleted;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建人编码
     */
    private String createUserCode;

    /**
     * 创建人名称
     */
    private String createUserName;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新用户编码
     */
    private String updateUserCode;

    /**
     * 更新用户名称
     */
    private String updateUserName;

    /**
     * 租户编码
     */
    private String tenantCode;

    /**
     * 用户编码
     */
    private String ownerCode;

    /**
     * 组织编码
     */
    private String organizationCode;

    /**
     * 排序
     */
    private Integer indexSort;

    /**
     * 版本号
     */
    @Version
    private Integer updateVersion;


}
