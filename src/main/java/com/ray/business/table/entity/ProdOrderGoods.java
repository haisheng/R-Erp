package com.ray.business.table.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableLogic;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单-步骤
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("prod_order_goods")
public class ProdOrderGoods implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    private String code;

    /**
     * 订单编号
     */
    private String orderNo;


    /**
     * 客户编号
     */
    private String customerCode;

    /**
     * 规格编号
     */
    private String goodsCode;

    /**
     * 客户型号名称
     */

    private String customerModelName;

    /**
     * 客户产品属性
     */
    private String customerModelProp;

    /**
     * 生产数量
     */
    private BigDecimal quantity;
    /**
     * 单价
     */
    private BigDecimal price;
    /**
     * 备注
     */
    private String remark;

    /**
     * 商品状态
     */
    private String goodsStatus;

    /**
     * 状态  1:启用 0:禁用
     */
    private Integer status;

    /**
     * 是否删除 1:删除 0:未删除
     */
    @TableLogic
    private Integer isDeleted;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 完成数量
     */
    private BigDecimal finishQuantity;

    /**
     * 完成卷数
     */
    private Integer total;


    /**
     * 发货数量
     */
    private BigDecimal sendQuantity;

    /**
     * 创建人编码
     */
    private String createUserCode;

    /**
     * 创建人名称
     */
    private String createUserName;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新用户编码
     */
    private String updateUserCode;

    /**
     * 更新用户名称
     */
    private String updateUserName;

    /**
     * 租户编码
     */
    private String tenantCode;

    /**
     * 用户编码
     */
    private String ownerCode;

    /**
     * 组织编码
     */
    private String organizationCode;

    /**
     * 排序
     */
    private Integer indexSort;

    /**
     * 版本号
     */
    @Version
    private Integer updateVersion;


}
