package com.ray.business.table.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 加工单物料明细
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("prod_business_in")
public class ProdBusinessIn implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 唯一单号
     */
    private String code;

    /**
     * 业务订单号
     */
    private String orderNo;

    /**
     * 加工单号
     */
    private String businessCode;

    /**
     * 商品编码
     */
    private String goodsCode;

    /**
     * 卷数
     */
    private Integer total;

    /**
     * 数量
     */
    private BigDecimal quantity;

    /**
     * 扣减数量
     */
    private BigDecimal deleteQuantity;

    /**
     * 实际数量
     */
    private BigDecimal realQuantity;

    /**
     * 重量
     */
    private BigDecimal goodsWeight;

    /**
     * 净重
     */
    private BigDecimal goodsNetWeight;
    /**
     * 缸号
     */
    private String gangNo;

    /**
     * 批次号
     */
    private String batchNo;
    /**
     * 单位
     */
    private String unit;

    /**
     * 序列号
     */
    private String serialNumber;

    /**
     * 对应出库数量
     */
    private String outQuantity;

    /**
     * 单价
     */
    private BigDecimal price;


    /**
     * 关联单号
     */
    private String outCode;

    /***
     * 描述
     */
    private String remark;

    /**
     * 状态  1:启用 0:禁用
     */
    private Integer status;

    /**
     * 是否删除 1:删除 0:未删除
     */
    @TableLogic
    private Integer isDeleted;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建人编码
     */
    private String createUserCode;

    /**
     * 创建人名称
     */
    private String createUserName;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新用户编码
     */
    private String updateUserCode;

    /**
     * 更新用户名称
     */
    private String updateUserName;

    /**
     * 租户编码
     */
    private String tenantCode;

    /**
     * 用户编码
     */
    private String ownerCode;

    /**
     * 组织编码
     */
    private String organizationCode;

    /**
     * 排序
     */
    private Integer indexSort;

    /**
     * 版本号
     */
    @Version
    private Integer updateVersion;


}
