package com.ray.business.table.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 加工单号
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("prod_business")
public class ProdBusiness implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 业务单号
     */
    private String businessCode;

    /**
     * 业务类型
     */
    private String businessType;

    /**
     * 客户编号
     */
    private String customerCode;

    /**
     * 步骤编码
     */
    private String stepCode;

    /**
     * 仓库
     */
    private String warehouseCode;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 数量
     */
    private BigDecimal quantity;

    /**
     * 商品编码
     */
    private String goodsCode;
    /**
     * 雇员
     */
    private String  employee;

    /**
     * 雇员a
     */
    private String  employeea;

    /**
     * 外部单号
     */
    private String  outBusinessCode;
    /***
     * 数量
     */
    private Integer total;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 对账状态
     */
    private Integer billStatus;

    /**
     * 对账单号
     */
    private String billNo;

    /**
     * 状态  1:启用 0:禁用
     */
    private Integer status;

    /**
     * 是否删除 1:删除 0:未删除
     */
    @TableLogic
    private Integer isDeleted;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建人编码
     */
    private String createUserCode;

    /**
     * 创建人名称
     */
    private String createUserName;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新用户编码
     */
    private String updateUserCode;

    /**
     * 更新用户名称
     */
    private String updateUserName;

    /**
     * 租户编码
     */
    private String tenantCode;

    /**
     * 用户编码
     */
    private String ownerCode;

    /**
     * 组织编码
     */
    private String organizationCode;

    /**
     * 排序
     */
    private Integer indexSort;

    /**
     * 版本号
     */
    @Version
    private Integer updateVersion;


}
