package com.ray.business.table.dto;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 分组
 * @Class: GroupBusinessDTO
 * @Package com.ray.business.table.dto
 * @date 2020/6/15 9:48
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class GroupBusinessDTO {

    private String stepCode;

    private String businessType;

    private String customerCode;

    private Integer quantity;

    private String orderStatus;
}
