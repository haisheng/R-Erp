package com.ray.business.table.dto;

import com.ray.common.dto.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 订单
 * @Class: OrderQueryDTO
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class SaleBackQueryDTO extends BaseDTO {


    /**
     * 退货单号
     */
    private String backCodeLike;

    /**
     * 退货单号
     */
    private String backCode;


    /**
     * 订单编码
     */
    private String orderNo;

    /**
     * 客户单号
     */
    private String customerCode;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 对账状态
     */
    private Integer billStatus;

    /**
     * 对账单号
     */
    private String billNo;
}
