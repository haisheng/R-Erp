package com.ray.business.table.dto;

import com.ray.business.table.entity.ProdOrderStep;
import com.ray.common.dto.BaseDTO;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 业务查询
 * @Class: BusinessQueryDTO
 * @Package com.ray.business.table.dto
 * @date 2020/6/11 10:34
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessQueryDTO extends BaseDTO {
    /**
     * 步骤编码
     **/
    private String stepCode;
    /**
     * 订单编号
     **/
    private String orderNo;

    /**
     * 业务单号
     **/
    private String businessCode;

    /**
     * 业务单号
     **/
    private String businessCodeLike;
    /**
     * 规格名称查询
     **/
    private String modelNameLike;
    /**
     * 当前步骤
     **/
    private ProdOrderStep orderStep;
    /**
     * 加工单位
     **/
    private String customerCode;
    /**
     * 订单状态
     */
    private String orderStatus;
    /**
     * 类型
     */
    private String businessType;

    /**对账状态**/
    private Integer billStatus;

    /**
     * 对账单号
     */
    private String billNo;

    private String goodsCode;

    /**
     * 外部单号
     */
    private String  outBusinessCode;

}
