package com.ray.business.table.dto;

import com.ray.common.dto.BaseDTO;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 扣款基础
 * @Class: DeductionBase
 * @Package com.ray.business.table.params.deduction
 * @date 2020/6/12 15:00
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class DeductionQueryDTO extends BaseDTO {

    /**
     * 业务单号
     */
    private String businessCode;

    /**
     * 订单单号
     */
    private String orderNo;
    /**
     * 订单状态
     */
    private String orderStatus;

}
