package com.ray.business.table.dto;

import com.ray.common.dto.BaseDTO;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 业务查询
 * @Class: BusinessQueryParams
 * @Package com.ray.business.table.params.business
 * @date 2020/6/11 10:34
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class UserBusinessQuantityDTO extends BaseDTO {

  private String companyCode;


  private String userProp;

}
