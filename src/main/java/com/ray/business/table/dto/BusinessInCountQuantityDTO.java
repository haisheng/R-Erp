package com.ray.business.table.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 入库数量统计
 * @Class: BusinessInCountQuantityDTO
 * @Package com.ray.business.table.dto
 * @date 2020/7/9 14:22
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessInCountQuantityDTO {

    private String orderNo;

    private String stepCode;

    private String goodsCode;

    private BigDecimal quantity;


}
