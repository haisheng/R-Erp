package com.ray.business.table.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 业务单统计
 * @Class: BusinessCountVO
 * @Package com.ray.statistics.vo
 * @date 2020/6/15 9:59
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessQuantityDTO {

    private String stepCode;

    private String businessType;

    private String customerCode;

    private BigDecimal quantity;

    private String userCode;

}
