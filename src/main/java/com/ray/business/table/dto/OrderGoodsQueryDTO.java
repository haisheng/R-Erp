package com.ray.business.table.dto;

import com.ray.common.dto.BaseDTO;
import lombok.Data;

/**
 * @author bo shen
 * @Description:
 * @Class: OrderQueryDTO
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderGoodsQueryDTO extends BaseDTO{

    private String  customerCode;
}
