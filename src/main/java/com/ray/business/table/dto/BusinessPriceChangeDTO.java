package com.ray.business.table.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 单价修改
 * @Class: BusinessPriceChangeDTO
 * @Package com.ray.business.table.dto
 * @date 2020/7/11 15:16
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessPriceChangeDTO {

    private String businessCode;

    private BigDecimal price;

    private Integer updateVersion;
}
