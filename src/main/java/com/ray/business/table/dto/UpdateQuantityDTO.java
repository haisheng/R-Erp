package com.ray.business.table.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 更新数量
 * @Class: UpdateQuantityDTO
 * @Package com.ray.business.table.dto
 * @date 2020/6/28 9:41
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class UpdateQuantityDTO {
    /**
     * 唯一code
     */
    private String code;

    /**
     * 数量
     */
    private BigDecimal quantity;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 卷数
     */
    private Integer total;

    /**
     * 版本号
     */
    private Integer updateVersion;
}
