package com.ray.business.table.vo;

import lombok.Data;

/**
 * @author bo shen
 * @Description:
 * @Class: QuickSearchGoodsVO
 * @Package com.ray.business.table.vo
 * @date 2020/7/14 17:29
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class QuickSearchGoodsVO {
    /**订单*/
    private String orderNo;

    private String stepCode;

    private String customerCode;

    private String goodsCode;
}
