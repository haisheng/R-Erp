package com.ray.business.table.vo;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author bo shen
 * @Description: 订单步骤
 * @Class: OrderStepBase
 * @Package com.ray.business.table.params.order.step
 * @date 2020/6/5 9:24
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderStepVO extends BaseVO{


    /**
     * 编码
     */
    @ApiModelProperty(value = "编码", required = true)
    private String code;

    /**
     * 物料编号
     */
    @ApiModelProperty(value = "物料编号", required = true)
    private String materialCode;

    /**
     * 步骤编号
     */
    @ApiModelProperty(value = "步骤编号", required = true)
    private String stepCode;


    @ApiModelProperty(value = "步骤名称", required = true)
    private String stepName;
    /**
     * 入库仓库
     */
    @ApiModelProperty(value = "入库仓库", required = true)
    private String inWarehouseCode;
    /**
     * 入库仓库
     */
    @ApiModelProperty(value = "入库仓库名称", required = true)
    private String inWarehouseName;

    /**
     * 出库仓库
     */
    @ApiModelProperty(value = "出库仓库", required = true)
    private String outWarehouseCode;

    /**
     * 出库仓库
     */
    @ApiModelProperty(value = "出库仓库名称", required = true)
    private String outWarehouseName;
    /**
     * 顺序
     */
    @ApiModelProperty(value = "顺序", required = true)
    @NotNull(message = "顺序不能为空")
    private Integer indexSort;

    /**
     * 是否订单出入库
     */
    private Integer orderStatus;

    /**
     * 是否支持验布入库
     */
    private Integer quickStatus;

}
