package com.ray.business.table.vo;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 扣款基础
 * @Class: DeductionBase
 * @Package com.ray.business.table.params.deduction
 * @date 2020/6/12 15:00
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class DeductionVO extends BaseVO {

    /**
     * 扣款单号
     */
    @ApiModelProperty(value = "扣款单号", required = true)
    private String deductionCode;

    /**
     * 业务单号
     */
    @ApiModelProperty(value = "业务单号", required = true)
    private String businessCode;

    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号", required = true)
    private String orderNo;


    /**
     * 扣款金额
     */
    @ApiModelProperty(value = "扣款金额", required = true)
    private BigDecimal amount;


    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号", required = true)
    private String customerCode;


    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称", required = true)
    private String customerName;


    /**
     * 订单状态
     */
    @ApiModelProperty(value = "订单状态", required = true)
    private String orderStatus;

    /**
     * 描述
     */
    @ApiModelProperty(value = "描述", required = true)
    private String remark;
}
