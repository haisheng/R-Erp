package com.ray.business.table.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ray.base.table.vo.FileVO;
import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author bo shen
 * @Description: 销售订单
 * @Class: SaleVO
 * @Package com.ray.business.table.params.purchase
 * @date 2020/6/7 17:56
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class SaleVO extends BaseVO{

    /**
     * 订单编码
     */
    @ApiModelProperty(value = "订单编码", required = true)
    private String orderNo;
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号", required = true)
    private String customerCode;

    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称", required = true)
    private String customerName;


    /**
     * 仓库编号
     */
    @ApiModelProperty(value = "仓库编号", required = true)
    private String warehouseCode;

    /**
     * 仓库名称
     */
    @ApiModelProperty(value = "仓库名称", required = true)
    private String warehouseName;


    /**
     * 订单状态
     */
    @ApiModelProperty(value = "订单状态", required = true)
    private String orderStatus;

    /**
     * 数量
     */
    private BigDecimal quantity;

    /**
     * 卷数
     */
    private Integer total;

    /**
     * 总金额
     */
    private BigDecimal totalAmount;
    /**
     * 发货时间
     */
    @ApiModelProperty(value = "发货时间", required = true)
    @JSONField(format = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date sendTime;

    /**
     * 对账状态
     */
    @ApiModelProperty("对账状态")
    private Integer billStatus;

    /**
     * 对账单号
     */
    @ApiModelProperty("对账单号")
    private String billNo;


    /**
     * 付款方式
     */
    private String payType;

    /**
     * 运输方式
     */
    private String transport;

    /**
     * 运输费用
     */
    private BigDecimal transportFee;


    /**
     * 币种
     */
    private String currency;

    /**
     * 是否含税
     */
    private Integer tax;

    /**
     * 备注
     */
    private String remark;


    @ApiModelProperty(value = "商品列表", required = true)
    private List<SaleGoodsVO> goods;

    private List<FileVO> files;
}
