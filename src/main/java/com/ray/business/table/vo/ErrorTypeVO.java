package com.ray.business.table.vo;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 异常
 * @Class: StepBase
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class ErrorTypeVO extends BaseVO{

    /**
     * 异常编码
     */
    @ApiModelProperty(value = "异常编码", required = true)
    private String errorCode;
    /**
     * 异常名称
     */
    @ApiModelProperty(value = "异常名称", required = true)
    private String errorName;

    /**
     * 数量
     */
    private Integer number;

    /**
     * 是否需要长度
     */
    private Integer needLength;

    /**
     * 扣减长度
     */
    private BigDecimal deleteLength;
}
