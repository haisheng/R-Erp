package com.ray.business.table.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author bo shen
 * @Description: 订单
 * @Class: OrderBase
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderVO extends BaseVO{

    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号", required = true)
    private String orderNo;

    /**
     * 订单名称
     */
    @ApiModelProperty(value = "订单名称", required = true)
    private String orderName;

    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号", required = true)
    private String customerCode;

    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称", required = true)
    private String customerName;

    /**
     * 发货时间
     */
    @ApiModelProperty(value = "发货时间", required = false)
    @JSONField(format = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date sendTime;


    /**
     * 订单数量
     */
    @ApiModelProperty(value = "订单数量", required = true)
    private BigDecimal quantity;


    /**
     * 订单状态
     */
    @ApiModelProperty(value = "订单状态", required = true)
    private String orderStatus;

    /**
     * 付款方式
     */
    private String payType;

    /**
     * 运输方式
     */
    private String transport;

    /**
     * 运输费用
     */
    private BigDecimal transportFee;


    /**
     * 币种
     */
    private String currency;

    /**
     * 是否含税
     */
    private Integer tax;

    /**
     * 备注
     */
    private String remark;

    /**
     * 业务员编码
     */
    private String ownerCode;

    /**
     * 业务员名称
     */
    private String ownerName;

    /**
     * 总金额
     */
    private BigDecimal totalAmount;

    /**
     * 货物信息
     */
    @ApiModelProperty(value = "货物信息", required = false)
    private List<OrderGoodsVO> goods;

}
