package com.ray.business.table.vo;

import com.ray.base.table.vo.material.model.MaterialModelVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 产品基础
 * @Class: OrderGoodsParams
 * @Package com.ray.business.table.params.order.goods
 * @date 2020/6/5 16:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class SendGoodsVO extends MaterialModelVO {


    private String bizCode;
    /**
     * 规格编号
     */
    @ApiModelProperty(value = "规格编号", required = true)
    private String goodsCode;

    /**
     * 规格名称
     */
    @ApiModelProperty(value = "规格名称", required = true)
    private String goodsName;

    /**
     * 发货数量
     */
    @ApiModelProperty(value = "发货数量", required = true)
    private BigDecimal quantity;

    /**
     * 卷数
     */
    private Integer total;


    /**
     * 批次号
     */
    @ApiModelProperty(value = "批次号", required = true)
    private String batchNo;

    /**
     * 序列号
     */
    @ApiModelProperty(value = "序列号", required = true)
    private String serialNumber;

    /**
     * 净重
     */
    private BigDecimal goodsWeight;
    /**
     * 净重
     */
    private BigDecimal goodsNetWeight;
    /**
     * 缸号
     */
    private String gangNo;


    private String linkCode;

    /**
     * 价格
     */
    private BigDecimal price;


    private BigDecimal totalAmount;

    /**
     * 客户型号名称
     */

    private String customerModelName;

    /**
     * 客户产品属性
     */
    private String customerModelProp;

    public  String getKey(){
      return   String.format("%s|%s|%S",goodsCode,batchNo,serialNumber);
    }

}
