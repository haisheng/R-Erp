package com.ray.business.table.vo;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 步骤
 * @Class: StepBase
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class UserBusinessQuantityVO extends BaseVO{

    /**
     * 用户编码
     */
    private String userCode;
    /**
     * 用户名称
     */
    private String userName;

    /**
     * 数量
     */
    private BigDecimal quantity;
}
