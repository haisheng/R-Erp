package com.ray.business.table.vo;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author bo shen
 * @Description: 业务单号
 * @Class: BusinessVO
 * @Package com.ray.business.table.vo
 * @date 2020/6/11 10:36
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessQuickVO extends BaseVO{


    /**
     * 订单编号
     */
    @ApiModelProperty("订单编号")
    private String orderNo;

    /**
     * 快捷单号
     */
    @ApiModelProperty("快捷单号")
    private String quickCode;

    /**
     * 加工户编号
     */
    @ApiModelProperty("加工户编号")
    private String customerCode;

    /**
     * 加工户名称
     */
    @ApiModelProperty("加工户名称")
    private String customerName;

    /**
     * 步骤编码
     */
    @ApiModelProperty("步骤编码")
    private String stepCode;

    /**
     * 商品编码
     */
    @ApiModelProperty("商品编码")
    private String goodsCode;
    /**
     * 雇员
     */
    private String  employeeName;
    /**
     * 雇员
     */
    private String  employee;

    /**
     * 雇员a
     */
    private String  employeea;
    /**
     * 步骤名称
     */
    @ApiModelProperty("步骤名称")
    private String stepName;
    /**
     * 仓库
     */
    @ApiModelProperty("仓库")
    private String warehouseCode;


    /**
     * 单价
     */
    @ApiModelProperty("单价")
    private BigDecimal price;


    /**
     * 名称
     */
    @ApiModelProperty(value = "名称", required = true)
    private String goodsName;

    /**
     * 规格名称
     */
    @ApiModelProperty(value = "规格名称", required = true)
    private String modelName;



    /**
     * 规格属性
     */
    @ApiModelProperty(value = "规格属性", required = false)
    private String modelProp;

    /**
     * 商品信息
     */
    private List<BusinessGoodsVO> goods;
}
