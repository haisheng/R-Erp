package com.ray.business.table.vo;

import com.ray.base.table.vo.material.model.MaterialModelVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 订单步骤
 * @Class: OrderStepBase
 * @Package com.ray.business.table.params.order.step
 * @date 2020/6/5 9:24
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderGoodsVO extends MaterialModelVO{


    /**
     * 编码
     */
    @ApiModelProperty(value = "编码", required = true)
    private String code;

    /**
     * 单价
     */
    private BigDecimal price;
    /**
     * 完成数量
     */
    private BigDecimal finishQuantity;

    /**
     * 完成卷数
     */
    private Integer total;

    /**
     * 发货数量
     */
    private BigDecimal sendQuantity;

    /**
     * 商品状态
     */
    private String goodsStatus;

    /**
     * 客户型号名称
     */

    private String customerModelName;

    /**
     * 客户产品属性
     */
    private String customerModelProp;


    private String goodsName;


    private String  orderNo;

    /**
     * 备注
     */
    private String remark;

}
