package com.ray.business.table.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * @author bo shen
 * @Description:
 * @Class: OrderGoodsCountVO
 * @Package com.ray.business.table.vo
 * @date 2020/7/9 13:56
 * @company <p>快速开发</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderGoodsCountVO extends HashMap<String,Object>{

    private String goodsCode;


    private String goodsName;


    private String modelName;


    private String modelProp;


    private BigDecimal orderQuantity;


    private BigDecimal finishQuantity;

    /**
     * 发货数量
     */
    private BigDecimal sendQuantity;

    private String unit;

}
