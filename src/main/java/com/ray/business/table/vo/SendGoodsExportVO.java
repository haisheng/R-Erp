package com.ray.business.table.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 产品基础
 * @Class: OrderGoodsParams
 * @Package com.ray.business.table.params.order.goods
 * @date 2020/6/5 16:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class SendGoodsExportVO {


    /*** 卷数*/
    @ExcelProperty(value = "卷数", index = 11)
    private Integer total;
    /*** 批次号*/
    @ExcelProperty(value = "批次号", index = 10)
    private String batchNo;
    /*** 序列号 */
    @ExcelProperty(value = "序列号", index = 6)
    private String serialNumber;
    /*** 毛重 */
    @ExcelProperty(value = "毛重", index = 7)
    private BigDecimal goodsWeight;
    /*** 净重 */
    @ExcelProperty(value = "净重", index = 8)
    private BigDecimal goodsNetWeight;
    /*** 价格*/
    @ExcelProperty(value = "价格", index = 9)
    private BigDecimal price;
    /*** 客户型号名称*/
    @ExcelProperty(value = "客户型号", index = 4)
    private String customerModelName;
    /*** 客户产品属性*/
    @ExcelProperty(value = "客户色号", index = 5)
    private String customerModelProp;
    /*** 规格名称 */
    @ExcelProperty(value = "色号", index = 2)
    private String modelName;
    /*** 规格属性*/
    @ExcelProperty(value = "门幅", index = 3)
    private String modelProp;
    /*** 数量*/
    @ExcelProperty(value = "数量", index = 12)
    private BigDecimal quantity;
    /*** 物料名称*/
    @ExcelProperty(value = "物料名称", index = 1)
    private String materialName;
    /*** 单位*/
    @ExcelProperty(value = "单位", index = 13)
    private String unit;
    /*** 重量*/
    @ExcelProperty(value = "克重", index = 3)
    private String weight;


}
