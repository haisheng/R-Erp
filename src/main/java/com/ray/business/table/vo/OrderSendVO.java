package com.ray.business.table.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ray.business.table.params.send.SendGoodsParams;
import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author bo shen
 * @Description: 发货基础
 * @Class: OrderSendBase
 * @Package com.ray.business.table.params.send
 * @date 2020/6/12 11:32
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderSendVO extends BaseVO {


    /**
     * 发货单号
     */
    @ApiModelProperty(value = "发货单号", required = true)
    private String sendCode;

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号", required = true)
    private String orderNo;

    /**
     * 仓库编号
     */
    @ApiModelProperty(value = "仓库编号", required = true)
    private String warehouseCode;


    /**
     * 仓库名称
     */
    @ApiModelProperty(value = "仓库名称", required = true)
    private String warehouseName;

    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号", required = true)
    private String customerCode;

    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称", required = true)
    private String customerName;

    /**
     * 订单状态
     */
    private  String orderStatus;


    /**
     * 数量
     */
    private BigDecimal quantity;

    /**
     * 卷数
     */
    private Integer total;

    /***
     * 币种
     */
    private  String  currency;

    /**
     * 总金额
     */
    private BigDecimal totalAmount;

    /**
     * 对账状态
     */
    @ApiModelProperty("对账状态")
    private Integer billStatus;

    /**
     * 对账单号
     */
    @ApiModelProperty("对账单号")
    private String billNo;

    /**
     * 发货时间
     */
    @JSONField(format = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDateTime sendTime;
    /**
     * 商品信息
     */
    @ApiModelProperty(value = "商品信息", required = true)
    private List<SendGoodsVO> goods;
}
