package com.ray.business.table.vo;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 产品基础
 * @Class: OrderGoodsParams
 * @Package com.ray.business.table.params.order.goods
 * @date 2020/6/5 16:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PurchaseInRecordVO extends PurchaseGoodsVO {

    /***入库单**/
    private String inCode;

    /***客户编号*/
    private String customerCode;
    /***客户名称*/
    private String customerName;
    /****对账状态**/
    private Integer billStatus;
    /****对账单号**/
    private String billNo;

}
