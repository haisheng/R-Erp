package com.ray.business.table.vo;

import com.ray.base.table.vo.material.MaterialVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 产品基础
 * @Class: OrderGoodsParams
 * @Package com.ray.business.table.params.order.goods
 * @date 2020/6/5 16:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class SaleGoodsVO extends MaterialVO{

    /**统一单号**/
    private String bizCode;
    /**
     * 规格编号
     */
    @ApiModelProperty(value = "规格编号", required = true)
    private String goodsCode;

    /**
     * 规格名称
     */
    @ApiModelProperty(value = "规格名称", required = true)
    private String goodsName;


    /**
     * 批次号
     */
    @ApiModelProperty(value = "批次号", required = true)
    private String batchNo;

    /**
     * 序列号
     */
    @ApiModelProperty(value = "序列号", required = true)
    private String serialNumber;

    /**
     * 销售数量
     */
    @ApiModelProperty(value = "销售数量", required = true)
    private BigDecimal quantity;

    /**
     * 卷数
     */
    @ApiModelProperty(value = "卷数", required = true)
    private Integer total;


    /**
     * 单价
     */
    @ApiModelProperty(value = "单价", required = true)
    private BigDecimal price;

    /***总价**/
    private BigDecimal totalAmount;

    /**
     * 规格名称
     */
    private String modelName;


    /**
     * 规格属性
     */
    private String modelProp;

}
