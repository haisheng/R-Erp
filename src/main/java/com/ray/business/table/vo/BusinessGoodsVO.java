package com.ray.business.table.vo;

import com.ray.base.table.vo.material.model.MaterialModelVO;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 业务物料信息
 * @Class: BusinessGoodsVO
 * @Package com.ray.business.table.vo
 * @date 2020/6/11 11:20
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessGoodsVO extends MaterialModelVO {

    /**统一单号**/
    private String bizCode;

    /**
     * 唯一单号
     */
    private String code;

    /**
     * 加工单号
     */
    private String businessCode;

    /**
     * 商品编码
     */
    private String goodsCode;

    /**
     * 商品名称
     */
    private String goodsName;


    /**
     * 卷数
     */
    private Integer total;
    /**
     * 数量
     */
    private BigDecimal quantity;

    /**
     * 扣减数量
     */
    private BigDecimal deleteQuantity;

    /**
     * 实际数量
     */
    private BigDecimal realQuantity;

    /**
     * 重量
     */
    private BigDecimal goodsWeight;

    /**
     * 净重
     */
    private BigDecimal goodsNetWeight;
    /**
     * 缸号
     */
    private String gangNo;

    /**
     * 批次号
     */
    private String batchNo;

    /**
     * 序列号
     */
    private String serialNumber;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 关联编号
     */
    private String linkCode;

    /**
     * 备注
     */
    private String remark;


    private String customerCode;

    private String customerName;

    private String stepName;

    private String stepCode;

    private String type;

    /***总价**/
    private BigDecimal totalAmount;
    /**
     * 唯一key
     *
     * @return
     */
    public String key() {
        return String.format("%s|%s|%s", goodsCode, batchNo, serialNumber);
    }
}
