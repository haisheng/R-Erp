package com.ray.business.table.params.business.quick;

import com.ray.business.table.params.business.BusinessGoodsParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description:
 * @Class: BusinessQuickInParams
 * @Package com.ray.business.table.params.business.quick
 * @date 2020/7/5 14:24
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessQuickInParams extends BusinessGoodsParams {

    /**
     * 快捷入库单号
     */
    @ApiModelProperty(value = "快捷入库单号", required = true)
    @NotBlank(message = "快捷入库单号不能为空")
    @Length(max = 50, message = "快捷入库单号长度不能超过50")
    private String quickCode;
}
