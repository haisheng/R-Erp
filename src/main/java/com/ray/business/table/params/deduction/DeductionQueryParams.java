package com.ray.business.table.params.deduction;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 扣款基础
 * @Class: DeductionBase
 * @Package com.ray.business.table.params.deduction
 * @date 2020/6/12 15:00
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class DeductionQueryParams extends BaseParams {

    /**
     * 业务单号
     */
    @ApiModelProperty(value = "业务单号", required = false)
    private String businessCode;
    /**
     * 订单单号
     */
    @ApiModelProperty(value = "订单单号", required = false)
    private String orderNo;

    @ApiModelProperty(value = "订单状态", required = false)
    private String orderStatus;

}
