package com.ray.business.table.params.purchase;

import com.ray.base.table.params.FileParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author bo shen
 * @Description: 采购订单
 * @Class: SaleBase
 * @Package com.ray.business.table.params.purchase
 * @date 2020/6/7 17:56
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PurchaseBase {
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号", required = true)
    @NotBlank(message = "客户编号不能为空")
    @Length(max = 50, message = "客户编号长度不能超过50")
    private String customerCode;


    /**
     * 仓库编号
     */
    @ApiModelProperty(value = "仓库编号", required = true)
    @NotBlank(message = "仓库编号不能为空")
    @Length(max = 50, message = "仓库编号长度不能超过50")
    private String warehouseCode;


    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号", required = false)
    private String businessOrderNo;


    /**
     * 付款方式
     */
    private String payType;

    /**
     * 运输方式
     */
    private String transport;

    /**
     * 运输费用
     */
    private BigDecimal transportFee;


    /**
     * 币种
     */
    private String currency;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否含税
     */
    private Integer tax;

    @ApiModelProperty(value = "商品列表", required = true)
    @NotNull(message = "商品列表不能为空")
    @Valid
    private List<PurchaseGoodsParams> goods;

    /**
     * 文件
     */
    private List<FileParams> files;
}
