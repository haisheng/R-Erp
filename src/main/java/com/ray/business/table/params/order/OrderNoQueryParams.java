package com.ray.business.table.params.order;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 订单
 * @Class: OrderQueryDTO
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderNoQueryParams extends BaseParams {

    /**
     * 订单编码
     */
    @ApiModelProperty(value = "订单编码", required = false)
    @NotBlank(message = "订单编号不能为空")
    private String orderNo;

}
