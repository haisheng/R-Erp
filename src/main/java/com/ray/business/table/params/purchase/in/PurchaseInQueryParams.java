package com.ray.business.table.params.purchase.in;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author bo shen
 * @Description: 订单
 * @Class: OrderQueryDTO
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PurchaseInQueryParams extends BaseParams {


    /**
     * 退货单号
     */
    @ApiModelProperty(value = "入库单号-模糊", required = false)
    private String inCodeLike ;

    /**
     * 退货单号
     */
    @ApiModelProperty(value = "入库单号", required = false)
    private String inCode;


    /**
     * 订单编码
     */
    @ApiModelProperty(value = "订单编码", required = false)
    @NotBlank(message = "采购订单不能为空")
    private String orderNo;

    @ApiModelProperty(value = "客户编号", required = true)
    private String customerCode;
    @ApiModelProperty(value = "订单状态", required = false)
    private String orderStatus;
    /**
     * 对账状态
     */
    private Integer billStatus;

    /**
     * 外部单号
     */
    private String  outBusinessCode;

    /**
     * 对账单号
     */
    private String billNo;

    private List<String> orderNos;

}
