package com.ray.business.table.params.error;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 异常
 * @Class: StepBase
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class ErrorTypeQueryParams extends BaseParams {


    /**
     * 异常名称
     */
    @ApiModelProperty(value = "异常名称-模糊", required = false)
    private String errorNameLike;

    /**
     * 异常名称
     */
    @ApiModelProperty(value = "异常名称", required = false)
    private String errorName;
}
