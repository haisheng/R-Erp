package com.ray.business.table.params.business;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description:
 * @Class: BusinessGoodsParams
 * @Package com.ray.business.table.params.business
 * @date 2020/6/11 15:43
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessGoodsParams {

    /**
     * 唯一单号
     */
    private String code;

    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码", required = true)
    @NotBlank(message = "商品编码不能为空")
    @Length(max = 50, message = "商品编码长度不能超过50")
    private String goodsCode;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量", required = true)
    @NotNull(message = "数量不能为空")
    @Min(value = 0,message = "数量不能小于0")
    private BigDecimal quantity;


    /**
     * 扣减数量
     */
    private BigDecimal deleteQuantity;

    /**
     * 实际数量
     */
    private BigDecimal realQuantity;

    /**
     * 批次号
     */
    private String batchNo;

    /**
     * 单位
     */
    @ApiModelProperty(value = "单位", required = true)
    @NotBlank(message = "单位不能为空")
    private String unit;
    /**
     * 序列号
     */
    private String serialNumber;

    /**
     * 卷数
     */
    @ApiModelProperty(value = "卷数", required = true)
    @NotNull(message = "卷数不能为空")
    @Min(value = 0,message = "卷数不能小于0")
    private Integer total = 0;

    /**
     * 重量
     */
    private BigDecimal goodsWeight;

    /**
     * 净重
     */
    private BigDecimal goodsNetWeight;
    /**
     * 缸号
     */
    private String gangNo;


    /**
     * 单价
     */
    private BigDecimal price;


    /**
     * 描述
     */
    private String remark;


    /**
     * 顺序
     */
    private Integer indexSort;


    public String getKey(){
        return String.format("%s|%s|%s|%s",goodsCode,batchNo,serialNumber,unit);
    }
}
