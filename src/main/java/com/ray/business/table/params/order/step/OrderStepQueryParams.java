package com.ray.business.table.params.order.step;

import com.ray.common.BaseParams;
import lombok.Data;

/**
 * @author bo shen
 * @Description:
 * @Class: OrderQueryDTO
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderStepQueryParams extends BaseParams {
    /**
     * 订单编号
     */
    private String materialCode;

}
