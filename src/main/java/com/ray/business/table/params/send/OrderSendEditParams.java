package com.ray.business.table.params.send;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 编辑
 * @Class: OrderSendCreateParams
 * @Package com.ray.business.table.params.send
 * @date 2020/6/12 14:03
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderSendEditParams extends OrderSendBase{
    /**
     * 发货单号
     */
    @ApiModelProperty(value = "发货单号", required = true)
    @NotBlank(message = "发货单号不能为空")
    @Length(max = 50, message = "发货单号长度不能超过50")
    private String sendCode;
}
