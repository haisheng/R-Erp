package com.ray.business.table.params.order.step;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 编辑
 * @Class: OrderEditParams
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:46
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderStepEditParams extends OrderStepBase{

    /**
     * 编码
     */
    @ApiModelProperty(value = "编码", required = true)
    @NotBlank(message = "编码不能为空")
    @Length(max = 50, message = "编码长度不能超过50")
    private String code;

}
