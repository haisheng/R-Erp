package com.ray.business.table.params.purchase;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author bo shen
 * @Description: 订单
 * @Class: OrderQueryDTO
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PurchaseQueryParams extends BaseParams {


    /**
     * 订单编码
     */
    @ApiModelProperty(value = "订单编码-模糊", required = false)
    private String orderNoLike;

    /**
     * 订单编码
     */
    @ApiModelProperty(value = "订单编码", required = false)
    private String orderNo;

    @ApiModelProperty(value = "客户编号", required = true)
    private String customerCode;
    /**
     * 客户订单编号
     */
    @ApiModelProperty(value = "客户订单编号", required = false)
    private String businessOrderNo;
    @ApiModelProperty(value = "订单状态", required = false)
    private String orderStatus;
    /**
     * 对账状态
     */
    private Integer billStatus;

    /**
     * 对账单号
     */
    private String billNo;

    /**
     * 外部发送时间
     */
    private LocalDateTime outSendStartTime;

    /**
     * 外部发送时间
     */
    private LocalDateTime outSendEndTime;


    private List<String> orderNos;
}
