package com.ray.business.table.params.order;

import com.ray.business.table.params.step.StepBase;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 订单编辑
 * @Class: OrderEditParams
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:46
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderEditParams extends OrderBase{

    /**
     * 订单编码`
     */
    @ApiModelProperty(value = "订单编码", required = true)
    @NotBlank(message = "订单编码不能为空")
    @Length(max = 50, message = "订单编码长度不能超过50")
    private String orderNo;

}
