package com.ray.business.table.params.send;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author bo shen
 * @Description: 发货基础
 * @Class: OrderSendBase
 * @Package com.ray.business.table.params.send
 * @date 2020/6/12 11:32
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderSendBase {



    /**
     * 仓库编号
     */
    @ApiModelProperty(value = "仓库编号", required = true)
    @NotBlank(message = "仓库编号不能为空")
    @Length(max = 50, message = "仓库编号长度不能超过50")
    private String warehouseCode;


    /**
     * 开始顺序
     */
    @ApiModelProperty(value = "开始顺序", required = true)
    private Integer indexSort;

    /**
     * 商品信息
     */
    @ApiModelProperty(value = "商品信息", required = true)
    @NotNull(message = "商品信息")
    @Valid
    private List<SendGoodsParams> goods;
}
