package com.ray.business.table.params.order;

import com.ray.business.table.params.step.StepBase;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 订单新增
 * @Class: StepCreateParams
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:46
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderCreateParams extends OrderBase{

}
