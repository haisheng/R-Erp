package com.ray.business.table.params.purchase;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description:
 * @Class: SaleCreateParams
 * @Package com.ray.business.table.params.purchase
 * @date 2020/6/7 18:02
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PurchaseCreateParams  extends PurchaseBase{

    /**
     * 客户订单编号
     */
    @ApiModelProperty(value = "客户订单编号", required = false)
    private String businessOrderNo;
}
