package com.ray.business.table.params.business.quick;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description:
 * @Class: BusinessQuickInParams
 * @Package com.ray.business.table.params.business.quick
 * @date 2020/7/5 14:24
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessQuickSubmitParams extends BaseParams {

    /**
     * 快捷入库单号
     */
    @ApiModelProperty(value = "快捷入库单号", required = true)
    @NotBlank(message = "快捷入库单号不能为空")
    @Length(max = 50, message = "快捷入库单号长度不能超过50")
    private String quickCode;

    /**
     * 单价
     */
    @ApiModelProperty(value = "单价", required = true)
    @NotNull(message = "单价不能为空")
    @Min(value = 0,message = "单价不能小于0")
    private BigDecimal price;

    /**
     * 雇员
     */
    @ApiModelProperty(value = "雇员", required = false)
    @NotBlank(message = "雇员不能为空")
    private String  employee;

    /**
     * 雇员a
     */
    @ApiModelProperty(value = "雇员a", required = false)
    @NotBlank(message = "雇员不能为空")
    private String  employeea;

    /**
     * 外部单号
     */
    private String outBusinessCode;
}
