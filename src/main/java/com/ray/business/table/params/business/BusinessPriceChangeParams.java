package com.ray.business.table.params.business;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 加工单价修改
 * @Class: BusinessPriceChangeParams
 * @Package com.ray.business.table.params.business
 * @date 2020/7/11 15:07
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessPriceChangeParams {

    /**
     * 加工单号
     */
    @ApiModelProperty(value = "加工单号", required = true)
    @NotBlank(message = "加工单号不能为空")
    @Length(max = 50, message = "加工单号长度不能超过50")
    private String businessCode;


    /**
     * 加工单价
     */
    @ApiModelProperty(value = "加工单价", required = true)
    @NotNull(message = "加工单价不能为空")
    @Min(value = 0,message = "单价不能是负数")
    private BigDecimal price;
}
