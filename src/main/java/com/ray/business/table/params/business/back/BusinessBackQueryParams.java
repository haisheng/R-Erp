package com.ray.business.table.params.business.back;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author bo shen
 * @Description: 业务查询
 * @Class: BusinessQueryParams
 * @Package com.ray.business.table.params.business
 * @date 2020/6/11 10:34
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessBackQueryParams extends BaseParams {

    /*** 加工单号**/
    @ApiModelProperty(value = "加工单号",required = false)
    private String businessCode;

    /*** 订单状态**/
    private String orderStatus;
}
