package com.ray.business.table.params.business.quick;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author bo shen
 * @Description: 业务查询
 * @Class: BusinessQueryParams
 * @Package com.ray.business.table.params.business
 * @date 2020/6/11 10:34
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessQuickQueryParams extends BaseParams {

    @ApiModelProperty(value = "步骤编码",required = true)
    @NotBlank(message = "步骤编码不能为空")
    private String stepCode;

    @ApiModelProperty(value = "订单编号")
    @NotBlank(message = "订单编号不能为空")
    private String orderNo;


    @ApiModelProperty(value = "加工单位", required = false)
    @NotBlank(message = "加工单位不能为空")
    private String customerCode;

    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码")
    @NotBlank(message = "商品编码不能为空")
    private String goodsCode;


}
