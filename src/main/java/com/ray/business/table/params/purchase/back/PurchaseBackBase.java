package com.ray.business.table.params.purchase.back;

import com.ray.base.table.params.FileParams;
import com.ray.business.table.params.purchase.PurchaseGoodsParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author bo shen
 * @Description: 采购退货
 * @Class: SaleOutBase
 * @Package com.ray.business.table.params.purchase.back
 * @date 2020/6/8 13:51
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PurchaseBackBase {

    /**
     * 订单编码
     */
    @ApiModelProperty(value = "订单编码", required = true)
    @NotBlank(message = "订单编码不能为空")
    @Length(max = 50, message = "订单编码长度不能超过50")
    private String orderNo;

    @ApiModelProperty(value = "商品列表", required = true)
    @NotNull(message = "商品列表不能为空")
    @Valid
    private List<PurchaseGoodsParams> goods;

    /**
     * 文件
     */
    private List<FileParams> files;
}
