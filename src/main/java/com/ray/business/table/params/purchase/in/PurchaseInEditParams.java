package com.ray.business.table.params.purchase.in;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 退货单创建
 * @Class: SaleOutCreateParams
 * @Package com.ray.business.table.params.purchase.back
 * @date 2020/6/8 13:57
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PurchaseInEditParams extends PurchaseInBase {

    /**
     * 退货单号
     */
    @ApiModelProperty(value = "入库单号", required = true)
    @NotBlank(message = "入库单号不能为空")
    @Length(max = 50, message = "入库单号长度不能超过50")
    private String inCode;
}
