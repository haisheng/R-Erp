package com.ray.business.table.params.sale;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description:
 * @Class: SaleCreateParams
 * @Package com.ray.business.table.params.purchase
 * @date 2020/6/7 18:02
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class SaleEditParams extends SaleBase {
    /**
     * 订单编码
     */
    @ApiModelProperty(value = "订单编码", required = true)
    @NotBlank(message = "订单编码不能为空")
    @Length(max = 50, message = "订单编码长度不能超过50")
    private String orderNo;
}
