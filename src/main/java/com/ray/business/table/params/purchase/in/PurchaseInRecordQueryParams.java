package com.ray.business.table.params.purchase.in;

import com.ray.common.BaseParams;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 订单
 * @Class: OrderQueryDTO
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PurchaseInRecordQueryParams extends BaseParams {


    /**
     * 订单编码
     */
    private String orderNo;
    /**
     * 客户单号
     */
    private String customerCode;


}
