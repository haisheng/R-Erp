package com.ray.business.table.params.business.back;

import com.ray.business.table.params.business.BusinessGoodsParams;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author bo shen
 * @Description: 退料单
 * @Class: BusinessBackBase
 * @Package com.ray.business.table.params.business.back
 * @date 2020/6/22 13:56
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessBackCreateParams extends BusinessBackBase {


}
