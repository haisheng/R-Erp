package com.ray.business.table.params.order;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 订单
 * @Class: OrderQueryDTO
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderQueryParams extends BaseParams {


    /**
     * 订单编码
     */
    @ApiModelProperty(value = "订单编码-模糊", required = false)
    private String orderNoLike;

    /**
     * 订单编码
     */
    @ApiModelProperty(value = "订单编码", required = false)
    private String orderNo;

    @ApiModelProperty(value = "客户编号", required = true)
    private String customerCode;
    @ApiModelProperty(value = "订单状态",required = false)
    private String orderStatus;
}
