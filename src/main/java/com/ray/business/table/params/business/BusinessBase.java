package com.ray.business.table.params.business;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author bo shen
 * @Description: 生产业务
 * @Class: BusinessBase
 * @Package com.ray.business.table.params.business
 * @date 2020/6/11 15:37
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessBase {

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号", required = true)
    @NotBlank(message = "订单编号不能为空")
    @Length(max = 50, message = "订单编号长度不能超过50")
    private String orderNo;

    /**
     * 客户编号
     */
    @ApiModelProperty(value = "加工客户编号", required = true)
    @NotBlank(message = "加工客户编号不能为空")
    @Length(max = 50, message = "加工客户编号长度不能超过50")
    private String customerCode;

    /**
     * 步骤编码
     */
    @ApiModelProperty(value = "步骤编码", required = true)
    @NotBlank(message = "步骤编码不能为空")
    @Length(max = 50, message = "步骤编码长度不能超过50")
    private String stepCode;

    /**
     * 仓库
     */
    @ApiModelProperty(value = "步骤编码", required = true)
    @NotBlank(message = "步骤编码不能为空")
    @Length(max = 50, message = "步骤编码长度不能超过50")
    private String warehouseCode;

    /**
     * 单价
     */
    @ApiModelProperty(value = "单价", required = true)
    @NotNull(message = "单价不能为空")
    @Min(value = 0,message = "单价不能小于0")
    private BigDecimal price;


    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码", required = true)
    @NotBlank(message = "商品编码不能为空")
    @Length(max = 50, message = "商品编码长度不能超过50")
    private String goodsCode;

    /**
     * 雇员
     */
    @ApiModelProperty(value = "雇员", required = false)
    private String  employee;

    /**
     * 雇员a
     */
    @ApiModelProperty(value = "雇员a", required = false)
    private String  employeea;

    @ApiModelProperty(value = "卷数", required = false)
    @NotNull(message = "卷数不能为空")
    @Min(value = 0,message = "卷数不能小于0")
    private Integer total;


    /**
     * 外部单号
     */
    private String  outBusinessCode;

    @ApiModelProperty(value = "商品信息", required = true)
    @NotNull(message = "商品信息不能为空")
    @Valid
    private List<BusinessGoodsParams> goods;
}
