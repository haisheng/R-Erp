package com.ray.business.table.params.error;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 错误
 * @Class: StepBase
 * @Package com.ray.business.table.params.errorType
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class ErrorTypeBase {


    /**
     * 错误名称
     */
    @ApiModelProperty(value = "错误名称", required = true)
    @NotBlank(message = "错误名称不能为空")
    @Length(max = 50, message = "错误名称长度不能超过50")
    private String errorName;

    /**
     * 异常数量
     */
    @NotNull(message = "异常数量不能为空")
    @Min(value = 0,message = "异常数量大于0")
    private Integer number;

    /**
     * 是否需求长度
     */
    @NotNull(message = "异常数量不能为空")
    private Integer needLength;

    /**
     * 扣减长度
     */
    @NotNull(message = "扣减长度不能为空")
    @Min(value = 0,message = "扣减长度大于0")
    private BigDecimal deleteLength;
}
