package com.ray.business.table.params.order.step;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author bo shen
 * @Description: 订单步骤
 * @Class: OrderStepBase
 * @Package com.ray.business.table.params.order.step
 * @date 2020/6/5 9:24
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderStepBase {



    /**
     * 步骤编号
     */
    @ApiModelProperty(value = "步骤编号", required = true)
    @NotBlank(message = "步骤编号不能为空")
    @Length(max = 50, message = "步骤编号长度不能超过50")
    private String stepCode;

    /**
     * 入库仓库
     */
    @ApiModelProperty(value = "入库仓库", required = true)
    @NotBlank(message = "入库仓库不能为空")
    @Length(max = 50, message = "入库仓库长度不能超过50")
    private String inWarehouseCode;

    /**
     * 出库仓库
     */
    @ApiModelProperty(value = "出库仓库", required = true)
    @NotBlank(message = "出库仓库不能为空")
    @Length(max = 50, message = "出库仓库长度不能超过50")
    private String outWarehouseCode;


    /**
     * 顺序
     */
    @ApiModelProperty(value = "顺序", required = true)
    @NotNull(message = "顺序不能为空")
    private Integer indexSort;
}
