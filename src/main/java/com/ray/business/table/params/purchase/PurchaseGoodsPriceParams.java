package com.ray.business.table.params.purchase;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description:
 * @Class: PurchaseGoodsPriceParams
 * @Package com.ray.business.table.params.order.goods
 * @date 2020/8/20 18:10
 * @company <p>传化绿色慧联科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PurchaseGoodsPriceParams {
    @ApiModelProperty(value = "编号", required = true)
    @NotBlank(message = "编号不能为空")
    private String code;

    @NotNull(message = "单价不能为空")
    private BigDecimal price;
}
