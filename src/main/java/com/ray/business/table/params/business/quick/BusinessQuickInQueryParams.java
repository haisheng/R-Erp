package com.ray.business.table.params.business.quick;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 业务查询
 * @Class: BusinessQueryParams
 * @Package com.ray.business.table.params.business
 * @date 2020/6/11 10:34
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessQuickInQueryParams extends BaseParams {

    @ApiModelProperty(value = "快捷入库单",required = true)
    @NotBlank(message = "快捷入库单不能为空")
    private String quickCode;




}
