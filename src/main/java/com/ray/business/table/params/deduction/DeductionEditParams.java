package com.ray.business.table.params.deduction;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 编辑
 * @Class: DeductionCreateParams
 * @Package com.ray.business.table.params.deduction
 * @date 2020/6/12 15:08
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class DeductionEditParams extends DeductionBase {

    /**
     * 扣款单号
     */
    @ApiModelProperty(value = "扣款单号", required = true)
    @NotBlank(message = "扣款单号不能为空")
    @Length(max = 50, message = "扣款单号长度不能超过50")
    private String deductionCode;

}
