package com.ray.business.table.params.deduction;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 创建
 * @Class: DeductionCreateParams
 * @Package com.ray.business.table.params.deduction
 * @date 2020/6/12 15:08
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class DeductionCreateParams extends DeductionBase{
}
