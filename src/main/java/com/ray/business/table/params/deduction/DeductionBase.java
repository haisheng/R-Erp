package com.ray.business.table.params.deduction;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 扣款基础
 * @Class: DeductionBase
 * @Package com.ray.business.table.params.deduction
 * @date 2020/6/12 15:00
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class DeductionBase {

    /**
     * 业务单号
     */
    @ApiModelProperty(value = "业务单号", required = true)
    @NotBlank(message = "业务单号不能为空")
    @Length(max = 50, message = "业务单号长度不能超过50")
    private String businessCode;


    /**
     * 扣款金额
     */
    @ApiModelProperty(value = "扣款金额", required = true)
    @NotNull(message = "扣款金额不能为空")
    private BigDecimal amount;

    /**
     * 描述
     */
    @ApiModelProperty(value = "描述", required = true)
    private String remark;
}
