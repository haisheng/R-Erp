package com.ray.business.table.params.order;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ray.business.table.params.order.goods.OrderGoodsParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author bo shen
 * @Description: 订单
 * @Class: OrderBase
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderBase {

    /**
     * 订单名称
     */
    @ApiModelProperty(value = "订单名称", required = false)
    @Length(max = 50, message = "订单名称长度不能超过50")
    private String orderName;

    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号", required = true)
    @NotBlank(message = "客户编号不能为空")
    @Length(max = 50, message = "客户编号长度不能超过50")
    private String customerCode;

    /**
     * 付款方式
     */
    private String payType;

    /**
     * 运输方式
     */
    private String transport;

    /**
     * 运输费用
     */
    private BigDecimal transportFee;


    /**
     * 币种
     */
    private String currency;

    /**
     * 备注
     */
    private String remark;

    /**
     * 业务员编码
     */
    private String ownerCode;

    /**
     * 是否含税
     */
    private Integer tax;
    /**
     * 发货时间
     */
    @ApiModelProperty(value = "发货时间", required = false)
    @JSONField(format = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
    @DateTimeFormat( pattern = "yyyy-MM-dd")
    private Date sendTime;


    /**
     * 订单物品信息
     */
    @ApiModelProperty(value = "订单物品信息", required = true)
    @NotNull(message = "订单物品信息不能为空")
    @Valid
    private List<OrderGoodsParams> goods;

}
