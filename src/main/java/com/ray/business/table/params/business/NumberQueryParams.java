package com.ray.business.table.params.business;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description:
 * @Class: NumberQueryParams
 * @Package com.ray.business.table.params.business
 * @date 2020/7/14 13:49
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class NumberQueryParams {

    @NotBlank(message = "序列号不能为空")
    private String serialNumber;
}
