package com.ray.business.table.params.purchase;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 产品基础
 * @Class: OrderGoodsParams
 * @Package com.ray.business.table.params.order.goods
 * @date 2020/6/5 16:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PurchaseGoodsParams {


    /**
     * 规格编号
     */
    @ApiModelProperty(value = "规格编号", required = true)
    @NotBlank(message = "规格编号不能为空")
    @Length(max = 50, message = "规格编号长度不能超过50")
    private String goodsCode;

    /**
     * 生产数量
     */
    @ApiModelProperty(value = "采购数量", required = true)
    @NotNull(message = "采购数量不能为空")
    @Min(value = 1,message = "采购不能小于1")
    private BigDecimal quantity;

    /**
     * 单价
     */
    @ApiModelProperty(value = "单价", required = true)
    @NotNull(message = "单价不能为空")
    @Min(value = 0,message = "单价不能大于0")
    private BigDecimal price;


    /**
     * 批次号
     */
    @ApiModelProperty(value = "批次号", required = true)
    private String batchNo;

    /**
     * 单位
     */
    @ApiModelProperty(value = "单位", required = true)
    @NotBlank(message = "单位不能为空")
    private String unit;

    /**
     * 序列号
     */
    @ApiModelProperty(value = "序列号", required = true)
    private String serialNumber;


    /**
     * 卷数
     */
    @ApiModelProperty(value = "卷数", required = true)
    private Integer total;


    public  String getKey(){
      return   String.format("%s|%s|%s|%s",goodsCode,batchNo,serialNumber,unit);
    }

}
