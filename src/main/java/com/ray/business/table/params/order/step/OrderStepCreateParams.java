package com.ray.business.table.params.order.step;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 订单新增
 * @Class: StepCreateParams
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:46
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderStepCreateParams extends OrderStepBase{

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "物料编号", required = true)
    @NotBlank(message = "物料编号不能为空")
    @Length(max = 50, message = "物料编号长度不能超过50")
    private String materialCode;
}
