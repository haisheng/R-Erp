package com.ray.business.table.params.business;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 创建订单
 * @Class: BusinessCreateParams
 * @Package com.ray.business.table.params.business
 * @date 2020/6/11 15:46
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessEditParams extends BusinessBase{

    /**
     * 加工单号
     */
    @ApiModelProperty(value = "加工单号", required = true)
    @NotBlank(message = "加工单号不能为空")
    @Length(max = 50, message = "加工单号长度不能超过50")
    private String businessCode;
}
