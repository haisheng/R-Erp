package com.ray.business.table.params.order.goods;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 产品基础
 * @Class: OrderGoodsParams
 * @Package com.ray.business.table.params.order.goods
 * @date 2020/6/5 16:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderGoodsParams {


    /**
     * 规格编号
     */
    @ApiModelProperty(value = "规格编号", required = true)
    @NotBlank(message = "规格编号不能为空")
    @Length(max = 50, message = "规格编号长度不能超过50")
    private String goodsCode;


    /**
     * 客户型号名称
     */
    @ApiModelProperty(value = "客户型号名称", required = true)
    @Length(max = 50, message = "客户型号名称长度不能超过50")
    private String customerModelName;

    /**
     * 规格编号
     */
    @ApiModelProperty(value = "客户产品属性", required = true)
    @Length(max = 50, message = "客户产品属性长度不能超过50")
    private String customerModelProp;

    /**
     * 生产数量
     */
    @ApiModelProperty(value = "生产数量", required = true)
    @NotNull(message = "生产数量不能为空")
    @Min(value = 1,message = "数量不能小于1")
    private BigDecimal quantity;

    /**
     * 单价
     */
    @ApiModelProperty(value = "单价", required = true)
    @NotNull(message = "单价不能为空")
    @Min(value = 0,message = "单价不能大于0")
    private BigDecimal price;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", required = true)
    private String remark;

}
