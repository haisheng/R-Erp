package com.ray.business.table.params.business;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author bo shen
 * @Description: 业务查询
 * @Class: BusinessQueryParams
 * @Package com.ray.business.table.params.business
 * @date 2020/6/11 10:34
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessQueryParams extends BaseParams {

    @ApiModelProperty(value = "步骤编码")
    private String stepCode;

    @ApiModelProperty(value = "订单编号")
    private String orderNo;

    /**
     * 规格名称查询
     **/
    @ApiModelProperty(value = "规格名称查询", required = false)
    private String modelNameLike;

    @ApiModelProperty(value = "加工单位", required = false)
    private String customerCode;

    @ApiModelProperty(value = "订单状态", required = false)
    private String orderStatus;

    /*** 业务单号**/
    @ApiModelProperty(value = "业务单号", required = false)
    private String businessCode;

    /**
     * 业务单号
     **/
    @ApiModelProperty(value = "业务单号-模糊", required = false)
    private String businessCodeLike;

    /**
     * 类型
     **/
    private String businessType;
    /**
     * 对账状态
     */
    private Integer billStatus;

    /*** 对账单号*/
    private String billNo;

    @ApiModelProperty(value = "物料类型")
    private String materialType;

    /**
     * 名称
     */
    private String nameLike;
    /**
     * 名称
     */
    private String name;

    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码")
    private String goodsCode;

    /*** 在列表中的*/
    @ApiModelProperty(value = "在列表中的", hidden = true)
    private List<String> modelCodes;

    /**
     * 外部单号
     */
    private String  outBusinessCode;

    private Integer orderProd;

}
