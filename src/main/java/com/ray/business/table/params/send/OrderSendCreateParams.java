package com.ray.business.table.params.send;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 创建
 * @Class: OrderSendCreateParams
 * @Package com.ray.business.table.params.send
 * @date 2020/6/12 14:03
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderSendCreateParams extends OrderSendBase {

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号", required = true)
    @NotBlank(message = "订单编号不能为空")
    @Length(max = 50, message = "订单编号长度不能超过50")
    private String orderNo;

}
