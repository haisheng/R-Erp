package com.ray.business.table.params.purchase.back;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 退货单创建
 * @Class: SaleBackCreateParams
 * @Package com.ray.business.table.params.purchase.back
 * @date 2020/6/8 13:57
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PurchaseBackCreateParams extends PurchaseBackBase{
}
