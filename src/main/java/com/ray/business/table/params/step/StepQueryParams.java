package com.ray.business.table.params.step;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 步骤
 * @Class: StepBase
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class StepQueryParams extends BaseParams {


    /**
     * 步骤名称
     */
    @ApiModelProperty(value = "步骤名称-模糊", required = false)
    private String stepNameLike;

    /**
     * 步骤名称
     */
    @ApiModelProperty(value = "步骤名称", required = false)
    private String stepName;
}
