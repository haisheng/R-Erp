package com.ray.business.table.params.sale;

import lombok.Data;

/**
 * @author bo shen
 * @Description:
 * @Class: SaleCreateParams
 * @Package com.ray.business.table.params.purchase
 * @date 2020/6/7 18:02
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class SaleCreateParams extends SaleBase {
}
