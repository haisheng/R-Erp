package com.ray.business.table.params.step;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 步骤
 * @Class: StepBase
 * @Package com.ray.business.table.params.step
 * @date 2020/6/4 19:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class StepBase {


    /**
     * 步骤名称
     */
    @ApiModelProperty(value = "步骤名称", required = true)
    @NotBlank(message = "步骤名称不能为空")
    @Length(max = 50, message = "步骤名称长度不能超过50")
    private String stepName;

    /**
     * 是否输出成品
     */
    private Integer goodsStatus;

    /**
     * 是否订单出入库
     */
    private Integer orderStatus;

    /**
     * 是否订单出入库
     */
    private Integer quickStatus;
}
