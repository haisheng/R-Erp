package com.ray.business.table.params.business;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 创建订单
 * @Class: BusinessCreateParams
 * @Package com.ray.business.table.params.business
 * @date 2020/6/11 15:46
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BusinessCreateParams extends BusinessBase{
}
