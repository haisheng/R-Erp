package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.enums.BusinessStatusEnum;
import com.ray.business.enums.BusinessTypeEnum;
import com.ray.business.enums.OrderStatusEnum;
import com.ray.business.enums.PurchaseStatusEnum;
import com.ray.business.table.dto.*;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdBusinessOut;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.business.table.mapper.ProdBusinessMapper;
import com.ray.common.CountParams;
import com.ray.common.SysMsgCodeConstant;
import com.ray.wms.table.entity.WmsOrderIn;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 加工单号 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdBusinessService extends ServiceImpl<ProdBusinessMapper, ProdBusiness> {

    @Autowired
    private ProdBusinessMapper prodBusinessMapper;

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdBusiness> page(CommonPage<BusinessQueryDTO, Page<ProdBusiness>> queryParams, LoginUser loginUser) {
        BusinessQueryDTO params = queryParams.getEntity();
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.like(StrUtil.isNotBlank(params.getBusinessCodeLike()), "business_code", params.getBusinessCodeLike());
            queryWrapper.gt(ObjectUtil.isNotNull(params.getStartTime()), "create_time", params.getStartTime());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getEndTime()), "create_time", params.getEndTime());
        }
        queryWrapper.in(StrUtil.isBlank(params.getOrderStatus()) && ObjectUtil.isNotEmpty(params.getIncludeStatus()),
                "order_status", params.getIncludeStatus());
        queryWrapper.orderByDesc("create_time");
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return page(queryParams.getPage(), queryWrapper);
    }


    /**
     * 编辑订单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdBusiness entity, LoginUser loginUser) {
        ProdBusiness query = new ProdBusiness();
        query.setBusinessCode(entity.getBusinessCode());
        UpdateWrapper<ProdBusiness> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询订单上一个工艺的入库订单
     *
     * @param orderNo
     * @param stepCode
     * @param loginUser
     * @return
     */
    public List<ProdBusiness> listByOrderNoAndSetpCode(String orderNo, String stepCode, String customerCode, LoginUser loginUser) {
        Assert.hasLength(orderNo, SysMsgCodeConstant.Error.ERR10000001);
        Assert.hasLength(stepCode, SysMsgCodeConstant.Error.ERR10000001);
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        entity.setOrderNo(orderNo);
        entity.setStepCode(stepCode);
        entity.setCustomerCode(customerCode);
        entity.setBusinessType(BusinessTypeEnum.IN.getValue());
        entity.setOrderStatus(BusinessStatusEnum.FINISH.getValue());
        entity.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }


    /**
     * 查询订单上一个工艺的入库订单
     *
     * @param orderNo
     * @param stepCode
     * @param loginUser
     * @return
     */
    public List<ProdBusiness> listByOrderNoSetpCodeAndGoodsCode(String orderNo, String stepCode, String goodsCode, LoginUser loginUser) {
        Assert.hasLength(orderNo, SysMsgCodeConstant.Error.ERR10000001);
        Assert.hasLength(stepCode, SysMsgCodeConstant.Error.ERR10000001);
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        entity.setOrderNo(orderNo);
        entity.setStepCode(stepCode);
        entity.setGoodsCode(goodsCode);
        entity.setBusinessType(BusinessTypeEnum.IN.getValue());
        entity.setOrderStatus(BusinessStatusEnum.FINISH.getValue());
        entity.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询订单上一个工艺的入库订单
     *
     * @param orderNo
     * @param stepCode
     * @param loginUser
     * @return
     */
    public List<ProdBusiness> listInBusiness(String orderNo, String stepCode, String goodsCode, LoginUser loginUser) {
        Assert.hasLength(orderNo, SysMsgCodeConstant.Error.ERR10000001);
        Assert.hasLength(stepCode, SysMsgCodeConstant.Error.ERR10000001);
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        entity.setOrderNo(orderNo);
        entity.setStepCode(stepCode);
        entity.setGoodsCode(goodsCode);
        entity.setBusinessType(BusinessTypeEnum.IN.getValue());
        entity.setOrderStatus(BusinessStatusEnum.FINISH.getValue());
        entity.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询订单上一个工艺的入库订单
     *
     * @param orderNo
     * @param stepCode
     * @param loginUser
     * @return
     */
    public List<ProdBusiness> listOutByOrderNoAndSetpCode(String orderNo, String stepCode, String customerCode, String goodsCode, LoginUser loginUser) {
        Assert.hasLength(orderNo, SysMsgCodeConstant.Error.ERR10000001);
        Assert.hasLength(stepCode, SysMsgCodeConstant.Error.ERR10000001);
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        entity.setOrderNo(orderNo);
        entity.setStepCode(stepCode);
        entity.setCustomerCode(customerCode);
        entity.setGoodsCode(goodsCode);
        entity.setBusinessType(BusinessTypeEnum.OUT.getValue());
        entity.setOrderStatus(BusinessStatusEnum.FINISH.getValue());
        entity.setStatus(YesOrNoEnum.NO.getValue());
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询客户对应的出单
     *
     * @param orderNo
     * @param stepCode
     * @param loginUser
     * @return
     */
    public List<ProdBusiness> listByOrderNo(String orderNo, String stepCode, String customerCode, String goodsCode, LoginUser loginUser) {
        Assert.hasLength(orderNo, "参数[orderNo]不存在");
        Assert.hasLength(stepCode, "参数[stepCode]不存在");
        Assert.hasLength(customerCode, "参数[customerCode]不存在");
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        entity.setOrderNo(orderNo);
        entity.setStepCode(stepCode);
        entity.setCustomerCode(customerCode);
        entity.setGoodsCode(goodsCode);
        entity.setBusinessType(BusinessTypeEnum.OUT.getValue());
        entity.setOrderStatus(OrderStatusEnum.FINISH.getValue());
        entity.setStatus(YesOrNoEnum.NO.getValue());
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询客户对应的出单
     *
     * @param loginUser
     * @return
     */
    public List<ProdBusiness> listInByOrderNo(String businessCode, LoginUser loginUser) {
        Assert.hasLength(businessCode, "参数[businessCode]不存在");
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        entity.setBusinessCode(businessCode);
        entity.setBusinessType(BusinessTypeEnum.OUT.getValue());
        entity.setOrderStatus(OrderStatusEnum.FINISH.getValue());
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }


    /**
     * 查询订单
     *
     * @param businessCode 业务加工订单编码
     * @param loginUser    当前操作员
     * @return
     */
    public ProdBusiness queryBusinessByBusinessCode(String businessCode, LoginUser loginUser) {
        ProdBusiness query = new ProdBusiness();
        query.setBusinessCode(businessCode);
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 完成订单
     *
     * @param businessCode
     * @param loginUser
     * @return
     */
    public Boolean finishOrder(String businessCode, LoginUser loginUser) {
        ProdBusiness entity = new ProdBusiness();
        entity.setOrderStatus(BusinessStatusEnum.FINISH.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdBusiness query = new ProdBusiness();
        query.setBusinessCode(businessCode);
        UpdateWrapper<ProdBusiness> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 分组统计 按钮工序和类型
     *
     * @param countParams
     * @param loginUser
     * @return
     */
    public List<GroupBusinessDTO> groupBusiness(CountParams countParams, LoginUser loginUser) {
        countParams.setTenantCode(loginUser.getCompanyCode());
        return prodBusinessMapper.countGroupBusiness(countParams);
    }

    /**
     * 分组统计 按钮工序和类型 訂單
     *
     * @param countParams
     * @param loginUser
     * @return
     */
    public List<GroupBusinessDTO> orderGroupBusiness(CountParams countParams, LoginUser loginUser) {
        countParams.setTenantCode(loginUser.getCompanyCode());
        return prodBusinessMapper.orderGroupBusiness(countParams);
    }


    /**
     * 确认对账
     *
     * @param businessCodes
     * @param loginUser
     * @return
     */
    public Boolean comfireBill(List<String> businessCodes, String billNo, LoginUser loginUser) {
        ProdBusiness entity = new ProdBusiness();
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setBillNo(billNo);
        DataOPUtil.editUser(entity, loginUser);
        ProdBusiness query = new ProdBusiness();
        query.setBillStatus(YesOrNoEnum.NO.getValue());
        UpdateWrapper<ProdBusiness> updateWrapper = new UpdateWrapper<>(query);
        updateWrapper.in("business_code", businessCodes);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 取消对账
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public Boolean cancelBill(String billNo, LoginUser loginUser) {
        ProdBusiness entity = new ProdBusiness();
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        entity.setBillNo("");
        DataOPUtil.editUser(entity, loginUser);
        ProdBusiness query = new ProdBusiness();
        query.setBillStatus(YesOrNoEnum.YES.getValue());
        query.setBillNo(billNo);
        UpdateWrapper<ProdBusiness> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询列表
     *
     * @param orderNos
     * @param businessType
     * @param loginUser
     * @return
     */
    public List<ProdBusiness> listByOrderNos(List<String> orderNos, String businessType, LoginUser loginUser) {
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        entity.setBusinessType(businessType);
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("business_code", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询列表
     *
     * @param orderNos
     * @param businessType
     * @param loginUser
     * @return
     */
    public List<ProdBusiness> listByOrderNosBIllFinish(List<String> orderNos, String businessType, LoginUser loginUser) {
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setBusinessType(businessType);
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("business_code", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 统计数量
     *
     * @param countParams
     * @param loginUser
     * @return
     */
    public List<BusinessQuantityDTO> businessQuantity(CountParams countParams, LoginUser loginUser) {
        countParams.setTenantCode(loginUser.getCompanyCode());
        return prodBusinessMapper.businessQuantity(countParams);
    }

    /**
     * 获取对账单对应的
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public List<ProdBusiness> listByBillNo(String billNo, LoginUser loginUser) {
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        entity.setBillNo(billNo);
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询对应出库单列表
     *
     * @param orderNo
     * @param stepCode
     * @param loginUser
     * @return
     */
    public List<ProdBusiness> listOutByOrderNoAndSetpCode(String orderNo, String stepCode, LoginUser loginUser) {
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        entity.setOrderNo(orderNo);
        entity.setStepCode(stepCode);
        entity.setBusinessType(BusinessTypeEnum.OUT.getValue());
        entity.setOrderStatus(BusinessStatusEnum.FINISH.getValue());
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 更新数量
     *
     * @param businessCode
     * @param quantity
     * @param updateVersion
     * @param loginUser
     * @return
     */
    public boolean updateQuantity(String businessCode, BigDecimal quantity, Integer total, Integer updateVersion, LoginUser loginUser) {
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        entity.setUpdateVersion(updateVersion + 1);
        entity.setQuantity(quantity);
        entity.setTotal(total);
        DataOPUtil.editUser(entity, loginUser);
        ProdBusiness query = BeanCreate.newBean(ProdBusiness.class);
        query.setBusinessCode(businessCode);
        query.setUpdateVersion(updateVersion);
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return update(entity, queryWrapper);
    }

    /**
     * 查询生效的出库单
     *
     * @param orderNo
     * @param stepCode
     * @param loginUser
     * @return
     */
    public List<ProdBusiness> listUsedByOrderNo(String orderNo, String stepCode, LoginUser loginUser) {
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        entity.setOrderNo(orderNo);
        entity.setStepCode(stepCode);
        entity.setBusinessType(BusinessTypeEnum.OUT.getValue());
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_status", Arrays.asList(BusinessStatusEnum.UN_CHECK.getValue(), BusinessStatusEnum.UN_IN.getValue(), BusinessStatusEnum.FINISH.getValue()));
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    public List<ProdBusiness> listOutGroupStep(BusinessQueryDTO businessQueryDTO,List<String> stepCodes, LoginUser loginUser) {
        if(ObjectUtil.isEmpty(stepCodes)){
            return  new ArrayList<>();
        }
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        BeanUtil.copyProperties(businessQueryDTO, entity);
        entity.setBusinessType(BusinessTypeEnum.OUT.getValue());
        entity.setStatus(YesOrNoEnum.NO.getValue());
        entity.setOrderStatus(BusinessStatusEnum.FINISH.getValue());
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("step_code",stepCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.groupBy("order_no,step_code,customer_code,goods_code");
        return list(queryWrapper);
    }

    /**
     * 产量统计
     *
     * @param userBusinessQuantityDTO
     * @return
     */
    public List<BusinessQuantityDTO> quantityCount(UserBusinessQuantityDTO userBusinessQuantityDTO) {
        return prodBusinessMapper.quantityCount(userBusinessQuantityDTO);
    }

    /**
     * 工序产量统计
     *
     * @return
     */
    public List<BusinessInCountQuantityDTO> countInQuantity(String orderNo, String goodsCode, LoginUser loginUser) {
        return prodBusinessMapper.countInQuantity(orderNo, goodsCode, loginUser.getCompanyCode());
    }

    /**
     * 工序产量统计
     *
     * @return
     */
    public List<BusinessInCountQuantityDTO> countAllQuantity(String orderNo, String goodsCode, LoginUser loginUser) {
        return prodBusinessMapper.countAllQuantity(orderNo, goodsCode, loginUser.getCompanyCode());
    }

    public Boolean updateQuantityPrice(BusinessPriceChangeDTO businessPriceChangeDTO, LoginUser loginUser) {
        ProdBusiness entity = BeanCreate.newBean(ProdBusiness.class);
        entity.setUpdateVersion(businessPriceChangeDTO.getUpdateVersion() + 1);
        entity.setPrice(businessPriceChangeDTO.getPrice());
        DataOPUtil.editUser(entity, loginUser);
        ProdBusiness query = BeanCreate.newBean(ProdBusiness.class);
        query.setBusinessCode(businessPriceChangeDTO.getBusinessCode());
        entity.setUpdateVersion(businessPriceChangeDTO.getUpdateVersion());
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return update(entity, queryWrapper);
    }

    public Boolean finishOrderByOrderNo(String orderCode, LoginUser loginUser) {
        ProdBusiness entity = new ProdBusiness();
        entity.setStatus(YesOrNoEnum.YES.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdBusiness query = new ProdBusiness();
        query.setOrderNo(orderCode);
        query.setBusinessType(BusinessTypeEnum.OUT.getValue());
        query.setOrderStatus(BusinessStatusEnum.FINISH.getValue());
        query.setStatus(YesOrNoEnum.NO.getValue());
        UpdateWrapper<ProdBusiness> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    public List<ProdBusiness> queryBusinessByBIllNo(String billNo, LoginUser loginUser) {
        ProdBusiness entity = new ProdBusiness();
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setBillNo(billNo);
        QueryWrapper<ProdBusiness> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }
}
