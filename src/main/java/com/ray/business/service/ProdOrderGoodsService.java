package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.dto.OrderGoodsQueryDTO;
import com.ray.business.table.entity.ProdOrderGoods;
import com.ray.business.table.mapper.ProdOrderGoodsMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 订单-商品 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdOrderGoodsService extends ServiceImpl<ProdOrderGoodsMapper, ProdOrderGoods> {




    /**
     * 编辑订单商品
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdOrderGoods entity, LoginUser loginUser) {
        ProdOrderGoods query = new ProdOrderGoods();
        query.setCode(entity.getCode());
        UpdateWrapper<ProdOrderGoods> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询订单商品
     *
     * @param code      订单商品编码
     * @param loginUser 当前操作员
     * @return
     */
    public ProdOrderGoods queryOrderGoodsByOrderCode(String code, LoginUser loginUser) {
        ProdOrderGoods query = new ProdOrderGoods();
        query.setCode(code);
        QueryWrapper<ProdOrderGoods> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }


    /**
     * 查询订单商品
     *
     * @param goodsCode 商品
     * @param loginUser 当前操作员
     * @return
     */
    public ProdOrderGoods queryOrderGoodsByGoodsCode(String orderNo, String goodsCode, LoginUser loginUser) {
        ProdOrderGoods query = new ProdOrderGoods();
        query.setOrderNo(orderNo);
        query.setGoodsCode(goodsCode);
        QueryWrapper<ProdOrderGoods> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdOrderGoods> page(CommonPage<OrderGoodsQueryDTO, Page<ProdOrderGoods>> queryParams, LoginUser loginUser) {
        OrderGoodsQueryDTO params = queryParams.getEntity();
        ProdOrderGoods entity = BeanCreate.newBean(ProdOrderGoods.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdOrderGoods> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<ProdOrderGoods> list(OrderGoodsQueryDTO queryParams, LoginUser loginUser) {
        ProdOrderGoods entity = BeanCreate.newBean(ProdOrderGoods.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<ProdOrderGoods> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param orderNo
     * @param loginUser
     * @return
     */
    public List<ProdOrderGoods> list(String orderNo, LoginUser loginUser) {
        ProdOrderGoods entity = BeanCreate.newBean(ProdOrderGoods.class);
        entity.setOrderNo(orderNo);
        QueryWrapper<ProdOrderGoods> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 删除订单商品
     *
     * @param orderNo
     * @param loginUser
     * @return
     */
    public Boolean deleteOrderGoods(String orderNo, LoginUser loginUser) {
        ProdOrderGoods entity = new ProdOrderGoods();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdOrderGoods query = new ProdOrderGoods();
        query.setOrderNo(orderNo);
        UpdateWrapper<ProdOrderGoods> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 更新完成数量
     * @param orderNo
     * @param goodCode
     * @param finishQuantity
     * @param updateVersion
     * @param loginUser
     * @return
     */
    public Boolean updateFinishQuantity(String orderNo, String goodCode, BigDecimal finishQuantity, Integer finishTotal, Integer updateVersion, LoginUser loginUser) {
        ProdOrderGoods entity = new ProdOrderGoods();
        entity.setFinishQuantity(finishQuantity);
        entity.setTotal(finishTotal);
        entity.setUpdateVersion(updateVersion + 1);
        DataOPUtil.editUser(entity, loginUser);
        ProdOrderGoods query = new ProdOrderGoods();
        query.setOrderNo(orderNo);
        query.setGoodsCode(goodCode);
        query.setUpdateVersion(updateVersion);
        UpdateWrapper<ProdOrderGoods> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 更新发货数量
     * @param orderNo
     * @param goodCode
     * @param sendQuantity
     * @param updateVersion
     * @param loginUser
     * @return
     */
    public Boolean updateSendQuantity(String orderNo, String goodCode, BigDecimal sendQuantity, Integer updateVersion, LoginUser loginUser) {
        ProdOrderGoods entity = new ProdOrderGoods();
        entity.setSendQuantity(sendQuantity);
        entity.setUpdateVersion(updateVersion + 1);
        DataOPUtil.editUser(entity, loginUser);
        ProdOrderGoods query = new ProdOrderGoods();
        query.setOrderNo(orderNo);
        query.setGoodsCode(goodCode);
        query.setUpdateVersion(updateVersion);
        UpdateWrapper<ProdOrderGoods> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }
}
