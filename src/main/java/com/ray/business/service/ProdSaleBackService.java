package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.enums.PurchaseStatusEnum;
import com.ray.business.enums.SaleStatusEnum;
import com.ray.business.table.dto.SaleBackQueryDTO;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.business.table.entity.ProdPurchaseBack;
import com.ray.business.table.entity.ProdSaleBack;
import com.ray.business.table.mapper.ProdSaleBackMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 销售订单-退货 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdSaleBackService extends ServiceImpl<ProdSaleBackMapper, ProdSaleBack> {
    /**
     * 查询 不是已取消的退货单
     *
     * @param orderNo
     * @param loginUser
     * @return
     */
    public int countBack(String orderNo, LoginUser loginUser) {
        ProdSaleBack query = new ProdSaleBack();
        query.setOrderNo(orderNo);
        QueryWrapper<ProdSaleBack> queryWrapper = new QueryWrapper<>(query);
        queryWrapper.ne("order_status", SaleStatusEnum.CANCEL.getValue());
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return count(queryWrapper);
    }

    /**
     * 编辑订单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdSaleBack entity, LoginUser loginUser) {
        ProdSaleBack query = new ProdSaleBack();
        query.setBackCode(entity.getBackCode());
        UpdateWrapper<ProdSaleBack> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询订单
     *
     * @param backCode  订单编码
     * @param loginUser 当前操作员
     * @return
     */
    public ProdSaleBack querySaleBackBySaleBackCode(String backCode, LoginUser loginUser) {
        ProdSaleBack query = new ProdSaleBack();
        query.setBackCode(backCode);
        QueryWrapper<ProdSaleBack> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }


    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdSaleBack> page(CommonPage<SaleBackQueryDTO, Page<ProdSaleBack>> queryParams, LoginUser loginUser) {
        SaleBackQueryDTO params = queryParams.getEntity();
        ProdSaleBack entity = BeanCreate.newBean(ProdSaleBack.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdSaleBack> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getBackCodeLike()), "back_code", params.getBackCodeLike());
            queryWrapper.gt(ObjectUtil.isNotNull(params.getStartTime()), "create_time", params.getStartTime());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getEndTime()), "create_time", params.getEndTime());
        }
        queryWrapper.in(StrUtil.isBlank(params.getOrderStatus()) && ObjectUtil.isNotEmpty(params.getIncludeStatus()),
                "order_status", params.getIncludeStatus());
        queryWrapper.orderByDesc("create_time");
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<ProdSaleBack> list(SaleBackQueryDTO queryParams, LoginUser loginUser) {
        ProdSaleBack entity = BeanCreate.newBean(ProdSaleBack.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<ProdSaleBack> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getBackCodeLike()), "back_code", queryParams.getBackCodeLike());
        return list(queryWrapper);
    }

    /**
     * 完成订单
     *
     * @param businessCode
     * @param loginUser
     * @return
     */
    public Boolean finishOrder(String businessCode, LoginUser loginUser) {
        ProdSaleBack entity = new ProdSaleBack();
        entity.setOrderStatus(PurchaseStatusEnum.FINISH.getValue());
        entity.setInTime(LocalDateTime.now());
        DataOPUtil.editUser(entity, loginUser);
        ProdSaleBack query = new ProdSaleBack();
        query.setBackCode(businessCode);
        UpdateWrapper<ProdSaleBack> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询采购单对于
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public List<ProdSaleBack> listByOrderNos(List<String> orderNos, List<String> orderStatus, LoginUser loginUser) {
        ProdSaleBack entity = BeanCreate.newBean(ProdSaleBack.class);
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        QueryWrapper<ProdSaleBack> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        queryWrapper.in(ObjectUtil.isNotNull(orderStatus), "order_status", orderStatus);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询采购单数量
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public Integer countByOrderNos(List<String> orderNos, List<String> orderStatus, LoginUser loginUser) {
        ProdSaleBack entity = BeanCreate.newBean(ProdSaleBack.class);
        QueryWrapper<ProdSaleBack> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        queryWrapper.in(ObjectUtil.isNotNull(orderStatus), "order_status", orderStatus);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return count(queryWrapper);
    }

    /**
     * 确认对账
     *
     * @param businessCodes
     * @param loginUser
     * @return
     */
    public Boolean comfireBill(List<String> businessCodes, String billNo, LoginUser loginUser) {
        ProdSaleBack entity = new ProdSaleBack();
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setBillNo(billNo);
        DataOPUtil.editUser(entity, loginUser);
        ProdSaleBack query = new ProdSaleBack();
        query.setBillStatus(YesOrNoEnum.NO.getValue());
        UpdateWrapper<ProdSaleBack> updateWrapper = new UpdateWrapper<>(query);
        updateWrapper.in("order_no", businessCodes);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 取消对账
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public Boolean cancelBill(String billNo, LoginUser loginUser) {
        ProdSaleBack entity = new ProdSaleBack();
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        entity.setBillNo("");
        DataOPUtil.editUser(entity, loginUser);
        ProdSaleBack query = new ProdSaleBack();
        query.setBillNo(billNo);
        query.setBillStatus(YesOrNoEnum.YES.getValue());
        UpdateWrapper<ProdSaleBack> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        if(count(updateWrapper)== 0){
            return  true;
        }
        return update(entity, updateWrapper);
    }

    /**
     * 查询列表数据
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public List<ProdSaleBack> listByBillNo(String billNo, String orderStatus, LoginUser loginUser) {
        ProdSaleBack entity = BeanCreate.newBean(ProdSaleBack.class);
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setBillNo(billNo);
        entity.setOrderStatus(orderStatus);
        QueryWrapper<ProdSaleBack> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 统计数量
     * @param orderNos
     * @param loginUser
     * @return
     */
    public Integer countBill(List<String> orderNos, LoginUser loginUser) {
        ProdSaleBack entity = BeanCreate.newBean(ProdSaleBack.class);
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        QueryWrapper<ProdSaleBack> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return count(queryWrapper);
    }
}
