package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.dto.OrderQueryDTO;
import com.ray.business.table.entity.ProdOrder;
import com.ray.business.table.mapper.ProdOrderMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 生产订单 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdOrderService extends ServiceImpl<ProdOrderMapper, ProdOrder>  {
    /**
     * 编辑订单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdOrder entity, LoginUser loginUser) {
        ProdOrder query = new ProdOrder();
        query.setOrderNo(entity.getOrderNo());
        UpdateWrapper<ProdOrder> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询订单
     *
     * @param orderNo 订单编码
     * @param loginUser    当前操作员
     * @return
     */
    public ProdOrder queryOrderByOrderCode(String orderNo, LoginUser loginUser) {
        ProdOrder query = new ProdOrder();
        query.setOrderNo(orderNo);
        QueryWrapper<ProdOrder> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 查询订单
     *
     * @param orderName 订单编码
     * @param loginUser    当前操作员
     * @return
     */
    public ProdOrder queryOrderByOrderName(String orderName, LoginUser loginUser) {
        ProdOrder query = new ProdOrder();
        query.setOrderName(orderName);
        QueryWrapper<ProdOrder> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdOrder> page(CommonPage<OrderQueryDTO, Page<ProdOrder>> queryParams, LoginUser loginUser) {
        OrderQueryDTO params = queryParams.getEntity();
        ProdOrder entity = BeanCreate.newBean(ProdOrder.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdOrder> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.like(StrUtil.isNotBlank(params.getOrderNoLike()), "order_no", params.getOrderNoLike());
            queryWrapper.gt(ObjectUtil.isNotNull(params.getStartTime()),"create_time",params.getStartTime());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getEndTime()),"create_time",params.getEndTime());
        }
        queryWrapper.in(StrUtil.isBlank(params.getOrderStatus()) && ObjectUtil.isNotEmpty(params.getIncludeStatus()),
                "order_status", params.getIncludeStatus());
        queryWrapper.orderByDesc("create_time");
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<ProdOrder> list(OrderQueryDTO queryParams, LoginUser loginUser) {
        ProdOrder entity = BeanCreate.newBean(ProdOrder.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<ProdOrder> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getOrderNoLike()), "order_no", queryParams.getOrderNoLike());
        return list(queryWrapper);
    }


    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<ProdOrder> listMy(OrderQueryDTO queryParams, LoginUser loginUser) {
        ProdOrder entity = BeanCreate.newBean(ProdOrder.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<ProdOrder> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getOrderNoLike()), "order_no", queryParams.getOrderNoLike());
        return list(queryWrapper);
    }

    public String queryOrderCurrency(Set<String> businessOrderNos, LoginUser loginUser) {
        ProdOrder entity = BeanCreate.newBean(ProdOrder.class);
        QueryWrapper<ProdOrder> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no",businessOrderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.groupBy("currency");
        List<ProdOrder> prodOrders  =  list(queryWrapper);
        if(ObjectUtil.isEmpty(prodOrders)){
            throw BusinessExceptionFactory.newException("订单不存在");
        }
        if(prodOrders.size() > 1){
            throw BusinessExceptionFactory.newException("货币类型不一致");
        }
        return  prodOrders.get(0).getCurrency();
    }
}
