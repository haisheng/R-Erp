package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.enums.SaleStatusEnum;
import com.ray.business.table.dto.PurchaseBackQueryDTO;
import com.ray.business.table.entity.ProdPurchaseBack;
import com.ray.business.table.mapper.ProdPurchaseBackMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 采购订单-退货 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdPurchaseBackService extends ServiceImpl<ProdPurchaseBackMapper, ProdPurchaseBack> {

    /**
     * 查询 不是已取消的退货单
     *
     * @param orderNo
     * @param loginUser
     * @return
     */
    public int countBack(String orderNo, LoginUser loginUser) {
        ProdPurchaseBack query = new ProdPurchaseBack();
        query.setOrderNo(orderNo);
        QueryWrapper<ProdPurchaseBack> queryWrapper = new QueryWrapper<>(query);
        queryWrapper.ne("order_status", SaleStatusEnum.CANCEL.getValue());
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return count(queryWrapper);
    }

    /**
     * 编辑订单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdPurchaseBack entity, LoginUser loginUser) {
        ProdPurchaseBack query = new ProdPurchaseBack();
        query.setBackCode(entity.getBackCode());
        UpdateWrapper<ProdPurchaseBack> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询订单
     *
     * @param backCode  订单编码
     * @param loginUser 当前操作员
     * @return
     */
    public ProdPurchaseBack queryPurchaseBackByPurchaseBackCode(String backCode, LoginUser loginUser) {
        ProdPurchaseBack query = new ProdPurchaseBack();
        query.setBackCode(backCode);
        QueryWrapper<ProdPurchaseBack> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }


    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdPurchaseBack> page(CommonPage<PurchaseBackQueryDTO, Page<ProdPurchaseBack>> queryParams, LoginUser loginUser) {
        PurchaseBackQueryDTO params = queryParams.getEntity();
        ProdPurchaseBack entity = BeanCreate.newBean(ProdPurchaseBack.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdPurchaseBack> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getBackCodeLike()), "back_code", params.getBackCodeLike());
            queryWrapper.gt(ObjectUtil.isNotNull(params.getStartTime()), "create_time", params.getStartTime());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getEndTime()), "create_time", params.getEndTime());
        }
        queryWrapper.in(StrUtil.isBlank(params.getOrderStatus()) && ObjectUtil.isNotEmpty(params.getIncludeStatus()),
                "order_status", params.getIncludeStatus());
        queryWrapper.orderByDesc("create_time");
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseBack> list(PurchaseBackQueryDTO queryParams, LoginUser loginUser) {
        ProdPurchaseBack entity = BeanCreate.newBean(ProdPurchaseBack.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<ProdPurchaseBack> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getBackCodeLike()), "back_code", queryParams.getBackCodeLike());
        return list(queryWrapper);
    }

    /**
     * 完成订单
     *
     * @param businessCode
     * @param loginUser
     * @return
     */
    public Boolean finishOrder(String businessCode, LoginUser loginUser) {
        ProdPurchaseBack entity = new ProdPurchaseBack();
        entity.setOrderStatus(SaleStatusEnum.FINISH.getValue());
        entity.setOutTime(LocalDateTime.now());
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchaseBack query = new ProdPurchaseBack();
        query.setBackCode(businessCode);
        UpdateWrapper<ProdPurchaseBack> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询采购单对于
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseBack> listByOrderNos(List<String> orderNos, List<String> orderStatus, LoginUser loginUser) {
        ProdPurchaseBack entity = BeanCreate.newBean(ProdPurchaseBack.class);
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        QueryWrapper<ProdPurchaseBack> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        queryWrapper.in(ObjectUtil.isNotNull(orderStatus), "order_status", orderStatus);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询采购单数量
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public Integer countByOrderNos(List<String> orderNos, List<String> orderStatus, LoginUser loginUser) {
        ProdPurchaseBack entity = BeanCreate.newBean(ProdPurchaseBack.class);
        QueryWrapper<ProdPurchaseBack> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        queryWrapper.in(ObjectUtil.isNotNull(orderStatus), "order_status", orderStatus);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return count(queryWrapper);
    }

    /**
     * 确认对账
     *
     * @param businessCodes
     * @param loginUser
     * @return
     */
    public Boolean comfireBill(List<String> businessCodes, String billNo, LoginUser loginUser) {
        ProdPurchaseBack entity = new ProdPurchaseBack();
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setBillNo(billNo);
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchaseBack query = new ProdPurchaseBack();
        query.setBillStatus(YesOrNoEnum.NO.getValue());
        UpdateWrapper<ProdPurchaseBack> updateWrapper = new UpdateWrapper<>(query);
        updateWrapper.in("order_no", businessCodes);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 取消对账
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public Boolean cancelBill(String billNo, LoginUser loginUser) {
        ProdPurchaseBack entity = new ProdPurchaseBack();
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        entity.setBillNo("");
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchaseBack query = new ProdPurchaseBack();
        query.setBillStatus(YesOrNoEnum.YES.getValue());
        query.setBillNo(billNo);
        UpdateWrapper<ProdPurchaseBack> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        if (count(updateWrapper) == 0) {
            return true;
        }
        return update(entity, updateWrapper);
    }

    /**
     * 查询列表数据
     *
     * @param billNo
     * @param orderStatus
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseBack> listByBillNo(String billNo, String orderStatus, LoginUser loginUser) {
        ProdPurchaseBack entity = BeanCreate.newBean(ProdPurchaseBack.class);
        entity.setBillNo(billNo);
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setOrderStatus(orderStatus);
        QueryWrapper<ProdPurchaseBack> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询的退货单
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseBack> listFinishByOrderNos(List<String> orderNos, LoginUser loginUser) {
        ProdPurchaseBack entity = BeanCreate.newBean(ProdPurchaseBack.class);
        entity.setOrderStatus(SaleStatusEnum.FINISH.getValue());
        QueryWrapper<ProdPurchaseBack> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 未对账
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public Integer countBill(List<String> orderNos, LoginUser loginUser) {
        ProdPurchaseBack entity = BeanCreate.newBean(ProdPurchaseBack.class);
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        QueryWrapper<ProdPurchaseBack> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return count(queryWrapper);
    }

    public Boolean updateTotalAmount(Long id, BigDecimal total, LoginUser loginUser) {
        ProdPurchaseBack entity = new ProdPurchaseBack();
        entity.setId(id);
        entity.setTotalAmount(total);
        DataOPUtil.editUser(entity, loginUser);
        return updateById(entity);
    }
}
