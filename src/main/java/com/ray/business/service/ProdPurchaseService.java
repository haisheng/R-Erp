package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.enums.PurchaseStatusEnum;
import com.ray.business.table.dto.PurchaseQueryDTO;
import com.ray.business.table.dto.UpdateQuantityDTO;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.business.table.mapper.ProdPurchaseMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 采购订单 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdPurchaseService extends ServiceImpl<ProdPurchaseMapper, ProdPurchase> {

    /**
     * 编辑订单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdPurchase entity, LoginUser loginUser) {
        ProdPurchase query = new ProdPurchase();
        query.setOrderNo(entity.getOrderNo());
        UpdateWrapper<ProdPurchase> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询订单
     *
     * @param orderNo   订单编码
     * @param loginUser 当前操作员
     * @return
     */
    public ProdPurchase queryPurchaseByPurchaseCode(String orderNo, LoginUser loginUser) {
        ProdPurchase query = new ProdPurchase();
        query.setOrderNo(orderNo);
        QueryWrapper<ProdPurchase> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }


    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdPurchase> page(CommonPage<PurchaseQueryDTO, Page<ProdPurchase>> queryParams, LoginUser loginUser) {
        PurchaseQueryDTO params = queryParams.getEntity();
        ProdPurchase entity = BeanCreate.newBean(ProdPurchase.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdPurchase> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.like(StrUtil.isNotBlank(params.getOrderNoLike()), "order_no", params.getOrderNoLike());
            queryWrapper.gt(ObjectUtil.isNotNull(params.getStartTime()), "create_time", params.getStartTime());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getEndTime()), "create_time", params.getEndTime());
        }
        queryWrapper.in(StrUtil.isBlank(params.getOrderStatus()) && ObjectUtil.isNotEmpty(params.getIncludeStatus()),
                "order_status", params.getIncludeStatus());
        queryWrapper.in(ObjectUtil.isNotEmpty(params.getOrderNos()), "order_no", params.getOrderNos());
        queryWrapper.orderByDesc("create_time");
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<ProdPurchase> list(PurchaseQueryDTO queryParams, LoginUser loginUser) {
        ProdPurchase entity = BeanCreate.newBean(ProdPurchase.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<ProdPurchase> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getOrderNoLike()), "order_no", queryParams.getOrderNoLike());
        return list(queryWrapper);
    }


    /**
     * 完成订单
     *
     * @param businessCode
     * @param loginUser
     * @return
     */
    public Boolean finishOrder(String businessCode, LoginUser loginUser) {
        ProdPurchase entity = new ProdPurchase();
        entity.setOrderStatus(PurchaseStatusEnum.FINISH.getValue());
        entity.setSendTime(LocalDateTime.now());
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchase query = new ProdPurchase();
        query.setOrderNo(businessCode);
        UpdateWrapper<ProdPurchase> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 通过业务订单号查询
     *
     * @param orderNo
     * @param loginUser
     * @return
     */
    public List<ProdPurchase> listByBusinessOrderNo(String orderNo, LoginUser loginUser) {
        ProdPurchase entity = BeanCreate.newBean(ProdPurchase.class);
        entity.setBusinessOrderNo(orderNo);
        QueryWrapper<ProdPurchase> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询采购单列表
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public List<ProdPurchase> listByOrderNos(List<String> orderNos, LoginUser loginUser) {
        ProdPurchase entity = BeanCreate.newBean(ProdPurchase.class);
        QueryWrapper<ProdPurchase> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 确认对账
     *
     * @param businessCodes
     * @param loginUser
     * @return
     */
    public Boolean comfireBill(List<String> businessCodes, String billNo, LoginUser loginUser) {
        ProdPurchase entity = new ProdPurchase();
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setBillNo(billNo);
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchase query = new ProdPurchase();
        query.setBillStatus(YesOrNoEnum.NO.getValue());
        UpdateWrapper<ProdPurchase> updateWrapper = new UpdateWrapper<>(query);
        updateWrapper.in("order_no", businessCodes);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 取消对账
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public Boolean cancelBill(String billNo, LoginUser loginUser) {
        ProdPurchase entity = new ProdPurchase();
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        entity.setBillNo("");
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchase query = new ProdPurchase();
        query.setBillStatus(YesOrNoEnum.YES.getValue());
        query.setBillNo(billNo);
        UpdateWrapper<ProdPurchase> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 通过对账单查询
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public List<ProdPurchase> queryPurchaseByBIllNo(String billNo, LoginUser loginUser) {
        ProdPurchase entity = BeanCreate.newBean(ProdPurchase.class);
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setBillNo(billNo);
        QueryWrapper<ProdPurchase> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 更新数量
     *
     * @param updateQuantityDTO
     * @param loginUser
     * @return
     */
    public boolean updateFinishQuantity(UpdateQuantityDTO updateQuantityDTO, LoginUser loginUser) {
        ProdPurchase entity = new ProdPurchase();
        entity.setFinishQuantity(updateQuantityDTO.getQuantity());
        entity.setTotalAmount(updateQuantityDTO.getAmount());
        entity.setTotal(updateQuantityDTO.getTotal());
        entity.setUpdateVersion(updateQuantityDTO.getUpdateVersion() + 1);
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchase query = new ProdPurchase();
        query.setOrderNo(updateQuantityDTO.getCode());
        query.setUpdateVersion(updateQuantityDTO.getUpdateVersion());
        UpdateWrapper<ProdPurchase> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    public String queryOrderCurrency(List<String> businessOrderNos, LoginUser loginUser) {
        ProdPurchase entity = BeanCreate.newBean(ProdPurchase.class);
        QueryWrapper<ProdPurchase> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", businessOrderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.groupBy("currency");
        List<ProdPurchase> prodOrders = list(queryWrapper);
        if (ObjectUtil.isEmpty(prodOrders)) {
            throw BusinessExceptionFactory.newException("订单不存在");
        }
        if (prodOrders.size() > 1) {
            throw BusinessExceptionFactory.newException("货币类型不一致");
        }
        return prodOrders.get(0).getCurrency();
    }

    public Boolean updateTotalAmount(Long id, BigDecimal total, LoginUser loginUser) {
        ProdPurchase entity = new ProdPurchase();
        entity.setId(id);
        entity.setTotalAmount(total);
        DataOPUtil.editUser(entity, loginUser);
        return updateById(entity);
    }

}
