package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.dto.PurchaseInQueryDTO;
import com.ray.business.table.dto.PurchaseInRecordQueryDTO;
import com.ray.business.table.entity.ProdPurchaseInRecord;
import com.ray.business.table.mapper.ProdPurchaseInRecordMapper;
import com.ray.business.table.params.purchase.PurchaseGoodsParams;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 采购退回记录明细 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-27
 */
@Service
public class ProdPurchaseInRecordService extends ServiceImpl<ProdPurchaseInRecordMapper, ProdPurchaseInRecord> {

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdPurchaseInRecord> pageRecord(CommonPage<PurchaseInRecordQueryDTO, Page<ProdPurchaseInRecord>> queryParams, LoginUser loginUser) {
        PurchaseInRecordQueryDTO params = queryParams.getEntity();
        ProdPurchaseInRecord entity = BeanCreate.newBean(ProdPurchaseInRecord.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdPurchaseInRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.gt(ObjectUtil.isNotNull(params.getStartTime()), "create_time", params.getStartTime());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getEndTime()), "create_time", params.getEndTime());
        }
        queryWrapper.orderByDesc("create_time");
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 删除订单商品
     *
     * @param inCode
     * @param loginUser
     * @return
     */
    public Boolean deleteOrderGoods(String inCode, LoginUser loginUser) {
        ProdPurchaseInRecord entity = new ProdPurchaseInRecord();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchaseInRecord query = new ProdPurchaseInRecord();
        query.setInCode(inCode);
        UpdateWrapper<ProdPurchaseInRecord> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 查询
     *
     * @param inCode
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseInRecord> list(String inCode, LoginUser loginUser) {
        ProdPurchaseInRecord entity = BeanCreate.newBean(ProdPurchaseInRecord.class);
        entity.setInCode(inCode);
        QueryWrapper<ProdPurchaseInRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }


    /**
     * 查询
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseInRecord> list(List<String> orderNos, List<String> inCodes, List<String> modelCodes, LoginUser loginUser) {
        ProdPurchaseInRecord entity = BeanCreate.newBean(ProdPurchaseInRecord.class);
        QueryWrapper<ProdPurchaseInRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        queryWrapper.in(ObjectUtil.isNotEmpty(inCodes), "in_code", inCodes);
        queryWrapper.in(ObjectUtil.isNotEmpty(modelCodes), "goods_code", modelCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询
     *
     * @param orderNo
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseInRecord> listByOrderNo(String orderNo, List<String> inCodes, LoginUser loginUser) {
        ProdPurchaseInRecord entity = BeanCreate.newBean(ProdPurchaseInRecord.class);
        entity.setOrderNo(orderNo);
        QueryWrapper<ProdPurchaseInRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in(ObjectUtil.isNotEmpty(inCodes), "in_code", inCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询
     *
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseInRecord> listByInCodes(List<String> inCodes, LoginUser loginUser) {
        ProdPurchaseInRecord entity = BeanCreate.newBean(ProdPurchaseInRecord.class);
        QueryWrapper<ProdPurchaseInRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in(ObjectUtil.isNotEmpty(inCodes), "in_code", inCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdPurchaseInRecord> page(CommonPage<PurchaseInQueryDTO, Page<ProdPurchaseInRecord>> queryParams, LoginUser loginUser) {
        PurchaseInQueryDTO params = queryParams.getEntity();
        ProdPurchaseInRecord entity = BeanCreate.newBean(ProdPurchaseInRecord.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdPurchaseInRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getInCodeLike()), "in_code", params.getInCodeLike());
        }
        queryWrapper.orderByDesc("create_time");
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 查询已退回数量
     *
     * @param inCode
     * @param purchaseGoodsParams
     * @param loginUser
     * @return
     */
    public BigDecimal queryBackQuantity(String orderNo, String inCode, PurchaseGoodsParams purchaseGoodsParams, LoginUser loginUser) {
        ProdPurchaseInRecord entity = BeanCreate.newBean(ProdPurchaseInRecord.class);
        entity.setOrderNo(orderNo);
        entity.setBatchNo(purchaseGoodsParams.getBatchNo());
        entity.setGoodsCode(purchaseGoodsParams.getGoodsCode());
        entity.setSerialNumber(purchaseGoodsParams.getSerialNumber());
        QueryWrapper<ProdPurchaseInRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.ne(StrUtil.isNotBlank(inCode), "in_code", inCode);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper).stream().map(ProdPurchaseInRecord::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * 查询已退回数量
     *
     * @param recordCode
     * @param loginUser
     * @return
     */
    public BigDecimal queryBackQuantity(String recordCode, List<String> inCodes, LoginUser loginUser) {
        if (ObjectUtil.isEmpty(inCodes)) {
            return new BigDecimal(0);
        }
        ProdPurchaseInRecord entity = BeanCreate.newBean(ProdPurchaseInRecord.class);
        entity.setRecordCode(recordCode);
        QueryWrapper<ProdPurchaseInRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.in("in_code", inCodes);
        return list(queryWrapper).stream().map(ProdPurchaseInRecord::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public List<ProdPurchaseInRecord> list(String orderNo, String goodsCode, LoginUser loginUser) {
        ProdPurchaseInRecord entity = BeanCreate.newBean(ProdPurchaseInRecord.class);
        entity.setOrderNo(orderNo);
        entity.setGoodsCode(goodsCode);
        QueryWrapper<ProdPurchaseInRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    public Boolean updatePrice(Long id, BigDecimal price, LoginUser loginUser) {
        ProdPurchaseInRecord entity = new ProdPurchaseInRecord();
        entity.setId(id);
        entity.setPrice(price);
        DataOPUtil.editUser(entity, loginUser);
        return updateById(entity);
    }

    public List<ProdPurchaseInRecord> listByOrderNo(List<String> orderNos, LoginUser loginUser) {
        ProdPurchaseInRecord entity = BeanCreate.newBean(ProdPurchaseInRecord.class);
        QueryWrapper<ProdPurchaseInRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in(ObjectUtil.isNotEmpty(orderNos), "order_no", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    public Boolean cancelOrderGoods(String inCode, LoginUser loginUser) {
        ProdPurchaseInRecord entity = new ProdPurchaseInRecord();
        entity.setStatus(YesOrNoEnum.NO.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchaseInRecord query = new ProdPurchaseInRecord();
        query.setInCode(inCode);
        UpdateWrapper<ProdPurchaseInRecord> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }
}
