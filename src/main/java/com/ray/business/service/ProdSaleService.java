package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.enums.SaleStatusEnum;
import com.ray.business.table.dto.SaleQueryDTO;
import com.ray.business.table.dto.UpdateQuantityDTO;
import com.ray.business.table.entity.ProdSale;
import com.ray.business.table.mapper.ProdSaleMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 销售订单 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdSaleService extends ServiceImpl<ProdSaleMapper, ProdSale> {


    /**
     * 编辑订单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdSale entity, LoginUser loginUser) {
        ProdSale query = new ProdSale();
        query.setOrderNo(entity.getOrderNo());
        UpdateWrapper<ProdSale> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询订单
     *
     * @param orderNo   订单编码
     * @param loginUser 当前操作员
     * @return
     */
    public ProdSale querySaleBySaleCode(String orderNo, LoginUser loginUser) {
        ProdSale query = new ProdSale();
        query.setOrderNo(orderNo);
        QueryWrapper<ProdSale> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }


    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdSale> page(CommonPage<SaleQueryDTO, Page<ProdSale>> queryParams, LoginUser loginUser) {
        SaleQueryDTO params = queryParams.getEntity();
        ProdSale entity = BeanCreate.newBean(ProdSale.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdSale> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.like(StrUtil.isNotBlank(params.getOrderNoLike()), "order_no", params.getOrderNoLike());
            queryWrapper.gt(ObjectUtil.isNotNull(params.getStartTime()), "create_time", params.getStartTime());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getEndTime()), "create_time", params.getEndTime());
        }
        queryWrapper.in(StrUtil.isBlank(params.getOrderStatus()) && ObjectUtil.isNotEmpty(params.getIncludeStatus()),
                "order_status", params.getIncludeStatus());
        queryWrapper.orderByDesc("create_time");
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<ProdSale> list(SaleQueryDTO queryParams, LoginUser loginUser) {
        ProdSale entity = BeanCreate.newBean(ProdSale.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<ProdSale> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getOrderNoLike()), "order_no", queryParams.getOrderNoLike());
        return list(queryWrapper);
    }


    /**
     * 完成订单
     *
     * @param businessCode
     * @param loginUser
     * @return
     */
    public Boolean finishOrder(String businessCode, LoginUser loginUser) {
        ProdSale entity = new ProdSale();
        entity.setOrderStatus(SaleStatusEnum.FINISH.getValue());
        entity.setSendTime(LocalDateTime.now());
        DataOPUtil.editUser(entity, loginUser);
        ProdSale query = new ProdSale();
        query.setOrderNo(businessCode);
        UpdateWrapper<ProdSale> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询销售单列表
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public List<ProdSale> listByOrderNos(List<String> orderNos, LoginUser loginUser) {
        ProdSale entity = BeanCreate.newBean(ProdSale.class);
        QueryWrapper<ProdSale> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 确认对账
     *
     * @param businessCodes
     * @param loginUser
     * @return
     */
    public Boolean comfireBill(List<String> businessCodes, String billNo, LoginUser loginUser) {
        ProdSale entity = new ProdSale();
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setBillNo(billNo);
        DataOPUtil.editUser(entity, loginUser);
        ProdSale query = new ProdSale();
        query.setBillStatus(YesOrNoEnum.NO.getValue());
        UpdateWrapper<ProdSale> updateWrapper = new UpdateWrapper<>(query);
        updateWrapper.in("order_no", businessCodes);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 取消对账
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public Boolean cancelBill(String billNo, LoginUser loginUser) {
        ProdSale entity = new ProdSale();
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        entity.setBillNo("");
        DataOPUtil.editUser(entity, loginUser);
        ProdSale query = new ProdSale();
        query.setBillNo(billNo);
        query.setBillStatus(YesOrNoEnum.YES.getValue());
        UpdateWrapper<ProdSale> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);

        return update(entity, updateWrapper);
    }

    public List<ProdSale> listByBillNo(String billNo, LoginUser loginUser) {
        ProdSale entity = BeanCreate.newBean(ProdSale.class);
        entity.setBillNo(billNo);
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<ProdSale> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 更新数量
     *
     * @param updateQuantityDTO
     * @param loginUser
     * @return
     */
    public boolean updateFinishQuantity(UpdateQuantityDTO updateQuantityDTO, LoginUser loginUser) {
        ProdSale entity = new ProdSale();
        entity.setFinishQuantity(updateQuantityDTO.getQuantity());
        entity.setTotalAmount(updateQuantityDTO.getAmount());
        entity.setTotal(updateQuantityDTO.getTotal());
        entity.setUpdateVersion(updateQuantityDTO.getUpdateVersion() + 1);
        DataOPUtil.editUser(entity, loginUser);
        ProdSale query = new ProdSale();
        query.setOrderNo(updateQuantityDTO.getCode());
        query.setUpdateVersion(updateQuantityDTO.getUpdateVersion());
        UpdateWrapper<ProdSale> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }


    public String queryOrderCurrency(List<String> businessOrderNos, LoginUser loginUser) {
        ProdSale entity = BeanCreate.newBean(ProdSale.class);
        QueryWrapper<ProdSale> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", businessOrderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.groupBy("currency");
        List<ProdSale> prodOrders = list(queryWrapper);
        if (ObjectUtil.isEmpty(prodOrders)) {
            throw BusinessExceptionFactory.newException("订单不存在");
        }
        if (prodOrders.size() > 1) {
            throw BusinessExceptionFactory.newException("货币类型不一致");
        }
        return prodOrders.get(0).getCurrency();
    }

    public List<ProdSale> querySaleByBIllNo(String billNo, LoginUser loginUser) {
        ProdSale entity = BeanCreate.newBean(ProdSale.class);
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setBillNo(billNo);
        QueryWrapper<ProdSale> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }
}
