package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.entity.ProdBusinessIn;
import com.ray.business.table.entity.ProdBusinessIn;
import com.ray.business.table.entity.ProdBusinessOut;
import com.ray.business.table.mapper.ProdBusinessInMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.util.Assert;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 加工单物料明细 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdBusinessInService extends ServiceImpl<ProdBusinessInMapper, ProdBusinessIn> {

    /**
     * 查询
     *
     * @param businessCodes
     * @param loginUser
     * @return
     */
    public List<ProdBusinessIn> list(List<String> businessCodes, LoginUser loginUser) {
        Assert.isTrue(ObjectUtil.isNotEmpty(businessCodes), "无加工单数据");
        ProdBusinessIn entity = BeanCreate.newBean(ProdBusinessIn.class);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<ProdBusinessIn> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("business_code", businessCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询
     *
     * @param businessCodes
     * @param loginUser
     * @return
     */
    public List<ProdBusinessIn> listAll(List<String> businessCodes, LoginUser loginUser) {
        Assert.isTrue(ObjectUtil.isNotEmpty(businessCodes), "无加工单数据");
        ProdBusinessIn entity = BeanCreate.newBean(ProdBusinessIn.class);
        QueryWrapper<ProdBusinessIn> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("business_code", businessCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询
     *
     * @param businessCode
     * @param loginUser
     * @return
     */
    public List<ProdBusinessIn> listAll(String businessCode, LoginUser loginUser) {
        ProdBusinessIn entity = BeanCreate.newBean(ProdBusinessIn.class);
        entity.setBusinessCode(businessCode);
        QueryWrapper<ProdBusinessIn> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.orderByAsc("index_sort");
        return list(queryWrapper);
    }

    /**
     * 删除数据
     *
     * @param businessCode
     * @param loginUser
     * @return
     */
    public Boolean deleteByBusinessCode(String businessCode, LoginUser loginUser) {
        ProdBusinessIn entity = new ProdBusinessIn();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdBusinessIn query = new ProdBusinessIn();
        query.setBusinessCode(businessCode);
        UpdateWrapper<ProdBusinessIn> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 查询可用的
     *
     * @param code
     * @param loginUser
     * @return
     */
    public ProdBusinessIn getUsableGoodsByCode(String code, LoginUser loginUser) {
        ProdBusinessIn entity = BeanCreate.newBean(ProdBusinessIn.class);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        entity.setCode(code);
        QueryWrapper<ProdBusinessIn> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 更新记录
     *
     * @param code
     * @param updateVersion
     * @param loginUser
     * @return
     */
    public boolean useBusinessIn(String code, Integer updateVersion, LoginUser loginUser) {
        ProdBusinessIn entity = BeanCreate.newBean(ProdBusinessIn.class);
        entity.setStatus(YesOrNoEnum.NO.getValue());
        entity.setUpdateVersion(updateVersion + 1);
        DataOPUtil.editUser(entity, loginUser);
        ProdBusinessIn query = BeanCreate.newBean(ProdBusinessIn.class);
        query.setUpdateVersion(updateVersion);
        query.setCode(code);
        QueryWrapper<ProdBusinessIn> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return update(entity, queryWrapper);
    }

    /**
     * 更新记录
     *
     * @param code
     * @param loginUser
     * @return
     */
    public boolean cancelBusinessIn(String code, LoginUser loginUser) {
        ProdBusinessIn entity = BeanCreate.newBean(ProdBusinessIn.class);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdBusinessIn query = BeanCreate.newBean(ProdBusinessIn.class);
        query.setCode(code);
        QueryWrapper<ProdBusinessIn> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return update(entity, queryWrapper);
    }

    public List<ProdBusinessIn> listByNumber(String serialNumber, LoginUser loginUser) {
        ProdBusinessIn entity = BeanCreate.newBean(ProdBusinessIn.class);
        entity.setSerialNumber(serialNumber);
        QueryWrapper<ProdBusinessIn> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.orderByDesc("create_time");
        return list(queryWrapper);
    }

    public List<Integer> listIndexSort( String businessCode, LoginUser loginUser) {
        ProdBusinessIn entity = BeanCreate.newBean(ProdBusinessIn.class);
        entity.setBusinessCode(businessCode);
        DataOPUtil.createUser(entity,loginUser);
       return getBaseMapper().listIndexSort(entity);
    }
}
