package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.dto.PurchaseQueryDTO;
import com.ray.business.table.entity.ProdPurchaseRecord;
import com.ray.business.table.mapper.ProdPurchaseRecordMapper;
import com.ray.business.table.params.purchase.PurchaseGoodsParams;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 销售记录明细 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdPurchaseRecordService extends ServiceImpl<ProdPurchaseRecordMapper, ProdPurchaseRecord> {

    /**
     * 删除订单商品
     *
     * @param orderNo
     * @param loginUser
     * @return
     */
    public Boolean deleteOrderGoods(String orderNo, LoginUser loginUser) {
        ProdPurchaseRecord entity = new ProdPurchaseRecord();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchaseRecord query = new ProdPurchaseRecord();
        query.setOrderNo(orderNo);
        UpdateWrapper<ProdPurchaseRecord> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 查询
     *
     * @param orderNo
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseRecord> list(String orderNo, LoginUser loginUser) {
        ProdPurchaseRecord entity = BeanCreate.newBean(ProdPurchaseRecord.class);
        entity.setOrderNo(orderNo);
        QueryWrapper<ProdPurchaseRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseRecord> list(List<String> orderNos, LoginUser loginUser) {
        ProdPurchaseRecord entity = BeanCreate.newBean(ProdPurchaseRecord.class);
        QueryWrapper<ProdPurchaseRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdPurchaseRecord> page(CommonPage<PurchaseQueryDTO, Page<ProdPurchaseRecord>> queryParams, LoginUser loginUser) {
        PurchaseQueryDTO params = queryParams.getEntity();
        ProdPurchaseRecord entity = BeanCreate.newBean(ProdPurchaseRecord.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdPurchaseRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getOrderNoLike()), "order_no", params.getOrderNoLike());
        }
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 查询订单商品
     *
     * @param goodsParams
     * @param orderNo
     * @param loginUser
     * @return
     */
    public ProdPurchaseRecord queryRecord(PurchaseGoodsParams goodsParams, String orderNo, LoginUser loginUser) {
        ProdPurchaseRecord entity = BeanCreate.newBean(ProdPurchaseRecord.class);
        entity.setOrderNo(orderNo);
        entity.setGoodsCode(goodsParams.getGoodsCode());
        entity.setUnit(goodsParams.getUnit());
        QueryWrapper<ProdPurchaseRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表
     *
     * @param queryDTO
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseRecord> list(PurchaseQueryDTO queryDTO, LoginUser loginUser) {
        ProdPurchaseRecord entity = BeanCreate.newBean(ProdPurchaseRecord.class);
        BeanUtil.copyProperties(queryDTO, entity);
        QueryWrapper<ProdPurchaseRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询已采购数量
     *
     * @param orderNos
     * @param modelCode
     * @param unit
     * @param loginUser
     */
    public BigDecimal countPurchaseQuantity(List<String> orderNos, String modelCode, String unit, LoginUser loginUser) {
        if (ObjectUtil.isEmpty(orderNos)) {
            return BigDecimal.ZERO;
        }
        ProdPurchaseRecord entity = BeanCreate.newBean(ProdPurchaseRecord.class);
        entity.setGoodsCode(modelCode);
        entity.setUnit(unit);
        QueryWrapper<ProdPurchaseRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return list(queryWrapper).stream().map(ProdPurchaseRecord::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public ProdPurchaseRecord queryPurchaseGoods(String code, LoginUser loginUser) {
        ProdPurchaseRecord entity = BeanCreate.newBean(ProdPurchaseRecord.class);
        entity.setCode(code);
        QueryWrapper<ProdPurchaseRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    public Boolean updatePrice(Long id, BigDecimal price, LoginUser loginUser) {
        ProdPurchaseRecord entity = new ProdPurchaseRecord();
        entity.setId(id);
        entity.setPrice(price);
        DataOPUtil.editUser(entity, loginUser);
        return updateById(entity);
    }
}
