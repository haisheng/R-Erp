package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.enums.OrderSendStatusEnum;
import com.ray.business.table.dto.OrderSendQueryDTO;
import com.ray.business.table.entity.ProdOrderSend;
import com.ray.business.table.mapper.ProdOrderSendMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 生产订单 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdOrderSendService extends ServiceImpl<ProdOrderSendMapper, ProdOrderSend>  {
    /**
     * 编辑订单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdOrderSend entity, LoginUser loginUser) {
        ProdOrderSend query = new ProdOrderSend();
        query.setSendCode(entity.getSendCode());
        UpdateWrapper<ProdOrderSend> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询订单
     *
     * @param orderNo 订单编码
     * @param loginUser    当前操作员
     * @return
     */
    public ProdOrderSend queryOrderSendByOrderSendCode(String orderNo, LoginUser loginUser) {
        ProdOrderSend query = new ProdOrderSend();
        query.setSendCode(orderNo);
        QueryWrapper<ProdOrderSend> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }



    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdOrderSend> page(CommonPage<OrderSendQueryDTO, Page<ProdOrderSend>> queryParams, LoginUser loginUser) {
        OrderSendQueryDTO params = queryParams.getEntity();
        ProdOrderSend entity = BeanCreate.newBean(ProdOrderSend.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdOrderSend> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in(StrUtil.isBlank(params.getOrderStatus()) && ObjectUtil.isNotEmpty(params.getIncludeStatus()),
                "order_status", params.getIncludeStatus());
        queryWrapper.orderByDesc("create_time");
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<ProdOrderSend> list(OrderSendQueryDTO queryParams, LoginUser loginUser) {
        ProdOrderSend entity = BeanCreate.newBean(ProdOrderSend.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<ProdOrderSend> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }


    /**
     * 完成订单
     * @param businessCode
     * @param loginUser
     * @return
     */
    public Boolean finishOrder(String businessCode, LoginUser loginUser) {
        ProdOrderSend entity = new ProdOrderSend();
        entity.setOrderStatus(OrderSendStatusEnum.FINISH.getValue());
        entity.setSendTime(LocalDateTime.now());
        DataOPUtil.editUser(entity, loginUser);
        ProdOrderSend query = new ProdOrderSend();
        query.setSendCode(businessCode);
        UpdateWrapper<ProdOrderSend> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    public List<ProdOrderSend> listByOrderNos( List<String> orderNos, LoginUser loginUser) {
        ProdOrderSend entity = BeanCreate.newBean(ProdOrderSend.class);
        QueryWrapper<ProdOrderSend> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("send_code", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 确认对账
     * @param orderNos
     * @param billNo
     * @param loginUser
     * @return
     */
    public boolean comfireBill(List<String> orderNos, String billNo, LoginUser loginUser) {
        ProdOrderSend entity = new ProdOrderSend();
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setBillNo(billNo);
        DataOPUtil.editUser(entity, loginUser);
        ProdOrderSend query = new ProdOrderSend();
        query.setBillStatus(YesOrNoEnum.NO.getValue());
        UpdateWrapper<ProdOrderSend> updateWrapper = new UpdateWrapper<>(query);
        updateWrapper.in("send_code", orderNos);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    public boolean cancelBill(String billNo, LoginUser loginUser) {
        ProdOrderSend entity = new ProdOrderSend();
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        entity.setBillNo("");
        DataOPUtil.editUser(entity, loginUser);
        ProdOrderSend query = new ProdOrderSend();
        query.setBillNo(billNo);
        query.setBillStatus(YesOrNoEnum.YES.getValue());
        UpdateWrapper<ProdOrderSend> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    public List<ProdOrderSend> listByBillNo(String billNo, LoginUser loginUser) {
        ProdOrderSend entity = BeanCreate.newBean(ProdOrderSend.class);
        entity.setBillNo(billNo);
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<ProdOrderSend> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }
}
