package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.dto.OrderStepQueryDTO;
import com.ray.business.table.entity.ProdOrderStep;
import com.ray.business.table.mapper.ProdOrderStepMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 订单步骤-步骤 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdOrderStepService extends ServiceImpl<ProdOrderStepMapper, ProdOrderStep> {
    /**
     * 编辑订单步骤
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdOrderStep entity, LoginUser loginUser) {
        ProdOrderStep query = new ProdOrderStep();
        query.setCode(entity.getCode());
        UpdateWrapper<ProdOrderStep> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询订单步骤
     *
     * @param code      订单步骤编码
     * @param loginUser 当前操作员
     * @return
     */
    public ProdOrderStep queryStepByOrderStepCode(String code, LoginUser loginUser) {
        ProdOrderStep query = new ProdOrderStep();
        query.setCode(code);
        QueryWrapper<ProdOrderStep> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 查询订单步骤
     *
     * @param indexSort 排序
     * @param loginUser 当前操作员
     * @return
     */
    public ProdOrderStep queryStepByIndexSort(String materialCode, Integer indexSort, LoginUser loginUser) {
        ProdOrderStep query = new ProdOrderStep();
        query.setMaterialCode(materialCode);
        query.setIndexSort(indexSort);
        QueryWrapper<ProdOrderStep> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 查询订单步骤
     *
     * @param stepCode  步骤
     * @param loginUser 当前操作员
     * @return
     */
    public ProdOrderStep queryStepByStepCode(String materialCode, String stepCode, LoginUser loginUser) {
        ProdOrderStep query = new ProdOrderStep();
        query.setMaterialCode(materialCode);
        query.setStepCode(stepCode);
        QueryWrapper<ProdOrderStep> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 查询订单步骤
     *
     * @param loginUser 当前操作员
     * @return
     */
    public List<ProdOrderStep> listStepByMaterialCode(String materialCode, LoginUser loginUser) {
        ProdOrderStep query = new ProdOrderStep();
        query.setMaterialCode(materialCode);
        QueryWrapper<ProdOrderStep> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdOrderStep> page(CommonPage<OrderStepQueryDTO, Page<ProdOrderStep>> queryParams, LoginUser loginUser) {
        OrderStepQueryDTO params = queryParams.getEntity();
        ProdOrderStep entity = BeanCreate.newBean(ProdOrderStep.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdOrderStep> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.orderByAsc("index_sort");
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<ProdOrderStep> list(OrderStepQueryDTO queryParams, LoginUser loginUser) {
        ProdOrderStep entity = BeanCreate.newBean(ProdOrderStep.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<ProdOrderStep> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.orderByAsc("index_sort");
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 获取订单最后一个工序
     * @param materialCode
     * @param loginUser
     * @return
     */
    public ProdOrderStep lastStep(String materialCode, LoginUser loginUser) {
        ProdOrderStep entity = BeanCreate.newBean(ProdOrderStep.class);
        entity.setMaterialCode(materialCode);
        QueryWrapper<ProdOrderStep> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.orderByDesc("index_sort");
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        List<ProdOrderStep> list = list(queryWrapper);
        if (ObjectUtil.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

}
