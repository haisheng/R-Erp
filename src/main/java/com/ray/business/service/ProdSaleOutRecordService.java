package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.dto.SaleOutQueryDTO;
import com.ray.business.table.entity.ProdPurchaseInRecord;
import com.ray.business.table.entity.ProdPurchaseRecord;
import com.ray.business.table.entity.ProdSaleOutRecord;
import com.ray.business.table.mapper.ProdSaleOutRecordMapper;
import com.ray.business.table.params.purchase.PurchaseGoodsParams;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 销售退回记录明细 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-27
 */
@Service
public class ProdSaleOutRecordService extends ServiceImpl<ProdSaleOutRecordMapper, ProdSaleOutRecord>  {


    /**
     * 删除订单商品
     *
     * @param outCode
     * @param loginUser
     * @return
     */
    public Boolean deleteOrderGoods(String outCode, LoginUser loginUser) {
        ProdSaleOutRecord entity = new ProdSaleOutRecord();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdSaleOutRecord query = new ProdSaleOutRecord();
        query.setOutCode(outCode);
        UpdateWrapper<ProdSaleOutRecord> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 列表查询
     *
     * @param outCode
     * @param loginUser
     * @return
     */
    public List<ProdSaleOutRecord> list(String outCode, LoginUser loginUser) {
        ProdSaleOutRecord entity = BeanCreate.newBean(ProdSaleOutRecord.class);
        entity.setOutCode(outCode);
        QueryWrapper<ProdSaleOutRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdPurchaseRecord> page(CommonPage<SaleOutQueryDTO, Page<ProdSaleOutRecord>> queryParams, LoginUser loginUser) {
        SaleOutQueryDTO params = queryParams.getEntity();
        ProdSaleOutRecord entity = BeanCreate.newBean(ProdSaleOutRecord.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdSaleOutRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getOutCodeLike()), "out_code", params.getOutCodeLike());
        }
        queryWrapper.orderByDesc("create_time");
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 查询已退回数量
     *
     * @param outCode
     * @param purchaseGoodsParams
     * @param loginUser
     * @return
     */
    public BigDecimal queryBackQuantity(String orderNo, String outCode, PurchaseGoodsParams purchaseGoodsParams, LoginUser loginUser) {
        ProdSaleOutRecord entity = BeanCreate.newBean(ProdSaleOutRecord.class);
        entity.setOrderNo(orderNo);
        entity.setBatchNo(purchaseGoodsParams.getBatchNo());
        entity.setGoodsCode(purchaseGoodsParams.getGoodsCode());
        entity.setSerialNumber(purchaseGoodsParams.getSerialNumber());
        QueryWrapper<ProdSaleOutRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.ne(StrUtil.isNotBlank(outCode), "back_code", outCode);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper).stream().map(ProdSaleOutRecord::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * 查询已退回数量
     *
     * @param recordCode
     * @param loginUser
     * @return
     */
    public BigDecimal queryBackQuantity(String recordCode, List<String> outCodes, LoginUser loginUser) {
        if (ObjectUtil.isEmpty(outCodes)) {
            return new BigDecimal(0);
        }
        ProdSaleOutRecord entity = BeanCreate.newBean(ProdSaleOutRecord.class);
        entity.setRecordCode(recordCode);
        QueryWrapper<ProdSaleOutRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.in("back_code", outCodes);
        return list(queryWrapper).stream().map(ProdSaleOutRecord::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * 列表
     * @param orderNo
     * @param outCodes
     * @param loginUser
     * @return
     */
    public List<ProdSaleOutRecord> listByOrderNo(String orderNo, List<String> outCodes, LoginUser loginUser) {
        ProdSaleOutRecord entity = BeanCreate.newBean(ProdSaleOutRecord.class);
        entity.setOrderNo(orderNo);
        QueryWrapper<ProdSaleOutRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in(ObjectUtil.isNotEmpty(outCodes), "out_code", outCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    public List<ProdSaleOutRecord> listByOutCodes(List<String> outCodes, LoginUser loginUser) {
        ProdSaleOutRecord entity = BeanCreate.newBean(ProdSaleOutRecord.class);
        QueryWrapper<ProdSaleOutRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in(ObjectUtil.isNotEmpty(outCodes), "out_code", outCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }
}
