package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.dto.PurchaseBackQueryDTO;
import com.ray.business.table.entity.ProdPurchaseBackRecord;
import com.ray.business.table.entity.ProdPurchaseInRecord;
import com.ray.business.table.entity.ProdPurchaseRecord;
import com.ray.business.table.mapper.ProdPurchaseBackRecordMapper;
import com.ray.business.table.params.purchase.PurchaseGoodsParams;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 采购退回记录明细 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdPurchaseBackRecordService extends ServiceImpl<ProdPurchaseBackRecordMapper, ProdPurchaseBackRecord> {

    /**
     * 删除订单商品
     *
     * @param backCode
     * @param loginUser
     * @return
     */
    public Boolean deleteOrderGoods(String backCode, LoginUser loginUser) {
        ProdPurchaseBackRecord entity = new ProdPurchaseBackRecord();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchaseBackRecord query = new ProdPurchaseBackRecord();
        query.setBackCode(backCode);
        UpdateWrapper<ProdPurchaseBackRecord> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 列表查询
     *
     * @param backCode
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseBackRecord> list(String backCode, LoginUser loginUser) {
        ProdPurchaseBackRecord entity = BeanCreate.newBean(ProdPurchaseBackRecord.class);
        entity.setBackCode(backCode);
        QueryWrapper<ProdPurchaseBackRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdPurchaseRecord> page(CommonPage<PurchaseBackQueryDTO, Page<ProdPurchaseBackRecord>> queryParams, LoginUser loginUser) {
        PurchaseBackQueryDTO params = queryParams.getEntity();
        ProdPurchaseBackRecord entity = BeanCreate.newBean(ProdPurchaseBackRecord.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdPurchaseBackRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getBackCodeLike()), "back_code", params.getBackCodeLike());
        }
        queryWrapper.orderByDesc("create_time");
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 查询已退回数量
     *
     * @param backCode
     * @param purchaseGoodsParams
     * @param loginUser
     * @return
     */
    public BigDecimal queryBackQuantity(String orderNo, String backCode, PurchaseGoodsParams purchaseGoodsParams, LoginUser loginUser) {
        ProdPurchaseBackRecord entity = BeanCreate.newBean(ProdPurchaseBackRecord.class);
        entity.setOrderNo(orderNo);
        entity.setBatchNo(purchaseGoodsParams.getBatchNo());
        entity.setGoodsCode(purchaseGoodsParams.getGoodsCode());
        entity.setSerialNumber(purchaseGoodsParams.getSerialNumber());
        entity.setUnit(purchaseGoodsParams.getUnit());
        QueryWrapper<ProdPurchaseBackRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.ne(StrUtil.isNotBlank(backCode), "back_code", backCode);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper).stream().map(ProdPurchaseBackRecord::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * 查询已退回数量
     *
     * @param modelCode
     * @param loginUser
     * @return
     */
    public BigDecimal queryBackQuantity(String modelCode, String unit, List<String> backCodes, LoginUser loginUser) {
        if (ObjectUtil.isEmpty(backCodes)) {
            return new BigDecimal(0);
        }
        ProdPurchaseBackRecord entity = BeanCreate.newBean(ProdPurchaseBackRecord.class);
        entity.setGoodsCode(modelCode);
        entity.setUnit(unit);
        QueryWrapper<ProdPurchaseBackRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.in("back_code", backCodes);
        return list(queryWrapper).stream().map(ProdPurchaseBackRecord::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public List<ProdPurchaseBackRecord> list(String orderNo, String goodsCode, LoginUser loginUser) {
        ProdPurchaseBackRecord entity = BeanCreate.newBean(ProdPurchaseBackRecord.class);
        entity.setOrderNo(orderNo);
        entity.setGoodsCode(goodsCode);
        QueryWrapper<ProdPurchaseBackRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    public Boolean updatePrice(Long id, BigDecimal price, LoginUser loginUser) {
        ProdPurchaseBackRecord entity = new ProdPurchaseBackRecord();
        entity.setId(id);
        entity.setPrice(price);
        DataOPUtil.editUser(entity, loginUser);
        return updateById(entity);
    }

    public List<ProdPurchaseBackRecord> listByOrderNo(List<String> orderNos, LoginUser loginUser) {
        ProdPurchaseBackRecord entity = BeanCreate.newBean(ProdPurchaseBackRecord.class);
        QueryWrapper<ProdPurchaseBackRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in(ObjectUtil.isNotEmpty(orderNos), "order_no", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

}
