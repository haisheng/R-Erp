package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.business.table.dto.SaleQueryDTO;
import com.ray.business.table.entity.ProdSaleRecord;
import com.ray.business.table.entity.ProdSaleRecord;
import com.ray.business.table.mapper.ProdSaleRecordMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.params.sale.SaleGoodsParams;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 销售记录明细 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdSaleRecordService extends ServiceImpl<ProdSaleRecordMapper, ProdSaleRecord> {
    /**
     * 删除订单商品
     * @param orderNo
     * @param loginUser
     * @return
     */
    public Boolean deleteOrderGoods( String orderNo, LoginUser loginUser) {
        ProdSaleRecord entity = new ProdSaleRecord();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdSaleRecord query = new ProdSaleRecord();
        query.setOrderNo(orderNo);
        UpdateWrapper<ProdSaleRecord> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 列表查询
     *
     * @param orderNo
     * @param loginUser
     * @return
     */
    public List<ProdSaleRecord> list(String orderNo, LoginUser loginUser) {
        ProdSaleRecord entity = BeanCreate.newBean(ProdSaleRecord.class);
        entity.setOrderNo(orderNo);
        QueryWrapper<ProdSaleRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdSaleRecord> page(CommonPage<SaleQueryDTO, Page<ProdSaleRecord>> queryParams, LoginUser loginUser) {
        SaleQueryDTO params = queryParams.getEntity();
        ProdSaleRecord entity = BeanCreate.newBean(ProdSaleRecord.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdSaleRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getOrderNoLike()), "order_no", params.getOrderNoLike());
        }
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 查询订单商品
     * @param goodsParams
     * @param orderNo
     * @param loginUser
     * @return
     */
    public ProdSaleRecord queryRecord(SaleGoodsParams goodsParams, String orderNo, LoginUser loginUser) {
        ProdSaleRecord entity = BeanCreate.newBean(ProdSaleRecord.class);
        entity.setOrderNo(orderNo);
        entity.setGoodsCode(goodsParams.getGoodsCode());
        entity.setUnit(goodsParams.getUnit());
        QueryWrapper<ProdSaleRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return  getOne(queryWrapper);
    }
}
