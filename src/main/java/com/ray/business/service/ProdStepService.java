package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.dto.StepQueryDTO;
import com.ray.business.table.entity.ProdStep;
import com.ray.business.table.mapper.ProdStepMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 生产步骤 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdStepService extends ServiceImpl<ProdStepMapper, ProdStep> {

    /**
     * 编辑步骤
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdStep entity, LoginUser loginUser) {
        ProdStep query = new ProdStep();
        query.setStepCode(entity.getStepCode());
        UpdateWrapper<ProdStep> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询步骤
     *
     * @param customerCode 步骤编码
     * @param loginUser    当前操作员
     * @return
     */
    public ProdStep queryStepByStepCode(String customerCode, LoginUser loginUser) {
        ProdStep query = new ProdStep();
        query.setStepCode(customerCode);
        QueryWrapper<ProdStep> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 查询步骤
     *
     * @param setpName 步骤编码
     * @param loginUser    当前操作员
     * @return
     */
    public ProdStep queryStepByStepName(String setpName, LoginUser loginUser) {
        ProdStep query = new ProdStep();
        query.setStepName(setpName);
        QueryWrapper<ProdStep> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdStep> page(CommonPage<StepQueryDTO, Page<ProdStep>> queryParams, LoginUser loginUser) {
        StepQueryDTO params = queryParams.getEntity();
        ProdStep entity = BeanCreate.newBean(ProdStep.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdStep> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getStepNameLike()), "step_name", params.getStepNameLike());
        }
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<ProdStep> list(StepQueryDTO queryParams, LoginUser loginUser) {
        ProdStep entity = BeanCreate.newBean(ProdStep.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<ProdStep> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getStepNameLike()), "step_name", queryParams.getStepNameLike());
        return list(queryWrapper);
    }


    public List<ProdStep> listByCodes(List<String> stepCode, LoginUser loginUser) {
        ProdStep entity = BeanCreate.newBean(ProdStep.class);
        QueryWrapper<ProdStep> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("step_code",stepCode);
        queryWrapper.orderByAsc("index_sort");
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    public List<ProdStep> listQuick(LoginUser loginUser) {
        ProdStep entity = BeanCreate.newBean(ProdStep.class);
        entity.setQuickStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<ProdStep> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }
}
