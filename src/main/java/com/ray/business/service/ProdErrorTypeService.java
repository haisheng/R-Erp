package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.dto.ErrorTypeQueryDTO;
import com.ray.business.table.entity.ProdErrorType;
import com.ray.business.table.mapper.ProdErrorTypeMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 验布错误类型 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-07-17
 */
@Service
public class ProdErrorTypeService extends ServiceImpl<ProdErrorTypeMapper, ProdErrorType> {

    /**
     * 编辑步骤
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdErrorType entity, LoginUser loginUser) {
        ProdErrorType query = new ProdErrorType();
        query.setErrorCode(entity.getErrorCode());
        UpdateWrapper<ProdErrorType> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询步骤
     *
     * @param customerCode 步骤编码
     * @param loginUser    当前操作员
     * @return
     */
    public ProdErrorType queryErrorByErrorCode(String customerCode, LoginUser loginUser) {
        ProdErrorType query = new ProdErrorType();
        query.setErrorCode(customerCode);
        QueryWrapper<ProdErrorType> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 查询步骤
     *
     * @param errorName 步骤编码
     * @param loginUser    当前操作员
     * @return
     */
    public ProdErrorType queryErrorByErrorName(String errorName, LoginUser loginUser) {
        ProdErrorType query = new ProdErrorType();
        query.setErrorName(errorName);
        QueryWrapper<ProdErrorType> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdErrorType> page(CommonPage<ErrorTypeQueryDTO, Page<ProdErrorType>> queryParams, LoginUser loginUser) {
        ErrorTypeQueryDTO params = queryParams.getEntity();
        ProdErrorType entity = BeanCreate.newBean(ProdErrorType.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdErrorType> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getErrorNameLike()), "error_name", params.getErrorNameLike());
        }
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<ProdErrorType> list(ErrorTypeQueryDTO queryParams, LoginUser loginUser) {
        ProdErrorType entity = BeanCreate.newBean(ProdErrorType.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<ProdErrorType> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getErrorNameLike()), "error_name", queryParams.getErrorNameLike());
        return list(queryWrapper);
    }

}
