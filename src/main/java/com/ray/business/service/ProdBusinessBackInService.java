package com.ray.business.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.entity.ProdBusinessBackIn;
import com.ray.business.table.mapper.ProdBusinessBackInMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 加工退料单物料明细 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-22
 */
@Service
public class ProdBusinessBackInService extends ServiceImpl<ProdBusinessBackInMapper, ProdBusinessBackIn> {

    /**
     * 查询信息
     *
     * @param backCode
     * @param loginUser
     * @return
     */
    public List<ProdBusinessBackIn> listAll(String backCode, LoginUser loginUser) {
        ProdBusinessBackIn entity = BeanCreate.newBean(ProdBusinessBackIn.class);
        entity.setBackCode(backCode);
        QueryWrapper<ProdBusinessBackIn> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 删除数据
     *
     * @param backCode
     * @param loginUser
     * @return
     */
    public boolean deleteByBackCode(String backCode, LoginUser loginUser) {
        ProdBusinessBackIn entity = new ProdBusinessBackIn();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdBusinessBackIn query = new ProdBusinessBackIn();
        query.setBackCode(backCode);
        UpdateWrapper<ProdBusinessBackIn> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }
}
