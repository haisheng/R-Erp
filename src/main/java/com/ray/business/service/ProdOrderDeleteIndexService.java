package com.ray.business.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.builder.OrderIndexBuilder;
import com.ray.business.table.entity.ProdOrderDeleteIndex;
import com.ray.business.table.entity.ProdOrderIndex;
import com.ray.business.table.mapper.ProdOrderDeleteIndexMapper;
import com.ray.business.table.mapper.ProdOrderIndexMapper;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 生产订单 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-07-25
 */
@Service
public class ProdOrderDeleteIndexService extends ServiceImpl<ProdOrderDeleteIndexMapper, ProdOrderDeleteIndex> {


    public ProdOrderDeleteIndex getDeleteIndex(String orderNo, String goodsCode, LoginUser loginUser) {
        ProdOrderDeleteIndex query = new ProdOrderDeleteIndex();
        query.setOrderNo(orderNo);
        query.setGoodsCode(goodsCode);
        QueryWrapper<ProdOrderDeleteIndex> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.orderByAsc("index_sort");
        List<ProdOrderDeleteIndex> deleteIndexList = list(queryWrapper);
        if (ObjectUtil.isNotEmpty(deleteIndexList)) {
            return deleteIndexList.get(0);
        }
        return null;
    }
}
