package com.ray.business.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.entity.ProdBusinessOut;
import com.ray.business.table.mapper.ProdBusinessOutMapper;
import com.ray.business.table.params.business.BusinessGoodsParams;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 加工单物料明细 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdBusinessOutService extends ServiceImpl<ProdBusinessOutMapper, ProdBusinessOut> {

    /**
     * 查询
     *
     * @param businessCodes
     * @param loginUser
     * @return
     */
    public List<ProdBusinessOut> list(List<String> businessCodes, LoginUser loginUser) {
        if (ObjectUtil.isEmpty(businessCodes)) {
            return new ArrayList<>();
        }
        ProdBusinessOut entity = BeanCreate.newBean(ProdBusinessOut.class);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<ProdBusinessOut> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("business_code", businessCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }


    /**
     * 查询
     *
     * @param businessCode
     * @param loginUser
     * @return
     */
    public List<ProdBusinessOut> listAll(String businessCode, LoginUser loginUser) {
        ProdBusinessOut entity = BeanCreate.newBean(ProdBusinessOut.class);
        entity.setBusinessCode(businessCode);
        QueryWrapper<ProdBusinessOut> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.orderByAsc("index_sort");
        return list(queryWrapper);
    }

    /**
     * 删除数据
     *
     * @param businessCode
     * @param loginUser
     * @return
     */
    public Boolean deleteByBusinessCode(String businessCode, LoginUser loginUser) {
        ProdBusinessOut entity = new ProdBusinessOut();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdBusinessOut query = new ProdBusinessOut();
        query.setBusinessCode(businessCode);
        UpdateWrapper<ProdBusinessOut> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 查询可用的
     *
     * @param code
     * @param loginUser
     * @return
     */
    public ProdBusinessOut getUsableGoodsByCode(String code, LoginUser loginUser) {
        ProdBusinessOut entity = BeanCreate.newBean(ProdBusinessOut.class);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        entity.setCode(code);
        QueryWrapper<ProdBusinessOut> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 更新记录
     *
     * @param code
     * @param updateVersion
     * @param loginUser
     * @return
     */
    public boolean useBusinessOut(String code, Integer updateVersion, LoginUser loginUser) {
        ProdBusinessOut entity = BeanCreate.newBean(ProdBusinessOut.class);
        entity.setStatus(YesOrNoEnum.NO.getValue());
        entity.setUpdateVersion(updateVersion + 1);
        DataOPUtil.editUser(entity, loginUser);
        ProdBusinessOut query = BeanCreate.newBean(ProdBusinessOut.class);
        query.setUpdateVersion(updateVersion);
        query.setCode(code);
        QueryWrapper<ProdBusinessOut> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return update(entity, queryWrapper);
    }

    /**
     * 更新记录
     *
     * @param code
     * @param loginUser
     * @return
     */
    public boolean cancelBusinessOut(String code, LoginUser loginUser) {
        ProdBusinessOut entity = BeanCreate.newBean(ProdBusinessOut.class);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdBusinessOut query = BeanCreate.newBean(ProdBusinessOut.class);
        query.setCode(code);
        QueryWrapper<ProdBusinessOut> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return update(entity, queryWrapper);
    }

    /**
     * 更新数量
     *
     * @param code
     * @param quantity
     * @param updateVersion
     * @param loginUser
     * @return
     */
    public boolean updateQuantity(String code, BigDecimal quantity, Integer updateVersion, LoginUser loginUser) {
        ProdBusinessOut entity = BeanCreate.newBean(ProdBusinessOut.class);
        entity.setUpdateVersion(updateVersion + 1);
        entity.setQuantity(quantity);
        DataOPUtil.editUser(entity, loginUser);
        ProdBusinessOut query = BeanCreate.newBean(ProdBusinessOut.class);
        query.setCode(code);
        QueryWrapper<ProdBusinessOut> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return update(entity, queryWrapper);
    }

    /**
     * 获取信息
     *
     * @param code
     * @param loginUser
     * @return
     */
    public ProdBusinessOut getGoodsByCode(String code, LoginUser loginUser) {
        ProdBusinessOut entity = BeanCreate.newBean(ProdBusinessOut.class);
        entity.setCode(code);
        QueryWrapper<ProdBusinessOut> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    public ProdBusinessOut getOutGoodsByNumber(String serialNumber, List<String> businessCodes, LoginUser loginUser) {
        if (ObjectUtil.isEmpty(businessCodes)) {
            return null;
        }
        ProdBusinessOut entity = BeanCreate.newBean(ProdBusinessOut.class);
        entity.setSerialNumber(serialNumber);
        QueryWrapper<ProdBusinessOut> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("business_code", businessCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.orderByDesc("create_time");
        List<ProdBusinessOut> list = list(queryWrapper);
        if (ObjectUtil.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    public ProdBusinessOut getOutGoodsByCode(BusinessGoodsParams goodsParams, List<String> businessCodes, LoginUser loginUser) {
        if (ObjectUtil.isEmpty(businessCodes)) {
            return null;
        }
        ProdBusinessOut entity = BeanCreate.newBean(ProdBusinessOut.class);
        entity.setGoodsCode(goodsParams.getGoodsCode());
        entity.setBatchNo(goodsParams.getBatchNo());
        entity.setSerialNumber(goodsParams.getSerialNumber());
        QueryWrapper<ProdBusinessOut> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("business_code", businessCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.orderByDesc("create_time");
        List<ProdBusinessOut> list = list(queryWrapper);
        if (ObjectUtil.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    public ProdBusinessOut queryByNumber(List<String> businessCodes, String serialNumber, LoginUser loginUser) {
        if (ObjectUtil.isEmpty(businessCodes)) {
            return null;
        }
        ProdBusinessOut entity = BeanCreate.newBean(ProdBusinessOut.class);
        entity.setSerialNumber(serialNumber);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<ProdBusinessOut> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("business_code", businessCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    public List<ProdBusinessOut> listByNumber( String serialNumber, LoginUser loginUser) {
        ProdBusinessOut entity = BeanCreate.newBean(ProdBusinessOut.class);
        entity.setSerialNumber(serialNumber);
        QueryWrapper<ProdBusinessOut> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.orderByDesc("create_time");
       return list(queryWrapper);
    }
}
