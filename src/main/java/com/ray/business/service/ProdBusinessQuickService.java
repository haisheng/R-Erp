package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.dto.BusinessQuickQueryDTO;
import com.ray.business.table.entity.ProdBusinessQuick;
import com.ray.business.table.mapper.ProdBusinessQuickMapper;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 加工单号-快捷入库 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-07-05
 */
@Service
public class ProdBusinessQuickService extends ServiceImpl<ProdBusinessQuickMapper, ProdBusinessQuick> {

    public ProdBusinessQuick queryEntity(BusinessQuickQueryDTO businessQuickQueryDTO, LoginUser loginUser) {
        ProdBusinessQuick query = new ProdBusinessQuick();
        BeanUtil.copyProperties(businessQuickQueryDTO, query);
        query.setStatus(YesOrNoEnum.NO.getValue());
        QueryWrapper<ProdBusinessQuick> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addUserDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }


    /**
     * 编辑
     *
     * @param quickCode 业务加工订单编码
     * @param loginUser 当前操作员
     * @return
     */
    public ProdBusinessQuick queryBusinessQuickByQuickCode(String quickCode, LoginUser loginUser) {
        ProdBusinessQuick query = new ProdBusinessQuick();
        query.setQuickCode(quickCode);
        query.setStatus(YesOrNoEnum.NO.getValue());
        QueryWrapper<ProdBusinessQuick> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 编辑订单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdBusinessQuick entity, LoginUser loginUser) {
        ProdBusinessQuick query = new ProdBusinessQuick();
        query.setQuickCode(entity.getQuickCode());
        UpdateWrapper<ProdBusinessQuick> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addUserDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }
}
