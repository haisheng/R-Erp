package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.enums.DeductionStatusEnum;
import com.ray.business.table.dto.DeductionQueryDTO;
import com.ray.business.table.entity.ProdBusinessDeduction;
import com.ray.business.table.mapper.ProdBusinessDeductionMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 加工单号-扣款 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdBusinessDeductionService extends ServiceImpl<ProdBusinessDeductionMapper, ProdBusinessDeduction> {
    /**
     * 编辑订单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdBusinessDeduction entity, LoginUser loginUser) {
        ProdBusinessDeduction query = new ProdBusinessDeduction();
        query.setDeductionCode(entity.getDeductionCode());
        UpdateWrapper<ProdBusinessDeduction> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询订单
     *
     * @param deductionCode 订单编码
     * @param loginUser     当前操作员
     * @return
     */
    public ProdBusinessDeduction queryBusinessDeductionByBusinessDeductionCode(String deductionCode, LoginUser loginUser) {
        ProdBusinessDeduction query = new ProdBusinessDeduction();
        query.setDeductionCode(deductionCode);
        QueryWrapper<ProdBusinessDeduction> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }


    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdBusinessDeduction> page(CommonPage<DeductionQueryDTO, Page<ProdBusinessDeduction>> queryParams, LoginUser loginUser) {
        DeductionQueryDTO params = queryParams.getEntity();
        ProdBusinessDeduction entity = BeanCreate.newBean(ProdBusinessDeduction.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdBusinessDeduction> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.orderByDesc("create_time");
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<ProdBusinessDeduction> list(DeductionQueryDTO queryParams, LoginUser loginUser) {
        ProdBusinessDeduction entity = BeanCreate.newBean(ProdBusinessDeduction.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<ProdBusinessDeduction> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }


    /**
     * 完成对账
     *
     * @param businessCode 业务单号
     * @param loginUser    当前操作员
     * @return
     */
    public boolean edit(String businessCode, LoginUser loginUser) {
        ProdBusinessDeduction entity = new ProdBusinessDeduction();
        entity.setOrderStatus(DeductionStatusEnum.FINANCE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdBusinessDeduction query = new ProdBusinessDeduction();
        query.setBusinessCode(businessCode);
        UpdateWrapper<ProdBusinessDeduction> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }


    /**
     * 确认对账
     *
     * @param businessCodes
     * @param loginUser
     * @return
     */
    public Boolean comfireBill(List<String> businessCodes, String billNo, LoginUser loginUser) {
        ProdBusinessDeduction entity = new ProdBusinessDeduction();
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setBillNo(billNo);
        DataOPUtil.editUser(entity, loginUser);
        ProdBusinessDeduction query = new ProdBusinessDeduction();
        query.setBillStatus(YesOrNoEnum.NO.getValue());
        UpdateWrapper<ProdBusinessDeduction> updateWrapper = new UpdateWrapper<>(query);
        updateWrapper.in("business_code", businessCodes);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 取消对账
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public Boolean cancelBill(String billNo, LoginUser loginUser) {
        ProdBusinessDeduction entity = new ProdBusinessDeduction();
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        entity.setBillNo("");
        DataOPUtil.editUser(entity, loginUser);
        ProdBusinessDeduction query = new ProdBusinessDeduction();
        query.setBillNo(billNo);
        query.setBillStatus(YesOrNoEnum.YES.getValue());
        UpdateWrapper<ProdBusinessDeduction> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        if(count(updateWrapper)== 0){
            return  true;
        }
        return update(entity, updateWrapper);
    }


    public List<ProdBusinessDeduction> listByOrderNos(List<String> orderNos, LoginUser loginUser) {
        ProdBusinessDeduction entity = BeanCreate.newBean(ProdBusinessDeduction.class);
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        QueryWrapper<ProdBusinessDeduction> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("business_code", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    public List<ProdBusinessDeduction> listByBillNo(String billNo, LoginUser loginUser) {
        ProdBusinessDeduction entity = BeanCreate.newBean(ProdBusinessDeduction.class);
        entity.setBillNo(billNo);
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<ProdBusinessDeduction> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     *
     * @param businessCodes
     * @param loginUser
     * @return
     */
    public Integer countBill(List<String> businessCodes, LoginUser loginUser) {
        ProdBusinessDeduction query = new ProdBusinessDeduction();
        query.setBillStatus(YesOrNoEnum.NO.getValue());
        QueryWrapper<ProdBusinessDeduction> queryWrapper = new QueryWrapper<>(query);
        queryWrapper.in("business_code", businessCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return  count(queryWrapper);
    }
}
