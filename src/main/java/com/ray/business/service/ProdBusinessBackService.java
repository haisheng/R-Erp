package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.enums.BusinessBackStatusEnum;
import com.ray.business.table.dto.BusinessBackQueryDTO;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdBusinessBack;
import com.ray.business.table.mapper.ProdBusinessBackMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * <p>
 * 加工退料单 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-22
 */
@Service
public class ProdBusinessBackService extends ServiceImpl<ProdBusinessBackMapper, ProdBusinessBack> {


    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdBusinessBack> page(CommonPage<BusinessBackQueryDTO, Page<ProdBusinessBack>> queryParams, LoginUser loginUser) {
        BusinessBackQueryDTO params = queryParams.getEntity();
        ProdBusinessBack entity = BeanCreate.newBean(ProdBusinessBack.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdBusinessBack> queryWrapper = new QueryWrapper<>(entity);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.gt(ObjectUtil.isNotNull(params.getStartTime()), "create_time", params.getStartTime());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getEndTime()), "create_time", params.getEndTime());
        }
        queryWrapper.in(StrUtil.isBlank(params.getOrderStatus()) && ObjectUtil.isNotEmpty(params.getIncludeStatus()),
                "order_status", params.getIncludeStatus());
        queryWrapper.orderByDesc("create_time");
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return page(queryParams.getPage(), queryWrapper);
    }


    /**
     * 编辑订单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdBusinessBack entity, LoginUser loginUser) {
        ProdBusinessBack query = new ProdBusinessBack();
        query.setBackCode(entity.getBackCode());
        UpdateWrapper<ProdBusinessBack> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询对应的
     *
     * @param businessCode
     * @param loginUser
     * @return
     */
    public int countUseByBusinessCode(String businessCode, LoginUser loginUser) {
        ProdBusinessBack query = new ProdBusinessBack();
        query.setBusinessCode(businessCode);
        QueryWrapper<ProdBusinessBack> queryWrapper = new QueryWrapper<>(query);
        queryWrapper.in("order_status", Arrays.asList(BusinessBackStatusEnum.UN_CHECK.getValue(),
                BusinessBackStatusEnum.FINISH.getValue(), BusinessBackStatusEnum.UN_IN.getValue()));
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return count(queryWrapper);
    }


    /**
     * 完成订单
     *
     * @param businessCode
     * @param loginUser
     * @return
     */
    public Boolean finishOrder(String businessCode, LoginUser loginUser) {
        ProdBusinessBack entity = new ProdBusinessBack();
        entity.setOrderStatus(BusinessBackStatusEnum.FINISH.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdBusinessBack query = new ProdBusinessBack();
        query.setBackCode(businessCode);
        UpdateWrapper<ProdBusinessBack> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    public ProdBusinessBack queryBusinessBackByBackCode(String backCode, LoginUser loginUser) {
        ProdBusinessBack query = new ProdBusinessBack();
        query.setBackCode(backCode);
        QueryWrapper<ProdBusinessBack> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }
}
