package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.enums.PurchaseInStatusEnum;
import com.ray.business.enums.SaleStatusEnum;
import com.ray.business.table.dto.PurchaseInQueryDTO;
import com.ray.business.table.entity.ProdPurchaseIn;
import com.ray.business.table.entity.ProdPurchaseInRecord;
import com.ray.business.table.mapper.ProdPurchaseInMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 采购订单-入库 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-27
 */
@Service
public class ProdPurchaseInService extends ServiceImpl<ProdPurchaseInMapper, ProdPurchaseIn> {

    /**
     * 查询 不是已取消的入库
     *
     * @param orderNo
     * @param loginUser
     * @return
     */
    public int countIn(String orderNo, LoginUser loginUser) {
        ProdPurchaseIn query = new ProdPurchaseIn();
        query.setOrderNo(orderNo);
        QueryWrapper<ProdPurchaseIn> queryWrapper = new QueryWrapper<>(query);
        queryWrapper.ne("order_status", PurchaseInStatusEnum.CANCEL.getValue());
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return count(queryWrapper);
    }

    /**
     * 编辑订单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdPurchaseIn entity, LoginUser loginUser) {
        ProdPurchaseIn query = new ProdPurchaseIn();
        query.setInCode(entity.getInCode());
        UpdateWrapper<ProdPurchaseIn> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询订单
     *
     * @param inCode    订单编码
     * @param loginUser 当前操作员
     * @return
     */
    public ProdPurchaseIn queryPurchaseInByInCode(String inCode, LoginUser loginUser) {
        ProdPurchaseIn query = new ProdPurchaseIn();
        query.setInCode(inCode);
        QueryWrapper<ProdPurchaseIn> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }


    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdPurchaseIn> page(CommonPage<PurchaseInQueryDTO, Page<ProdPurchaseIn>> queryParams, LoginUser loginUser) {
        PurchaseInQueryDTO params = queryParams.getEntity();
        ProdPurchaseIn entity = BeanCreate.newBean(ProdPurchaseIn.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdPurchaseIn> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getInCodeLike()), "in_code", params.getInCodeLike());
            queryWrapper.gt(ObjectUtil.isNotNull(params.getStartTime()), "create_time", params.getStartTime());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getEndTime()), "create_time", params.getEndTime());
        }
        queryWrapper.in(StrUtil.isBlank(params.getOrderStatus()) && ObjectUtil.isNotEmpty(params.getIncludeStatus()),
                "order_status", params.getIncludeStatus());
        queryWrapper.gt(ObjectUtil.isNotNull(params.getOutSendStartTime()), "out_send_time", params.getOutSendStartTime());
        queryWrapper.lt(ObjectUtil.isNotNull(params.getOutSendEndTime()), "out_send_time", params.getOutSendEndTime());
        queryWrapper.orderByDesc("create_time");
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseIn> list(PurchaseInQueryDTO queryParams, LoginUser loginUser) {
        ProdPurchaseIn entity = BeanCreate.newBean(ProdPurchaseIn.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<ProdPurchaseIn> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getInCodeLike()), "in_code", queryParams.getInCodeLike());
        queryWrapper.gt(ObjectUtil.isNotNull(queryParams.getStartTime()), "create_time", queryParams.getStartTime());
        queryWrapper.lt(ObjectUtil.isNotNull(queryParams.getEndTime()), "create_time", queryParams.getEndTime());
        queryWrapper.gt(ObjectUtil.isNotNull(queryParams.getOutSendStartTime()), "out_send_time", queryParams.getOutSendStartTime());
        queryWrapper.lt(ObjectUtil.isNotNull(queryParams.getOutSendEndTime()), "out_send_time", queryParams.getOutSendEndTime());
        return list(queryWrapper);
    }

    /**
     * 完成订单
     *
     * @param businessCode
     * @param loginUser
     * @return
     */
    public Boolean finishOrder(String businessCode, LoginUser loginUser) {
        ProdPurchaseIn entity = new ProdPurchaseIn();
        entity.setOrderStatus(SaleStatusEnum.FINISH.getValue());
        entity.setInTime(LocalDateTime.now());
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchaseIn query = new ProdPurchaseIn();
        query.setInCode(businessCode);
        UpdateWrapper<ProdPurchaseIn> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询采购单对于
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseIn> listByOrderNos(List<String> orderNos, List<String> orderStatus, LoginUser loginUser) {
        ProdPurchaseIn entity = BeanCreate.newBean(ProdPurchaseIn.class);
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        QueryWrapper<ProdPurchaseIn> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        queryWrapper.in(ObjectUtil.isNotNull(orderStatus), "order_status", orderStatus);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询采购单数量
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public Integer countByOrderNos(List<String> orderNos, List<String> orderStatus, LoginUser loginUser) {
        ProdPurchaseIn entity = BeanCreate.newBean(ProdPurchaseIn.class);
        QueryWrapper<ProdPurchaseIn> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        queryWrapper.in(ObjectUtil.isNotNull(orderStatus), "order_status", orderStatus);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return count(queryWrapper);
    }

    /**
     * 确认对账
     *
     * @param businessCodes
     * @param loginUser
     * @return
     */
    public Boolean comfireBill(List<String> businessCodes, String billNo, LoginUser loginUser) {
        ProdPurchaseIn entity = new ProdPurchaseIn();
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setBillNo(billNo);
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchaseIn query = new ProdPurchaseIn();
        query.setBillStatus(YesOrNoEnum.NO.getValue());
        UpdateWrapper<ProdPurchaseIn> updateWrapper = new UpdateWrapper<>(query);
        updateWrapper.in("order_no", businessCodes);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 取消对账
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public Boolean cancelBill(String billNo, LoginUser loginUser) {
        ProdPurchaseIn entity = new ProdPurchaseIn();
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        entity.setBillNo("");
        DataOPUtil.editUser(entity, loginUser);
        ProdPurchaseIn query = new ProdPurchaseIn();
        query.setBillStatus(YesOrNoEnum.YES.getValue());
        query.setBillNo(billNo);
        UpdateWrapper<ProdPurchaseIn> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询列表数据
     *
     * @param billNo
     * @param orderStatus
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseIn> listByBillNo(String billNo, String orderStatus, LoginUser loginUser) {
        ProdPurchaseIn entity = BeanCreate.newBean(ProdPurchaseIn.class);
        entity.setBillNo(billNo);
        entity.setBillStatus(YesOrNoEnum.YES.getValue());
        entity.setOrderStatus(orderStatus);
        QueryWrapper<ProdPurchaseIn> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询的退货单
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public List<ProdPurchaseIn> listFinishByOrderNos(List<String> orderNos, LoginUser loginUser) {
        ProdPurchaseIn entity = BeanCreate.newBean(ProdPurchaseIn.class);
        entity.setOrderStatus(SaleStatusEnum.FINISH.getValue());
        QueryWrapper<ProdPurchaseIn> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 未对账
     *
     * @param orderNos
     * @param loginUser
     * @return
     */
    public Integer countBill(List<String> orderNos, LoginUser loginUser) {
        ProdPurchaseIn entity = BeanCreate.newBean(ProdPurchaseIn.class);
        entity.setBillStatus(YesOrNoEnum.NO.getValue());
        QueryWrapper<ProdPurchaseIn> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_no", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return count(queryWrapper);
    }

    public Boolean updateTotalAmount(Long id, BigDecimal total, LoginUser loginUser) {
        ProdPurchaseIn entity = new ProdPurchaseIn();
        entity.setId(id);
        entity.setTotalAmount(total);
        DataOPUtil.editUser(entity, loginUser);
        return updateById(entity);
    }
}
