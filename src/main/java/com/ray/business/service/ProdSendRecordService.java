package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.dto.SaleQueryDTO;
import com.ray.business.table.entity.ProdSendRecord;
import com.ray.business.table.mapper.ProdSendRecordMapper;
import com.ray.business.table.params.sale.SaleGoodsParams;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 加工单物料明细 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdSendRecordService extends ServiceImpl<ProdSendRecordMapper, ProdSendRecord> {
    /**
     * 删除订单商品
     *
     * @param sendCode
     * @param loginUser
     * @return
     */
    public Boolean deleteOrderGoods(String sendCode, LoginUser loginUser) {
        ProdSendRecord entity = new ProdSendRecord();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdSendRecord query = new ProdSendRecord();
        query.setSendCode(sendCode);
        UpdateWrapper<ProdSendRecord> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 列表查询
     *
     * @param sendCode
     * @param loginUser
     * @return
     */
    public List<ProdSendRecord> list(String sendCode, LoginUser loginUser) {
        ProdSendRecord entity = BeanCreate.newBean(ProdSendRecord.class);
        entity.setSendCode(sendCode);
        QueryWrapper<ProdSendRecord> queryWrapper = new QueryWrapper<>(entity);
        //排序
        queryWrapper.orderByAsc("index_sort");
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdSendRecord> page(CommonPage<SaleQueryDTO, Page<ProdSendRecord>> queryParams, LoginUser loginUser) {
        SaleQueryDTO params = queryParams.getEntity();
        ProdSendRecord entity = BeanCreate.newBean(ProdSendRecord.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdSendRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getOrderNoLike()), "order_no", params.getOrderNoLike());
        }
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 查询订单商品
     *
     * @param goodsParams
     * @param sendCode
     * @param loginUser
     * @return
     */
    public ProdSendRecord queryRecord(SaleGoodsParams goodsParams, String sendCode, LoginUser loginUser) {
        ProdSendRecord entity = BeanCreate.newBean(ProdSendRecord.class);
        entity.setSendCode(sendCode);
        entity.setBatchNo(goodsParams.getBatchNo());
        entity.setSerialNumber(goodsParams.getSerialNumber());
        entity.setGoodsCode(goodsParams.getGoodsCode());
        QueryWrapper<ProdSendRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    public List<ProdSendRecord> listAll(List<String> sendCodes, LoginUser loginUser) {
        ProdSendRecord entity = BeanCreate.newBean(ProdSendRecord.class);
        QueryWrapper<ProdSendRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("send_code", sendCodes);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }
}
