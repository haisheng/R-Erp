package com.ray.business.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.dto.SaleBackQueryDTO;
import com.ray.business.table.entity.ProdSaleBackRecord;
import com.ray.business.table.entity.ProdSaleRecord;
import com.ray.business.table.mapper.ProdSaleBackRecordMapper;
import com.ray.business.table.params.sale.SaleGoodsParams;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 销售退回记录明细 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class ProdSaleBackRecordService extends ServiceImpl<ProdSaleBackRecordMapper, ProdSaleBackRecord>  {

    /**
     * 删除订单商品
     * @param backCode
     * @param loginUser
     * @return
     */
    public Boolean deleteOrderGoods( String backCode, LoginUser loginUser) {
        ProdSaleBackRecord entity = new ProdSaleBackRecord();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        ProdSaleBackRecord query = new ProdSaleBackRecord();
        query.setBackCode(backCode);
        UpdateWrapper<ProdSaleBackRecord> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 列表查询
     *
     * @param backCode
     * @param loginUser
     * @return
     */
    public List<ProdSaleBackRecord> list(String backCode, LoginUser loginUser) {
        ProdSaleBackRecord entity = BeanCreate.newBean(ProdSaleBackRecord.class);
        entity.setBackCode(backCode);
        QueryWrapper<ProdSaleBackRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<ProdSaleRecord> page(CommonPage<SaleBackQueryDTO, Page<ProdSaleBackRecord>> queryParams, LoginUser loginUser) {
        SaleBackQueryDTO params = queryParams.getEntity();
        ProdSaleBackRecord entity = BeanCreate.newBean(ProdSaleBackRecord.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<ProdSaleBackRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getBackCodeLike()), "back_code", params.getBackCodeLike());
        }
        return page(queryParams.getPage(), queryWrapper);
    }


    /**
     * 查询已退回数量
     * @param backCode
     * @param saleGoodsParams
     * @param loginUser
     * @return
     */
    public BigDecimal queryBackQuantity(String orderNo, String backCode, SaleGoodsParams saleGoodsParams, LoginUser loginUser) {
        ProdSaleBackRecord entity = BeanCreate.newBean(ProdSaleBackRecord.class);
        entity.setOrderNo(orderNo);
        entity.setBatchNo(saleGoodsParams.getBatchNo());
        entity.setGoodsCode(saleGoodsParams.getGoodsCode());
        entity.setSerialNumber(saleGoodsParams.getSerialNumber());
        entity.setUnit(saleGoodsParams.getUnit());
        QueryWrapper<ProdSaleBackRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.ne(StrUtil.isNotBlank(backCode),"back_code",backCode);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper).stream().map(ProdSaleBackRecord::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public List<ProdSaleBackRecord> listByOrderNo(List<String> orderNos, LoginUser loginUser) {
        ProdSaleBackRecord entity = BeanCreate.newBean(ProdSaleBackRecord.class);
        QueryWrapper<ProdSaleBackRecord> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in(ObjectUtil.isNotEmpty(orderNos), "order_no", orderNos);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }
}
