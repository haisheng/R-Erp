package com.ray.business.service.compose;

import com.ray.business.builder.BusinessVOBuilder;
import com.ray.business.enums.PurchaseStatusEnum;
import com.ray.business.service.ProdSaleBackService;
import com.ray.business.service.ProdSaleService;
import com.ray.business.table.entity.ProdSale;
import com.ray.business.table.entity.ProdSaleBack;
import com.ray.business.table.vo.BusinessVO;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 采购服务
 * @Class: PurcahseService
 * @Package com.ray.business.service.compose
 * @date 2020/6/21 17:10
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
public class SaleService {

    @Autowired
    private ProdSaleService prodSaleService;
    @Autowired
    private ProdSaleBackService prodSaleBackService;


    public List<BusinessVO> querySaleByBIllNo(String billNo, String customerName, LoginUser loginUser) {
        Assert.hasLength(billNo,"参数[billNo]不能为空");
        //获取对账单对应的扣款单
        List<ProdSaleBack> prodSaleBacks = prodSaleBackService.listByBillNo(billNo, PurchaseStatusEnum.FINISH.getValue(),loginUser);
        final Map<String, BigDecimal> decimalMap = prodSaleBacks.stream()
                .collect(Collectors.groupingBy(ProdSaleBack::getOrderNo,Collectors.reducing(BigDecimal.ZERO,ProdSaleBack::getTotalAmount,BigDecimal::add)));

        List<ProdSale> prodBusinesses = prodSaleService.listByBillNo(billNo,loginUser);
        //转换
        return  prodBusinesses.stream().map(prodSale -> {
            return new BusinessVOBuilder().append(prodSale).appendCustomerName(customerName).appendDeductionAmount(decimalMap.get(prodSale.getOrderNo())).bulid();
        }).collect(Collectors.toList());
    }
}
