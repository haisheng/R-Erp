package com.ray.business.service.compose;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.builder.SendGoodsBuilder;
import com.ray.business.builder.SendGoodsExportBuilder;
import com.ray.business.check.OrderSendCheck;
import com.ray.business.service.ProdOrderGoodsService;
import com.ray.business.service.ProdOrderSendService;
import com.ray.business.service.ProdSendRecordService;
import com.ray.business.table.entity.ProdOrderGoods;
import com.ray.business.table.entity.ProdOrderSend;
import com.ray.business.table.entity.ProdSendRecord;
import com.ray.business.table.vo.SendGoodsExportVO;
import com.ray.business.table.vo.SendGoodsVO;
import com.ray.export.request.ExportQueryRequest;
import com.ray.export.runner.RunnerResult;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.exports.DataCount;
import com.ray.export.strategy.exports.DataCountResult;
import com.ray.export.strategy.exports.DataExport;
import com.ray.export.strategy.result.Error;
import com.ray.export.strategy.result.Success;
import com.ray.export.strategy.result.SuccessResult;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SendDataExportService implements DataCount, DataExport, Success, Error {

    @Autowired
    private ProdSendRecordService prodSendRecordService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ProdOrderGoodsService prodOrderGoodsService;
    @Autowired
    private ProdOrderSendService prodOrderSendService;

    @Override
    public DataCountResult count(ExportQueryRequest exportQueryRequest) {
        DataCountResult dataCountResult = new DataCountResult();
        dataCountResult.setCount(1L);
        return dataCountResult;
    }

    @Override
    public List<Object> pageDataQuery(ExportQueryRequest exportQueryRequest, Integer pageNo, Integer pageSize) {
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //分页查询
        List<Object> carModelList = new ArrayList<>();
        ProdOrderSend prodOrderSend = prodOrderSendService.queryOrderSendByOrderSendCode(exportQueryRequest.getTaskKey(), loginUser);
        new OrderSendCheck(prodOrderSend).checkNull("发货单不存在");
        //查询
        List<ProdSendRecord> prodOrderSendRecords = prodSendRecordService.list(exportQueryRequest.getTaskKey(), loginUser);
        Map<String, MaterialModelVO> modelMap = new HashMap<>();
        Map<String, ProdOrderGoods> goodsMap = new HashMap<>();
        List<SendGoodsExportVO> saleGoodsVOS = prodOrderSendRecords.stream().map(prodOrderSendRecord -> {
            MaterialModelVO materialModelVO = modelMap.get(prodOrderSendRecord.getGoodsCode());
            if (ObjectUtil.isNull(materialModelVO)) {
                materialModelVO = goodsService.queryGoodsByCode(prodOrderSendRecord.getGoodsCode(), loginUser);
                modelMap.put(prodOrderSendRecord.getGoodsCode(), materialModelVO);
            }
            ProdOrderGoods orderGoods = goodsMap.get(prodOrderSendRecord.getGoodsCode());
            if (ObjectUtil.isNull(orderGoods)) {
                orderGoods = prodOrderGoodsService.queryOrderGoodsByGoodsCode(prodOrderSend.getOrderNo(), prodOrderSendRecord.getGoodsCode(), loginUser);
                goodsMap.put(prodOrderSendRecord.getGoodsCode(), orderGoods);
            }
            return new SendGoodsExportBuilder().append(prodOrderSendRecord).append(materialModelVO).append(orderGoods).bulid();
        }).collect(Collectors.toList());
        carModelList.addAll(saleGoodsVOS);
        return carModelList;
    }

    @Override
    public List<String> head() {
        return null;
    }


    @Override
    public SuccessResult doSuccessCall(DataModel dataModel) {
        return new SuccessResult(true, null, "导出成功");
    }

    @Override
    public SuccessResult doErrorCall(DataModel dataModel, RunnerResult runnerResult, String errorMsg, Class<?> clazz) {
        log.info("异常:{}", JSON.toJSONString(runnerResult.getResult()));
        return new SuccessResult(true, "异常回调成功");
    }

}
