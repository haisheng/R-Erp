package com.ray.business.service.compose;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ray.business.builder.OrderDeleteIndexBuilder;
import com.ray.business.builder.OrderIndexBuilder;
import com.ray.business.service.ProdOrderDeleteIndexService;
import com.ray.business.service.ProdOrderIndexService;
import com.ray.business.table.entity.ProdOrderDeleteIndex;
import com.ray.business.table.entity.ProdOrderIndex;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author bo shen
 * @Description:
 * @Class: IndexService
 * @Package com.ray.business.service.compose
 * @date 2020/8/2 19:37
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class IndexService {
    @Autowired
    private ProdOrderIndexService prodOrderIndexService;
    @Autowired
    private ProdOrderDeleteIndexService prodOrderDeleteIndexService;

    /**
     * 查询顺序
     * @param orderNo
     * @param loginUser
     * @return
     */
    public synchronized Integer queryIndex(String orderNo,String goodsCode, LoginUser loginUser) {
        //查询可用的退回的顺序
        ProdOrderDeleteIndex prodOrderDeleteIndex =  prodOrderDeleteIndexService.getDeleteIndex(orderNo,goodsCode,loginUser);
       if(ObjectUtil.isNotNull(prodOrderDeleteIndex)){
            log.info("使用删除的顺序:{}",prodOrderDeleteIndex.getIndexSort());
           prodOrderDeleteIndexService.removeById(prodOrderDeleteIndex.getId());
           return  prodOrderDeleteIndex.getIndexSort();
       }
        ProdOrderIndex query = new ProdOrderIndex();
        query.setOrderNo(orderNo);
        query.setGoodsCode(goodsCode);
        QueryWrapper<ProdOrderIndex> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        ProdOrderIndex prodOrderIndex = prodOrderIndexService.getOne(queryWrapper);
        OrderIndexBuilder orderIndexBuilder = new OrderIndexBuilder();
        if(ObjectUtil.isNull(prodOrderIndex)){
            prodOrderIndexService.save(orderIndexBuilder.appendOrderNo(orderNo).appendGoodsCode(goodsCode).appendCreate(loginUser).appendIndex(1).bulid());
        }else {
            prodOrderIndexService.edit(orderIndexBuilder.appendOrderNo(orderNo).appendGoodsCode(goodsCode).appendEdit(loginUser).appendIndex(prodOrderIndex.getIndexSort() +1).bulid(),loginUser);
            return prodOrderIndex.getIndexSort() +1;
        }
        return  1;
    }

    public Boolean saveDeleteIndex(String orderNo, String goodsCode, Integer indexSort, LoginUser loginUser) {
        OrderDeleteIndexBuilder orderIndexBuilder = new OrderDeleteIndexBuilder();
        return   prodOrderDeleteIndexService.save(orderIndexBuilder.appendOrderNo(orderNo).appendGoodsCode(goodsCode).appendCreate(loginUser).appendIndex(indexSort).bulid());
    }
}
