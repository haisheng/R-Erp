package com.ray.business.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.table.entity.ProdBusinessQuickIn;
import com.ray.business.table.mapper.ProdBusinessQuickInMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 加工单快捷入库明细 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-07-05
 */
@Service
public class ProdBusinessQuickInService extends ServiceImpl<ProdBusinessQuickInMapper, ProdBusinessQuickIn>  {

    public List<ProdBusinessQuickIn> listByQuickCode(String quickCode, LoginUser loginUser) {
        ProdBusinessQuickIn entity = BeanCreate.newBean(ProdBusinessQuickIn.class);
        entity.setQuickCode(quickCode);
        QueryWrapper<ProdBusinessQuickIn> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 编辑订单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdBusinessQuickIn entity, LoginUser loginUser) {
        ProdBusinessQuickIn query = new ProdBusinessQuickIn();
        query.setCode(entity.getCode());
        UpdateWrapper<ProdBusinessQuickIn> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addUserDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    public ProdBusinessQuickIn listByQuickCodeAndIndexSort( String quickCode, Integer indexSort, LoginUser loginUser) {
        ProdBusinessQuickIn entity = BeanCreate.newBean(ProdBusinessQuickIn.class);
        entity.setQuickCode(quickCode);
        entity.setIndexSort(indexSort);
        QueryWrapper<ProdBusinessQuickIn> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return  getOne(queryWrapper);
    }

    public ProdBusinessQuickIn listByQuickCodeAndSerialNumber( String quickCode, String serialNumber, LoginUser loginUser) {
        ProdBusinessQuickIn entity = BeanCreate.newBean(ProdBusinessQuickIn.class);
        entity.setQuickCode(quickCode);
        entity.setSerialNumber(serialNumber);
        QueryWrapper<ProdBusinessQuickIn> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return  getOne(queryWrapper);
    }

    public ProdBusinessQuickIn getEntityByCode(String code, LoginUser loginUser) {
        ProdBusinessQuickIn entity = BeanCreate.newBean(ProdBusinessQuickIn.class);
        entity.setCode(code);
        QueryWrapper<ProdBusinessQuickIn> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return  getOne(queryWrapper);
    }
}
