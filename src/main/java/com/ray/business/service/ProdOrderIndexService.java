package com.ray.business.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.business.builder.OrderIndexBuilder;
import com.ray.business.table.entity.ProdErrorType;
import com.ray.business.table.entity.ProdOrderIndex;
import com.ray.business.table.mapper.ProdOrderIndexMapper;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 生产订单 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-07-25
 */
@Service
public class ProdOrderIndexService extends ServiceImpl<ProdOrderIndexMapper, ProdOrderIndex> {

    /**
     * 编辑步骤
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(ProdOrderIndex entity, LoginUser loginUser) {
        ProdOrderIndex query = new ProdOrderIndex();
        query.setOrderNo(entity.getOrderNo());
        query.setGoodsCode(entity.getGoodsCode());
        UpdateWrapper<ProdOrderIndex> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }
}
