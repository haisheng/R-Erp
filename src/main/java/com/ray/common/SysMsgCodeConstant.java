package com.ray.common;

/**
 * @author bo shen
 * @Description:
 * @Class: SysMsgCodeConstant
 * @Package com.ray.common
 * @date 2020/5/27 12:55
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class SysMsgCodeConstant {

    /***系统内部业务异常**/
    public static class Error {
        public static final String ERR10000001 = "ERR10000001";
        public static final String ERR10000002 = "ERR10000002";
        public static final String ERR10000003 = "ERR10000003";
        public static final String ERR10000004 = "ERR10000004";
        public static final String ERR10000005 = "ERR10000005";
        public static final String ERR10000006 = "ERR10000006";
        public static final String ERR10000007 = "ERR10000007";
        public static final String ERR10000008 = "ERR10000008";
        public static final String ERR10000009 = "ERR10000009";
        public static final String ERR10000010 = "ERR10000010";
        public static final String ERR10000011 = "ERR10000011";
        public static final String ERR10000012 = "ERR10000012";
        public static final String ERR10000013 = "ERR10000013";
        public static final String ERR10000014 = "ERR10000014";
        public static final String ERR10000015 = "ERR10000015";
        public static final String ERR10000016 = "ERR10000016";
        public static final String ERR10000017 = "ERR10000017";
        public static final String ERR10000018 = "ERR10000018";
        public static final String ERR10000019 = "ERR10000019";
        public static final String ERR10000020 = "ERR10000020";
        public static final String ERR10000021 = "ERR10000021";
        public static final String ERR10000022 = "ERR10000022";
        public static final String ERR10000023 = "ERR10000023";
        public static final String ERR10000024 = "ERR10000024";
        public static final String ERR10000025 = "ERR10000025";
        public Error() {
        }
    }
}
