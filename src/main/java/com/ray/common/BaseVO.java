package com.ray.common;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author bo shen
 * @Description: 基础对象
 * @Class: BaseVO
 * @Package com.ray.system.table.vo
 * @date 2020/5/27 11:22
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BaseVO {
    @ApiModelProperty(value = "状态  1:启用 0:禁用")
    private Integer status;
    @ApiModelProperty(value = "创建时间")
  //  @JSONField(format = "yyyy-MM-dd HH:mm:ss")
   // @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JSONField(format = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDateTime createTime;
    @ApiModelProperty(value = "创建人编号")
    private String createUserCode;
    @ApiModelProperty(value = "创建人名称")
    private String createUserName;
    @ApiModelProperty(value = "排序")
    private Integer indexSort;
}
