package com.ray.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author bo shen
 * @Description: 基础对象
 * @Class: BaseVO
 * @Package com.ray.system.table.vo
 * @date 2020/5/27 11:22
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BaseParams {
    @ApiModelProperty(value = "状态  1:启用 0:禁用")
    private Integer status;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "开始时间")
    private LocalDateTime endTime;

    /**
     * 状态列表
     */
    private List<String> includeStatus;



}
