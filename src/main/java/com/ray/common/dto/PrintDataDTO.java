package com.ray.common.dto;

import com.ray.template.Template;
import com.ray.template.TemplateProp;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description:
 * @Class: PrintDataDTO
 * @Package com.ray.common.dto
 * @date 2020/7/6 8:47
 * @company <p>快速开发</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@Template(code = "print")
public class PrintDataDTO {

    /**
     * 物料名称
     */
    @TemplateProp(property = "materialName", name = "名称")
    private String materialName;

    /**
     * 别名
     */
    @TemplateProp(property = "materialNameEn", name = "别名")
    private String materialNameEn;


    /**
     * 物料类型
     */
    @TemplateProp(property = "materialTypeName", name = "类型")
    private String materialTypeName;

    /**
     * 单位
     */
    @TemplateProp(property = "unit", name = "单位")
    private String unit;

    /**
     * 重量
     */
    @TemplateProp(property = "weight", name = "克重")
    private String weight;


    /**
     * 毛重
     */
    @TemplateProp(property = "goodsWeight", name = "毛重")
    private BigDecimal goodsWeight;

    /**
     * 净重
     */
    @TemplateProp(property = "goodsWeight", name = "净重")
    private BigDecimal goodsNetWeight;
    /**
     * 缸号
     */
    @TemplateProp(property = "gangNo", name = "缸号")
    private String gangNo;
    /**
     * 体积
     */
    @TemplateProp(property = "volume", name = "体积")
    private String volume;

    /**
     * 规格名称
     */
    @TemplateProp(property = "modelName", name = "色号")
    private String modelName;


    /**
     * 条形码
     */
    @TemplateProp(property = "barCode", name = "条形码")
    private String barCode;

    /**
     * 规格属性
     */
    @TemplateProp(property = "modelProp", name = "门幅")
    private String modelProp;


    /**
     * 客户规格
     */
    @TemplateProp(property = "customerModelName", name = "客户型号")
    private String customerModelName;

    /**
     * 客户规格属性
     */
    @TemplateProp(property = "customerModelProp", name = "客户色号")
    private String customerModelProp;

    /**
     * 数量
     */
    @TemplateProp(property = "quantity", name = "数量")
    private BigDecimal quantity;

    /**
     * 数量
     */
    @TemplateProp(property = "deleteQuantity", name = "扣减数量")
    private BigDecimal deleteQuantity;

    /**
     * 数量
     */
    @TemplateProp(property = "realQuantity", name = "实际数量")
    private BigDecimal realQuantity;

    /**
     * 序列号
     */
    @TemplateProp(property = "serialNumber", name = "序列号")
    private String serialNumber;

    /**
     * 顺序
     */
    @TemplateProp(property = "indexSort", name = "顺序")
    private Integer indexSort;

}
