package com.ray.common.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author bo shen
 * @Description: 基础
 * @Class: BaseDTO
 * @Package com.ray.common.dto
 * @date 2020/6/1 14:51
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BaseDTO {
    /*** 状态*/
    private  Integer status;
    /*** 开始时间*/
    private LocalDateTime startTime;
    /*** 结束时间 */
    private LocalDateTime endTime;
    /**
     * 状态列表
     */
    private List<String> includeStatus;
}
