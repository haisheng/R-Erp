package com.ray.common;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description:
 * @Class: AmountUpdate
 * @Package com.ray.common
 * @date 2020/7/27 11:34
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class AmountUpdate {

    private String businessCode;

    private BigDecimal amount;

    private Integer updateVersion;


}
