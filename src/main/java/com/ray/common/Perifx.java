package com.ray.common;

/**
 * @author bo shen
 * @Description: 前缀
 * @Class: Perifx
 * @Package com.ray.common
 * @date 2020/5/27 11:31
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class Perifx {

    /***权限 前缀**/
    public final  static String AUTH = "AUTH:";
    /***职位 前缀**/
    public final  static String POSITION_CODE = "PO";
    /***应用 前缀**/
    public final  static String APP_CODE = "APP";
    /***公司 前缀**/
    public final  static String COMPANY_CODE = "CO";
    /***用户 前缀**/
    public final  static String USER_CODE = "UC";
    /***菜单 前缀**/
    public final  static String MENU_CODE = "MC";
    /***菜单 前缀**/
    public final  static String ROLE_CODE = "RC";
    /***客户 前缀**/
    public final  static String CUSTOMER_CODE = "CC";
    /***通用 前缀**/
    public final  static String CODE = "C";
    /***入库 前缀**/
    public final  static String IN_CODE = "IO";
    /***出库 前缀**/
    public final  static String OUT_CODE = "CO";
    /***订单 前缀**/
    public final  static String ORDER_CODE = "DD";
    /***采购 前缀**/
    public final  static String PURCHASE_CODE = "CG";
    /***销售 前缀**/
    public final  static String SALE_CODE = "XS";
    /***采购-退货 前缀**/
    public final  static String PURCHASE_BACK_CODE = "CT";
    /***采购-入库 前缀**/
    public final  static String PURCHASE_IN_CODE = "PI";
    /***销售-退货 前缀**/
    public final  static String SALE_BACK_CODE = "XT";
    /***销售-出库 前缀**/
    public final  static String SALE_OUT_CODE = "SO";
    /***商品记录 前缀**/
    public final  static String RECORD_CODE = "GC";
    /***加工业务记录 前缀**/
    public final  static String BUSINESS_CODE = "JG";
    /***发货 前缀**/
    public final  static String SEND_CODE = "FH";
    /***对账 前缀**/
    public final  static String BILL_CODE = "DZ";
    /***退料 前缀**/
    public final  static String BUSINESS_BACK_CODE = "TL";

}
