package com.ray.common;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author bo shen
 * @Description: 统计对象
 * @Class: CountParams
 * @Package com.ray.common
 * @date 2020/6/15 9:03
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class CountParams {

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private Integer status;

    private String orderStatus;

    private String tenantCode;

    private String orderNo;

    private String customerCode;
}
