package com.ray.common;

/**
 * @author bo shen
 * @Description:
 * @Class: SysConstant
 * @Package com.ray.common
 * @date 2020/5/27 12:55
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class SysConstant {


    /***图片验证码失效时间   3分钟***/
    public static final long IMAGE_TIME = 3 * 60;
    /***登录失效时间   2小时***/
    public static final long LOGIN_TIME = 2 * 60 * 60;
    /***树根节点***/
    public static final String MENU_ROOT = "0";
}
