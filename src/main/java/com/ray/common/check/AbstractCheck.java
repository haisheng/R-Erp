package com.ray.common.check;


import cn.hutool.core.util.StrUtil;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description:AbstractCheck
 * @Class: AbstractBuilder
 * @Package com.ray.builder
 * @date 2020/3/11 14:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public  class AbstractCheck<T> {
    /***对象**/
    protected T entity;

    public AbstractCheck(T entity) {
        this.entity = entity;
    }

    public AbstractCheck checkNull(String messageCode) {
        if(entity == null){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public AbstractCheck checkNotNull(String messageCode) {
        if(entity != null){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

}
