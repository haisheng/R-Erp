package com.ray.common.builder;

import java.lang.reflect.InvocationTargetException;

/**
 * @author bo shen
 * @Description: 凭接接口
 * @Class: Append
 * @Package com.ray.builder
 * @date 2018/10/9 15:50
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface Append<T> {
    /**
     * 添加一个熟悉
      * @param field 字段
     * @param value 值
     * @return
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
   T append(String field, Object value) throws IllegalAccessException ,InvocationTargetException;

}
