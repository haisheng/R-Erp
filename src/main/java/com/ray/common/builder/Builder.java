package com.ray.common.builder;

/**
 * @author bo shen
 * @Description: 统一默认构造器接口
 * @Class: Builder
 * @Package com.ray.builder
 * @date 2018/10/9 15:42
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface Builder<T> {
    /***
     * 默认构造方法
     * @return 构造对象
     */
    T bulid();

    /**
     * 构造一个字符串
     * @return
     */
    String bulidString();
}
