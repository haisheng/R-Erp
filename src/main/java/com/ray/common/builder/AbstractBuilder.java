package com.ray.common.builder;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.ray.common.BeanField;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.enums.YesOrNoEnum;

/**
 * @author bo shen
 * @Description:
 * @Class: AbstractBuilder
 * @Package com.ray.builder
 * @date 2020/3/11 14:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class AbstractBuilder<T, B> implements Builder<T> {
    /***对象**/
    protected T entity;
    /***构建对象**/
    protected B builder;

    public AbstractBuilder(T entity) {
        this.entity = entity;
    }

    public void setBuilder(B builder) {
        this.builder = builder;
    }

    @Override
    public T bulid() {
        return entity;
    }

    @Override
    public String bulidString() {
        if (entity != null) {
            return JSON.toJSONString(entity);
        }
        return null;
    }

    public String getCode() {
        return null;
    }

    /**
     * 添加新增用户
     *
     * @param loginUser
     * @return
     */
    public B appendCreate(LoginUser loginUser) {
        BeanUtil.setFieldValue(entity, BeanField.createUserName, loginUser.getUserName());
        BeanUtil.setFieldValue(entity, BeanField.createUserCode, loginUser.getUserCode());
        BeanUtil.setFieldValue(entity, BeanField.isDeleted, DeleteEnum.USE.getValue());
        BeanUtil.setFieldValue(entity, BeanField.tenantCode, loginUser.getCompanyCode());
        BeanUtil.setFieldValue(entity, BeanField.organizationCode, loginUser.getDeptCode());
        BeanUtil.setFieldValue(entity, BeanField.ownerCode, loginUser.getUserCode());
        return builder;
    }

    /**
     * 添加编辑用户
     *
     * @param loginUser
     * @return
     */
    public B appendEdit(LoginUser loginUser) {
        BeanUtil.setFieldValue(entity, BeanField.updateUserName, loginUser.getUserName());
        BeanUtil.setFieldValue(entity, BeanField.updateUserCode, loginUser.getUserCode());
        return builder;
    }

    /**
     * 删除状态
     * @return
     */
    public B delete() {
        BeanUtil.setFieldValue(entity, BeanField.isDeleted, DeleteEnum.DELETE.getValue());
        return builder;
    }

    /**
     * 状态开启
     * @return
     */
    public B open() {
        BeanUtil.setFieldValue(entity, BeanField.status, YesOrNoEnum.YES.getValue());
        return builder;
    }

    /**
     * 状态关闭
     * @return
     */
    public B close() {
        BeanUtil.setFieldValue(entity, BeanField.status, YesOrNoEnum.NO.getValue());
        return builder;
    }
}
