package com.ray.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bo shen
 * @Description:
 * @Class: BooleanValue
 * @Package com.ray.common
 * @date 2020/8/2 20:18
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ObjectValue<T> {

    private T value;
}
