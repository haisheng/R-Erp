package com.ray.common;

import com.baomidou.mybatisplus.annotation.TableLogic;

import java.time.LocalDateTime;

/**
 * @author bo shen
 * @Description: 对象属性
 * @Class: BeanField
 * @Package com.ray.common
 * @date 2020/5/27 13:10
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class BeanField {

    /*** 状态 status*/
    public static final String status = "status";

    /**  是否删除 */
    public static final String isDeleted = "isDeleted";

    /**
     * 创建时间
     */
    public static final String createTime = "createTime";

    /**
     * 创建人编码
     */
    public static final String createUserCode = "createUserCode";

    /**
     * 创建人名称
     */
    public static final String createUserName = "createUserName";

    /**
     * 更新时间
     */
    public static final String updateTime = "updateTime";

    /**
     * 更新用户编码
     */
    public static final String updateUserCode = "updateUserCode";

    /**
     * 更新用户名称
     */
    public static final String updateUserName = "updateUserName";

    /**
     * 租户编码
     */
    public static final String tenantCode = "tenantCode";

    /**
     * 用户编码
     */
    public static final String ownerCode = "ownerCode";

    /**
     * 组织编码
     */
    public static final String organizationCode = "organizationCode";

    /**
     * 系统编码
     */
    public static final String appCode = "appCode";


    /**
     * 上级公司
     */
    public static final String parentCompanyCode = "parentCompanyCode";


    /**
     * 内部类
     */
    public  static  class DbField {

        public static final String IS_DELETED = "is_deleted";
        public static final String TENANT_CODE = "tenant_code";
        public static final String OWNER_CODE = "owner_code";
        public static final String ORGANIZATION_CODE = "organization_code";
        public static final String COMAPNY_CODE = "comapny_code";
        public static final String DEPT_CODE = "dept_code";

    }
}
