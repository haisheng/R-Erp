package com.ray.wms.check;

import com.ray.common.check.AbstractCheck;
import com.ray.wms.table.entity.WmsWarehouse;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: WarehouseCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class WarehouseCheck extends AbstractCheck<WmsWarehouse> {


    public WarehouseCheck(WmsWarehouse entity) {
        super(entity);
    }

    @Override
    public WarehouseCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }


}
