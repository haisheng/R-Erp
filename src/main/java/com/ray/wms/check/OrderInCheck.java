package com.ray.wms.check;

import cn.hutool.core.util.StrUtil;
import com.ray.common.check.AbstractCheck;
import com.ray.wms.enums.OrderStatusEnum;
import com.ray.wms.table.entity.WmsOrderIn;
import com.ray.wms.table.entity.WmsWarehouse;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: CustomerCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class OrderInCheck extends AbstractCheck<WmsOrderIn> {


    public OrderInCheck(WmsOrderIn entity) {
        super(entity);
    }

    @Override
    public OrderInCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }

    /**
     * 是否可以操作
     * @return
     */
    public OrderInCheck canDoCheck(String messageCode) {
        //已处理   已作废的不能操作
        if(StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.DO.getValue())
                || StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.INVALID.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 是否可以取消
     * @return
     */
    public OrderInCheck cancelCheck(String messageCode) {
        //已操作的才能取消
        if(!StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.DO.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 是否可以取消
     * @return
     */
    public OrderInCheck invalidCheck(String messageCode) {
        if(!StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.UN_DO.getValue())&&
                !StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.CANCEL.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
