package com.ray.wms.check;

import com.ray.common.check.AbstractCheck;
import com.ray.wms.table.entity.WmsWarehouse;
import com.ray.wms.table.entity.WmsWarehouseStockDetail;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: WarehouseCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class StockDetailCheck extends AbstractCheck<WmsWarehouseStockDetail> {


    public StockDetailCheck(WmsWarehouseStockDetail entity) {
        super(entity);
    }

    @Override
    public StockDetailCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }


    public StockDetailCheck checkQuantity( BigDecimal quantity, String messageCode) {
        if(quantity.compareTo(entity.getQuantity()) > 0){
         log.info("库存数量:{},操作数量:{}",entity.getQuantity(),quantity);
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
