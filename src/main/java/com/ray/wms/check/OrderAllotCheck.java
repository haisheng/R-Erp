package com.ray.wms.check;

import cn.hutool.core.util.StrUtil;
import com.ray.common.check.AbstractCheck;
import com.ray.wms.enums.OrderStatusEnum;
import com.ray.wms.table.entity.WmsOrderAllot;
import com.ray.wms.table.entity.WmsOrderIn;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: CustomerCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class OrderAllotCheck extends AbstractCheck<WmsOrderAllot> {


    public OrderAllotCheck(WmsOrderAllot entity) {
        super(entity);
    }

    @Override
    public OrderAllotCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }

    /**
     * 是否可以操作
     * @return
     */
    public OrderAllotCheck canDoCheck(String messageCode) {
        //已处理   已作废的不能操作
        if(StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.DO.getValue())
                || StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.INVALID.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 是否可以取消
     * @return
     */
    public OrderAllotCheck cancelCheck(String messageCode) {
        //已操作的才能取消
        if(!StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.DO.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 是否可以取消
     * @return
     */
    public OrderAllotCheck invalidCheck(String messageCode) {
        //已取消的才能 作废
        if(!StrUtil.equals(entity.getOrderStatus(), OrderStatusEnum.CANCEL.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
