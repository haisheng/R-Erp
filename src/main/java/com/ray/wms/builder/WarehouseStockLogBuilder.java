package com.ray.wms.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.common.builder.AbstractBuilder;
import com.ray.wms.table.dto.GoodsDTO;
import com.ray.wms.table.entity.WmsWarehouseStockLog;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

/**
 * @author bo shen
 * @Description: Warehouse 构造器
 * @Class: WarehouseBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class WarehouseStockLogBuilder extends AbstractBuilder<WmsWarehouseStockLog, WarehouseStockLogBuilder> {

    public WarehouseStockLogBuilder() {
        super(new WmsWarehouseStockLog());
        super.setBuilder(this);
    }

    @Override
    public WmsWarehouseStockLog bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public WarehouseStockLogBuilder append(GoodsDTO goodsDTO) {
        if (ObjectUtil.isNotNull(goodsDTO)) {
            entity.setGoodsCode(goodsDTO.getGoodsCode());
            entity.setQuantity(goodsDTO.getQuantity());
            entity.setOrderNo(goodsDTO.getOrderNo());
            entity.setInOutType(goodsDTO.getInOutType());
            entity.setWarehouseCode(goodsDTO.getWarehouseCode());
            entity.setSerialNumber(goodsDTO.getSerialNumber());
            entity.setBatchNo(goodsDTO.getBatchNo());
            entity.setInTime(LocalDateTime.now());
            entity.setUnit(goodsDTO.getUnit());
            entity.setTotal(goodsDTO.getTotal());
        }
        return this;
    }


}
