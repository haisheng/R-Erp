package com.ray.wms.builder;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.business.table.entity.*;
import com.ray.common.builder.AbstractBuilder;
import com.ray.wms.table.dto.GoodsDTO;
import com.ray.wms.table.entity.WmsOrderRecord;
import com.ray.wms.table.entity.WmsWarehouseStockDetail;
import com.ray.woodencreate.beans.CommonValue;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Warehouse 构造器
 * @Class: WarehouseBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class GoodsBuilder extends AbstractBuilder<GoodsDTO, GoodsBuilder> {

    public GoodsBuilder() {
        super(new GoodsDTO());
        super.setBuilder(this);
    }

    @Override
    public GoodsDTO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public GoodsBuilder append(WmsOrderRecord wmsOrderRecord) {
        if (ObjectUtil.isNotNull(wmsOrderRecord)) {
            entity.setBatchNo(StrUtil.emptyToNull(wmsOrderRecord.getBatchNo()));
            entity.setGoodsCode(wmsOrderRecord.getGoodsCode());
            entity.setQuantity(wmsOrderRecord.getQuantity());
            entity.setUnit(wmsOrderRecord.getUnit());
            entity.setTotal(wmsOrderRecord.getTotal());
            entity.setSerialNumber(StrUtil.emptyToNull(wmsOrderRecord.getSerialNumber()));
        }
        return this;
    }

    public GoodsBuilder appendWarehouseCode(String warehouseCode) {
        entity.setWarehouseCode(warehouseCode);
        return this;
    }

    public GoodsBuilder appendOrderNo(String orderNo) {
        if (StrUtil.isNotBlank(orderNo)) {
            entity.setOrderNo(orderNo);
        } else {
            entity.setOrderNo(CommonValue.DEFAULT_ZERO);
        }
        return this;
    }

    public GoodsBuilder appendInOutType(String type) {
        entity.setInOutType(type);
        return this;
    }

    public GoodsBuilder cancel() {
        entity.setQuantity(new BigDecimal(0).subtract(entity.getQuantity()));
        entity.setTotal(0 - entity.getTotal());
        return this;
    }

    public GoodsBuilder append(ProdPurchaseBackRecord prodPurchaseBackRecord) {
        if (ObjectUtil.isNotNull(prodPurchaseBackRecord)) {
            entity.setBatchNo(StrUtil.emptyToNull(prodPurchaseBackRecord.getBatchNo()));
            entity.setUnit(prodPurchaseBackRecord.getUnit());
            entity.setGoodsCode(prodPurchaseBackRecord.getGoodsCode());
            entity.setQuantity(prodPurchaseBackRecord.getQuantity());
            entity.setTotal(prodPurchaseBackRecord.getTotal());
            entity.setSerialNumber(StrUtil.emptyToNull(prodPurchaseBackRecord.getSerialNumber()));
        }
        return this;
    }


    public GoodsBuilder append(ProdSaleBackRecord prodSaleBackRecord) {
        if (ObjectUtil.isNotNull(prodSaleBackRecord)) {
            entity.setUnit(prodSaleBackRecord.getUnit());
            entity.setBatchNo(StrUtil.emptyToNull(prodSaleBackRecord.getBatchNo()));
            entity.setGoodsCode(prodSaleBackRecord.getGoodsCode());
            entity.setQuantity(prodSaleBackRecord.getQuantity());
            entity.setTotal(prodSaleBackRecord.getTotal());
            entity.setSerialNumber(StrUtil.emptyToNull(prodSaleBackRecord.getSerialNumber()));

        }
        return this;
    }

    public GoodsBuilder append(ProdBusinessIn prodBusinessIn) {
        if (ObjectUtil.isNotNull(prodBusinessIn)) {
            entity.setBatchNo(StrUtil.emptyToNull(prodBusinessIn.getBatchNo()));
            entity.setGoodsCode(prodBusinessIn.getGoodsCode());
            entity.setUnit(prodBusinessIn.getUnit());
            entity.setTotal(prodBusinessIn.getTotal());
            entity.setQuantity(prodBusinessIn.getQuantity());
            entity.setSerialNumber(StrUtil.emptyToNull(prodBusinessIn.getSerialNumber()));
        }
        return this;
    }

    public GoodsBuilder append(ProdBusinessOut prodBusinessOut) {
        if (ObjectUtil.isNotNull(prodBusinessOut)) {
            entity.setUnit(prodBusinessOut.getUnit());
            entity.setBatchNo(StrUtil.emptyToNull(prodBusinessOut.getBatchNo()));
            entity.setGoodsCode(prodBusinessOut.getGoodsCode());
            entity.setTotal(prodBusinessOut.getTotal());
            entity.setQuantity(prodBusinessOut.getQuantity());
            entity.setSerialNumber(StrUtil.emptyToNull(prodBusinessOut.getSerialNumber()));
        }
        return this;
    }

    public GoodsBuilder append(ProdSendRecord prodOrderSendRecord) {
        if (ObjectUtil.isNotNull(prodOrderSendRecord)) {
            entity.setBatchNo(StrUtil.emptyToNull(prodOrderSendRecord.getBatchNo()));
            entity.setGoodsCode(prodOrderSendRecord.getGoodsCode());
            entity.setQuantity(prodOrderSendRecord.getQuantity());
            entity.setSerialNumber(StrUtil.emptyToNull(prodOrderSendRecord.getSerialNumber()));
            entity.setTotal(prodOrderSendRecord.getTotal());
            entity.setUnit(prodOrderSendRecord.getUnit());
        }
        return this;
    }

    public GoodsBuilder append(WmsWarehouseStockDetail detail) {
        if (ObjectUtil.isNotNull(detail)) {
            entity.setBatchNo(StrUtil.emptyToNull(detail.getBatchNo()));
            entity.setGoodsCode(detail.getGoodsCode());
            entity.setQuantity(detail.getQuantity());
            entity.setSerialNumber(StrUtil.emptyToNull(detail.getSerialNumber()));
            entity.setUnit(detail.getUnit());
            entity.setTotal(detail.getTotal());
        }
        return this;
    }

    public GoodsBuilder appendQuantity(BigDecimal quantity) {
        entity.setQuantity(quantity);
        return this;
    }

    public GoodsBuilder append(ProdBusinessBackIn prodBusinessBackIn) {
        if (ObjectUtil.isNotNull(prodBusinessBackIn)) {
            entity.setBatchNo(StrUtil.emptyToNull(prodBusinessBackIn.getBatchNo()));
            entity.setTotal(prodBusinessBackIn.getTotal());
            entity.setGoodsCode(prodBusinessBackIn.getGoodsCode());
            entity.setQuantity(prodBusinessBackIn.getQuantity());
            entity.setSerialNumber(StrUtil.emptyToNull(prodBusinessBackIn.getSerialNumber()));
            entity.setUnit(prodBusinessBackIn.getUnit());
        }
        return this;
    }

    public GoodsBuilder append(ProdPurchaseInRecord prodPurchaseInRecord) {
        if (ObjectUtil.isNotNull(prodPurchaseInRecord)) {
            entity.setTotal(prodPurchaseInRecord.getTotal());
            entity.setBatchNo(StrUtil.emptyToNull(prodPurchaseInRecord.getBatchNo()));
            entity.setGoodsCode(prodPurchaseInRecord.getGoodsCode());
            entity.setQuantity(prodPurchaseInRecord.getQuantity());
            entity.setSerialNumber(StrUtil.emptyToNull(prodPurchaseInRecord.getSerialNumber()));
            entity.setUnit(prodPurchaseInRecord.getUnit());
            entity.setTotal(prodPurchaseInRecord.getTotal());
        }
        return this;
    }

    public GoodsBuilder append(ProdSaleOutRecord prodSaleOutRecord) {
        if (ObjectUtil.isNotNull(prodSaleOutRecord)) {
            entity.setBatchNo(StrUtil.emptyToNull(prodSaleOutRecord.getBatchNo()));
            entity.setGoodsCode(prodSaleOutRecord.getGoodsCode());
            entity.setTotal(prodSaleOutRecord.getTotal());
            entity.setQuantity(prodSaleOutRecord.getQuantity());
            entity.setSerialNumber(StrUtil.emptyToNull(prodSaleOutRecord.getSerialNumber()));
            entity.setUnit(prodSaleOutRecord.getUnit());
        }
        return this;
    }
}
