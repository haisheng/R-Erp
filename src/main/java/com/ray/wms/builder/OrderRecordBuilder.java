package com.ray.wms.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import com.ray.wms.enums.OrderStatusEnum;
import com.ray.wms.table.dto.GoodsDTO;
import com.ray.wms.table.dto.OrderCreateDTO;
import com.ray.wms.table.entity.WmsOrderIn;
import com.ray.wms.table.entity.WmsOrderRecord;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Warehouse 构造器
 * @Class: WarehouseBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class OrderRecordBuilder extends AbstractBuilder<WmsOrderRecord, OrderRecordBuilder> {

    public OrderRecordBuilder() {
        super(new WmsOrderRecord());
        super.setBuilder(this);
    }

    @Override
    public WmsOrderRecord bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public OrderRecordBuilder append(GoodsDTO goodsDTO) {
        if (ObjectUtil.isNotNull(goodsDTO)) {
            entity.setBatchNo(goodsDTO.getBatchNo());
            entity.setGoodsCode(goodsDTO.getGoodsCode());
            entity.setSerialNumber(goodsDTO.getSerialNumber());
            entity.setQuantity(goodsDTO.getQuantity());
            entity.setOrderNo(goodsDTO.getOrderNo());
            entity.setUnit(goodsDTO.getUnit());
            entity.setTotal(goodsDTO.getTotal());
        }
        return this;
    }


    public OrderRecordBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }


    @Override
    public String getCode() {
        return entity.getOrderNo();
    }
}
