package com.ray.wms.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import com.ray.wms.table.entity.WmsWarehouse;
import com.ray.wms.table.params.warehouse.WarehouseCreateParams;
import com.ray.wms.table.params.warehouse.WarehouseEditParams;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Warehouse 构造器
 * @Class: WarehouseBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class WarehouseBuilder extends AbstractBuilder<WmsWarehouse, WarehouseBuilder> {

    public WarehouseBuilder() {
        super(new WmsWarehouse());
        super.setBuilder(this);
        entity.setWarehouseCode(CodeCreateUtil.getCode(Perifx.CODE));
        log.info("获得仓库编码:{}", entity.getWarehouseCode());
    }

    @Override
    public WmsWarehouse bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public WarehouseBuilder append(WarehouseCreateParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    /**
     *  添加状态
     * @return
     */
    public WarehouseBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }
    /**
     *  添加编码
     * @return
     */
    public WarehouseBuilder appendCode(String code) {
        entity.setWarehouseCode(code);
        return this;
    }
    /**
     * 构建创建信息
     * @param editParams
     * @return
     */
    public WarehouseBuilder append(WarehouseEditParams editParams) {
        if(ObjectUtil.isNotNull(editParams)){
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getWarehouseCode();
    }



}
