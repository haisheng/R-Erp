package com.ray.wms.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseMaterial;
import com.ray.base.table.entity.BaseMaterialModel;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.common.builder.AbstractBuilder;
import com.ray.wms.table.entity.*;
import com.ray.wms.table.vo.order.GoodsVO;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author bo shen
 * @Description: GoodsVO 构造器
 * @Class: GoodsVOBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class GoodsVOBuilder extends AbstractBuilder<GoodsVO, GoodsVOBuilder> {

    public GoodsVOBuilder() {
        super(new GoodsVO());
        super.setBuilder(this);
    }

    @Override
    public GoodsVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public GoodsVOBuilder append(WmsOrderRecord wmsOrderRecord) {
        if (ObjectUtil.isNotNull(wmsOrderRecord)) {
            BeanUtil.copyProperties(wmsOrderRecord, entity);
        }
        return this;
    }

    public GoodsVOBuilder append(BaseMaterialModel materialModel) {
        if (ObjectUtil.isNotNull(materialModel)) {
            entity.setGoodsName(materialModel.getModelName());
            entity.setName(materialModel.getName());
            entity.setBarCode(materialModel.getBarCode());
            entity.setModelProp(materialModel.getModelProp());
        }
        return this;
    }

    public GoodsVOBuilder append(BaseMaterial baseMaterial) {
        if (ObjectUtil.isNotNull(baseMaterial)) {
            entity.setMaterialName(baseMaterial.getMaterialName());
            entity.setUnit(baseMaterial.getUnit());
            entity.setMaterialType(baseMaterial.getMaterialType());
            entity.setMaterialNameEn(baseMaterial.getMaterialNameEn());
        }
        return this;
    }

    public GoodsVOBuilder append(WmsWarehouse wmsWarehouse) {
        if (ObjectUtil.isNotNull(wmsWarehouse)) {
            entity.setWarehouseName(wmsWarehouse.getWarehouseName());
        }
        return this;
    }

    public GoodsVOBuilder append(WmsWarehouseStock warehouseStock) {
        if (ObjectUtil.isNotNull(warehouseStock)) {
            BeanUtil.copyProperties(warehouseStock, entity);
        }
        return this;
    }

    public GoodsVOBuilder append(WmsWarehouseStockDetail warehouseStockDetail) {
        if (ObjectUtil.isNotNull(warehouseStockDetail)) {
            BeanUtil.copyProperties(warehouseStockDetail, entity);
        }
        return this;
    }

    public GoodsVOBuilder append(WmsWarehouseStockLog warehouseStockLog) {
        if (ObjectUtil.isNotNull(warehouseStockLog)) {
            BeanUtil.copyProperties(warehouseStockLog, entity);
        }
        return this;
    }

    public GoodsVOBuilder append(MaterialModelVO materialModel) {
        if (ObjectUtil.isNotNull(materialModel)) {
            BeanUtil.copyProperties(materialModel, entity);
            entity.setGoodsName(materialModel.getModelName());
            entity.setGoodsCode(materialModel.getModelCode());
            entity.setModelProp(materialModel.getModelProp());
            entity.setModelName(materialModel.getModelName());
        }
        return this;
    }

    public GoodsVOBuilder appendTime(LocalDateTime createTime) {
        entity.setCreateTime(createTime);
        return this;
    }

    public GoodsVOBuilder appendUser(String userName) {
        entity.setCreateUserName(userName);
        return this;
    }

    public GoodsVOBuilder appendPrice(BigDecimal price) {
        entity.setPrice(price);
        return this;
    }
}
