package com.ray.wms.builder;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import com.ray.wms.enums.OrderStatusEnum;
import com.ray.wms.table.dto.OrderCreateDTO;
import com.ray.wms.table.entity.WmsOrderIn;
import com.ray.woodencreate.beans.CommonValue;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Warehouse 构造器
 * @Class: WarehouseBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class OrderInBuilder extends AbstractBuilder<WmsOrderIn, OrderInBuilder> {

    public OrderInBuilder() {
        super(new WmsOrderIn());
        super.setBuilder(this);
        entity.setOrderNo(CodeCreateUtil.getCode(Perifx.IN_CODE));
    }

    @Override
    public WmsOrderIn bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public OrderInBuilder append(OrderCreateDTO orderCreateDTO) {
        if (ObjectUtil.isNotNull(orderCreateDTO)) {
            entity.setWarehouseCode(orderCreateDTO.getWarehouseCode());
            entity.setBusinessCode(orderCreateDTO.getBusinessCode());
            if(StrUtil.isNotBlank(orderCreateDTO.getBusinessOrderNo())){
                entity.setBusinessOrderNo(orderCreateDTO.getBusinessOrderNo());
            }else {
                entity.setBusinessOrderNo(CommonValue.DEFAULT_ZERO);
            }
        }
        entity.setOrderStatus(OrderStatusEnum.UN_DO.getValue());
        return this;
    }

    public OrderInBuilder appendWarehouseCode(String warehouseCode) {
        entity.setWarehouseCode(warehouseCode);
        return this;
    }

    public OrderInBuilder appendOrderNo(String orderNo) {
        entity.setOrderNo(orderNo);
        return this;
    }


    @Override
    public String getCode() {
        return entity.getOrderNo();
    }
}
