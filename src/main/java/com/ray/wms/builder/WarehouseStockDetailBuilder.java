package com.ray.wms.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import com.ray.wms.table.dto.GoodsDTO;
import com.ray.wms.table.entity.WmsWarehouseStock;
import com.ray.wms.table.entity.WmsWarehouseStockDetail;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Warehouse 构造器
 * @Class: WarehouseBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class WarehouseStockDetailBuilder extends AbstractBuilder<WmsWarehouseStockDetail, WarehouseStockDetailBuilder> {

    public WarehouseStockDetailBuilder() {
        super(new WmsWarehouseStockDetail());
        super.setBuilder(this);
        entity.setStockCode(CodeCreateUtil.getCode(Perifx.CODE));
    }

    @Override
    public WmsWarehouseStockDetail bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public WarehouseStockDetailBuilder append(GoodsDTO goodsDTO) {
        if (ObjectUtil.isNotNull(goodsDTO)) {
            entity.setGoodsCode(goodsDTO.getGoodsCode());
            entity.setQuantity(goodsDTO.getQuantity());
            entity.setWarehouseCode(goodsDTO.getWarehouseCode());
            entity.setSerialNumber(goodsDTO.getSerialNumber());
            entity.setBatchNo(goodsDTO.getBatchNo());
            entity.setBusinessOrderNo(goodsDTO.getOrderNo());
            entity.setUnit(goodsDTO.getUnit());
            entity.setTotal(goodsDTO.getTotal());
        }
        return this;
    }


}
