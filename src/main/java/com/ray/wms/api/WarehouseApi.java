package com.ray.wms.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.common.SysMsgCodeConstant;
import com.ray.system.builder.CommonPageBuilder;
import com.ray.validate.support.utils.ValidateUtil;
import com.ray.wms.builder.WarehouseBuilder;
import com.ray.wms.check.WarehouseCheck;
import com.ray.wms.service.WmsWarehouseService;
import com.ray.wms.table.dto.WarehouseQueryDTO;
import com.ray.wms.table.entity.WmsWarehouse;
import com.ray.wms.table.params.warehouse.WarehouseCreateParams;
import com.ray.wms.table.params.warehouse.WarehouseEditParams;
import com.ray.wms.table.params.warehouse.WarehouseQueryParams;
import com.ray.wms.table.vo.warehouse.WarehouseVO;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.Assert;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 仓库服务
 * @Class: WarehouseApi
 * @Package com.ray.wms.api
 * @date 2020/6/2 16:29
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class WarehouseApi {

    @Autowired
    private WmsWarehouseService wmsWarehouseService;


    /**
     * 查询产品类型列表信息 分页  非删除的
     * @param queryParams
     * @return
     */
    public Result<IPage<WarehouseVO>> pageWarehouses(CommonPage<WarehouseQueryParams,Page<WarehouseVO>> queryParams){
        Assert.notNull(queryParams, SysMsgCodeConstant.Error.ERR10000001);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        CommonPageBuilder<WarehouseQueryDTO,WmsWarehouse> commonPageBuilder = new CommonPageBuilder<>(WarehouseQueryDTO.class);
        commonPageBuilder.appendEntity(queryParams.getEntity()).appendQuery(queryParams.getQuery()).appendPage(queryParams.getPage());
        IPage<WmsWarehouse> page = wmsWarehouseService.page(commonPageBuilder.bulid(),loginUser);
        List<WmsWarehouse> warehouses = page.getRecords();
        //结果对象
        IPage<WarehouseVO> pageList = new Page<>();
        pageList.setTotal(page.getTotal());
        pageList.setCurrent(page.getCurrent());
        pageList.setSize(page.getSize());
        //查询到结果 数据转换
        if(ObjectUtil.isNotNull(warehouses)){
            pageList.setRecords(warehouses.stream().map(sysWarehouse -> {
                WarehouseVO warehouseVO = new WarehouseVO();
                BeanUtil.copyProperties(sysWarehouse,warehouseVO);
                return  warehouseVO;
            }).collect(Collectors.toList()));
        }else {
            pageList.setRecords(new ArrayList<>());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002,pageList);
    }

    /**
     * 查询产品类型列表信息--启用的产品类型
     * @param queryParams
     * @return
     */
    public Result<List<WarehouseVO>> queryWarehouses(WarehouseQueryParams queryParams){
        Assert.notNull(queryParams, SysMsgCodeConstant.Error.ERR10000001);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        WarehouseQueryDTO queryDTO = new WarehouseQueryDTO();
        BeanUtil.copyProperties(queryParams,queryDTO);
        queryDTO.setStatus(YesOrNoEnum.YES.getValue());
        List<WmsWarehouse> warehouses = wmsWarehouseService.list(queryDTO,loginUser);
        //查询对象
        List<WarehouseVO> list = new ArrayList<>();
        if(ObjectUtil.isNotNull(warehouses)){
            list = warehouses.stream().map(sysWarehouse -> {
                WarehouseVO warehouseVO = new WarehouseVO();
                BeanUtil.copyProperties(sysWarehouse,warehouseVO);
                return  warehouseVO;
            }).collect(Collectors.toList());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002,list);
    }




    /**
     * 创建产品类型
     * @param createParams 创建对象
     * @return Result
     */
    @Transactional
    public Result<String> createWarehouse(WarehouseCreateParams createParams) {
        ValidateUtil.validate(createParams);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        WarehouseBuilder warehouseBuilder = new WarehouseBuilder();
        warehouseBuilder.append(createParams).appendStatus(YesOrNoEnum.YES.getValue()).appendCreate(loginUser);
        //保存产品类型信息
        if(!wmsWarehouseService.save(warehouseBuilder.bulid())){
            log.info("保存产品类型接口异常,参数:{}", JSON.toJSONString(warehouseBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001,warehouseBuilder.getCode());
    }

    /**
     * 编辑产品类型
     * @param editParams 编辑对象
     * @return Result
     */
    @Transactional
    public Result<String> editWarehouse(WarehouseEditParams editParams) {
        ValidateUtil.validate(editParams);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        WarehouseBuilder warehouseBuilder = new WarehouseBuilder();
        warehouseBuilder.append(editParams).appendEdit(loginUser);
        //编辑产品类型信息
        if(!wmsWarehouseService.edit(warehouseBuilder.bulid(),loginUser)){
            log.info("编辑产品类型接口异常,参数:{}", JSON.toJSONString(warehouseBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001,warehouseBuilder.getCode());
    }


    /**
     * 删除产品类型
     * @param warehouseCode 产品类型编码
     * @return Result
     */
    @Transactional
    public Result<String> deleteWarehouse(String warehouseCode) {
        ValidateUtil.hasLength(warehouseCode,"参数[warehouseCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        WmsWarehouse sysWarehouse = wmsWarehouseService.queryWarehouseByWarehouseCode(warehouseCode,loginUser);
        new WarehouseCheck(sysWarehouse).checkNull(SysMsgCodeConstant.Error.ERR10000002);
        WarehouseBuilder warehouseBuilder = new WarehouseBuilder();
        warehouseBuilder.appendCode(warehouseCode).appendEdit(loginUser).delete();
        //删除产品类型信息
        if(!wmsWarehouseService.edit(warehouseBuilder.bulid(),loginUser)){
            log.info("删除产品类型接口异常,参数:{}", JSON.toJSONString(warehouseBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001,warehouseBuilder.getCode());
    }

    /**
     * 开启产品类型
     * @param warehouseCode 产品类型编码
     * @return Result
     */
    @Transactional
    public Result<String> openWarehouse(String warehouseCode) {
        ValidateUtil.hasLength(warehouseCode,"参数[warehouseCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        WmsWarehouse sysWarehouse = wmsWarehouseService.queryWarehouseByWarehouseCode(warehouseCode,loginUser);
        new WarehouseCheck(sysWarehouse).checkNull(SysMsgCodeConstant.Error.ERR10000002);
        WarehouseBuilder warehouseBuilder = new WarehouseBuilder();
        warehouseBuilder.appendCode(warehouseCode).appendEdit(loginUser).open();
        //开启产品类型信息
        if(!wmsWarehouseService.edit(warehouseBuilder.bulid(),loginUser)){
            log.info("开启产品类型接口异常,参数:{}", JSON.toJSONString(warehouseBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001,warehouseBuilder.getCode());
    }

    /**
     * 开启产品类型
     * @param warehouseCode 产品类型编码
     * @return Result
     */
    @Transactional
    public Result<String> closeWarehouse(String warehouseCode) {
        ValidateUtil.hasLength(warehouseCode,"参数[warehouseCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        WmsWarehouse sysWarehouse = wmsWarehouseService.queryWarehouseByWarehouseCode(warehouseCode,loginUser);
        new WarehouseCheck(sysWarehouse).checkNull(SysMsgCodeConstant.Error.ERR10000002);
        WarehouseBuilder warehouseBuilder = new WarehouseBuilder();
        warehouseBuilder.appendCode(warehouseCode).appendEdit(loginUser).close();
        //关闭产品类型信息
        if(!wmsWarehouseService.edit(warehouseBuilder.bulid(),loginUser)){
            log.info("关闭产品类型接口异常,参数:{}", JSON.toJSONString(warehouseBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001,warehouseBuilder.getCode());
    }

    /**
     * 产品类型详情
     * @param warehouseCode 产品类型编码
     * @return Result
     */
    public Result<WarehouseVO> viewWarehouse(String warehouseCode) {
        ValidateUtil.hasLength(warehouseCode,"参数[warehouseCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        WmsWarehouse sysWarehouse = wmsWarehouseService.queryWarehouseByWarehouseCode(warehouseCode,loginUser);
        new WarehouseCheck(sysWarehouse).checkNull(SysMsgCodeConstant.Error.ERR10000002);
        WarehouseVO warehouseVO = new WarehouseVO();
        BeanUtil.copyProperties(sysWarehouse,warehouseVO);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002,warehouseVO);
    }
}
