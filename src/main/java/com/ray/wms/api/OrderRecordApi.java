package com.ray.wms.api;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.service.BaseMaterialModelService;
import com.ray.base.service.BaseMaterialService;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.table.entity.BaseMaterial;
import com.ray.base.table.entity.BaseMaterialModel;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.system.builder.CommonPageBuilder;
import com.ray.wms.builder.GoodsVOBuilder;
import com.ray.wms.service.WmsOrderRecordService;
import com.ray.wms.table.dto.OrderRecordQueryDTO;
import com.ray.wms.table.entity.WmsOrderRecord;
import com.ray.wms.table.params.order.OrderRecordQueryParams;
import com.ray.wms.table.vo.order.GoodsVO;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 出库相关服务
 * @Class: InApi
 * @Package com.ray.wms.api
 * @date 2020/6/3 15:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class OrderRecordApi {

    @Autowired
    private WmsOrderRecordService wmsOrderRecordService;
   @Autowired
    private GoodsService goodsService;


    /**
     * 查询订单商品信息
     * @param queryParams
     * @return
     */
    public Result<IPage<GoodsVO>> queryOrderRecords(CommonPage<OrderRecordQueryParams,Page<GoodsVO>> queryParams){
        LoginUser loginUser = LogInUserUtil.get();
        Map<String,MaterialModelVO> modelMap = new HashMap<>();
        CommonPageBuilder<OrderRecordQueryDTO,WmsOrderRecord> commonPageBuilder = new CommonPageBuilder<>(OrderRecordQueryDTO.class);
        commonPageBuilder.appendEntity(queryParams.getEntity()).appendQuery(queryParams.getQuery()).appendPage(queryParams.getPage());
        IPage<WmsOrderRecord> page = wmsOrderRecordService.page(commonPageBuilder.bulid(),loginUser);
        List<WmsOrderRecord> records = page.getRecords();
        //结果对象
        IPage<GoodsVO> pageList = new Page<>();
        pageList.setTotal(page.getTotal());
        pageList.setCurrent(page.getCurrent());
        pageList.setSize(page.getSize());
        //查询到结果 数据转换
        if(ObjectUtil.isNotNull(records)){
            pageList.setRecords(records.stream().map(wmsOrderRecord -> {
                GoodsVOBuilder goodsBuilder = new GoodsVOBuilder();
                MaterialModelVO materialModel = modelMap.get(wmsOrderRecord.getGoodsCode());
                if(ObjectUtil.isNull(materialModel)){
                    materialModel = goodsService.queryGoodsByCode(wmsOrderRecord.getGoodsCode(),loginUser);
                    modelMap.put(wmsOrderRecord.getGoodsCode(),materialModel);
                }
                goodsBuilder.append(materialModel).append(wmsOrderRecord);
                return  goodsBuilder.bulid();
            }).collect(Collectors.toList()));
        }else {
            pageList.setRecords(new ArrayList<>());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002,pageList);
    }




}
