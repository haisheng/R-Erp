package com.ray.wms.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.wms.table.dto.OrderRecordQueryDTO;
import com.ray.wms.table.entity.WmsOrderRecord;
import com.ray.wms.table.mapper.WmsOrderRecordMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 出入库单明细 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class WmsOrderRecordService extends ServiceImpl<WmsOrderRecordMapper, WmsOrderRecord>  {

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<WmsOrderRecord> page(CommonPage<OrderRecordQueryDTO, Page<WmsOrderRecord>> queryParams, LoginUser loginUser) {
        OrderRecordQueryDTO params = queryParams.getEntity();
        WmsOrderRecord entity = BeanCreate.newBean(WmsOrderRecord.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<WmsOrderRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return page(queryParams.getPage(), queryWrapper);
    }
    /**
     * 查询订单明细数据
     * @param orderNo
     * @param loginUser
     * @return
     */
    public List<WmsOrderRecord> list(String orderNo, LoginUser loginUser) {
        WmsOrderRecord entity = BeanCreate.newBean(WmsOrderRecord.class);
        entity.setOrderNo(orderNo);
        QueryWrapper<WmsOrderRecord> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }
}
