package com.ray.wms.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.wms.table.dto.WarehouseQueryDTO;
import com.ray.wms.table.entity.WmsWarehouse;
import com.ray.wms.table.mapper.WmsWarehouseMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 仓库 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class WmsWarehouseService extends ServiceImpl<WmsWarehouseMapper, WmsWarehouse> {

    /**
     * 编辑仓库类型
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(WmsWarehouse entity, LoginUser loginUser) {
        WmsWarehouse query = new WmsWarehouse();
        query.setWarehouseCode(entity.getWarehouseCode());
        UpdateWrapper<WmsWarehouse> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询仓库类型
     *
     * @param warehouseCode 仓库类型编码
     * @param loginUser    当前操作员
     * @return
     */
    public WmsWarehouse queryWarehouseByWarehouseCode(String warehouseCode, LoginUser loginUser) {
        WmsWarehouse query = new WmsWarehouse();
        query.setWarehouseCode(warehouseCode);
        QueryWrapper<WmsWarehouse> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<WmsWarehouse> page(CommonPage<WarehouseQueryDTO, Page<WmsWarehouse>> queryParams, LoginUser loginUser) {
        WarehouseQueryDTO params = queryParams.getEntity();
        WmsWarehouse entity = BeanCreate.newBean(WmsWarehouse.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<WmsWarehouse> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getWarehouseNameLike()), "warehouse_name", params.getWarehouseNameLike());
        }
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<WmsWarehouse> list(WarehouseQueryDTO queryParams, LoginUser loginUser) {
        WmsWarehouse entity = BeanCreate.newBean(WmsWarehouse.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<WmsWarehouse> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getWarehouseNameLike()), "warehouse_name", queryParams.getWarehouseNameLike());
        return list(queryWrapper);
    }

}
