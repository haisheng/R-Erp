package com.ray.wms.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.common.CountParams;
import com.ray.wms.enums.OrderStatusEnum;
import com.ray.wms.table.dto.OrderQueryDTO;
import com.ray.wms.table.dto.OrderStatusChangeDTO;
import com.ray.wms.table.entity.WmsOrderOut;
import com.ray.wms.table.mapper.WmsOrderOutMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 出库单 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class WmsOrderOutService extends ServiceImpl<WmsOrderOutMapper, WmsOrderOut> {
    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<WmsOrderOut> page(CommonPage<OrderQueryDTO, Page<WmsOrderOut>> queryParams, LoginUser loginUser) {
        OrderQueryDTO params = queryParams.getEntity();
        WmsOrderOut entity = BeanCreate.newBean(WmsOrderOut.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<WmsOrderOut> queryWrapper = new QueryWrapper<>(entity);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getOrderNoLike()), "order_no", params.getOrderNoLike());
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getBusinessCodeLike()), "business_code", params.getBusinessCodeLike());
            queryWrapper.gt(ObjectUtil.isNotNull(params.getStartTime()), "create_time", params.getStartTime());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getEndTime()), "create_time", params.getEndTime());
        }
        queryWrapper.orderByDesc("create_time");
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 查询入库单
     *
     * @param orderNo
     * @param loginUser
     * @return
     */
    public WmsOrderOut queryByOrderNo(String orderNo, LoginUser loginUser) {
        WmsOrderOut query = new WmsOrderOut();
        query.setOrderNo(orderNo);
        QueryWrapper<WmsOrderOut> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 更新状态
     *
     * @param orderStatusChangeDTO
     * @param loginUser
     * @return
     */
    public boolean updateStatus(OrderStatusChangeDTO orderStatusChangeDTO, LoginUser loginUser) {
        WmsOrderOut entity = new WmsOrderOut();
        entity.setOrderStatus(orderStatusChangeDTO.getOrderStatus());
        entity.setOrderTime(LocalDateTime.now());
        entity.setOrderUserCode(loginUser.getUserCode());
        entity.setOrderUserName(loginUser.getUserName());
        entity.setUpdateVersion(orderStatusChangeDTO.getUpdateVersion() + 1);
        WmsOrderOut query = new WmsOrderOut();
        query.setOrderNo(orderStatusChangeDTO.getOrderNo());
        query.setUpdateVersion(orderStatusChangeDTO.getUpdateVersion());
        UpdateWrapper<WmsOrderOut> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 业务订单
     *
     * @param orderNo
     * @param loginUser
     * @return
     */
    public List<WmsOrderOut> list(String orderNo, List<String> status,LoginUser loginUser) {
        WmsOrderOut entity = BeanCreate.newBean(WmsOrderOut.class);
        entity.setBusinessCode(orderNo);
        QueryWrapper<WmsOrderOut> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("order_status",status);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 统计出库数量
     *
     * @param countParams
     * @param loginUser
     * @return
     */
    public Integer countOrderOut(CountParams countParams, LoginUser loginUser) {
        WmsOrderOut entity = BeanCreate.newBean(WmsOrderOut.class);
        BeanUtil.copyProperties(countParams,entity);
        QueryWrapper<WmsOrderOut> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.ge(ObjectUtil.isNotNull(countParams.getStartTime()), "create_time", countParams.getStartTime());
        queryWrapper.le(ObjectUtil.isNotNull(countParams.getEndTime()), "create_time", countParams.getEndTime());
        queryWrapper.in("order_status", Arrays.asList(OrderStatusEnum.UN_DO.getValue(),OrderStatusEnum.DO.getValue()));
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return count(queryWrapper);
    }
}
