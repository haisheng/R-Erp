package com.ray.wms.service.compose;

import com.alibaba.fastjson.JSON;
import com.ray.common.SysMsgCodeConstant;
import com.ray.wms.check.OrderAllotCheck;
import com.ray.wms.enums.OrderStatusEnum;
import com.ray.wms.service.WmsOrderAllotService;
import com.ray.wms.table.dto.OrderDTO;
import com.ray.wms.table.dto.OrderStatusChangeDTO;
import com.ray.wms.table.entity.WmsOrderAllot;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import com.ray.woodencreate.result.MsgCodeConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author bo shen
 * @Description: 入库集成服务
 * @Class: InService
 * @Package com.ray.wms.service.compose
 * @date 2020/6/3 15:08
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class AllotService {

    @Autowired
    private WmsOrderAllotService wmsOrderAllotService;
    @Autowired
    private InService inService;
    @Autowired
    private OutService outService;

    /**
     * 操作
     *
     * @param loginUser
     * @return
     */
    public boolean orderAllot(WmsOrderAllot orderAllot, LoginUser loginUser) {
        OrderDTO inOrder = new OrderDTO(orderAllot.getOrderNo(), orderAllot.getInWarehouseCode(),null);
        inService.orderIn(inOrder, loginUser);
        OrderDTO outOrder = new OrderDTO(orderAllot.getOrderNo(), orderAllot.getOutWarehouseCode(),null);
        outService.orderOut(outOrder, loginUser);
        return true;
    }


    /**
     * 入库单取消操作
     *
     * @param orderAllot
     * @param loginUser
     * @return
     */
    public boolean orderCancel(WmsOrderAllot orderAllot, LoginUser loginUser) {
        OrderDTO inOrder = new OrderDTO(orderAllot.getOrderNo(), orderAllot.getOutWarehouseCode(),null);
        inService.orderIn(inOrder, loginUser);
        OrderDTO outOrder = new OrderDTO(orderAllot.getOrderNo(), orderAllot.getInWarehouseCode(),null);
        outService.orderCancel(outOrder, loginUser);
        return true;
    }


    /**
     * 作废订单
     *
     * @param orderNo
     * @param loginUser
     * @return
     */
    public boolean invalidOrder(String orderNo, LoginUser loginUser) {
        WmsOrderAllot wmsOrderAllot = wmsOrderAllotService.queryByOrderNo(orderNo, loginUser);
        new OrderAllotCheck(wmsOrderAllot).checkNull(SysMsgCodeConstant.Error.ERR10000002).invalidCheck(SysMsgCodeConstant.Error.ERR10000015);
        //更新订单状态
        OrderStatusChangeDTO orderStatusChangeDTO = new OrderStatusChangeDTO(orderNo, OrderStatusEnum.INVALID.getValue(), wmsOrderAllot.getUpdateVersion());
        if (wmsOrderAllotService.updateStatus(orderStatusChangeDTO, loginUser)) {
            log.info("更新订单状态异常,参数：{}", JSON.toJSONString(orderStatusChangeDTO));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return true;
    }
}
