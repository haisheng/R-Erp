package com.ray.wms.service.compose;

import cn.hutool.core.util.ObjectUtil;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.wms.builder.GoodsVOBuilder;
import com.ray.wms.service.WmsWarehouseService;
import com.ray.wms.service.WmsWarehouseStockDetailService;
import com.ray.wms.service.WmsWarehouseStockLogService;
import com.ray.wms.service.WmsWarehouseStockService;
import com.ray.wms.table.dto.GoodsDTO;
import com.ray.wms.table.entity.WmsWarehouse;
import com.ray.wms.table.entity.WmsWarehouseStockDetail;
import com.ray.wms.table.vo.order.GoodsVO;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description:
 * @Class: StockService
 * @Package com.ray.wms.service.compose
 * @date 2020/6/30 10:03
 * @company <p>快速开发</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class StockService {

    @Autowired
    private WmsWarehouseStockDetailService wmsWarehouseStockDetailService;
    @Autowired
    private WmsWarehouseService wmsWarehouseService;
    @Autowired
    private GoodsService goodsService;

    /**
     * 查询库存明细详情
     *
     * @param
     * @return
     */
    public List<GoodsVO> queryStockDetails(GoodsDTO goodsDTO, BigDecimal price) {
        LoginUser loginUser = LogInUserUtil.get();
        Map<String, MaterialModelVO> modelMap = new HashMap<>();
        Map<String, WmsWarehouse> wmsWarehouseMap = new HashMap<>();
        List<WmsWarehouseStockDetail> records = wmsWarehouseStockDetailService.list(goodsDTO, loginUser);
        List<GoodsVO> goodsVOS = records.stream().map(warehouseStockDetail -> {
            MaterialModelVO materialModel = modelMap.get(warehouseStockDetail.getGoodsCode());
            GoodsVOBuilder goodsBuilder = new GoodsVOBuilder();
            //库存详情对象转换
            if (ObjectUtil.isNull(materialModel)) {
                materialModel = goodsService.queryGoodsByCode(warehouseStockDetail.getGoodsCode(), loginUser);
                modelMap.put(warehouseStockDetail.getGoodsCode(), materialModel);
            }
            //查询仓库信息
            WmsWarehouse wmsWarehouse = wmsWarehouseMap.get(warehouseStockDetail.getWarehouseCode());
            if (ObjectUtil.isEmpty(wmsWarehouse)) {
                wmsWarehouse = wmsWarehouseService.queryWarehouseByWarehouseCode(warehouseStockDetail.getWarehouseCode(), loginUser);
                wmsWarehouseMap.put(wmsWarehouse.getWarehouseCode(), wmsWarehouse);
            }
            goodsBuilder.append(materialModel).append(wmsWarehouse).append(warehouseStockDetail)
                    .appendTime(warehouseStockDetail.getCreateTime()).appendUser(warehouseStockDetail.getCreateUserName()).appendPrice(price);
            return goodsBuilder.bulid();
        }).collect(Collectors.toList());
        return goodsVOS;
    }


}
