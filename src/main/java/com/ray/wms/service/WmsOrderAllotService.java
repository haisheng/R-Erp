package com.ray.wms.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.wms.table.dto.OrderAllotQueryDTO;
import com.ray.wms.table.dto.OrderStatusChangeDTO;
import com.ray.wms.table.entity.WmsOrderAllot;
import com.ray.wms.table.mapper.WmsOrderAllotMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * <p>
 * 调拨单 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class WmsOrderAllotService extends ServiceImpl<WmsOrderAllotMapper, WmsOrderAllot> {

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<WmsOrderAllot> page(CommonPage<OrderAllotQueryDTO, Page<WmsOrderAllot>> queryParams, LoginUser loginUser) {
        OrderAllotQueryDTO params = queryParams.getEntity();
        WmsOrderAllot entity = BeanCreate.newBean(WmsOrderAllot.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<WmsOrderAllot> queryWrapper = new QueryWrapper<>(entity);
        if(ObjectUtil.isNotNull(params)){
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getOrderNoLike()),"order_no",params.getOrderNoLike());
            queryWrapper.gt(ObjectUtil.isNotNull(params.getStartTime()),"create_time",params.getStartTime());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getEndTime()),"create_time",params.getEndTime());
        }
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 查询入库单
     * @param orderNo
     * @param loginUser
     * @return
     */
    public WmsOrderAllot queryByOrderNo(String orderNo, LoginUser loginUser) {
        WmsOrderAllot query = new WmsOrderAllot();
        query.setOrderNo(orderNo);
        QueryWrapper<WmsOrderAllot> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 更新状态
     * @param orderStatusChangeDTO
     * @param loginUser
     * @return
     */
    public boolean updateStatus(OrderStatusChangeDTO orderStatusChangeDTO, LoginUser loginUser) {
        WmsOrderAllot entity = new WmsOrderAllot();
        entity.setOrderStatus(orderStatusChangeDTO.getOrderStatus());
        entity.setOrderTime(LocalDateTime.now());
        entity.setOrderUserCode(loginUser.getUserCode());
        entity.setOrderUserName(loginUser.getUserName());
        entity.setUpdateVersion(orderStatusChangeDTO.getUpdateVersion() +1);
        WmsOrderAllot query = new WmsOrderAllot();
        query.setOrderNo(orderStatusChangeDTO.getOrderNo());
        query.setUpdateVersion(orderStatusChangeDTO.getUpdateVersion());
        UpdateWrapper<WmsOrderAllot> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity,updateWrapper);
    }
}
