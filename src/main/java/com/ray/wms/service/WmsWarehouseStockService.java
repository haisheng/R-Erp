package com.ray.wms.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.common.SysMsgCodeConstant;
import com.ray.wms.builder.WarehouseStockBuilder;
import com.ray.wms.table.dto.GoodsDTO;
import com.ray.wms.table.dto.StockQueryDTO;
import com.ray.wms.table.entity.WmsOrderAllot;
import com.ray.wms.table.entity.WmsWarehouseStock;
import com.ray.wms.table.mapper.WmsWarehouseStockMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 * 仓库库存 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
@Slf4j
public class WmsWarehouseStockService extends ServiceImpl<WmsWarehouseStockMapper, WmsWarehouseStock> {


    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<WmsWarehouseStock> page(CommonPage<StockQueryDTO, Page<WmsWarehouseStock>> queryParams, LoginUser loginUser) {
        StockQueryDTO params = queryParams.getEntity();
        WmsWarehouseStock entity = BeanCreate.newBean(WmsWarehouseStock.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<WmsWarehouseStock> queryWrapper = new QueryWrapper<>(entity);
        //queryWrapper.gt("quantity",new BigDecimal(0));
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 添加库存信息
     *
     * @param goodsDTO
     * @param loginUser
     * @return
     */
    public boolean addStock(GoodsDTO goodsDTO, LoginUser loginUser) {
        //查询记录
        WmsWarehouseStock wmsWarehouseStock = query(goodsDTO, loginUser);
        if (ObjectUtil.isNull(wmsWarehouseStock)) {
            if(goodsDTO.getQuantity().compareTo(new BigDecimal(0)) <0){
                log.info("库存不够,{},{}", JSON.toJSONString(goodsDTO), JSON.toJSONString(wmsWarehouseStock));
                throw BusinessExceptionFactory.newException(SysMsgCodeConstant.Error.ERR10000017);
            }
            //没有库存插入库存数据
            return save(new WarehouseStockBuilder().append(goodsDTO).appendCreate(loginUser).bulid());
        } else {
            if (wmsWarehouseStock.getQuantity().add(goodsDTO.getQuantity()).compareTo(new BigDecimal(0)) <0) {
                log.info("库存不够,{},{}", JSON.toJSONString(goodsDTO), JSON.toJSONString(wmsWarehouseStock));
                throw BusinessExceptionFactory.newException(SysMsgCodeConstant.Error.ERR10000017);
            }
            WmsWarehouseStock entity = new WmsWarehouseStock();
            entity.setQuantity(wmsWarehouseStock.getQuantity().add(goodsDTO.getQuantity()));
            entity.setUpdateVersion(wmsWarehouseStock.getUpdateVersion() + 1);
            WmsWarehouseStock query = new WmsWarehouseStock();
            query.setWarehouseCode(goodsDTO.getWarehouseCode());
            query.setGoodsCode(goodsDTO.getGoodsCode());
            query.setUpdateVersion(wmsWarehouseStock.getUpdateVersion());
            query.setUnit(goodsDTO.getUnit());
            UpdateWrapper<WmsWarehouseStock> updateWrapper = new UpdateWrapper<>(query);
            DataAuthUtil.addDataAuth(updateWrapper, loginUser);
            return update(entity, updateWrapper);
        }
    }

    /**
     * 查询库存记录
     *
     * @param goodsDTO
     * @param loginUser
     * @return
     */
    public WmsWarehouseStock query(GoodsDTO goodsDTO, LoginUser loginUser) {
        WmsWarehouseStock query = new WmsWarehouseStock();
        query.setGoodsCode(goodsDTO.getGoodsCode());
        query.setWarehouseCode(goodsDTO.getWarehouseCode());
        query.setUnit(goodsDTO.getUnit());
        QueryWrapper<WmsWarehouseStock> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }
}
