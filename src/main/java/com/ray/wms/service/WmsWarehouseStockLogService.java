package com.ray.wms.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.wms.builder.WarehouseStockDetailBuilder;
import com.ray.wms.builder.WarehouseStockLogBuilder;
import com.ray.wms.table.dto.GoodsDTO;
import com.ray.wms.table.dto.StockQueryDTO;
import com.ray.wms.table.entity.WmsWarehouseStockDetail;
import com.ray.wms.table.entity.WmsWarehouseStockLog;
import com.ray.wms.table.mapper.WmsWarehouseStockLogMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 仓库库存明细 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
@Service
public class WmsWarehouseStockLogService extends ServiceImpl<WmsWarehouseStockLogMapper, WmsWarehouseStockLog> {


    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<WmsWarehouseStockLog> page(CommonPage<StockQueryDTO, Page<WmsWarehouseStockLog>> queryParams, LoginUser loginUser) {
        StockQueryDTO params = queryParams.getEntity();
        WmsWarehouseStockLog entity = BeanCreate.newBean(WmsWarehouseStockLog.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<WmsWarehouseStockLog> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.orderByDesc("create_time");
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return page(queryParams.getPage(), queryWrapper);
    }

    public boolean addLog(GoodsDTO goodsDTO, LoginUser loginUser) {
        return save(new WarehouseStockLogBuilder().append(goodsDTO).appendCreate(loginUser).bulid());
    }
}
