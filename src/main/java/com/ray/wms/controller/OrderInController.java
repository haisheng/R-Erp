package com.ray.wms.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.wms.api.OrderInApi;
import com.ray.wms.api.WarehouseApi;
import com.ray.wms.table.params.order.OrderQueryParams;
import com.ray.wms.table.params.warehouse.WarehouseCreateParams;
import com.ray.wms.table.params.warehouse.WarehouseEditParams;
import com.ray.wms.table.params.warehouse.WarehouseQueryParams;
import com.ray.wms.table.vo.order.OrderVO;
import com.ray.wms.table.vo.warehouse.WarehouseVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-仓库管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/wms/order/in")
@Api("仓库服务-入库")
@Validated
public class OrderInController {

    @Autowired
    private OrderInApi orderInApi;

    @ApiOperation("查询入库列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<OrderVO>> page(@RequestBody @Valid CommonPage<OrderQueryParams, Page<OrderVO>> queryParams) {
        return orderInApi.queryOrderIns(queryParams);
    }


    @ApiOperation("入库")
    @PostMapping(value = "/in", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> in(@ApiParam(value = "入库单号", required = true) @RequestParam(required = true) String orderNo) {
        return orderInApi.doOrder(orderNo);
    }


    @ApiOperation("入库取消")
    @PostMapping(value = "/cancel", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> cancel(@ApiParam(value = "入库单号", required = true) @RequestParam(required = true) String orderNo) {
        return orderInApi.doCancelOrder(orderNo);
    }


}
