package com.ray.wms.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.wms.api.OrderOutApi;
import com.ray.wms.table.params.order.OrderQueryParams;
import com.ray.wms.table.vo.order.OrderVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 系统基础服务-出库管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/wms/order/out")
@Api("仓库服务-出库")
@Validated
public class OrderOutController {

    @Autowired
    private OrderOutApi orderOutApi;

    @ApiOperation("查询出库列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<OrderVO>> page(@RequestBody @Valid CommonPage<OrderQueryParams, Page<OrderVO>> queryParams) {
        return orderOutApi.queryOrderOuts(queryParams);
    }


    @ApiOperation("出库")
    @PostMapping(value = "/out", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> out(@ApiParam(value = "入库单号", required = true) @RequestParam(required = true) String orderNo) {
        return orderOutApi.doOrder(orderNo);
    }


    @ApiOperation("出库取消")
    @PostMapping(value = "/cancel", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> cancel(@ApiParam(value = "入库单号", required = true) @RequestParam(required = true) String orderNo) {
        return orderOutApi.doCancelOrder(orderNo);
    }


}
