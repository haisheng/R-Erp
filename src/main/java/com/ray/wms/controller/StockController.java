package com.ray.wms.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.wms.api.StockApi;
import com.ray.wms.table.params.DetailChangeParams;
import com.ray.wms.table.params.WareHouseChangeParams;
import com.ray.wms.table.params.order.StockQueryParams;
import com.ray.wms.table.vo.order.GoodsVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>
 * 系统基础服务-库存管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/wms/order/stock")
@Api("仓库服务-库存")
@Validated
public class StockController {

    @Autowired
    private StockApi stockApi;


    @ApiOperation("查询库存列表查询")
    @PostMapping(value = "/stock", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<GoodsVO>> stock(@RequestBody @Valid CommonPage<StockQueryParams, Page<GoodsVO>> queryParams) {
        return stockApi.queryStocks(queryParams);
    }


    @ApiOperation("查询库存详情列表查询")
    @PostMapping(value = "/detail", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<GoodsVO>> detail(@RequestBody @Valid CommonPage<StockQueryParams, Page<GoodsVO>> queryParams) {
        return stockApi.queryStockDetails(queryParams);
    }

    @ApiOperation("查询库存日志列表查询")
    @PostMapping(value = "/log", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<GoodsVO>> log(@RequestBody @Valid CommonPage<StockQueryParams, Page<GoodsVO>> queryParams) {
        return stockApi.queryStockLogs(queryParams);
    }

    @ApiOperation("变更业务订单")
    @PostMapping(value = "/detailChange", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> detailChange(@RequestBody @Valid DetailChangeParams changeParams) {
        return stockApi.detailChange(changeParams);
    }

    @ApiOperation("变更仓库")
    @PostMapping(value = "/warehouseChange", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> warehouseChange(@RequestBody @Valid WareHouseChangeParams changeParams) {
        return stockApi.wareHouseChange(changeParams);
    }


}
