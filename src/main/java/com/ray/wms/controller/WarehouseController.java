package com.ray.wms.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.wms.api.WarehouseApi;
import com.ray.wms.table.params.warehouse.WarehouseCreateParams;
import com.ray.wms.table.params.warehouse.WarehouseEditParams;
import com.ray.wms.table.params.warehouse.WarehouseQueryParams;
import com.ray.wms.table.vo.warehouse.WarehouseVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-仓库管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/wms/warehouse")
@Api("仓库服务-仓库")
@Validated
public class WarehouseController {

    @Autowired
    private WarehouseApi warehouseApi;

    @ApiOperation("查询仓库列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<WarehouseVO>> page(@RequestBody @Valid CommonPage<WarehouseQueryParams, Page<WarehouseVO>> queryParams) {
        return warehouseApi.pageWarehouses(queryParams);
    }

    @ApiOperation("查询仓库列表")
    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<WarehouseVO>> list(@RequestBody @Valid WarehouseQueryParams queryParams) {
        return warehouseApi.queryWarehouses(queryParams);
    }


    @ApiOperation("仓库新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid WarehouseCreateParams createParams) {
        return warehouseApi.createWarehouse(createParams);
    }

    @ApiOperation("仓库编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid WarehouseEditParams editParams) {
        return warehouseApi.editWarehouse(editParams);
    }

    @ApiOperation("仓库删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "仓库编码", required = true) @RequestParam(required = true) String warehouseCode) {
        return warehouseApi.deleteWarehouse(warehouseCode);
    }

    @ApiOperation("仓库详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<WarehouseVO> view(@ApiParam(value = "仓库编码", required = true) @RequestParam(required = true) String warehouseCode) {
        return warehouseApi.viewWarehouse(warehouseCode);
    }

    @ApiOperation("仓库开启")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "仓库编码", required = true) @RequestParam(required = true) String warehouseCode) {
        return warehouseApi.openWarehouse(warehouseCode);
    }


    @ApiOperation("仓库关闭")
    @PostMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "仓库编码", required = true) @RequestParam(required = true) String warehouseCode) {
        return warehouseApi.closeWarehouse(warehouseCode);
    }


}
