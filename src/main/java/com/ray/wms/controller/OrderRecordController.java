package com.ray.wms.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.wms.api.OrderRecordApi;
import com.ray.wms.table.params.order.OrderRecordQueryParams;
import com.ray.wms.table.vo.order.GoodsVO;
import com.ray.wms.table.vo.order.OrderVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>
 * 系统基础服务-仓库管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/wms/order/record")
@Api("仓库服务-物料详情")
@Validated
public class OrderRecordController {

    @Autowired
    private OrderRecordApi orderRecordApi;

    @ApiOperation("查询出入库详情列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<GoodsVO>> page(@RequestBody @Valid CommonPage<OrderRecordQueryParams, Page<GoodsVO>> queryParams) {
        return orderRecordApi.queryOrderRecords(queryParams);
    }


}
