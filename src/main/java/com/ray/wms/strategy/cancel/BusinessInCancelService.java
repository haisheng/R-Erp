package com.ray.wms.strategy.cancel;

import cn.hutool.core.util.ObjectUtil;
import com.ray.business.check.BusinessCheck;
import com.ray.business.service.ProdBusinessService;
import com.ray.business.service.ProdOrderStepService;
import com.ray.business.service.compose.BusinessService;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdOrderStep;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.util.CodeSplitUtil;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 采购入库回写
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "cancel", strategy = "JG", desc = "加工入库取消")
public class BusinessInCancelService implements Strategy<String, LoginUser, Boolean> {

    @Autowired
    private ProdBusinessService prodBusinessService;
    @Autowired
    private BusinessService businessService;
    @Autowired
    private ProdOrderStepService prodOrderStepService;

    @Override
    public Boolean execute(String businessCode, LoginUser loginUser) {
        //校验
        ProdBusiness business = prodBusinessService.queryBusinessByBusinessCode(businessCode, loginUser);
        new BusinessCheck(business).checkNull("加工单号不存在");
        //判断工序是否最后一步
        ProdOrderStep prodOrderStep = prodOrderStepService.queryStepByStepCode(CodeSplitUtil.getFirst(business.getGoodsCode()), business.getStepCode(), loginUser);
        if (ObjectUtil.isNotNull(prodOrderStep)) {
            //查询工序的下一步
            ProdOrderStep last = prodOrderStepService.queryStepByIndexSort(CodeSplitUtil.getFirst(business.getGoodsCode()), prodOrderStep.getIndexSort() + 1, loginUser);
            if (ObjectUtil.isNull(last)) {
                //取消
                businessService.wmsCancel(business, loginUser);
            }
        }
        return true;
    }
}
