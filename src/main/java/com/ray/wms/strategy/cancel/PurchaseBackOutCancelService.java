package com.ray.wms.strategy.cancel;

import com.ray.business.check.PurchaseBackCheck;
import com.ray.business.service.ProdPurchaseBackService;
import com.ray.business.table.entity.ProdPurchaseBack;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 采购入库回写
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "cancel", strategy = "CT", desc = "采购退货出库取消")
public class PurchaseBackOutCancelService implements Strategy<String, LoginUser, Boolean> {


    @Override
    public Boolean execute(String businessCode, LoginUser loginUser) {
        return true;
    }
}
