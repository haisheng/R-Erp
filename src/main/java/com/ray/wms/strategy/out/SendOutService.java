package com.ray.wms.strategy.out;

import com.ray.business.api.OrderSendApi;
import com.ray.business.check.OrderSendCheck;
import com.ray.business.service.ProdOrderSendService;
import com.ray.business.table.entity.ProdOrderSend;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 采购入库回写
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "out", strategy = "FH", desc = "发货出库")
public class SendOutService implements Strategy<String, LoginUser, Boolean> {

    @Autowired
    private ProdOrderSendService prodOrderSendService;
    @Autowired
    private OrderSendApi orderSendApi;

    @Override
    public Boolean execute(String businessCode, LoginUser loginUser) {
        //校验
        ProdOrderSend orderSend = prodOrderSendService.queryOrderSendByOrderSendCode(businessCode, loginUser);
        new OrderSendCheck(orderSend).checkNull("发货单不存在");
        return orderSendApi.finishOrder(orderSend, loginUser);
    }
}
