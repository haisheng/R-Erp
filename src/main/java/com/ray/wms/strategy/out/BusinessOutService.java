package com.ray.wms.strategy.out;

import com.ray.business.check.BusinessCheck;
import com.ray.business.check.SaleCheck;
import com.ray.business.service.ProdBusinessService;
import com.ray.business.service.ProdSaleService;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdSale;
import com.ray.common.SysMsgCodeConstant;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 采购入库回写
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "out", strategy = "JG", desc = "加工出库")
public class BusinessOutService implements Strategy<String, LoginUser, Boolean> {

    @Autowired
    private ProdBusinessService prodBusinessService;

    @Override
    public Boolean execute(String businessCode, LoginUser loginUser) {
        //校验
        ProdBusiness business = prodBusinessService.queryBusinessByBusinessCode(businessCode, loginUser);
        new BusinessCheck(business).checkNull("加工单号存在存在");
        return prodBusinessService.finishOrder(businessCode, loginUser);
    }
}
