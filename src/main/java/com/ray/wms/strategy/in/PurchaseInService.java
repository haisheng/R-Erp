package com.ray.wms.strategy.in;

import com.ray.business.api.PurchaseInApi;
import com.ray.business.check.PurchaseCheck;
import com.ray.business.check.PurchaseInCheck;
import com.ray.business.service.ProdPurchaseInService;
import com.ray.business.service.ProdPurchaseService;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.business.table.entity.ProdPurchaseIn;
import com.ray.common.SysMsgCodeConstant;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 采购入库回写
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "in", strategy = "PI", desc = "采购入库")
public class PurchaseInService implements Strategy<String, LoginUser, Boolean> {

    @Autowired
    private ProdPurchaseInService prodPurchaseInService;

    @Override
    public Boolean execute(String businessCode, LoginUser loginUser) {
        //校验
        ProdPurchaseIn prodPurchaseIn = prodPurchaseInService.queryPurchaseInByInCode(businessCode, loginUser);
        new PurchaseInCheck(prodPurchaseIn).checkNull("采购入库订单不存在");
        return prodPurchaseInService.finishOrder(businessCode, loginUser);
    }
}
