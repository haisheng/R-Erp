package com.ray.wms.strategy.in;

import com.ray.business.check.SaleBackCheck;
import com.ray.business.service.ProdSaleBackService;
import com.ray.business.table.entity.ProdSaleBack;
import com.ray.common.SysMsgCodeConstant;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 采购入库回写
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "in", strategy = "XT", desc = "销售退回入库")
public class SaleBackInService implements Strategy<String, LoginUser, Boolean> {

    @Autowired
    private ProdSaleBackService prodSaleBackService;

    @Override
    public Boolean execute(String businessCode, LoginUser loginUser) {
        //校验
        ProdSaleBack prodSaleBack = prodSaleBackService.querySaleBackBySaleBackCode(businessCode, loginUser);
        new SaleBackCheck(prodSaleBack).checkNull("销售退回单不存在");
        return prodSaleBackService.finishOrder(businessCode, loginUser);
    }
}
