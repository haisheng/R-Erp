package com.ray.wms.strategy.in;

import com.ray.business.check.BusinessBackCheck;
import com.ray.business.check.BusinessCheck;
import com.ray.business.service.ProdBusinessBackService;
import com.ray.business.table.entity.ProdBusinessBack;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 采购入库回写
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "in", strategy = "TL", desc = "退料入库")
public class BusinessBackInService implements Strategy<String, LoginUser, Boolean> {

    @Autowired
    private ProdBusinessBackService prodBusinessBackService;


    @Override
    public Boolean execute(String businessCode, LoginUser loginUser) {
        //校验
        ProdBusinessBack business = prodBusinessBackService.queryBusinessBackByBackCode(businessCode, loginUser);
        new BusinessBackCheck(business).checkNull("退料单号不存在");
        return prodBusinessBackService.finishOrder(businessCode, loginUser);
    }
}
