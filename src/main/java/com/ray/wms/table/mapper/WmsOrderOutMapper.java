package com.ray.wms.table.mapper;

import com.ray.wms.table.entity.WmsOrderOut;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 出库单 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface WmsOrderOutMapper extends BaseMapper<WmsOrderOut> {

}
