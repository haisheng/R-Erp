package com.ray.wms.table.mapper;

import com.ray.wms.table.entity.WmsWarehouseStockDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 仓库库存明细 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface WmsWarehouseStockDetailMapper extends BaseMapper<WmsWarehouseStockDetail> {

}
