package com.ray.wms.table.mapper;

import com.ray.wms.table.entity.WmsWarehouseStock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 仓库库存 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface WmsWarehouseStockMapper extends BaseMapper<WmsWarehouseStock> {

}
