package com.ray.wms.table.mapper;

import com.ray.wms.table.entity.WmsOrderAllot;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 调拨单 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface WmsOrderAllotMapper extends BaseMapper<WmsOrderAllot> {

}
