package com.ray.wms.table.mapper;

import com.ray.wms.table.entity.WmsOrderRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 出入库单明细 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface WmsOrderRecordMapper extends BaseMapper<WmsOrderRecord> {

}
