package com.ray.wms.table.mapper;

import com.ray.wms.table.entity.WmsOrderIn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 入库单 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface WmsOrderInMapper extends BaseMapper<WmsOrderIn> {

}
