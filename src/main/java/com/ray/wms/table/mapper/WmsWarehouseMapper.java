package com.ray.wms.table.mapper;

import com.ray.wms.table.entity.WmsWarehouse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 仓库 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-02
 */
public interface WmsWarehouseMapper extends BaseMapper<WmsWarehouse> {

}
