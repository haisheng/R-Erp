package com.ray.wms.table.params;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 业务订单变更
 * @Class: DetailChangeParams
 * @Package com.ray.wms.table.params
 * @date 2020/6/15 21:06
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class WareHouseChangeParams {

    @NotBlank(message = "记录单号不能为空")
    private String code;
    /*** 仓库*/
    @NotBlank(message = "仓库")
    private String warehouseCode;

    @NotNull(message = "数量不能为空")
    @DecimalMin(value = "0.0001",message = "数量不能小于0")
    private BigDecimal quantity;
}
