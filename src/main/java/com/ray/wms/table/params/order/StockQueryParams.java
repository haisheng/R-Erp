package com.ray.wms.table.params.order;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author bo shen
 * @Description: 库存
 * @Class: StockQueryParams
 * @Package com.ray.wms.table.params.order
 * @date 2020/6/4 10:22
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class StockQueryParams extends BaseParams {

    @ApiModelProperty("仓库编码")
    private String warehouseCode;
    @ApiModelProperty("商品编号")
    private String goodsCode;
    /**
     * 业务仓库
     */
    private String businessOrderNo = "0";

    /**所有**/
    private Boolean allData= false;

    /**
     * 在列表中的
     */
    private List<String> modelCodes;


}
