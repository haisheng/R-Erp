package com.ray.wms.table.params.warehouse;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 仓库
 * @Class: WarehouseBase
 * @Package com.ray.wms.table.params.warehouse
 * @date 2020/6/2 16:24
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class WarehouseBase {

    /**
     * 仓库名称
     */
    @ApiModelProperty(value = "仓库名称", required = true)
    @NotBlank(message = "仓库名称不能为空")
    @Length(max = 50, message = "仓库名称名称长度不能超过50")
    private String warehouseName;

}
