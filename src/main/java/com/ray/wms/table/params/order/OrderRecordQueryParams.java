package com.ray.wms.table.params.order;

import com.ray.common.BaseParams;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 记录查询
 * @Class: OrderRecordQueryParams
 * @Package com.ray.wms.table.params.order
 * @date 2020/6/4 9:56
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderRecordQueryParams extends BaseParams {

    @NotBlank(message = "订单编号不能为空")
    private  String orderNo;
}
