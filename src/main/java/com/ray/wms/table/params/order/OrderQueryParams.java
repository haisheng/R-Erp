package com.ray.wms.table.params.order;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author bo shen
 * @Description: 订单查询
 * @Class: OrderQueryDTO
 * @Package com.ray.wms.table.params.order
 * @date 2020/6/3 15:18
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderQueryParams extends BaseParams{

    @ApiModelProperty("出入库订单号-模糊")
    private String  orderNoLike;

    @ApiModelProperty("出入库订单号")
    private String  orderNo;

    @ApiModelProperty("仓库编号")
    private String  warehouseCode;

    @ApiModelProperty("业务编号")
    private String  businessCode;

    @ApiModelProperty("业务编号-模糊")
    private String  businessCodeLike;

    @ApiModelProperty("订单状态")
    private String  orderStatus;
    /**
     * 订单号
     */
    private String businessOrderNo;

}
