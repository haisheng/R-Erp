package com.ray.wms.table.params.warehouse;

import com.ray.base.table.params.material.MaterialBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 职位新增
 * @Class: MaterialTypeCreateParams
 * @Package com.ray.system.table.params.position
 * @date 2020/5/27 11:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("仓库新增对象")
public class WarehouseEditParams extends WarehouseBase {

    /**
     * 仓库编码
     */
    @ApiModelProperty(value = "仓库编码", required = true)
    @NotBlank(message = "仓库编码不能为空")
    @Length(max = 50, message = "仓库编码长度不能超过50")
    private String warehouseCode;

}
