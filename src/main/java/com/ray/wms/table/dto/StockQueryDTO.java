package com.ray.wms.table.dto;

import com.ray.common.dto.BaseDTO;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author bo shen
 * @Description: 库存
 * @Class: StockQueryParams
 * @Package com.ray.wms.table.params.order
 * @date 2020/6/4 10:22
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class StockQueryDTO extends BaseDTO{

    private String warehouseCode;

    private String goodsCode;
    /**
     * 业务仓库
     */
    private String businessOrderNo = "0";

    /**所有**/
    private Boolean allData;

    /**
     * 在列表中的
     */
    private List<String> modelCodes;
}
