package com.ray.wms.table.dto;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author bo shen
 * @Description:
 * @Class: OrderCreateDTO
 * @Package com.ray.wms.table.dto
 * @date 2020/6/3 18:29
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderCreateDTO {

    @NotBlank(message = "仓库单号不能为空")
    private String warehouseCode;

    @NotBlank(message = "业务单号不能为空")
    private String businessCode;

    @NotBlank(message = "业务订单单号不能为空")
    private String businessOrderNo;

    @NotNull(message = "商品列表不能为空")
    @Valid
    private List<GoodsDTO> goodsDTOS;
}
