package com.ray.wms.table.dto;

import com.ray.common.dto.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 订单查询
 * @Class: OrderQueryDTO
 * @Package com.ray.wms.table.params.order
 * @date 2020/6/3 15:18
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderQueryDTO extends BaseDTO {
    /**
     * 订单模糊查询
     */
    private String orderNoLike;
    /**
     * 订单
     */
    private String orderNo;

    /**
     * c仓库
     */
    private String warehouseCode;

    /**
     * 业务单号
     */
    private String businessCode;

    /**
     * 业务单号模糊
     */
    private String businessCodeLike;

    /**
     * 订单状态
     */
    private String orderStatus;
    /**
     * 订单号
     */
    private String businessOrderNo;

}
