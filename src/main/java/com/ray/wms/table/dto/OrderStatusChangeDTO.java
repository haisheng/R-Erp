package com.ray.wms.table.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bo shen
 * @Description: 订单状态变化对象
 * @Class: OrderStatusChangeDTO
 * @Package com.ray.wms.table.dto
 * @date 2020/6/3 16:21
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderStatusChangeDTO {


    private String orderNo;

    private String orderStatus;

    private Integer updateVersion;
}
