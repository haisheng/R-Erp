package com.ray.wms.table.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bo shen
 * @Description: 订单
 * @Class: OrderDTO
 * @Package com.ray.wms.table.dto
 * @date 2020/6/4 9:03
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO {

    private String orderNo;

    private String warehouseCode;

    private String businessOrderNo;
}
