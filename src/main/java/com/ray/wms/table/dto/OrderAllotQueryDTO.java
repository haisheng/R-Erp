package com.ray.wms.table.dto;

import com.ray.common.dto.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 订单查询
 * @Class: OrderQueryDTO
 * @Package com.ray.wms.table.params.order
 * @date 2020/6/3 15:18
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderAllotQueryDTO extends BaseDTO{

    private String  orderNo;

    private String  orderNoLike;

    private String  orderStatus;

}
