package com.ray.wms.table.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 商品信息
 * @Class: GoodsDTO
 * @Package com.ray.wms.table.dto
 * @date 2020/6/3 16:54
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class GoodsDTO {

    /**
     * 仓库编码
     */
    @NotBlank(message = "仓库单号不能为空")
    private String warehouseCode;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 商品编码
     */
    private String goodsCode;

    /**
     * 数量
     */
    private BigDecimal quantity;

    /**
     * 卷数
     */
    private Integer total;

    /**
     * 批次号
     */
    private String batchNo;

    /**
     * 单位
     */
    private String unit;

    /**
     * 序列号
     */
    private String serialNumber;

    /**
     * 出入库类型
     */
    private String  inOutType;

    /**
     * 版本
     */
    private Integer version;
}
