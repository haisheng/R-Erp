package com.ray.wms.table.dto;

import com.ray.common.dto.BaseDTO;
import lombok.Data;

/**
 * @author bo shen
 * @Description: warehouse查询
 * @Class: MaterialTypeQueryParams
 * @Package com.ray.system.table.params.customer
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class WarehouseQueryDTO extends BaseDTO{

    private String warehouseNameLike;

    private String warehouseName;

}
