package com.ray.wms.table.vo.order;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ray.common.BaseVO;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author bo shen
 * @Description: 出入库订单详情对象
 * @Class: OrderVO
 * @Package com.ray.wms.table.vo.order
 * @date 2020/6/3 15:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrderAllotVO extends BaseVO{

    /**
     * 入库单
     */
    private String orderNo;

    /**
     * 仓库编号
     */
    private String inWarehouseCode;


    /**
     * 仓库名称
     */
    private String inWarehouseName;


    /**
     * 仓库编号
     */
    private String outWarehouseCode;


    /**
     * 仓库名称
     */
    private String outWarehouseName;

    /**
     * 业务单号
     */
    private String businessCode;

    /**
     * 订单操作时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime orderTime;

    /**
     * 订单操作人
     */
    private String orderUserCode;

    /**
     * 订单操作人
     */
    private String orderUserName;

    /**
     * 单据状态
     */
    private String orderStatus;


    /**
     * 货物明细
     */
    private List<GoodsVO> goodsVOS;
}
