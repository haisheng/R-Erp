package com.ray.wms.table.vo.order;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 商品信息
 * @Class: GoodsVO
 * @Package com.ray.wms.table.vo.order
 * @date 2020/6/4 9:40
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class GoodsVO extends BaseVO{


    /**
     * 记录单号
     */
    private String stockCode;

    /**
     * 订单号
     */
    private String orderNo;


    /**
     * 业务订单号
     */
    private String businessOrderNo;

    /**
     * 商品编码
     */
    private String goodsCode;

    /**
     * 数量
     */
    private BigDecimal quantity;

    /**
     * 卷数
     */
    private Integer total;

    /**
     * 批次号
     */
    private String batchNo;

    /**
     * 序列号
     */
    private String serialNumber;

    /**
     * 物料名称
     */
    private String materialCode;


    /**
     * 型号/规格名称
     */
    private String goodsName;
    private String modelName;


    /**
     * 扩展名称
     */
    private String name;

    /**
     * 条形码
     */
    private String barCode;

    /**
     * 仓库编码
     */
    private String warehouseCode;
    /**
     * 仓库名称
     */
    private String warehouseName;

    /**
     * 物料名称
     */
    private String materialName;

    /**
     * 物料名称-其他名称
     */
    private String materialNameEn;

    /**
     * 物料类型
     */
    private String materialType;

    /**
     * 物料类型
     */
    private String materialTypeName;

    /**
     * 单位
     */
    private String unit;

    /**
     * 规格属性
     */
    @ApiModelProperty(value = "规格属性", required = false)
    private String modelProp;


    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 重量
     */
    @ApiModelProperty(value = "重量", required = true)
    private String weight;

    /**
     * 体积
     */
    @ApiModelProperty(value = "体积", required = true)
    private String volume;

}
