package com.ray.wms.table.vo.warehouse;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 仓库
 * @Class: WarehouseBase
 * @Package com.ray.wms.table.params.warehouse
 * @date 2020/6/2 16:24
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class WarehouseVO extends BaseVO{

    /**
     * 仓库编码
     */
    @ApiModelProperty(value = "仓库编码", required = true)
    private String warehouseCode;

    /**
     * 仓库名称
     */
    @ApiModelProperty(value = "仓库名称", required = true)
    private String warehouseName;

}
