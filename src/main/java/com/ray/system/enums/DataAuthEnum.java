package com.ray.system.enums;

import lombok.Getter;
import org.springframework.util.StringUtils;

/**
 * @author bo shen
 * @Description: 数据权限
 * @Class: DataAuthEnum
 * @Package com.ray.system.enums
 * @date 2020/5/27 10:50
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Getter
public enum DataAuthEnum {
    ALL("ALL", "全部"),
    GROUP_COMPANY("GROUP_COMPANY", "总公司"),
    COMPANY("COMPANY", "公司"),
    ORG("ORG", "部门"),
    PERSON("PERSON", "个人"),;

    DataAuthEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    /***值***/
    private String value;
    /***名称描述***/
    private String name;

    /**
     * 获取枚举对象
     *
     * @param value 值
     * @return
     */
    public static DataAuthEnum getEnum(String value) {
        for (DataAuthEnum entity : DataAuthEnum.values()) {
            if (StringUtils.pathEquals(value, entity.value)) {
                return entity;
            }
        }
        return null;
    }

    /**
     * value是否在列表中
     *
     * @param value
     * @return
     */
    public static boolean valueInEnum(String value) {
        for (DataAuthEnum entity : DataAuthEnum.values()) {
            if (StringUtils.pathEquals(value, entity.value)) {
                return true;
            }
        }
        return false;
    }
}
