package com.ray.system.enums;

import lombok.Getter;
import org.springframework.util.StringUtils;

/**
 * @author bo shen
 * @Description: 文件
 * @Class: FileTypeEnum
 * @Package com.ray.system.enums
 * @date 2020/5/27 10:50
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Getter
public enum FileTypeEnum {
    PAY("PAY", "支付"),
    PURCHASE("PURCHASE", "采购"),
    PURCHASE_IN("PURCHASE_IN", "采购入库"),
    PURCHASE_BACK("PURCHASE_BACK", "采购退货"),
    SALE("SALE", "销售"),
    SALE_OUT("SALE_OUT", "销售出库"),
    SALE_BACK("SALE_BACK", "销售退货"),
    ORDER("ORDER", "订单"),
    ADVANCE("ADVANCE", "预付款"),
  ;

    FileTypeEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    /***值***/
    private String value;
    /***名称描述***/
    private String name;

    /**
     * 获取枚举对象
     *
     * @param value 值
     * @return
     */
    public static FileTypeEnum getEnum(String value) {
        for (FileTypeEnum entity : FileTypeEnum.values()) {
            if (StringUtils.pathEquals(value, entity.value)) {
                return entity;
            }
        }
        return null;
    }

    /**
     * value是否在列表中
     *
     * @param value
     * @return
     */
    public static boolean valueInEnum(String value) {
        for (FileTypeEnum entity : FileTypeEnum.values()) {
            if (StringUtils.pathEquals(value, entity.value)) {
                return true;
            }
        }
        return false;
    }
}
