package com.ray.system.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.system.api.OrganizationApi;
import com.ray.system.table.params.organization.OrganizationCreateParams;
import com.ray.system.table.params.organization.OrganizationEditParams;
import com.ray.system.table.params.organization.OrganizationQueryParams;
import com.ray.system.table.vo.organization.OranizationTreeVO;
import com.ray.system.table.vo.organization.OrganizationVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-部门管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/sys/organization")
@Api("系统基础服务-部门")
@Validated
public class OrganizationController {

    @Autowired
    private OrganizationApi organizationApi;

    @ApiOperation("查询部门列表查询")
    @RequestMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<OrganizationVO>> page(@RequestBody @Valid CommonPage<OrganizationQueryParams, Page<OrganizationVO>> queryParams) {
        return organizationApi.pageOrganizations(queryParams);
    }

    @ApiOperation("查询部门列表")
    @RequestMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<OrganizationVO>> list(@RequestBody @Valid OrganizationQueryParams queryParams) {
        return organizationApi.queryOrganizations(queryParams);
    }


    @ApiOperation("树形列表")
    @RequestMapping(value = "/tree", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<OranizationTreeVO>> tree(@RequestBody @Valid OrganizationQueryParams queryParams) {
        return organizationApi.tree(queryParams);
    }

    @ApiOperation("部门新增")
    @RequestMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid OrganizationCreateParams createParams) {
        return organizationApi.createOrganization(createParams);
    }

    @ApiOperation("部门编辑")
    @RequestMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid OrganizationEditParams editParams) {
        return organizationApi.editOrganization(editParams);
    }

    @ApiOperation("部门删除")
    @RequestMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "部门编码", required = true) @RequestParam(required = true) String deptCode) {
        return organizationApi.deleteOrganization(deptCode);
    }

    @ApiOperation("部门详情")
    @RequestMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<OrganizationVO> view(@ApiParam(value = "部门编码", required = true) @RequestParam(required = true) String deptCode) {
        return organizationApi.viewOrganization(deptCode);
    }

    @ApiOperation("部门开启")
    @RequestMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "部门编码", required = true) @RequestParam(required = true) String deptCode) {
        return organizationApi.openOrganization(deptCode);
    }


    @ApiOperation("部门关闭")
    @RequestMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "部门编码", required = true) @RequestParam(required = true) String deptCode) {
        return organizationApi.closeOrganization(deptCode);
    }


}
