package com.ray.system.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.system.api.RoleApi;
import com.ray.system.table.params.role.*;
import com.ray.system.table.vo.role.RoleVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-角色管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/sys/role")
@Api("系统基础服务-角色")
@Validated
public class RoleController {

    @Autowired
    private RoleApi roleApi;

    @ApiOperation("查询角色列表查询")
    @RequestMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<RoleVO>> page(@RequestBody @Valid CommonPage<RoleQueryParams, Page<RoleVO>> queryParams) {
        return roleApi.pageRoles(queryParams);
    }

    @ApiOperation("查询角色列表查询")
    @RequestMapping(value = "/pageForManager", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<RoleVO>> pageForManager(@RequestBody @Valid CommonPage<RoleManagerQueryParams, Page<RoleVO>> queryParams) {
        return roleApi.pageRolesForManager(queryParams);
    }

    @ApiOperation("查询角色列表")
    @RequestMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<RoleVO>> list(@RequestBody @Valid RoleQueryParams queryParams) {
        return roleApi.queryRoles(queryParams);
    }
    @ApiOperation("查询角色列表-公司可用")
    @RequestMapping(value = "/listForManager", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<RoleVO>> listForManager(@RequestBody @Valid UserRoleQueryParams queryParams) {
        return roleApi.listForManager(queryParams);
    }



    @ApiOperation("角色新增")
    @RequestMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid RoleCreateParams createParams) {
        return roleApi.createRole(createParams);
    }

    @ApiOperation("角色编辑")
    @RequestMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid RoleEditParams editParams) {
        return roleApi.editRole(editParams);
    }

    @ApiOperation("角色新增-管理")
    @RequestMapping(value = "/addForManager", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> addForManager(@RequestBody @Valid RoleManagerCreateParams createParams) {
        return roleApi.createRoleForManager(createParams);
    }

    @ApiOperation("角色编辑-管理")
    @RequestMapping(value = "/editForManager", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> editForManager(@RequestBody @Valid RoleManagerEditParams editParams) {
        return roleApi.editRoleForManager(editParams);
    }

    @ApiOperation("角色删除")
    @RequestMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "角色编码", required = true) @RequestParam(required = true) String roleCode) {
        return roleApi.deleteRole(roleCode);
    }

    @ApiOperation("角色详情")
    @RequestMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<RoleVO> view(@ApiParam(value = "角色编码", required = true) @RequestParam(required = true) String roleCode) {
        return roleApi.viewRole(roleCode);
    }

    @ApiOperation("角色开启")
    @RequestMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "角色编码", required = true) @RequestParam(required = true) String roleCode) {
        return roleApi.openRole(roleCode);
    }


    @ApiOperation("角色关闭")
    @RequestMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "角色编码", required = true) @RequestParam(required = true) String roleCode) {
        return roleApi.closeRole(roleCode);
    }


    @ApiOperation("角色添加授权")
    @RequestMapping(value = "/addAuth", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> addAuth(@RequestBody @Valid RoleAuthParams authParams) {
        return roleApi.addAuth(authParams);
    }

    @ApiOperation("角色移除授权")
    @RequestMapping(value = "/removeAuth", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> removeAuth(@RequestBody @Valid RoleAuthParams authParams) {
        return roleApi.removeAuth(authParams);
    }


}
