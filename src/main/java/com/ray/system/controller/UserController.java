package com.ray.system.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.system.api.UserApi;
import com.ray.system.table.params.user.*;
import com.ray.system.table.vo.user.UserVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-用户管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/sys/user")
@Api("系统基础服务-用户")
@Validated
public class UserController {

    @Autowired
    private UserApi userApi;

    @ApiOperation("查询用户列表查询")
    @RequestMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<UserVO>> page(@RequestBody @Valid CommonPage<UserQueryParams, Page<UserVO>> queryParams) {
        return userApi.pageUsers(queryParams);
    }


    @ApiOperation("查询用户列表查询-所有用户")
    @RequestMapping(value = "/pageAll", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<UserVO>> pageAll(@RequestBody @Valid CommonPage<UserQueryParams, Page<UserVO>> queryParams) {
        return userApi.pageAll(queryParams);
    }

    @ApiOperation("查询用户列表查询")
    @RequestMapping(value = "/pageForManager", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<UserVO>> pageForManager(@RequestBody @Valid CommonPage<UserManagerQueryParams, Page<UserVO>> queryParams) {
        return userApi.pageUsersForManager(queryParams);
    }

    @ApiOperation("查询用户列表")
    @RequestMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<UserVO>> page(@RequestBody @Valid UserQueryParams queryParams) {
        return userApi.queryUsers(queryParams);
    }


    @ApiOperation("用户新增")
    @RequestMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid UserCreateParams createParams) {
        return userApi.createUser(createParams);
    }

    @ApiOperation("用户编辑")
    @RequestMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid UserEditParams editParams) {
        return userApi.editUser(editParams);
    }

    @ApiOperation("用户编辑自己")
    @RequestMapping(value = "/editMy", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> editMy(@RequestBody @Valid MyEditParams editParams) {
        return userApi.editMy(editParams);
    }

    @ApiOperation("用户添加授权")
    @RequestMapping(value = "/addAuth", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> addAuth(@RequestBody @Valid UserAuthParams authParams) {
        return userApi.addAuth(authParams);
    }

    @ApiOperation("用户移除授权")
    @RequestMapping(value = "/removeAuth", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> removeAuth(@RequestBody @Valid UserAuthParams authParams) {
        return userApi.removeAuth(authParams);
    }

    @ApiOperation("用户详情")
    @RequestMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<UserVO> view(@ApiParam(value = "用户编码", required = true) @RequestParam(required = true) String userCode,
    @ApiParam(value = "企业编码", required = false) @RequestParam(required = false) String companyCode) {
        return userApi.viewUser(userCode,companyCode);
    }

    @ApiOperation("用户开启")
    @RequestMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@RequestBody @Valid UserOpenCloseParams params) {
        return userApi.openUser(params);
    }


    @ApiOperation("用户关闭")
    @RequestMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@RequestBody @Valid UserOpenCloseParams params) {
        return userApi.closeUser(params);
    }


    @ApiOperation("用户开启-运营")
    @RequestMapping(value = "/openForManager", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> openForManager(@ApiParam(value = "用户编码", required = true) @RequestParam(required = true) String userCode) {
        return userApi.openUserForManageer(userCode);
    }


    @ApiOperation("用户关闭-运营")
    @RequestMapping(value = "/closeForManager", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> closeForManager(@ApiParam(value = "用户编码", required = true) @RequestParam(required = true) String userCode) {
        return userApi.closeUserForManageer(userCode);
    }

    @ApiOperation("重置密码-运营")
    @RequestMapping(value = "/resetPassword", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> resetPassword(@ApiParam(value = "用户编码", required = true) @RequestParam(required = true) String userCode) {
        return userApi.resetPassword(userCode);
    }

    @ApiOperation("重置密码-运营")
    @RequestMapping(value = "/updatePassword", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> updatePassword(@RequestBody @Valid  UpdatePasswordParams updatePasswordParams ) {
        return userApi.updatePassword(updatePasswordParams);
    }

    @ApiOperation("变更职位")
    @RequestMapping(value = "/changePosition", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> changePosition(@RequestBody @Valid  PositionChangeParams changeParams ) {
        return userApi.positionChange(changeParams);
    }

}
