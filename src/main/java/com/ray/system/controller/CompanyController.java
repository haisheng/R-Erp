package com.ray.system.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.system.api.CompanyApi;
import com.ray.system.table.params.company.*;
import com.ray.system.table.vo.company.CompanyVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-公司管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/sys/company")
@Api("系统基础服务-公司")
@Validated
public class CompanyController {

    @Autowired
    private CompanyApi companyApi;

    @ApiOperation("查询公司列表查询")
    @RequestMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<CompanyVO>> page(@RequestBody @Valid CommonPage<CompanyQueryParams, Page<CompanyVO>> queryParams) {
        return companyApi.pageCompanys(queryParams);
    }

    @ApiOperation("查询公司列表查询")
    @RequestMapping(value = "/pageForManager", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<CompanyVO>> pageForManager(@RequestBody @Valid CommonPage<CompanyQueryParams, Page<CompanyVO>> queryParams) {
        return companyApi.pageCompanysForManager(queryParams);
    }

    @ApiOperation("查询公司列表")
    @RequestMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<CompanyVO>> list(@RequestBody @Valid CompanyQueryParams queryParams) {
        return companyApi.queryCompanys(queryParams);
    }


    @ApiOperation("公司新增")
    @RequestMapping(value = "/addForManager", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> addForManager(@RequestBody @Valid CompanyCreateParams createParams) {
        return companyApi.createCompanyForManager(createParams);
    }

    @ApiOperation("公司新增")
    @RequestMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid CompanyCreateParams createParams) {
        return companyApi.createCompany(createParams);
    }

    @ApiOperation("公司编辑")
    @RequestMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid CompanyEditParams editParams) {
        return companyApi.editCompany(editParams);
    }

    @ApiOperation("公司删除")
    @RequestMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "公司编码", required = true) @RequestParam(required = true) String companyCode) {
        return companyApi.deleteCompany(companyCode);
    }

    @ApiOperation("公司详情")
    @RequestMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<CompanyVO> view(@ApiParam(value = "公司编码", required = true) @RequestParam(required = true) String companyCode) {
        return companyApi.viewCompany(companyCode);
    }

    @ApiOperation("公司开启")
    @RequestMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "公司编码", required = true) @RequestParam(required = true) String companyCode) {
        return companyApi.openCompany(companyCode);
    }


    @ApiOperation("公司关闭")
    @RequestMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "公司编码", required = true) @RequestParam(required = true) String companyCode) {
        return companyApi.closeCompany(companyCode);
    }

    @ApiOperation("公司新增用户")
    @RequestMapping(value = "/addUser", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> addUser(@RequestBody @Valid CompanyUserCreateParams createParams) {
        return companyApi.createAdmin(createParams);
    }

    @ApiOperation("公司编辑用户")
    @RequestMapping(value = "/editUser", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> editUser(@RequestBody @Valid CompanyUserEditParams editParams) {
        return companyApi.editAdmin(editParams);
    }

    @ApiOperation("运营编辑用户")
    @RequestMapping(value = "/superEditUser", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> superEditUser(@RequestBody @Valid AdminUserEditParams editParams) {
        return companyApi.superEdit(editParams);
    }
}
