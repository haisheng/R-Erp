package com.ray.system.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.system.api.WorkApi;
import com.ray.system.table.params.work.WorkCreateParams;
import com.ray.system.table.params.work.WorkEditParams;
import com.ray.system.table.params.work.WorkQueryParams;
import com.ray.system.table.vo.work.WorkVO;
import com.ray.util.DateUtils;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 系统基础服务-待办管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/sys/work")
@Api("系统基础服务-待办")
@Validated
public class WorkController {

    @Autowired
    private WorkApi workApi;

    @ApiOperation("查询待办列表查询")
    @RequestMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<WorkVO>> page(@RequestBody @Valid CommonPage<WorkQueryParams, Page<WorkVO>> queryParams) {
        return workApi.pageWorks(queryParams);
    }

    @ApiOperation("查询待办列表")
    @RequestMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<WorkVO>> list(@RequestBody @Valid WorkQueryParams queryParams) {
        return workApi.queryWorks(queryParams);
    }

    @ApiOperation("查询待办列表")
    @RequestMapping(value = "/today", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<WorkVO>> today(@RequestBody @Valid WorkQueryParams queryParams) {
        queryParams.setWorkStart(LocalDateTime.now());
        queryParams.setWorkEnd(LocalDateTime.now());
        return workApi.queryWorks(queryParams);
    }

    @ApiOperation("待办新增")
    @RequestMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid WorkCreateParams createParams) {
        return workApi.createWork(createParams);
    }

    @ApiOperation("待办编辑")
    @RequestMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid WorkEditParams editParams) {
        return workApi.editWork(editParams);
    }

    @ApiOperation("待办删除")
    @RequestMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "待办编码", required = true) @RequestParam(required = true) String workCode) {
        return workApi.deleteWork(workCode);
    }

    @ApiOperation("待办详情")
    @RequestMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<WorkVO> view(@ApiParam(value = "待办编码", required = true) @RequestParam(required = true) String workCode) {
        return workApi.viewWork(workCode);
    }

    @ApiOperation("待办开启")
    @RequestMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "待办编码", required = true) @RequestParam(required = true) String workCode) {
        return workApi.openWork(workCode);
    }


    @ApiOperation("待办关闭")
    @RequestMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "待办编码", required = true) @RequestParam(required = true) String workCode) {
        return workApi.closeWork(workCode);
    }


}
