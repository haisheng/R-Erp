package com.ray.system.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.system.api.PositionApi;
import com.ray.system.table.params.position.PositionCreateParams;
import com.ray.system.table.params.position.PositionEditParams;
import com.ray.system.table.params.position.PositionQueryParams;
import com.ray.system.table.vo.position.PositionVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-职位管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/sys/position")
@Api("系统基础服务-职位")
@Validated
public class PositionController {

    @Autowired
    private PositionApi positionApi;

    @ApiOperation("查询职位列表查询")
    @RequestMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<PositionVO>> page(@RequestBody @Valid CommonPage<PositionQueryParams, Page<PositionVO>> queryParams) {
        return positionApi.pagePositions(queryParams);
    }

    @ApiOperation("查询职位列表")
    @RequestMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<PositionVO>> list(@RequestBody @Valid PositionQueryParams queryParams) {
        return positionApi.queryPositions(queryParams);
    }

    @ApiOperation("查询职位列表")
    @RequestMapping(value = "/usable", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<PositionVO>> usable(@RequestBody @Valid PositionQueryParams queryParams) {
        return positionApi.queryUsablePositions(queryParams);
    }


    @ApiOperation("职位新增")
    @RequestMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid PositionCreateParams createParams) {
        return positionApi.createPosition(createParams);
    }

    @ApiOperation("职位编辑")
    @RequestMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid PositionEditParams editParams) {
        return positionApi.editPosition(editParams);
    }

    @ApiOperation("职位删除")
    @RequestMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "职位编码", required = true) @RequestParam(required = true) String positionCode) {
        return positionApi.deletePosition(positionCode);
    }

    @ApiOperation("职位详情")
    @RequestMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<PositionVO> view(@ApiParam(value = "职位编码", required = true) @RequestParam(required = true) String positionCode) {
        return positionApi.viewPosition(positionCode);
    }

    @ApiOperation("职位开启")
    @RequestMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "职位编码", required = true) @RequestParam(required = true) String positionCode) {
        return positionApi.openPosition(positionCode);
    }


    @ApiOperation("职位关闭")
    @RequestMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "职位编码", required = true) @RequestParam(required = true) String positionCode) {
        return positionApi.closePosition(positionCode);
    }


}
