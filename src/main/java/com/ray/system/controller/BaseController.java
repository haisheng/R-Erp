package com.ray.system.controller;


import com.ray.system.api.BaseApi;
import com.ray.system.table.params.base.LoginParams;
import com.ray.system.table.vo.menu.AuthVO;
import com.ray.system.table.vo.menu.MenuTreeVO;
import com.ray.woodencreate.annotation.Login;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.annotation.NoLogin;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 系统基础服务
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/")
@Api("系统基础服务")
@Validated
public class BaseController {

    @Autowired
    private BaseApi baseApi;

    @ApiOperation("用户登陆")
    @PostMapping(value = "/login",consumes = MediaType.APPLICATION_JSON_VALUE)
    @Login
    @NoAuth
    @NoLogin
    public Result<LoginUser> login(@RequestBody @Valid LoginParams loginParams, HttpServletRequest request){
        return  baseApi.login(loginParams,request);
    }

    @ApiOperation("图片验证码")
    @PostMapping(value = "/imageCode",consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    @NoLogin
    public Result<String> imageCode(HttpServletRequest request){
        return  baseApi.queryImageCode(request);
    }


    @ApiOperation("退出登录")
    @PostMapping(value = "/logOut",consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> logOut(){
        return  baseApi.logOut();
    }

    @ApiOperation("获取菜单")
    @PostMapping(value = "/leftMenu",consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<MenuTreeVO>> leftMenu(){
        return  baseApi.leftMenu();
    }

    @ApiOperation("获取按钮权限")
    @PostMapping(value = "/auth",consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<Set<String>> auth(){
        return  baseApi.auth();
    }

    @ApiOperation("切换公司")
    @PostMapping(value = "/switchCompany",consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<LoginUser> switchCompany(@ApiParam(value = "公司编码",required = true) @RequestParam(required = true) String companyCode){
        return  baseApi.switchCompany(companyCode);
    }
}
