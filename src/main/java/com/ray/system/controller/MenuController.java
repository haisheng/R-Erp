package com.ray.system.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.system.api.MenuApi;
import com.ray.system.table.params.CodeParams;
import com.ray.system.table.params.menu.MenuCreateParams;
import com.ray.system.table.params.menu.MenuEditParams;
import com.ray.system.table.params.menu.MenuQueryParams;
import com.ray.system.table.params.role.RoleManagerQueryParams;
import com.ray.system.table.vo.menu.MenuTreeVO;
import com.ray.system.table.vo.menu.MenuVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-菜单管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/sys/menu")
@Api("系统基础服务-菜单")
@Validated
public class MenuController {

    @Autowired
    private MenuApi menuApi;

    @ApiOperation("查询菜单列表查询")
    @RequestMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<MenuVO>> page(@RequestBody @Valid CommonPage<MenuQueryParams, Page<MenuVO>> queryParams) {
        return menuApi.pageMenus(queryParams);
    }


    @ApiOperation("查询菜单列表")
    @RequestMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<MenuVO>> list(@RequestBody @Valid MenuQueryParams queryParams) {
        return menuApi.queryMenus(queryParams);
    }

    @ApiOperation("树形列表")
    @RequestMapping(value = "/tree", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<MenuTreeVO>> tree(@RequestBody @Valid CodeParams queryParams) {
        return menuApi.tree(queryParams);
    }

    @ApiOperation("树形列表--权限")
    @RequestMapping(value = "/authTree", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<MenuTreeVO>> authTree(@RequestBody @Valid CodeParams queryParams) {
        return menuApi.authTree(queryParams);
    }


    @ApiOperation("菜单新增")
    @RequestMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid MenuCreateParams createParams) {
        return menuApi.createMenu(createParams);
    }

    @ApiOperation("菜单编辑")
    @RequestMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid MenuEditParams editParams) {
        return menuApi.editMenu(editParams);
    }

    @ApiOperation("菜单删除")
    @RequestMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "菜单编码", required = true) @RequestParam(required = true) String menuCode) {
        return menuApi.deleteMenu(menuCode);
    }

    @ApiOperation("菜单详情")
    @RequestMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<MenuVO> view(@ApiParam(value = "菜单编码", required = true) @RequestParam(required = true) String menuCode) {
        return menuApi.viewMenu(menuCode);
    }

    @ApiOperation("菜单开启")
    @RequestMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "菜单编码", required = true) @RequestParam(required = true) String menuCode) {
        return menuApi.openMenu(menuCode);
    }


    @ApiOperation("菜单关闭")
    @RequestMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "菜单编码", required = true) @RequestParam(required = true) String menuCode) {
        return menuApi.closeMenu(menuCode);
    }

    @ApiOperation("自动生成按钮")
    @RequestMapping(value = "/createBtn", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> createBtn(@ApiParam(value = "菜单编码", required = true) @RequestParam(required = true) String menuCode) {
        return menuApi.createBtn(menuCode);
    }
}
