package com.ray.system.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.system.api.AppApi;
import com.ray.system.table.params.app.AppCreateParams;
import com.ray.system.table.params.app.AppEditParams;
import com.ray.system.table.params.app.AppQueryParams;
import com.ray.system.table.vo.app.AppVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-应用管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/sys/app")
@Api("系统基础服务-应用")
@Validated
public class AppController {

    @Autowired
    private AppApi appApi;

    @ApiOperation("查询APP列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<AppVO>> page(@RequestBody @Valid CommonPage<AppQueryParams, Page<AppVO>> queryParams) {
        return appApi.pageApps(queryParams);
    }

    @ApiOperation("查询APP列表")
    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<AppVO>> page(@RequestBody @Valid AppQueryParams queryParams) {
        return appApi.queryApps(queryParams);
    }


    @ApiOperation("APP新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid AppCreateParams createParams) {
        return appApi.createApp(createParams);
    }

    @ApiOperation("APP编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid AppEditParams editParams) {
        return appApi.editApp(editParams);
    }

    @ApiOperation("APP删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "应用编码", required = true) @RequestParam(required = true) String appCode) {
        return appApi.deleteApp(appCode);
    }

    @ApiOperation("APP详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<AppVO> view(@ApiParam(value = "应用编码", required = true) @RequestParam(required = true) String appCode) {
        return appApi.viewApp(appCode);
    }

    @ApiOperation("APP开启")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "应用编码", required = true) @RequestParam(required = true) String appCode) {
        return appApi.openApp(appCode);
    }


    @ApiOperation("APP关闭")
    @PostMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "应用编码", required = true) @RequestParam(required = true) String appCode) {
        return appApi.closeApp(appCode);
    }


}
