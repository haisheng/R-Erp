package com.ray.system.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.system.table.dto.WorkQueryDTO;
import com.ray.system.table.entity.SysWork;
import com.ray.system.table.mapper.SysWorkMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 我的待办任务 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-07-20
 */
@Service
public class SysWorkService extends ServiceImpl<SysWorkMapper, SysWork>  {
    /**
     * 编辑待办
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(SysWork entity, LoginUser loginUser) {
        SysWork query = new SysWork();
        query.setWorkCode(entity.getWorkCode());
        UpdateWrapper<SysWork> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addUserDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询待办
     *
     * @param workCode 待办编码
     * @param loginUser    当前操作员
     * @return
     */
    public SysWork queryWorkByWorkCode(String workCode, LoginUser loginUser) {
        SysWork query = new SysWork();
        query.setWorkCode(workCode);
        QueryWrapper<SysWork> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addUserDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }



    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<SysWork> page(CommonPage<WorkQueryDTO, Page<SysWork>> queryParams, LoginUser loginUser) {
        WorkQueryDTO params = queryParams.getEntity();
        SysWork entity = BeanCreate.newBean(SysWork.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<SysWork> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addUserDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getWorkTitleLike()), "work_title", params.getWorkTitleLike());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getWorkStart()), "work_start_time", params.getWorkStart());
            queryWrapper.gt(ObjectUtil.isNotNull(params.getWorkEnd()), "work_end_time", params.getWorkEnd());
        }
        queryWrapper.orderByDesc("work_end_time");
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<SysWork> list(WorkQueryDTO queryParams, LoginUser loginUser) {
        SysWork entity = BeanCreate.newBean(SysWork.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<SysWork> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addUserDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getWorkTitleLike()), "work_title", queryParams.getWorkTitleLike());
       queryWrapper.lt(ObjectUtil.isNotNull(queryParams.getWorkStart()), "work_start_time", queryParams.getWorkStart());
       queryWrapper.gt(ObjectUtil.isNotNull(queryParams.getWorkEnd()), "work_end_time", queryParams.getWorkEnd());
        queryWrapper.orderByAsc("work_start_time");
        return list(queryWrapper);
    }

}
