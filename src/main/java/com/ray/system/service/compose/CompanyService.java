package com.ray.system.service.compose;

import cn.hutool.core.util.ObjectUtil;
import com.ray.system.service.SysOrganizationService;
import com.ray.system.service.SysUserCompanyService;
import com.ray.system.service.SysUserInfoService;
import com.ray.system.table.entity.SysOrganization;
import com.ray.system.table.entity.SysUserCompany;
import com.ray.system.table.entity.SysUserInfo;
import com.ray.validate.support.utils.ValidateUtil;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author bo shen
 * @Description: 聚合服务  公司相关
 * @Class: CompanyService
 * @Package com.ray.system.service.compose
 * @date 2020/5/28 11:10
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class CompanyService {
    @Autowired
    private SysUserCompanyService sysUserCompanyService;
    @Autowired
    private SysOrganizationService sysOrganizationService;
    @Autowired
    private SysUserInfoService sysUserInfoService;

    /**
     * 删除公司相关关联信息
     *
     * @param companyCode
     * @param loginUser
     * @return
     */
    public Boolean deleteLink(String companyCode, LoginUser loginUser) {
        //删除部门信息
        sysOrganizationService.deleteLink(companyCode, loginUser);
        //删除公司与人员的关系
        sysUserCompanyService.deleteLink(companyCode, loginUser);
        //删除人员扩展信息
        sysUserInfoService.deleteLink(companyCode, loginUser);
        return true;
    }

    /**
     * 火说
     *
     * @param userCode
     * @param companyCode
     * @return
     */
    public SysOrganization getOrganizationForLogin(String userCode, String companyCode) {
        ValidateUtil.hasLength(userCode, "参数[userCode]不能为空");
        ValidateUtil.hasLength(companyCode, "参数[companyCode]不能为空");
        SysUserCompany sysUserCompany = sysUserCompanyService.getEntityForLogin(userCode, companyCode);
        //存在职位就返回职位详情
        if (ObjectUtil.isNotNull(sysUserCompany)) {
            return  sysOrganizationService.queryOrganizationByLogin(sysUserCompany.getDeptCode());
        }
        return null;
    }
}
