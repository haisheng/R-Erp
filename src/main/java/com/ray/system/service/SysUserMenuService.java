package com.ray.system.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.system.table.entity.SysPosition;
import com.ray.system.table.entity.SysUserMenu;
import com.ray.system.table.mapper.SysUserMenuMapper;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 用户菜单关联 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@Service
public class SysUserMenuService extends ServiceImpl<SysUserMenuMapper, SysUserMenu> {

    @Autowired
    private  SysUserMenuMapper sysUserMenuMapper;

    /**
     * 查询用户是否拥有该菜单
     * @param userCode
     * @param menuCode
     * @return
     */
    public boolean queryUserHasMenu(String userCode,String menuCode) {
        //查询用户拥有的菜单
        SysUserMenu sysUserMenu = sysUserMenuMapper.queryUserMenu(userCode,menuCode);
        return ObjectUtil.isNotNull(sysUserMenu);
    }


    /**
     * 编辑角色菜单关联
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean delete(SysUserMenu entity, LoginUser loginUser) {
        SysUserMenu query = new SysUserMenu();
        query.setMenuCode(entity.getMenuCode());
        query.setRoleCode(entity.getRoleCode());
        query.setIsDeleted(DeleteEnum.USE.getValue());
        UpdateWrapper<SysUserMenu> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询角色对应菜单
     * @param roleCode
     * @return
     */
    public List<SysUserMenu> queryMenuByRoleCode(String roleCode) {
        SysUserMenu query = new SysUserMenu();;
        query.setRoleCode(roleCode);
        query.setStatus(YesOrNoEnum.YES.getValue());
        query.setIsDeleted(DeleteEnum.USE.getValue());
        QueryWrapper<SysUserMenu> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.notDelete(queryWrapper);
        return  list(queryWrapper);
    }
}
