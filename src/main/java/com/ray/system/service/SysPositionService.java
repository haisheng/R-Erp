package com.ray.system.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.system.table.dto.PositionQueryDTO;
import com.ray.system.table.entity.SysPosition;
import com.ray.system.table.mapper.SysPositionMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户菜单关联 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-27
 */
@Service
public class SysPositionService extends ServiceImpl<SysPositionMapper, SysPosition> {

    /**
     * 编辑职位
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(SysPosition entity, LoginUser loginUser) {
        SysPosition query = new SysPosition();
        query.setPositionCode(entity.getPositionCode());
        UpdateWrapper<SysPosition> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询职位
     *
     * @param positionCode 职位编码
     * @param loginUser    当前操作员
     * @return
     */
    public SysPosition queryPositionByPositionCode(String positionCode, LoginUser loginUser) {
        SysPosition query = new SysPosition();
        query.setPositionCode(positionCode);
        QueryWrapper<SysPosition> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 查询职位
     *
     * @param positionCode 职位编码
     * @return
     */
    public SysPosition queryGPositionByPositionCode(String positionCode) {
        SysPosition query = new SysPosition();
        query.setPositionCode(positionCode);
        QueryWrapper<SysPosition> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.notDelete(queryWrapper);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<SysPosition> page(CommonPage<PositionQueryDTO, Page<SysPosition>> queryParams, LoginUser loginUser) {
        PositionQueryDTO params = queryParams.getEntity();
        SysPosition entity = BeanCreate.newBean(SysPosition.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<SysPosition> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getPositionNameLike()), "position_name", params.getPositionNameLike());
        }
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<SysPosition> list(PositionQueryDTO queryParams, LoginUser loginUser) {
        SysPosition entity = BeanCreate.newBean(SysPosition.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<SysPosition> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getPositionNameLike()), "position_name", queryParams.getPositionNameLike());
        return list(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @return
     */
    public List<SysPosition> usable(PositionQueryDTO queryParams) {
        SysPosition entity = BeanCreate.newBean(SysPosition.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<SysPosition> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getPositionNameLike()), "position_name", queryParams.getPositionNameLike());
        return list(queryWrapper);
    }

    /**
     * 获取职位详情
     * @param positionCode
     * @return
     */
    public SysPosition queryPositionByLogin(String positionCode) {
        SysPosition query = new SysPosition();
        query.setPositionCode(positionCode);
        query.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<SysPosition> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.notDelete(queryWrapper);
        return getOne(queryWrapper);
    }
}
