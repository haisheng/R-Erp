package com.ray.system.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.system.table.entity.SysUserInfo;
import com.ray.system.table.entity.SysUserPosition;
import com.ray.system.table.mapper.SysUserPositionMapper;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户菜单关联 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-27
 */
@Service
public class SysUserPositionService extends ServiceImpl<SysUserPositionMapper, SysUserPosition> {

    /**
     * 更新职位
     * @param entity
     * @return
     */
    public boolean changePosition(SysUserPosition entity) {
        SysUserPosition query = new SysUserPosition();
        query.setCompanyCode(entity.getCompanyCode());
        query.setUserCode(entity.getUserCode());
        UpdateWrapper<SysUserPosition> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 获取用户在公司的职位
     * @param userCode
     * @param companyCode
     * @return
     */
    public SysUserPosition queryPositionByLogin(String userCode, String companyCode) {
        SysUserPosition query = new SysUserPosition();
        query.setCompanyCode(companyCode);
        query.setUserCode(userCode);
        QueryWrapper<SysUserPosition> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.notDelete(queryWrapper);
        return getOne(queryWrapper);
    }
}
