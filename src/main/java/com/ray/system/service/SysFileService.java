package com.ray.system.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.base.table.dto.FileDTO;
import com.ray.system.builder.FileBuilder;
import com.ray.system.enums.FileTypeEnum;
import com.ray.system.table.entity.SysFile;
import com.ray.system.table.mapper.SysFileMapper;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.util.Assert;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 系统文件表 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-07-14
 */
@Service
public class SysFileService extends ServiceImpl<SysFileMapper, SysFile>{


    /**
     * 保存文件
     * @param businessCode
     * @param fileTypeEnum
     * @param files
     * @param loginUser
     * @return
     */
    public Boolean saveFile(String businessCode, FileTypeEnum fileTypeEnum, List<FileDTO> files, LoginUser loginUser){
        Assert.hasLength(businessCode,"业务单号不能为空");
        if(ObjectUtil.isNotEmpty(files)){
            //构建文件信息
            List<SysFile> sysFiles = files.stream().map(fileDTO -> {
                FileBuilder fileBuilder = new FileBuilder();
                fileBuilder.append(fileDTO).appendCreate(loginUser).appendBusinessCode(businessCode).appendType(fileTypeEnum.getValue());
                return fileBuilder.bulid();
            }).collect(Collectors.toList());
            return saveBatch(sysFiles);
        }
      return  true;
    }

    /**
     * 删除文件
     * @param businessCode
     * @param fileType
     * @param loginUser
     * @return
     */
    public Boolean deleteFile(String businessCode, String fileType, LoginUser loginUser){
        Assert.hasLength(businessCode,"业务单号不能为空");
        SysFile entity = new  SysFile();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity,loginUser);
        SysFile query = new SysFile();
        query.setIsDeleted(DeleteEnum.USE.getValue());
        query.setBusinessCode(businessCode);
        query.setBusinessType(fileType);
        UpdateWrapper<SysFile> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }


    /**
     * 删除文件
     * @param businessCode
     * @param fileType
     * @param loginUser
     * @return
     */
    public List<SysFile> queryFiles(String businessCode, String fileType, LoginUser loginUser){
        SysFile query = new SysFile();
        query.setIsDeleted(DeleteEnum.USE.getValue());
        query.setBusinessCode(businessCode);
        query.setBusinessType(fileType);
        QueryWrapper<SysFile> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }


}
