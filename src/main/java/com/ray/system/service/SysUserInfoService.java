package com.ray.system.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.system.builder.TemplateRecordBuilder;
import com.ray.system.table.entity.SysUserInfo;
import com.ray.system.table.mapper.SysUserInfoMapper;
import com.ray.template.TemplateRecord;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户扩展信息 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@Service
public class SysUserInfoService extends ServiceImpl<SysUserInfoMapper, SysUserInfo> {

    /**
     * 删除人员扩展信息
     *
     * @param companyCode
     * @param loginUser
     * @return
     */
    public boolean deleteLink(String companyCode, LoginUser loginUser) {
        SysUserInfo entity = new SysUserInfo();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        SysUserInfo query = new SysUserInfo();
        query.setCompanyCode(companyCode);
        UpdateWrapper<SysUserInfo> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 删除用户扩展信息
     *
     * @param userCode
     * @param loginUser
     * @return
     */
    public boolean deleteUserInfo(String userCode, LoginUser loginUser) {
        SysUserInfo entity = new SysUserInfo();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        SysUserInfo query = new SysUserInfo();
        query.setUserCode(userCode);
        UpdateWrapper<SysUserInfo> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 查询用户扩展信息
     *
     * @param userCode
     * @param loginUser
     * @return
     */
    public List<TemplateRecord> queryUserInfoByUserCode(String userCode, LoginUser loginUser) {
        SysUserInfo query = new SysUserInfo();
        query.setUserCode(userCode);
        QueryWrapper<SysUserInfo> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return list(queryWrapper).stream().map(sysCompanyInfo -> {
            return  new TemplateRecordBuilder().append(sysCompanyInfo).bulid();
        }).collect(Collectors.toList());
    }
}
