package com.ray.system.service;

import com.ray.system.table.entity.SysOpLog;
import com.ray.system.table.mapper.SysOpLogMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户操作日志 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@Service
public class SysOpLogService extends ServiceImpl<SysOpLogMapper, SysOpLog> {

}
