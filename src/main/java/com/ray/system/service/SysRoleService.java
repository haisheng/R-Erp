package com.ray.system.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.system.table.dto.RoleQueryDTO;
import com.ray.system.table.entity.SysRole;
import com.ray.system.table.entity.SysRole;
import com.ray.system.table.mapper.SysRoleMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 系统角色表 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@Service
public class SysRoleService extends ServiceImpl<SysRoleMapper, SysRole> {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    /**
     * 编辑角色
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(SysRole entity, LoginUser loginUser) {
        SysRole query = new SysRole();
        query.setRoleCode(entity.getRoleCode());
        UpdateWrapper<SysRole> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询角色
     *
     * @param RoleCode  角色编码
     * @param loginUser 当前操作员
     * @return
     */
    public SysRole queryRoleByRoleCode(String RoleCode, LoginUser loginUser) {
        SysRole query = new SysRole();
        query.setRoleCode(RoleCode);
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<SysRole> page(CommonPage<RoleQueryDTO, Page<SysRole>> queryParams, LoginUser loginUser) {
        RoleQueryDTO params = queryParams.getEntity();
        SysRole entity = BeanCreate.newBean(SysRole.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getRoleNameLike()), "role_name", params.getRoleNameLike());
        }
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<SysRole> list(RoleQueryDTO queryParams, LoginUser loginUser) {
        SysRole entity = BeanCreate.newBean(SysRole.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getRoleNameLike()), "Role_name", queryParams.getRoleNameLike());
        return list(queryWrapper);
    }

    /**
     * 通过角色名称查询角色
     *
     * @param roleName
     * @param appCode
     * @return
     */
    public SysRole queryRoleByRoleName(String roleName, String appCode) {
        SysRole entity = BeanCreate.newBean(SysRole.class);
        entity.setRoleName(roleName);
        entity.setAppCode(appCode);
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        return getOne(queryWrapper);
    }

    /**
     * 查询用户在系统下拥有的角色
     * @param userCode
     * @param appCode
     * @return
     */
    public List<SysRole> listByUserCode(String userCode, String appCode) {
        return  sysRoleMapper.listByUserCode(userCode,appCode);
    }
}
