package com.ray.system.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.system.table.dto.CompanyQueryDTO;
import com.ray.system.table.entity.SysCompany;
import com.ray.system.table.mapper.SysCompanyMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 公司主表 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@Service
public class SysCompanyService extends ServiceImpl<SysCompanyMapper, SysCompany> {

    /**
     * 编辑公司
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(SysCompany entity, LoginUser loginUser) {
        SysCompany query = new SysCompany();
        query.setCompanyCode(entity.getCompanyCode());
        UpdateWrapper<SysCompany> pdateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(pdateWrapper, loginUser);
        return update(entity, pdateWrapper);
    }

    /**
     * 查询公司
     *
     * @param companyCode 公司编码
     * @param loginUser    当前操作员
     * @return
     */
    public SysCompany queryCompanyByCompanyCode(String companyCode, LoginUser loginUser) {
        SysCompany query = new SysCompany();
        query.setCompanyCode(companyCode);
        QueryWrapper<SysCompany> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<SysCompany> page(CommonPage<CompanyQueryDTO, Page<SysCompany>> queryParams, LoginUser loginUser) {
        CompanyQueryDTO params = queryParams.getEntity();
        SysCompany entity = BeanCreate.newBean(SysCompany.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<SysCompany> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getCompanyNameLike()), "company_name", params.getCompanyNameLike());
        }
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<SysCompany> list(CompanyQueryDTO queryParams, LoginUser loginUser) {
        SysCompany entity = BeanCreate.newBean(SysCompany.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<SysCompany> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getCompanyNameLike()), "company_name", queryParams.getCompanyNameLike());
        return list(queryWrapper);
    }

    /**
     * 通过公司名称查询公司
     * @param companyName
     * @return
     */
    public SysCompany queryCompanyByCompanyName(String companyName) {
        SysCompany entity = BeanCreate.newBean(SysCompany.class);
        entity.setCompanyName(companyName);
        QueryWrapper<SysCompany> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        return  getOne(queryWrapper);
    }

    /**
     * 查询子公司数量
     * @param companyCode
     * @return
     */
    public Integer countChildCompany(String companyCode) {
        SysCompany entity = BeanCreate.newBean(SysCompany.class);
        entity.setParentCompanyCode(companyCode );
        QueryWrapper<SysCompany> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        return count(queryWrapper);
    }
    /**
     * 通过公司名称查询公司
     * @param companyCode
     * @return
     */
    public SysCompany queryCompanyByLogin(String companyCode) {
        SysCompany entity = BeanCreate.newBean(SysCompany.class);
        entity.setCompanyCode(companyCode);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<SysCompany> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        return  getOne(queryWrapper);
    }

    /**
     * 批量获取公司信息
     * @param companyCodes
     * @return
     */
    public List<SysCompany> queryCompanyByCompanyCodesForLogin(List<String> companyCodes) {
        SysCompany entity = BeanCreate.newBean(SysCompany.class);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<SysCompany> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        queryWrapper.in("company_code", companyCodes);
        return list(queryWrapper);
    }
}
