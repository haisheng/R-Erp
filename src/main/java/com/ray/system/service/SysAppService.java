package com.ray.system.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.system.table.dto.AppQueryDTO;
import com.ray.system.table.entity.SysApp;
import com.ray.system.table.mapper.SysAppMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 系统应用表 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@Service
public class SysAppService extends ServiceImpl<SysAppMapper, SysApp> {


    /**
     * 编辑应用
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(SysApp entity, LoginUser loginUser) {
        SysApp query = new SysApp();
        query.setAppCode(entity.getAppCode());
        UpdateWrapper<SysApp> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询应用
     *
     * @param appCode   应用编码
     * @param loginUser 当前操作员
     * @return
     */
    public SysApp queryAppByAppCode(String appCode, LoginUser loginUser) {
        SysApp query = new SysApp();
        query.setAppCode(appCode);
        QueryWrapper<SysApp> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<SysApp> page(CommonPage<AppQueryDTO, Page<SysApp>> queryParams, LoginUser loginUser) {
        AppQueryDTO params = queryParams.getEntity();
        SysApp entity = BeanCreate.newBean(SysApp.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<SysApp> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getAppNameLike()), "app_name", params.getAppNameLike());
        }
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getQuery()), "app_name", queryParams.getQuery());
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<SysApp> list(AppQueryDTO queryParams, LoginUser loginUser) {
        SysApp entity = BeanCreate.newBean(SysApp.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<SysApp> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getAppNameLike()), "app_name", queryParams.getAppNameLike());
        return list(queryWrapper);
    }

    /**
     * @param appKey
     * @return
     */
    public SysApp queryAppByAppKey(String appKey) {
        SysApp entity = BeanCreate.newBean(SysApp.class);
        entity.setAppKey(appKey);
        QueryWrapper<SysApp> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        return getOne(queryWrapper);
    }

    /**
     * 查询应用
     *
     * @param appCode 应用编码
     * @return
     */
    public SysApp queryAppByAppCode(String appCode) {
        SysApp query = new SysApp();
        query.setAppCode(appCode);
        QueryWrapper<SysApp> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.notDelete(queryWrapper);
        return getOne(queryWrapper);
    }

}
