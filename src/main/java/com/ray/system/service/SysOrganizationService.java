package com.ray.system.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.common.BeanField;
import com.ray.system.table.dto.OrganizationQueryDTO;
import com.ray.system.table.entity.SysOrganization;
import com.ray.system.table.mapper.SysOrganizationMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@Service
public class SysOrganizationService extends ServiceImpl<SysOrganizationMapper, SysOrganization>  {


    /**
     * 删除与组织/部门关联的部门
     * @param organizationCode
     * @param loginUser
     */
    public Boolean deleteLink(String organizationCode, LoginUser loginUser) {
        SysOrganization entity = new SysOrganization();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity,loginUser);
        SysOrganization query = new SysOrganization();
        UpdateWrapper<SysOrganization> updateWrapper = new UpdateWrapper<>(query);
        updateWrapper.likeRight(BeanField.DbField.DEPT_CODE,organizationCode);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);

    }

    /**
     * 编辑组织
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(SysOrganization entity, LoginUser loginUser) {
        SysOrganization query = new SysOrganization();
        query.setDeptCode(entity.getDeptCode());
        UpdateWrapper<SysOrganization> pdateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(pdateWrapper, loginUser);
        return update(entity, pdateWrapper);
    }

    /**
     * 查询组织
     *
     * @param organizationCode 组织编码
     * @param loginUser    当前操作员
     * @return
     */
    public SysOrganization queryOrganizationByOrganizationCode(String organizationCode, LoginUser loginUser) {
        SysOrganization query = new SysOrganization();
        query.setDeptCode(organizationCode);
        QueryWrapper<SysOrganization> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<SysOrganization> page(CommonPage<OrganizationQueryDTO, Page<SysOrganization>> queryParams, LoginUser loginUser) {
        OrganizationQueryDTO params = queryParams.getEntity();
        SysOrganization entity = BeanCreate.newBean(SysOrganization.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<SysOrganization> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getDeptNameLike()), "dept_name", params.getDeptNameLike());
        }
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<SysOrganization> list(OrganizationQueryDTO queryParams, LoginUser loginUser) {
        SysOrganization entity = BeanCreate.newBean(SysOrganization.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<SysOrganization> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getDeptNameLike()), "dept_name", queryParams.getDeptNameLike());
        return list(queryWrapper);
    }

    /**
     * 通过组织名称查询组织
     * @param organizationName
     * @return
     */
    public SysOrganization queryOrganizationByOrganizationName(String organizationName,String  companyCode) {
        SysOrganization entity = BeanCreate.newBean(SysOrganization.class);
        entity.setDeptName(organizationName);
        entity.setCompanyCode(companyCode);
        QueryWrapper<SysOrganization> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        return  getOne(queryWrapper);
    }

    /**
     * 查询子组织数量
     * @param organizationCode
     * @return
     */
    public Integer countChildOrganization(String organizationCode) {
        SysOrganization entity = BeanCreate.newBean(SysOrganization.class);
        entity.setParentDeptCode(organizationCode );
        QueryWrapper<SysOrganization> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        return count(queryWrapper);
    }

    /**
     * 获取组织详情
     * @param deptCode
     * @return
     */
    public SysOrganization queryOrganizationByLogin(String deptCode) {
        SysOrganization entity = BeanCreate.newBean(SysOrganization.class);
        entity.setDeptCode(deptCode );
        QueryWrapper<SysOrganization> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        return getOne(queryWrapper);
    }
}
