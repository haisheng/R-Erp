package com.ray.system.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ray.common.BeanField;
import com.ray.system.table.entity.SysPosition;
import com.ray.system.table.entity.SysUserCompany;
import com.ray.system.table.mapper.SysUserCompanyMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 用户公司关联表 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@Service
public class SysUserCompanyService extends ServiceImpl<SysUserCompanyMapper, SysUserCompany> {

    /**
     * 删除与公司/部门关联的用户
     *
     * @param companyCode
     * @param loginUser
     */
    public boolean deleteLink(String companyCode, LoginUser loginUser) {
        SysUserCompany entity = new SysUserCompany();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        SysUserCompany query = new SysUserCompany();
        UpdateWrapper<SysUserCompany> updateWrapper = new UpdateWrapper<>(query);
        updateWrapper.likeRight(BeanField.DbField.COMAPNY_CODE, companyCode);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 编辑用户状态
     *
     * @param userCode
     * @param deptCode
     * @param yesOrNoEnum
     * @param loginUser
     * @return
     */
    public boolean updateStatus(String userCode, String deptCode, YesOrNoEnum yesOrNoEnum, LoginUser loginUser) {
        SysUserCompany entity = new SysUserCompany();
        entity.setStatus(yesOrNoEnum.getValue());
        DataOPUtil.editUser(entity, loginUser);
        SysUserCompany query = new SysUserCompany();
        query.setUserCode(userCode);
        query.setDeptCode(deptCode);
        UpdateWrapper<SysUserCompany> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询用户与组织的关系
     *
     * @param userCode
     * @param companyCode
     * @return
     */
    public SysUserCompany queryLink(String userCode, String companyCode) {
        SysUserCompany query = new SysUserCompany();
        query.setUserCode(userCode);
        query.setComapnyCode(companyCode);
        QueryWrapper<SysUserCompany> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.notDelete(queryWrapper);
        return getOne(queryWrapper);
    }

    /**
     * 获取当前人所属的公司
     *
     * @param userCode
     * @return
     */
    public List<SysUserCompany> listByLogin(String userCode) {
        SysUserCompany query = new SysUserCompany();
        query.setUserCode(userCode);
        query.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<SysUserCompany> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.notDelete(queryWrapper);
        return list(queryWrapper);
    }

    /**
     * 获取公司用户关联记录
     * @param userCode
     * @param companyCode
     * @return
     */
    public SysUserCompany getEntityForLogin(String userCode, String companyCode) {
        SysUserCompany query = new SysUserCompany();
        query.setUserCode(userCode);
        query.setComapnyCode(companyCode);
        query.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<SysUserCompany> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.notDelete(queryWrapper);
        return  getOne(queryWrapper);
    }

    /**
     * 获取公司下面的用户
     * @param companyCode
     * @return
     */
    public List<SysUserCompany> queryUserByCompanyCode(String companyCode,String deptCode) {
        SysUserCompany query = new SysUserCompany();
        query.setComapnyCode(companyCode);
        query.setDeptCode(deptCode);
        QueryWrapper<SysUserCompany> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.notDelete(queryWrapper);
        return list(queryWrapper);
    }
}
