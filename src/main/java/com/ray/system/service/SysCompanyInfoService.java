package com.ray.system.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.system.builder.TemplateRecordBuilder;
import com.ray.system.table.entity.SysCompanyInfo;
import com.ray.system.table.mapper.SysCompanyInfoMapper;
import com.ray.template.TemplateRecord;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 公司扩展信息 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@Service
public class SysCompanyInfoService extends ServiceImpl<SysCompanyInfoMapper, SysCompanyInfo> {

    /**
     * 删除公司扩展信息
     *
     * @param companyCode
     * @param loginUser
     * @return
     */
    public Boolean deleteCompanyInfo(String companyCode, LoginUser loginUser) {
        SysCompanyInfo entity = new SysCompanyInfo();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        SysCompanyInfo query = new SysCompanyInfo();
        query.setCompanyCode(companyCode);
        UpdateWrapper<SysCompanyInfo> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 查询公司扩展信息
     *
     * @param companyCode
     * @param loginUser
     * @return
     */
    public List<TemplateRecord> queryCompanyInfoByCompanyCode(String companyCode, LoginUser loginUser) {
        SysCompanyInfo query = new SysCompanyInfo();
        query.setCompanyCode(companyCode);
        QueryWrapper<SysCompanyInfo> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return list(queryWrapper).stream().map(sysCompanyInfo -> {
          return  new TemplateRecordBuilder().append(sysCompanyInfo).bulid();
        }).collect(Collectors.toList());
    }
}
