package com.ray.system.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.system.table.dto.MenuQueryDTO;
import com.ray.system.table.entity.SysMenu;
import com.ray.system.table.mapper.SysMenuMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 系统菜单表 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@Service
public class SysMenuService extends ServiceImpl<SysMenuMapper, SysMenu> {

    @Autowired
    private SysMenuMapper sysMenuMapper;

    /**
     * 编辑菜单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(SysMenu entity, LoginUser loginUser) {
        SysMenu query = new SysMenu();
        query.setMenuCode(entity.getMenuCode());
        UpdateWrapper<SysMenu> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询菜单
     *
     * @param MenuCode  菜单编码
     * @param loginUser 当前操作员
     * @return
     */
    public SysMenu queryMenuByMenuCode(String MenuCode, LoginUser loginUser) {
        SysMenu query = new SysMenu();
        query.setMenuCode(MenuCode);
        QueryWrapper<SysMenu> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 查询菜单
     *
     * @param MenuCode  菜单编码
     * @return
     */
    public SysMenu queryGMenuByMenuCode(String MenuCode) {
        SysMenu query = new SysMenu();
        query.setMenuCode(MenuCode);
        QueryWrapper<SysMenu> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.notDelete(queryWrapper);
        return getOne(queryWrapper);
    }


    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<SysMenu> page(CommonPage<MenuQueryDTO, Page<SysMenu>> queryParams, LoginUser loginUser) {
        MenuQueryDTO params = queryParams.getEntity();
        SysMenu entity = BeanCreate.newBean(SysMenu.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<SysMenu> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getMenuNameLike()), "menu_name", params.getMenuNameLike());
        }
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<SysMenu> list(MenuQueryDTO queryParams, LoginUser loginUser) {
        SysMenu entity = BeanCreate.newBean(SysMenu.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<SysMenu> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getMenuNameLike()), "menu_name", queryParams.getMenuNameLike());
        return list(queryWrapper);
    }

    /**
     * 通过菜单名称查询菜单
     *
     * @param menuName
     * @param appCode
     * @return
     */
    public SysMenu queryMenuByMenuName(String menuName, String appCode) {
        SysMenu entity = BeanCreate.newBean(SysMenu.class);
        entity.setMenuName(menuName);
        entity.setAppCode(appCode);
        QueryWrapper<SysMenu> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        return getOne(queryWrapper);
    }

    /**
     * 查询子菜单的数量
     *
     * @param menuCode
     * @return
     */
    public Integer countChildMenu(String menuCode) {
        SysMenu entity = BeanCreate.newBean(SysMenu.class);
        entity.setParentMenuCode(menuCode);
        QueryWrapper<SysMenu> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        return count(queryWrapper);
    }

    /**
     * 查询app对应的菜单
     *
     * @param appCode
     * @param loginUser
     * @return
     */
    public List<SysMenu> queryMenuByAppCode(String appCode,List<String> menuTypes, LoginUser loginUser) {
        SysMenu entity = BeanCreate.newBean(SysMenu.class);
        entity.setAppCode(appCode);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<SysMenu> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.in("menu_type",menuTypes);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询app对应的菜单
     *
     * @param appCode
     * @param userCode
     * @return
     */
    public List<SysMenu> listByUserCode(String userCode, String appCode, List<String> menuTypes) {
        return sysMenuMapper.listByUserCode(userCode, appCode, menuTypes);
    }
}
