package com.ray.system.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.system.table.dto.UserQueryDTO;
import com.ray.system.table.entity.SysUser;
import com.ray.system.table.mapper.SysUserMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 用户基本信息表 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@Service
public class SysUserService extends ServiceImpl<SysUserMapper, SysUser>  {
    /**
     * 编辑用户
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(SysUser entity, LoginUser loginUser) {
        SysUser query = new SysUser();
        query.setUserCode(entity.getUserCode());
        UpdateWrapper<SysUser> pdateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(pdateWrapper);
        return update(entity, pdateWrapper);
    }

    /**
     * 查询用户
     *
     * @param userCode 用户编码
     * @param loginUser    当前操作员
     * @return
     */
    public SysUser queryUserByUserCode(String userCode, LoginUser loginUser) {
        if(StrUtil.isBlank(userCode)){
            return  null;
        }
        SysUser query = new SysUser();
        query.setUserCode(userCode);
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.notDelete(queryWrapper);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param userCodes
     * @return
     */
    public IPage<SysUser> page(CommonPage<UserQueryDTO, Page<SysUser>> queryParams, Collection<String> userCodes) {
        UserQueryDTO params = queryParams.getEntity();
        SysUser entity = BeanCreate.newBean(SysUser.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getUserNameLike()), "user_name", params.getUserNameLike());
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getMobileLike()), "mobile", params.getMobileLike());
        }
        queryWrapper.in("user_code",userCodes);
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<SysUser> list(UserQueryDTO queryParams, LoginUser loginUser) {
        SysUser entity = BeanCreate.newBean(SysUser.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getUserNameLike()), "user_name", queryParams.getUserNameLike());
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getMobileLike()), "mobile", queryParams.getMobileLike());
        return list(queryWrapper);
    }

    /**
     * 通过手机号码查询
     * @param mobile
     * @return
     */
    public SysUser queryUserByMobile(String mobile) {
        SysUser entity = BeanCreate.newBean(SysUser.class);
        entity.setMobile(mobile);
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        return  getOne(queryWrapper);
    }

    /**
     * 登陆查询用户信息
     * @param userCode
     * @return
     */
    public SysUser queryUserByUserCodeForLogin(String userCode) {
        SysUser entity = BeanCreate.newBean(SysUser.class);
        entity.setUserCode(userCode);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        return  getOne(queryWrapper);
    }

    /**
     * 查询所有
     * @param
     * @return
     */
    public IPage<SysUser> pageAll(CommonPage<UserQueryDTO, Page<SysUser>> queryParams) {
        UserQueryDTO params = queryParams.getEntity();
        SysUser entity = BeanCreate.newBean(SysUser.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getUserNameLike()), "user_name", params.getUserNameLike());
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getMobileLike()), "mobile", params.getMobileLike());
        }
        return page(queryParams.getPage(), queryWrapper);
    }
}
