package com.ray.system.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.system.table.entity.SysLogin;
import com.ray.system.table.mapper.SysLoginMapper;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户登录信息表 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@Service
public class SysLoginService extends ServiceImpl<SysLoginMapper, SysLogin> {
    /**
     * 编辑登录
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(SysLogin entity, LoginUser loginUser) {
        SysLogin query = new SysLogin();
        query.setUserCode(entity.getUserCode());
        DataOPUtil.editUser(entity,loginUser);
        UpdateWrapper<SysLogin> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 获取登录
     * @param loginName
     * @param password
     * @return
     */
    public SysLogin queryByLogin(String loginName, String password) {
        SysLogin query = new SysLogin();
        query.setLoginName(loginName);
        query.setPassword(password);
        query.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<SysLogin> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.notDelete(queryWrapper);
        return getOne(queryWrapper);
    }
}
