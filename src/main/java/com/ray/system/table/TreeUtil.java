
package com.ray.system.table;

import cn.hutool.core.util.StrUtil;
import com.ray.system.table.vo.menu.MenuTreeVO;
import com.ray.system.table.vo.organization.OranizationTreeVO;

import java.util.ArrayList;
import java.util.List;

public class TreeUtil {
    /**
     * 两层循环实现建树
     *
     * @param treeNodes 传入的树节点列表
     * @return
     */
    public static List<MenuTreeVO> bulid(List<MenuTreeVO> treeNodes, String root) {
        List<MenuTreeVO> trees = new ArrayList<MenuTreeVO>();
        for (MenuTreeVO treeNode : treeNodes) {
            if (root.equals(treeNode.getParentMenuCode())) {
                trees.add(treeNode);
            }
            for (MenuTreeVO it : treeNodes) {
                if (StrUtil.equals(it.getParentMenuCode(),treeNode.getMenuCode())) {
                    treeNode.add(it);
                }
            }
        }
        return trees;
    }

    /**
     * 两层循环实现建树
     *
     * @param treeNodes 传入的树节点列表
     * @return
     */
    public static List<OranizationTreeVO> bulidOrg(List<OranizationTreeVO> treeNodes, String root) {
        List<OranizationTreeVO> trees = new ArrayList<OranizationTreeVO>();
        for (OranizationTreeVO treeNode : treeNodes) {
            if (root.equals(treeNode.getParentDeptCode())) {
                trees.add(treeNode);
            }
            for (OranizationTreeVO it : treeNodes) {
                if (StrUtil.equals(it.getParentDeptCode(),treeNode.getDeptCode())) {
                    treeNode.add(it);
                }
            }
        }
        return trees;
    }

}
