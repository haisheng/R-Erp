package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysOpLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户操作日志 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
public interface SysOpLogMapper extends BaseMapper<SysOpLog> {

}
