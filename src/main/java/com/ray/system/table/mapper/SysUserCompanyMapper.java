package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysUserCompany;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户公司关联表 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
public interface SysUserCompanyMapper extends BaseMapper<SysUserCompany> {

}
