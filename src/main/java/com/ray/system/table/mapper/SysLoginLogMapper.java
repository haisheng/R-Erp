package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户登录日志 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {

}
