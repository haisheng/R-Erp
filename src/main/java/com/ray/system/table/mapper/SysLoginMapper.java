package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysLogin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户登录信息表 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
public interface SysLoginMapper extends BaseMapper<SysLogin> {

}
