package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统角色表 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-07-14
 */
public interface SysFileMapper extends BaseMapper<SysFile> {

}
