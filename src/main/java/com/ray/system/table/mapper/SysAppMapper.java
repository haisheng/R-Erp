package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysApp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统应用表 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
public interface SysAppMapper extends BaseMapper<SysApp> {

}
