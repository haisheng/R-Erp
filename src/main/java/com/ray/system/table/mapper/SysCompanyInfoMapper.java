package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysCompanyInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 公司扩展信息 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
public interface SysCompanyInfoMapper extends BaseMapper<SysCompanyInfo> {

}
