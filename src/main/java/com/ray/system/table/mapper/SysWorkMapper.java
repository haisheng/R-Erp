package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysWork;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 我的待办任务 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-07-20
 */
public interface SysWorkMapper extends BaseMapper<SysWork> {

}
