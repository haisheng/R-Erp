package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysCompany;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 公司主表 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
public interface SysCompanyMapper extends BaseMapper<SysCompany> {

}
