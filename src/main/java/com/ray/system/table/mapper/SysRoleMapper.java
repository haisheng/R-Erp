package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统角色表 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 查询用户在系统下拥有的角色
     * @param userCode
     * @param appCode
     * @return
     */
    List<SysRole> listByUserCode(@Param("userCode") String userCode, @Param("appCode") String appCode);
}
