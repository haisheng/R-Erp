package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysUserPosition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户菜单关联 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-27
 */
public interface SysUserPositionMapper extends BaseMapper<SysUserPosition> {

}
