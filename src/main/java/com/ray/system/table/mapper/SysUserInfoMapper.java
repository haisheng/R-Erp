package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysUserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户扩展信息 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
public interface SysUserInfoMapper extends BaseMapper<SysUserInfo> {

}
