package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统菜单表 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 查询用户拥有的菜单
     * @param userCode
     * @param appCode
     * @param menuTypes
     * @return
     */
    List<SysMenu> listByUserCode(@Param("userCode") String userCode,@Param("appCode") String appCode,@Param("menuTypes") List<String> menuTypes);
}
