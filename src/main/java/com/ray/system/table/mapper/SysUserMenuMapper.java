package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysUserMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户菜单关联 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
public interface SysUserMenuMapper extends BaseMapper<SysUserMenu> {

    /**
     * 查询用户是否存在与菜单的关系
     * @param userCode
     * @param menuCode
     * @return
     */
    SysUserMenu queryUserMenu(@Param("userCode") String userCode, @Param("menuCode")String menuCode);
}
