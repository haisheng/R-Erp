package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户基本信息表 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
