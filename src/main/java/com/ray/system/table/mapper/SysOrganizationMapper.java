package com.ray.system.table.mapper;

import com.ray.system.table.entity.SysOrganization;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
public interface SysOrganizationMapper extends BaseMapper<SysOrganization> {

}
