package com.ray.system.table.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 用户查询
 * @Class: UserQueryParams
 * @Package com.ray.system.table.params
 * @date 2020/5/27 8:34
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class UserQueryDTO {

    /*** 用户名称*/
    private String userName;

    /*** 用户名称*/
    private String userNameLike;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 手机号码
     */
    private String mobileLike;

    /*** 邮箱*/
    private String email;

    /**
     * 身份证
     */
    private String idCard;
    /**
     * 状态
     */
    private Integer status;
}
