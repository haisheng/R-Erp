package com.ray.system.table.dto;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 菜单查询
 * @Class: MenuQueryParams
 * @Package com.ray.system.table.params.menu
 * @date 2020/5/28 15:30
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class MenuQueryDTO {

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 菜单名称
     */
    private String menuNameLike;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 应用编码
     */
    private String appCode;
}
