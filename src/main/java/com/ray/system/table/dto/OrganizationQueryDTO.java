package com.ray.system.table.dto;

import lombok.Data;

/**
 * @author bo shen
 * @Description: deptQueryDTO
 * @Class: deptQueryDTO
 * @Package com.ray.system.table.dto
 * @date 2020/5/28 10:06
 * @dept <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class OrganizationQueryDTO {

    /**
     * 部门名称-模糊查询
     */
    private String deptNameLike;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 上级部门
     */
    private String parentDeptCode;

    /**
     * 状态
     */
    private Integer status;
}
