package com.ray.system.table.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: CompanyQueryDTO
 * @Class: CompanyQueryDTO
 * @Package com.ray.system.table.dto
 * @date 2020/5/28 10:06
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class CompanyQueryDTO {

    /**
     * 公司名称-模糊查询
     */
    private String companyNameLike;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 上级公司
     */
    private String parentCompanyCode;

    /**
     * 状态
     */
    private Integer status;
}
