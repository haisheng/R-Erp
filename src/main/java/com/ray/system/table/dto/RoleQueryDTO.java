package com.ray.system.table.dto;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 角色查询
 * @Class: RoleQueryParams
 * @Package com.ray.system.table.params.role
 * @date 2020/5/28 16:28
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class RoleQueryDTO {
    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色名称
     */
    private String roleNameLike;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 系统编码
     */
    private String appCode;
}
