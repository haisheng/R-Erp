package com.ray.system.table.dto;

import com.ray.common.dto.BaseDTO;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 查询对象
 * @Class: PositionQueryDTO
 * @Package com.ray.system.table.dto
 * @date 2020/5/27 17:04
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PositionQueryDTO extends BaseDTO{

    /**
     * 职位名称-模糊查询
     */
    private String positionNameLike;

    /**
     * 职位名称
     */
    private String positionName;

    /**
     * 状态  1:启用 0:禁用
     */
    private Integer useble;
}
