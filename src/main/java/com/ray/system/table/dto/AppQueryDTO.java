package com.ray.system.table.dto;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 应用查询
 * @Class: AppQueryParams
 * @Package com.ray.system.table.dto
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class AppQueryDTO {

    /**
     * 应用名称-模糊查询
     */
    private String appNameLike;

    /**
     * 应用名称
     */
    private String appName;

    /**
     * 应用key
     */
    private String appKey;

    /**
     * 状态
     */
    private Integer status;

}
