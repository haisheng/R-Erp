package com.ray.system.table.dto;

import com.ray.template.Template;
import com.ray.template.TemplateProp;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 用户扩展信息DTO
 * @Class: CompanyInfoDTO
 * @Package com.ray.system.table.dto
 * @date 2020/5/28 11:28
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@Template(code = "user")
public class UserInfoDTO {
    /***地址***/
    @TemplateProp(property = "address", name = "地址")
    private String address;
}
