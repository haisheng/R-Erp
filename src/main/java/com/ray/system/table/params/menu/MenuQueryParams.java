package com.ray.system.table.params.menu;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 菜单查询
 * @Class: MenuQueryParams
 * @Package com.ray.system.table.params.menu
 * @date 2020/5/28 15:30
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("菜单查询")
public class MenuQueryParams  extends BaseParams {

    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称", required = false)
    private String menuName;

    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称-模糊", required = false)
    private String menuNameLike;
}
