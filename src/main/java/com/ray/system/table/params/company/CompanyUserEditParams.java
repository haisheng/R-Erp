package com.ray.system.table.params.company;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 创建公司员工
 * @Class: CompanyUserCreateParams
 * @Package com.ray.system.table.params.company
 * @date 2020/5/29 18:53
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class CompanyUserEditParams {

    /*** 用户编码*/
    @ApiModelProperty(value = "用户编码", required = true)
    @NotBlank(message = "用户编码不能为空")
    @Length(max = 50, message = "用户编码长度不能超过50")
    private String userCode;

    /*** 用户姓名*/
    @ApiModelProperty(value = "用户姓名", required = true)
    @NotBlank(message = "用户姓名不能为空")
    @Length(max = 50, message = "用户姓名长度不能超过50")
    private String userName;

    /*** 公司编码*/
    @ApiModelProperty(value = "公司编码", required = true)
    private String companyCode;


    /** * 手机号码*/
    @ApiModelProperty(value = "手机号码", required = true)
    @NotBlank(message = "手机号码不能为空")
    @Length(min = 11,max = 11, message = "手机号码长度11")
    private String mobile;



    @ApiModelProperty(value = "职位编码", required = true)
    @NotBlank(message = "职位编码不能为空")
    @Length(max = 50, message = "职位编码长度不能超过50")
    private String positionCode;
}
