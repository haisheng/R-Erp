package com.ray.system.table.params.position;

import com.ray.common.BaseParams;
import com.ray.common.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 职位查询
 * @Class: PositionQueryParams
 * @Package com.ray.system.table.params.position
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("职位查询")
@Data
public class PositionQueryParams  extends BaseParams {
    /**
     * 职位名称-模糊查询
     */
    @ApiModelProperty("职位名称-模糊查询")
    private String positionNameLike;
    /**
     * 职位名称
     */
    @ApiModelProperty("职位名称")
    private String positionName;
    /**
     * 是否可见
     */
    @ApiModelProperty("是否可见")
    private Integer useble;

}
