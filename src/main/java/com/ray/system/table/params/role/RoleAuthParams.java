package com.ray.system.table.params.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 角色授权
 * @Class: RoleAuthParams
 * @Package com.ray.system.table.params.role
 * @date 2020/5/28 17:02
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("角色授权")
public class RoleAuthParams {
    /**
     * 角色编码
     */
    @ApiModelProperty(value = "角色编码", required = true)
    @NotBlank(message = "角色编码不能为空")
    @Length(max = 50, message = "角色编码长度不能超过50")
    private String roleCode;


    /**
     * 菜单编码
     */
    @ApiModelProperty(value = "菜单编码", required = true)
    @NotBlank(message = "菜单编码不能为空")
    @Length(max = 50, message = "菜单编码长度不能超过50")
    private String menuCode;
}
