package com.ray.system.table.params.position;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author bo shen
 * @Description: 职位基础
 * @Class: PositionBase
 * @Package com.ray.system.table.params.position
 * @date 2020/5/27 11:10
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PositionBase {

    @ApiModelProperty(value = "职位名称", required = true)
    @NotBlank(message = "职位名称不能为空")
    @Length(max = 50, message = "职位名称长度不能超过50")
    private String positionName;
    @ApiModelProperty(value = "数据权限", required = true)
    @NotBlank(message = "数据权限不能为空")
    @Length(max = 50, message = "数据权限长度不能超过50")
    private String dataAuth;
    @ApiModelProperty(value = "可视权限", required = true)
    @NotNull(message = "可视权限不能为空")
    private Integer useble;
}
