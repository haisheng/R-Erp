package com.ray.system.table.params.organization;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * @author bo shen
 * @Description: 创建部门
 * @Class: OrganizationCreateParams
 * @Package com.ray.system.table.params.company
 * @date 2020/5/27 8:49
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("部门创建对象")
@Data
public class OrganizationCreateParams extends OrganizationBase {
    /*** 上级部门编码*/
    @ApiModelProperty(value = "上级部门编码",required = false)
    @Length(max = 50,message = "上级部门编码长度不能超过50")
    private String parentDeptCode;
}
