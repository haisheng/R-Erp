package com.ray.system.table.params.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 角色编辑
 * @Class: RoleEditParams
 * @Package com.ray.system.table.params.role
 * @date 2020/5/28 16:30
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("角色编辑")
@Data
public class RoleEditParams extends RoleBase{
    /**
     * 角色编码
     */
    @ApiModelProperty(value = "角色编码", required = true)
    @NotBlank(message = "角色编码不能为空")
    @Length(max = 50, message = "角色编码长度不能超过50")
    private String roleCode;
}
