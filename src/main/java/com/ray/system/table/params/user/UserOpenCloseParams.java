package com.ray.system.table.params.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 用户开启关闭
 * @Class: UserOpenCloseParams
 * @Package com.ray.system.table.params.user
 * @date 2020/5/28 14:32
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("用户开启关闭")
public class UserOpenCloseParams {

    @ApiModelProperty(value = "用户编码", required = true)
    @NotBlank(message = "用户编码不能为空")
    @Length(max = 50, message = "用户编码长度不能超过50")
    private String userCode;

    @ApiModelProperty(value = "组织/公司编码", required = true)
    @NotBlank(message = "组织/公司不能为空")
    @Length(max = 50, message = "组织/公司长度不能超过50")
    private String deptCode;
}
