package com.ray.system.table.params.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 登录对象
 * @Class: LoginParams
 * @Package com.ray.system.table.params.base
 * @date 2020/5/29 9:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("登录对象")
public class LoginParams {

    @ApiModelProperty(value = "登录名称", required = true)
    @NotBlank(message = "登录名称不能为空")
    @Length(max = 50, message = "登录名称长度不能超过50")
    private String loginName;
    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = "密码不能为空")
    @Length(max = 50, message = "密码长度不能超过50")
    private String password;
    @ApiModelProperty(value = "图片验证码", required = true)
    @NotBlank(message = "图片验证码不能为空")
    @Length(max = 6, message = "图片验证码长度不能超过6")
    private String imageCode;
    @ApiModelProperty(value = "系统编码", required = true)
    @NotBlank(message = "系统编码不能为空")
    @Length(max = 6, message = "系统编码长度不能超过6")
    private String appKey;
}
