package com.ray.system.table.params.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author bo shen
 * @Description: 角色基础
 * @Class: RoleBase
 * @Package com.ray.system.table.params.role
 * @date 2020/5/28 16:25
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("角色基础")
public class RoleBase {

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称", required = true)
    @NotBlank(message = "角色名称不能为空")
    @Length(max = 50, message = "角色名称长度不能超过50")
    private String roleName;

}
