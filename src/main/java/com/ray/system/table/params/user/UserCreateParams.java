package com.ray.system.table.params.user;

import com.ray.system.table.params.position.PositionBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 用户新增
 * @Class: UserCreateParams
 * @Package com.ray.system.table.params.user
 * @date 2020/5/27 11:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("用户新增对象")
public class UserCreateParams extends UserBase{
    /** * 手机号码*/
    @ApiModelProperty(value = "手机号码", required = true)
    @NotBlank(message = "手机号码不能为空")
    @Length(min = 11,max = 11, message = "手机号码长度11")
    private String mobile;
}
