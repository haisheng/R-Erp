package com.ray.system.table.params.user;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 用户查询
 * @Class: UserQueryParams
 * @Package com.ray.system.table.params
 * @date 2020/5/27 8:34
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("用户查询对象")
public class UserManagerQueryParams  extends BaseParams {

    /*** 用户名称*/
    @ApiModelProperty(value = "用户名称", required = true)
    private String userName;

    /*** 用户名称*/
    @ApiModelProperty(value = "用户名称-模糊", required = true)
    private String userNameLike;

    /** * 手机号码*/
    @ApiModelProperty(value = "手机号码", required = true)
    private String mobile;

    /** * 手机号码*/
    @ApiModelProperty(value = "手机号码-模糊", required = true)
    private String mobileLike;

    /*** 邮箱*/
    @ApiModelProperty(value = "邮箱", required = false)
    private String email;

    /** 身份证 */
    @ApiModelProperty(value = "身份证", required = false)
    private String idCard;

    /** 公司 */
    @NotBlank(message = "公司编码不能为空")
    @ApiModelProperty(value = "公司", required = false)
    private String companyCode;

}
