package com.ray.system.table.params.menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 菜单编辑对象
 * @Class: MenuCreateParams
 * @Package com.ray.system.table.params.menu
 * @date 2020/5/28 15:29
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("菜单编辑")
public class MenuEditParams extends MenuBase{

    /**
     * 菜单编码
     */
    @ApiModelProperty(value = "菜单编码", required = true)
    @NotBlank(message = "菜单编码不能为空")
    @Length(max = 50, message = "菜单编码长度不能超过50")
    private String menuCode;
}
