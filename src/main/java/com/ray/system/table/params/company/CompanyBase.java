package com.ray.system.table.params.company;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 公司基础对象
 * @Class: OrganizationBase
 * @Package com.ray.system.table.params.company
 * @date 2020/5/28 10:01
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel
@Data
public class CompanyBase {
    /*** 公司名称*/
    @ApiModelProperty(value = "公司名称",required = true)
    @NotBlank(message = "公司名称不能为空")
    @Length(max = 50,message = "公司名称长度不能超过50")
    private String companyName;
    /*** 统一社会信用代码*/
    @ApiModelProperty(value = "统一社会信用代码",required = false)
    @Length(max = 50,message = "统一社会信用代码长度不能超过50")
    private String creditCode;
    /*** 手机号码*/
    @ApiModelProperty(value = "手机号码",required = true)
    @NotBlank(message = "手机号码不能为空")
    @Length(min = 11,max = 11,message = "手机号码长度为11位")
    private String mobile;
    /**法人信息**/
    @ApiModelProperty(value = "上级公司编码",required = false)
    @Length(max = 50,message = "上级公司编码长度不能超过50")
    private String legalPerson;
    /***公司地址***/
    @ApiModelProperty(value = "公司地址",required = false)
    @Length(max = 200,message = "公司地址长度不能超过200")
    private String address;
}
