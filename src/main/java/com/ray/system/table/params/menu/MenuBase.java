package com.ray.system.table.params.menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 菜单基础
 * @Class: MenuBase
 * @Package com.ray.system.table.params.menu
 * @date 2020/5/28 15:21
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("菜单基础")
public class MenuBase {


    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称", required = true)
    @NotBlank(message = "菜单名称不能为空")
    @Length(max = 50, message = "菜单名称长度不能超过50")
    private String menuName;

    /**
     * 菜单类型
     */
    @ApiModelProperty(value = "菜单类型", required = true)
    @NotBlank(message = "菜单类型不能为空")
    @Length(max = 50, message = "菜单类型长度不能超过50")
    private String menuType;

    /**
     * 上级菜单
     */
    @ApiModelProperty(value = "上级菜单", required = false)
    @Length(max = 50, message = "上级菜单长度不能超过50")
    private String parentMenuCode;

    /**
     * 菜单路径
     */
    @ApiModelProperty(value = "菜单路径", required = false)
    @Length(max = 50, message = "菜单路径长度不能超过50")
    private String menuUrl;

    /**
     * 权限路径
     */
    @ApiModelProperty(value = "权限路径", required = false)
    @Length(max = 50, message = "权限路径长度不能超过50")
    private String authUrl;

    /**
     * 图标样式
     */
    @ApiModelProperty(value = "图标样式", required = false)
    @Length(max = 50, message = "图标样式长度不能超过50")
    private String icon;

    /**
     * 菜单权限码
     */
    @ApiModelProperty(value = "菜单权限码", required = false)
    @Length(max = 50, message = "菜单权限码长度不能超过50")
    private String permission;

    /**
     * 打开方式
     */
    @ApiModelProperty(value = "打开方式", required = false)
    @Length(max = 50, message = "打开方式长度不能超过50")
    private String openType;

    /**
     * 组件
     */
    @ApiModelProperty(value = "组件", required = false)
    @Length(max = 50, message = "组件长度不能超过50")
    private String component;

    /**
     * 所属应用
     */
    @ApiModelProperty(value = "所属应用", required = true)
    @NotBlank(message = "所属应用不能为空")
    @Length(max = 50, message = "所属应用长度不能超过50")
    private String appCode;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序", required = true)
    private Integer indexSort = 0;
}
