package com.ray.system.table.params.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: app基础
 * @Class: AppBase
 * @Package com.ray.system.table.params.app
 * @date 2020/5/27 11:10
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class AppBase {

    @ApiModelProperty(value = "应用名称", required = true)
    @NotBlank(message = "应用名称不能为空")
    @Length(max = 50, message = "应用名称长度不能超过50")
    private String appName;

    @ApiModelProperty("应用key")
    @NotBlank(message = "应用key不能为空")
    @Length(max = 50, message = "应用key长度不能超过50")
    private String appKey;
}
