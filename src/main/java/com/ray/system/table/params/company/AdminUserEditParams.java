package com.ray.system.table.params.company;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 创建公司员工
 * @Class: CompanyUserCreateParams
 * @Package com.ray.system.table.params.company
 * @date 2020/5/29 18:53
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class AdminUserEditParams {

    /*** 用户编码*/
    @ApiModelProperty(value = "用户编码", required = true)
    @NotBlank(message = "用户编码不能为空")
    @Length(max = 50, message = "用户编码长度不能超过50")
    private String userCode;

    /*** 用户姓名*/
    @ApiModelProperty(value = "用户姓名", required = true)
    @NotBlank(message = "用户姓名不能为空")
    @Length(max = 50, message = "用户姓名长度不能超过50")
    private String userName;


}
