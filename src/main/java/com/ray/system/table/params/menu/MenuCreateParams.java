package com.ray.system.table.params.menu;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 菜单创建对象
 * @Class: MenuCreateParams
 * @Package com.ray.system.table.params.menu
 * @date 2020/5/28 15:29
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("菜单新增")
public class MenuCreateParams extends MenuBase{
}
