package com.ray.system.table.params.work;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author bo shen
 * @Description: 待办查询
 * @Class: WorkQueryParams
 * @Package com.ray.system.table.params.position
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("待办查询")
@Data
public class WorkQueryParams extends BaseParams {
    /**
     * 待办名称-模糊查询
     */
    @ApiModelProperty("待办名称-模糊查询")
    private String workTitleLike;
    /**
     * 待办名称
     */
    @ApiModelProperty("待办名称")
    private String workTitle;


    /**
     * 开始时间
     */
    private LocalDateTime workStart;

    /**
     * 结束时间
     */
    private LocalDateTime workEnd;


}
