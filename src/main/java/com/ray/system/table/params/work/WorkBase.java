package com.ray.system.table.params.work;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author bo shen
 * @Description: 职位基础
 * @Class: WorkBase
 * @Package com.ray.system.table.params.position
 * @date 2020/5/27 11:10
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class WorkBase {

    @ApiModelProperty(value = "待办名称", required = true)
    @NotBlank(message = "待办名称不能为空")
    @Length(max = 50, message = "待办名称长度不能超过50")
    private String workTitle;
    @ApiModelProperty(value = "待办内容", required = true)
    @NotBlank(message = "待办内容不能为空")
    @Length(max = 50, message = "待办内容长度不能超过50")
    private String workContent;
    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间", required = true)
    @NotNull(message = "开始时间不能为空")
    private LocalDateTime workStartTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间", required = true)
    @NotNull(message = "结束时间不能为空")
    private LocalDateTime workEndTime;

}
