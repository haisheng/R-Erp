package com.ray.system.table.params.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author bo shen
 * @Description: 用户基础信息
 * @Class: UserBase
 * @Package com.ray.system.table.params.user
 * @date 2020/5/28 13:38
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("用户基础信息")
public class UserBase {

    /*** 用户名称*/
    @ApiModelProperty(value = "用户名称", required = true)
    @NotBlank(message = "用户名称不能为空")
    @Length(max = 50, message = "用户名称长度不能超过50")
    private String userName;

    /*** 邮箱*/
    @ApiModelProperty(value = "邮箱", required = false)
    @Length(max = 50, message = "邮箱长度不能超过50")
    private String email;


    /*** 性别*/
    @ApiModelProperty(value = "性别", required = false)
    @Length(max = 50, message = "性别长度不能超过50")
    private String sex;

    /** 身份证 */
    @ApiModelProperty(value = "身份证", required = false)
    @Length(max = 50, message = "身份证长度不能超过50")
    private String idCard;

    /*** 生日*/
    @ApiModelProperty(value = "生日", required = false)
    private LocalDate birthday;


    /** 居住地址 */
    @ApiModelProperty(value = "居住地址", required = false)
    @Length(max = 200, message = "居住地址长度不能超过200")
    private String address;

    /** QQ */
    @ApiModelProperty(value = "QQ", required = false)
    @Length(max = 200, message = "QQ长度不能超过50")
    private String qq;

    /** 微信 */
    @ApiModelProperty(value = "微信", required = false)
    @Length(max = 50, message = "微信长度不能超过50")
    private String wx;

    @ApiModelProperty(value = "职位编码", required = true)
    @NotBlank(message = "职位编码不能为空")
    @Length(max = 50, message = "职位编码长度不能超过50")
    private String positionCode;

    /** 组织编码 */
    @ApiModelProperty(value = "组织编码", required = false)
    @Length(max = 200, message = "组织编码长度不能超过50")
    private String deptCode;


}
