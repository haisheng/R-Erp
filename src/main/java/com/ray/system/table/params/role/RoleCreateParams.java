package com.ray.system.table.params.role;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 角色新增
 * @Class: RoleCreateParams
 * @Package com.ray.system.table.params.role
 * @date 2020/5/28 16:30
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("角色新增")
@Data
public class RoleCreateParams extends RoleBase{
}
