package com.ray.system.table.params.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 用户数据权限变更
 * @Class: PositionChangeParams
 * @Package com.ray.system.table.params.user
 * @date 2020/7/7 10:25
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class PositionChangeParams {


    @ApiModelProperty(value = "用户编码", required = true)
    @NotBlank(message = "用户编码不能为空")
    @Length(max = 50, message = "用户编码长度不能超过50")
    private String userCode;

    @ApiModelProperty(value = "职位编码", required = true)
    @NotBlank(message = "职位编码不能为空")
    @Length(max = 50, message = "职位编码长度不能超过50")
    private String positionCode;

}
