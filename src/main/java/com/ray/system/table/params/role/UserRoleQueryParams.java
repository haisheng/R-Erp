package com.ray.system.table.params.role;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 角色查询
 * @Class: RoleQueryParams
 * @Package com.ray.system.table.params.role
 * @date 2020/5/28 16:28
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("角色查询对象")
public class UserRoleQueryParams  extends BaseParams {

    /**
     * 用户编码
     */
    @ApiModelProperty(value = "用户编码", required = true)
    @NotBlank(message = "用户编码不能为空")
    @Length(max = 50, message = "用户编码长度不能超过50")
    private String userCode;
    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称", required = true)
    private String roleName;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称-模糊", required = true)
    private String roleNameLike;
}
