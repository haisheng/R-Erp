package com.ray.system.table.params.organization;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 部门基础对象
 * @Class: OrganizationBase
 * @Package com.ray.system.table.params.dept
 * @date 2020/5/28 10:01
 * @dept <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel
@Data
public class OrganizationBase {
    /*** 部门名称*/
    @ApiModelProperty(value = "部门名称",required = true)
    @NotBlank(message = "部门名称不能为空")
    @Length(max = 50,message = "部门名称长度不能超过50")
    private String deptName;
}
