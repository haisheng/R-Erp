package com.ray.system.table.params.company;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 创建公司
 * @Class: OrganizationCreateParams
 * @Package com.ray.system.table.params.company
 * @date 2020/5/27 8:49
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("公司创建对象")
@Data
public class CompanyCreateParams extends  CompanyBase{

}
