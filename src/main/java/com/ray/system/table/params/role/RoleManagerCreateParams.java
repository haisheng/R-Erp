package com.ray.system.table.params.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author bo shen
 * @Description: 角色新增
 * @Class: RoleCreateParams
 * @Package com.ray.system.table.params.role
 * @date 2020/5/28 16:30
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("角色新增")
@Data
public class RoleManagerCreateParams extends RoleBase{

    /**
     * 所属应用
     */
    @ApiModelProperty(value = "所属应用", required = true)
    @NotBlank(message = "所属应用不能为空")
    @Length(max = 50, message = "所属应用长度不能超过50")
    private String appCode;

    /**
     * 是否删除  1:能 0:不能
     */
    @ApiModelProperty(value = "是否删除", required = true)
    @NotNull(message = "是否删除不能为空")
    private Integer canDelete;
}
