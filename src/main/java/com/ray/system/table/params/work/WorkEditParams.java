package com.ray.system.table.params.work;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 职位编辑
 * @Class: MaterialTypeEditParams
 * @Package com.ray.system.table.params.position
 * @date 2020/5/27 11:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("待办编辑对象")
public class WorkEditParams extends WorkBase {
    @ApiModelProperty(value = "待办编码", required = true)
    @NotBlank(message = "待办编码不能为空")
    @Length(max = 50, message = "待办编码长度不能超过50")
    private String workCode;

}
