package com.ray.system.table.params.company;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 公司查询对象
 * @Class: OrganizationQueryParams
 * @Package com.ray.system.table.params.company
 * @date 2020/5/28 10:03
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("公司查询对象")
public class CompanyQueryParams extends BaseParams{

    @ApiModelProperty("公司名称-模糊查询")
    private String companyNameLike;

    @ApiModelProperty("公司名称")
    private String companyName;

    @ApiModelProperty("上级公司")
    private String parentCompanyCode;

}
