package com.ray.system.table.params.app;

import com.ray.system.table.params.position.PositionBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 应用编辑对象
 * @Class: MaterialTypeEditParams
 * @Package com.ray.system.table.params.app
 * @date 2020/5/27 11:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("应用编辑对象")
public class AppEditParams extends AppBase {
    @ApiModelProperty(value = "应用编码", required = true)
    @NotBlank(message = "应用编码不能为空")
    @Length(max = 50, message = "应用编码长度不能超过50")
    private String appCode;

}
