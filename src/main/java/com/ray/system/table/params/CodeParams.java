package com.ray.system.table.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 编码查询
 * @Class: CodeParams
 * @Package com.ray.system.table.params
 * @date 2020/5/30 16:10
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class CodeParams {
    @ApiModelProperty(value = "编码", required = true)
    @NotBlank(message = "编码不能为空")
    private String code;
}
