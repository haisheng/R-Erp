package com.ray.system.table.params.app;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 应用查询
 * @Class: AppQueryParams
 * @Package com.ray.system.table.params.app
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("应用查询")
@Data
public class AppQueryParams  extends BaseParams {

    @ApiModelProperty("应用名称-模糊查询")
    private String appNameLike;

    @ApiModelProperty("应用名称")
    private String appName;

    @ApiModelProperty("应用key")
    private String appKey;
}
