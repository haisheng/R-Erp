package com.ray.system.table.params.app;

import com.ray.system.table.params.position.PositionBase;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 应用新增对象
 * @Class: AppCreateParams
 * @Package com.ray.system.table.params.app
 * @date 2020/5/27 11:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("应用新增对象")
public class AppCreateParams extends AppBase{
}
