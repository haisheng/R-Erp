package com.ray.system.table.params.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 更换部门
 * @Class: ChangeDeptParams
 * @Package com.ray.system.table.params.base
 * @date 2020/5/29 11:06
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("更换部门参数")
public class ChangeDeptParams {

    @ApiModelProperty(value = "用户编码", required = true)
    @NotBlank(message = "用户编码不能为空")
    @Length(max = 50, message = "用户编码长度不能超过50")
    private String userCode;

    @ApiModelProperty(value = "部门编码", required = true)
    @NotBlank(message = "部门编码不能为空")
    @Length(max = 50, message = "部门编码长度不能超过50")
    private String deptCode;
}
