package com.ray.system.table.params.organization;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 部门查询对象
 * @Class: OrganizationQueryParams
 * @Package com.ray.system.table.params.dept
 * @date 2020/5/28 10:03
 * @dept <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("部门查询对象")
public class OrganizationQueryParams  extends BaseParams {

    @ApiModelProperty("部门名称-模糊查询")
    private String deptNameLike;

    @ApiModelProperty("部门名称")
    private String deptName;

    @ApiModelProperty("上级部门")
    private String parentDeptCode;

}
