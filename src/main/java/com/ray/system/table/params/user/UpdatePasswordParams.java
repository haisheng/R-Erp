package com.ray.system.table.params.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 密码修改
 * @Class: UpdatePasswordParams
 * @Package com.ray.system.table.params.user
 * @date 2020/6/17 8:53
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class UpdatePasswordParams {
    
    @ApiModelProperty(value = "原始密码", required = true)
    @NotBlank(message = "原始密码不能为空")
    @Length(max = 50, message = "原始密码长度不能超过50")
    private String oldPassword;

    @ApiModelProperty(value = "新密码", required = true)
    @NotBlank(message = "新密码不能为空")
    @Length(max = 50, message = "新密码长度不能超过50")
    private String newPassword;
}
