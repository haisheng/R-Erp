package com.ray.system.table.vo.organization;

import cn.hutool.core.util.ObjectUtil;
import com.ray.system.table.vo.menu.MenuVO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class OranizationTreeVO extends OrganizationVO {

    private List<OranizationTreeVO> children;

    private Boolean hasChildren;

    public void add(OranizationTreeVO it) {
        hasChildren = true;
        if (ObjectUtil.isNull(children)) {
            children = new ArrayList<OranizationTreeVO>();
        }
        children.add(it);
    }
}
