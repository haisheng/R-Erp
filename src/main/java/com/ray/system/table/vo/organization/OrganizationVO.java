package com.ray.system.table.vo.organization;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 组织部门对象
 * @Class: OrganizationVO
 * @Package com.ray.system.table.vo.organization
 * @date 2020/5/28 10:00
 * @dept <p>Ray快速开发平台部门</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("组织部门对象")
@Data
public class OrganizationVO extends BaseVO{
    @ApiModelProperty(value = "组织部门编码", required = false)
    private String deptCode;
    /*** 组织部门名称*/
    @ApiModelProperty(value = "组织部门名称",required = false)
    private String deptName;
    /*** 上级组织部门编码*/
    @ApiModelProperty(value = "上级组织部门编码",required = false)
    private String parentDeptCode;
    /*** 上级组织部门编码*/
    @ApiModelProperty(value = "上级组织部门名称",required = false)
    private String parentDeptName;
}
