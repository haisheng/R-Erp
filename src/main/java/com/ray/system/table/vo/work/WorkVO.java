package com.ray.system.table.vo.work;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author bo shen
 * @Description: 职位对象
 * @Class: PositionVO
 * @Package com.ray.system.table.vo.position
 * @date 2020/5/27 11:19
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("待办对象")
@Data
public class WorkVO extends BaseVO {

    /**
     * 待办编码
     */
    private String workCode;

    /**
     * 待办标题
     */
    private String workTitle;

    /**
     * 待办内容
     */
    private String workContent;

    /**
     * 开始时间
     */
    @JSONField(format = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDateTime workStartTime;
    /**
     * 结束时间
     */
    @JSONField(format = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDateTime workEndTime;
}
