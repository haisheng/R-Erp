package com.ray.system.table.vo.menu;

import cn.hutool.core.util.ObjectUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MenuTreeVO extends MenuVO {

    private List<MenuTreeVO> children;

    private Boolean auth;

    private Boolean hasChildren;

    public void add(MenuTreeVO it) {
        hasChildren = true;
        if (ObjectUtil.isNull(children)) {
            children = new ArrayList<MenuTreeVO>();
        }
        children.add(it);
    }
}
