package com.ray.system.table.vo.app;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 职位对象
 * @Class: PositionVO
 * @Package com.ray.system.table.vo.app
 * @date 2020/5/27 11:19
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("应用对象")
@Data
public class AppVO extends BaseVO {

    @ApiModelProperty(value = "应用编码")
    private String appCode;
    @ApiModelProperty(value = "应用名称")
    private String appName;
    @ApiModelProperty(value = "应用key")
    private String appKey;
}
