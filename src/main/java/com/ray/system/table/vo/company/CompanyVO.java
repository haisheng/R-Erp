package com.ray.system.table.vo.company;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 公司对象
 * @Class: CompanyVO
 * @Package com.ray.system.table.vo.company
 * @date 2020/5/28 10:00
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("公司对象")
@Data
public class CompanyVO extends BaseVO{
    @ApiModelProperty(value = "公司编码", required = false)
    private String companyCode;
    /*** 公司名称*/
    @ApiModelProperty(value = "公司名称",required = false)
    private String companyName;
    /*** 上级公司编码*/
    @ApiModelProperty(value = "上级公司编码",required = false)
    private String parentCompanyCode;
    /*** 上级公司编码*/
    @ApiModelProperty(value = "上级公司名称",required = false)
    private String parentCompanyName;
    /*** 统一社会信用代码*/
    @ApiModelProperty(value = "统一社会信用代码",required = false)
    private String creditCode;
    /*** 手机号码*/
    @ApiModelProperty(value = "手机号码",required = false)
    private String mobile;
    /**法人信息**/
    @ApiModelProperty(value = "法人信息",required = false)
    private String legalPerson;
    /***公司地址***/
    @ApiModelProperty(value = "公司地址",required = false)
    private String address;
}
