package com.ray.system.table.vo.menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

/**
 * @author bo shen
 * @Description: 权限
 * @Class: AuthVO
 * @Package com.ray.system.table.vo.menu
 * @date 2020/5/29 13:25
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("资源对象")
@NoArgsConstructor
@AllArgsConstructor
public class AuthVO {
    /***菜单**/
    @ApiModelProperty("菜单")
    private List<MenuTreeVO> menuTreeVOs;
    /***按钮编码***/
    @ApiModelProperty("按钮编码")
    private Set<String> btnCode;
}
