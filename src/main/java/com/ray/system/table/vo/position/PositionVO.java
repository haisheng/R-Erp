package com.ray.system.table.vo.position;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 职位对象
 * @Class: PositionVO
 * @Package com.ray.system.table.vo.position
 * @date 2020/5/27 11:19
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("职位对象")
@Data
public class PositionVO extends BaseVO {

    @ApiModelProperty(value = "职位编码")
    private String positionCode;
    @ApiModelProperty(value = "职位名称")
    private String positionName;
    @ApiModelProperty(value = "数据权限")
    private String dataAuth;
    @ApiModelProperty(value = "非超管是否可见 0 不可见  1:可见")
    private Integer useble;
}
