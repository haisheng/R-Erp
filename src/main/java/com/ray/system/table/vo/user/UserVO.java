package com.ray.system.table.vo.user;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;

/**
 * @author bo shen
 * @Description: 用户结果对象
 * @Class: UserVO
 * @Package com.ray.system.table.vo
 * @date 2020/5/27 8:38
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("用户结果对象")
public class UserVO extends BaseVO {

    /*** 用户编码*/
    @ApiModelProperty(value = "用户编码", required = true)
    private String userCode;

    /*** 用户名称*/
    @ApiModelProperty(value = "用户名称", required = true)
    private String userName;

    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码", required = true)
    private String mobile;

    /*** 邮箱*/
    @ApiModelProperty(value = "邮箱", required = false)
    private String email;


    /*** 性别*/
    @ApiModelProperty(value = "性别", required = false)
    private String sex;

    /**
     * 身份证
     */
    @ApiModelProperty(value = "身份证", required = false)
    private String idCard;

    /*** 生日*/
    @ApiModelProperty(value = "生日", required = false)
    private LocalDate birthday;


    /**
     * 居住地址
     */
    @ApiModelProperty(value = "居住地址", required = false)
    private String address;

    /**
     * QQ
     */
    @ApiModelProperty(value = "QQ", required = false)
    private String qq;

    /**
     * 微信
     */
    @ApiModelProperty(value = "微信", required = false)
    private String wx;
    /**
     * 职位编码
     */
    @ApiModelProperty(value = "职位编码", required = true)
    private String positionCode;
    /**
     * 职位名称
     */
    @ApiModelProperty(value = "职位名称", required = true)
    private String positionName;


    /**
     * 部门编码
     */
    @ApiModelProperty(value = "部门编码", required = true)
    private String deptCode;


    /**
     * 部门名称
     */
    @ApiModelProperty(value = "部门名称", required = true)
    private String deptName;

}
