package com.ray.system.table.vo.role;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 角色信息
 * @Class: RoleVO
 * @Package com.ray.system.table.vo.role
 * @date 2020/5/28 16:31
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("角色对象")
public class RoleVO extends BaseVO{
    /**
     * 角色编码
     */
    @ApiModelProperty(value = "角色编码", required = true)
    private String roleCode;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称", required = true)
    private String roleName;

    /**
     * 所属应用
     */
    @ApiModelProperty(value = "所属应用", required = true)
    private String appCode;

    /**
     * 系统名称
     */
    @ApiModelProperty(value = "系统名称", required = true)
    private String appName;

    /**
     * 是否删除  1:能 0:不能
     */
    @ApiModelProperty(value = "是否删除", required = true)
    private Integer canDelete;


    private Boolean auth;
}
