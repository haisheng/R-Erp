package com.ray.system.table.vo.menu;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 菜单信息
 * @Class: MenuVO
 * @Package com.ray.system.table.vo.menu
 * @date 2020/5/28 15:47
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("菜单对象")
@Data
public class MenuVO extends BaseVO {
    /**
     * 菜单编码
     */
    @ApiModelProperty(value = "菜单编码", required = true)
    private String menuCode;
    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称", required = true)
    private String menuName;

    /**
     * 菜单类型
     */
    @ApiModelProperty(value = "菜单类型", required = true)
    private String menuType;

    /**
     * 上级菜单
     */
    @ApiModelProperty(value = "上级菜单", required = false)
    private String parentMenuCode;

    /**
     * 上级菜单名称
     */
    @ApiModelProperty(value = "上级菜单名称", required = false)
    private String parentMenuName;

    /**
     * 菜单路径
     */
    @ApiModelProperty(value = "菜单路径", required = false)
    private String menuUrl;

    /**
     * 权限路径
     */
    @ApiModelProperty(value = "权限路径", required = false)
    private String authUrl;

    /**
     * 图标样式
     */
    @ApiModelProperty(value = "图标样式", required = false)
    private String icon;

    /**
     * 菜单权限码
     */
    @ApiModelProperty(value = "菜单权限码", required = false)
    private String permission;

    /**
     * 打开方式
     */
    @ApiModelProperty(value = "打开方式", required = false)
    private String openType;

    /**
     * 组件
     */
    @ApiModelProperty(value = "组件", required = false)
    private String component;

    /**
     * 所属应用
     */
    @ApiModelProperty(value = "所属应用", required = true)
    private String appCode;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序", required = true)
    private Integer indexSort = 0;
}
