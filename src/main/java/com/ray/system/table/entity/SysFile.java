package com.ray.system.table.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统角色表
 * </p>
 *
 * @author shenbo
 * @since 2020-07-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_file")
public class SysFile implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 文件编码
     */
    private String fileCode;

    /**
     * 业务单号
     */
    private String businessCode;

    /**
     * 业务类型
     */
    private String businessType;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 文件地址
     */
    private String fileUrl;

    /**
     * 状态  1:启用 0:禁用
     */
    private Integer status;

    /**
     * 是否删除 1:删除 0:未删除
     */
    @TableLogic
    private Integer isDeleted;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建人编码
     */
    private String createUserCode;

    /**
     * 创建人名称
     */
    private String createUserName;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新用户编码
     */
    private String updateUserCode;

    /**
     * 更新用户名称
     */
    private String updateUserName;

    /**
     * 租户编码
     */
    private String tenantCode;

    /**
     * 用户编码
     */
    private String ownerCode;

    /**
     * 组织编码
     */
    private String organizationCode;

    /**
     * 排序
     */
    private Integer indexSort;

    /**
     * 版本号
     */
    @Version
    private Integer updateVersion;


}
