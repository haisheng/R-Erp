package com.ray.system.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.common.SysMsgCodeConstant;
import com.ray.system.builder.CommonPageBuilder;
import com.ray.system.builder.WorkBuilder;
import com.ray.system.check.WorkCheck;
import com.ray.system.service.SysWorkService;
import com.ray.system.table.dto.WorkQueryDTO;
import com.ray.system.table.entity.SysWork;
import com.ray.system.table.params.work.WorkCreateParams;
import com.ray.system.table.params.work.WorkEditParams;
import com.ray.system.table.params.work.WorkQueryParams;
import com.ray.system.table.vo.work.WorkVO;
import com.ray.validate.support.utils.ValidateUtil;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.Assert;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 待办相关服务
 * @Class: WorkApi
 * @Package com.ray.system.api
 * @date 2020/5/27 11:15
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class WorkApi {

    @Autowired
    private SysWorkService sysWorkService;


    /**
     * 查询待办列表信息 分页  非删除的
     * @param queryParams
     * @return
     */
    public Result<IPage<WorkVO>> pageWorks(CommonPage<WorkQueryParams,Page<WorkVO>> queryParams){
        Assert.notNull(queryParams, SysMsgCodeConstant.Error.ERR10000001);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        CommonPageBuilder<WorkQueryDTO,SysWork> commonPageBuilder = new CommonPageBuilder<>(WorkQueryDTO.class);
        commonPageBuilder.appendEntity(queryParams.getEntity()).appendQuery(queryParams.getQuery()).appendPage(queryParams.getPage());
        IPage<SysWork> page = sysWorkService.page(commonPageBuilder.bulid(),loginUser);
        List<SysWork> positions = page.getRecords();
        //结果对象
        IPage<WorkVO> pageList = new Page<>();
        pageList.setTotal(page.getTotal());
        pageList.setCurrent(page.getCurrent());
        pageList.setSize(page.getSize());
        //查询到结果 数据转换
        if(ObjectUtil.isNotNull(positions)){
            pageList.setRecords(positions.stream().map(sysWork -> {
                WorkVO positionVO = new WorkVO();
                BeanUtil.copyProperties(sysWork,positionVO);
                return  positionVO;
            }).collect(Collectors.toList()));
        }else {
            pageList.setRecords(new ArrayList<>());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002,pageList);
    }

    /**
     * 查询待办列表信息--启用的待办
     * @param queryParams
     * @return
     */
    public Result<List<WorkVO>> queryWorks(WorkQueryParams queryParams){
        Assert.notNull(queryParams, SysMsgCodeConstant.Error.ERR10000001);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        WorkQueryDTO queryDTO = new WorkQueryDTO();
        BeanUtil.copyProperties(queryParams,queryDTO);
        queryDTO.setStatus(YesOrNoEnum.YES.getValue());
        List<SysWork> positions = sysWorkService.list(queryDTO,loginUser);
        //查询对象
        List<WorkVO> list = new ArrayList<>();
        if(ObjectUtil.isNotNull(positions)){
            list = positions.stream().map(sysWork -> {
                WorkVO positionVO = new WorkVO();
                BeanUtil.copyProperties(sysWork,positionVO);
                return  positionVO;
            }).collect(Collectors.toList());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002,list);
    }

  



    /**
     * 创建待办
     * @param createParams 创建对象
     * @return Result
     */
    @Transactional
    public Result<String> createWork(WorkCreateParams createParams) {
        ValidateUtil.validate(createParams);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        WorkBuilder positionBuilder = new WorkBuilder();
        positionBuilder.append(createParams).appendStatus(YesOrNoEnum.YES.getValue()).appendCreate(loginUser);
        //保存待办信息
        if(!sysWorkService.save(positionBuilder.bulid())){
            log.info("保存待办接口异常,参数:{}", JSON.toJSONString(positionBuilder.bulid()));
            throw BusinessExceptionFactory.newException("保存待办接口异常");
        }
       return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001,positionBuilder.getCode());
    }

    /**
     * 编辑待办
     * @param editParams 编辑对象
     * @return Result
     */
    @Transactional
    public Result<String> editWork(WorkEditParams editParams) {
        ValidateUtil.validate(editParams);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        WorkBuilder positionBuilder = new WorkBuilder();
        positionBuilder.append(editParams).appendEdit(loginUser);
        //编辑待办信息
        if(!sysWorkService.edit(positionBuilder.bulid(),loginUser)){
            log.info("编辑待办接口异常,参数:{}", JSON.toJSONString(positionBuilder.bulid()));
            throw BusinessExceptionFactory.newException("编辑待办接口异常");
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001,positionBuilder.getCode());
    }


    /**
     * 删除待办
     * @param workCode 待办编码
     * @return Result
     */
    @Transactional
    public Result<String> deleteWork(String workCode) {
        ValidateUtil.hasLength(workCode,"参数[workCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        SysWork sysWork = sysWorkService.queryWorkByWorkCode(workCode,loginUser);
        new WorkCheck(sysWork).checkNull(SysMsgCodeConstant.Error.ERR10000002);
        WorkBuilder positionBuilder = new WorkBuilder();
        positionBuilder.appendCode(workCode).appendEdit(loginUser).delete();
        //删除待办信息
        if(!sysWorkService.edit(positionBuilder.bulid(),loginUser)){
            log.info("删除待办接口异常,参数:{}", JSON.toJSONString(positionBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001,positionBuilder.getCode());
    }

    /**
     * 开启待办
     * @param workCode 待办编码
     * @return Result
     */
    @Transactional
    public Result<String> openWork(String workCode) {
        ValidateUtil.hasLength(workCode,"参数[workCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        SysWork sysWork = sysWorkService.queryWorkByWorkCode(workCode,loginUser);
        new WorkCheck(sysWork).checkNull(SysMsgCodeConstant.Error.ERR10000002);
        WorkBuilder positionBuilder = new WorkBuilder();
        positionBuilder.appendCode(workCode).appendEdit(loginUser).open();
        //开启待办信息
        if(!sysWorkService.edit(positionBuilder.bulid(),loginUser)){
            log.info("开启待办接口异常,参数:{}", JSON.toJSONString(positionBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001,positionBuilder.getCode());
    }

    /**
     * 开启待办
     * @param workCode 待办编码
     * @return Result
     */
    @Transactional
    public Result<String> closeWork(String workCode) {
        ValidateUtil.hasLength(workCode,"参数[workCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        SysWork sysWork = sysWorkService.queryWorkByWorkCode(workCode,loginUser);
        new WorkCheck(sysWork).checkNull(SysMsgCodeConstant.Error.ERR10000002);
        WorkBuilder positionBuilder = new WorkBuilder();
        positionBuilder.appendCode(workCode).appendEdit(loginUser).close();
        //关闭待办信息
        if(!sysWorkService.edit(positionBuilder.bulid(),loginUser)){
            log.info("关闭待办接口异常,参数:{}", JSON.toJSONString(positionBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001,positionBuilder.getCode());
    }

    /**
     * 待办详情
     * @param workCode 待办编码
     * @return Result
     */
    public Result<WorkVO> viewWork(String workCode) {
        ValidateUtil.hasLength(workCode,"参数[workCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        SysWork sysWork = sysWorkService.queryWorkByWorkCode(workCode,loginUser);
        new WorkCheck(sysWork).checkNull(SysMsgCodeConstant.Error.ERR10000002);
        WorkVO positionVO = new WorkVO();
        BeanUtil.copyProperties(sysWork,positionVO);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002,positionVO);
    }

}
