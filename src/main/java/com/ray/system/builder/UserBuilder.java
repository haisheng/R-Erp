package com.ray.system.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysUser;
import com.ray.system.table.entity.SysUser;
import com.ray.system.table.params.company.AdminUserEditParams;
import com.ray.system.table.params.company.CompanyUserCreateParams;
import com.ray.system.table.params.company.CompanyUserEditParams;
import com.ray.system.table.params.user.MyEditParams;
import com.ray.system.table.params.user.UserCreateParams;
import com.ray.system.table.params.user.UserEditParams;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: User 构造器
 * @Class: UserBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @User <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class UserBuilder extends AbstractBuilder<SysUser, UserBuilder> {

    public UserBuilder() {
        super(new SysUser());
        super.setBuilder(this);
        entity.setUserCode(CodeCreateUtil.getCode(Perifx.USER_CODE));
        log.info("获得用户编码:{}", entity.getUserCode());
    }

    @Override
    public SysUser bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public UserBuilder append(UserCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public UserBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public UserBuilder appendCode(String code) {
        entity.setUserCode(code);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public UserBuilder append(UserEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getUserCode();
    }


    public UserBuilder append(CompanyUserCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    public UserBuilder append(CompanyUserEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            entity.setUserCode(editParams.getUserCode());
            entity.setUserName(editParams.getUserName());
        }
        return this;
    }

    public UserBuilder append(AdminUserEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            entity.setUserCode(editParams.getUserCode());
            entity.setUserName(editParams.getUserName());
        }
        return this;
    }

    public UserBuilder append(MyEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }
}
