package com.ray.system.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysOrganization;
import com.ray.system.table.params.organization.OrganizationCreateParams;
import com.ray.system.table.params.organization.OrganizationEditParams;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Organization 构造器
 * @Class: OrganizationBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @Organization <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class OrganizationBuilder extends AbstractBuilder<SysOrganization, OrganizationBuilder> {

    public OrganizationBuilder() {
        super(new SysOrganization());
        super.setBuilder(this);
    }

    @Override
    public SysOrganization bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public OrganizationBuilder append(OrganizationCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams,entity);
            entity.setDeptCode(CodeCreateUtil.nextChildCode(createParams.getParentDeptCode()));
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public OrganizationBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public OrganizationBuilder appendCode(String code) {
        entity.setDeptCode(code);
        return this;
    }

    /**
     * 添加公司编码
     *
     * @return
     */
    public OrganizationBuilder appendCompanyCode(String companyCode) {
        entity.setCompanyCode(companyCode);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public OrganizationBuilder append(OrganizationEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getOrganizationCode();
    }


}
