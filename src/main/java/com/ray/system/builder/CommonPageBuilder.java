package com.ray.system.builder;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.TypeUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.common.BeanField;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.params.position.PositionQueryParams;
import com.ray.system.table.vo.position.PositionVO;
import com.ray.util.StringUtil;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.page.CommonPage;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author bo shen
 * @Description: CommonPageBuilder
 * @Class: CommonPageBuilder
 * @Package com.ray.woodencreate.builder
 * @date 2020/5/27 17:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class CommonPageBuilder<P,V> extends AbstractBuilder<CommonPage<P,Page<V>>,CommonPageBuilder> {
    /***第一个泛型类型**/
   private Class<P> pClass;


    public CommonPageBuilder(Class<P> pClass) {
        super(new CommonPage<>());
        super.setBuilder(this);
       this.pClass = pClass;
    }


    @Override
    public CommonPage<P, Page<V>> bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 添加参数
     * @param queryParams
     * @return
     */
    public CommonPageBuilder appendEntity(Object queryParams) {
        if(ObjectUtil.isNotNull(queryParams)){
            StringUtil.emptyToNull(queryParams);
            P p = BeanCreate.newBean(pClass);
            BeanUtil.copyProperties(queryParams,p);
            entity.setEntity(p);
        }
        return this;
    }

    /**
     * 查询
     * @param query
     * @return
     */
    public CommonPageBuilder appendQuery(String query) {
        entity.setQuery(query);
        return this;
    }

    /**
     * 分页信息
     * @param page
     * @return
     */
    public CommonPageBuilder appendPage(Page<Object> page) {
        if(ObjectUtil.isNotNull(page)){
            Page<V> vPage = new Page<>();
            BeanUtil.copyProperties(page,vPage);
            entity.setPage(vPage);
        }
        return this;
    }

    public CommonPageBuilder appendCode(String appCode) {
        P p = BeanCreate.newBean(pClass);
        if(ObjectUtil.isNotNull(entity.getEntity())){
            p = entity.getEntity();
        }
        BeanUtil.setFieldValue(p, BeanField.appCode, appCode);
        entity.setEntity(p);
        return this;
    }

    public CommonPageBuilder appendParentCompanyCode(String companyCode) {
        P p = BeanCreate.newBean(pClass);
        if(ObjectUtil.isNotNull(entity.getEntity())){
            p = entity.getEntity();
        }
        BeanUtil.setFieldValue(p, BeanField.parentCompanyCode, companyCode);
        entity.setEntity(p);
        return this;
    }
}
