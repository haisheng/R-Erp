package com.ray.system.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysApp;
import com.ray.system.table.entity.SysPosition;
import com.ray.system.table.params.app.AppCreateParams;
import com.ray.system.table.params.app.AppEditParams;
import com.ray.system.table.params.position.PositionCreateParams;
import com.ray.system.table.params.position.PositionEditParams;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: App 构造器
 * @Class: AppBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class AppBuilder extends AbstractBuilder<SysApp, AppBuilder> {

    public AppBuilder() {
        super(new SysApp());
        super.setBuilder(this);
        entity.setAppCode(CodeCreateUtil.getCode(Perifx.APP_CODE));
        log.info("获得应用编码:{}", entity.getAppCode());
    }

    @Override
    public SysApp bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public AppBuilder append(AppCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public AppBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public AppBuilder appendCode(String code) {
        entity.setAppCode(code);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public AppBuilder append(AppEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getAppCode();
    }


}
