package com.ray.system.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysLogin;
import com.ray.system.table.params.user.UpdatePasswordParams;
import com.ray.woodencreate.util.Md5Util;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

/**
 * @author bo shen
 * @Description: LoginBuilder 构造器
 * @Class: LoginBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @User <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class LoginBuilder extends AbstractBuilder<SysLogin, LoginBuilder> {

    public LoginBuilder() {
        super(new SysLogin());
        super.setBuilder(this);
    }

    @Override
    public SysLogin bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    /**
     * 添加状态
     *
     * @return
     */
    public LoginBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public LoginBuilder appendCode(String code) {
        entity.setUserCode(code);
        return this;
    }


    /**
     * 添加登录名称
     *
     * @return
     */
    public LoginBuilder appendLoginName(String name) {
        entity.setLoginName(name);
        entity.setPassword(Md5Util.getMd5(name.substring(name.length() - 6)));
        return this;
    }

    /**
     * @return
     */
    public LoginBuilder reset(String mobile) {
        entity.setPassword(Md5Util.getMd5(mobile.substring(mobile.length() - 6)));
        return this;
    }

    public LoginBuilder appendSessionKey(String sessionKey) {
        entity.setSessionKey(sessionKey);
        entity.setLoginTime(LocalDateTime.now());
        return this;
    }

    /**
     * 设置默认登陆公司
     * @param companyCode
     * @return
     */
    public LoginBuilder appendComapnyCode(String companyCode) {
        entity.setCompanyCode(companyCode);
        return this;
    }

    @Override
    public String getCode() {
        return entity.getUserCode();
    }


    public LoginBuilder append( UpdatePasswordParams updatePasswordParams) {
        if(ObjectUtil.isNotNull(updatePasswordParams)){
            entity.setPassword(updatePasswordParams.getNewPassword());
        }
        return this;
    }
}
