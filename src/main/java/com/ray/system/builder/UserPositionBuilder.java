package com.ray.system.builder;


import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysUserPosition;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: App 构造器
 * @Class: AppBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class UserPositionBuilder extends AbstractBuilder<SysUserPosition, UserPositionBuilder> {

    public UserPositionBuilder() {
        super(new SysUserPosition());
        super.setBuilder(this);
    }

    @Override
    public SysUserPosition bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }



    /**
     * 添加编码
     *
     * @return
     */
    public UserPositionBuilder appendCode(String code) {
        entity.setUserCode(code);
        return this;
    }
    /**
     * 添加编码
     *
     * @return
     */
    public UserPositionBuilder appendCompanyCode(String companyCode) {
        entity.setCompanyCode(companyCode);
        return this;
    }
    /**
     * 添加编码
     *
     * @return
     */
    public UserPositionBuilder appendPositionCode(String positionCode) {
        entity.setPositionCode(positionCode);
        return this;
    }

}
