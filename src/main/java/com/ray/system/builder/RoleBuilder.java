package com.ray.system.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysRole;
import com.ray.system.table.params.role.RoleCreateParams;
import com.ray.system.table.params.role.RoleEditParams;
import com.ray.system.table.params.role.RoleManagerCreateParams;
import com.ray.system.table.params.role.RoleManagerEditParams;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Role 构造器
 * @Class: RoleBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class RoleBuilder extends AbstractBuilder<SysRole,RoleBuilder> {

    public RoleBuilder() {
        super(new SysRole());
        super.setBuilder(this);
        entity.setRoleCode(CodeCreateUtil.getCode(Perifx.ROLE_CODE));
        log.info("获得角色编码:{}", entity.getRoleCode());
    }

    @Override
    public SysRole bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public RoleBuilder append(RoleCreateParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public RoleBuilder append(RoleManagerCreateParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    /**
     *  添加状态
     * @return
     */
    public RoleBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }
    /**
     *  添加编码
     * @return
     */
    public RoleBuilder appendCode(String code) {
        entity.setRoleCode(code);
        return this;
    }

    /**
     *  添加编码
     * @return
     */
    public RoleBuilder appendAppCode(String appCode) {
        entity.setAppCode(appCode);
        return this;
    }
    /**
     * 构建创建信息
     * @param editParams
     * @return
     */
    public RoleBuilder append(RoleEditParams editParams) {
        if(ObjectUtil.isNotNull(editParams)){
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    /**
     * 构建创建信息
     * @param editParams
     * @return
     */
    public RoleBuilder append(RoleManagerEditParams editParams) {
        if(ObjectUtil.isNotNull(editParams)){
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getRoleCode();
    }



}
