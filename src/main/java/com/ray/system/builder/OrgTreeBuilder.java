package com.ray.system.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysOrganization;
import com.ray.system.table.vo.organization.OranizationTreeVO;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Menu 构造器
 * @Class: MenuBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class OrgTreeBuilder extends AbstractBuilder<OranizationTreeVO, OrgTreeBuilder> {

    public OrgTreeBuilder() {
        super(new OranizationTreeVO());
        super.setBuilder(this);

    }

    @Override
    public OranizationTreeVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public OrgTreeBuilder append(SysOrganization sysOrganization) {
        if(ObjectUtil.isNotNull(sysOrganization)){
            BeanUtil.copyProperties(sysOrganization,entity);
        }
        return this;
    }

}
