package com.ray.system.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.enums.MenuTypeEnum;
import com.ray.system.table.entity.SysMenu;
import com.ray.system.table.params.menu.MenuCreateParams;
import com.ray.system.table.params.menu.MenuEditParams;
import com.ray.util.CodeCreateUtil;
import com.ray.woodencreate.enums.DeleteEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Menu 构造器
 * @Class: MenuBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class MenuBuilder extends AbstractBuilder<SysMenu,MenuBuilder> {

    public MenuBuilder() {
        super(new SysMenu());
        super.setBuilder(this);
        entity.setMenuCode(CodeCreateUtil.getCode(Perifx.MENU_CODE));
        log.info("获得菜单编码:{}", entity.getMenuCode());
        entity.setIsDeleted(DeleteEnum.USE.getValue());
    }

    @Override
    public SysMenu bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public MenuBuilder append(MenuCreateParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    /**
     *  添加状态
     * @return
     */
    public MenuBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }
    /**
     *  添加编码
     * @return
     */
    public MenuBuilder appendCode(String code) {
        entity.setMenuCode(code);
        return this;
    }
    /**
     * 构建创建信息
     * @param editParams
     * @return
     */
    public MenuBuilder append(MenuEditParams editParams) {
        if(ObjectUtil.isNotNull(editParams)){
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getMenuCode();
    }

    public MenuBuilder appendName(String menuName) {
        entity.setMenuName(menuName);
        return this;
    }

    public MenuBuilder appendPermission(String permission) {
        entity.setPermission(permission);
        return this;
    }

    public MenuBuilder appendParentMenuCode(String parentMenuCode) {
        entity.setParentMenuCode(parentMenuCode);
        return this;
    }

    public MenuBuilder appendAppCode(String appCode) {
        entity.setAppCode(appCode);
        entity.setMenuType(MenuTypeEnum.BTN.getValue());
        return this;
    }
}
