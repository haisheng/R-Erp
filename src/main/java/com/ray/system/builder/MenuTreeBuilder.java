package com.ray.system.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysMenu;
import com.ray.system.table.params.menu.MenuCreateParams;
import com.ray.system.table.params.menu.MenuEditParams;
import com.ray.system.table.vo.menu.MenuTreeVO;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Menu 构造器
 * @Class: MenuBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class MenuTreeBuilder extends AbstractBuilder<MenuTreeVO, MenuTreeBuilder> {

    public MenuTreeBuilder() {
        super(new MenuTreeVO());
        super.setBuilder(this);

    }

    @Override
    public MenuTreeVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    public MenuTreeBuilder append(SysMenu sysMenu) {
        if(ObjectUtil.isNotNull(sysMenu)){
            BeanUtil.copyProperties(sysMenu,entity);
        }
        return this;
    }

    public MenuTreeBuilder appendAuth(boolean auth) {
        entity.setAuth(auth);
        return this;
    }
}
