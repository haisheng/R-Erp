package com.ray.system.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysUserRole;
import com.ray.system.table.params.role.RoleAuthParams;
import com.ray.system.table.params.user.UserAuthParams;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: SysUserRole 构造器
 * @Class: UserRoleBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class UserRoleBuilder extends AbstractBuilder<SysUserRole, UserRoleBuilder> {

    public UserRoleBuilder() {
        super(new SysUserRole());
        super.setBuilder(this);
    }

    @Override
    public SysUserRole bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }



    /**
     * 添加编码
     *
     * @return
     */
    public UserRoleBuilder appendUserCode(String code) {
        entity.setUserCode(code);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public UserRoleBuilder appendRoleCode(String code) {
        entity.setRoleCode(code);
        return this;
    }

    /**
     *
     * @param authParams
     * @return
     */
    public UserRoleBuilder append(UserAuthParams authParams) {
        if(ObjectUtil.isNotNull(authParams)){
            entity.setUserCode(authParams.getUserCode());
            entity.setRoleCode(authParams.getRoleCode());
        }
        return this;
    }
}
