package com.ray.system.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysCompany;
import com.ray.system.table.params.company.CompanyCreateParams;
import com.ray.system.table.params.company.CompanyEditParams;
import com.ray.util.CodeCreateUtil;
import com.ray.woodencreate.beans.CommonValue;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Company 构造器
 * @Class: MaterialBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class CompanyBuilder extends AbstractBuilder<SysCompany, CompanyBuilder> {

    public CompanyBuilder() {
        super(new SysCompany());
        super.setBuilder(this);
        entity.setCompanyCode(CodeCreateUtil.getCode(Perifx.COMPANY_CODE));
        entity.setParentCompanyCode("0");
        log.info("获得公司编码:{}", entity.getCompanyCode());
    }

    @Override
    public SysCompany bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public CompanyBuilder append(CompanyCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams,entity);
            //当前存在上级公司编码  本身公司编码需要规则生成
            if(StrUtil.isNotBlank(entity.getParentCompanyCode()) && !StrUtil.equals(CommonValue.DEFAULT_ZERO,entity.getParentCompanyCode())){
               entity.setCompanyCode(CodeCreateUtil.nextCode(entity.getParentCompanyCode()));
            }
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public CompanyBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public CompanyBuilder appendCode(String code) {
        entity.setCompanyCode(code);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public CompanyBuilder append(CompanyEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getCompanyCode();
    }


    public CompanyBuilder appendParentCompanyCode(String companyCode) {
        entity.setParentCompanyCode(companyCode);
        return this;
    }
}
