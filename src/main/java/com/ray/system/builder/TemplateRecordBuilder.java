package com.ray.system.builder;

import cn.hutool.core.util.ObjectUtil;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysCompanyInfo;
import com.ray.system.table.entity.SysUserInfo;
import com.ray.template.TemplateRecord;

/**
 * @author bo shen
 * @Description: TemplateRecord
 * @Class: TemplateRecordBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/28 14:22
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class TemplateRecordBuilder extends AbstractBuilder<TemplateRecord,TemplateRecordBuilder>{

    public TemplateRecordBuilder() {
        super(new TemplateRecord());
        super.setBuilder(this);
    }

    @Override
    public void setBuilder(TemplateRecordBuilder builder) {
        super.setBuilder(builder);
    }

    @Override
    public TemplateRecord bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建
     * @param sysCompanyInfo
     * @return
     */
    public TemplateRecordBuilder append(SysCompanyInfo sysCompanyInfo) {
         if(ObjectUtil.isNotNull(sysCompanyInfo)){
             entity.setPropertyValue(sysCompanyInfo.getValue());
             entity.setProperty(sysCompanyInfo.getProp());
             entity.setPropertyName(sysCompanyInfo.getName());
         }
        return this;
    }

    /**
     * 构建
     * @param sysUserInfo
     * @return
     */
    public TemplateRecordBuilder append(SysUserInfo sysUserInfo) {
        if(ObjectUtil.isNotNull(sysUserInfo)){
            entity.setPropertyValue(sysUserInfo.getValue());
            entity.setProperty(sysUserInfo.getProp());
            entity.setPropertyName(sysUserInfo.getName());
        }
        return this;
    }
}
