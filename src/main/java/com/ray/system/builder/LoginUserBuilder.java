package com.ray.system.builder;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.*;
import com.ray.system.table.vo.company.CompanyVO;
import com.ray.woodencreate.beans.LoginUser;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 登录用户构建
 * @Class: LoginUserBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/29 9:45
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class LoginUserBuilder extends AbstractBuilder<LoginUser, LoginUserBuilder> {

    public LoginUserBuilder() {
        super(new LoginUser());
        super.setBuilder(this);
    }

    @Override
    public void setBuilder(LoginUserBuilder builder) {
        super.setBuilder(builder);
    }

    @Override
    public LoginUser bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 用户信息
     *
     * @param sysUser
     * @return
     */
    public LoginUserBuilder append(SysUser sysUser) {
        if (ObjectUtil.isNotNull(sysUser)) {
            entity.setUserName(sysUser.getUserName());
            entity.setUserCode(sysUser.getUserCode());
            entity.setMoblie(sysUser.getMobile());
        }
        return this;
    }

    public LoginUserBuilder appnedSessionKey(String sessionKey) {
        entity.setToken(sessionKey);
        return this;
    }

    /**
     * 公司信息
     *
     * @param sysCompany
     * @return
     */
    public LoginUserBuilder append(SysCompany sysCompany) {
        if (ObjectUtil.isNotNull(sysCompany)) {
            entity.setCompanyCode(sysCompany.getCompanyCode());
            entity.setCompanyName(sysCompany.getCompanyName());
        }
        return this;
    }

    /**
     * 职位信息
     *
     * @param sysPosition
     * @return
     */
    public LoginUserBuilder append(SysPosition sysPosition) {
        if (ObjectUtil.isNotNull(sysPosition)) {
            entity.setPositionCode(sysPosition.getPositionCode());
            entity.setPositionName(sysPosition.getPositionName());
            entity.setDataAuth(sysPosition.getDataAuth());
        }
        return this;
    }

    /**
     * 部门信息
     *
     * @param sysOrganization
     * @return
     */
    public LoginUserBuilder appendDept(SysOrganization sysOrganization) {
        if (ObjectUtil.isNotNull(sysOrganization)) {
            entity.setDeptCode(sysOrganization.getDeptCode());
            entity.setDeptName(sysOrganization.getDeptName());
        }
        return this;
    }

    /**
     * 部门信息
     *
     * @param sysCompany
     * @return
     */
    public LoginUserBuilder appendDept(SysCompany sysCompany) {
        if (ObjectUtil.isNotNull(sysCompany)) {
            entity.setDeptCode(sysCompany.getCompanyCode());
            entity.setDeptName(sysCompany.getCompanyName());
        }
        return this;
    }

    public LoginUserBuilder append(LoginUser loginUser) {
        if (ObjectUtil.isNotNull(loginUser)) {
            BeanUtil.copyProperties(loginUser, entity);
        }
        return this;
    }

    public LoginUserBuilder append(List<SysCompany> sysCompanies) {
        if (ObjectUtil.isNotEmpty(sysCompanies)) {
            List<CompanyVO> companyVOS = sysCompanies.stream().map(sysCompany -> {
                CompanyVO companyVO = new CompanyVO();
                BeanUtil.copyProperties(sysCompany, companyVO);
                return companyVO;
            }).collect(Collectors.toList());
            entity.setCompanys(companyVOS);

        }
        return this;
    }

    public LoginUserBuilder append(SysApp sysApp) {
        if (ObjectUtil.isNotNull(sysApp)) {
            entity.setAppCode(sysApp.getAppCode());
            entity.setAppKey(sysApp.getAppKey());
            entity.setAppName(sysApp.getAppName());
        }
        return this;
    }
}
