package com.ray.system.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysUserInfo;
import com.ray.template.TemplateRecord;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: SysUserInfo 构造器
 * @Class: UserInfoBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @User <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class UserInfoBuilder extends AbstractBuilder<SysUserInfo, UserInfoBuilder> {

    public UserInfoBuilder() {
        super(new SysUserInfo());
        super.setBuilder(this);
    }

    @Override
    public SysUserInfo bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param templateRecord
     * @return
     */
    public UserInfoBuilder append(TemplateRecord templateRecord) {
        if (ObjectUtil.isNotNull(templateRecord)) {
            entity.setName(templateRecord.getPropertyName());
            entity.setProp(templateRecord.getProperty());
            entity.setValue(templateRecord.getPropertyValue());
        }
        return this;
    }


    /**
     * 添加编码
     *
     * @return
     */
    public UserInfoBuilder appendCode(String code) {
        entity.setUserCode(code);
        return this;
    }

    /**
     * 添加组织编码
     *
     * @return
     */
    public UserInfoBuilder appendCompanyCode(String companyCode) {
        entity.setCompanyCode(companyCode);
        return this;
    }


    @Override
    public String getCode() {
        return entity.getUserCode();
    }


}
