package com.ray.system.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysUserMenu;
import com.ray.system.table.params.role.RoleAuthParams;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: SysUserMenu 构造器
 * @Class: UserMenuBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class UserMenuBuilder extends AbstractBuilder<SysUserMenu, UserMenuBuilder> {

    public UserMenuBuilder() {
        super(new SysUserMenu());
        super.setBuilder(this);
    }

    @Override
    public SysUserMenu bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }



    /**
     * 添加编码
     *
     * @return
     */
    public UserMenuBuilder appendRoleCode(String code) {
        entity.setRoleCode(code);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public UserMenuBuilder appendMenuCode(String code) {
        entity.setMenuCode(code);
        return this;
    }

    /**
     *
     * @param authParams
     * @return
     */
    public UserMenuBuilder append(RoleAuthParams authParams) {
        if(ObjectUtil.isNotNull(authParams)){
            entity.setRoleCode(authParams.getRoleCode());
            entity.setMenuCode(authParams.getMenuCode());
        }
        return this;
    }
}
