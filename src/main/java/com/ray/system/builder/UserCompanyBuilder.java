package com.ray.system.builder;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysUserCompany;
import com.ray.system.table.entity.SysUserMenu;
import com.ray.system.table.params.role.RoleAuthParams;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: SysUserMenu 构造器
 * @Class: UserMenuBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class UserCompanyBuilder extends AbstractBuilder<SysUserCompany, UserCompanyBuilder> {

    public UserCompanyBuilder() {
        super(new SysUserCompany());
        super.setBuilder(this);
    }

    @Override
    public SysUserCompany bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }



    /**
     * 添加编码
     *
     * @return
     */
    public UserCompanyBuilder appendCompanyCode(String code) {
        entity.setComapnyCode(code);
        entity.setDeptCode(code);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public UserCompanyBuilder appendDeptCode(String code) {
        if(StrUtil.isNotBlank(code)){
            entity.setDeptCode(code);
        }
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public UserCompanyBuilder appendUserCode(String code) {
        entity.setUserCode(code);
        return this;
    }

}
