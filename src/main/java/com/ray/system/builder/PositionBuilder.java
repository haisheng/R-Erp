package com.ray.system.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysPosition;
import com.ray.system.table.params.position.PositionCreateParams;
import com.ray.system.table.params.position.PositionEditParams;
import com.ray.util.CodeCreateUtil;
import com.ray.woodencreate.enums.DeleteEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Position 构造器
 * @Class: CustomerBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class PositionBuilder extends AbstractBuilder<SysPosition,PositionBuilder> {

    public PositionBuilder() {
        super(new SysPosition());
        super.setBuilder(this);
        entity.setPositionCode(CodeCreateUtil.getCode(Perifx.POSITION_CODE));
        log.info("获得职位编码:{}", entity.getPositionCode());
    }

    @Override
    public SysPosition bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public PositionBuilder append(PositionCreateParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    /**
     *  添加状态
     * @return
     */
    public PositionBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }
    /**
     *  添加编码
     * @return
     */
    public PositionBuilder appendCode(String code) {
        entity.setPositionCode(code);
        return this;
    }
    /**
     * 构建创建信息
     * @param editParams
     * @return
     */
    public PositionBuilder append(PositionEditParams editParams) {
        if(ObjectUtil.isNotNull(editParams)){
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getPositionCode();
    }



}
