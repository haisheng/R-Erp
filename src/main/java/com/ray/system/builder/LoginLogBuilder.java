package com.ray.system.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysLoginLog;
import com.ray.system.table.entity.SysUser;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

/**
 * @author bo shen
 * @Description: LoginLog 构造器
 * @Class: LoginLogBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class LoginLogBuilder extends AbstractBuilder<SysLoginLog,LoginLogBuilder> {

    public LoginLogBuilder() {
        super(new SysLoginLog());
        super.setBuilder(this);
    }

    @Override
    public SysLoginLog bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    /**
     *  添加状态
     * @return
     */
    public LoginLogBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 用户信息
     * @param sysUser
     * @return
     */
    public LoginLogBuilder append(SysUser sysUser) {
        if(ObjectUtil.isNotNull(sysUser)){
            entity.setLoginTime(LocalDateTime.now());
            entity.setUserCode(sysUser.getUserCode());
            entity.setUserName(sysUser.getUserName());
        }
        return this;
    }
}
