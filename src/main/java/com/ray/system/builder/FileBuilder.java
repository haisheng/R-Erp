package com.ray.system.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.dto.FileDTO;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.system.table.entity.SysFile;
import com.ray.system.table.params.menu.MenuCreateParams;
import com.ray.util.CodeCreateUtil;
import com.ray.woodencreate.enums.DeleteEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: Menu 构造器
 * @Class: MenuBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class FileBuilder extends AbstractBuilder<SysFile,FileBuilder> {

    public FileBuilder() {
        super(new SysFile());
        super.setBuilder(this);
        entity.setFileCode(CodeCreateUtil.getCode(Perifx.RECORD_CODE));
        entity.setIsDeleted(DeleteEnum.USE.getValue());
    }

    @Override
    public SysFile bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public FileBuilder append(FileDTO createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    public FileBuilder appendBusinessCode(String businessCode) {
        entity.setBusinessCode(businessCode);
        return this;
    }
    public FileBuilder appendType(String type) {
        entity.setBusinessType(type);
        return this;
    }

    @Override
    public String getCode() {
        return entity.getFileCode();
    }



}
