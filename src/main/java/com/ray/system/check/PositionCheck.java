package com.ray.system.check;

import com.ray.common.check.AbstractCheck;
import com.ray.system.table.entity.SysPosition;

/**
 * @author bo shen
 * @Description: 职位校验
 * @Class: CustomerCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class PositionCheck extends AbstractCheck<SysPosition> {


    public PositionCheck(SysPosition entity) {
        super(entity);
    }

    @Override
    public PositionCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }


}
