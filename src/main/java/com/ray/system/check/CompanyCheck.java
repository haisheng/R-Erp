package com.ray.system.check;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.common.check.AbstractCheck;
import com.ray.system.table.entity.SysCompany;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 公司校验
 * @Class: MaterialCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class CompanyCheck extends AbstractCheck<SysCompany> {


    public CompanyCheck(SysCompany entity) {
        super(entity);
    }

    @Override
    public CompanyCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }

    /**
     * 校验
     * @param companyName
     * @param messageCode
     */
    public CompanyCheck checkCompanyName(String companyName,String messageCode) {
        //存在 并且appCode相等
        if(ObjectUtil.isNotNull(entity) && !StrUtil.equals(companyName,entity.getCompanyName())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验不能有子公司
     * @param childConpanyCount
     * @return
     */
    public CompanyCheck checkNotHasChild(int childConpanyCount,String messageCode) {
        if(childConpanyCount >0){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
