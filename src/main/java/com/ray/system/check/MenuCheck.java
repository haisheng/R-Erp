package com.ray.system.check;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.common.check.AbstractCheck;
import com.ray.system.enums.MenuTypeEnum;
import com.ray.system.table.entity.SysMenu;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 职位校验
 * @Class: MaterialCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class MenuCheck extends AbstractCheck<SysMenu> {


    public MenuCheck(SysMenu entity) {
        super(entity);
    }

    @Override
    public MenuCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }

    /**
     * 校验
     * @param menuName
     * @param messageCode
     */
    public MenuCheck checkMenuName(String menuName,String messageCode) {
        if(ObjectUtil.isNotNull(entity) && !StrUtil.equals(menuName,entity.getMenuName())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验不能有子组织
     * @param childDeptCount
     * @return
     */
    public MenuCheck checkNotHasChild(int childDeptCount, String messageCode) {
        if(childDeptCount >0){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验是否一致
     * @param appCode
     * @param messageCode
     * @return
     */
    public MenuCheck chechSamAppCode( String appCode, String messageCode) {
        if(!StrUtil.equals(appCode,entity.getAppCode())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public MenuCheck checkType(String menuType,String messageCode) {
        //按钮下面不能创建子节点
        if(StrUtil.equals(entity.getMenuType(), MenuTypeEnum.BTN.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        //非菜单下面不能创建菜单
        if(!StrUtil.equals(entity.getMenuType(), MenuTypeEnum.MENU.getValue()) && StrUtil.equals(menuType, MenuTypeEnum.MENU.getValue())){
            throw BusinessExceptionFactory.newException(messageCode);
        }

        return this;
    }
}
