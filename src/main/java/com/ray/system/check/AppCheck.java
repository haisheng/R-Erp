package com.ray.system.check;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.common.check.AbstractCheck;
import com.ray.system.table.entity.SysApp;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 应用校验
 * @Class: AppCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class AppCheck extends AbstractCheck<SysApp> {


    public AppCheck(SysApp entity) {
        super(entity);
    }

    @Override
    public AppCheck checkNull(String messageCode) {
         super.checkNull(messageCode);
         return this;
    }

    /**
     * 校验
     * @param appCode
     * @param messageCode
     */
    public void checkAppKey(String appCode,String messageCode) {
        //存在 并且appCode相等
        if(ObjectUtil.isNotNull(entity) && !StrUtil.equals(appCode,entity.getAppCode())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
    }
}
