package com.ray.system.check;

import com.ray.common.check.AbstractCheck;
import com.ray.system.table.entity.SysLogin;

/**
 * @author bo shen
 * @Description: 登录校验
 * @Class: LoginCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class LoginCheck extends AbstractCheck<SysLogin> {


    public LoginCheck(SysLogin entity) {
        super(entity);
    }

    @Override
    public LoginCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }


}
