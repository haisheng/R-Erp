package com.ray.system.check;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.common.check.AbstractCheck;
import com.ray.system.table.entity.SysOrganization;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 组织校验
 * @Class: OrganizationCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @Organization <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class OrganizationCheck extends AbstractCheck<SysOrganization> {


    public OrganizationCheck(SysOrganization entity) {
        super(entity);
    }

    @Override
    public OrganizationCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }

    /**
     * 校验
     * @param OrganizationName
     * @param messageCode
     */
    public OrganizationCheck checkOrganizationName(String OrganizationName, String messageCode) {
        if(ObjectUtil.isNotNull(entity) && !StrUtil.equals(OrganizationName,entity.getDeptName())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验不能有子组织
     * @param childDeptCount
     * @return
     */
    public OrganizationCheck checkNotHasChild(int childDeptCount, String messageCode) {
        if(childDeptCount >0){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
