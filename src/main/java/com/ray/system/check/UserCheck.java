package com.ray.system.check;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.common.check.AbstractCheck;
import com.ray.system.table.entity.SysUser;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 用户校验
 * @Class: UserCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @User <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class UserCheck extends AbstractCheck<SysUser> {


    public UserCheck(SysUser entity) {
        super(entity);
    }

    @Override
    public UserCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }

    /**
     * 校验
     * @param mobile
     * @param messageCode
     */
    public UserCheck checkMobile(String mobile, String messageCode) {
        if(ObjectUtil.isNotNull(entity) && !StrUtil.equals(mobile,entity.getMobile())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

}
