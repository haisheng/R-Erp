package com.ray.system.check;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.common.check.AbstractCheck;
import com.ray.system.table.entity.SysApp;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 职位校验
 * @Class: ImageCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class ImageCheck extends AbstractCheck<String> {


    public ImageCheck(String entity) {
        super(entity);
    }

    @Override
    public ImageCheck checkNull(String messageCode) {
         super.checkNull(messageCode);
         return this;
    }

    /**
     * 校验
     * @param imageCode
     * @param messageCode
     */
    public void checkSam(String imageCode,String messageCode) {
        if(ObjectUtil.isNotNull(entity) && !StrUtil.equals(imageCode.toUpperCase(),entity.toUpperCase())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
    }
}
