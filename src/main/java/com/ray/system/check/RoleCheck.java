package com.ray.system.check;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.common.check.AbstractCheck;
import com.ray.system.table.entity.SysRole;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 角色校验
 * @Class: MaterialCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class RoleCheck extends AbstractCheck<SysRole> {


    public RoleCheck(SysRole entity) {
        super(entity);
    }

    @Override
    public RoleCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }

    /**
     * 校验
     * @param roleName
     * @param messageCode
     */
    public RoleCheck checkRoleName(String roleName, String messageCode) {
        if(ObjectUtil.isNotNull(entity) && !StrUtil.equals(roleName,entity.getRoleName())){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public RoleCheck checkDelete(String messageCode) {
        if(entity.getCanDelete() == 0){
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
