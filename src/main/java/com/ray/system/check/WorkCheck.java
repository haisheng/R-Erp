package com.ray.system.check;

import com.ray.common.check.AbstractCheck;
import com.ray.system.table.entity.SysWork;

/**
 * @author bo shen
 * @Description: 待办校验
 * @Class: WorkCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class WorkCheck extends AbstractCheck<SysWork> {


    public WorkCheck(SysWork entity) {
        super(entity);
    }

    @Override
    public WorkCheck checkNull(String message) {
         super.checkNull(message);
         return this;
    }


}
