package com.ray.util;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.dto.FileDTO;
import com.ray.base.table.params.FileParams;
import com.ray.base.table.vo.FileVO;
import com.ray.system.table.entity.SysFile;
import com.ray.woodencreate.config.SysFileConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description:
 * @Class: FileRecordUtil
 * @Package com.ray.util
 * @date 2020/7/14 12:29
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class FileRecordUtil {

    public static List<FileDTO> toFileDTO(List<FileParams> files) {
        if(ObjectUtil.isNotEmpty(files)){
         return    files.parallelStream().map(fileParams -> {
             FileDTO fileDTO = new FileDTO();
             BeanUtil.copyProperties(fileParams,fileDTO);
             return fileDTO;
            }).collect(Collectors.toList());
        }
        return  null;
    }


    public static List<FileVO> toFileVO(List<SysFile> files, SysFileConfig fileConfig) {
        if(ObjectUtil.isNotEmpty(files)){
            return    files.parallelStream().map(fileParams -> {
                FileVO fileDTO = new FileVO();
                BeanUtil.copyProperties(fileParams,fileDTO);
                fileDTO.setFileUrl(fileParams.getFileUrl());
                if(ObjectUtil.isNotNull(fileConfig)){
                    fileDTO.setFullUrl(String.format("%s%s",fileConfig.getPrefix(),fileParams.getFileUrl()));
                }
                return fileDTO;
            }).collect(Collectors.toList());
        }
        return  new ArrayList<>();
    }
}
