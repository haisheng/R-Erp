package com.ray.util;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import lombok.extern.slf4j.Slf4j;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;

/**
 * @author bo shen
 * @Description: 获取一个唯一Code
 * @Class: IdUtil
 * @Package com.tf56.bms.util
 * @date 2020/1/10 14:21
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class CodeCreateUtil {

    public static synchronized String getCode(String perifx) {
        try {
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            StringBuilder tradeNumber = new StringBuilder(perifx);
            int a = secureRandom.nextInt(10);
            int b = secureRandom.nextInt(10);
            int c = secureRandom.nextInt(10);
            int d = secureRandom.nextInt(10);
            tradeNumber.append(DateUtil.format(new Date(), "yyyyMMddHHmm"));
            tradeNumber.append(a);
            tradeNumber.append(b);
            tradeNumber.append(c);
            tradeNumber.append(d);
            return tradeNumber.toString();
        } catch (NoSuchAlgorithmException e) {
            log.info("生成Code异常",e);
            throw BusinessExceptionFactory.newException("生成Code异常");
        }
    }

    public static synchronized String nextCode(String perifx) {
        try {
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            StringBuilder tradeNumber = new StringBuilder(perifx);
            int a = secureRandom.nextInt(10);
            int b = secureRandom.nextInt(10);
            int c = secureRandom.nextInt(10);
            int d = secureRandom.nextInt(10);
            tradeNumber.append("|");
            tradeNumber.append(DateUtil.format(new Date(), DatePattern.PURE_DATE_FORMAT));
            tradeNumber.append(a);
            tradeNumber.append(b);
            tradeNumber.append(c);
            tradeNumber.append(d);
            return tradeNumber.toString();
        } catch (NoSuchAlgorithmException e) {
            log.info("生成Code异常",e);
            throw BusinessExceptionFactory.newException("生成Code异常");
        }
    }

    public static synchronized String nextChildCode(String perifx) {
        try {
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            StringBuilder tradeNumber = new StringBuilder(perifx);
            int a = secureRandom.nextInt(10);
            int b = secureRandom.nextInt(10);
            int c = secureRandom.nextInt(10);
            int d = secureRandom.nextInt(10);
            tradeNumber.append(".");
            tradeNumber.append(DateUtil.format(new Date(), DatePattern.PURE_DATE_FORMAT));
            tradeNumber.append(a);
            tradeNumber.append(b);
            tradeNumber.append(c);
            tradeNumber.append(d);
            return tradeNumber.toString();
        } catch (NoSuchAlgorithmException e) {
            log.info("生成Code异常",e);
            throw BusinessExceptionFactory.newException("生成Code异常");
        }
    }

}
