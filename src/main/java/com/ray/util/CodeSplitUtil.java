package com.ray.util;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;

/**
 * @author bo shen
 * @Description:
 * @Class: CodeSplitUtil
 * @Package com.ray.util
 * @date 2020/8/3 16:26
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class CodeSplitUtil {

    public static String getFirst(String goodsCode) {
        if (StrUtil.isNotBlank(goodsCode)) {
            int index = goodsCode.indexOf("|");
            if (index > 0) {
                return goodsCode.substring(0, index);
            }
            return goodsCode;
        }
        return goodsCode;
    }
}
