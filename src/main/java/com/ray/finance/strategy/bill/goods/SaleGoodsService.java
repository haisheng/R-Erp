package com.ray.finance.strategy.bill.goods;

import cn.hutool.core.util.ObjectUtil;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.builder.PurchaseGoodsBuilder;
import com.ray.business.builder.SaleGoodsBuilder;
import com.ray.business.service.ProdSaleBackRecordService;
import com.ray.business.service.ProdSaleOutRecordService;
import com.ray.business.service.ProdSaleOutService;
import com.ray.business.service.ProdSaleService;
import com.ray.business.table.entity.*;
import com.ray.business.table.vo.PurchaseGoodsVO;
import com.ray.business.table.vo.SaleGoodsVO;
import com.ray.finance.table.params.bill.BillCreateParams;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 对账单
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "goodsList", strategy = "SALE", desc = "销售对账单商品明细")
public class SaleGoodsService implements Strategy<String, LoginUser, List<?>> {

    @Autowired
    private ProdSaleOutRecordService prodSaleOutRecordService;
    @Autowired
    private ProdSaleOutService prodSaleOutService;
    @Autowired
    private ProdSaleBackRecordService prodSaleBackRecordService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ProdSaleService prodSaleService;

    @Override
    public List<?> execute(String billNo, LoginUser loginUser) {
        //查询对账单对应的订单
        List<String> orderNOs = prodSaleService.querySaleByBIllNo(billNo, loginUser).stream().map(ProdSale::getOrderNo).collect(Collectors.toList());
        BillCreateParams billCreateParams = new BillCreateParams();
        billCreateParams.setOrderNos(orderNOs);
        return saleGoodsList(billCreateParams);
    }

    /**
     * 銷售单明细
     *
     * @param createParams
     * @return
     */
    public List<SaleGoodsVO> saleGoodsList(@Valid BillCreateParams createParams) {
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //查询采购单列表
        List<String> orderNos = createParams.getOrderNos();
        if (ObjectUtil.isEmpty(orderNos)) {
            return new ArrayList<>();
        }
        Map<String, ProdSaleOutRecord> inRecordMap = new HashMap<>();
        //查询完成的订单
        List<String> finishNos = prodSaleOutService.listFinishByOrderNos(orderNos, loginUser).stream().map(ProdSaleOut::getOutCode).collect(Collectors.toList());
        //入库数据
        List<ProdSaleOutRecord> records = prodSaleOutRecordService.listByOutCodes(finishNos, loginUser);
        //处理数据
        records.forEach(prodSaleOutRecord -> {
            String key = String.format("%s_%s", prodSaleOutRecord.getOrderNo(), prodSaleOutRecord.getGoodsCode());
            ProdSaleOutRecord outRecord = inRecordMap.get(key);
            if (ObjectUtil.isNull(outRecord)) {
                inRecordMap.put(key, prodSaleOutRecord);
            } else {
                outRecord.setQuantity(outRecord.getQuantity().add(outRecord.getQuantity()));
                outRecord.setTotal(outRecord.getTotal() + outRecord.getTotal());
            }
        });
        //退货数据
        List<ProdSaleBackRecord> backRecords = prodSaleBackRecordService.listByOrderNo(orderNos, loginUser);
        //处理数据
        backRecords.forEach(prodSaleBackRecord -> {
            String key = String.format("%s_%s", prodSaleBackRecord.getOrderNo(), prodSaleBackRecord.getGoodsCode());
            ProdSaleOutRecord outRecord = inRecordMap.get(key);
            if (ObjectUtil.isNotNull(outRecord)) {
                outRecord.setQuantity(outRecord.getQuantity().subtract(prodSaleBackRecord.getQuantity()));
                outRecord.setTotal(outRecord.getTotal() - (prodSaleBackRecord.getTotal()));
            }
        });
        final List<BigDecimal> quantity = new ArrayList<>();
        Map<String, MaterialModelVO> modelMap = new HashMap<>();
        final List<BigDecimal> totalPriceList = new ArrayList<>();
        List<SaleGoodsVO> saleGoodsVOS = inRecordMap.values().stream().map(prodSaleOutRecord -> {
            MaterialModelVO materialModelVO = modelMap.get(prodSaleOutRecord.getGoodsCode());
            if (ObjectUtil.isNull(materialModelVO)) {
                materialModelVO = goodsService.queryGoodsByCode(prodSaleOutRecord.getGoodsCode(), loginUser);
                modelMap.put(prodSaleOutRecord.getGoodsCode(), materialModelVO);
            }
            totalPriceList.add(prodSaleOutRecord.getQuantity().multiply(prodSaleOutRecord.getPrice()));
            quantity.add(prodSaleOutRecord.getQuantity());
            return new SaleGoodsBuilder().append(prodSaleOutRecord).append(materialModelVO)
                    .appendTotalAmount(prodSaleOutRecord.getQuantity().multiply(prodSaleOutRecord.getPrice())).bulid();
        }).collect(Collectors.toList());
        //计算总价
        SaleGoodsVO purchaseGoodsVO = new SaleGoodsVO();
        purchaseGoodsVO.setBizCode("合计:");
        purchaseGoodsVO.setTotalAmount(totalPriceList.stream().reduce(BigDecimal::add).get());
        purchaseGoodsVO.setQuantity(quantity.stream().reduce(BigDecimal::add).get());
        saleGoodsVOS.add(purchaseGoodsVO);
        return saleGoodsVOS;
    }

}
