package com.ray.finance.strategy.bill.cancel;

import com.ray.business.service.ProdBusinessDeductionService;
import com.ray.business.service.ProdBusinessService;
import com.ray.finance.builder.BillBuilder;
import com.ray.finance.enums.BillStatusEnum;
import com.ray.finance.service.FinaBillService;
import com.ray.finance.table.entity.FinaBill;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 对账单
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "cancelBill", strategy = "BUSINESS", desc = "加工对账单取消")
public class BusinessCancelService implements Strategy<FinaBill, LoginUser, String> {

    @Autowired
    private ProdBusinessService prodBusinessService;
    @Autowired
    private ProdBusinessDeductionService prodBusinessDeductionService;
    @Autowired
    private FinaBillService finaBillService;

    @Override
    public String execute(FinaBill finaBill, LoginUser loginUser) {
        BillBuilder billBuilder = new BillBuilder();
        billBuilder.appendEdit(loginUser).appendCode(finaBill.getBillNo()).appendOrderStatus(BillStatusEnum.CANCEL.getValue());
        finaBillService.edit(billBuilder.bulid(), loginUser);
        //删除关联
        if (!prodBusinessService.cancelBill(finaBill.getBillNo(), loginUser)) {
            throw BusinessExceptionFactory.newException("删除加工单对账信息异常");
        }
        if (!prodBusinessDeductionService.cancelBill(finaBill.getBillNo(), loginUser)) {
            throw BusinessExceptionFactory.newException("删除加工单扣款对账信息异常");
        }
        return billBuilder.getCode();
    }
}
