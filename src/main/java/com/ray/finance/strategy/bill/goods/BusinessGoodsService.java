package com.ray.finance.strategy.bill.goods;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.service.BaseCustomerService;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.builder.BusinessGoodsVOBuilder;
import com.ray.business.enums.BusinessTypeEnum;
import com.ray.business.service.*;
import com.ray.business.service.compose.BusinessService;
import com.ray.business.service.compose.PurcahseService;
import com.ray.business.service.compose.SaleService;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdBusinessIn;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.finance.api.BillAdjustApi;
import com.ray.finance.service.FinaBillAdjustService;
import com.ray.finance.service.FinaBillPayService;
import com.ray.finance.service.FinaBillService;
import com.ray.finance.service.compose.AdvanceService;
import com.ray.finance.table.entity.FinaBill;
import com.ray.finance.table.params.bill.BillCreateParams;
import com.ray.magicBlock.BlockDispatch;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 对账单
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "goodsList", strategy = "BUSINESS", desc = "销售对账单商品明细")
public class BusinessGoodsService implements Strategy<String, LoginUser, List<?>> {


    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ProdBusinessInService prodBusinessInService;
    @Autowired
    private ProdBusinessService prodBusinessService;

    @Override
    public List<?> execute(String billNo, LoginUser loginUser) {
        //查询对账单对应的订单
        List<String> orderNOs = prodBusinessService.queryBusinessByBIllNo(billNo, loginUser).stream().map(ProdBusiness::getBusinessCode).collect(Collectors.toList());
        BillCreateParams billCreateParams = new BillCreateParams();
        billCreateParams.setOrderNos(orderNOs);
        return businessGoodsList(billCreateParams);
    }

    /**
     * 加工对账单明细
     *
     * @param createParams
     * @return
     */
    public List<BusinessGoodsVO> businessGoodsList(BillCreateParams createParams) {
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //查询采购单列表
        List<String> orderNos = createParams.getOrderNos();
        if (ObjectUtil.isEmpty(orderNos)) {
            return new ArrayList<>();
        }
        Map<String, BigDecimal> inRecordMap = new HashMap<>();

        //查询加工单号
        List<ProdBusiness> prodBusinesses = prodBusinessService.listByOrderNosBIllFinish(orderNos, BusinessTypeEnum.IN.getValue(), loginUser);
        prodBusinesses.forEach(prodBusiness -> {
            inRecordMap.put(prodBusiness.getBusinessCode(), NumberUtil.null2Zero(prodBusiness.getPrice()));
        });
        List<ProdBusinessIn> records = prodBusinessInService.listAll(orderNos, loginUser);

        Map<String, MaterialModelVO> modelMap = new HashMap<>();
        //价格
        final List<BigDecimal> totalPriceList = new ArrayList<>();
        //数量
        final List<BigDecimal> quantity = new ArrayList<>();
        List<BusinessGoodsVO> businessGoodsVOS = records.stream().map(prodBusinessIn -> {
            MaterialModelVO materialModelVO = modelMap.get(prodBusinessIn.getGoodsCode());
            if (ObjectUtil.isNull(materialModelVO)) {
                materialModelVO = goodsService.queryGoodsByCode(prodBusinessIn.getGoodsCode(), loginUser);
                modelMap.put(prodBusinessIn.getGoodsCode(), materialModelVO);
            }
            //数量
            quantity.add(prodBusinessIn.getQuantity());
            //价格
            totalPriceList.add(prodBusinessIn.getQuantity().multiply(NumberUtil.null2Zero(inRecordMap.get(prodBusinessIn.getBusinessCode()))));
            return new BusinessGoodsVOBuilder().append(prodBusinessIn).append(materialModelVO)
                    .appendPrice(NumberUtil.null2Zero(inRecordMap.get(prodBusinessIn.getBusinessCode())))
                    .appendTotalAmount(prodBusinessIn.getQuantity().multiply(NumberUtil.null2Zero(inRecordMap.get(prodBusinessIn.getBusinessCode()))))
                    .bulid();
        }).collect(Collectors.toList());
        //计算总价
        BusinessGoodsVO purchaseGoodsVO = new BusinessGoodsVO();
        purchaseGoodsVO.setBizCode("合计:");
        purchaseGoodsVO.setTotalAmount(totalPriceList.stream().reduce(BigDecimal::add).get());
        purchaseGoodsVO.setQuantity(quantity.stream().reduce(BigDecimal::add).get());
        businessGoodsVOS.add(purchaseGoodsVO);
        return businessGoodsVOS;
    }

}
