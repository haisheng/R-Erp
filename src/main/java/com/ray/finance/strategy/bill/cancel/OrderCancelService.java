package com.ray.finance.strategy.bill.cancel;

import com.ray.business.service.ProdOrderSendService;
import com.ray.business.service.ProdSaleBackService;
import com.ray.business.service.ProdSaleService;
import com.ray.finance.builder.BillBuilder;
import com.ray.finance.enums.BillStatusEnum;
import com.ray.finance.service.FinaBillService;
import com.ray.finance.table.entity.FinaBill;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 对账单
 * @Class: SaleInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "cancelBill", strategy = "ORDER", desc = "订单对账单取消")
public class OrderCancelService implements Strategy<FinaBill, LoginUser, String> {

    @Autowired
    private ProdOrderSendService prodOrderSendService;
    @Autowired
    private FinaBillService finaBillService;

    @Override
    public String execute(FinaBill finaBill, LoginUser loginUser) {
        BillBuilder billBuilder = new BillBuilder();
        billBuilder.appendEdit(loginUser).appendCode(finaBill.getBillNo()).appendOrderStatus(BillStatusEnum.CANCEL.getValue());
        finaBillService.edit(billBuilder.bulid(), loginUser);
        //删除关联
        if (!prodOrderSendService.cancelBill(finaBill.getBillNo(), loginUser)) {
            throw BusinessExceptionFactory.newException("取消销售对账异常");
        }
        return billBuilder.getCode();
    }
}
