package com.ray.finance.strategy.bill.goods;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.service.compose.GoodsService;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.business.builder.SendGoodsBuilder;
import com.ray.business.service.ProdOrderGoodsService;
import com.ray.business.service.ProdOrderSendService;
import com.ray.business.service.ProdSendRecordService;
import com.ray.business.table.entity.ProdBusiness;
import com.ray.business.table.entity.ProdOrderGoods;
import com.ray.business.table.entity.ProdOrderSend;
import com.ray.business.table.entity.ProdSendRecord;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.business.table.vo.SendGoodsVO;
import com.ray.finance.table.params.bill.BillCreateParams;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 对账单
 * @Class: PurchaseInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "goodsList", strategy = "ORDER", desc = "订单对账单商品明细")
public class OrderGoodsService implements Strategy<String, LoginUser, List<?>> {

    @Autowired
    private ProdOrderSendService prodOrderSendService;
    @Autowired
    private ProdSendRecordService prodSendRecordService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ProdOrderGoodsService prodOrderGoodsService;

    @Override
    public List<?> execute(String billNo, LoginUser loginUser) {
        //查询对账单对应的订单
        List<String> orderNOs = prodOrderSendService.listByBillNo(billNo, loginUser).stream().map(ProdOrderSend::getSendCode).collect(Collectors.toList());
        BillCreateParams billCreateParams = new BillCreateParams();
        billCreateParams.setOrderNos(orderNOs);
        return orderGoodsList(billCreateParams,loginUser);
    }

    private List<SendGoodsVO> orderGoodsList(BillCreateParams createParams, LoginUser loginUser) {
        //查询采购单列表
        List<String> orderNos = createParams.getOrderNos();
        if (ObjectUtil.isEmpty(orderNos)) {
            return new ArrayList<>();
        }
        //查询
        List<ProdSendRecord> prodOrderSendRecords = prodSendRecordService.listAll(orderNos, loginUser);
        Map<String, MaterialModelVO> modelMap = new HashMap<>();
        Map<String, ProdOrderGoods> goodsMap = new HashMap<>();
        final  List<BigDecimal> quantity = new ArrayList<>();
        final List<BigDecimal> totalPriceList = new ArrayList<>();
        List<SendGoodsVO> saleGoodsVOS = prodOrderSendRecords.stream().map(prodOrderSendRecord -> {
            MaterialModelVO materialModelVO = modelMap.get(prodOrderSendRecord.getGoodsCode());
            if (ObjectUtil.isNull(materialModelVO)) {
                materialModelVO = goodsService.queryGoodsByCode(prodOrderSendRecord.getGoodsCode(), loginUser);
                modelMap.put(prodOrderSendRecord.getGoodsCode(), materialModelVO);
            }
            ProdOrderGoods orderGoods = goodsMap.get(prodOrderSendRecord.getGoodsCode());
            if (ObjectUtil.isNull(orderGoods)) {
                orderGoods=  prodOrderGoodsService.queryOrderGoodsByGoodsCode(prodOrderSendRecord.getOrderNo(),prodOrderSendRecord.getGoodsCode(),loginUser);
                goodsMap.put(prodOrderSendRecord.getGoodsCode(), orderGoods);
            }
            totalPriceList.add(prodOrderSendRecord.getQuantity().multiply(prodOrderSendRecord.getPrice()));
            quantity.add(prodOrderSendRecord.getQuantity());
            return new SendGoodsBuilder().append(prodOrderSendRecord).append(materialModelVO).append(orderGoods)
                    .appendTotalAmount(prodOrderSendRecord.getQuantity().multiply(prodOrderSendRecord.getPrice()))
                    .bulid();
        }).collect(Collectors.toList());
        SendGoodsVO purchaseGoodsVO = new SendGoodsVO();
        purchaseGoodsVO.setBizCode("合计:");
        purchaseGoodsVO.setTotalAmount(totalPriceList.stream().reduce(BigDecimal::add).get());
        purchaseGoodsVO.setQuantity(quantity.stream().reduce(BigDecimal::add).get());
        saleGoodsVOS.add(purchaseGoodsVO);
        return saleGoodsVOS;
    }


}
