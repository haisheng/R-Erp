package com.ray.finance.strategy.advance.create;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.business.check.OrderCheck;
import com.ray.business.check.OrderSendCheck;
import com.ray.business.service.ProdOrderSendService;
import com.ray.business.service.ProdOrderService;
import com.ray.business.table.entity.ProdOrder;
import com.ray.business.table.entity.ProdOrderSend;
import com.ray.finance.builder.AdvanceBuilder;
import com.ray.finance.builder.BillBuilder;
import com.ray.finance.check.AdvanceCheck;
import com.ray.finance.enums.AdvanceTypeEnum;
import com.ray.finance.enums.BillSourceEnum;
import com.ray.finance.enums.BillStatusEnum;
import com.ray.finance.enums.BillTypeEnum;
import com.ray.finance.service.FinaAdvanceService;
import com.ray.finance.service.FinaBillService;
import com.ray.finance.table.entity.FinaAdvance;
import com.ray.finance.table.entity.FinaBill;
import com.ray.finance.table.params.advance.AdvanceCreateParams;
import com.ray.finance.table.params.bill.BillCreateParams;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.system.enums.FileTypeEnum;
import com.ray.system.service.SysFileService;
import com.ray.util.FileRecordUtil;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author bo shen
 * @Description: 预付款单
 * @Class: SaleInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "createAdvance", strategy = "ORDER", desc = "订单预付款单")
public class OrderCreateService implements Strategy<AdvanceCreateParams, LoginUser, String> {

    @Autowired
    private ProdOrderService prodOrderService;
    @Autowired
    private FinaAdvanceService finaAdvanceService;
    @Autowired
    private SysFileService sysFileService;

    @Override
    public String execute(AdvanceCreateParams createParams, LoginUser loginUser) {
        //查询订单是否存在
        ProdOrder prodOrder = prodOrderService.queryOrderByOrderCode(createParams.getOrderNo(),loginUser);
        new OrderCheck(prodOrder).checkNull("业务订单不存在").checkCanAdvance("订单已经完成");
        //查询订单是否已经存在预付款单
        FinaAdvance finaAdvance = finaAdvanceService.queryAdvanceByOrderNo(createParams.getOrderNo(),loginUser);
        new AdvanceCheck(finaAdvance).checkNotNull("预付款记录已经存在");
        AdvanceBuilder advanceBuilder = new AdvanceBuilder();
        advanceBuilder.append(createParams).append(prodOrder).appendType(AdvanceTypeEnum.ORDER.getValue()).appendCreate(loginUser);
        finaAdvanceService.save(advanceBuilder.bulid());
        //保存文件
        sysFileService.saveFile(advanceBuilder.getCode(), FileTypeEnum.ADVANCE, FileRecordUtil.toFileDTO(createParams.getFiles()),loginUser);
        return advanceBuilder.getCode();
    }
}
