package com.ray.finance.strategy.advance.create;

import com.ray.business.check.OrderCheck;
import com.ray.business.check.PurchaseCheck;
import com.ray.business.service.ProdOrderService;
import com.ray.business.service.ProdPurchaseService;
import com.ray.business.table.entity.ProdOrder;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.finance.builder.AdvanceBuilder;
import com.ray.finance.check.AdvanceCheck;
import com.ray.finance.enums.AdvanceTypeEnum;
import com.ray.finance.service.FinaAdvanceService;
import com.ray.finance.table.entity.FinaAdvance;
import com.ray.finance.table.params.advance.AdvanceCreateParams;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.system.enums.FileTypeEnum;
import com.ray.system.service.SysFileService;
import com.ray.util.FileRecordUtil;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 预付款单
 * @Class: SaleInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "createAdvance", strategy = "PURCHASE", desc = "采购订单预付款单")
public class PurchaseCreateService implements Strategy<AdvanceCreateParams, LoginUser, String> {

    @Autowired
    private ProdPurchaseService prodPurchaseService;
    @Autowired
    private FinaAdvanceService finaAdvanceService;
    @Autowired
    private SysFileService sysFileService;

    @Override
    public String execute(AdvanceCreateParams createParams, LoginUser loginUser) {
        //查询采购订单是否存在
        ProdPurchase prodPurchase = prodPurchaseService.queryPurchaseByPurchaseCode(createParams.getOrderNo(),loginUser);
        new PurchaseCheck(prodPurchase).checkNull("采购订单不存在").checkCanAdvance("订单已经完成");
        //查询订单是否已经存在预付款单
        FinaAdvance finaAdvance = finaAdvanceService.queryAdvanceByOrderNo(createParams.getOrderNo(),loginUser);
        new AdvanceCheck(finaAdvance).checkNotNull("预付款记录已经存在");
        AdvanceBuilder advanceBuilder = new AdvanceBuilder();
        advanceBuilder.append(createParams).append(prodPurchase).appendType(AdvanceTypeEnum.PURCHASE.getValue()).appendCreate(loginUser);
        finaAdvanceService.save(advanceBuilder.bulid());
        //保存文件
        sysFileService.saveFile(advanceBuilder.getCode(), FileTypeEnum.ADVANCE, FileRecordUtil.toFileDTO(createParams.getFiles()),loginUser);
        return advanceBuilder.getCode();
    }
}
