package com.ray.finance.strategy.advance.delete;

import com.alibaba.fastjson.JSON;
import com.ray.business.check.PurchaseCheck;
import com.ray.business.service.ProdPurchaseService;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.finance.builder.AdvanceBuilder;
import com.ray.finance.check.AdvanceCheck;
import com.ray.finance.service.FinaAdvanceService;
import com.ray.finance.table.entity.FinaAdvance;
import com.ray.magicBlock.Strategy;
import com.ray.magicBlock.anno.Block;
import com.ray.system.enums.FileTypeEnum;
import com.ray.system.service.SysFileService;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author bo shen
 * @Description: 预付款单
 * @Class: SaleInService
 * @Package com.ray.wms.strategy.in
 * @date 2020/6/8 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Block(group = "deleteAdvance", strategy = "PURCHASE", desc = "采购订单预付款单")
public class PurchaseDeleteService implements Strategy<FinaAdvance, LoginUser, String> {

    @Autowired
    private ProdPurchaseService prodPurchaseService;
    @Autowired
    private FinaAdvanceService finaAdvanceService;
    @Autowired
    private SysFileService sysFileService;

    @Override
    public String execute(FinaAdvance advance, LoginUser loginUser) {
        //查询订单是否存在
        ProdPurchase prodPurchase = prodPurchaseService.queryPurchaseByPurchaseCode(advance.getOrderNo(),loginUser);
        new PurchaseCheck(prodPurchase).checkNull("采购订单不存在").checkCanAdvance("订单已经完成");
        //查询订单是否已经存在预付款单
        new AdvanceCheck(advance).checkNull("预付款记录不存在").checkUsed("预付款金额已被使用");
        AdvanceBuilder advanceBuilder = new AdvanceBuilder();
        advanceBuilder.appendCode(advance.getAdvanceCode()).appendEdit(loginUser).delete();
        if(!finaAdvanceService.edit(advanceBuilder.bulid(),loginUser)){
            log.info("编辑预付款单异常:{}", JSON.toJSONString(advanceBuilder.bulid()));
            throw BusinessExceptionFactory.newException("编辑预付款单异常");
        }
        //删除文件
        sysFileService.deleteFile(advance.getAdvanceCode(), FileTypeEnum.ADVANCE.getValue(),loginUser);
        return advanceBuilder.getCode();
    }
}
