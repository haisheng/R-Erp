package com.ray.finance.table.params.advance;

import com.ray.base.table.params.FileParams;
import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author bo shen
 * @Description: 预付款
 * @Class: AdvanceBase
 * @Package com.ray.finance.table.params.advance
 * @date 2020/7/27 8:44
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class AdvanceQueryParams extends BaseParams{

    /**
     * 预付单单号
     */
    @ApiModelProperty(value = "预付单单号", required = true)
    private String advanceCode;

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号", required = true)
    private String orderNo;

    /**
     * 业务类型
     */
    @ApiModelProperty(value = "业务类型", required = true)
    private String type;

}
