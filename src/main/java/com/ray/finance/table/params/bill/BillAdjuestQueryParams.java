package com.ray.finance.table.params.bill;

import com.ray.common.BaseParams;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 对账单查询
 * @Class: BillQueryParams
 * @Package com.ray.finance.table.params
 * @date 2020/6/15 15:30
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BillAdjuestQueryParams extends BaseParams{

    @NotBlank(message = "对账单号不能为空")
    private String billNo;

}
