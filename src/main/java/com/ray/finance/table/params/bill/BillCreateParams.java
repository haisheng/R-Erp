package com.ray.finance.table.params.bill;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author bo shen
 * @Description: 对账单创建
 * @Class: BillCreateParams
 * @Package com.ray.finance.table.params
 * @date 2020/6/15 15:28
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BillCreateParams {

    @NotNull(message = "订单列表不能为空")
    private List<String> orderNos;
}
