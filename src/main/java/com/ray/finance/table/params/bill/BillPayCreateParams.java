package com.ray.finance.table.params.bill;

import com.ray.base.table.params.FileParams;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 对账单创建
 * @Class: BillCreateParams
 * @Package com.ray.finance.table.params
 * @date 2020/6/15 15:28
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BillPayCreateParams {
    /**
     * 对账单号
     */
    @NotBlank(message = "调整单号不能为空")
    private String billNo;

    /**
     * 金额
     */
    @NotNull(message = "金额不能为空")
    private BigDecimal amount;

    /**
     * 银行账号
     */
    private String bankCode;
    /**
     * 银行名称
     */
    private String bankName;
    /**
     * 账户人
     */
    private String bankUser;


    private LocalDateTime payTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 文件
     */
    private List<FileParams> files;
}
