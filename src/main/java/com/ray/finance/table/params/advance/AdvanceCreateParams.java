package com.ray.finance.table.params.advance;

import lombok.Data;

/**
 * @author bo shen
 * @Description:
 * @Class: AdvanceCreateParams
 * @Package com.ray.finance.table.params.advance
 * @date 2020/7/27 8:52
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class AdvanceCreateParams extends AdvanceBase {
}
