package com.ray.finance.table.params.advance;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ray.base.table.params.FileParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author bo shen
 * @Description: 预付款
 * @Class: AdvanceBase
 * @Package com.ray.finance.table.params.advance
 * @date 2020/7/27 8:44
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class AdvanceBase {

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号", required = true)
    @NotBlank(message = "订单编号不能为空")
    @Length(max = 50, message = "订单编号长度不能超过50")
    private String orderNo;

    /**
     * 预付金额
     */
    @ApiModelProperty(value = "预付金额", required = true)
    @NotNull(message = "预付金额不能为空")
    @DecimalMin(value = "0.001",message = "预付金额不能小于0")
    private BigDecimal amount;


    /**
     * 登记时间
     */
    @ApiModelProperty(value = "登记时间", required = true)
    @NotNull(message = "登记时间不能为空")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat( pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime payTime;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", required = true)
    private String remark;

    /**
     * 银行账号
     */
    private String bankCode;
    /**
     * 银行名称
     */
    private String bankName;
    /**
     * 账户人
     */
    private String bankUser;

    /**
     * 文件
     */
    @ApiModelProperty(value = "文件", required = true)
    private List<FileParams> files;

}
