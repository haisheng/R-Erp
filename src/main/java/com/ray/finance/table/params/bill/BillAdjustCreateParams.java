package com.ray.finance.table.params.bill;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 对账单创建
 * @Class: BillCreateParams
 * @Package com.ray.finance.table.params
 * @date 2020/6/15 15:28
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BillAdjustCreateParams {
    /**
     * 对账单号
     */
    @NotBlank(message = "调整单号不能为空")
    private String billNo;

    /**
     * 对账金额
     */
    @NotNull(message = "调整金额不能为空")
    private BigDecimal amount;

    /**
     * 调整原因
     */
    private String remark;
}
