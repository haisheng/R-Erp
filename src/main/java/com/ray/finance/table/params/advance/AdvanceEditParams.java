package com.ray.finance.table.params.advance;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description:
 * @Class: AdvanceCreateParams
 * @Package com.ray.finance.table.params.advance
 * @date 2020/7/27 8:52
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class AdvanceEditParams extends AdvanceBase {

    /**
     * 预付款单号
     */
    @ApiModelProperty(value = "预付款单号", required = true)
    @NotBlank(message = "预付款单号不能为空")
    @Length(max = 50, message = "预付款单号长度不能超过50")
    private String advanceCode;
}
