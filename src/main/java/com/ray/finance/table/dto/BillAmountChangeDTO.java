package com.ray.finance.table.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 调整
 * @Class: BillAmountChangeDTO
 * @Package com.ray.finance.table.dto
 * @date 2020/6/19 11:29
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BillAmountChangeDTO {

    private  String  billNo;


    private BigDecimal amount;


    private Integer  updateVersion;
}
