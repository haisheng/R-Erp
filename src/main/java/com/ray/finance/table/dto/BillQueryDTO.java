package com.ray.finance.table.dto;

import com.ray.common.BaseParams;
import com.ray.common.dto.BaseDTO;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author bo shen
 * @Description: 对账单查询
 * @Class: BillQueryParams
 * @Package com.ray.finance.table.params
 * @date 2020/6/15 15:30
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BillQueryDTO extends BaseDTO {

    private String billType;

    /**
     * 客户编号
     */
    private String customerCode;

    /**
     * 订单状态
     */
    private String orderStatus;
    /**
     * 对账单
     */
    private String billNo;
    /**
     * 工序
     */
    private String stepCode;


}
