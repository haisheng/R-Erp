package com.ray.finance.table.dto;

import com.ray.common.dto.BaseDTO;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 预付款
 * @Class: AdvanceBase
 * @Package com.ray.finance.table.params.advance
 * @date 2020/7/27 8:44
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class AdvanceQueryDTO extends BaseDTO{

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 业务类型
     */
    private String type;

}
