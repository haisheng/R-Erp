package com.ray.finance.table.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 汇总统计
 * @Class: BillCountVO
 * @Package com.ray.finance.table.vo
 * @date 2020/7/8 8:43
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BillCountDTO {

    /**
     * 客户编号
     */
    private String customerCode;

    /**
     * 对账金额
     */
    private BigDecimal amount;


    /**
     * 已支付
     */
    private BigDecimal payAmount;


    private String currency;
}
