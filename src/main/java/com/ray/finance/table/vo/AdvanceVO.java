package com.ray.finance.table.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ray.base.table.vo.FileVO;
import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author bo shen
 * @Description: 预付款
 * @Class: AdvanceBase
 * @Package com.ray.finance.table.vo
 * @date 2020/7/27 8:44
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class AdvanceVO extends BaseVO {

    /**
     * 预付款单号
     */
    @ApiModelProperty(value = "预付款单号", required = true)
    private String advanceCode;
    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号", required = true)
    private String orderNo;

    /**
     * 预付金额
     */
    @ApiModelProperty(value = "预付金额", required = true)
    private BigDecimal amount;


    /**
     * 可用金额
     */
    private BigDecimal usableAmount;

    /**
     * 客户编码
     */
    private String customerCode;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 登记时间
     */
    @ApiModelProperty(value = "登记时间", required = true)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime payTime;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", required = true)
    private String remark;

    /**
     * 银行账号
     */
    private String bankCode;
    /**
     * 银行名称
     */
    private String bankName;
    /**
     * 账户人
     */
    private String bankUser;

    /**
     * 文件
     */
    @ApiModelProperty(value = "文件", required = true)
    private List<FileVO> files;

}
