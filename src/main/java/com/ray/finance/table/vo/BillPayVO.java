package com.ray.finance.table.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ray.base.table.vo.FileVO;
import com.ray.common.BaseVO;
import com.ray.util.Decimal2Serializer;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 对账单
 * @Class: BillVO
 * @Package com.ray.finance.table.vo
 * @date 2020/6/15 15:33
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BillPayVO extends BaseVO {

    /**
     * 付款编号
     */
    private String payCode;

    /**
     * 付款类型
     */
    private String  payType;
    /**
     * 对账单号
     */
    private String billNo;

    /**
     * 金额
     */
    @JsonSerialize(using = Decimal2Serializer.class)
    private BigDecimal amount;

    /**
     * 银行账号
     */
    private String bankCode;
    /**
     * 银行名称
     */
    private String bankName;
    /**
     * 账户人
     */
    private String bankUser;
    /**
     * 备注
     */
    private String remark;

    /**
     * 付款时间
     */
    @JSONField(format = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDateTime payTime;


    private List<FileVO> files;

}
