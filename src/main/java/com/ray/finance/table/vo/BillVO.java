package com.ray.finance.table.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ray.business.table.vo.BusinessVO;
import com.ray.common.BaseVO;
import com.ray.util.Decimal2Serializer;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author bo shen
 * @Description: 对账单
 * @Class: BillVO
 * @Package com.ray.finance.table.vo
 * @date 2020/6/15 15:33
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BillVO extends BaseVO {
    /**
     * 对账单号
     */
    private String billNo;

    /**
     * 对账单类型 应收 应付
     */
    private String billType;

    /**
     * 账单来源  加工  销售  采购  订单
     */
    private String billSource;

    /**
     * 币种
     */
    private String currency;

    /**
     * 客户编号
     */
    private String customerCode;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 步骤编码
     */
    private String stepCode;

    /**
     * 步骤名称
     */
    private String stepName;

    /**
     * 总金额
     */
    @JsonSerialize(using = Decimal2Serializer.class)
    private BigDecimal totalAmount;

    /**
     * 扣款金额
     */
    @JsonSerialize(using = Decimal2Serializer.class)
    private BigDecimal deductionAmount;

    /**
     * 对账金额
     */
    @JsonSerialize(using = Decimal2Serializer.class)
    private BigDecimal amount;


    /**
     * 已支付
     */
    @JsonSerialize(using = Decimal2Serializer.class)
    private BigDecimal payAmount;

    /**
     * 支付时间
     */
    private LocalDateTime payTime;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 调整单信息
     */
    private List<BillAdjustVO> adjustVos;
    /**
     * 加工单详情
     */
    private List<BusinessVO> businessVOS;
}
