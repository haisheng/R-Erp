package com.ray.finance.table.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ray.common.BaseVO;
import com.ray.util.Decimal2Serializer;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author bo shen
 * @Description: 对账单
 * @Class: BillVO
 * @Package com.ray.finance.table.vo
 * @date 2020/6/15 15:33
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BillAdjustVO extends BaseVO {
    /**
     * 对账单号
     */
    private String billNo;

    /**
     * 调整单
     */
    private String adjustNo;

    /**
     * t调整方式 增加  减少
     */
    private String adjustType;

    /**
     * 客户编号
     */
    private String customerCode;

    /**
     * 客户编号
     */
    private String customerName;

    /**
     * 对账金额
     */
    @JsonSerialize(using = Decimal2Serializer.class)
    private BigDecimal amount;

    /**
     * 调整原因
     */
    private String remark;

}
