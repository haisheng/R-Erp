package com.ray.finance.table.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ray.base.table.vo.FileVO;
import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author bo shen
 * @Description: 预付款
 * @Class: AdvanceBase
 * @Package com.ray.finance.table.vo
 * @date 2020/7/27 8:44
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class AdvanceDetailVO extends BaseVO {

    /**
     * 对账单号
     */
    private String billNo;

    /**
     * 预付款单号
     */
    private String advanceCode;

    /**
     * 已支付金额
     */
    private BigDecimal amount;
}
