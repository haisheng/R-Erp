package com.ray.finance.table.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ray.util.Decimal2Serializer;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 汇总统计
 * @Class: BillCountVO
 * @Package com.ray.finance.table.vo
 * @date 2020/7/8 8:43
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BillCountVO {
    /**
     * 类型
     */
    private String typeName;

    /**
     * 客户编号
     */
    private String customerCode;

    /**
     * 客户名称
     */
    private String customerName;
    /**
     * 对账金额
     */
    @JsonSerialize(using = Decimal2Serializer.class)
    private BigDecimal amount;


    /**
     * 已支付
     */
    @JsonSerialize(using = Decimal2Serializer.class)
    private BigDecimal payAmount;


    private String currency;
}
