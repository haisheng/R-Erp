package com.ray.finance.table.mapper;

import com.ray.finance.table.entity.FinaAdvanceDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 对账单使用预付款记录 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-07-27
 */
public interface FinaAdvanceDetailMapper extends BaseMapper<FinaAdvanceDetail> {

}
