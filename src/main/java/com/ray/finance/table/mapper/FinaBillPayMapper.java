package com.ray.finance.table.mapper;

import com.ray.finance.table.entity.FinaBillPay;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 对账单 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-07-05
 */
public interface FinaBillPayMapper extends BaseMapper<FinaBillPay> {

}
