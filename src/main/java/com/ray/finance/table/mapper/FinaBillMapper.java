package com.ray.finance.table.mapper;

import com.ray.finance.table.dto.BillCountDTO;
import com.ray.finance.table.dto.BillCountQueryDTO;
import com.ray.finance.table.entity.FinaBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 对账单 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-15
 */
public interface FinaBillMapper extends BaseMapper<FinaBill> {

    /**
     * 金额统计
     * @param billCountQueryDTO
     * @return
     */
    List<BillCountDTO> countAmount(BillCountQueryDTO billCountQueryDTO);

    /**
     * 金额全部统计
     * @param billCountQueryDTO
     * @return
     */
    List<BillCountDTO> countAllAmount(BillCountQueryDTO billCountQueryDTO);
}
