package com.ray.finance.table.mapper;

import com.ray.finance.table.entity.FinaAdvance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 预付款 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-07-27
 */
public interface FinaAdvanceMapper extends BaseMapper<FinaAdvance> {

}
