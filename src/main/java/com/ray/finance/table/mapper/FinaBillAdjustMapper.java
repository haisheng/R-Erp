package com.ray.finance.table.mapper;

import com.ray.finance.table.entity.FinaBillAdjust;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 加工单号 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-15
 */
public interface FinaBillAdjustMapper extends BaseMapper<FinaBillAdjust> {

}
