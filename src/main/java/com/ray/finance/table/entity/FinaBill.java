package com.ray.finance.table.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 对账单
 * </p>
 *
 * @author shenbo
 * @since 2020-06-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fina_bill")
public class FinaBill implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 对账单号
     */
    private String billNo;

    /**
     * 对账单类型 应收 应付
     */
    private String billType;

    /**
     * 账单来源  加工  销售  采购  订单
     */
    private String billSource;

    /**
     * 币种
     */
    private String currency;

    /**
     * 客户编号
     */
    private String customerCode;

    /**
     * 步骤编码
     */
    private String stepCode;

    /**
     * 总金额
     */
    private BigDecimal totalAmount;

    /**
     * 扣款金额
     */
    private BigDecimal deductionAmount;

    /**
     * 对账金额
     */
    private BigDecimal amount;

    /**
     * 已支付金额
     */
    private BigDecimal payAmount;

    /**
     * 支付时间
     */
    private LocalDateTime payTime;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 状态  1:启用 0:禁用
     */
    private Integer status;

    /**
     * 是否删除 1:删除 0:未删除
     */
    @TableLogic
    private Integer isDeleted;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建人编码
     */
    private String createUserCode;

    /**
     * 创建人名称
     */
    private String createUserName;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新用户编码
     */
    private String updateUserCode;

    /**
     * 更新用户名称
     */
    private String updateUserName;

    /**
     * 租户编码
     */
    private String tenantCode;

    /**
     * 用户编码
     */
    private String ownerCode;

    /**
     * 组织编码
     */
    private String organizationCode;

    /**
     * 排序
     */
    private Integer indexSort;

    /**
     * 版本号
     */
    @Version
    private Integer updateVersion;


}
