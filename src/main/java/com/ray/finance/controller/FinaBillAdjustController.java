package com.ray.finance.controller;


import com.ray.finance.api.BillAdjustApi;
import com.ray.finance.table.params.bill.BillAdjuestQueryParams;
import com.ray.finance.table.params.bill.BillAdjustCreateParams;
import com.ray.finance.table.vo.BillAdjustVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 调整单 前端控制器
 * </p>
 *
 * @author shenbo
 * @since 2020-06-15
 */
@RestController
@RequestMapping("/fina/bill/adjust")
public class FinaBillAdjustController {

    @Autowired
    private BillAdjustApi billAdjustApi;

    @ApiOperation("查询对账列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BillAdjustVO>> page(@RequestBody @Valid BillAdjuestQueryParams queryParams) {
        return billAdjustApi.queryAdjusts(queryParams);
    }

    @ApiOperation("创建调整单")
    @PostMapping(value = "add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid BillAdjustCreateParams createParams) {
        return billAdjustApi.createAdjust(createParams);
    }


    @ApiOperation("创建调整单")
    @PostMapping(value = "delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "调整单号", required = true) @RequestParam(required = true) String adjustNo) {
        return billAdjustApi.deleteAdjust(adjustNo);
    }

}
