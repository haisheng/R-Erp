package com.ray.finance.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.business.table.vo.PurchaseGoodsVO;
import com.ray.finance.api.BillApi;
import com.ray.finance.enums.BillSourceEnum;
import com.ray.finance.enums.BillTypeEnum;
import com.ray.finance.table.params.bill.BillCountQueryParams;
import com.ray.finance.table.params.bill.BillCreateParams;
import com.ray.finance.table.params.bill.BillQueryParams;
import com.ray.finance.table.vo.BillCountVO;
import com.ray.finance.table.vo.BillVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import javax.validation.Valid;

/**
 * <p>
 * 对账单 前端控制器
 * </p>
 *
 * @author shenbo
 * @since 2020-06-15
 */
@RestController
@RequestMapping("/fina/bill")
@Validated
public class FinaBillController {

    @Autowired
    private BillApi billApi;

    @ApiOperation("查询对账列表查询")
    @PostMapping(value = "/list/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<BillVO>> page(@RequestBody @Valid CommonPage<BillQueryParams, Page<BillVO>> queryParams) {
        return billApi.pageBillss(queryParams);
    }

    @ApiOperation("生产对账单")
    @PostMapping(value = "/business/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid BillCreateParams createParams) {
        return billApi.createBusinessBill(createParams, BillSourceEnum.BUSINESS);
    }

    @ApiOperation("加工对账单-明细")
    @PostMapping(value = "/business/goods/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BusinessGoodsVO>> businessGoodsList(@RequestBody @Valid BillCreateParams createParams) {
        return billApi.businessGoodsList(createParams);
    }

    @ApiOperation("采购对账单")
    @PostMapping(value = "/pruchase/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> pruchaseAdd(@RequestBody @Valid BillCreateParams createParams) {
        return billApi.createBusinessBill(createParams, BillSourceEnum.PURCHASE);
    }

    @ApiOperation("采购对账单-明细")
    @PostMapping(value = "/pruchase/goods/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<PurchaseGoodsVO>> pruchaseGoodsList(@RequestBody @Valid BillCreateParams createParams) {
        return billApi.pruchaseGoodsList(createParams);
    }

    @ApiOperation("对账单商品-明细")
    @PostMapping(value = "/goods/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<?>> goodsList(@RequestBody @Valid BillQueryParams queryParams) {
        return billApi.goodsList(queryParams);
    }


    @ApiOperation("销售对账单")
    @PostMapping(value = "/sale/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> saleAdd(@RequestBody @Valid BillCreateParams createParams) {
        return billApi.createBusinessBill(createParams, BillSourceEnum.SALE);
    }

    @ApiOperation("订单对账单")
    @PostMapping(value = "/order/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> orderAdd(@RequestBody @Valid BillCreateParams createParams) {
        return billApi.createBusinessBill(createParams, BillSourceEnum.ORDER);
    }

    @ApiOperation("对账单通过")
    @PostMapping(value = "/pass", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> pass(@ApiParam(value = "对账单号", required = true) @RequestParam(required = true) String billNo) {
        return billApi.passBill(billNo);
    }

    @ApiOperation("对账单取消")
    @PostMapping(value = "/cancel", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> cancel(@ApiParam(value = "对账单号", required = true) @RequestParam(required = true) String billNo) {
        return billApi.cancelBill(billNo);
    }


    @ApiOperation("对账单完成")
    @PostMapping(value = "/finish", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> finish(@ApiParam(value = "对账单号", required = true) @RequestParam(required = true) String billNo) {
        return billApi.finishBill(billNo);
    }

    @ApiOperation("结算订单")
    @PostMapping(value = "/settlement", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> settlement(@ApiParam(value = "对账单号", required = true) @RequestParam(required = true) String billNo) {
        return billApi.settlementBill(billNo,true);
    }

    @ApiOperation("结清订单")
    @PostMapping(value = "/settle", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> settle(@ApiParam(value = "对账单号", required = true) @RequestParam(required = true) String billNo) {
        return billApi.settlementBill(billNo,false);
    }


    @ApiOperation("对账单详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<BillVO> view(@ApiParam(value = "对账单号", required = true) @RequestParam(required = true) String billNo) {
        return billApi.viewBill(billNo);
    }


    @ApiOperation("收款汇总")
    @PostMapping(value = "/count/in", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BillCountVO>> countIn(@RequestBody @Valid BillCountQueryParams queryParams) {
        return billApi.billCount(queryParams, BillTypeEnum.IN.getValue());
    }

    @ApiOperation("付款汇总")
    @PostMapping(value = "/count/out", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BillCountVO>> countOut(@RequestBody @Valid BillCountQueryParams queryParams) {
        return billApi.billCount(queryParams, BillTypeEnum.OUT.getValue());
    }

    @ApiOperation("对账汇总")
    @PostMapping(value = "/count/all", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BillCountVO>> countAll(@RequestBody @Valid BillCountQueryParams queryParams) {
        return billApi.billAllCount(queryParams);
    }
}
