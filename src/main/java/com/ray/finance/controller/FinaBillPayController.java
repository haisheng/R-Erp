package com.ray.finance.controller;


import com.ray.finance.api.BillPayApi;
import com.ray.finance.table.params.bill.BillPayCreateParams;
import com.ray.finance.table.params.bill.BillPayQueryParams;
import com.ray.finance.table.vo.BillPayVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 付款单 前端控制器
 * </p>
 *
 * @author shenbo
 * @since 2020-06-15
 */
@RestController
@RequestMapping("/fina/bill/pay")
public class FinaBillPayController {

    @Autowired
    private BillPayApi billPayApi;

    @ApiOperation("查询对账列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BillPayVO>> page(@RequestBody @Valid BillPayQueryParams queryParams) {
        return billPayApi.queryPays(queryParams);
    }

    @ApiOperation("创建付款单")
    @PostMapping(value = "add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid BillPayCreateParams createParams) {
        return billPayApi.createPay(createParams);
    }


    @ApiOperation("创建付款单")
    @PostMapping(value = "delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "付款单号", required = true) @RequestParam(required = true) String payCode) {
        return billPayApi.deletePay(payCode);
    }

}
