package com.ray.finance.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.finance.api.AdvanceApi;
import com.ray.finance.api.AdvanceApi;
import com.ray.finance.enums.AdvanceTypeEnum;
import com.ray.finance.table.params.advance.AdvanceCreateParams;
import com.ray.finance.table.params.advance.AdvanceEditParams;
import com.ray.finance.table.params.advance.AdvanceQueryParams;
import com.ray.finance.table.params.bill.BillQueryParams;
import com.ray.finance.table.vo.AdvanceDetailVO;
import com.ray.finance.table.vo.AdvanceVO;
import com.ray.finance.table.vo.BillVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 预付款 前端控制器
 * </p>
 *
 * @author shenbo
 * @since 2020-06-15
 */
@RestController
@RequestMapping("/fina/advance")
public class FinaAdvanceController {

    @Autowired
    private AdvanceApi advanceApi;

    @ApiOperation("查询预付款列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<AdvanceVO>> page(@RequestBody @Valid CommonPage<AdvanceQueryParams, Page<AdvanceVO>> queryParams) {
        return advanceApi.pageAdvances(queryParams);
    }

    @ApiOperation("创建预付款--订单")
    @PostMapping(value = "order/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> orderAdd(@RequestBody @Valid AdvanceCreateParams createParams) {
        return advanceApi.createAdvance(createParams, AdvanceTypeEnum.ORDER);
    }

    @ApiOperation("创建预付款--采购单")
    @PostMapping(value = "purchase/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> purchaseAdd(@RequestBody @Valid AdvanceCreateParams createParams) {
        return advanceApi.createAdvance(createParams, AdvanceTypeEnum.PURCHASE);
    }

    @ApiOperation("编辑预付款")
    @PostMapping(value = "edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid AdvanceEditParams editParams) {
        return advanceApi.editAdvance(editParams);
    }

    @ApiOperation("删除预付款")
    @PostMapping(value = "delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "预付款号", required = true) @RequestParam(required = true) String advanceCode) {
        return advanceApi.deleteAdvance(advanceCode);
    }

    @ApiOperation("预付款详情")
    @PostMapping(value = "view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<AdvanceVO> view(@ApiParam(value = "预付款号", required = true) @RequestParam(required = true) String advanceCode) {
        return advanceApi.viewAdvance(advanceCode);
    }

    @ApiOperation("预付款明细")
    @PostMapping(value = "detail", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<AdvanceDetailVO>> detail(@RequestBody @Valid AdvanceQueryParams queryParams) {
        return advanceApi.viewAdvanceDetail(queryParams.getAdvanceCode());
    }
}
