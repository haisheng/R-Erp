package com.ray.finance.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.finance.enums.AdjustTypeEnum;
import com.ray.finance.table.entity.FinaBill;
import com.ray.finance.table.entity.FinaBillAdjust;
import com.ray.finance.table.params.bill.BillAdjustCreateParams;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BillAdjustBuilder extends AbstractBuilder<FinaBillAdjust, BillAdjustBuilder> {

    public BillAdjustBuilder() {
        super(new FinaBillAdjust());
        super.setBuilder(this);
        entity.setAdjustNo(CodeCreateUtil.getCode(Perifx.BILL_CODE));
    }

    @Override
    public FinaBillAdjust bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    /**
     * 添加编码
     *
     * @return
     */
    public BillAdjustBuilder appendCode(String code) {
        entity.setAdjustNo(code);
        return this;
    }

    @Override
    public String getCode() {
        return entity.getAdjustNo();
    }


    public BillAdjustBuilder append(BillAdjustCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
            if (createParams.getAmount().compareTo(new BigDecimal(0)) > 0) {
                entity.setAdjustType(AdjustTypeEnum.IN.getValue());
            } else {
                entity.setAdjustType(AdjustTypeEnum.OUT.getValue());
            }
        }
        return this;
    }

    public BillAdjustBuilder append(FinaBill finaBill) {
        if (ObjectUtil.isNotNull(finaBill)) {
            entity.setCustomerCode(finaBill.getCustomerCode());
        }
        return this;
    }
}
