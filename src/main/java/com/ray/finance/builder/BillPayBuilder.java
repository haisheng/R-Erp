package com.ray.finance.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.finance.table.entity.FinaAdvance;
import com.ray.finance.table.entity.FinaBillPay;
import com.ray.finance.table.params.bill.BillPayCreateParams;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BillPayBuilder extends AbstractBuilder<FinaBillPay, BillPayBuilder> {

    public BillPayBuilder() {
        super(new FinaBillPay());
        super.setBuilder(this);
        entity.setPayCode(CodeCreateUtil.getCode(Perifx.CODE));
    }

    @Override
    public FinaBillPay bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    /**
     * 添加编码
     *
     * @return
     */
    public BillPayBuilder appendCode(String code) {
        entity.setPayCode(code);
        return this;
    }

    @Override
    public String getCode() {
        return entity.getPayCode();
    }


    public BillPayBuilder append(BillPayCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    public BillPayBuilder appendPayType(String value) {
        entity.setPayType(value);
        return this;
    }

    public BillPayBuilder appendBillNo(String code) {
        entity.setBillNo(code);
        return this;
    }

    public BillPayBuilder append(FinaAdvance advance) {
        if (ObjectUtil.isNotNull(advance)) {
          entity.setBankCode(advance.getBankCode());
          entity.setBankName(advance.getBankName());
          entity.setBankUser(advance.getBankUser());
          entity.setPayTime(advance.getPayTime());
        }
        return this;
    }

    public BillPayBuilder appendAmount(BigDecimal realAmount) {
        entity.setAmount(realAmount);
        return this;
    }

    public BillPayBuilder appendDesc(String s) {
        entity.setRemark(s);
        return this;
    }
}
