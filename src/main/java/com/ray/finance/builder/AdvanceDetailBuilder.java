package com.ray.finance.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdOrder;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.finance.table.entity.FinaAdvance;
import com.ray.finance.table.entity.FinaAdvanceDetail;
import com.ray.finance.table.params.advance.AdvanceCreateParams;
import com.ray.finance.table.params.advance.AdvanceEditParams;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class AdvanceDetailBuilder extends AbstractBuilder<FinaAdvanceDetail, AdvanceDetailBuilder> {

    public AdvanceDetailBuilder() {
        super(new FinaAdvanceDetail());
        super.setBuilder(this);
    }

    @Override
    public FinaAdvanceDetail bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 添加编码
     *
     * @return
     */
    public AdvanceDetailBuilder appendCode(String code) {
        entity.setAdvanceCode(code);
        return this;
    }

    @Override
    public String getCode() {
        return entity.getAdvanceCode();
    }


    public AdvanceDetailBuilder appendBillNo(String code) {
        entity.setBillNo(code);
        return this;
    }

    public AdvanceDetailBuilder appendAmount(BigDecimal amount) {
        entity.setAmount(amount);
        return this;
    }
}
