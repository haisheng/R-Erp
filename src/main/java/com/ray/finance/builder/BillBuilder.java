package com.ray.finance.builder;


import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.finance.table.entity.FinaBill;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BillBuilder extends AbstractBuilder<FinaBill, BillBuilder> {

    public BillBuilder() {
        super(new FinaBill());
        super.setBuilder(this);
        entity.setBillNo(CodeCreateUtil.getCode(Perifx.BILL_CODE));
    }

    @Override
    public FinaBill bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }


    /**
     * 添加状态
     *
     * @return
     */
    public BillBuilder appendOrderStatus(String status) {
        entity.setOrderStatus(status);
        return this;
    }


    /**
     * 添加编码
     *
     * @return
     */
    public BillBuilder appendCode(String code) {
        entity.setBillNo(code);
        return this;
    }

    @Override
    public String getCode() {
        return entity.getBillNo();
    }

    public BillBuilder appendTime(LocalDateTime now) {
        entity.setPayTime(now);
        return this;
    }
}
