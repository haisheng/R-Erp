package com.ray.finance.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.business.table.entity.ProdOrder;
import com.ray.business.table.entity.ProdPurchase;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.finance.table.entity.FinaAdvance;
import com.ray.finance.table.params.advance.AdvanceCreateParams;
import com.ray.finance.table.params.advance.AdvanceEditParams;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class AdvanceBuilder extends AbstractBuilder<FinaAdvance, AdvanceBuilder> {

    public AdvanceBuilder() {
        super(new FinaAdvance());
        super.setBuilder(this);
        entity.setAdvanceCode(CodeCreateUtil.getCode(Perifx.RECORD_CODE));
    }

    @Override
    public FinaAdvance bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 添加编码
     *
     * @return
     */
    public AdvanceBuilder appendCode(String code) {
        entity.setAdvanceCode(code);
        return this;
    }

    @Override
    public String getCode() {
        return entity.getAdvanceCode();
    }

    public AdvanceBuilder append(AdvanceCreateParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
            entity.setUsableAmount(BigDecimal.ZERO);
        }
        return  this;
    }

    public AdvanceBuilder append(AdvanceEditParams editParams) {
        if(ObjectUtil.isNotNull(editParams)){
            BeanUtil.copyProperties(editParams,entity);
        }
        return  this;
    }

    public AdvanceBuilder append(ProdOrder prodOrder) {
        if(ObjectUtil.isNotNull(prodOrder)){
           entity.setCustomerCode(prodOrder.getCustomerCode());
        }
        return  this;
    }

    public AdvanceBuilder append(ProdPurchase prodPurchase) {
        if(ObjectUtil.isNotNull(prodPurchase)){
            entity.setCustomerCode(prodPurchase.getCustomerCode());
        }
        return  this;
    }

    public AdvanceBuilder appendType(String value) {
        entity.setType(value);
        return this;
    }


}
