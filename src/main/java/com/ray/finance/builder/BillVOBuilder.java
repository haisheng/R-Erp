package com.ray.finance.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.business.table.entity.ProdStep;
import com.ray.business.table.vo.BusinessVO;
import com.ray.common.builder.AbstractBuilder;
import com.ray.finance.table.entity.FinaBill;
import com.ray.finance.table.vo.BillAdjustVO;
import com.ray.finance.table.vo.BillVO;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author bo shen
 * @Description: Order 构造器
 * @Class: OrderBuilder
 * @Package com.ray.system.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BillVOBuilder extends AbstractBuilder<BillVO, BillVOBuilder> {

    public BillVOBuilder() {
        super(new BillVO());
        super.setBuilder(this);
    }

    @Override
    public BillVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param bill
     * @return
     */
    public BillVOBuilder append(FinaBill bill) {
        if (ObjectUtil.isNotNull(bill)) {
            BeanUtil.copyProperties(bill, entity);
        }
        return this;
    }

    public BillVOBuilder append(BaseCustomer baseCustomer) {
        if (ObjectUtil.isNotNull(baseCustomer)) {
            entity.setCustomerCode(baseCustomer.getCustomerCode());
            entity.setCustomerName(baseCustomer.getCustomerName());
        }
        return this;
    }

    public BillVOBuilder append(ProdStep step) {
        if (ObjectUtil.isNotNull(step)) {
            entity.setStepCode(step.getStepCode());
            entity.setStepName(step.getStepName());
        }
        return this;
    }


    public BillVOBuilder append(List<BillAdjustVO> data) {
        entity.setAdjustVos(data);
        return this;
    }

    public BillVOBuilder appendBusiness(List<BusinessVO> businessVOS) {
        entity.setBusinessVOS(businessVOS);
        return this;
    }
}
