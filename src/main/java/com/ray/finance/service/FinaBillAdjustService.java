package com.ray.finance.service;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.base.table.entity.BaseMaterialType;
import com.ray.finance.table.dto.BillAdjuestQueryDTO;
import com.ray.finance.table.entity.FinaBillAdjust;
import com.ray.finance.table.mapper.FinaBillAdjustMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 加工单号 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-15
 */
@Service
public class FinaBillAdjustService extends ServiceImpl<FinaBillAdjustMapper, FinaBillAdjust> {


    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<FinaBillAdjust> list(BillAdjuestQueryDTO queryParams, LoginUser loginUser) {
        FinaBillAdjust entity = BeanCreate.newBean(FinaBillAdjust.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<FinaBillAdjust> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 編輯
     *
     * @param entity
     * @param loginUser
     * @return
     */
    public boolean edit(FinaBillAdjust entity, LoginUser loginUser) {
        FinaBillAdjust query = new FinaBillAdjust();
        query.setAdjustNo(entity.getAdjustNo());
        UpdateWrapper<FinaBillAdjust> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 删除对账单对应的调整单
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public Boolean deleteByBillNo(String billNo, LoginUser loginUser) {
        FinaBillAdjust entity = new FinaBillAdjust();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        FinaBillAdjust query = new FinaBillAdjust();
        query.setBillNo(billNo);
        UpdateWrapper<FinaBillAdjust> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    public FinaBillAdjust queryAdjustByNo(String adjustCode, LoginUser loginUser) {
        FinaBillAdjust entity = BeanCreate.newBean(FinaBillAdjust.class);
        entity.setAdjustNo(adjustCode);
        QueryWrapper<FinaBillAdjust> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }
}
