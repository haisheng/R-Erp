package com.ray.finance.service;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.finance.table.dto.BillAmountChangeDTO;
import com.ray.finance.table.dto.BillPayQueryDTO;
import com.ray.finance.table.entity.FinaBill;
import com.ray.finance.table.entity.FinaBillPay;
import com.ray.finance.table.mapper.FinaBillPayMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 对账单 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-07-05
 */
@Service
public class FinaBillPayService extends ServiceImpl<FinaBillPayMapper, FinaBillPay> {
    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<FinaBillPay> list(BillPayQueryDTO queryParams, LoginUser loginUser) {
        FinaBillPay entity = BeanCreate.newBean(FinaBillPay.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<FinaBillPay> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 編輯
     *
     * @param entity
     * @param loginUser
     * @return
     */
    public boolean edit(FinaBillPay entity, LoginUser loginUser) {
        FinaBillPay query = new FinaBillPay();
        query.setPayCode(entity.getPayCode());
        UpdateWrapper<FinaBillPay> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 删除对账单对应的调整单
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public Boolean deleteByBillNo(String billNo, LoginUser loginUser) {
        FinaBillPay entity = new FinaBillPay();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        FinaBillPay query = new FinaBillPay();
        query.setBillNo(billNo);
        UpdateWrapper<FinaBillPay> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    public FinaBillPay queryPayByNo(String payCode, LoginUser loginUser) {
        FinaBillPay entity = BeanCreate.newBean(FinaBillPay.class);
        entity.setPayCode(payCode);
        QueryWrapper<FinaBillPay> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    public FinaBillPay queryPayByBillNoAndType(String billNo, String type, LoginUser loginUser) {
        FinaBillPay entity = BeanCreate.newBean(FinaBillPay.class);
        entity.setBillNo(billNo);
        entity.setPayType(type);
        QueryWrapper<FinaBillPay> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    public boolean updateAmount(BillAmountChangeDTO billAmountChangeDTO, LoginUser loginUser) {
        FinaBillPay entity = new FinaBillPay();
        entity.setUpdateVersion(billAmountChangeDTO.getUpdateVersion() + 1);
        entity.setAmount(billAmountChangeDTO.getAmount());
        entity.setPayTime(LocalDateTime.now());
        DataOPUtil.editUser(entity, loginUser);
        FinaBillPay query = new FinaBillPay();
        query.setPayCode(billAmountChangeDTO.getBillNo());
        query.setUpdateVersion(billAmountChangeDTO.getUpdateVersion());
        QueryWrapper<FinaBillPay> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return update(entity, queryWrapper);
    }
}
