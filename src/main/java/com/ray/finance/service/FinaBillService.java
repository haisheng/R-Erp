package com.ray.finance.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.finance.enums.BillStatusEnum;
import com.ray.finance.table.dto.BillAmountChangeDTO;
import com.ray.finance.table.dto.BillCountDTO;
import com.ray.finance.table.dto.BillCountQueryDTO;
import com.ray.finance.table.dto.BillQueryDTO;
import com.ray.finance.table.entity.FinaBill;
import com.ray.finance.table.mapper.FinaBillMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 对账单 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-15
 */
@Service
public class FinaBillService extends ServiceImpl<FinaBillMapper, FinaBill> {

    @Autowired
    private FinaBillMapper finaBillMapper;

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<FinaBill> page(CommonPage<BillQueryDTO, Page<FinaBill>> queryParams, LoginUser loginUser) {
        BillQueryDTO params = queryParams.getEntity();
        FinaBill entity = BeanCreate.newBean(FinaBill.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<FinaBill> queryWrapper = new QueryWrapper<>(entity);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.gt(ObjectUtil.isNotNull(params.getStartTime()), "create_time", params.getStartTime());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getEndTime()), "create_time", params.getEndTime());
        }
        queryWrapper.in(StrUtil.isBlank(params.getOrderStatus()) && ObjectUtil.isNotEmpty(params.getIncludeStatus()),
                "order_status", params.getIncludeStatus());
        queryWrapper.orderByDesc("create_time");
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return page(queryParams.getPage(), queryWrapper);
    }


    /**
     * 编辑订单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(FinaBill entity, LoginUser loginUser) {
        FinaBill query = new FinaBill();
        query.setBillNo(entity.getBillNo());
        UpdateWrapper<FinaBill> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }


    /**
     * 查询订单
     *
     * @param billNo    业务加工订单编码
     * @param loginUser 当前操作员
     * @return
     */
    public FinaBill queryBillByBillNo(String billNo, LoginUser loginUser) {
        FinaBill query = new FinaBill();
        query.setBillNo(billNo);
        QueryWrapper<FinaBill> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }


    /**
     * 更新金额
     *
     * @param billAmountChangeDTO
     * @param loginUser
     * @return
     */
    public boolean updateAmount(BillAmountChangeDTO billAmountChangeDTO, LoginUser loginUser) {
        FinaBill entity = new FinaBill();
        entity.setUpdateVersion(billAmountChangeDTO.getUpdateVersion() + 1);
        entity.setAmount(billAmountChangeDTO.getAmount());
        DataOPUtil.editUser(entity, loginUser);
        FinaBill query = new FinaBill();
        query.setBillNo(billAmountChangeDTO.getBillNo());
        query.setUpdateVersion(billAmountChangeDTO.getUpdateVersion());
        QueryWrapper<FinaBill> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return update(entity, queryWrapper);
    }

    /**
     * 更新付款金额
     *
     * @param billAmountChangeDTO
     * @param loginUser
     * @return
     */
    public boolean updatePayAmount(BillAmountChangeDTO billAmountChangeDTO, LoginUser loginUser) {
        FinaBill entity = new FinaBill();
        entity.setUpdateVersion(billAmountChangeDTO.getUpdateVersion() + 1);
        entity.setPayAmount(billAmountChangeDTO.getAmount());
        entity.setPayTime(LocalDateTime.now());
        DataOPUtil.editUser(entity, loginUser);
        FinaBill query = new FinaBill();
        query.setBillNo(billAmountChangeDTO.getBillNo());
        query.setUpdateVersion(billAmountChangeDTO.getUpdateVersion());
        QueryWrapper<FinaBill> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return update(entity, queryWrapper);
    }

    /**
     * 收付款信息统计
     *
     * @param billCountQueryDTO
     * @return
     */
    public List<BillCountDTO> countAmount(BillCountQueryDTO billCountQueryDTO) {
        return finaBillMapper.countAmount(billCountQueryDTO);
    }

    /**
     * 收付款全部信息统计
     *
     * @param billCountQueryDTO
     * @return
     */
    public List<BillCountDTO> countAllAmount(BillCountQueryDTO billCountQueryDTO) {
        return finaBillMapper.countAllAmount(billCountQueryDTO);
    }

    public FinaBill getNextBill(String billNo,String customerCode, String billType, LoginUser loginUser) {
        FinaBill query = new FinaBill();
        query.setCustomerCode(customerCode);
        query.setBillType(billType);
        query.setOrderStatus(BillStatusEnum.FINISH.getValue());
        QueryWrapper<FinaBill> queryWrapper = new QueryWrapper<>(query);
        queryWrapper.notIn("bill_no", new ArrayList<>(Arrays.asList(billNo)));
        queryWrapper.orderByAsc("create_time");
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        List<FinaBill> finaBills = list(queryWrapper);
        if (ObjectUtil.isNotEmpty(finaBills)) {
            return finaBills.get(0);
        }
        return null;
    }
}
