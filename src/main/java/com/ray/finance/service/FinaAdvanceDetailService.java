package com.ray.finance.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.common.AmountUpdate;
import com.ray.finance.table.entity.FinaAdvance;
import com.ray.finance.table.entity.FinaAdvanceDetail;
import com.ray.finance.table.mapper.FinaAdvanceDetailMapper;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 对账单使用预付款记录 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-07-27
 */
@Service
public class FinaAdvanceDetailService extends ServiceImpl<FinaAdvanceDetailMapper, FinaAdvanceDetail> {
    /**
     * 删除预付款单对应单明细
     *
     * @param advanceCode
     * @param loginUser
     * @return
     */
    public boolean deleteByAdvanceCode(String advanceCode, LoginUser loginUser) {
        FinaAdvanceDetail entity = new FinaAdvanceDetail();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        FinaAdvanceDetail query = new FinaAdvanceDetail();
        query.setAdvanceCode(advanceCode);
        UpdateWrapper<FinaAdvanceDetail> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 删除
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public boolean deleteByBillNo(String billNo, LoginUser loginUser) {
        FinaAdvanceDetail entity = new FinaAdvanceDetail();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        FinaAdvanceDetail query = new FinaAdvanceDetail();
        query.setBillNo(billNo);
        UpdateWrapper<FinaAdvanceDetail> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    /**
     * 查询对账单对应的记录
     *
     * @param billNo
     * @param loginUser
     * @return
     */
    public List<FinaAdvanceDetail> listByBillNo(String billNo, LoginUser loginUser) {
        FinaAdvanceDetail query = new FinaAdvanceDetail();
        query.setBillNo(billNo);
        QueryWrapper<FinaAdvanceDetail> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    public List<FinaAdvanceDetail> listByAdvanceCode(String advanceCode, LoginUser loginUser) {
        FinaAdvanceDetail query = new FinaAdvanceDetail();
        query.setAdvanceCode(advanceCode);
        QueryWrapper<FinaAdvanceDetail> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    public FinaAdvanceDetail getAdvanceDetailByBillNo(String billNo, LoginUser loginUser) {
        FinaAdvanceDetail query = new FinaAdvanceDetail();
        query.setBillNo(billNo);
        QueryWrapper<FinaAdvanceDetail> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    public boolean updateUsableAmount(AmountUpdate amountUpdate, LoginUser loginUser) {
        FinaAdvanceDetail entity = new FinaAdvanceDetail();
        entity.setUpdateVersion(amountUpdate.getUpdateVersion() + 1);
        entity.setAmount(amountUpdate.getAmount());
        FinaAdvanceDetail query = new FinaAdvanceDetail();
        query.setBillNo(amountUpdate.getBusinessCode());
        query.setUpdateVersion(amountUpdate.getUpdateVersion());
        UpdateWrapper<FinaAdvanceDetail> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }
}
