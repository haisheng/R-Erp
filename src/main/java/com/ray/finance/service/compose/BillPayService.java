package com.ray.finance.service.compose;

import com.ray.common.check.AbstractCheck;
import com.ray.finance.enums.PayTypeEnum;
import com.ray.finance.service.FinaBillPayService;
import com.ray.finance.table.dto.BillAmountChangeDTO;
import com.ray.finance.table.entity.FinaBillPay;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description:
 * @Class: BillPayService
 * @Package com.ray.finance.service.compose
 * @date 2020/8/19 19:27
 * @company <p>传化绿色慧联科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
public class BillPayService {

    @Autowired
    private FinaBillPayService finaBillPayService;

    public boolean updatePayDetailAmout(String billNo, BigDecimal usableAmount, LoginUser loginUser) {
        //付款记录
        FinaBillPay finaBillPay = finaBillPayService.queryPayByBillNoAndType(billNo, PayTypeEnum.ADVANCE.getValue(), loginUser);
        new AbstractCheck<>(finaBillPay).checkNull("付款记录不存在");
        BillAmountChangeDTO billPayAmountChangeDTO = new BillAmountChangeDTO(finaBillPay.getPayCode(), finaBillPay.getAmount().add(usableAmount), finaBillPay.getUpdateVersion());
        if (!finaBillPayService.updateAmount(billPayAmountChangeDTO, loginUser)) {
            throw BusinessExceptionFactory.newException("更新对账单付款记录金额异常");
        }
        return true;
    }
}
