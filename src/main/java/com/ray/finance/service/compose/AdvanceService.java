package com.ray.finance.service.compose;

import cn.hutool.core.util.ObjectUtil;
import com.ray.common.AmountUpdate;
import com.ray.finance.service.FinaAdvanceDetailService;
import com.ray.finance.service.FinaAdvanceService;
import com.ray.finance.table.entity.FinaAdvance;
import com.ray.finance.table.entity.FinaAdvanceDetail;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author bo shen
 * @Description:
 * @Class: AdvanceService
 * @Package com.ray.finance.service.compose
 * @date 2020/7/27 13:54
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class AdvanceService {

    @Autowired
    private FinaAdvanceService finaAdvanceService;
    @Autowired
    private FinaAdvanceDetailService finaAdvanceDetailService;

    public Boolean cancelAdvice(String billNo, LoginUser loginUser) {
        //查询对账单对应的记录
        List<FinaAdvanceDetail> details = finaAdvanceDetailService.listByBillNo(billNo, loginUser);
        details.forEach(finaAdvanceDetail -> {
            //查询对应的预付款记录
            FinaAdvance finaAdvance = finaAdvanceService.queryAdvanceByAdvanceCode(finaAdvanceDetail.getAdvanceCode(), loginUser);
            if (ObjectUtil.isNotNull(finaAdvance)) {
                //操作实际可用额度
                AmountUpdate amountUpdate = new AmountUpdate();
                amountUpdate.setBusinessCode(finaAdvance.getAdvanceCode());
                amountUpdate.setUpdateVersion(finaAdvance.getUpdateVersion());
                amountUpdate.setAmount(finaAdvance.getUsableAmount().subtract(finaAdvanceDetail.getAmount()));
                if (!finaAdvanceService.updateUsableAmount(amountUpdate, loginUser)) {
                    log.info("更新预付单已使用金额异常");
                    throw BusinessExceptionFactory.newException("更新预付单已使用金额异常");
                }
            }
        });
        finaAdvanceDetailService.deleteByBillNo(billNo, loginUser);
        return true;
    }

    /**
     * 更新预付款信息
     *
     * @param finaAdvanceDetail
     * @param finaAdvance
     * @param amount
     * @return
     */
    public boolean updateAmount(FinaAdvanceDetail finaAdvanceDetail, FinaAdvance finaAdvance, BigDecimal amount, LoginUser loginUser) {
        //操作实际可用额度
        AmountUpdate amountUpdate = new AmountUpdate();
        amountUpdate.setBusinessCode(finaAdvance.getAdvanceCode());
        amountUpdate.setUpdateVersion(finaAdvance.getUpdateVersion());
        amountUpdate.setAmount(finaAdvance.getUsableAmount().add(amount));
        if (!finaAdvanceService.updateUsableAmount(amountUpdate, loginUser)) {
            log.info("更新预付单已使用金额异常");
            throw BusinessExceptionFactory.newException("更新预付单已使用金额异常");
        }
        //操作实际可用额度
        AmountUpdate amountDetailUpdate = new AmountUpdate();
        amountDetailUpdate.setBusinessCode(finaAdvanceDetail.getBillNo());
        amountDetailUpdate.setUpdateVersion(finaAdvanceDetail.getUpdateVersion());
        amountDetailUpdate.setAmount(finaAdvanceDetail.getAmount().add(amount));
        if (!finaAdvanceDetailService.updateUsableAmount(amountDetailUpdate, loginUser)) {
            log.info("更新预付单已使用金额异常");
            throw BusinessExceptionFactory.newException("更新预付单明细金额异常");
        }
        return true;
    }
}
