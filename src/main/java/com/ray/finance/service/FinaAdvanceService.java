package com.ray.finance.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.common.AmountUpdate;
import com.ray.finance.table.dto.AdvanceQueryDTO;
import com.ray.finance.table.entity.FinaAdvance;
import com.ray.finance.table.mapper.FinaAdvanceMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 预付款 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-07-27
 */
@Service
public class FinaAdvanceService extends ServiceImpl<FinaAdvanceMapper, FinaAdvance>  {

    /**
     * 编辑预付款单
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(FinaAdvance entity, LoginUser loginUser) {
        FinaAdvance query = new FinaAdvance();
        query.setAdvanceCode(entity.getAdvanceCode());
        UpdateWrapper<FinaAdvance> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询预付款单
     *
     * @param advanceCode 预付款单编码
     * @param loginUser    当前操作员
     * @return
     */
    public FinaAdvance queryAdvanceByAdvanceCode(String advanceCode, LoginUser loginUser) {
        FinaAdvance query = new FinaAdvance();
        query.setAdvanceCode(advanceCode);
        QueryWrapper<FinaAdvance> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }



    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<FinaAdvance> page(CommonPage<AdvanceQueryDTO, Page<FinaAdvance>> queryParams, LoginUser loginUser) {
        AdvanceQueryDTO params = queryParams.getEntity();
        FinaAdvance entity = BeanCreate.newBean(FinaAdvance.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<FinaAdvance> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.gt(ObjectUtil.isNotNull(params.getStartTime()),"create_time",params.getStartTime());
            queryWrapper.lt(ObjectUtil.isNotNull(params.getEndTime()),"create_time",params.getEndTime());
        }
        queryWrapper.orderByDesc("create_time");
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<FinaAdvance> list(AdvanceQueryDTO queryParams, LoginUser loginUser) {
        FinaAdvance entity = BeanCreate.newBean(FinaAdvance.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<FinaAdvance> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    /**
     * 查询业务单对应的预付款单
     * @param orderNo
     * @param loginUser
     * @return
     */
    public FinaAdvance queryAdvanceByOrderNo( String orderNo, LoginUser loginUser) {
        FinaAdvance query = new FinaAdvance();
        query.setOrderNo(orderNo);
        QueryWrapper<FinaAdvance> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    public boolean updateUsableAmount(AmountUpdate amountUpdate, LoginUser loginUser) {
        FinaAdvance entity = new FinaAdvance();
        entity.setUpdateVersion(amountUpdate.getUpdateVersion() +1);
        entity.setUsableAmount(amountUpdate.getAmount());
        FinaAdvance query = new FinaAdvance();
        query.setAdvanceCode(amountUpdate.getBusinessCode());
        query.setUpdateVersion(amountUpdate.getUpdateVersion());
        UpdateWrapper<FinaAdvance> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }
}
