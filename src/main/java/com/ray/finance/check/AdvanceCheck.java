package com.ray.finance.check;

import com.ray.common.check.AbstractCheck;
import com.ray.finance.table.entity.FinaAdvance;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: StepCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class AdvanceCheck extends AbstractCheck<FinaAdvance> {


    public AdvanceCheck(FinaAdvance entity) {
        super(entity);
    }

    @Override
    public AdvanceCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }


    @Override
    public AdvanceCheck checkNotNull(String messageCode) {
        super.checkNotNull(messageCode);
        return this;
    }

    public AdvanceCheck checkUsed(String messageCode) {
        if (entity.getUsableAmount().compareTo(BigDecimal.ZERO) != 0) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public AdvanceCheck checkAmount(BigDecimal newAmount, String messageCode) {
        if (entity.getUsableAmount().compareTo(newAmount) > 0) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
