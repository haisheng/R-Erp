package com.ray.finance.check;

import cn.hutool.core.util.StrUtil;
import com.ray.business.enums.BusinessStatusEnum;
import com.ray.business.enums.BusinessTypeEnum;
import com.ray.common.check.AbstractCheck;
import com.ray.finance.enums.BillStatusEnum;
import com.ray.finance.table.entity.FinaBill;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: 校验
 * @Class: StepCheck
 * @Package com.ray.system.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class BillCheck extends AbstractCheck<FinaBill> {


    public BillCheck(FinaBill entity) {
        super(entity);
    }

    @Override
    public BillCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public BillCheck checkCanEdit(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BillStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    /**
     * 校验
     *
     * @param messageCode
     */
    public BillCheck checkFinish(String messageCode) {
        if (StrUtil.equals(entity.getOrderStatus(), BillStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


    /**
     * 校验
     *
     * @param messageCode
     */
    public BillCheck checkSettle(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BillStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


    /**
     * 校验
     *
     * @param messageCode
     */
    public BillCheck checkCanCancel(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BillStatusEnum.UN_CHECK.getValue()) &&
                !StrUtil.equals(entity.getOrderStatus(), BillStatusEnum.PASS.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public BillCheck checkCanCheck(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BillStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


    public BillCheck checkCanDelete(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BillStatusEnum.UN_CHECK.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public BillCheck checkAdjust(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BillStatusEnum.PASS.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public BillCheck checkCanFinish(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BillStatusEnum.PASS.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

    public BillCheck checkPay(String messageCode) {
        if (!StrUtil.equals(entity.getOrderStatus(), BillStatusEnum.FINISH.getValue())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }
}
