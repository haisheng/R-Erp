package com.ray.outside;

import com.alibaba.fastjson.JSON;
import com.ray.business.table.params.business.NumberQueryParams;
import com.ray.business.table.vo.BusinessGoodsVO;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.List;

/**
 * @author bo shen
 * @Description:
 * @Class: DataService
 * @Package com.ray.outside
 * @date 2020/7/12 19:12
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class DataService {

    @Autowired
    private RestTemplate restTemplate;

    public Result<Object>getDataNumber(String ipAddress) {
        log.info("请求地址ip:{}", ipAddress);
        try {
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.add("Content-Type", "application/json");
            HttpEntity<String> requestEntity = new HttpEntity<String>(null, requestHeaders);
            String result = restTemplate.postForObject("http://" + ipAddress + ":9999/erp/data", requestEntity, String.class);
            log.info("数据读取结果:{}", JSON.toJSONString(result));
            return JSON.parseObject(result,Result.class);
        } catch (Exception e) {
            log.error("数据读取异常", e);
            throw BusinessExceptionFactory.newException("数据读取异常");
        }
    }

}
