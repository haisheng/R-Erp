package com.ray.woodencreate.page;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 通用列表查询
 * @author ray
 *
 * @param <E>
 * @param <T>
 */
@Data
public class CommonPage<E, T extends Page> {
	@ApiModelProperty("通用查询文档")
	private String query;
	@ApiModelProperty("分页信息")
	@NotNull(message = "分页信息不能为空")
	private Page page = new Page();
	@ApiModelProperty("查询对象")
	@Valid
	private E entity;
}
