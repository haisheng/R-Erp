package com.ray.woodencreate.page;

import java.io.Serializable;
import java.util.List;

/**
 * <h1>系统分页对象</h1>
 * @author bo shen
 * @Description: 统分页对象
 * @Class: MsgCode
 * @Package com.toptoday.woodencreate.page
 * @date 2018/10/9 16:50
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class PageDTO<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	/** 页 **/
	private Integer page = 1;
	/** 分页大小 **/
	private Integer pageSize = 10;
	/** 总数 **/
	private long total;
	/*** 记录 **/
	private List<T> records;
	/*** 是否拥有下一页 **/
	private boolean hasNextPage;
	/***  起始的跳过条数 --主要用于手机端分页，没滑动刷新的时候都取原来已经刷的数据的后续数据**/
	private Integer limitStart;
	/**排序*/	
	private String orderBy;

	public PageDTO() {

	}

	public PageDTO(Integer page, Integer pageSize, String orderBy) {
		this.page = page;
      this.pageSize = pageSize;
      this.orderBy = orderBy;
	}

    /***
     * 获取分页开始页码
     * @return 开始页面
     */
    public int getStartLimit() {
        return page < 0 ? page : (page - 1) * pageSize;
    }
    /**
     * @param page     page
     * @param pageSize pageSize
	 * @return PageDTO
	 */
	public static <T> PageDTO<T> createPage(Integer page, Integer pageSize) {
		return createPage(page, pageSize, null);
    }

    /**
     * @param page     page
     * @param pageSize pageSize
     * @param orderBy  orderBy
	 * @return PageDTO
	 */
	public static <T> PageDTO<T> createPage(Integer page, Integer pageSize, String orderBy) {
		return new PageDTO<>(page, pageSize, orderBy);
	}


    /****
     * 添加总数
     * @param total total
	 * @return PageDTO<T>
	 */
	public PageDTO<T> addTotal(Integer total) {
		this.total = total;
		return this;
	}
	
	/****
     * 添加记录
     * @param records records
	 * @return PageDTO<T>
	 */
	public PageDTO<T> addRecords(List<T> records) {
		this.records = records;
		return this;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
		//手机端分页的时候
		if(limitStart == null) {
            this.hasNextPage = this.page * this.pageSize < total;
        }
    }

    public List<T> getRecords() {
        return records;
	}

	public void setRecords(List<T> records) {
		this.records = records;
	}

	public boolean isHasNextPage() {
		return hasNextPage;
	}

	public void setHasNextPage(boolean hasNextPage) {
		this.hasNextPage = hasNextPage;
	}

	public Integer getLimitStart() {
		return limitStart;
	}

	public void setLimitStart(Integer limitStart) {
		this.limitStart = limitStart;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	
}
