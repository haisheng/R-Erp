package com.ray.woodencreate.enums;
/**
 * 描述: 是否删除枚举
 * @author ray
 * @date 2017-06-12
 * @version 1.0
 */
public enum DeleteEnum {
    /**
     * "1","删除"
     */
    DELETE(1, "删除"),
    /**
     * "0","使用"
     */
    USE(0,"使用");
			
	  /**校验类型**/
	  private Integer value;
	  /**校验类型名称**/
	  private String name;

    DeleteEnum(Integer value, String name) {
        this.value = value;
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
