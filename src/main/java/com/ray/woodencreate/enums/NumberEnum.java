package com.ray.woodencreate.enums;
/**
 * 描述: 数量枚举
 * @author ray
 * @date 2017-06-12
 * @version 1.0
 */
public enum NumberEnum {
    /**
     * "1","1"
     */
    ONE(1, "1"),
    /**
     * "0","0"
     */
    ZERO(0,"0");

	  /**校验类型**/
	  private Integer value;
	  /**校验类型名称**/
	  private String name;

    NumberEnum(Integer value, String name) {
        this.value = value;
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
