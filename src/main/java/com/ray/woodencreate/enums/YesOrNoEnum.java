package com.ray.woodencreate.enums;
/**
 * 描述: 是否枚举
 * @author ray
 * @date 2017-06-12
 * @version 1.0
 */
public enum YesOrNoEnum {
    /**
     * "1","是"
     */
    YES(1, "是"),
    /**
     * "0","否"
     */
    NO(0,"否");

	  /**值**/
	  private Integer value;
	  /**名称**/
	  private String name;

    YesOrNoEnum(Integer value, String name) {
        this.value = value;
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
