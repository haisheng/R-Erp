package com.ray.woodencreate.mybatis;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 基础查询
 * @Class: BaseQuery
 * @Package com.toptoday.woodencreate.mybatis
 * @date 2019/12/22 9:39
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class BaseQuery {
    /***数据拥有者**/
    private Integer dataOwner;
    /***数据**/
    private Integer dataId;
    /***删除标记**/
    private String delFlag;
}
