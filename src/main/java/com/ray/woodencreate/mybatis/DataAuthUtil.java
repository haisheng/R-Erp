package com.ray.woodencreate.mybatis;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ray.common.BeanField;
import com.ray.system.enums.DataAuthEnum;
import com.ray.system.table.entity.SysApp;
import com.ray.system.table.entity.SysPosition;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.util.Assert;

/**
 * @author bo shen
 * @Description: 数据权限
 * @Class: DataAuthUtil
 * @Package com.ray.woodencreate.mybatis
 * @date 2020/5/27 15:42
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class DataAuthUtil {
    /**
     * 添加数据权限
     * @param updateWrapper
     * @param loginUser
     */
    public static void addDataAuth(UpdateWrapper<?> updateWrapper, LoginUser loginUser) {
        Assert.notNull(updateWrapper, MsgCodeConstant.Error.ERR88000003);
        Assert.notNull(loginUser, MsgCodeConstant.Error.ERR88000003);
        String dataAuth = loginUser.getDataAuth();
        updateWrapper.eq(BeanField.DbField.IS_DELETED, DeleteEnum.USE.getValue());
        if(StrUtil.equals(dataAuth, DataAuthEnum.GROUP_COMPANY.getValue())){
            updateWrapper.likeRight(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
        }else if(StrUtil.equals(dataAuth, DataAuthEnum.COMPANY.getValue())){
            updateWrapper.eq(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
        }else if(StrUtil.equals(dataAuth, DataAuthEnum.ORG.getValue())){
            updateWrapper.eq(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
            updateWrapper.likeRight(BeanField.DbField.ORGANIZATION_CODE,loginUser.getDeptCode());
        }else if(StrUtil.equals(dataAuth, DataAuthEnum.PERSON.getValue())){
            updateWrapper.eq(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
            updateWrapper.like(BeanField.DbField.OWNER_CODE,loginUser.getUserCode());
        }else if(StrUtil.equals(dataAuth, DataAuthEnum.ALL.getValue())){
            updateWrapper.eq(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
        }else {
            //没有权限不能查询数据
            updateWrapper.eq(BeanField.DbField.TENANT_CODE,"0");
        }
        updateWrapper.eq(BeanField.DbField.IS_DELETED, DeleteEnum.USE.getValue());

    }

    /**
     * 添加数据权限
     * @param queryWrapper
     * @param loginUser
     */
    public static void addDataAuth(QueryWrapper<?> queryWrapper, LoginUser loginUser) {
        Assert.notNull(queryWrapper, MsgCodeConstant.Error.ERR88000003);
        Assert.notNull(loginUser, MsgCodeConstant.Error.ERR88000003);
        String dataAuth = loginUser.getDataAuth();
        if(StrUtil.equals(dataAuth, DataAuthEnum.GROUP_COMPANY.getValue())){
            queryWrapper.likeRight(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
        }else if(StrUtil.equals(dataAuth, DataAuthEnum.COMPANY.getValue())){
            queryWrapper.eq(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
        }else if(StrUtil.equals(dataAuth, DataAuthEnum.ORG.getValue())){
            queryWrapper.eq(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
            queryWrapper.likeRight(BeanField.DbField.ORGANIZATION_CODE,loginUser.getDeptCode());
        }else if(StrUtil.equals(dataAuth, DataAuthEnum.PERSON.getValue())){
            queryWrapper.eq(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
            queryWrapper.like(BeanField.DbField.OWNER_CODE,loginUser.getUserCode());
        } else if(StrUtil.equals(dataAuth, DataAuthEnum.GROUP_COMPANY.getValue())){
            queryWrapper.likeRight(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
        }else if(StrUtil.equals(dataAuth, DataAuthEnum.ALL.getValue())){
            queryWrapper.eq(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
        }else {
            //没有权限不能查询数据
            queryWrapper.eq(BeanField.DbField.TENANT_CODE,"0");
        }
        queryWrapper.eq(BeanField.DbField.IS_DELETED, DeleteEnum.USE.getValue());
    }

    /**
     * 添加数据权限
     * @param updateWrapper
     * @param loginUser
     */
    public static void addCompanyDataAuth(UpdateWrapper<?> updateWrapper, LoginUser loginUser) {
        Assert.notNull(updateWrapper, MsgCodeConstant.Error.ERR88000003);
        Assert.notNull(loginUser, MsgCodeConstant.Error.ERR88000003);
        updateWrapper.eq(BeanField.DbField.IS_DELETED, DeleteEnum.USE.getValue());
        updateWrapper.eq(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
    }

    /**
     * 添加数据权限
     * @param queryWrapper
     * @param loginUser
     */
    public static void addComapnyDataAuth(QueryWrapper<?> queryWrapper, LoginUser loginUser) {
        Assert.notNull(queryWrapper, MsgCodeConstant.Error.ERR88000003);
        Assert.notNull(loginUser, MsgCodeConstant.Error.ERR88000003);
        queryWrapper.eq(BeanField.DbField.IS_DELETED, DeleteEnum.USE.getValue());
        queryWrapper.eq(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());

    }

    /**
     * 添加数据权限
     * @param updateWrapper
     * @param loginUser
     */
    public static void addComapnyDataAuth(UpdateWrapper<?> updateWrapper, LoginUser loginUser) {
        Assert.notNull(updateWrapper, MsgCodeConstant.Error.ERR88000003);
        Assert.notNull(loginUser, MsgCodeConstant.Error.ERR88000003);
        updateWrapper.eq(BeanField.DbField.IS_DELETED, DeleteEnum.USE.getValue());
        updateWrapper.eq(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
    }


    /**
     * 添加数据权限
     * @param queryWrapper
     * @param loginUser
     */
    public static void addUserDataAuth(QueryWrapper<?> queryWrapper, LoginUser loginUser) {
        Assert.notNull(queryWrapper, MsgCodeConstant.Error.ERR88000003);
        Assert.notNull(loginUser, MsgCodeConstant.Error.ERR88000003);
        queryWrapper.eq(BeanField.DbField.IS_DELETED, DeleteEnum.USE.getValue());
        queryWrapper.eq(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
        queryWrapper.eq(BeanField.DbField.OWNER_CODE,loginUser.getUserCode());
    }

    /**
     * 添加数据权限
     * @param updateWrapper
     * @param loginUser
     */
    public static void addUserDataAuth(UpdateWrapper<?> updateWrapper, LoginUser loginUser) {
        Assert.notNull(updateWrapper, MsgCodeConstant.Error.ERR88000003);
        Assert.notNull(loginUser, MsgCodeConstant.Error.ERR88000003);
        updateWrapper.eq(BeanField.DbField.IS_DELETED, DeleteEnum.USE.getValue());
        updateWrapper.eq(BeanField.DbField.TENANT_CODE,loginUser.getCompanyCode());
        updateWrapper.eq(BeanField.DbField.OWNER_CODE,loginUser.getUserCode());
    }

    /**
     * 不删除
     * @param queryWrapper
     */
    public static void notDelete(QueryWrapper<?> queryWrapper) {
        queryWrapper.eq(BeanField.DbField.IS_DELETED, DeleteEnum.USE.getValue());
    }

    /**
     * 不删除
     * @param updateWrapper
     */
    public static void notDelete(UpdateWrapper<?> updateWrapper) {
        updateWrapper.eq(BeanField.DbField.IS_DELETED, DeleteEnum.USE.getValue());
    }
}
