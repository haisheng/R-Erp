package com.ray.woodencreate.mybatis;

import cn.hutool.core.bean.BeanUtil;
import com.ray.common.BeanField;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;

/**
 * @author bo shen
 * @Description: 数据权限
 * @Class: DataAuthUtil
 * @Package com.ray.woodencreate.mybatis
 * @date 2020/5/27 15:42
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class DataOPUtil {

    public static void editUser(Object entity,LoginUser loginUser) {
        BeanUtil.setFieldValue(entity, BeanField.updateUserName, loginUser.getUserName());
        BeanUtil.setFieldValue(entity, BeanField.updateUserCode, loginUser.getUserCode());
    }

    public static void createUser(Object entity,LoginUser loginUser) {
        BeanUtil.setFieldValue(entity, BeanField.createUserName, loginUser.getUserName());
        BeanUtil.setFieldValue(entity, BeanField.createUserCode, loginUser.getUserCode());
        BeanUtil.setFieldValue(entity, BeanField.isDeleted, DeleteEnum.USE.getValue());
        BeanUtil.setFieldValue(entity, BeanField.tenantCode, loginUser.getCompanyCode());
        BeanUtil.setFieldValue(entity, BeanField.organizationCode, loginUser.getDeptCode());
        BeanUtil.setFieldValue(entity, BeanField.ownerCode, loginUser.getUserCode());
    }
}
