package com.ray.woodencreate.mybatis;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.mapping.SqlCommandType;

@Slf4j
public class MybatisParamsOpUser {

    public static void change(Object entity, SqlCommandType opType) {
        switch (opType){
            case INSERT:
                setUserForAdd(entity);
                break;
            case UPDATE:
                setUserForUpdate(entity);
                break;
            case SELECT:
                if(entity instanceof QueryWrapper){
                    // 权限处理
                    setUserForSelect((QueryWrapper)entity);
                }else  if( entity instanceof BaseQuery){
                // 权限处理
                setUserForSelect((BaseQuery)entity);
            }
                break;
             default:
                 break;
        }
    }

    /**
     * 给数据库对象设置数据
     * @param target 操作目标
     */
    protected static void setUserForAdd(Object target){


    }

    /**
     * 给数据库对象设置数据
     * @param target 操作目标
     */
    protected  static void setUserForUpdate(Object target){

    }


    /**
     * 给数据库对象设置数据
     */
    protected  static void setUserForSelect(BaseQuery baseQuery){


    }

    /**
     * 给数据库对象设置数据
     */
    protected  static void setUserForSelect(QueryWrapper<Object> queryWrapper){


    }
}
