package com.ray.woodencreate.util;

import lombok.extern.slf4j.Slf4j;

import java.security.MessageDigest;

/**
 * MD5加密
 * @author chenrg
 *
 */
@Slf4j
public class Md5Util {
	
	/**
	 * MD5加密
     * @param key key
     * @return String
     */
	public static String getMd5(String key){
        StringBuilder buffer = new StringBuilder();
        try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(key.getBytes("UTF-8"));
			for (byte b : digest.digest()) {
				buffer.append(String.format("%02x", b & 0xff));
			}
		} catch (Exception e) {
            log.error(e.getMessage());
        }
		return buffer.toString();
	}
}
