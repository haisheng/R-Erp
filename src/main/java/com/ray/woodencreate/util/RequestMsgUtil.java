package com.ray.woodencreate.util;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.servlet.http.HttpServletRequest;

/**
 * @author bo shen
 * @Description: 请求消息
 * @Class: RequestMsgUtil
 * @Package com.toptoday.woodencreate.util
 * @date 2018/10/11 9:50
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class RequestMsgUtil {

    /**记录一个消息ID**/
    private static ThreadLocal<RequestMsg> msgIds =  new ThreadLocal<>();

    public static RequestMsg getMsg(){
        return  msgIds.get();
    }

    public static void createMsg(HttpServletRequest request,String appName){
        msgIds.set(new RequestMsg(request.getMethod().toString(),IPUtil.getIpAddress(request),request.getRequestURL().toString(),appName));
    }

    @Data
    @AllArgsConstructor
    public static class RequestMsg {
        /**请求方法**/
        private String mothed;
        /**host**/
        private String host;
        /**请求地址**/
        private String url;

        private String appName;

    }

}
