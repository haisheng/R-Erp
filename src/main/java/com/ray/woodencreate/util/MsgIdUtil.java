package com.ray.woodencreate.util;

import java.util.UUID;

/**
 * @author bo shen
 * @Description: 记录msgid
 * @Class: MsgIdUtil
 * @Package com.toptoday.woodencreate.logs
 * @date 2018/10/10 18:22
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class MsgIdUtil {
    /**记录一个消息ID**/
    private static ThreadLocal<String> msgIds =  new ThreadLocal<>();

    public static String getMsgId(){
        return  msgIds.get();
    }

    public static void createMsgId(){
        msgIds.set(UUID.randomUUID().toString());
    }

}
