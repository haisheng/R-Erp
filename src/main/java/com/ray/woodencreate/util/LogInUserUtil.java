package com.ray.woodencreate.util;

import com.ray.woodencreate.beans.LoginUser;

/**
 * @author bo shen
 * @Description: 登录用户
 * @Class: LogInUserUtil
 * @Package com.toptoday.woodencreate.logs
 * @date 2018/10/10 18:22
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class LogInUserUtil {
    /**记录一个登录token**/
    private static ThreadLocal<LoginUser> tokens =  new ThreadLocal<>();

    public static void add(LoginUser user) {
         tokens.set(user);
    }

    public static LoginUser get() {
       return tokens.get();
    }
}
