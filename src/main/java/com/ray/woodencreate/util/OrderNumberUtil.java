package com.ray.woodencreate.util;

import org.springframework.util.Assert;

import java.util.Random;

public class OrderNumberUtil {

    /**
     * 获取一个随机的订单号
     * @param perfix
     * @return
     */
    public static  String getNumber(String perfix){
        Assert.notNull(perfix,"前缀不能为空");
        StringBuffer number = new StringBuffer(perfix);
        number.append(System.currentTimeMillis());
        Random ra =new Random();
        for(int i=0;i<6;i++){
            number.append(ra.nextInt(10));
        }
        return number.toString();
    }
}
