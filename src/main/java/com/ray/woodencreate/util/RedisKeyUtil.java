package com.ray.woodencreate.util;

public class RedisKeyUtil {

    public  static String getKey(Object value){
        return String.format("%s:%s",Md5Util.getMd5(value.toString()),value);
    }

    public  static String getKey(String perfix,String value){
        return String.format("%s:%s",perfix,value);
    }

    public static void main(String[] args) {
        System.out.println(RedisKeyUtil.getKey(111));
    }

}
