package com.ray.woodencreate.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import  org.springframework.context.ApplicationContext;

import java.lang.annotation.Annotation;
import java.util.Map;

/**
 * @author bo shen
 * @Description: applicationContext 全局工具类
 * @Class: SpringApplicationUtil
 * @Package com.toptoday.woodencreate.util
 * @date 2018/10/23 11:24
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Component
public class SpringApplicationUtil implements ApplicationContextAware{

    private static ApplicationContext applicationContext;

    public static <T> T getBean(String name,Class<T> tClass) {
        return (T)applicationContext.getBean(name);
    }

    public static <T> T getBean(Class<T> tClass) {
        return applicationContext.getBean(tClass);
    }

    public static Map<String, Object> getBeansWithAnnotation(Class<? extends Annotation> annotationType){
       return  applicationContext.getBeansWithAnnotation(annotationType);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringApplicationUtil.applicationContext = applicationContext;
    }
}
