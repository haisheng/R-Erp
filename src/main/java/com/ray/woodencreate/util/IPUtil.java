package com.ray.woodencreate.util;

import javax.servlet.http.HttpServletRequest;

/**
 * @author bo shen
 * @Description: IP工具类
 * @Class: IPUtil
 * @Package com.toptoday.woodencreate.util
 * @date 2018/10/11 10:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class IPUtil {
    /*** 
     *  描述：获取IP地址*
    */
    public static String getIpAddress(HttpServletRequest request){
        String ip = request.getHeader("x-forwarded-for");
        if(ip == null || ip.length() ==0 || "nuknown".equalsIgnoreCase(ip)){
            ip = request.getHeader("Proxy-Client-IP");
        }if(ip == null || ip.length() ==0 || "nuknown".equalsIgnoreCase(ip)){
            ip = request.getHeader("WL-Proxy-Client-IP");
        }if(ip == null || ip.length() ==0 || "nuknown".equalsIgnoreCase(ip)){
            ip = request.getRemoteAddr();
        }
        return ip;
    }

}
