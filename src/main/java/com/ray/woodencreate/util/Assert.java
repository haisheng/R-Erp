package com.ray.woodencreate.util;

import com.ray.woodencreate.exception.BusinessExceptionFactory;
import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.Map;

public class Assert {

    public static void state(boolean expression, String messageCode) {
        if (!expression) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
    }

    public static void isTrue(boolean expression, String messageCode) {
        if (!expression) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
    }


    public static void isNull(@Nullable Object object, String messageCode) {
        if (object != null) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
    }




    public static void notNull(@Nullable Object object, String messageCode) {
        if (object == null) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
    }



    public static void hasLength(@Nullable String text, String messageCode) {
        if (!StringUtils.hasLength(text)) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
    }



    public static void hasText(@Nullable String text, String messageCode) {
        if (!StringUtils.hasText(text)) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
    }




    public static void doesNotContain(@Nullable String textToSearch, String substring, String messageCode) {
        if (StringUtils.hasLength(textToSearch) && StringUtils.hasLength(substring) && textToSearch.contains(substring)) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
    }



    public static void notEmpty(@Nullable Object[] array, String messageCode) {
        if (ObjectUtils.isEmpty(array)) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
    }



    public static void noNullElements(@Nullable Object[] array, String messageCode) {
        if (array != null) {
            Object[] var2 = array;
            int var3 = array.length;

            for(int var4 = 0; var4 < var3; ++var4) {
                Object element = var2[var4];
                if (element == null) {
                    throw BusinessExceptionFactory.newException(messageCode);
                }
            }
        }

    }



    public static void notEmpty(@Nullable Collection<?> collection, String messageCode) {
        if (CollectionUtils.isEmpty(collection)) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
    }



    public static void notEmpty(@Nullable Map<?, ?> map, String messageCode) {
        if (CollectionUtils.isEmpty(map)) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
    }

}
