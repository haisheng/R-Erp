package com.ray.woodencreate.util;

import java.util.UUID;

/**
 * @author bo shen
 * @Description: token
 * @Class: TokenInfoUtil
 * @Package com.toptoday.woodencreate.logs
 * @date 2018/10/10 18:22
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class TokenInfoUtil {
    /**记录一个登录token**/
    private static ThreadLocal<String> tokens =  new ThreadLocal<>();

    public static void add(String token) {
         tokens.set(token);
    }

    public static String get() {
       return tokens.get();
    }
}
