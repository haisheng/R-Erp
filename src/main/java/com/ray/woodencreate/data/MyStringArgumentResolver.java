package com.ray.woodencreate.data;

import cn.hutool.core.util.StrUtil;
import io.lettuce.core.dynamic.support.MethodParameter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ValueConstants;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver;

/**
 * @author bo shen
 * @Description: MyStringArgumentResolver
 * @Class: MyStringArgumentResolver
 * @Package com.ray.woodencreate.data
 * @date 2020/6/12 22:47
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class MyStringArgumentResolver extends AbstractNamedValueMethodArgumentResolver {
    @Override
    protected NamedValueInfo createNamedValueInfo(org.springframework.core.MethodParameter parameter) {
        return new NamedValueInfo("", false, ValueConstants.DEFAULT_NONE);
    }

    @Nullable
    @Override
    protected Object resolveName(String name, org.springframework.core.MethodParameter parameter, NativeWebRequest request) throws Exception {
        String[] param = request.getParameterValues(name);
        if (param == null) {
            return null;
        }
        if (StringUtils.isEmpty(param[0])) {
            return null;
        }
        return param[0];
    }

    @Override
    public boolean supportsParameter(org.springframework.core.MethodParameter parameter) {
        return parameter.getParameterType().equals(String.class);
    }
}
