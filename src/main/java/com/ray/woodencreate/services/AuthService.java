package com.ray.woodencreate.services;

import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.result.Result;

import java.util.Set;

/**
 * @author bo shen
 * @Description: 权限服务
 * @Class: TestService
 * @Package com.toptoday.woodencreate.services
 * @date 2018/10/11 13:29
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */

public interface AuthService  {

    /**获取默认用户**/
    Result<LoginUser> defaultLoginUser();

    /**
     * 用户权限
     * @return
     */
    Result<Set<String>>  auths();
}
