package com.ray.woodencreate.services.remote;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * @author bo shen
 * @Description: TODO
 * @Class: FeginConfig
 * @Package com.toptoday.woodencreate.services.remote
 * @date 2018/11/20 15:41
 * @company
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Configuration
public class FeginConfig implements RequestInterceptor {

    private List<String> excludeName = new ArrayList<>();

    public FeginConfig(List<String> excludeName) {
        this.excludeName = excludeName;
    }

    public FeginConfig() {
    }


    @Override
    public void apply(RequestTemplate requestTemplate) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        Enumeration<String> headerNames = request.getHeaderNames();
        if (headerNames != null) {
            while (headerNames.hasMoreElements()) {
                String name = headerNames.nextElement();
                if(!excludeName.contains(name)){
                    String values = request.getHeader(name);
                    requestTemplate.header(name, values);
                }

            }
        }
    }
}
