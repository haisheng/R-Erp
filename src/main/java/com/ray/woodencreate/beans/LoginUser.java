package com.ray.woodencreate.beans;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.ray.system.enums.DataAuthEnum;
import com.ray.system.table.vo.company.CompanyVO;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author bo shen
 * @Description: 登录用户
 * @Class: LoginUser
 * @Package com.toptoday.woodencreate.beans
 * @date 2018/10/18 14:51
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class LoginUser {
    /**ID主键**/
    private Integer id;
    /**用户编码**/
    private String userCode;
    /**用户姓名**/
    private String userName;
    /**公司code**/
    private String companyCode;
    /**公司名称**/
    private String companyName;
    /**部门code**/
    private String deptCode;
    /**部门名称**/
    private String deptName;
    /**创建时间**/
    private LocalDateTime createDate = LocalDateTime.now();
    /**注册手机**/
    private String moblie;
    /**登录应用**/
    private String appCode;
    /**登录应用**/
    private String appKey;
    /**登录应用名称**/
    private String appName;
    /***职位**/
    private String positionCode;
    /***职位**/
    private String positionName;
    /**数据权限**/
    private String dataAuth;
    /**token**/
    private String token;
    /**属于公司**/
    private List<CompanyVO> companys;

    /**
     * 是否拥有所有数据权限
     * @return
     */
    public boolean isAllData(){
        return StrUtil.equals(dataAuth, DataAuthEnum.ALL.getValue());
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
