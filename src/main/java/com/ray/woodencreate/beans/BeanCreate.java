package com.ray.woodencreate.beans;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

/**
 * @author bo shen
 * @Description: 对象创建
 * @Class: BeanCreate
 * @Package com.toptoday.woodencreate.beans
 * @date 2018/11/9 16:38
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BeanCreate {
    public static <T> T newBean(Class<T> clazz){
        try {
            return clazz.newInstance();
        } catch (Exception e) {
            log.info("create bean error");
        }
        return null;
    }

    public static <T> T newBean(T t,Class<T> clazz){
        try {
            return ObjectUtils.isEmpty(t) ? newBean(clazz): t ;
        } catch (Exception e) {
            log.info("create bean error");
        }
        return null;
    }

}
