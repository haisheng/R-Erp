package com.ray.woodencreate.beans;

/**
 * @author bo shen
 * @Description: CommonValue
 * @Class: CommonValue
 * @Package com.toptoday.woodencreate.beans
 * @date 2018/12/6 11:18
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class CommonValue {
    public static String APP_NAME = "";
    public static String DEFAULT_ZERO = "0";
}
