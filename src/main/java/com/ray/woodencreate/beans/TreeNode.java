/*
 * Copyright 2015-2019 Evun Technology. 
 * 
 * This software is the confidential and proprietary information of
 * Evun Technology. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with evun.cn.
 */
package com.ray.woodencreate.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 树状结构对象的存储实现类
 *
 * @author yangw
 * @since 1.0.0
 */
public class TreeNode implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Long 	id;
	private String 	name;
    /**
     * 当前层级
     */
    private Integer level;
    /**
     * 是否叶子节点
     */
    private Boolean leaf = Boolean.TRUE;
    private String 	parentId;
	private Boolean isParent = true;
	private Object value;
    private List<TreeNode> children = new ArrayList<>();
    private TreeNode parent;
	public TreeNode(Long id, String name,Integer level, boolean leaf) {
		setId(id);
		setName(name);
		setLeaf(leaf);
		setLevel(level);
	}
	public TreeNode(Long id, String parentId,String name,Integer level, boolean leaf) {
		setId(id);
		setParentId(parentId);
		setName(name);
		setLeaf(leaf);
		setLevel(level);
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public Boolean getLeaf() {
		return leaf;
	}
	public void setLeaf(Boolean leaf) {
		this.leaf = leaf;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	
	public TreeNode getParent() {
		return parent;
	}
	public void setParent(TreeNode parent) {
		this.parent = parent;
	}
	public List<TreeNode> getChildren() {
		return children;
	}
	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}
	public Boolean getIsParent() {
		return isParent;
	}
	public void setIsParent(Boolean isParent) {
		this.isParent = isParent;
	}
	
	
}
