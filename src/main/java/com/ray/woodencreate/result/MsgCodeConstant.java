package com.ray.woodencreate.result;

/**
 * @author bo shen
 * @Description: 系统消息code常量配置
 * @Class: MsgCodeConstant
 * @Package com.toptoday.woodencreate.result
 * @date 2018/10/10 16:10
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class MsgCodeConstant {

    /***
     * 成功编码类
     */
    public static class Success {

        public final static  String SUC00000001 = "SUC00000001";
        public final static  String SUC00000002 = "SUC00000002";

    }

    /**
     * 错误编码类
     */
    public static class Error {
        public final static  String ERR00000000 = "ERR00000000";
        public final static  String ERR00000001 = "ERR00000001";
        public final static  String ERR00000002 = "ERR00000002";
        public final static  String ERR00000003 = "ERR00000003";
        public final static  String ERR00000004 = "ERR00000004";

        //异常相关
        public final static  String ERR88000001 = "ERR88000001";
        public final static  String ERR88000002 = "ERR88000002";
        public final static  String ERR88000003 = "ERR88000003";
        public final static  String ERR88000004 = "ERR88000004";
        public final static  String ERR88000005 = "ERR88000005";
        public final static  String ERR88000006 = "ERR88000006";
        public final static  String ERR88000007 = "ERR88000007";
        public final static  String ERR88000008 = "ERR88000008";
        public final static  String ERR88000009 = "ERR88000009";
        public final static  String ERR88000010 = "ERR88000010";
        public final static  String ERR88000011 = "ERR88000011";
        public final static  String ERR88000012 = "ERR88000012";
        public final static  String ERR88000013 = "ERR88000013";
        public final static  String ERR88000014 = "ERR88000014";
        //熔断相关
        public final static  String ERR99000001 = "ERR99000001";


    }

    /**
     * 模板常量类
     */
    public static class Template {
        public final  static String KEY = "YGBZDDMM";
        public final  static String TEMP00000001 = "TEMP00000001";
        public final  static String TEMP00000002 = "TEMP00000002";
        public final  static String TEMP00000003 = "TEMP00000003";
        public final  static String TEMP00000004 = "TEMP00000004";
        public final  static String TEMP00000005 = "TEMP00000005";
        public final  static String TEMP00000006 = "TEMP00000006";
        public final  static String TEMP00000007 = "TEMP00000007";
        public final  static String TEMP00000008 = "TEMP00000008";

    }


}
