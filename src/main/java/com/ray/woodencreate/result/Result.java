package com.ray.woodencreate.result;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bo shen
 * @Description: 方法请求结果对象
 * @Class: Result
 * @Package com.toptoday.woodencreate.result
 * @date 2018/10/9 14:57
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@NoArgsConstructor
public class Result<T> {
    /**是否调用成功*/
    private Boolean success;
    /***成功或错误编码**/
    private String code;
    /**成功或错误信息**/
    private String msg;
    /**消息结果*/
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;

    public Result(Boolean success, String code, String msg, T data) {
        this.success = success;
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
