package com.ray.woodencreate.result;

import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: 结果集对象
 * @Class: ResultFactory
 * @Package com.toptoday.woodencreate.result
 * @date 2018/10/9 15:05
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class ResultFactory {

    /**
     * 创建一个操作成功的结果对象
     * @param successCode 正确编码
     * @param data 数据
     * @param <T>  泛型对象
     * @return
     */
    public static  <T>  Result<T> createSuccessResult(String successCode,T data){
        return new Result<T>(true,successCode,MsgCodeUtil.getMsgCodeMessage(successCode),data);
    }




    /**
     * 创建一个操作成功的结果对象
     * @param successCode 正确编码
     * @param <T>  泛型对象
     * @return
     */
    public static  <T>  Result<T> createSuccessResult(String successCode){
        return new Result<T>(true,successCode,MsgCodeUtil.getMsgCodeMessage(successCode),null);
    }


    /**
     * 创建一个异常的结果对象
     * @param errorCode 正确编码
     * @param data 数据
     * @param <T>  泛型对象
     * @return
     */
    public static  <T>  Result<T> createErrorResult(String errorCode,T data){
        return new Result<T>(false,errorCode,MsgCodeUtil.getMsgCodeMessage(errorCode),data);
    }

    /**
     * 创建一个异常的结果对象
     * @param errorCode 正确编码
     * @param <T>  泛型对象
     * @return
     */
    public static  <T>  Result<T> createErrorResult(String errorCode){
        return new Result<T>(false,errorCode,MsgCodeUtil.getMsgCodeMessage(errorCode),null);
    }

    /**
     * 创建一个异常的结果对象
     * @param errorCode 正确编码
     * @param <T>  泛型对象
     * @return
     */
    public static  <T>  Result<T> createErrorResult(String errorCode,String ...args){
        return new Result<T>(false,errorCode,String.format(MsgCodeUtil.getMsgCodeMessage(errorCode),args),null);
    }

}
