package com.ray.woodencreate.annotation;

import java.lang.annotation.*;

/**
 * @author bo shen
 * @Description: 认证
 * @Class: Auth
 * @Package com.toptoday.woodencreate.annotation
 * @date 2019/12/22 9:57
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Auth {
}
