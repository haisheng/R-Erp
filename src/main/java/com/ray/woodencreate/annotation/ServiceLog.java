package com.ray.woodencreate.annotation;

import java.lang.annotation.*;

/**
 * @author bo shen
 * @Description: 服务层日志
 * @Class: NoAuth
 * @Package com.toptoday.woodencreate.annotation
 * @date 2018/10/23 17:28
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ServiceLog {

    String name() default "";
}
