package com.ray.woodencreate.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author bo shen
 * @Description: 性能切面
 * @Class: Performance
 * @Package com.toptoday.woodencreate.aspect
 * @date 2018/10/23 9:52
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Performance {
}
