package com.ray.woodencreate.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author bo shen
 * @Description: 启动数据库配置
 * @Class: EnableDataSourceConfig
 * @Package com.toptoday.woodencreate.config
 * @date 2018/10/22 17:33
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({DataSourceAutoConfiguration.class, DruidDataSourceAutoConfigure.class})
public @interface EnableDataSourceConfig {
}
