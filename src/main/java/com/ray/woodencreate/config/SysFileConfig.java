package com.ray.woodencreate.config;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.Duration;

/**
 * @author bo shen
 * @Description: redis配置
 * @Class: RedisConfig
 * @Package com.toptoday.woodencreate.config
 * @date 2018/11/5 8:57
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Configuration
@Data
@Slf4j
public class SysFileConfig implements ApplicationContextAware{

    @Value("${sys.file.dir}")
    private String dir;
    @Value("${sys.file.image.dir}")
    private String imageDir;
    @Value("${sys.file.prefix}")
    private String prefix;
    @Value("${sys.html.path}")
    private String path;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if(StrUtil.isNotBlank(dir)){
            File root = new File(dir);
            if(!root.exists()){
                root.mkdirs();
            }
        }
        if(StrUtil.isNotBlank(imageDir)){
            File root = new File(imageDir);
            if(!root.exists()){
                root.mkdirs();
            }
        }

    }
}