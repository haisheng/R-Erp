package com.ray.woodencreate.config;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author bo shen
 * @Description: 启动数据库配置
 * @Class: EnableDataSourceConfig
 * @Package com.toptoday.woodencreate.config
 * @date 2018/10/22 17:33
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({SwaggerConfig.class})
public @interface EnableSwaggerConfig {
}
