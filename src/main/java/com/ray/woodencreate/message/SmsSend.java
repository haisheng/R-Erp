package com.ray.woodencreate.message;

import com.ray.woodencreate.result.Result;

/**
 * @author bo shen
 * @Description: 短信发送接口
 * @Class: SmsSend
 * @Package com.toptoday.woodencreate.message
 * @date 2018/11/5 9:50
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface SmsSend {

    /**
     * 短信发送接口
     * @param phone 手机号码
     * @param content 发送内容
     * @return
     */
    Result<String> send(String phone,String content);
}
