package com.ray.woodencreate.builder;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * @author bo shen
 * @Description: 默认构造器添加着
 * @Class: AbsstractAppend
 * @Package com.toptoday.woodencreate.builder
 * @date 2018/10/9 15:51
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public abstract class AbsstractAppend<T,O> implements  Append<T> {
    /**
     * 统一字段操作
     * @param target 操作对象
     * @param fieldName 操作字段
     * @param value 操作值
     * @return
     */
    private void addValue(O target ,String fieldName, Object value) throws IllegalAccessException ,InvocationTargetException {
        if(log.isDebugEnabled()){
            log.debug("执行addValue方法,参数:{},{},{}",target,fieldName,value);
        }
        Assert.notNull(target,"params target is null");
        Assert.hasText(fieldName,"field target is null");
        Field field =  ReflectionUtils.findField(target.getClass(),fieldName);
        Assert.notNull(field,"method not exist");
        field.setAccessible(true);
        field.set(target,value);
    }

    @Override
    public T append(String field,Object value) {
        try {
            this.addValue(getTarget(),field,value);
        }catch ( Exception e){
           log.error(e.getMessage());
        }
        return (T)this;
    }

    /**
     * 获取当前操作对象
     * @return
     */
   public  O getTarget(){
       return  null;
   }


}
