package com.ray.woodencreate.logs;

import com.alibaba.fastjson.JSON;

/**
 * @author bo shen
 * @Description: 方法请求参数日志构造着
 * @Class: MothedParamsLogBuilder
 * @Package com.toptoday.woodencreate.logs
 * @date 2018/10/10 16:33
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class MothedParamsLogBuilder extends  SystemLogBuilder {

    private final static String TEMPLATE = "执行方法[%s]接收参数为:%s";

    public MothedParamsLogBuilder(String mothedName,Object ... params) {
        super();
        //String buildLog = buildLog(params);
        this.appendMsg(String.format(TEMPLATE,mothedName, JSON.toJSON(params)));
    }

    /**
     * 组装消息信息
     * @param params
     * @return
     */
    private String buildLog(Object... params) {
        StringBuffer log = new StringBuffer(TEMPLATE);
        for(Object param :params){
            log.append(Format.findFormatByClass(param.getClass()).getCode());
        }
        return log.toString();
    }


    /**
     * 支持转换的类型
     */
    public  enum  Format {
        //空
        NULL("",null,"默认空"),
        //字符串
        STRING("%s",String.class,"字符串");

        private String code;

        private Class<?> clazz;
        /**描述**/
        private String desc;

        Format(String code, Class<?> clazz, String desc) {
            this.code = code;
            this.clazz = clazz;
            this.desc = desc;
        }
        
        /**
         * 通过类型获取对应的
         * @param clazz
         * @return
         */
        public static Format findFormatByClass(Class<?> clazz){
            for(Format f : Format.values()){
                if(f.clazz == clazz){
                    return  f;
                }
            }
            return NULL;
        }

        public String getCode() {
            return code;
        }
    }

}
