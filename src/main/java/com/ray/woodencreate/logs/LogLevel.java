package com.ray.woodencreate.logs;

/**
 * @author bo shen
 * @Description: 日志等级
 * @Class: LogLevel
 * @Package com.toptoday.woodencreate.logs
 * @date 2018/10/10 15:54
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public enum LogLevel {
    // 提示
    TIPS("TIPS","提示"),
    // 业务
    BUSINESS("BUSINESS","业务"),
    // 性能
    PERFORMANCE("PERFORMANCE","性能"),
    // 请求
    REQUEST("REQUEST","请求"),
    // 异常
    ERROR("ERROR","异常")
    ;

    LogLevel(String levelCode, String levelName) {
        this.levelCode = levelCode;
        this.levelName = levelName;
    }
    /**日志等级**/
    private String levelCode;
    /**日志名称/描述**/
    private String levelName;

    public String getLevelCode() {
        return levelCode;
    }
}
