package com.ray.woodencreate.logs;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author bo shen
 * @Description: 系统统日志
 * @Class: SystemLog
 * @Package com.toptoday.woodencreate.logs
 * @date 2018/10/9 15:21
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class SystemLog {

   /**uuid*/
   private String  sid;
    /**业务链路ID**/
   private String msgId;

   /**log生成时间**/
   @JsonFormat(pattern="YYYY-MM-DD HH:mm:ss")
   private Date time;

   /**log等级**/
   private String  level;

   /**信息简要**/
   private String  msg;

   /**项目名称*/
   private String appName;

   /**请求方法**/
   private String mothed;

    /**host**/
    private String host;

    /**请求地址**/
    private String url;

    /**状态码**/
    private String status;

    /**执行毫秒**/
    private Long elapsed;

    /**错误的完整信息**/
    private String error;

    /**错误发生类**/
    private String errorClass;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

}
