package com.ray.woodencreate.logs;

import com.ray.woodencreate.builder.AbsstractAppend;
import com.ray.woodencreate.builder.Builder;
import com.ray.woodencreate.result.MsgCodeUtil;
import com.ray.woodencreate.util.MsgIdUtil;
import com.ray.woodencreate.util.RequestMsgUtil;
import org.springframework.util.ObjectUtils;

import java.util.UUID;

/**
 * @author bo shen
 * @Description: 系统日志构建器
 * @Class: SystemLogBuilder
 * @Package com.toptoday.woodencreate.logs
 * @date 2018/10/9 15:37
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class SystemLogBuilder extends AbsstractAppend<SystemLogBuilder,SystemLog> implements Builder<SystemLog> {
    /**构造对象**/
    private SystemLog systemLog;

    public SystemLogBuilder() {
        /***创建一个默认对象*/
        this.systemLog = new SystemLog();
        //创建一个UUID
        this.createSid();
        this.createMsgId();
        this.appendLevelTips();
    }

    /***
     * 创建UUID
     * @return
     */
    private SystemLogBuilder createSid(){
        this.systemLog.setSid(UUID.randomUUID().toString());
        return this;
    }

    /***
     * 创建网络对象
     * @return
     */
    private SystemLogBuilder createRequestMsg(){
        //获取网络信息
        RequestMsgUtil.RequestMsg  msg = RequestMsgUtil.getMsg();
        //异常空排除
        if(!ObjectUtils.isEmpty(msg)){
            this.systemLog.setHost(msg.getHost());
            this.systemLog.setMothed(msg.getMothed());
            this.systemLog.setUrl(msg.getUrl());
            this.systemLog.setAppName(msg.getAppName());
        }
        return this;
    }

    /***
     * 创建消息id
     * @return
     */
    private SystemLogBuilder createMsgId(){
        this.systemLog.setMsgId(MsgIdUtil.getMsgId());
        return this;
    }
     /**************日志等级*******************/
    /***
     * 添加一个提示等级的日志
      * @return
     */
    public SystemLogBuilder appendLevelTips(){
        this.systemLog.setLevel(LogLevel.TIPS.getLevelCode());
        return this;
    }

    /***
     * 添加一个业务等级的日志
     * @return
     */
    public SystemLogBuilder appendLevelBusiness(){
        this.systemLog.setLevel(LogLevel.BUSINESS.getLevelCode());
        return this;
    }

    /***
     * 添加一个性能等级的日志
     * @param time 耗时
     * @return
     */
    public SystemLogBuilder appendLevelPerformance(long time){
        this.systemLog.setLevel(LogLevel.PERFORMANCE.getLevelCode());
        this.systemLog.setElapsed(time);
        return this;
    }

    /***
     * 添加一个请求等级的日志
     * @return
     */
    public SystemLogBuilder appendLevelRequest(){
        this.systemLog.setLevel(LogLevel.REQUEST.getLevelCode());
        this.createRequestMsg();
        return this;
    }
    /***
     * 添加一个异常等级的日志
     * @return
     */
    public SystemLogBuilder appendLevelError(String errorMsg,String errorClass){
        this.systemLog.setLevel(LogLevel.ERROR.getLevelCode());
        this.systemLog.setError(errorMsg);
        this.systemLog.setErrorClass(errorClass);
        return this;
    }

    /***
     * 添加一个异常等级的日志
     * @return
     */
    public SystemLogBuilder appendLevelError(String errorMsg){
        this.systemLog.setLevel(LogLevel.ERROR.getLevelCode());
        this.systemLog.setError(errorMsg);
        return this;
    }

    /***
     * 添加主体消息信息
     * @return
     */
    public SystemLogBuilder appendMsg(String msg){
        this.systemLog.setMsg(msg);
        return this;
    }

    /***
     * 添加主体消息信息
     * @return
     */
    public SystemLogBuilder appendMsgCode(String msgCode, String ...args){
        this.systemLog.setMsg(String.format(MsgCodeUtil.getMsgCodeMessage(msgCode),args));
        return this;
    }

    @Override
    public SystemLog bulid() {
        return systemLog;
    }

    @Override
    public String bulidString() {
        return systemLog == null ? null: systemLog.toString() ;
    }

    @Override
    public SystemLog getTarget() {
        return systemLog;
    }
}
