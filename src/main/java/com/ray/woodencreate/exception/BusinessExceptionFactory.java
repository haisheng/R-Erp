package com.ray.woodencreate.exception;

import com.ray.woodencreate.logs.SystemLogBuilder;
import com.ray.woodencreate.result.MsgCodeUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: 系统业务日志处理
 * @Class: BusinessExceptionFactory
 * @Package com.toptoday.woodencreate.exception
 * @date 2018/10/10 15:38
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BusinessExceptionFactory {

    /**
     * 创建一个业务异常
     * @param errorCode 异常代码
     * @return
     */
  public static  BusinessException newException(String errorCode){
      if(MsgCodeUtil.notExist(errorCode)){
          log.info(new SystemLogBuilder().appendMsg(MsgCodeUtil.getMsgCodeMessage(errorCode)).bulidString());
      }
      return  new BusinessException(errorCode);
  }

    /**
     * 创建一个业务异常
     * @param errorCode 异常代码
     * @return
     */
    public static  BusinessException newException(String errorCode,String ... args){
        if(MsgCodeUtil.notExist(errorCode)){
            log.info(new SystemLogBuilder().appendMsgCode(errorCode,args).bulidString());
        }
        return  new BusinessException(errorCode,args);
    }
}
