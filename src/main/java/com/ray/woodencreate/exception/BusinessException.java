package com.ray.woodencreate.exception;

import com.ray.woodencreate.result.MsgCodeUtil;

/**
 * @author bo shen
 * @Description: 系统业务异常
 * @Class: BusinessException
 * @Package com.toptoday.woodencreate.exception
 * @date 2018/10/9 16:44
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class BusinessException extends RuntimeException{

    /**异常编号*/
    private String errorCode;

    private String[] args;

    public BusinessException(String errorCode) {
        super(MsgCodeUtil.getMsgCodeMessage(errorCode));
        this.errorCode = errorCode;
    }

    public BusinessException(String errorCode,String... args) {
        super(MsgCodeUtil.getMsgCodeMessage(errorCode));
        this.errorCode = errorCode;
        this.args = args;
    }


    public String getErrorCode() {
        return errorCode;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }
}
