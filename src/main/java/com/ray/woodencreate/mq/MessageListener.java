package com.ray.woodencreate.mq;


import com.ray.woodencreate.logs.SystemLogBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

/**
 *
 */
@Slf4j
public class MessageListener implements MessageListenerConcurrently {

  private MessageDispatch dispatch;

    public MessageListener(MessageDispatch dispatch) {
        this.dispatch = dispatch;
    }

    @Override
    public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
        // 设置自动提交
        for (MessageExt msg : msgs){
            boolean result = dispatch.dispatch(msg);
            if (!result){
                log.info(new SystemLogBuilder().appendLevelTips().appendMsg("消息消费失败-返回：false").bulidString());
                return ConsumeConcurrentlyStatus.RECONSUME_LATER;
            }
        }
        log.info(new SystemLogBuilder().appendLevelTips().appendMsg("消息消费成功-返回：true").bulidString());
        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
    }

}