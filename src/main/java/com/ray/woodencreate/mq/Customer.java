package com.ray.woodencreate.mq;

import com.ray.woodencreate.result.Result;
import org.apache.rocketmq.common.message.MessageExt;

/**
 * @author bo shen
 * @Description: MQ消费接口
 * @Class: Customer
 * @Package com.toptoday.woodencreate.mq
 * @date 2018/11/27 8:56
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface Customer {
    /**
     * 消费信息接口
     * @param message
     * @return
     */
    public Result<Boolean> sendMessage(MessageExt message);
}
