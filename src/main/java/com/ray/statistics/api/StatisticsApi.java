package com.ray.statistics.api;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.business.enums.BusinessTypeEnum;
import com.ray.business.enums.OrderStatusEnum;
import com.ray.business.service.ProdBusinessService;
import com.ray.business.service.ProdStepService;
import com.ray.business.table.dto.BusinessQuantityDTO;
import com.ray.business.table.dto.GroupBusinessDTO;
import com.ray.business.table.entity.ProdStep;
import com.ray.common.CountParams;
import com.ray.statistics.vo.BusinessCountVO;
import com.ray.statistics.vo.BusinessQuantityVO;
import com.ray.statistics.vo.WmsOrderCountVO;
import com.ray.util.DateUtils;
import com.ray.wms.service.WmsOrderInService;
import com.ray.wms.service.WmsOrderOutService;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author bo shen
 * @Description: 统计
 * @Class: StatisticsApi
 * @Package com.ray.statistics
 * @date 2020/6/15 8:57
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class StatisticsApi {


    @Autowired
    private ProdBusinessService prodBusinessService;
    @Autowired
    private ProdStepService prodStepService;
    @Autowired
    private WmsOrderInService wmsOrderInService;
    @Autowired
    private WmsOrderOutService wmsOrderOutService;

    /**
     * 今日新增分组
     *
     * @return
     */
    public Result<List<BusinessCountVO>> todayGroupBusiness() {
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        List<BusinessCountVO> lists = new ArrayList<>();
        CountParams countParams = new CountParams();
        countParams.setStartTime(DateUtils.getStartTime(new Date()));
        countParams.setEndTime(DateUtils.getEndTime(new Date()));
        Map<String, BusinessCountVO> countVOMap = new HashMap<>();
        countParams.setOrderStatus(OrderStatusEnum.FINISH.getValue());
        List<GroupBusinessDTO> groupBusinessDTOS = prodBusinessService.groupBusiness(countParams, loginUser);
        if (ObjectUtil.isNotEmpty(groupBusinessDTOS)) {
            groupBusinessDTOS.forEach(groupBusinessDTO -> {
                BusinessCountVO businessCountVO = countVOMap.get(groupBusinessDTO.getStepCode());
                if (ObjectUtil.isNull(businessCountVO)) {
                    businessCountVO = new BusinessCountVO();
                    businessCountVO.setStepCode(groupBusinessDTO.getStepCode());
                    //查询步骤
                    ProdStep prodStep = prodStepService.queryStepByStepCode(groupBusinessDTO.getStepCode(), loginUser);
                    if (ObjectUtil.isNotNull(prodStep)) {
                        businessCountVO.setStepName(prodStep.getStepName());
                    }
                }
                if (StrUtil.equals(groupBusinessDTO.getBusinessType(), BusinessTypeEnum.IN.getValue())) {
                    businessCountVO.setInCount(groupBusinessDTO.getQuantity());
                } else {
                    businessCountVO.setOutCount(groupBusinessDTO.getQuantity());
                }
                countVOMap.put(groupBusinessDTO.getStepCode(), businessCountVO);
            });
            //数据转换
            lists = new ArrayList<>(countVOMap.values());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, lists);
    }


    /**
     * 统计数量
     *
     * @return
     */
    public Result<List<BusinessQuantityVO>> countBusinessQuantity(CountParams countParams) {
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        countParams.setOrderStatus(OrderStatusEnum.FINISH.getValue());
        List<BusinessQuantityVO> lists = new ArrayList<>();
        List<BusinessQuantityDTO> businessQuantityDTOS = prodBusinessService.businessQuantity(countParams, loginUser);
        Map<String, BusinessQuantityVO> countVOMap = new HashMap<>();
        if (ObjectUtil.isNotEmpty(businessQuantityDTOS)) {
            businessQuantityDTOS.forEach(businessQuantityDTO -> {
                BusinessQuantityVO businessQuantityVO = countVOMap.get(businessQuantityDTO.getStepCode());
                if (ObjectUtil.isNull(businessQuantityVO)) {
                    businessQuantityVO = new BusinessQuantityVO();
                    businessQuantityVO.setStepCode(businessQuantityDTO.getStepCode());
                    //查询步骤
                    ProdStep prodStep = prodStepService.queryStepByStepCode(businessQuantityDTO.getStepCode(), loginUser);
                    if (ObjectUtil.isNotNull(prodStep)) {
                        businessQuantityVO.setStepName(prodStep.getStepName());
                    }
                }
                if (StrUtil.equals(businessQuantityDTO.getBusinessType(), BusinessTypeEnum.IN.getValue())) {
                    businessQuantityVO.setInCount(businessQuantityDTO.getQuantity());
                } else {
                    businessQuantityVO.setOutCount(businessQuantityDTO.getQuantity());
                }
                countVOMap.put(businessQuantityDTO.getStepCode(), businessQuantityVO);
            });
            //数据转换
            lists = new ArrayList<>(countVOMap.values());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, lists);
    }


    /**
     * 今日新增分组
     *
     * @return
     */
    public Result<List<WmsOrderCountVO>> todayWmsOrder() {
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        List<WmsOrderCountVO> lists = new ArrayList<>();
        CountParams countParams = new CountParams();
        countParams.setStartTime(DateUtils.getStartTime(new Date()));
        countParams.setEndTime(DateUtils.getEndTime(new Date()));
        Integer inCount = wmsOrderInService.countOrderIn(countParams, loginUser);
        Integer outCount = wmsOrderOutService.countOrderOut(countParams, loginUser);
        lists.add(new WmsOrderCountVO("入库订单", inCount));
        lists.add(new WmsOrderCountVO("出库订单", outCount));
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, lists);
    }


}
