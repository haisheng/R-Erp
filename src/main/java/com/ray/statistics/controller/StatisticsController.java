package com.ray.statistics.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.common.CountParams;
import com.ray.statistics.api.StatisticsApi;
import com.ray.statistics.vo.BusinessCountVO;
import com.ray.statistics.vo.BusinessQuantityVO;
import com.ray.statistics.vo.WmsOrderCountVO;
import com.ray.util.DateUtils;
import com.ray.wms.api.StockApi;
import com.ray.wms.table.params.order.StockQueryParams;
import com.ray.wms.table.vo.order.GoodsVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 系统基础服务-库存管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/statistics")
@Api("报表服务")
@Validated
public class StatisticsController {

    @Autowired
    private StatisticsApi statisticsApi;


    @ApiOperation("今日新增-业务单统计")
    @PostMapping(value = "/todayGroupBusiness", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BusinessCountVO>> todayGroupBusiness() {
        return statisticsApi.todayGroupBusiness();
    }

    @ApiOperation("今日新增-出入库订单统计")
    @PostMapping(value = "/todayWmsOrder", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<WmsOrderCountVO>> todayWmsOrder() {
        return statisticsApi.todayWmsOrder();
    }

    @ApiOperation("今日新增-业务单总量")
    @PostMapping(value = "/todayBusinessQuantity", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BusinessQuantityVO>> todayBusinessQuantity(@RequestBody @Valid CountParams countParams) {
        countParams.setStartTime(DateUtils.getStartTime(new Date()));
        countParams.setEndTime(DateUtils.getEndTime(new Date()));
        return statisticsApi.countBusinessQuantity(countParams);
    }

    @ApiOperation("按月新增-业务单总量")
    @PostMapping(value = "/monthBusinessQuantity", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<BusinessQuantityVO>> monthBusinessQuantity(@RequestBody @Valid CountParams countParams) {
        countParams.setStartTime(DateUtils.getMonthStartTime(new Date()));
        countParams.setEndTime(DateUtils.getMonthEndTime(new Date()));
        return statisticsApi.countBusinessQuantity(countParams);
    }

}
