package com.ray.statistics.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bo shen
 * @Description: 出入库单统计
 * @Class: BusinessCountVO
 * @Package com.ray.statistics.vo
 * @date 2020/6/15 9:59
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WmsOrderCountVO {

    private String name;

    private Integer count;


}
