package com.ray;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ray.export.spring.EnableImportExportAspect;
import com.ray.magicBlock.BlockConfig;
import com.ray.magicBlock.spring.EnableBlock;
import com.ray.woodencreate.config.EnableDataSourceConfig;
import com.ray.woodencreate.config.EnableMvcConfig;
import com.ray.woodencreate.config.EnableSwaggerConfig;
import com.ray.woodencreate.config.MyBatisConfig;
import com.ray.woodencreate.mybatis.AutoMapperScan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;


/**
 * @author bo shen
 * @Description: 通用配置
 * @Class: WoodenCrateApplication
 * @Package com.toptoday.woodencreate
 * @date 2018/10/22 17:00
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@EnableSwaggerConfig
@EnableMvcConfig
@EnableDataSourceConfig
@EnableCircuitBreaker
@EnableFeignClients
@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = true)
@AutoMapperScan("${mybatis.mapperScan}")
@EnableBlock
@Slf4j
@EnableImportExportAspect
public class ErpApplication extends SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication.run(ErpApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		// 注意这里要指向原先用main方法执行的Application启动类
		log.info("ERP 启动中...");
		return builder.sources(ErpApplication.class);
	}


	/**
	 * 运行请求的MediaType
	 * @param objectMapper
	 * @return
	 */
	@Bean
	public HttpMessageConverter httpMessageConverter(ObjectMapper objectMapper){
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();
		supportedMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
		supportedMediaTypes.add(new MediaType(MediaType.APPLICATION_FORM_URLENCODED, Charset.forName("utf-8")));
		supportedMediaTypes.add(new MediaType(MediaType.TEXT_HTML, Charset.forName("utf-8")));
		supportedMediaTypes.add(MediaType.TEXT_PLAIN);
		converter.setSupportedMediaTypes(supportedMediaTypes);
		converter.setObjectMapper(objectMapper);
		return converter;
	}

	@Bean
	public BlockConfig blockConfig(){
		BlockConfig config = new BlockConfig();
		config.setPackages("com.ray");
		return  config;
	}


	/**
	 * 分页插件
	 *
	 * @return
	 */
	@Bean
	public PaginationInterceptor paginationInterceptor() {
		return new PaginationInterceptor();
	}
}
