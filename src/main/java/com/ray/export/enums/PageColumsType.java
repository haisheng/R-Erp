package com.ray.export.enums;

/**
 * @author bo shen
 * @Description: 页面模板配置
 * @Class: PageColumsType
 * @Package com.ray.enums
 * @date 2019/12/3 13:38
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public enum PageColumsType {
    NAME("按名称","name"),
    INDEX("按顺序","index");


    /**名称**/
    private String name;
    /**值***/
    private String value;

    PageColumsType(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
