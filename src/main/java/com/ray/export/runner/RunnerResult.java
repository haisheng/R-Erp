package com.ray.export.runner;

import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.result.SuccessResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;

/**
 * @author bo shen
 * @Description: 策略链条
 * @Class: RunnerChain
 * @Package com.ray.strategy
 * @date 2019/11/21 14:09
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RunnerResult {
   /***执行者**/
   private Class<?> runType;
   /**上级内容**/
   private RunnerResult per;
   /**数据**/
   private DataModel dataModel;
   /***最终结果**/
   private SuccessResult result;
   /***异步状态*/
   private boolean asynchronou = false;

   public RunnerResult(Class<?> runType,DataModel dataModel) {
      this.runType = runType;
      this.dataModel = dataModel;
   }

   public RunnerResult(Class<?> runType,DataModel dataModel, boolean asynchronou) {
      this.runType = runType;
      this.dataModel = dataModel;
      this.asynchronou = asynchronou;
   }

   public RunnerResult(Class<?> runType,DataModel dataModel, boolean asynchronou,SuccessResult result) {
      this.runType = runType;
      this.dataModel = dataModel;
      this.asynchronou = asynchronou;
      this.result = result;
   }

   public RunnerResult(Class<?> runType,DataModel dataModel, SuccessResult result) {
      this.runType = runType;
      this.dataModel = dataModel;
      this.result = result;
   }
   public RunnerResult(Class<?> runType, SuccessResult result) {
      this.runType = runType;
   }

   public boolean getStatus() {
      if(ObjectUtils.isEmpty(result)){
         return  false;
      }
      return result.getStatus();
   }
}
