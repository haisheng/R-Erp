package com.ray.export.runner.exports;

import com.ray.export.empty.Empty;
import com.ray.export.runner.Runner;
import com.ray.export.runner.RunnerResult;
import com.ray.export.spring.ApplicationContextUtil;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.exports.AsyncVerification;
import com.ray.export.strategy.exports.AsyncVerificationResult;
import com.ray.export.strategy.file.FileVerification;
import com.ray.export.template.ExportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

/**
 * @author jumWang
 * @Description: 异步校验执行者
 * @Class: AsyncVerificationRunner
 * @Package com.ray.runner.exports
 * @date 2019/11/27 13:32
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class AsyncVerificationRunner implements Runner<ExportTemplate> {

    @Override
    public RunnerResult doRunner(ExportTemplate annotation, DataModel dataModel, RunnerResult perResult) {
        log.info("执行异步校验操作");
        Class<?> asyncVerificationClass = annotation.asyncVerification();
        if (asyncVerificationClass == Empty.class) {
            return new RunnerResult(Empty.class, dataModel,dataModel.getBeforeResult());
        }
        //判读配置是否符合规则
        if (!AsyncVerification.class.isAssignableFrom(asyncVerificationClass)) {
            log.error("异步校验操作配置错误");
            throw new RuntimeException("异步校验操作配置错误");
        }
        //获取执行对象
        AsyncVerification asyncVerification = (AsyncVerification) ApplicationContextUtil.getApplicationContext().getBean(asyncVerificationClass);
        if (ObjectUtils.isEmpty(asyncVerification)) {
            log.error("获取执行对象异常[{}]", asyncVerificationClass.getName());
            throw new RuntimeException("获取执行对象异常");
        }
        //异步校验
        AsyncVerificationResult asyncVerificationResult = asyncVerification.asyncVerification(dataModel.getCount());
        if (ObjectUtils.isEmpty(asyncVerificationResult)) {
            log.error("异步校验失败");
            throw new RuntimeException("异步校验失败");
        }
        dataModel.setAsynchronou(asyncVerificationResult.isAsynchronou());
        return new RunnerResult(FileVerification.class, dataModel, asyncVerificationResult.isAsynchronou(), asyncVerificationResult);
    }

}
