package com.ray.export.runner.imports;

import com.ray.export.empty.Empty;
import com.ray.export.exception.RunnerException;
import com.ray.export.runner.Runner;
import com.ray.export.runner.RunnerResult;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.result.SuccessResult;
import com.ray.export.template.ImportTemplate;
import com.ray.export.template.SyncImportTemplate;
import org.springframework.util.ObjectUtils;

import java.lang.annotation.Annotation;
import java.util.Arrays;

/**
 * @author bo shen
 * @Description: 文件类型校验执行者
 * @Class: FileTypeValidateRunner
 * @Package com.ray.runner
 * @date 2019/11/21 14:28
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class FileTypeValidateRunner implements Runner<Annotation> {

    @Override
    public RunnerResult doRunner(Annotation annotation, DataModel dataModel, RunnerResult perResult) {
        try {
            if (annotation instanceof SyncImportTemplate) {
                return this.hander(((SyncImportTemplate) annotation).fileType(), dataModel, perResult);
            } else if (annotation instanceof ImportTemplate) {
                return this.hander(((ImportTemplate) annotation).fileType(), dataModel, perResult);
            }
            return this.hander(new String[]{}, dataModel, perResult);
        } catch (Exception e) {
            //异常转换
            throw new RunnerException(e.getMessage(), Empty.class);
        }
    }

    public RunnerResult hander(String[] fileType, DataModel dataModel, RunnerResult perResult) {
        //读取文件类型  filetype为空标识支持全部 配置后支持配置类型
        if (!ObjectUtils.isEmpty(fileType)) {
            String fileName = dataModel.getFile().getOriginalFilename();
            String thisFileType = fileName.substring(fileName.lastIndexOf(".") + 1);
            if (!Arrays.asList(fileType).contains(thisFileType)) {
                throw new RuntimeException(String.format("当前文件格式[%s]不支持,配置支持格式:%s", thisFileType, Arrays.asList(fileType).toString()));
            }
        }
        return new RunnerResult(Empty.class, dataModel,new SuccessResult("校验通过"));
    }
}
