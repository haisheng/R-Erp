package com.ray.export.runner.imports;

import com.ray.export.empty.Empty;
import com.ray.export.exception.RunnerException;
import com.ray.export.runner.Hander;
import com.ray.export.runner.Runner;
import com.ray.export.runner.RunnerResult;
import com.ray.export.spring.ApplicationContextUtil;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.file.FileResult;
import com.ray.export.strategy.file.FileStore;
import com.ray.export.template.ImportTemplate;
import com.ray.export.template.SyncImportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 文件上传执行者
 * @Class: FileStoreRunner
 * @Package com.ray.runner
 * @date 2019/11/21 15:00
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class FileStoreRunner implements Runner<Annotation>, Hander {

    @Override
    public RunnerResult doRunner(Annotation annotation, DataModel dataModel, RunnerResult perResult) {
        try {
            if (annotation instanceof SyncImportTemplate) {
                return this.hander(((SyncImportTemplate) annotation).fileStore(), dataModel, perResult);
            } else if (annotation instanceof ImportTemplate) {
                return this.hander(((ImportTemplate) annotation).fileStore(), dataModel, perResult);
            }
            return this.hander(null, dataModel, perResult);
        } catch (Exception e) {
            //异常转换
            throw new RunnerException(e.getMessage(), FileStore.class);
        }
    }

    public RunnerResult hander(Class<?> fileStoreClass, DataModel dataModel, RunnerResult perResult) {
        log.info("执行文件存储方法");
        if (fileStoreClass == Empty.class) {
            return new RunnerResult(Empty.class, dataModel,dataModel.getBeforeResult());
        }
        //判读配置是否符合规则
        if (!FileStore.class.isAssignableFrom(fileStoreClass)) {
            log.error("文件存储配置错误");
            throw new RuntimeException("文件存储配置错误");
        }
        //获取执行对象
        FileStore fileStore = (FileStore) ApplicationContextUtil.getApplicationContext().getBean(fileStoreClass);
        if (ObjectUtils.isEmpty(fileStore)) {
            log.error("获取执行对象异常[{}]", fileStoreClass.getName());
            throw new RuntimeException("获取执行对象异常");
        }
        //上节点执行结果
        FileResult fileResult = fileStore.saveFile(dataModel);
        if (ObjectUtils.isEmpty(fileResult)) {
            log.error("文件存储失败");
            throw new RuntimeException("文件存储失败");
        }
        dataModel.setTaskKey(fileResult.getUrl());
        try {
            if (!ObjectUtils.isEmpty(dataModel.getFile())) {
                dataModel.setFileInputStream(dataModel.getFile().getInputStream());
            }
        } catch (IOException e) {
            log.info("文件流获取异常");
            throw  new RuntimeException("文件流获取异常");
        }
        return new RunnerResult(FileStore.class, dataModel, fileResult);
    }
}
