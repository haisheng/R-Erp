package com.ray.export.runner.imports;

import com.ray.export.empty.Empty;
import com.ray.export.exception.RunnerException;
import com.ray.export.runner.Hander;
import com.ray.export.runner.Runner;
import com.ray.export.runner.RunnerResult;
import com.ray.export.spring.ApplicationContextUtil;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.data.DataVerification;
import com.ray.export.strategy.data.DataVerificationResult;
import com.ray.export.template.ConfirmImportTemplate;
import com.ray.export.template.SyncImportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 数据校验执行者
 * @Class: DataVerificationRunner
 * @Package com.ray.runner.imports
 * @date 2019/11/22 9:26
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class DataVerificationRunner implements Runner<Annotation>, Hander {

    @Override
    public RunnerResult doRunner(Annotation annotation, DataModel dataModel, RunnerResult perResult) {
        try {
            if (annotation instanceof SyncImportTemplate) {
                return this.hander(((SyncImportTemplate) annotation).dataVerification(), dataModel, perResult);
            } else if (annotation instanceof ConfirmImportTemplate) {
                return this.hander(((ConfirmImportTemplate) annotation).dataVerification(), dataModel, perResult);
            }
            return this.hander(null, dataModel, perResult);
        } catch (Exception e) {
            //异常转换
            throw new RunnerException(e.getMessage(), DataVerification.class);
        }
    }

    @Override
    public RunnerResult hander(Class<?> dataVerificationClass, DataModel dataModel, RunnerResult perResult) {
        log.info("数据校验方法被执行");
        if (dataVerificationClass == Empty.class) {
            log.info("数据校验方法执行--默认空实现");
            return new RunnerResult(Empty.class, dataModel,dataModel.getBeforeResult());
        }
        //判读配置是否符合规则
        if (!DataVerification.class.isAssignableFrom(dataVerificationClass)) {
            log.error("数据校验配置错误");
            throw new RuntimeException("数据校验配置错误");
        }
        //获取执行对象
        DataVerification dataVerification = (DataVerification) ApplicationContextUtil.getApplicationContext().getBean(dataVerificationClass);
        if (ObjectUtils.isEmpty(dataVerification)) {
            log.error("获取执行对象异常[{}]", dataVerificationClass.getName());
            throw new RuntimeException("获取执行对象异常");
        }
        //上节点执行结果
        DataVerificationResult verificationResult = dataVerification.dataVerification(dataModel);
        if (ObjectUtils.isEmpty(verificationResult)) {
            log.error("文件校验异常");
            throw new RuntimeException("文件校验异常");
        }
        dataModel.setValidateMsg(verificationResult.getValidateMsg());
        return new RunnerResult(DataVerification.class, dataModel, verificationResult);
    }
}

