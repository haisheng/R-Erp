package com.ray.export.runner.common;

import com.ray.export.empty.Empty;
import com.ray.export.exception.RunnerException;
import com.ray.export.runner.Hander;
import com.ray.export.runner.Runner;
import com.ray.export.runner.RunnerResult;
import com.ray.export.spring.ApplicationContextUtil;
import com.ray.export.strategy.async.BeforeAsync;
import com.ray.export.strategy.async.KeySuccessResult;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.template.ConfirmImportTemplate;
import com.ray.export.template.ExportTemplate;
import com.ray.export.template.ImportTemplate;
import com.ray.export.template.SyncImportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 异步执行前执行者
 * @Class: DataLogRunner
 * @Package com.ray.runner.imports
 * @date 2019/11/22 11:06
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class BeforeAsyncRunner implements Runner<Annotation>, Hander {
    @Override
    public RunnerResult doRunner(Annotation annotation, DataModel dataModel, RunnerResult perResult) {
        try {
            if (annotation instanceof SyncImportTemplate) {
                return this.hander(((SyncImportTemplate) annotation).beforeAsync(), dataModel, perResult);
            } else if (annotation instanceof ImportTemplate) {
                return this.hander(((ImportTemplate) annotation).beforeAsync(), dataModel, perResult);
            } else if (annotation instanceof ConfirmImportTemplate) {
                return this.hander(((ConfirmImportTemplate) annotation).beforeAsync(), dataModel, perResult);
            } else if (annotation instanceof ExportTemplate) {
                return this.hander(((ExportTemplate) annotation).beforeAsync(), dataModel, perResult);
            }
            return this.hander(null, dataModel, perResult);
        } catch (Exception e) {
            //异常转换
            throw new RunnerException(e.getMessage(), BeforeAsync.class);
        }
    }

    @Override
    public RunnerResult hander(Class<?> successClass, DataModel dataModel, RunnerResult perResult) {
        log.info("执行异步执行前置方法");
        if (successClass == Empty.class) {
            return new RunnerResult(Empty.class, dataModel,dataModel.getBeforeResult());
        }
        //判读配置是否符合规则
        if (!BeforeAsync.class.isAssignableFrom(successClass)) {
            log.error("异步执行前置方法配置错误");
            throw new RuntimeException("异步执行前置方法配置错误");
        }
        //获取执行对象
        BeforeAsync beforeAsync = (BeforeAsync) ApplicationContextUtil.getApplicationContext().getBean(successClass);
        if (ObjectUtils.isEmpty(beforeAsync)) {
            log.error("获取执行对象异常[{}]", successClass.getName());
            throw new RuntimeException("获取执行对象异常");
        }
        KeySuccessResult successResult = beforeAsync.beforeAsync(dataModel);
        if (ObjectUtils.isEmpty(successResult)) {
            log.error("异步执行前置方法失败");
            throw new RuntimeException("异步执行前置方法失败");
        }
        dataModel.setTaskCode(successResult.getKey());
        return new RunnerResult(BeforeAsync.class, dataModel, successResult);
    }
}
