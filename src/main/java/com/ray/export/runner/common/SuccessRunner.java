package com.ray.export.runner.common;

import com.ray.export.empty.Empty;
import com.ray.export.exception.RunnerException;
import com.ray.export.runner.Hander;
import com.ray.export.runner.Runner;
import com.ray.export.runner.RunnerResult;
import com.ray.export.spring.ApplicationContextUtil;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.result.Success;
import com.ray.export.strategy.result.SuccessResult;
import com.ray.export.template.ConfirmImportTemplate;
import com.ray.export.template.ExportTemplate;
import com.ray.export.template.ImportTemplate;
import com.ray.export.template.SyncImportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 成功回调执行者
 * @Class: DataLogRunner
 * @Package com.ray.runner.imports
 * @date 2019/11/22 11:06
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class SuccessRunner implements Runner<Annotation>, Hander {
    @Override
    public RunnerResult doRunner(Annotation annotation, DataModel dataModel, RunnerResult perResult) {
        try {
            if (annotation instanceof SyncImportTemplate) {
                return this.hander(((SyncImportTemplate) annotation).success(), dataModel, perResult);
            } else if (annotation instanceof ImportTemplate) {
                return this.hander(((ImportTemplate) annotation).success(), dataModel, perResult);
            } else if (annotation instanceof ConfirmImportTemplate) {
                return this.hander(((ConfirmImportTemplate) annotation).success(), dataModel, perResult);
            } else if (annotation instanceof ExportTemplate) {
                return this.hander(((ExportTemplate) annotation).success(), dataModel, perResult);
            }
            return this.hander(null, dataModel, perResult);
        } catch (Exception e) {
            //异常转换
            throw new RunnerException(e.getMessage(), Success.class);
        }
    }


    @Override
    public RunnerResult hander(Class<?> successClass, DataModel dataModel, RunnerResult perResult) {
        log.info("执行成功回调");
        if (successClass == Empty.class) {
            return new RunnerResult(Empty.class, dataModel,dataModel.getBeforeResult());
        }
        //判读配置是否符合规则
        if (!Success.class.isAssignableFrom(successClass)) {
            log.error("成功回调配置错误");
            throw new RuntimeException("成功回调配置错误");
        }
        //获取执行对象
        Success success = (Success) ApplicationContextUtil.getApplicationContext().getBean(successClass);
        if (ObjectUtils.isEmpty(success)) {
            log.error("获取执行对象异常[{}]", successClass.getName());
            throw new RuntimeException("获取执行对象异常");
        }
        SuccessResult successResult = success.doSuccessCall(dataModel);
        if (ObjectUtils.isEmpty(successResult)) {
            log.error("成功回调失败");
            throw new RuntimeException("成功回调失败");
        }
        return new RunnerResult(Success.class, dataModel, successResult);
    }
}
