package com.ray.export.runner.common;

import com.ray.export.empty.Empty;
import com.ray.export.exception.RunnerException;
import com.ray.export.runner.Hander;
import com.ray.export.runner.Runner;
import com.ray.export.runner.RunnerResult;
import com.ray.export.spring.ApplicationContextUtil;
import com.ray.export.strategy.data.DataLog;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.result.SuccessResult;
import com.ray.export.template.ImportTemplate;
import com.ray.export.template.SyncImportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 日志写入执行者
 * @Class: DataLogRunner
 * @Package com.ray.runner.imports
 * @date 2019/11/22 11:06
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class DataLogRunner implements Runner<Annotation>, Hander {
    @Override
    public RunnerResult doRunner(Annotation annotation, DataModel dataModel, RunnerResult perResult) {
        try {
            if (annotation instanceof SyncImportTemplate) {
                return this.hander(((SyncImportTemplate) annotation).dataLog(), dataModel, perResult);
            } else if (annotation instanceof ImportTemplate) {
                return this.hander(((ImportTemplate) annotation).dataLog(), dataModel, perResult);
            }
            return this.hander(null, dataModel, perResult);
        } catch (Exception e) {
            //异常转换
            throw new RunnerException(e.getMessage(), DataLog.class);
        }
    }

    @Override
    public RunnerResult hander(Class<?> dataLogClass, DataModel dataModel, RunnerResult perResult) {
        log.info("执行日志操作回调");
        if (dataLogClass == Empty.class) {
            return new RunnerResult(Empty.class, dataModel,dataModel.getBeforeResult());
        }
        //判读配置是否符合规则
        if (!DataLog.class.isAssignableFrom(dataLogClass)) {
            log.error("日志操作回调配置错误");
            throw new RuntimeException("日志操作回调配置错误");
        }
        //获取执行对象
        DataLog dataLog = (DataLog) ApplicationContextUtil.getApplicationContext().getBean(dataLogClass);
        if (ObjectUtils.isEmpty(dataLog)) {
            log.error("获取执行对象异常[{}]", dataLogClass.getName());
            throw new RuntimeException("获取执行对象异常");
        }
        //上节点执行结果
        // dataModel.setPerResult(perResult);
        SuccessResult successResult = dataLog.doLog(dataModel);
        if (ObjectUtils.isEmpty(successResult)) {
            log.error("日志操作回调失败");
            throw new RuntimeException("日志操作回调失败");
        }
        return new RunnerResult(DataLog.class, dataModel, successResult);
    }
}
