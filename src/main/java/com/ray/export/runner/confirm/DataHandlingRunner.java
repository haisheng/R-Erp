package com.ray.export.runner.confirm;

import com.ray.export.empty.Empty;
import com.ray.export.exception.RunnerException;
import com.ray.export.runner.Hander;
import com.ray.export.runner.Runner;
import com.ray.export.runner.RunnerResult;
import com.ray.export.spring.ApplicationContextUtil;
import com.ray.export.strategy.data.DataHandling;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.result.SuccessResult;
import com.ray.export.template.ConfirmImportTemplate;
import com.ray.export.template.SyncImportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 数据处理
 * @Class: DataHandlingRunner
 * @Package com.ray.runner.confirm
 * @date 2019/11/25 16:13
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class DataHandlingRunner implements Runner<Annotation>, Hander {


    @Override
    public RunnerResult doRunner(Annotation annotation, DataModel dataModel, RunnerResult perResult) {
        try {
            if (annotation instanceof SyncImportTemplate) {
                return this.hander(((SyncImportTemplate) annotation).dataHandling(), dataModel, perResult);
            } else if (annotation instanceof ConfirmImportTemplate) {
                return this.hander(((ConfirmImportTemplate) annotation).dataHandling(), dataModel, perResult);
            }
            return this.hander(null, dataModel, perResult);
        } catch (Exception e) {
            //异常转换
            throw new RunnerException(e.getMessage(), DataHandling.class);
        }
    }

    @Override
    public RunnerResult hander(Class<?> dataHandlingClass, DataModel dataModel, RunnerResult perResult) {
        log.debug("执行数据处理操作");
        if (dataHandlingClass == Empty.class) {
            return new RunnerResult(Empty.class, dataModel,dataModel.getBeforeResult());
        }
        //判读配置是否符合规则
        if (!DataHandling.class.isAssignableFrom(dataHandlingClass)) {
            log.error("数据处理操作配置错误");
            throw new RuntimeException("数据处理调配置错误");
        }
        //获取执行对象
        DataHandling dataHandling = (DataHandling) ApplicationContextUtil.getApplicationContext().getBean(dataHandlingClass);
        if (ObjectUtils.isEmpty(dataHandling)) {
            log.error("获取执行对象异常[{}]", dataHandlingClass.getName());
            throw new RuntimeException("获取执行对象异常");
        }
        SuccessResult successResult = dataHandling.dataHandling(dataModel, dataModel.getContents());
        if (ObjectUtils.isEmpty(successResult)) {
            log.error("数据处理操作失败");
            throw new RuntimeException("数据处理操作失败");
        }
        return new RunnerResult(DataHandling.class, dataModel, successResult);
    }

}
