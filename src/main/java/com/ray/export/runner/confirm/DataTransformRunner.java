package com.ray.export.runner.confirm;

import com.ray.export.empty.Empty;
import com.ray.export.exception.RunnerException;
import com.ray.export.runner.Hander;
import com.ray.export.runner.Runner;
import com.ray.export.runner.RunnerResult;
import com.ray.export.spring.ApplicationContextUtil;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.data.DataTransform;
import com.ray.export.strategy.data.DataTransformResult;
import com.ray.export.template.ConfirmImportTemplate;
import com.ray.export.template.SyncImportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StopWatch;

import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 数据转换
 * @Class: DataTransformRunner
 * @Package com.ray.runner.confirm
 * @date 2019/11/22 17:12
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class DataTransformRunner implements Runner<Annotation>, Hander {


    @Override
    public RunnerResult doRunner(Annotation annotation, DataModel dataModel, RunnerResult perResult) {
        try {
            if (annotation instanceof SyncImportTemplate) {
                return this.hander(((SyncImportTemplate) annotation).dataTransform(), dataModel, perResult);
            } else if (annotation instanceof ConfirmImportTemplate) {
                return this.hander(((ConfirmImportTemplate) annotation).dataTransform(), dataModel, perResult);
            }
            return this.hander(null, dataModel, perResult);
        } catch (Exception e) {
            //异常转换
            throw new RunnerException(e.getMessage(), DataTransform.class);
        }
    }

    @Override
    public RunnerResult hander(Class<?> dataTransformClass, DataModel dataModel, RunnerResult perResult) {
        log.debug("执行数据转换操作");
        if (dataTransformClass == Empty.class) {
            return new RunnerResult(Empty.class, dataModel,dataModel.getBeforeResult());
        }
        //判读配置是否符合规则
        if (!DataTransform.class.isAssignableFrom(dataTransformClass)) {
            log.error("数据转换操作配置错误");
            throw new RuntimeException("数据转换作调配置错误");
        }
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        //获取执行对象
        DataTransform dataTransform = (DataTransform) ApplicationContextUtil.getApplicationContext().getBean(dataTransformClass);
        if (ObjectUtils.isEmpty(dataTransform)) {
            log.error("获取执行对象异常[{}]", dataTransformClass.getName());
            throw new RuntimeException("获取执行对象异常");
        }
        DataTransformResult successResult = dataTransform.dataTransform(dataModel);
        if (ObjectUtils.isEmpty(successResult)) {
            log.error("数据转换操作失败");
            throw new RuntimeException("数据转换操作失败");
        }
        dataModel.setContents(successResult.getContents());
        stopWatch.stop();
        log.info("内容转换耗时:{}",stopWatch.getTotalTimeMillis());
        return new RunnerResult(DataTransform.class, dataModel, successResult);
    }
}
