package com.ray.export.runner.confirm;

import com.ray.export.empty.Empty;
import com.ray.export.exception.RunnerException;
import com.ray.export.runner.Hander;
import com.ray.export.runner.Runner;
import com.ray.export.runner.RunnerResult;
import com.ray.export.spring.ApplicationContextUtil;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.file.*;
import com.ray.export.template.ConfirmImportTemplate;
import com.ray.export.template.SyncImportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 文件加载执行者
 * @Class: FileLoadRunner
 * @Package com.ray.runner.confirm
 * @date 2019/11/22 13:32
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class FileVerificationRunner implements Runner<Annotation>, Hander {


    @Override
    public RunnerResult doRunner(Annotation annotation, DataModel dataModel, RunnerResult perResult) {
        try {
            if (annotation instanceof SyncImportTemplate) {
                return this.hander(((SyncImportTemplate) annotation).fileVerification(), dataModel, perResult);
            } else if (annotation instanceof ConfirmImportTemplate) {
                return this.hander(((ConfirmImportTemplate) annotation).fileVerification(), dataModel, perResult);
            }
            return this.hander(null, dataModel, perResult);
        } catch (Exception e) {
            //异常转换
            throw new RunnerException(e.getMessage(), FileVerification.class);
        }
    }

    @Override
    public RunnerResult hander(Class<?> fileVerificationClass, DataModel dataModel, RunnerResult perResult) {
        log.info("执行文件校验操作");
        if (fileVerificationClass == Empty.class) {
            return new RunnerResult(Empty.class, dataModel,dataModel.getBeforeResult());
        }
        //判读配置是否符合规则
        if (!FileVerification.class.isAssignableFrom(fileVerificationClass)) {
            log.error("文件加载操作配置错误");
            throw new RuntimeException("文件加载操作调配置错误");
        }
        //获取执行对象
        FileVerification fileVerification = (FileVerification) ApplicationContextUtil.getApplicationContext().getBean(fileVerificationClass);
        if (ObjectUtils.isEmpty(fileVerification)) {
            log.error("获取执行对象异常[{}]", fileVerificationClass.getName());
            throw new RuntimeException("获取执行对象异常");
        }
        //上节点执行结果
        // dataModel.setPerResult(perResult);
        FileVerificationResult successResult = fileVerification.fileVerification(dataModel);
        if (ObjectUtils.isEmpty(successResult)) {
            log.error("文件加载操作失败");
            throw new RuntimeException("文件加载操作失败");
        }
        dataModel.setAsynchronou(successResult.isAsynchronou());
        return new RunnerResult(FileVerification.class, dataModel, successResult.isAsynchronou(), successResult);
    }
}
