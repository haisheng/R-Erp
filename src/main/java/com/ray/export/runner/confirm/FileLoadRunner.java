package com.ray.export.runner.confirm;

import com.ray.export.empty.Empty;
import com.ray.export.exception.RunnerException;
import com.ray.export.runner.Hander;
import com.ray.export.runner.Runner;
import com.ray.export.runner.RunnerResult;
import com.ray.export.spring.ApplicationContextUtil;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.file.FileLoad;
import com.ray.export.strategy.file.FileResult;
import com.ray.export.template.ConfirmImportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 文件加载执行者
 * @Class: FileLoadRunner
 * @Package com.ray.runner.confirm
 * @date 2019/11/22 13:32
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class FileLoadRunner implements Runner<Annotation>, Hander {


    @Override
    public RunnerResult doRunner(Annotation annotation, DataModel dataModel, RunnerResult perResult) {
        try {
            if (annotation instanceof ConfirmImportTemplate) {
                return this.hander(((ConfirmImportTemplate) annotation).fileLoad(), dataModel, perResult);
            }
            return this.hander(null, dataModel, perResult);
        } catch (Exception e) {
            //异常转换
            throw new RunnerException(e.getMessage(), FileLoad.class);
        }
    }

    @Override
    public RunnerResult hander(Class<?> fileLoadClass, DataModel dataModel, RunnerResult perResult) {
        log.info("执行文件加载操作");
        if (fileLoadClass == Empty.class) {
            return new RunnerResult(Empty.class, dataModel,dataModel.getBeforeResult());
        }
        //判读配置是否符合规则
        if (!FileLoad.class.isAssignableFrom(fileLoadClass)) {
            log.error("文件加载操作配置错误");
            throw new RuntimeException("文件加载操作调配置错误");
        }
        //获取执行对象
        FileLoad fileLoad = (FileLoad) ApplicationContextUtil.getApplicationContext().getBean(fileLoadClass);
        if (ObjectUtils.isEmpty(fileLoad)) {
            log.error("获取执行对象异常[{}]", fileLoadClass.getName());
            throw new RuntimeException("获取执行对象异常");
        }
        FileResult successResult = fileLoad.loadFile(dataModel);
        if (ObjectUtils.isEmpty(successResult)) {
            log.error("文件加载操作失败");
            throw new RuntimeException("文件加载操作失败");
        }
        dataModel.setFileInputStream(successResult.getFile());
        return new RunnerResult(FileLoad.class, dataModel, successResult);
    }
}
