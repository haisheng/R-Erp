package com.ray.export.runner;

import com.ray.export.strategy.data.DataModel;

/**
 * @author bo shen
 * @Description: 数据处理
 * @Class: Hander
 * @Package com.ray.runner
 * @date 2019/11/26 17:35
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface Hander {

    RunnerResult  hander(Class<?> clazz, DataModel dataModel, RunnerResult perResult);
}
