package com.ray.export.runner;

import com.ray.export.strategy.data.DataModel;

import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 策略---生成策略链的统一模板
 * @Class: Runner
 * @Package com.ray.strategy
 * @date 2019/11/21 14:24
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface Runner<E extends  Annotation> {
    /**
     * 执行者方法
     * @param annotation
     * @param dataModel
     * @return
     */
    RunnerResult doRunner(E annotation, DataModel dataModel, RunnerResult perResult);
}
