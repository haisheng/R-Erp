package com.ray.export.strategy.async;

import com.ray.export.strategy.data.DataModel;

/**
 * @author bo shen
 * @Description: 异步条用执行前执行
 * @Class: BeforeAsync
 * @Package com.ray.strategy.async
 * @date 2019/11/26 16:55
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface BeforeAsync {
    /**异步条用方法*/
    KeySuccessResult beforeAsync(DataModel dataModel);
}
