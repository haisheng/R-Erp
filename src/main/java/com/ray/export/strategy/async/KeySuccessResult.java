package com.ray.export.strategy.async;

import com.ray.export.strategy.result.SuccessResult;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 唯一值
 * @Class: KeySuccessResult
 * @Package com.ray.strategy.async
 * @date 2019/11/27 15:17
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class KeySuccessResult extends SuccessResult {

    private String key;

    public KeySuccessResult(String key) {
        this.key = key;
    }

    public KeySuccessResult(Boolean status, Object data, String msg, String key) {
        super(status, data, msg);
        this.key = key;
    }

    public KeySuccessResult(String msg, String key) {
        super(msg);
        this.key = key;
    }

    public KeySuccessResult(Boolean status, String msg, String key) {
        super(status, msg);
        this.key = key;
    }

    public KeySuccessResult(Boolean status, Object data, String key) {
        super(status, data);
        this.key = key;
    }
}
