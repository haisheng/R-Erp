package com.ray.export.strategy.exports;

import com.ray.export.request.ExportQueryRequest;

/**
 * <p>ClassName:     DataCount
 * <p>Description:   数据统计接口
 * <p>Author         jumWang
 * <p>Version        V1.0
 * <p>Date           2019.12.3
 */
public interface DataCount {
    /**
     * 获取结果总数
     * @param exportQueryRequest
     * @return
     */
    DataCountResult count(ExportQueryRequest exportQueryRequest);
}
