package com.ray.export.strategy.exports;

import com.ray.export.strategy.result.SuccessResult;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>ClassName:     DataCountResult
 * <p>Description:   数据统计结果
 * <p>Author         jumWang
 * <p>Version        V1.0
 * <p>Date           2019.12.3
 */
@NoArgsConstructor
@Data
public class DataCountResult extends SuccessResult {
    /**数据总数*/
    private Long count;
}
