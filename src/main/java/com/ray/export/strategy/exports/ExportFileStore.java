package com.ray.export.strategy.exports;

import com.ray.export.strategy.file.FileResult;

/**
 * <p>ClassName:     ExportFileStore
 * <p>Description:   导出文件存储接口
 * <p>Author         jumWang
 * <p>Version        V1.0
 * <p>Date           2019.12.5
 */
public interface ExportFileStore {
    /**
     * 保存文件
     * @param tmpFilePath
     * @return
     */
    FileResult saveFile(String tmpFilePath);
}
