package com.ray.export.strategy.exports;

/**
 * @author jumWang
 * @Description: 异步校验
 * @Class: AsyncVerification
 * @Package com.ray.strategy.exports
 * @date 2019/11/27 14:40
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface AsyncVerification {
    /**
     * 异步校验
     * @param count
     * @return
     */
    AsyncVerificationResult asyncVerification(Long count);
}
