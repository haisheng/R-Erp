package com.ray.export.strategy.exports;

/**
 * <p>ClassName:     PageVerification
 * <p>Description:   分页校验接口
 * <p>Author         jumWang
 * <p>Version        V1.0
 * <p>Date           2019.12.3
 */
public interface PageVerification {
    /**
     * 分页校验
     * @param count
     * @return
     */
    PageVerificationResult pageVerification(Long count);
}
