package com.ray.export.strategy.exports;

import com.ray.export.strategy.result.SuccessResult;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jumWang
 * @Description: 分页校验结果
 * @Class: PageVerificationResult
 * @Package com.ray.strategy.exports
 * @date 2019/12/02 14:40
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@NoArgsConstructor
@Data
public class PageVerificationResult extends SuccessResult{

    /**是否分页*/
    private Boolean isPaging = true;
    /**分页大小*/
    private Integer pageSize = 100;

}
