package com.ray.export.strategy.exports;

import com.ray.export.request.ExportQueryRequest;

import java.util.List;

/**
 * <p>ClassName:     DataExport
 * <p>Description:   数据导出接口
 * <p>Author         jumWang
 * <p>Version        V1.0
 * <p>Date           2019.12.3
 */
public interface DataExport {

    /**
     * 分页查询
     * @param exportQueryRequest
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<Object> pageDataQuery(ExportQueryRequest exportQueryRequest, Integer pageNo, Integer pageSize);

    /**
     * 列表头
     * @return
     */
    List<String> head();

}
