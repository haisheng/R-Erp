package com.ray.export.strategy.exports;

import com.ray.export.strategy.result.SuccessResult;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jumWang
 * @Description: 异步校验结果
 * @Class: AsyncVerificationResult
 * @Package com.ray.strategy.exports
 * @date 2019/11/22 14:40
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@NoArgsConstructor
@Data
public class AsyncVerificationResult extends SuccessResult{

    /***异步状态*/
    private boolean asynchronou = false;

}
