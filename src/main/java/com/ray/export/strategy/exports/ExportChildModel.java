package com.ray.export.strategy.exports;

import com.alibaba.excel.annotation.ExcelIgnore;
import lombok.Data;

import java.util.List;

/**
 * <p>ClassName:     ExportChildModel
 * <p>Description:   导出子结构
 * <p>Author         jumWang
 * <p>Version        V1.0
 * <p>Date           2019.12.10
 */
@Data
public class ExportChildModel<T> {
    /**子数据*/
    @ExcelIgnore
    private List<T> childList;
}
