package com.ray.export.strategy.header;

/**
 * @author bo shen
 * @Description: 表头模板接口
 * @Class: Header
 * @Package com.ray.strategy.header
 * @date 2019/11/20 19:02
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface Header {
}
