package com.ray.export.strategy.header;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 头部列属性
 * @Class: HeaderColumn
 * @Package com.ray.strategy.header
 * @date 2019/11/20 19:15
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class HeaderColumn {

    /**名称**/
    private String name;
    /**属性**/
    private String property;
    /**样式**/
    private HeaderStyle style;
}
