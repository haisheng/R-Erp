package com.ray.export.strategy.file;

import com.ray.export.excel.ExcelSheet;
import com.ray.export.strategy.result.SuccessResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.reflect.ParameterizedType;
import java.util.Map;

/**
 * @author bo shen
 * @Description: 文件内容结果
 * @Class: FileContentResult
 * @Package com.ray.strategy.file
 * @date 2019/11/22 16:12
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileContentResult<T> extends SuccessResult{
    /**文件内容**/
    Map<String,ExcelSheet<T>> contents;

    /***异步状态  通知下个节点是否异步--*/
    private boolean asynchronou = false;

    public FileContentResult(Boolean status,String msg) {
        super(status, null, msg);
    }

    /***
     * 获取泛型类型
     * @return
     */
    public Class<T> getTClass() {
        Class<T> tClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        return tClass;
    }
}
