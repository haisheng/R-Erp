package com.ray.export.strategy.file;

import com.ray.export.strategy.data.DataModel;

/**
 * @author bo shen
 * @Description: 文件内容读取
 * @Class: FileContentRead
 * @Package com.ray.strategy.file
 * @date 2019/11/22 16:10
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface FileContentRead<T> {
    /**
     * 读取文件内容
     * @param dataModel
     * @return
     */
     FileContentResult<T> readContent(DataModel<T> dataModel);
}
