package com.ray.export.strategy.file;

import com.ray.export.strategy.result.SuccessResult;
import lombok.Data;

import java.io.InputStream;

/**
 * @author bo shen
 * @Description: 文件结果对象
 * @Class: FileResult
 * @Package com.ray.strategy.file
 * @date 2019/11/21 15:28
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class FileResult extends SuccessResult{
    /**文件相当路径**/
    private String filePath;
    /**文件全路径**/
    private String url;
    /**文件流**/
    private InputStream file;

}
