package com.ray.export.strategy.file;

import com.ray.export.strategy.data.DataModel;

/**
 * @author bo shen
 * @Description: 文件校验
 * @Class: FileVerification
 * @Package com.ray.strategy.file
 * @date 2019/11/22 14:40
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface FileVerification {
    /**文件校验**/
    FileVerificationResult fileVerification(DataModel dataModel);
}
