package com.ray.export.strategy.file;

import com.ray.export.strategy.data.DataModel;

/**
 * @author bo shen
 * @Description: 文件存储策略
 * @Class: FileStore
 * @Package com.ray.strategy.file
 * @date 2019/11/20 18:05
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface FileStore {

    /***
     *  文件保存
     * @param dataModel 数据对象
     * @return
     */
     FileResult saveFile(DataModel dataModel);
}
