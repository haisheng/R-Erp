package com.ray.export.strategy.file;

import com.ray.export.strategy.result.SuccessResult;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bo shen
 * @Description: 文件校验结果
 * @Class: FileVerification
 * @Package com.ray.strategy.file
 * @date 2019/11/22 14:40
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@NoArgsConstructor
@Data
public class FileVerificationResult extends SuccessResult{

    /***异步状态*/
    private boolean asynchronou = false;

    public FileVerificationResult(boolean asynchronou) {
        super(true,null,null);
        this.asynchronou = asynchronou;
    }
    public FileVerificationResult(boolean asynchronou,Object data,String msg) {
        super(true,data,msg);
        this.asynchronou = asynchronou;
    }

    public FileVerificationResult(boolean asynchronou,Object data) {
        super(true,data,null);
        this.asynchronou = asynchronou;
    }

}
