package com.ray.export.strategy.file;

import com.ray.export.strategy.data.DataModel;

/**
 * @author bo shen
 * @Description: 文件模板
 * @Class: FileTemplate
 * @Package com.ray.strategy.file
 * @date 2019/11/22 16:21
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface FileTemplate<T> {
    /***
     * 模型配置接口
     * @param dataModel
     * @return
     */
    FileTemplateResult<T> readTemplate(DataModel dataModel);
}
