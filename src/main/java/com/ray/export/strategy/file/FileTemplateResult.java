package com.ray.export.strategy.file;
import com.ray.export.request.Page;
import com.ray.export.strategy.result.SuccessResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author bo shen
 * @Description: 文件内容结果
 * @Class: FileContentResult
 * @Package com.ray.strategy.file
 * @date 2019/11/22 16:12
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileTemplateResult<T> extends SuccessResult{
    /**模板**/
    List<Page<T>> pages;
}
