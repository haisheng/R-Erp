package com.ray.export.strategy.file;

import com.ray.export.strategy.data.DataModel;

/**
 * @author bo shen
 * @Description: 文件头读取
 * @Class: HeadRead
 * @Package com.ray.strategy.file
 * @date 2019/12/4 8:51
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface HeadRead {

    FileContentResult headRead(DataModel dataModel);
}
