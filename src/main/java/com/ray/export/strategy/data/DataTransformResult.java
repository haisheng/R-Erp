package com.ray.export.strategy.data;

import com.ray.export.strategy.result.SuccessResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author bo shen
 * @Description: 结果对象
 * @Class: DataTransformResult
 * @Package com.ray.strategy.data
 * @date 2019/11/22 17:10
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DataTransformResult<T> extends SuccessResult{
    /**文件内容**/
    Map<String,List<T>> contents;

    public DataTransformResult(Boolean status,String msg) {
        super(status, null, msg);
    }
}
