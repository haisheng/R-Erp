package com.ray.export.strategy.data;

import com.ray.export.strategy.result.SuccessResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author bo shen
 * @Description: 数据校验结果对象
 * @Class: DataVerificationResult
 * @Package com.ray.strategy.data
 * @date 2019/11/22 9:30
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataVerificationResult extends SuccessResult implements Serializable{

    /***异步状态*/
    private boolean asynchronou = false;
    /**错误信息**/
    private List<String> validateMsg;

    public DataVerificationResult(Boolean status,Object data) {
        super(status, data, null);
    }

    public DataVerificationResult(Boolean status,Object data,List<String> validateMsg,String msg) {
        super(status, data, msg);
        this.validateMsg = validateMsg;
    }
    public DataVerificationResult(Boolean status,Object data,List<String> validateMsg) {
        super(status, data, null);
        this.validateMsg = validateMsg;
    }

    public DataVerificationResult(Boolean status,Object data,String msg) {
        super(status, data, msg);
    }
}
