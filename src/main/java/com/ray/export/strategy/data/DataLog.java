package com.ray.export.strategy.data;

import com.ray.export.strategy.result.SuccessResult;

/**
 * @author bo shen
 * @Description: 日志文件
 * @Class: DataLog
 * @Package com.ray.strategy.data
 * @date 2019/11/20 18:59
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface DataLog {
    /**
     * 日志操作回调
     * @param dataModel
     * @return
     */
    SuccessResult doLog(DataModel dataModel);
}
