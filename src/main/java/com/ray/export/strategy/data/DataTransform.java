package com.ray.export.strategy.data;

/**
 * @author bo shen
 * @Description: 数据格式转换
 * @Class: DataTransform
 * @Package com.ray.strategy.data
 * @date 2019/11/22 17:09
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface DataTransform<T,K> {
    /**
     * 数据转换
     * @param dataModel
     * @return
     */
    DataTransformResult<T> dataTransform(DataModel<K> dataModel);
}
