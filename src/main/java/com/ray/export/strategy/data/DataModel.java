package com.ray.export.strategy.data;


import com.ray.export.excel.ExcelSheet;
import com.ray.export.request.ExportQueryRequest;
import com.ray.export.request.ImportConfirmRequest;
import com.ray.export.strategy.result.SuccessResult;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author bo shen
 * @Description: 数据模型 用于各个节点数据的传输
 * @Class: DataModel
 * @Package com.ray.strategy.data
 * @date 2019/11/21 9:59
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class DataModel<T> {
    /**模板code**/
    private String templateCode;
    /**文件流*/
    private MultipartFile file;
    /**数据唯一标识  可以是文件路径*/
    private String taskKey;
    /***任务编号***/
    private String taskCode;
    /***上节点执行结果**/
    /**http 结果对象**/
    private HttpServletResponse response;
    /**http 请求对象**/
    private HttpServletRequest request;
    /**导入确认对象*/
    private ImportConfirmRequest importConfirm;
    /**文件流**/
    private InputStream fileInputStream;
    /***异步状态*/
    private boolean asynchronou = false;
    /***自定义数据**/
    private SuccessResult beforeResult;
    /**文件内容**/
    Map<String,ExcelSheet<T>> contents;
    /**数据泛型对象**/
    private Class<?> dataClazz = HashMap.class;
    /**错误信息**/
    private List<String> validateMsg;
    /**导出查询入参**/
    private ExportQueryRequest exportQueryRequest;
    /**导出结果数**/
    private Long count = 0L;
    /**导出是否分页*/
    private Boolean isPaging = true;
    /**导出分页大小*/
    private Integer pageSize = 100;
    /**导出暂存地址(相对路径)*/
    private String tmpFilePath;
}
