package com.ray.export.strategy.data;

/**
 * @author bo shen
 * @Description: 数据校验
 * @Class: DataVerification
 * @Package com.ray.strategy.data
 * @date 2019/11/20 18:10
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface DataVerification {
    /**
     * 文件校验
     * @param dataModel
     * @return
     */
    DataVerificationResult dataVerification(DataModel dataModel);
}
