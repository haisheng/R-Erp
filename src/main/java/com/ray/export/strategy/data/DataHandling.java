package com.ray.export.strategy.data;

import com.ray.export.excel.ExcelSheet;
import com.ray.export.strategy.result.SuccessResult;

import java.util.Map;

/**
 * @author bo shen
 * @Description: 数据处理
 * @Class: DataHandling
 * @Package com.ray.strategy.data
 * @date 2019/11/25 16:13
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface DataHandling<T> {
    /***
     * 数据处理
     * @param datas
     * @return
     */
    SuccessResult dataHandling(DataModel dataModel, Map<String, ExcelSheet<T>> datas);
}
