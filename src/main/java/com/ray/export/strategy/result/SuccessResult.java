package com.ray.export.strategy.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bo shen
 * @Description: 成功回调结果
 * @Class: SuccessResult
 * @Package com.ray.strategy.result
 * @date 2019/11/22 11:10
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SuccessResult {
    /**执行通过  执行不通过 针对runner***/
    private Boolean status = true;
    /**回调结果**/
    private Object data;
    /**信息**/
    private String msg;

    public SuccessResult(String msg) {
        this.msg = msg;
    }

    public SuccessResult(Boolean status,String msg) {
        this.status = status;
        this.msg = msg;
    }

    public SuccessResult(Boolean status,Object data) {
        this.status = status;
        this.data = data;
    }
}
