package com.ray.export.strategy.result;

import com.ray.export.runner.RunnerResult;
import com.ray.export.strategy.data.DataModel;

/**
 * @author bo shen
 * @Description: 错误回调
 * @Class: Success
 * @Package com.ray.strategy.result
 * @date 2019/11/20 19:00
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface Error {
    /**
     * 错误执行
     * @param dataModel
     * @return
     */
    SuccessResult doErrorCall(DataModel dataModel, RunnerResult runnerResult, String errorMsg, Class<?> clazz);
}
