package com.ray.export.strategy.result;

import com.ray.export.strategy.data.DataModel;

/**
 * @author bo shen
 * @Description: 成功回调
 * @Class: Success
 * @Package com.ray.strategy.result
 * @date 2019/11/20 19:00
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface Success {
    /**
     * 成功执行
     * @param dataModel
     * @return
     */
    SuccessResult doSuccessCall(DataModel dataModel);
}
