package com.ray.export.exception;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: 执行者异常
 * @Class: RunnerException
 * @Package com.ray.exception
 * @date 2019/11/27 13:18
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@Slf4j
public class DataRunnerException extends RuntimeException{

    private Object object;


    public DataRunnerException(String message, Object object) {
        super(message);
        this.object = object;
    }

    public DataRunnerException(String message, Throwable cause, Object object) {
        super(message, cause);
        this.object = object;
    }

    public DataRunnerException(Throwable cause, Object object) {
        super(cause);
        this.object = object;
    }

    public DataRunnerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Object object) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.object = object;
    }
}
