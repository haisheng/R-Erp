package com.ray.export.exception;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: 执行者异常
 * @Class: RunnerException
 * @Package com.ray.exception
 * @date 2019/11/27 13:18
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@Slf4j
public class RunnerException extends RuntimeException{

    private Class<?> runType;


    public RunnerException(String message, Class<?> runType) {
        super(message);
        this.runType = runType;
        log.warn("执行异常:{}---{}",runType.getName(),message);
    }

    public RunnerException(String message, Throwable cause, Class<?> runType) {
        super(message, cause);
        this.runType = runType;
        log.warn("执行异常:{}---{}",runType.getName(),message);
    }

    public RunnerException(Throwable cause, Class<?> runType) {
        super(cause);
        this.runType = runType;
    }

    public RunnerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Class<?> runType) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.runType = runType;
    }
}
