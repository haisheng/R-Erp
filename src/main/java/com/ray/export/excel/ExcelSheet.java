package com.ray.export.excel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author bo shen
 * @Description: 解析内容信息
 * @Class: ExcelSheet
 * @Package com.ray.file.excel
 * @date 2019/11/25 10:53
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExcelSheet<T> {
    /**名称**/
    private String sheetName;
    /***头***/
    private Map<Integer,String> head;
    /***内容***/
    private List<T> contents;
}
