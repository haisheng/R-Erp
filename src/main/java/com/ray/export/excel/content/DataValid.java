package com.ray.export.excel.content;

/**
 * @author bo shen
 * @Description: excel同步解析
 * @Class: ExcelContentDataTransform
 * @Package com.ray.file.excel.head
 * @date 2019/11/25 8:47
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface DataValid<T,R> {

    /**
     * 数据校验
     * @param t
     * @return
     */
    String Valid(T t, R r);
}
