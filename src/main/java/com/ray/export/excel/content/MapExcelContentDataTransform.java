package com.ray.export.excel.content;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * @author bo shen
 * @Description: list头部解析
 * @Class: ListExcelContentDataTransform
 * @Package com.ray.file.excel.head
 * @date 2019/11/25 8:49
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class MapExcelContentDataTransform implements ExcelContentDataTransform<Map<Integer,String>> {

    @Override
    public Map<String, String> excelContentDataTransform(Map<Integer,String> dataFrom) {
        log.debug("开始进行数据转换，map -> map");
        Map<String, String> data = new HashMap<>();
        for (Integer index : dataFrom.keySet()) {
            data.put(String.valueOf(index),dataFrom.get(index));
        }
        return data;
    }
}
