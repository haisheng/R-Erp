package com.ray.export.excel.content;

import com.ray.export.excel.ColumsBoolean;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * @author bo shen
 * @Description:
 * @Class: MapValid
 * @Package com.ray.file.excel.content
 * @date 2019/11/28 14:59
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class MapValid implements DataValid<Map<String, String>, Map<String, ColumsBoolean>> {

    @Override
    public String Valid(Map<String, String> data, Map<String, ColumsBoolean> rule) {
        StringBuffer buffer = new StringBuffer();
        for (String key : data.keySet()) {
            ColumsBoolean requier = rule.get(key);
            //配置  并且 必填
            if (requier != null && requier.getRequire()) {
                if (ObjectUtils.isEmpty(data.get(key))) {
                    buffer.append(String.format("[%s]没有数据,", requier.getName()));
                }
            }
        }
        if(StringUtils.hasLength(buffer.toString())){
            return buffer.toString();
        }
        return null;
    }
}
