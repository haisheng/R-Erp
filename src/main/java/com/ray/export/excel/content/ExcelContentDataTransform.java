package com.ray.export.excel.content;

import java.util.Map;

/**
 * @author bo shen
 * @Description: excel同步解析
 * @Class: ExcelContentDataTransform
 * @Package com.ray.file.excel.head
 * @date 2019/11/25 8:47
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface ExcelContentDataTransform<T> {

    /**
     * 同步数据解析
     * @param t
     * @return
     */
    Map<String,String> excelContentDataTransform(T t);
}
