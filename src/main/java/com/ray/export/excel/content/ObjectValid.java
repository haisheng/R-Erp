package com.ray.export.excel.content;
import com.ray.validate.support.utils.ValidateUtil;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * @author bo shen
 * @Description:
 * @Class: MapValid
 * @Package com.ray.file.excel.content
 * @date 2019/11/28 14:59
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class ObjectValid implements DataValid<Object,Object>{

    @Override
    public String Valid(Object data, Object o2) {
        List<String>  lists = ValidateUtil.getValidateMsg(data);
        if(!ObjectUtils.isEmpty(lists)){
            return  String.join(",",lists);
        }
        return null;
    }
}
