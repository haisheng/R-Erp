package com.ray.export.excel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bo shen
 * @Description: 字段属性
 * @Class: ColumsBoolean
 * @Package com.ray.file.excel
 * @date 2019/11/28 17:49
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ColumsBoolean {
    /**是否必填**/
    private Boolean require;
    /**名称**/
    private String name;

}
