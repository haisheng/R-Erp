package com.ray.export.excel.head;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author bo shen
 * @Description: 头模板数据解析
 * @Class: ContentDataTransformUtil
 * @Package com.ray.file.excel.head
 * @date 2019/11/25 8:50
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class HeadDataTransformUtil {

    private static Map<Class<?>,ExcelHeadDataTransform> dataTransforms = new HashMap<Class<?>,ExcelHeadDataTransform>();

    static {
        addDataTransform(List.class,new ListExcelHeadDataTransform());
        addDataTransform(ArrayList.class,new ListExcelHeadDataTransform());


    }

    /**
     * 添加一个执行器 如果key相同会替换原来的执行器
     *
     * @param clazz
     * @param dataTransform
     */
    public static void addDataTransform(Class<?> clazz, ExcelHeadDataTransform dataTransform) {
        Assert.notNull(clazz, "参数[clazz]不能为空");
        Assert.notNull(dataTransform, "参数[actuator]不能为空");
        if (log.isDebugEnabled()) {
            log.debug("执行者{},{}被添加", clazz, dataTransform.getClass().getName());
        }
        dataTransforms.put(clazz, dataTransform);
    }

    /**
     * 获取实际的执行者
     * @param clazz
     * @return
     */
    public static ExcelHeadDataTransform getExcelHeadDataTransform(Class<?> clazz) {
        ExcelHeadDataTransform headDataTransform = dataTransforms.get(clazz);
        if(ObjectUtils.isEmpty(headDataTransform)){
            log.info("参数为[{}]的数据转对象不存在",clazz.getName());
            throw new RuntimeException("数据转对象不存在");
        }
        return  headDataTransform;
    }


}
