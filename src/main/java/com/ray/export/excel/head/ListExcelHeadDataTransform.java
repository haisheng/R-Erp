package com.ray.export.excel.head;

import java.util.List;

/**
 * @author bo shen
 * @Description: list头部解析
 * @Class: ListExcelContentDataTransform
 * @Package com.ray.file.excel.head
 * @date 2019/11/25 8:49
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class ListExcelHeadDataTransform implements ExcelHeadDataTransform<List>{

    @Override
    public List<String> excelHeadDataTransform(List list) {
        return list;
    }
}
