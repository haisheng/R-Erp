package com.ray.export.builder;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.ray.export.enums.PageColumsType;
import com.ray.export.request.*;
import com.ray.export.strategy.data.DataModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author bo shen
 * @Description: 数据对象构造者
 * @Class: DataModelBuilder
 * @Package com.ray.builder
 * @date 2019/11/21 13:25
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class DataModelBuilder {
    /**
     * 构件参数
     *
     * @param params
     * @return
     */
    public static DataModel build(Object[] params) {
        DataModel dataModel = new DataModel();
        for (Object object : params) {
            if (object instanceof MultipartFile) {
                MultipartFile file = (MultipartFile) object;
                dataModel.setFile(file);
            } else if (object instanceof HttpServletRequest) {
                HttpServletRequest request = (HttpServletRequest) object;
                dataModel.setRequest(request);
            } else if (object instanceof HttpServletResponse) {
                HttpServletResponse response = (HttpServletResponse) object;
                dataModel.setResponse(response);
            } else if (object instanceof ImportConfirmRequest) {
                ImportConfirmRequest confirmRequest = (ImportConfirmRequest) object;
                dataModel.setImportConfirm(confirmRequest);
                dataModel.setTaskKey(confirmRequest.getTaskKey());
            }
            if (object instanceof ExportQueryRequest) {
                ExportQueryRequest exportQueryRequest = (ExportQueryRequest) object;
                dataModel.setExportQueryRequest(exportQueryRequest);
                dataModel.setTaskKey(exportQueryRequest.getTaskKey());
            }
        }

        //默认生存一个taskCode
        dataModel.setTaskCode(UUID.randomUUID().toString());
        //默认初始化ImportConfirmRequest
        if (ObjectUtils.isEmpty(dataModel.getImportConfirm())) {
            dataModel.setImportConfirm(new ImportConfirmRequest());
        }
        //转换数据
        pagesFormat(dataModel);
        return dataModel;
    }

    /**
     * 解析文本数据
     **/
    private static void pagesFormat(DataModel dataModel) {
        ImportConfirmRequest importConfirmRequest = dataModel.getImportConfirm();
        if (ObjectUtils.isEmpty(importConfirmRequest.getPages()) && StringUtils.hasLength(importConfirmRequest.getPagesValue())) {
            log.info(importConfirmRequest.getPagesValue());
            if (StringUtils.pathEquals(importConfirmRequest.getType(), PageColumsType.NAME.getValue())) {
                importConfirmRequest.setPages(JSON.parseObject(replaceBlank(importConfirmRequest.getPagesValue()), new TypeReference<List<Page<NameColums>>>() {
                }));
            } else if (StringUtils.pathEquals(importConfirmRequest.getType(), PageColumsType.INDEX.getValue())) {
                importConfirmRequest.setPages(JSON.parseObject(replaceBlank(importConfirmRequest.getPagesValue()), new TypeReference<List<Page<IndexColums>>>() {
                }));
            } else {
                throw new RuntimeException("请指定importConfirmRequest参数字段配置类型");
            }
        }
    }


    public static String replaceBlank(String str) {
        String dest = "";
        if (str != null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }

}
