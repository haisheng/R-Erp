package com.ray.export.empty;

import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: 空文件上传实现
 * @Class: Empty
 * @Package com.ray.empty
 * @date 2019/11/21 16:28
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class Empty {

}
