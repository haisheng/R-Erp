package com.ray.export.support.result;

import com.ray.export.runner.RunnerResult;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.result.SuccessResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.ray.export.strategy.result.Error;
/**
 * @author bo shen
 * @Description: 默认结果处理实现
 * @Class: DefaultResult
 * @Package com.ray.support.result
 * @date 2019/11/22 10:42
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class DefaultError implements Error {
    @Override
    public SuccessResult doErrorCall(DataModel dataModel, RunnerResult runnerResult,String errorMgs, Class<?> clazz) {

        log.info("异常信息,异常类型:{},内容:{}",clazz.getName(),errorMgs);
        return new SuccessResult(true,"异常回调成功");
    }
}
