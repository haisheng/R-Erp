package com.ray.export.support.result;

import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.result.Result;
import com.ray.export.strategy.result.SuccessResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author bo shen
 * @Description: 默认结果处理实现
 * @Class: DefaultResult
 * @Package com.ray.support.result
 * @date 2019/11/22 10:42
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class DefaultImportResult implements Result {
    @Override
    public SuccessResult doResult(DataModel dataModel) {
        Map<String,Object> result = new HashMap();
        result.put("url",dataModel.getTaskKey());
        result.put("head",dataModel.getContents());
        return new SuccessResult(true,result,"调用成功");
    }
}
