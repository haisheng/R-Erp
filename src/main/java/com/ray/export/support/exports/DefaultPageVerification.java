package com.ray.export.support.exports;

import com.ray.export.strategy.exports.PageVerification;
import com.ray.export.strategy.exports.PageVerificationResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * <p>ClassName:     DefaultPageVerification
 * <p>Description:   默认分页校验实现
 * <p>Author         jumWang
 * <p>Version        V1.0
 * <p>Date           2019.12.3
 */
@Slf4j
@Service
public class DefaultPageVerification implements PageVerification {

    @Value("${exports.pageVerification.pageSize:100}")
    private Integer pageSize;

    @Override
    public PageVerificationResult pageVerification(Long count) {
        if(0 >= pageSize) {
            throw new RuntimeException("分页大小必须大于0");
        }
        PageVerificationResult pageVerificationResult = new PageVerificationResult();
        if(pageSize >= count) {
            pageVerificationResult.setIsPaging(false);
        }else {
            pageVerificationResult.setIsPaging(true);
            pageVerificationResult.setPageSize(pageSize);
        }
        return pageVerificationResult;
    }
}
