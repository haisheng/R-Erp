package com.ray.export.support.exports;

import com.ray.export.strategy.exports.AsyncVerification;
import com.ray.export.strategy.exports.AsyncVerificationResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * <p>ClassName:     DefaultAsyncVerification
 * <p>Description:   默认异步校验实现
 * <p>Author         jumWang
 * <p>Version        V1.0
 * <p>Date           2019.12.3
 */
@Slf4j
@Service
public class DefaultAsyncVerification implements AsyncVerification {

    @Value("${exports.asyncVerification.asyncSize:10000}")
    private Long asyncSize;

    @Override
    public AsyncVerificationResult asyncVerification(Long count) {
        if(0 >= asyncSize) {
            throw new RuntimeException("异步校验大小必须大于0");
        }
        AsyncVerificationResult asyncVerificationResult = new AsyncVerificationResult();
        if (asyncSize < count) {
            asyncVerificationResult.setAsynchronou(true);
            asyncVerificationResult.setMsg("文件导出中,请在任务列表查询任务进度...");
        }else {
            asyncVerificationResult.setAsynchronou(false);
        }
        return asyncVerificationResult;
    }
}
