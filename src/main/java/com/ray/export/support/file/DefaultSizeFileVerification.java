package com.ray.export.support.file;

import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.file.FileVerification;
import com.ray.export.strategy.file.FileVerificationResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * @author bo shen
 * @Description: 文件大小校验
 * @Class: SizeFileVerification
 * @Package com.ray.support.file
 * @date 2019/11/22 14:50
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class DefaultSizeFileVerification implements FileVerification {

    @Value("${fileVerification.asynchronou.size:20}")
    private double fileSize;
    @Value("${fileVerification.asynchronou.tips:文件太大,异步处理中..}")
    private String tips;

    @Override
    public FileVerificationResult fileVerification(DataModel dataModel) {
        Assert.notNull(dataModel.getFileInputStream(),"文件不存在");
        log.info("进人文件大小校验");
        try{
            int size = dataModel.getFileInputStream().available();
            double kbSize = size/1024;
            log.info("文件大小:{}KB/{}KB",kbSize,size/1024);
            FileVerificationResult fileVerificationResult = new FileVerificationResult(kbSize>fileSize,null,tips);
            return  fileVerificationResult;
        }catch (Exception e){
            log.info("读取文件大小异常");
            throw new RuntimeException("读取文件大小异常");
        }
    }
}
