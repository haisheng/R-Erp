package com.ray.export.support.file;

import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.file.FileVerification;
import com.ray.export.strategy.file.FileVerificationResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * @author bo shen
 * @Description: 默认同步处理
 * @Class: SizeFileVerification
 * @Package com.ray.support.file
 * @date 2019/11/22 14:50
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class DefaultSyncFileVerification implements FileVerification {

    @Override
    public FileVerificationResult fileVerification(DataModel dataModel) {
        Assert.notNull(dataModel.getFileInputStream(),"文件不存在");
        return   new FileVerificationResult(false,null,"文件同步处理");
    }
}
