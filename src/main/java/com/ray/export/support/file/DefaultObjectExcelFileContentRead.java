package com.ray.export.support.file;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.read.builder.ExcelReaderBuilder;
import com.ray.export.excel.ContentListener;
import com.ray.export.excel.ExcelSheet;
import com.ray.export.request.ImportConfirmRequest;
import com.ray.export.request.Page;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.file.FileContentRead;
import com.ray.export.strategy.file.FileContentResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.io.BufferedInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author bo shen
 * @Description: excel文件内容读取
 * @Class: ExcelFileContentRead
 * @Package com.ray.support.file
 * @date 2019/11/22 16:27
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class DefaultObjectExcelFileContentRead implements FileContentRead<Object> {

    @Override
    public FileContentResult<Object> readContent(DataModel<Object> dataModel) {
        Assert.notNull(dataModel.getFileInputStream(), "文件不存在");
        log.info("进人内容读取");
        Map<String, ExcelSheet<Object>> contents = new HashMap<>();
        try {
            ContentListener<Object> headReadListener = new ContentListener();
            ExcelReaderBuilder excelReaderBuilder = EasyExcel.read(new BufferedInputStream(dataModel.getFileInputStream()),headReadListener);
            ExcelReader excelReader = excelReaderBuilder.build();
            List<Sheet> sheets = excelReader.getSheets();
            for(Sheet sheet :sheets){
                Class<?> dataClazz = getDataClazz(sheet.getSheetName(),dataModel.getImportConfirm(),dataModel.getDataClazz());
                if(ObjectUtils.isEmpty(dataClazz)){
                    excelReader.read(sheet,HashMap.class);
                }else {
                    excelReader.read(sheet,dataClazz);
                }
            }
            excelReader.finish();
            contents = headReadListener.getContents();
        } catch (Exception e) {
            log.info("文件读取失败.", e);
            return new FileContentResult(false, "文件读取失败");
        }
        return new FileContentResult(contents,false);
    }

    /**
     * 获取excel对应的模型
     * @param sheetName
     * @param importConfirm
     * @param dataClazz
     * @return
     */
    private Class<?> getDataClazz(String sheetName, ImportConfirmRequest<Object> importConfirm, Class<?> dataClazz) {
        //直接返回配置
        if(!StringUtils.hasLength(sheetName)|| ObjectUtils.isEmpty(importConfirm) || ObjectUtils.isEmpty(importConfirm.getPages())){
            return  dataClazz;
        }

        for(Page page :importConfirm.getPages()){
            if(StringUtils.pathEquals(page.getSheetName(),sheetName)){
                return  page.getDataClazz();
            }
        }

        return dataClazz;
    }


}
