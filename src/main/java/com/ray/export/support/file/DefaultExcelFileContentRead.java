package com.ray.export.support.file;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.builder.ExcelReaderBuilder;
import com.ray.export.excel.ContentListener;
import com.ray.export.excel.ExcelSheet;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.file.FileContentRead;
import com.ray.export.strategy.file.FileContentResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.BufferedInputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author bo shen
 * @Description: excel文件内容读取
 * @Class: ExcelFileContentRead
 * @Package com.ray.support.file
 * @date 2019/11/22 16:27
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class DefaultExcelFileContentRead implements FileContentRead<Map<Integer,String>> {

    @Override
    public FileContentResult<Map<Integer,String>> readContent(DataModel<Map<Integer,String>> dataModel) {
        Assert.notNull(dataModel.getFileInputStream(), "文件不存在");
        log.info("进人内容读取");
        Map<String, ExcelSheet<Map<Integer,String>>> contents = new HashMap<>();
        try {
            ContentListener<Map<Integer,String>> headReadListener = new ContentListener();
            ExcelReaderBuilder excelReaderBuilder = EasyExcel.read(new BufferedInputStream(dataModel.getFileInputStream()), headReadListener);
            excelReaderBuilder.doReadAll();
            contents = headReadListener.getContents();
        } catch (Exception e) {
            log.info("文件读取失败.", e);
            return new FileContentResult(false, "文件读取失败");
        }
        return new FileContentResult(contents,false);
    }

}
