package com.ray.export.support.file;

import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.file.FileLoad;
import com.ray.export.strategy.file.FileResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.net.URL;

/**
 * @author bo shen
 * @Description: http文件路径地址加载
 * @Class: HttpFileLoad
 * @Package com.ray.support.file
 * @date 2019/11/22 14:11
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class DefaultHttpFileLoad implements FileLoad{
    @Override
    public FileResult loadFile(DataModel dataModel) {
        Assert.hasLength(dataModel.getTaskKey(),"文件路径不存在.");
        FileResult fileResult = null;
        try {
            URL url = new URL(dataModel.getTaskKey());
            fileResult = new FileResult();
            fileResult.setFile(url.openStream());
        }catch (Exception e){
            log.info("读取[{}]文件失败",dataModel.getTaskKey());
            throw new RuntimeException("读取文件失败");
        }
        return fileResult;
    }
}
