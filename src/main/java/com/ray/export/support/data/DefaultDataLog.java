package com.ray.export.support.data;

import com.ray.export.strategy.data.DataLog;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.result.SuccessResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author bo shen
 * @Description: 默认日志写入
 * @Class: DefaultDataLog
 * @Package com.ray.support.data
 * @date 2019/11/22 11:31
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class DefaultDataLog implements DataLog {
    @Override
    public SuccessResult doLog(DataModel dataModel) {
        if (ObjectUtils.isEmpty(dataModel.getRequest())) {
            log.info("导入任务[{}],日志回调.", dataModel.getTemplateCode());
        } else {
            HttpServletRequest request = dataModel.getRequest();
            log.info("导入任务[{}],日志回调,调用方信息-IP:{}.信息:{}。", dataModel.getTemplateCode(), request.getRemoteAddr(),request.getHeader("token"));
        }
        return new SuccessResult();
    }
}
