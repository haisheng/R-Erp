package com.ray.export.support.data;

import com.ray.export.excel.ColumsBoolean;
import com.ray.export.excel.ExcelSheet;
import com.ray.export.excel.content.DataVerificationUtil;
import com.ray.export.request.ImportConfirmRequest;
import com.ray.export.request.IndexColums;
import com.ray.export.request.NameColums;
import com.ray.export.request.Page;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.data.DataVerification;
import com.ray.export.strategy.data.DataVerificationResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author bo shen
 * @Description: 默认数据校验
 * @Class: DefaultDataVerification
 * @Package com.ray.support.data
 * @date 2019/11/22 9:47
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class DefaultDataVerification implements DataVerification{

    /***模板配置**/
    private Map<String, Map<String, ColumsBoolean>> template = new HashMap<>();
    @Override
    public DataVerificationResult dataVerification(DataModel dataModel) {
        initTemplate(dataModel.getImportConfirm());
        List<String> contents = verification(dataModel);
        if(contents.isEmpty()){
            return new DataVerificationResult();
        }else {
            return new DataVerificationResult(false,contents,contents,"验证异常");
        }
    }

    /**
     * 数据校验
     * @param dataModel
     * @return
     */
    private List<String> verification(DataModel dataModel) {
        //返回结果
        List<String> result = new ArrayList<>();
        try {
            Map<String, ExcelSheet<Map<Integer, String>>> content = dataModel.getContents();
            for (String key : content.keySet()) {
                ExcelSheet<Map<Integer, String>> excelSheet = content.get(key);
                int index = 1;
                for (Object data : excelSheet.getContents()) {
                    String msg =  DataVerificationUtil.verification(data,template.get(key));
                    if(StringUtils.hasLength(msg)){
                        result.add(String.format("标签页:%s,第%d行,错误信息:%s",key,index,msg));
                    }
                    index++;
                }
            }
        } catch (Exception e) {
            log.info("文件转换异常",e);
            throw  new RuntimeException("文件转换异常");
        }

        return  result;
    }


    /**
     * 读取文件模板头
     *
     * @param importConfirmRequest
     */
    private void initTemplate(ImportConfirmRequest<Object> importConfirmRequest) {
        for (Page<?> page : importConfirmRequest.getPages()) {
            //使用模型不需要数据转换
            if(page.getUseModel()){
                continue;
            }
            log.debug("标签页:{}-----------------开始----------", page.getSheetName());
            Map<String, ColumsBoolean> sheetMap = new HashMap<>();
            for (Object col : page.getColums()) {
                //不支持map
                if(col instanceof Map){
                    throw new RuntimeException("配置模板不支持map格式");
                }
                if (col instanceof NameColums) {
                    NameColums nameColums = (NameColums) col;
                    sheetMap.put(nameColums.getProperty(), new ColumsBoolean(nameColums.getRequire(),nameColums.getName()));
                }
                if (col instanceof IndexColums) {
                    IndexColums indexColums = (IndexColums) col;
                    sheetMap.put(String.valueOf(indexColums.getProperty()),  new ColumsBoolean(indexColums.getRequire(),String.valueOf(indexColums.getIndex())));
                }
            }
            template.put(page.getSheetName(), sheetMap);
            log.debug("标签页:{}-----------------结束----------", page.getSheetName());
        }
    }


}
