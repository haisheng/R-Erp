package com.ray.export.support.data;


import com.ray.export.excel.ExcelSheet;
import com.ray.export.excel.content.ContentDataTransformUtil;
import com.ray.export.request.ImportConfirmRequest;
import com.ray.export.request.IndexColums;
import com.ray.export.request.NameColums;
import com.ray.export.request.Page;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.data.DataTransform;
import com.ray.export.strategy.data.DataTransformResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author bo shen
 * @Description: 默认数转换
 * @Class: DefaultDataTransform
 * @Package com.ray.support.data
 * @date 2019/11/22 17:18
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class DefaultDataTransform implements DataTransform<Map<String, String>,Map<Integer, String>> {

    /***模板配置**/
    private Map<String, Map<String, String>> template = new HashMap<>();

    @Override
    public DataTransformResult<Map<String, String>> dataTransform(DataModel<Map<Integer, String>> dataModel) {
        initTemplate(dataModel.getImportConfirm());
        Map<String, ExcelSheet<Map<String, String>>> contents = transform(dataModel);
        return new DataTransformResult(contents);
    }

    /**
     * 转换文件内容
     *
     * @param dataModel
     * @return
     */
    private Map<String, ExcelSheet<Map<String, String>>> transform(DataModel<Map<Integer, String>> dataModel) {
        //返回结果
        Map<String, ExcelSheet<Map<String, String>>> result = new HashMap<>();
        try {
            Map<String, ExcelSheet<Map<Integer, String>>> content = dataModel.getContents();
            for (String key : content.keySet()) {
                log.info("开始解析文件--{}",key);
                ExcelSheet<Map<Integer, String>> excelSheet = content.get(key);
                List<Map<String, String>> list = new ArrayList<>();
                Map<String, String> obj = null;
                //数据直接转化
                //解析导入头  模板默认第一行为头
                Map<Integer, String> head = changeHeader(template.get(key), excelSheet.getHead());
                for (Map<Integer, String> data : excelSheet.getContents()) {
                    obj = mapTemplateTransform(head, data);
                    list.add(obj);
                }
                result.put(key, new ExcelSheet(key,head,list));
            }
        } catch (Exception e) {
            log.info("文件转换异常");
            throw  new RuntimeException("文件转换异常");
        }
        return result;
    }

    /**
     * 调用有模板进行模板解析
     * @param temps
     * @param head
     * @return
     */
    private Map<Integer,String> changeHeader(Map<String, String> temps, Map<Integer, String> head) {
        Map<Integer,String> temp = new HashMap<>();
        //没有模板直接返回head
        if(ObjectUtils.isEmpty(temps)){
            return  head;
        }
        //有模板进行模板解析
        for(Integer index : head.keySet()){
            temp.put(index,temps.get(head.get(index)));
        }
        return  temp;
    }

    /**
     * 读取文件模板头
     *
     * @param importConfirmRequest
     */
    private void initTemplate(ImportConfirmRequest<Object> importConfirmRequest) {
        for (Page<?> page : importConfirmRequest.getPages()) {
            //使用模型不需要数据转换
            if(page.getUseModel()){
                continue;
            }
            log.debug("标签页:{}-----------------开始----------", page.getSheetName());
            Map<String, String> sheetMap = new HashMap<>();
            for (Object col : page.getColums()) {
                if (col instanceof NameColums) {
                    NameColums nameColums = (NameColums) col;
                    sheetMap.put(nameColums.getName(), nameColums.getProperty());
                }
                if (col instanceof IndexColums) {
                    IndexColums indexColums = (IndexColums) col;
                    sheetMap.put(String.valueOf(indexColums.getIndex()), indexColums.getProperty());
                }
            }
            template.put(page.getSheetName(), sheetMap);
            log.debug("标签页:{}-----------------结束----------", page.getSheetName());
        }
    }

    /**
     * 数据转换
     *
     * @param head
     * @param data
     * @return
     */
    private Map<String, String> mapTemplateTransform(Map<Integer, String> head, Map<Integer, String> data) {
        Map<String, String> result = new HashMap<>();
        Map<String, String> dataMap = ContentDataTransformUtil.getExcelContentDataTransform(data.getClass()).excelContentDataTransform(data);
        for (int index : head.keySet()) {
            result.put(head.get(index), dataMap.get(String.valueOf(index)));
        }
        return result;
    }


}
