package com.ray.export.support.data;

import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.file.FileVerification;
import com.ray.export.strategy.file.FileVerificationResult;
import org.springframework.stereotype.Service;

/**
 * @author bo shen
 * @Description: 默认文件校验   同步
 * @Class: DefaultFileVerification
 * @Package com.ray.support.data
 * @date 2019/12/12 13:19
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
public class DefaultFileVerification implements FileVerification {

    @Override
    public FileVerificationResult fileVerification(DataModel dataModel) {
        FileVerificationResult fileVerificationResult =  new FileVerificationResult(false,dataModel.getTaskCode(),"文件默认校验");
        return fileVerificationResult;
    }
}
