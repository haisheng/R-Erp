package com.ray.export.thread;

import com.alibaba.fastjson.JSON;
import com.ray.export.exception.RunnerException;
import com.ray.export.runner.RunnerResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StopWatch;

/**
 * @author bo shen
 * @Description: 启动线程驱动
 * @Class: ThreadEventListener
 * @Package com.ray
 * @date 2019/11/26 11:05
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Component
@Slf4j
public class ErrorThreadEventListener implements ApplicationListener<ErrorThreadEvent> {
    @Autowired(required = false)
    private PlatformTransactionManager platformTransactionManager;
    @Autowired(required = false)
    private TransactionDefinition transactionDefinition;

    @Override
    @Async
    public void onApplicationEvent(ErrorThreadEvent event) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        //在有事务管理的情况下
        //开始事务配置
        TransactionStatus transaction = null;
        if (!ObjectUtils.isEmpty(platformTransactionManager)) {
            log.info("异常任务开启事务");
            // 获得事务状态
            transaction = platformTransactionManager.getTransaction(transactionDefinition);
        }
        try {
            //异步执行前执行
            RunnerResult result = event.doRun();
            if (!ObjectUtils.isEmpty(platformTransactionManager)) {
                platformTransactionManager.commit(transaction);
            }
            log.info("异常线程异步执行结果：{},结果参数：{}", result.getStatus(), JSON.toJSONString(result.getResult()));
        } catch (RunnerException e) {
            if (!ObjectUtils.isEmpty(platformTransactionManager)) {
                log.info("事务回滚：{}", event.getDataModel().getTemplateCode());
                platformTransactionManager.rollback(transaction);
            }
        }
        stopWatch.stop();
        log.info("任务:[{}]异常线程异步执行结束，耗时：{}",event.getDataModel().getTemplateCode(),stopWatch.getTotalTimeMillis());
    }

}
