package com.ray.export.thread;

import com.ray.export.runner.Runner;
import com.ray.export.runner.RunnerFactory;
import com.ray.export.runner.RunnerResult;
import com.ray.export.strategy.data.DataModel;
import lombok.Data;
import org.springframework.context.ApplicationEvent;

import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 线程驱动对象
 * @Class: ThreadEvent
 * @Package com.ray.thread
 * @date 2019/11/26 11:06
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class ErrorThreadEvent extends ApplicationEvent {

    private Annotation annotation;
    /**数据模型**/
    private DataModel dataModel;
    /***上次结果**/
    private RunnerResult runnerResult;


    public ErrorThreadEvent( Annotation annotation, DataModel dataModel, RunnerResult runnerResult) {
        super("不用调用");
        this.dataModel = dataModel;
        this.runnerResult = runnerResult;
        this.annotation = annotation;
    }

    /**事件执行**/
    public RunnerResult doRun() {
        Runner runner = RunnerFactory.getRunner(Error.class);
        return runner.doRunner(annotation, dataModel, runnerResult);
    }
}
