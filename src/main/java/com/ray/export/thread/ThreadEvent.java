package com.ray.export.thread;

import com.ray.export.runner.RunnerChain;
import com.ray.export.runner.RunnerResult;
import com.ray.export.strategy.data.DataModel;
import lombok.Data;
import org.springframework.context.ApplicationEvent;
import org.springframework.util.ObjectUtils;

import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 线程驱动对象
 * @Class: ThreadEvent
 * @Package com.ray.thread
 * @date 2019/11/26 11:06
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class ThreadEvent extends ApplicationEvent {
    /***配置**/
    private Annotation  annotation;
    /**数据模型**/
    private DataModel dataModel;
    /***上次结果**/
    private RunnerResult runnerResult;
    /**链路**/
    private RunnerChain runnerChain;


    public ThreadEvent(Annotation annotation, DataModel dataModel, RunnerResult runnerResult, RunnerChain runnerChain) {
        super("不用调用");
        this.annotation = annotation;
        this.dataModel = dataModel;
        this.runnerResult = runnerResult;
        this.runnerChain = runnerChain;
    }

    /**事件执行**/
    public RunnerResult doRun( RunnerResult result) {
        //前置结果放入扩展数据中
        if(!ObjectUtils.isEmpty(result)){
            dataModel.setBeforeResult(result.getResult());
            return runnerChain.doChain(annotation,dataModel,result);
        }else {
            dataModel.setBeforeResult(runnerResult.getResult());
            return runnerChain.doChain(annotation,dataModel,runnerResult);
        }
    }
}
