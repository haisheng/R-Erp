package com.ray.export.spring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author bo shen
 * @Description: spring上下文工具类
 * @Class: ApplicationContextUtil
 * @Package com.ray.spring
 * @date 2019/11/21 15:24
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */

@Component
@Slf4j
public class ApplicationContextUtil implements ApplicationContextAware {
    private static ApplicationContext context;

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        log.info("spring上下文被加载");
        context = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        return context;
    }
}