package com.ray.export.request;

import lombok.Data;

import java.util.List;

/**
 * @author bo shen
 * @Description:
 * @Class: Page
 * @Package com.ray.request
 * @date 2019/11/22 15:36
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class Page<T> {
    /**页签**/
    private String sheetName;
    /***是否使用模型*/
    private Boolean useModel = false;
    /***模型**/
    private Class<?> dataClazz;
    /**
     * 导入格式模板
     **/
    private List<T> colums;

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public List<T> getColums() {
        return colums;
    }

    public void setColums(List<T> colums) {
        this.colums = colums;
        this.useModel = false;
    }

    public Class<?> getDataClazz() {
        return dataClazz;
    }

    public void setDataClazz(Class<?> dataClazz) {
        this.useModel = true;
        this.dataClazz = dataClazz;
    }
}
