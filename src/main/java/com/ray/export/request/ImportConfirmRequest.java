package com.ray.export.request;

import com.ray.export.enums.PageColumsType;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author bo shen
 * @Description: 导入确认参数
 * @Class: ImportConfirmRequest
 * @Package com.ray.request
 * @date 2019/11/21 10:22
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class ImportConfirmRequest<T> implements Serializable {
    /** 确认任务唯一值  可以是文件url 也可以业务唯一值 **/
    private String taskKey;
    /*** 页模板*/
    private List<Page<T>> pages;
    /***页面模板***/
    private String pagesValue;
    /***名称配置 =name  顺序配置=index ***/
    private String type = PageColumsType.NAME.getValue();
}
