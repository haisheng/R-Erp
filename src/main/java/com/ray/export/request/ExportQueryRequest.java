package com.ray.export.request;

import lombok.Data;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

/**
 * @author jumWang
 * @Description: 导出查询参数
 * @Class: ExportQueryRequest
 * @Package com.ray.request
 * @date 2019/11/27 10:22
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class ExportQueryRequest<T> implements Serializable {


    /**
     * 确认任务唯一值  可以是文件url 也可以业务唯一值
     **/
    private String taskKey;

    /**查询参数*/
    private T queryParams;

    /**模板路径*/
    private String templatePath;


    /***
     * 获取泛型类型
     * @return
     */
    public Class<T> getTClass() {
        Class<T> tClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        return tClass;
    }
}
