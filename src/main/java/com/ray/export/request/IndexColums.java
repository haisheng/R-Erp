package com.ray.export.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author bo shen
 * @Description: 名称模板
 * @Class: IndexColums
 * @Package com.ray.request
 * @date 2019/11/21 10:26
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndexColums implements Serializable {
    /**
     * 位置
     */
    private Integer index;

    /**
     * 对应属性
     **/
    private String property;

    /**是否必填**/
    private Boolean require = false;
}
