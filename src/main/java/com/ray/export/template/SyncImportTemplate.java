package com.ray.export.template;

import com.ray.export.empty.Empty;
import com.ray.export.support.file.DefaultSyncFileVerification;
import com.ray.export.support.result.DefaultResult;

import java.lang.annotation.*;
import java.util.HashMap;

/**
 * 同步导入流程模板配置   所有策略通过--没有确认
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Template
public @interface SyncImportTemplate {
    //模板编码
    String templateCode();

    //支持文件类型
    String [] fileType() default {};

    //文件存储策略
    Class<?> fileStore() default Empty.class;

    //文件读取
    Class<?> fileContentRead() default Empty.class;

    //数据校验策略
    Class<?> dataVerification() default Empty.class;

    //日志写入策略
    Class<?> dataLog() default Empty.class;

    //文件校验
    Class<?> fileVerification() default DefaultSyncFileVerification.class;

    //文件模板加载
    Class<?> fileTemplate() default Empty.class;

    //数据转换
    Class<?> dataTransform() default Empty.class;

    //数据处理
    Class<?> dataHandling() default Empty.class;

    //成功回调策略
    Class<?> success() default Empty.class;

    //结果返回策略
    Class<?> result() default DefaultResult.class;
    
    //数据与传输对象
    Class<?> dataClazz() default HashMap.class;

    //错误回调策略
    Class<?> error() default Empty.class;

    //异步前置执行
    Class<?> beforeAsync() default Empty.class;

}
