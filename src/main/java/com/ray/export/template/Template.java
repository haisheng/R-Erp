package com.ray.export.template;

import java.lang.annotation.*;

/**
 * 元注解
 */
@Documented
@Target({ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Template {
}
