package com.ray.export.template;

import com.ray.export.empty.Empty;

import java.lang.annotation.*;
import java.util.HashMap;

/**
  导出流程模板配置
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Template
public @interface ExportTemplate {
    //模板编码
    String templateCode();

    //数据与传输对象
    Class<?> dataClazz() default HashMap.class;

    //数据统计策略
    Class<?> dataCount() default Empty.class;

    //异步策略
    Class<?> asyncVerification() default Empty.class;

    //分页策略
    Class<?> pageVerification() default Empty.class;

    //数据导出策略
    Class<?> dataExport() default Empty.class;

    //文件存储策略
    Class<?> exportFileStore() default Empty.class;

    //结果返回策略
    Class<?> result() default Empty.class;

    //成功回调策略
    Class<?> success() default Empty.class;

    //错误回调策略
    Class<?> error() default Empty.class;

    //异步前置执行
    Class<?> beforeAsync() default Empty.class;
}
