package com.ray.export.template.actuator;

import com.ray.export.template.ConfirmImportTemplate;
import com.ray.export.template.ExportTemplate;
import com.ray.export.template.ImportTemplate;
import com.ray.export.template.SyncImportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

/**
 * @author bo shen
 * @Description: 执行器工厂 用于注册 调度执行器
 * @Class: ActuatorFactory
 * @Package com.ray.template.actuator
 * @date 2019/11/21 9:45
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class ActuatorFactory {

    /**
     * 执行器列表
     **/
    private static Map<Class<?>, Actuator> actuators = new HashMap<Class<?>, Actuator>();

    //默认添加执行器对象
   static {
        //同步文件导入
        addActuator(SyncImportTemplate.class, new SyncImportTemplateActuator());
        //文件导入
        addActuator(ImportTemplate.class, new ImportTemplateActuator());
        //文件导入确认
        addActuator(ConfirmImportTemplate.class, new ConfirmExportTemplateActuator());
        //文件导出
        addActuator(ExportTemplate.class, new ExportTemplateActuator());
    }

    /**
     * 添加一个执行器 如果key相同会替换原来的执行器
     *
     * @param clazz
     * @param actuator
     */
    public static void addActuator(Class<?> clazz, Actuator actuator) {
        Assert.notNull(clazz, "参数[clazz]不能为空");
        Assert.notNull(actuator, "参数[actuator]不能为空");
        if (log.isDebugEnabled()) {
            log.debug("执行器{},{}被添加", clazz, actuator.getClass().getName());
        }
        actuators.put(clazz, actuator);
    }

    /**
     * 获取的一个执行器对象 如果没有获得  抛出异常
     *
     * @param clazz
     */
    public static Actuator getActuator(Class<?> clazz) {
        Assert.notNull(clazz, "参数[clazz]不能为空");
        Actuator actuator = actuators.get(clazz);
        if (ObjectUtils.isEmpty(actuator)) {
            throw new RuntimeException("无法找到执行器对象");
        }
        return actuator;
    }

    /**
     * 判断注解是否被注册
     * @param aClass
     * @return
     */
    public static boolean isSupport(Class<? extends Annotation> aClass) {
        return (actuators.containsKey(aClass));
    }
}
