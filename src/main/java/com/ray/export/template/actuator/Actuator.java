package com.ray.export.template.actuator;

import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 执行器接口  用于对应模板的执行器
 * @Class: Actuator
 * @Package com.ray.template.actuator
 * @date 2019/11/21 9:44
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface Actuator <E extends Annotation>{

    /**
     * 执行器运行方法
     * @param params
     * @return
     */
    Object  run(E annotation, Object... params);
}
