package com.ray.export.template.actuator;

import com.ray.export.builder.DataModelBuilder;
import com.ray.export.runner.RunnerChain;
import com.ray.export.runner.RunnerFactory;
import com.ray.export.runner.RunnerResult;
import com.ray.export.strategy.data.DataHandling;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.data.DataTransform;
import com.ray.export.strategy.data.DataVerification;
import com.ray.export.strategy.file.FileContentRead;
import com.ray.export.strategy.file.FileLoad;
import com.ray.export.strategy.file.FileTemplate;
import com.ray.export.strategy.file.FileVerification;
import com.ray.export.strategy.result.Result;
import com.ray.export.strategy.result.Success;
import com.ray.export.template.ConfirmImportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.util.StopWatch;

/**
 * @author bo shen
 * @Description: 导入模板执行器
 * @Class: ImportTemplateActuator
 * @Package com.ray.template.actuator
 * @date 2019/11/21 11:29
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class ConfirmExportTemplateActuator implements Actuator<ConfirmImportTemplate>{
    public Object run(ConfirmImportTemplate annotation,Object... params) {
        log.info("当前执行导入确认任务[{}].",annotation.templateCode());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        //参数校验及解析  上传任务接收对象必须有 MultipartFile 对象
        //构造数据传输节点
        DataModel dataModel = DataModelBuilder.build(params);
        Assert.notNull(dataModel.getImportConfirm(),"确认参数不能为空");
        dataModel.setTemplateCode(annotation.templateCode());
        dataModel.setDataClazz(annotation.dataClazz());
        //读取模板配置 --生成执行链
        RunnerChain chain =  new RunnerFactory()
                .add(annotation.fileLoad(), FileLoad.class)
                .add(annotation.fileVerification(), FileVerification.class)
                .add(annotation.fileTemplate(), FileTemplate.class)
                .add(annotation.fileContentRead(), FileContentRead.class)
                .add(annotation.dataTransform(), DataTransform.class)
                .add(annotation.dataVerification(), DataVerification.class)
                .add(annotation.dataHandling(), DataHandling.class)
                .add(annotation.success(), Success.class)
                .add(annotation.result(), Result.class).getHead();
        //执行配置链
        RunnerResult runnerResult = chain.doChain(annotation,dataModel,null);
        stopWatch.stop();
        log.info("前执行导入确认任务[{}]执行结束，耗时：{}",annotation.templateCode(),stopWatch.getTotalTimeMillis());
        return runnerResult.getResult();
    }
}
