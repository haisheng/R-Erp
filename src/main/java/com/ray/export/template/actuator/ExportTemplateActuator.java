package com.ray.export.template.actuator;

import com.ray.export.builder.DataModelBuilder;
import com.ray.export.runner.RunnerChain;
import com.ray.export.runner.RunnerFactory;
import com.ray.export.runner.RunnerResult;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.exports.*;
import com.ray.export.strategy.result.Result;
import com.ray.export.strategy.result.Success;
import com.ray.export.template.ExportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.util.StopWatch;

/**
 * @author bo shen
 * @Description: 导出模板执行器
 * @Class: ExportTemplateActuator
 * @Package com.ray.template.actuator
 * @date 2019/11/21 11:29
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class ExportTemplateActuator implements Actuator<ExportTemplate>{
    public Object run(ExportTemplate annotation,Object... params) {
        log.info("当前执行导出任务[{}].",annotation.templateCode());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        //参数校验及解析  上传任务接收对象必须有 MultipartFile 对象
        //构造数据传输节点
        DataModel dataModel = DataModelBuilder.build(params);
        dataModel.setTemplateCode(annotation.templateCode());
        dataModel.setDataClazz(annotation.dataClazz());
        Assert.notNull(dataModel.getDataClazz(),"数据与传输对象不能为空");
        //读取模板配置 --生成执行链
        RunnerChain chain =  new RunnerFactory()
                .add(annotation.dataCount(), DataCount.class)
                .add(annotation.pageVerification(), PageVerification.class)
                .add(annotation.asyncVerification(), AsyncVerification.class)
                .add(annotation.dataExport(), DataExport.class)
                .add(annotation.exportFileStore(), ExportFileStore.class)
                .add(annotation.success(), Success.class)
                .add(annotation.result(), Result.class).getHead();
        //执行配置链
        RunnerResult runnerResult = chain.doChain(annotation,dataModel,null);
        stopWatch.stop();
        log.info("当前执行导出任务[{}]执行结束，耗时：{}",annotation.templateCode(),stopWatch.getTotalTimeMillis());
        return runnerResult.getResult();
    }
}
