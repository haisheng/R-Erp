package com.ray.export.template.actuator;

import com.ray.export.builder.DataModelBuilder;
import com.ray.export.runner.RunnerChain;
import com.ray.export.runner.RunnerFactory;
import com.ray.export.runner.RunnerResult;
import com.ray.export.runner.imports.FileTypeValidateRunner;
import com.ray.export.strategy.data.DataLog;
import com.ray.export.strategy.data.DataModel;
import com.ray.export.strategy.file.FileStore;
import com.ray.export.strategy.file.HeadRead;
import com.ray.export.strategy.result.Result;
import com.ray.export.strategy.result.Success;
import com.ray.export.template.ImportTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.util.StopWatch;

/**
 * @author bo shen
 * @Description: 导出模板执行器
 * @Class: ImportTemplateActuator
 * @Package com.ray.template.actuator
 * @date 2019/11/21 11:29
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class ImportTemplateActuator implements Actuator<ImportTemplate>{
    public Object run(ImportTemplate annotation,Object... params) {
        log.info("当前执行导入任务[{}].",annotation.templateCode());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        //参数校验及解析  上传任务接收对象必须有 MultipartFile 对象
        //构造数据传输节点
        DataModel dataModel = DataModelBuilder.build(params);
        dataModel.setTemplateCode(annotation.templateCode());
        dataModel.setDataClazz(annotation.dataClazz());
        Assert.notNull(dataModel.getFile(),"方法无文件对象[MultipartFile],上传文件请用MultipartFile 接收");
        //读取模板配置 --生成执行链
        RunnerChain chain =  new RunnerFactory().add(new FileTypeValidateRunner())
                .add(annotation.dataLog(), DataLog.class)
                .add(annotation.fileStore(), FileStore.class)
                .add(annotation.headRead(), HeadRead.class)
                .add(annotation.success(), Success.class)
                .add(annotation.result(), Result.class).getHead();
        //执行配置链
        RunnerResult runnerResult = chain.doChain(annotation,dataModel,null);
        stopWatch.stop();
        log.info("当前执行导入任务[{}]执行结束，耗时：{}",annotation.templateCode(),stopWatch.getTotalTimeMillis());
        return runnerResult.getResult();
    }
}
