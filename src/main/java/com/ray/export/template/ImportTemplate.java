package com.ray.export.template;

import com.ray.export.empty.Empty;
import com.ray.export.support.result.DefaultImportResult;

import java.lang.annotation.*;
import java.util.HashMap;

/**
 * 导入流程模板配置   所有策略通过
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Template
public @interface ImportTemplate {
    //模板编码
    String templateCode();

    //支持文件类型
    String [] fileType() default {};

    //文件存储策略
    Class<?> fileStore() default Empty.class;

    //文件头读取
    Class<?> headRead() default Empty.class;

    //日志写入策略
    Class<?> dataLog() default Empty.class;

    //成功回调策略
    Class<?> success() default Empty.class;

    //结果返回策略
    Class<?> result() default DefaultImportResult.class;
    
    //数据与传输对象
    Class<?> dataClazz() default HashMap.class;

    //错误回调策略
    Class<?> error() default Empty.class;

    //异步前置执行
    Class<?> beforeAsync() default Empty.class;

}
