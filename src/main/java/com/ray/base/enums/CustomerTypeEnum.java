package com.ray.base.enums;

import lombok.Getter;
import org.springframework.util.StringUtils;

/**
 * @author bo shen
 * @Description: 客户类型类型
 * @Class: CustomerTypeEnum
 * @Package com.ray.base.enums
 * @date 2020/5/27 10:50
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Getter
public enum CustomerTypeEnum {
    CUSTOMER("CUSTOMER", "客户"),
    SUPPLIER("SUPPLIER", "供应商"),
    HANDLE("HANDLE", "加工企业"),;

    CustomerTypeEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    /***值***/
    private String value;
    /***名称描述***/
    private String name;

    /**
     * 获取枚举对象
     *
     * @param value 值
     * @return
     */
    public static CustomerTypeEnum getEnum(String value) {
        for (CustomerTypeEnum entity : CustomerTypeEnum.values()) {
            if (StringUtils.pathEquals(value, entity.value)) {
                return entity;
            }
        }
        return null;
    }

    /**
     * value是否在列表中
     *
     * @param value
     * @return
     */
    public static boolean valueInEnum(String value) {
        for (CustomerTypeEnum entity : CustomerTypeEnum.values()) {
            if (StringUtils.pathEquals(value, entity.value)) {
                return true;
            }
        }
        return false;
    }
}
