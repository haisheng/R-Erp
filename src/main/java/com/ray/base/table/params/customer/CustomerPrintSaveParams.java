package com.ray.base.table.params.customer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 客户模板新增对象
 * @Class: CustomerCreateParams
 * @Package com.ray.system.table.params.position
 * @date 2020/5/27 11:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("客户模板新增对象")
public class CustomerPrintSaveParams  {

    @ApiModelProperty("客户类型")
    @NotBlank(message = "客户类型不能能为空")
    @Length(max = 50, message = "客户类型长度不能超过50")
    private String customerCode;

    @ApiModelProperty("模板内容")
    @NotBlank(message = "模板内容不能能为空")
    private String template;
}
