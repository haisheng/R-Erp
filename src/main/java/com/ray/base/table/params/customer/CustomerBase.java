package com.ray.base.table.params.customer;

import com.ray.base.table.params.material.PropParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author bo shen
 * @Description: 基础属性
 * @Class: MaterialTypeBase
 * @Package com.ray.base.table.params.customer
 * @date 2020/6/1 13:35
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class CustomerBase {

    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称", required = true)
    @NotBlank(message = "客户名称不能为空")
    @Length(max = 50, message = "客户名称长度不能超过50")
    private String customerName;

    /**
     * 联系人
     */
    @ApiModelProperty(value = "联系人", required = false)
    @Length(max = 50, message = "联系人长度不能超过50")
    private String linkName;

    /**
     * 联系电话
     */
    @ApiModelProperty(value = "联系电话", required = false)
    @Length(max = 50, message = "联系电话长度不能超过50")
    private String linkMoblie;

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址", required = false)
    private String address;

    @Valid
    @ApiModelProperty(value = "扩展属性", required = true)
    @NotNull(message = "扩展属性不能为空")
    private List<PropParams> props;
}
