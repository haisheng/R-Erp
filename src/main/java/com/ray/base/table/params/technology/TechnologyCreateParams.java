package com.ray.base.table.params.technology;

import com.ray.base.table.params.material.PropParams;
import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author bo shen
 * @Description: 技术工艺对象
 * @Class: TechnologyVO
 * @Package com.ray.base.table.vo.technology
 * @date 2020/6/27 12:57
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class TechnologyCreateParams extends BaseParams {


    /**
     * 工艺编号
     */
    @ApiModelProperty("工艺编号")
    @NotBlank(message = "工艺编号不能为空")
    private String technologyCode;
    /**
     * 型号编码
     */
    @ApiModelProperty("型号编码")
    @NotBlank(message = "型号编码不能为空")
    private String modelCode;

    /**
     * 工艺方式
     */
    private String stepMothed;


    /**
     * 工艺说明
     */
    private String remark;

    /**
     * 工艺原料
     */
    @ApiModelProperty("工艺原料")
    @NotNull(message = "工艺原料不能为空")
    @Valid
    private List<TechnologyGoodsParams> goods;

    @Valid
    @ApiModelProperty(value = "扩展属性", required = true)
    @NotNull(message = "扩展属性不能为空")
    private List<PropParams> props;
}
