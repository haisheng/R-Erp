package com.ray.base.table.params.material.model;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 规格查询
 * @Class: MaterialTypeQueryParams
 * @Package com.ray.system.table.params.customer
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("规格查询")
@Data
public class MaterialModelQueryParams  extends BaseParams {


    /**
     * 物料编码
     */
    @ApiModelProperty(value = "物料编码")
    private String materialCode;
    /**
     * 规格名称
     */
    @ApiModelProperty("规格名称-模糊查询")
    private String modelNameLike;
    /**
     * 规格名称
     */
    @ApiModelProperty("规格名称")
    private String modelName;

    /**
     * 名称
     */
    @ApiModelProperty("名称-模糊")
    private String nameLike;
    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 物料类型
     */
    @ApiModelProperty(value = "物料类型", required = true)
    private String materialType;

    @ApiModelProperty("业务订单编号")
    private String businessOrderNo;

}
