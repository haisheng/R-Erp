package com.ray.base.table.params.material.type;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 基础属性
 * @Class: MaterialTypeBase
 * @Package com.ray.base.table.params.customer
 * @date 2020/6/1 13:35
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class MaterialTypeBase {

    /**
     * 类型名称
     */
    @ApiModelProperty(value = "类型名称名称", required = true)
    @NotBlank(message = "类型名称不能为空")
    @Length(max = 50, message = "类型名称长度不能超过50")
    private String typeName;

   
}
