package com.ray.base.table.params.material;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author bo shen
 * @Description: 基础
 * @Class: MaterialBase
 * @Package com.ray.base.table.params.material
 * @date 2020/6/1 16:31
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class MaterialBase {

    /**
     * 物料名称
     */
    @ApiModelProperty(value = "物料名称", required = true)
    @NotBlank(message = "物料名称不能为空")
    @Length(max = 50, message = "物料名称长度不能超过50")
    private String materialName;



    /**
     * 物料名称-别名
     */
    @ApiModelProperty(value = "物料名称-别名", required = true)
    @Length(max = 50, message = "物料名称-别名长度不能超过50")
    private String materialNameEn;

    /**
     * 物料类型
     */
    @ApiModelProperty(value = "物料类型", required = true)
    @NotBlank(message = "物料类型不能为空")
    @Length(max = 50, message = "物料类型长度不能超过50")
    private String materialType;

    /**
     * 单位
     */
    @ApiModelProperty(value = "单位", required = true)
    @NotBlank(message = "单位不能为空")
    @Length(max = 50, message = "单位长度不能超过50")
    private String unit;


    /**
     * 重量
     */
    @ApiModelProperty(value = "重量", required = true)
    private String weight;

    /**
     * 重量差
     */
    private BigDecimal weightDiff;

    /**
     * 体积
     */
    @ApiModelProperty(value = "体积", required = true)
    private String volume;


    @Valid
    @ApiModelProperty(value = "扩展属性", required = true)
    @NotNull(message = "扩展属性不能为空")
    private List<PropParams> props;
}
