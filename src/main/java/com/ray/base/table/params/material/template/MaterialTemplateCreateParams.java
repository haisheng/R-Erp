package com.ray.base.table.params.material.template;

import com.ray.base.table.params.material.model.MaterialModelBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 新增
 * @Class: MaterialTypeCreateParams
 * @Package com.ray.system.table.params.position
 * @date 2020/5/27 11:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("物料模板新增对象")
public class MaterialTemplateCreateParams extends MaterialTemplateBase{


}
