package com.ray.base.table.params.material;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 规格查询
 * @Class: MaterialTypeQueryParams
 * @Package com.ray.system.table.params.customer
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("物料查询")
@Data
public class MaterialQueryParams extends BaseParams {

    @ApiModelProperty("物料名称-模糊查询")
    private String materialNameLike;

    @ApiModelProperty("物料名称")
    private String materialName;

    @ApiModelProperty("物料类型")
    private String materialType;

}
