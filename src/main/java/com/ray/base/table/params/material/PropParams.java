package com.ray.base.table.params.material;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 属性
 * @Class: PropParams
 * @Package com.ray.base.table.params
 * @date 2020/6/1 16:36
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("属性值")
public class PropParams {

    /**
     * 属性名称
     */
    @ApiModelProperty(value = "属性名称", required = true)
    @NotBlank(message = "属性名称不能为空")
    @Length(max = 50, message = "属性名称长度不能超过50")
    private String name;

    /**
     * 属性名
     */
    @ApiModelProperty(value = "属性名", required = true)
    @NotBlank(message = "属性名不能为空")
    @Length(max = 50, message = "属性名长度不能超过50")
    private String prop;

    /**
     * 属性值
     */
    @ApiModelProperty(value = "属性值", required = false)
    @Length(max = 50, message = "属性值不能超过50")
    private String value;
}
