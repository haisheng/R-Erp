package com.ray.base.table.params.material.template;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 基础属性
 * @Class: MaterialTypeBase
 * @Package com.ray.base.table.params.customer
 * @date 2020/6/1 13:35
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class MaterialTemplateBase {


    /**
     * 属性名称
     */
    @ApiModelProperty(value = "属性名称", required = true)
    @NotBlank(message = "属性名称不能为空")
    @Length(max = 50, message = "属性名称长度不能超过50")
    private String name;

    /**
     * 属性名
     */
    @ApiModelProperty(value = "属性名", required = true)
    @NotBlank(message = "属性名不能为空")
    @Length(max = 50, message = "属性名长度不能超过50")
    private String prop;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序", required = false)
    private Integer indexSort = 0;

   
}
