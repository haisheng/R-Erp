package com.ray.base.table.params.material.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 基础属性
 * @Class: MaterialTypeBase
 * @Package com.ray.base.table.params.customer
 * @date 2020/6/1 13:35
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class MaterialModelBase {

    /**
     * 型号/规格名称
     */
    @ApiModelProperty(value = "型号/规格名称", required = true)
    @NotBlank(message = "型号/规格名称不能为空")
    @Length(max = 50, message = "型号/规格名称长度不能超过50")
    private String modelName;


    /**
     * 扩展名称
     */
    @ApiModelProperty(value = "扩展名称", required = false)
    @Length(max = 50, message = "扩展名称长度不能超过50")
    private String name;

    /**
     * 规格属性
     */
    @ApiModelProperty(value = "规格属性", required = false)
    @Length(max = 50, message = "规格属性长度不能超过50")
    private String modelProp;

    /**
     * 条形码
     */
    @ApiModelProperty(value = "条形码名称", required = false)
    @Length(max = 50, message = "条形码长度不能超过50")
    private String barCode;

   
}
