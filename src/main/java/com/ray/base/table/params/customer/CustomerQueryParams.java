package com.ray.base.table.params.customer;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author bo shen
 * @Description: 客户查询
 * @Class: CustomerQueryParams
 * @Package com.ray.base.table.params.customer
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("客户查询")
@Data
public class CustomerQueryParams extends BaseParams {

    @ApiModelProperty("客户类型")
    private String customerType;

    @ApiModelProperty("客户名称-模糊查询")
    private String customerNameLike;

    @ApiModelProperty("客户名称")
    private String customerName;

    @ApiModelProperty(value = "联系人", required = false)
    private String linkName;

    @ApiModelProperty(value = "联系电话", required = false)
    private String linkMoblie;

    @ApiModelProperty(value = "客户列", required = false,hidden = true)
    private List<String> customerCodes;

    private List<String> customerTypes;

}
