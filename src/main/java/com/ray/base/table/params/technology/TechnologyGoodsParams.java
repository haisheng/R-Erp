package com.ray.base.table.params.technology;

import com.ray.base.table.vo.material.model.MaterialModelVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 技术工艺商品对象
 * @Class: TechnologyVO
 * @Package com.ray.base.table.vo.technology
 * @date 2020/6/27 12:57
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class TechnologyGoodsParams {
    /**
     * 工艺  原料类型
     */
    @ApiModelProperty("原料类型")
    @NotBlank(message = "原料类型不能为空")
    private String modelType;

    /**
     * 型号编码
     */
    @ApiModelProperty("型号编码")
    @NotBlank(message = "型号编码不能为空")
    private String modelCode;

    /**
     * 单位
     */
    @ApiModelProperty("单位编码")
    @NotBlank(message = "单位编码不能为空")
    private String unit;

    /**
     * 用量
     */
    @ApiModelProperty("用量")
    @NotNull(message = "用量不能为空")
    @DecimalMin(value = "0.0001", message = "用量大于0")
    private BigDecimal useQuantity;

    public String getKey(){
        return String.format("%s|%s",modelCode,unit);
    }
}
