package com.ray.base.table.params.material.type;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 职位新增
 * @Class: MaterialTypeCreateParams
 * @Package com.ray.system.table.params.position
 * @date 2020/5/27 11:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("类型新增对象")
public class MaterialTypeCreateParams extends MaterialTypeBase{

}
