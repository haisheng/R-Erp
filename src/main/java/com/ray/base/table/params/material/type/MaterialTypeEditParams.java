package com.ray.base.table.params.material.type;

import com.ray.base.table.params.customer.CustomerBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 类型编辑
 * @Class: MaterialTypeEditParams
 * @Package com.ray.system.table.params.position
 * @date 2020/5/27 11:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("类型编辑对象")
public class MaterialTypeEditParams extends MaterialTypeBase {
    @ApiModelProperty(value = "类型编码", required = true)
    @NotBlank(message = "类型编码不能为空")
    @Length(max = 50, message = "类型编码长度不能超过50")
    private String typeCode;

}
