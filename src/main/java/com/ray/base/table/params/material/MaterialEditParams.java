package com.ray.base.table.params.material;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author bo shen
 * @Description: 职位新增
 * @Class: MaterialTypeCreateParams
 * @Package com.ray.system.table.params.position
 * @date 2020/5/27 11:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("物料新增对象")
public class MaterialEditParams extends MaterialBase {

    /**
     * 物料编码
     */
    @ApiModelProperty(value = "物料编码", required = true)
    @NotBlank(message = "物料编码不能为空")
    @Length(max = 50, message = "物料编码长度不能超过50")
    private String materialCode;

}
