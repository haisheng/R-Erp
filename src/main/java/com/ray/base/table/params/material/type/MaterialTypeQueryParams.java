package com.ray.base.table.params.material.type;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 类型查询
 * @Class: MaterialTypeQueryParams
 * @Package com.ray.system.table.params.customer
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("类型查询")
@Data
public class MaterialTypeQueryParams  extends BaseParams {

    

    @ApiModelProperty("类型名称-模糊查询")
    private String typeNameLike;

    @ApiModelProperty("类型名称")
    private String typeName;

}
