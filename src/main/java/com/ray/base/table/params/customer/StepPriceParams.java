package com.ray.base.table.params.customer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 属性
 * @Class: PropParams
 * @Package com.ray.base.table.params
 * @date 2020/6/1 16:36
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("工序价格")
public class StepPriceParams {



    /**
     * 工序
     */
    @ApiModelProperty(value = "工序", required = true)
    @NotBlank(message = "工序不能为空")
    @Length(max = 50, message = "工序长度不能超过50")
    private String stepCode;

    /**
     * 加工单价
     */
    @ApiModelProperty(value = "加工单价", required = false)
    @NotNull(message = "加工单价不能为空")
    @Min(value = 0,message = "加工单价不能是负数")
    private BigDecimal price;
}
