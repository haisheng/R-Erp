package com.ray.base.table.params;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 文件对象
 * @Class: FileParams
 * @Package com.ray.base.table.params
 * @date 2020/7/14 11:09
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class FileParams {

    private String fileName;

    private String fileUrl;

}
