package com.ray.base.table.params.material.template;

import com.ray.common.BaseParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 查询查询
 * @Class: MaterialTypeQueryParams
 * @Package com.ray.system.table.params.customer
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@ApiModel("规格查询")
@Data
public class MaterialTemplateQueryParams  extends BaseParams {


    @ApiModelProperty("名称-模糊查询")
    private String nameLike;

    @ApiModelProperty("规格")
    private String name;

    @ApiModelProperty("属性")
    private String prop;

}
