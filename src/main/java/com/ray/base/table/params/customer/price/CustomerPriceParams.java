package com.ray.base.table.params.customer.price;

import com.ray.base.table.params.customer.CustomerBase;
import com.ray.base.table.params.customer.StepPriceParams;
import com.ray.base.table.params.material.PropParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author bo shen
 * @Description: 客户新增
 * @Class: CustomerCreateParams
 * @Package com.ray.system.table.params.position
 * @date 2020/5/27 11:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("客户加工单价")
public class CustomerPriceParams  {

    @ApiModelProperty(value = "客户编码", required = true)
    @NotBlank(message = "客户编码不能为空")
    @Length(max = 50, message = "客户编码长度不能超过50")
    private String customerCode;

    @Valid
    @ApiModelProperty(value = "加工单价", required = true)
    @NotNull(message = "加工单价不能为空")
    private List<StepPriceParams> props;
}
