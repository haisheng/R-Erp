package com.ray.base.table.mapper;

import com.ray.base.table.entity.BaseMaterialExtend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户扩展信息 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-01
 */
public interface BaseMaterialExtendMapper extends BaseMapper<BaseMaterialExtend> {

}
