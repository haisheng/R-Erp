package com.ray.base.table.mapper;

import com.ray.base.table.entity.BaseCustomer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统应用表 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-01
 */
public interface BaseCustomerMapper extends BaseMapper<BaseCustomer> {

}
