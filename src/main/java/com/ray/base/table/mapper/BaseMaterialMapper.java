package com.ray.base.table.mapper;

import com.ray.base.table.entity.BaseMaterial;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 物料/产品 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-01
 */
public interface BaseMaterialMapper extends BaseMapper<BaseMaterial> {

}
