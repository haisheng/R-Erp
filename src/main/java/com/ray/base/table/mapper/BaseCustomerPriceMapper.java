package com.ray.base.table.mapper;

import com.ray.base.table.entity.BaseCustomerPrice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客户表加工费用配置 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-07-11
 */
public interface BaseCustomerPriceMapper extends BaseMapper<BaseCustomerPrice> {

}
