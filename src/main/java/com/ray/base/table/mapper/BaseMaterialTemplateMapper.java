package com.ray.base.table.mapper;

import com.ray.base.table.entity.BaseMaterialTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品属性模板定义 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-01
 */
public interface BaseMaterialTemplateMapper extends BaseMapper<BaseMaterialTemplate> {

}
