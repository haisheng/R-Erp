package com.ray.base.table.mapper;

import com.ray.base.table.entity.BaseCustomerPrint;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客户-唛头 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-07-04
 */
public interface BaseCustomerPrintMapper extends BaseMapper<BaseCustomerPrint> {

}
