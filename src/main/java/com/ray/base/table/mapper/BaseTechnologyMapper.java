package com.ray.base.table.mapper;

import com.ray.base.table.entity.BaseTechnology;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 技术工艺 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-27
 */
public interface BaseTechnologyMapper extends BaseMapper<BaseTechnology> {

}
