package com.ray.base.table.mapper;

import com.ray.base.table.entity.BaseMaterialType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 物料类型 Mapper 接口
 * </p>
 *
 * @author shenbo
 * @since 2020-06-01
 */
public interface BaseMaterialTypeMapper extends BaseMapper<BaseMaterialType> {

}
