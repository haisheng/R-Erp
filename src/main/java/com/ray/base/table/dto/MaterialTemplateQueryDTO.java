package com.ray.base.table.dto;

import com.ray.common.dto.BaseDTO;
import lombok.Data;

/**
 * @author bo shen
 * @Description:原料模板查询
 * @Class: MaterialTemplateQueryDTO
 * @Package com.ray.system.table.dto
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class MaterialTemplateQueryDTO extends BaseDTO {

    /*** 属性名称模糊*/
    private String nameLike;
    /*** 属性名称*/
    private String name;
    /*** 属性*/
    private String prop;

}
