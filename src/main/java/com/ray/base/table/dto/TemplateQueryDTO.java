package com.ray.base.table.dto;

import com.ray.common.dto.BaseDTO;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 模板属性查询
 * @Class: TemplateQueryDTO
 * @Package com.ray.system.table.dto
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class TemplateQueryDTO extends BaseDTO {

    /*** 模板属性名称模糊 */
    private String nameLike;

    /**
     * 模板属性名称
     */
    private String name;
    /**
     * 模板属性
     */
    private String prop;

    /**
     * 模板类型
     */
    private String templateType;

}
