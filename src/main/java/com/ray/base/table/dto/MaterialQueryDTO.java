package com.ray.base.table.dto;

import com.ray.common.dto.BaseDTO;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 物料查询
 * @Class: MaterialQueryDTO
 * @Package com.ray.base.table.dto
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class MaterialQueryDTO extends BaseDTO {

    /*** 物料名称-模糊 */
    private String materialNameLike;
    /*** 物料名称*/
    private String materialName;
    /** 物料类型*/
    private String materialType;

}
