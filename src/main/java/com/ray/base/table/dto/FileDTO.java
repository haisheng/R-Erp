package com.ray.base.table.dto;

import lombok.Data;

/**
 * @author bo shen
 * @Description: 文件对象
 * @Class: FileDTO
 * @Package com.ray.base.table.dto
 * @date 2020/7/14 11:09
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class FileDTO {

    /**文件名称**/
    private String fileName;
    /**文件地址**/
    private String fileUrl;

}
