package com.ray.base.table.dto;

import com.ray.common.dto.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author bo shen
 * @Description: 客户查询
 * @Class: CustomerQueryDTO
 * @Package com.ray.base.table.dto
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class CustomerQueryDTO extends BaseDTO {

    /** 客户类型*/
    private String customerType;
    /*** 客户名称-模糊查询*/
    private String customerNameLike;
    /*** 客户名称*/
    private String customerName;
    /*** 联系人*/
    private String linkName;
    /*** 联系电话*/
    private String linkMoblie;
    /*** 列表 */
    private List<String> customerCodes;
    private List<String> customerTypes;

}
