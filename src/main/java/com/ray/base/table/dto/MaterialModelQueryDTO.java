package com.ray.base.table.dto;

import com.ray.common.dto.BaseDTO;
import lombok.Data;

import java.util.List;

/**
 * @author bo shen
 * @Description: 规格查询
 * @Class: MaterialModelQueryDTO
 * @Package com.ray.base.table.dto
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class MaterialModelQueryDTO extends BaseDTO {

    /** 物料编码*/
    private String materialCode;
    /** 型号名称-模糊*/
    private String modelNameLike;
    /** * 型号名称 */
    private String modelName;
    /*** 物料类型 */
    private String materialType;
    /*** 在列表中的 */
    private List<String> modelCodes;
    /*** 名称*/
    private String nameLike;
    /*** 名称 */
    private String name;

}
