package com.ray.base.table.dto;

import com.ray.common.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 类型查询
 * @Class: MaterialTypeQueryDTO
 * @Package com.ray.system.table.dto
 * @date 2020/5/27 11:07
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class MaterialTypeQueryDTO extends BaseDTO {

    /*** 类型名称-模糊 */
    private String typeNameLike;
    /*** 类型名称*/
    private String typeName;

}
