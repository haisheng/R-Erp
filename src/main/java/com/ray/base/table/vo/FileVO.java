package com.ray.base.table.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description:
 * @Class: FileVO
 * @Package com.ray.base.table.vo
 * @date 2020/7/13 15:26
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class FileVO {

    @ApiModelProperty("文件路径")
    private String fileUrl;
    @ApiModelProperty("全路径")
    private String fullUrl;
    @ApiModelProperty("文件编码")
    private String fileCode;
    @ApiModelProperty("文件名称")
    private String fileName;
}
