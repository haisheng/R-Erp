package com.ray.base.table.vo.material.model;

import com.ray.base.table.vo.material.MaterialVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 基础属性
 * @Class: MaterialTypeBase
 * @Package com.ray.base.table.params.customer
 * @date 2020/6/1 13:35
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class MaterialModelVO extends MaterialVO {


    @ApiModelProperty(value = "规格编码", required = true)
    private String modelCode;

    /**
     * 规格名称
     */
    @ApiModelProperty(value = "规格名称", required = true)
    private String modelName;


    /**
     * 名称
     */
    @ApiModelProperty(value = "名称", required = true)
    private String name;

    /**
     * 物料编码
     */
    @ApiModelProperty(value = "物料编码", required = true)
    private String materialCode;

    /**
     * 条形码
     */
    @ApiModelProperty(value = "条形码", required = true)
    private String barCode;

    /**
     * 规格属性
     */
    @ApiModelProperty(value = "规格属性", required = false)
    private String modelProp;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量", required = false)
    private BigDecimal quantity;


    /**
     * 完成数量
     */
    @ApiModelProperty(value = "完成数量", required = false)
    private BigDecimal finishQuantity;


    /**
     * 数据编号
     */
    @ApiModelProperty(value = "数据编号", required = false)
    private String dataKey;



    public  String getKey(){
        return   String.format("%s|%s",modelCode,getUnit());
    }

}
