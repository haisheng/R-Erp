package com.ray.base.table.vo.customer;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 客户对象
 * @Class: CustomerVO
 * @Package com.ray.base.table.vo
 * @date 2020/6/1 13:45
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("客户模板信息")
public class CustomerPrintVO extends BaseVO {

    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号", required = true)
    private String customerCode;
    /**
     * 模板内容
     */
    @ApiModelProperty(value = "模板内容", required = true)
    private String template;


}
