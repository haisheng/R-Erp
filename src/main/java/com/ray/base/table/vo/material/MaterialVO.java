package com.ray.base.table.vo.material;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author bo shen
 * @Description: 基础属性
 * @Class: MaterialTypeBase
 * @Package com.ray.base.table.params.customer
 * @date 2020/6/1 13:35
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class MaterialVO extends BaseVO{


    /**
     * 物料编码
     */
    @ApiModelProperty(value = "物料编码", required = true)
    private String materialCode;

    /**
     * 物料名称
     */
    @ApiModelProperty(value = "物料名称", required = true)
    private String materialName;

    /**
     * 别名
     */
    @ApiModelProperty(value = "别名", required = true)
    private String materialNameEn;

    /**
     * 物料类型
     */
    @ApiModelProperty(value = "物料类型", required = true)
    private String materialType;

    /**
     * 物料类型名称
     */
    @ApiModelProperty(value = "物料类型名称", required = true)
    private String materialTypeName;

    /**
     * 单位
     */
    @ApiModelProperty(value = "单位", required = true)
    private String unit;



    /**
     * 重量
     */
    @ApiModelProperty(value = "重量", required = true)
    private String weight;


    /**
     * 重量差
     */
    private BigDecimal weightDiff;

    /**
     * 体积
     */
    @ApiModelProperty(value = "体积", required = true)
    private String volume;


    @ApiModelProperty(value = "扩展属性", required = true)
    private List<PropVO> props;


}
