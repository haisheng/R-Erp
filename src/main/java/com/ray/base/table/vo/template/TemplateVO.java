package com.ray.base.table.vo.template;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 基础属性
 * @Class: MaterialTypeBase
 * @Package com.ray.base.table.params.customer
 * @date 2020/6/1 13:35
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class TemplateVO extends BaseVO {

    /**
     * 模板编号
     */
    @ApiModelProperty(value = "模板编号", required = true)
    private String templateCode;

    /**
     * 模板类型
     */
    @ApiModelProperty(value = "模板类型", required = true)
    private String templateType;
    /**
     * 属性名称
     */
    @ApiModelProperty(value = "属性名称", required = true)
    private String name;

    /**
     * 属性名
     */
    @ApiModelProperty(value = "属性名", required = true)
    private String prop;

    /**
     * 公司编码
     */
    @ApiModelProperty(value = "公司编码", required = true)
    private String companyCode;


}
