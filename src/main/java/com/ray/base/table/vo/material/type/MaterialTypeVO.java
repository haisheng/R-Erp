package com.ray.base.table.vo.material.type;

import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author bo shen
 * @Description: 基础属性
 * @Class: MaterialTypeBase
 * @Package com.ray.base.table.params.customer
 * @date 2020/6/1 13:35
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class MaterialTypeVO extends BaseVO {


    @ApiModelProperty(value = "类型编码", required = true)
    private String typeCode;

    /**
     * 类型名称
     */
    @ApiModelProperty(value = "类型名称名称", required = true)
    private String typeName;


}
