package com.ray.base.table.vo.customer.price;

import com.ray.base.table.vo.customer.StepPriceVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author bo shen
 * @Description: 客户新增
 * @Class: CustomerCreateParams
 * @Package com.ray.system.table.params.position
 * @date 2020/5/27 11:09
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("客户加工单价")
public class CustomerPriceVO {

    @ApiModelProperty("客户编号")
    private String customerCode;
    @ApiModelProperty("步骤单价")
    private List<StepPriceVO> props;
}
