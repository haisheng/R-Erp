package com.ray.base.table.vo.technology;

import com.ray.base.table.vo.material.PropVO;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author bo shen
 * @Description: 技术工艺对象
 * @Class: TechnologyVO
 * @Package com.ray.base.table.vo.technology
 * @date 2020/6/27 12:57
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class TechnologyVO extends MaterialModelVO{
    /**
     * 工艺编号
     */
    @ApiModelProperty("工艺编号")
    private String technologyCode;
    /**
     * 型号编码
     */
    @ApiModelProperty("型号编码")
    private String modelCode;

    /**
     * 工艺方式
     */
    @ApiModelProperty("工艺方式")
    private String stepMothed;

    /**
     * 工艺说明
     */
    @ApiModelProperty("工艺说明")
    private String remark;

    /**
     * 原料
     */
    @ApiModelProperty("原料")
    private List<TechnologyGoodsVO> goods;

    @ApiModelProperty(value = "扩展属性", required = true)
    private List<PropVO> props;

}
