package com.ray.base.table.vo.customer;

import com.ray.base.table.vo.material.PropVO;
import com.ray.common.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author bo shen
 * @Description: 客户对象
 * @Class: CustomerVO
 * @Package com.ray.base.table.vo
 * @date 2020/6/1 13:45
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("客户信息")
public class CustomerVO extends BaseVO {

    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号", required = true)
    private String customerCode;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称", required = true)
    private String customerName;

    /**
     * 客户类型  客户，供应商，加工户
     */
    @ApiModelProperty(value = "客户类型", required = true)
    private String customerType;

    /**
     * 联系人
     */
    @ApiModelProperty(value = "联系人", required = false)
    private String linkName;

    /**
     * 联系电话
     */
    @ApiModelProperty(value = "联系电话", required = false)
    private String linkMoblie;

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址", required = false)
    private String address;


    @ApiModelProperty(value = "扩展属性", required = true)
    private List<PropVO> props;
}
