package com.ray.base.table.vo.technology;

import com.ray.base.table.vo.material.model.MaterialModelVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 技术工艺商品对象
 * @Class: TechnologyVO
 * @Package com.ray.base.table.vo.technology
 * @date 2020/6/27 12:57
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
public class TechnologyGoodsVO extends MaterialModelVO{
    /**
     * 工艺  原料类型
     */
    @ApiModelProperty("工艺原料类型")
    private String modelType;

    /**
     * 型号编码
     */
    @ApiModelProperty("型号编码")
    private String modelCode;

    /**
     * 单位
     */
    @ApiModelProperty("单位")
    private String unit;

    /**
     * 用量
     */
    @ApiModelProperty("用量")
    private BigDecimal useQuantity;
}
