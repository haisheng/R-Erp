package com.ray.base.table.vo.customer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author bo shen
 * @Description: 属性
 * @Class: PropParams
 * @Package com.ray.base.table.params
 * @date 2020/6/1 16:36
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Data
@ApiModel("工序价格")
public class StepPriceVO {



    /**
     * 工序
     */
    @ApiModelProperty(value = "工序", required = true)
    private String stepCode;

    /**
     * 工序名称
     */
    @ApiModelProperty(value = "工序名称", required = true)
    private String stepName;


    /**
     * 加工单价
     */
    @ApiModelProperty(value = "加工单价", required = false)
    private BigDecimal price;
}
