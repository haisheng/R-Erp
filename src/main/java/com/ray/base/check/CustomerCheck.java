package com.ray.base.check;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: BaseCustomer校验
 * @Class: CustomerCheck
 * @Package com.ray.base.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class CustomerCheck extends AbstractCheck<BaseCustomer> {


    public CustomerCheck(BaseCustomer entity) {
        super(entity);
    }

    @Override
    public CustomerCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }

    /**
     * 校验
     *
     * @param customerCode
     * @param messageCode
     */
    public CustomerCheck checkSam(String customerCode, String messageCode) {
        //存在 并且appCode相等
        if (ObjectUtil.isNotNull(entity) && !StrUtil.equals(customerCode, entity.getCustomerCode())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

}
