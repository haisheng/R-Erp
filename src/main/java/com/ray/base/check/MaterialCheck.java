package com.ray.base.check;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.base.table.entity.BaseMaterial;
import com.ray.common.check.AbstractCheck;
import com.ray.system.table.entity.SysCompany;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: BaseMaterial校验
 * @Class: MaterialCheck
 * @Package com.ray.base.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class MaterialCheck extends AbstractCheck<BaseMaterial> {


    public MaterialCheck(BaseMaterial entity) {
        super(entity);
    }

    @Override
    public MaterialCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }

    /**
     * 校验
     *
     * @param materialcode
     * @param messageCode
     */
    public MaterialCheck checkMaterialName(String materialcode, String messageCode) {
        //存在 并且appCode相等
        if (ObjectUtil.isNotNull(entity) && !StrUtil.equals(materialcode, entity.getMaterialCode())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }

}
