package com.ray.base.check;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.base.table.entity.BaseMaterialTemplate;
import com.ray.common.check.AbstractCheck;
import com.ray.woodencreate.exception.BusinessExceptionFactory;

/**
 * @author bo shen
 * @Description: BaseMaterialTemplate校验
 * @Class: MaterialTemplateCheck
 * @Package com.ray.base.check
 * @date 2020/5/27 10:16
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class MaterialTemplateCheck extends AbstractCheck<BaseMaterialTemplate> {


    public MaterialTemplateCheck(BaseMaterialTemplate entity) {
        super(entity);
    }

    @Override
    public MaterialTemplateCheck checkNull(String message) {
        super.checkNull(message);
        return this;
    }

    /**
     * 校验
     *
     * @param templateCode
     * @param messageCode
     */
    public MaterialTemplateCheck checkTemplateProp(String templateCode, String messageCode) {
        //存在 并且appCode相等
        if (ObjectUtil.isNotNull(entity) && !StrUtil.equals(templateCode, entity.getTemplateCode())) {
            throw BusinessExceptionFactory.newException(messageCode);
        }
        return this;
    }


}
