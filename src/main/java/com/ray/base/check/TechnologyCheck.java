package com.ray.base.check;

import com.ray.base.table.entity.BaseMaterial;
import com.ray.base.table.entity.BaseTechnology;
import com.ray.common.check.AbstractCheck;

/**
 * @author bo shen
 * @Description:
 * @Class: TechnologyCheck
 * @Package com.ray.base.check
 * @date 2020/6/27 16:07
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class TechnologyCheck extends AbstractCheck<BaseTechnology> {

    public TechnologyCheck(BaseTechnology entity) {
        super(entity);
    }

    @Override
    public TechnologyCheck checkNull(String messageCode) {
        super.checkNull(messageCode);
        return this;
    }
}
