package com.ray.base.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.base.table.dto.CustomerQueryDTO;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.base.table.mapper.BaseCustomerMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 系统客户表 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-01
 */
@Service
public class BaseCustomerService extends ServiceImpl<BaseCustomerMapper, BaseCustomer>{

    /**
     * 编辑客户
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(BaseCustomer entity, LoginUser loginUser) {
        BaseCustomer query = new BaseCustomer();
        query.setCustomerCode(entity.getCustomerCode());
        UpdateWrapper<BaseCustomer> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询客户
     *
     * @param customerCode 客户编码
     * @param loginUser    当前操作员
     * @return
     */
    public BaseCustomer queryCustomerByCustomerCode(String customerCode, LoginUser loginUser) {
        BaseCustomer query = new BaseCustomer();
        query.setCustomerCode(customerCode);
        QueryWrapper<BaseCustomer> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<BaseCustomer> page(CommonPage<CustomerQueryDTO, Page<BaseCustomer>> queryParams, LoginUser loginUser) {
        CustomerQueryDTO params = queryParams.getEntity();
        BaseCustomer entity = BeanCreate.newBean(BaseCustomer.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<BaseCustomer> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getCustomerNameLike()), "customer_name", params.getCustomerNameLike());
            queryWrapper.in(ObjectUtil.isNotEmpty(params.getCustomerCodes()), "customer_code", params.getCustomerCodes());
        }
        queryWrapper.in(ObjectUtil.isNotEmpty(params.getCustomerCodes()), "customer_code", params.getCustomerCodes());
        queryWrapper.in(ObjectUtil.isNotEmpty(params.getCustomerTypes()), "customer_type", params.getCustomerTypes());
        queryWrapper.orderByDesc("create_time" );
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<BaseCustomer> list(CustomerQueryDTO queryParams, LoginUser loginUser) {
        BaseCustomer entity = BeanCreate.newBean(BaseCustomer.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<BaseCustomer> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getCustomerNameLike()), "customer_name", queryParams.getCustomerNameLike());
        queryWrapper.in(ObjectUtil.isNotEmpty(queryParams.getCustomerCodes()), "customer_code", queryParams.getCustomerCodes());
        return list(queryWrapper);
    }

    /**
     * @param customerName
     * @return
     */
    public BaseCustomer queryCustomerByCustomerName(String customerName,String customerType,LoginUser loginUser) {
        BaseCustomer entity = BeanCreate.newBean(BaseCustomer.class);
        entity.setCustomerName(customerName);
        entity.setCustomerType(customerType);
        QueryWrapper<BaseCustomer> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper,loginUser);
        return getOne(queryWrapper);
    }
    

}
