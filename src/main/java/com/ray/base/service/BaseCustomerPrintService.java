package com.ray.base.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.base.table.entity.BaseCustomerPrint;
import com.ray.base.table.mapper.BaseCustomerPrintMapper;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户-唛头 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-07-04
 */
@Service
public class BaseCustomerPrintService extends ServiceImpl<BaseCustomerPrintMapper, BaseCustomerPrint>  {
    /**
     * 编辑客户
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(BaseCustomerPrint entity, LoginUser loginUser) {
        BaseCustomerPrint query = new BaseCustomerPrint();
        query.setCustomerCode(entity.getCustomerCode());
        UpdateWrapper<BaseCustomerPrint> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询客户
     *
     * @param customerCode 客户编码
     * @param loginUser    当前操作员
     * @return
     */
    public BaseCustomerPrint queryCustomerByCustomerCode(String customerCode, LoginUser loginUser) {
        BaseCustomerPrint query = new BaseCustomerPrint();
        query.setCustomerCode(customerCode);
        QueryWrapper<BaseCustomerPrint> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }
}
