package com.ray.base.service.compose;

import com.ray.base.check.TechnologyCheck;
import com.ray.base.service.BaseTechnologyRecordService;
import com.ray.base.service.BaseTechnologyService;
import com.ray.base.table.entity.BaseTechnology;
import com.ray.base.table.entity.BaseTechnologyRecord;
import com.ray.woodencreate.beans.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author bo shen
 * @Description:
 * @Class: TechnologyService
 * @Package com.ray.base.service.compose
 * @date 2020/6/27 20:45
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class TechnologyService {

    @Autowired
    private BaseTechnologyService baseTechnologyService;
    @Autowired
    private BaseTechnologyRecordService baseTechnologyRecordService;

    public List<BaseTechnologyRecord> queryTechnologyRecordByModelCode(String goodsCode, LoginUser loginUser) {
        //获取权限信息
        BaseTechnology baseTechnology = baseTechnologyService.queryTechnologyByModelCode(goodsCode, loginUser);
        new TechnologyCheck(baseTechnology).checkNull("技术工艺未配置");
        return baseTechnologyRecordService.listByTechnologyCode(baseTechnology.getTechnologyCode(), loginUser);
    }

    public BaseTechnologyRecord queryRecord(String goodsCode, String modelType, LoginUser loginUser) {
        BaseTechnology baseTechnology = baseTechnologyService.queryTechnologyByModelCode(goodsCode, loginUser);
        new TechnologyCheck(baseTechnology).checkNull("技术工艺未配置");
        return baseTechnologyRecordService.queryRecord(baseTechnology.getTechnologyCode(),modelType, loginUser);
    }
}
