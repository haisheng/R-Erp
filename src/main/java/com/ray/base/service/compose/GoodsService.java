package com.ray.base.service.compose;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.service.BaseMaterialModelService;
import com.ray.base.service.BaseMaterialService;
import com.ray.base.service.BaseMaterialTypeService;
import com.ray.base.table.entity.BaseMaterial;
import com.ray.base.table.entity.BaseMaterialModel;
import com.ray.base.table.entity.BaseMaterialType;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.wms.table.vo.order.GoodsVO;
import com.ray.woodencreate.beans.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author bo shen
 * @Description:
 * @Class: OrderProdGoodsService
 * @Package com.ray.base.service.compose
 * @date 2020/6/5 16:54
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
public class GoodsService {

    @Autowired
    private BaseMaterialModelService baseMaterialModelService;
    @Autowired
    private BaseMaterialService baseMaterialService;
    @Autowired
    private BaseMaterialTypeService baseMaterialTypeService;

    /**
     * 查询商品信息
     *
     * @param goodsCode
     * @param loginUser
     * @return
     */
    public MaterialModelVO queryGoodsByCode(String goodsCode, LoginUser loginUser) {
        MaterialModelVO materialModelVO = new MaterialModelVO();
        //查询规格是否存在
        BaseMaterialModel baseMaterialModel = baseMaterialModelService.queryMaterialModelByMaterialModelCode(goodsCode, loginUser);
        if (ObjectUtil.isNull(baseMaterialModel)) {
            return null;
        }
        BeanUtil.copyProperties(baseMaterialModel, materialModelVO);
        BaseMaterial baseMaterial = baseMaterialService.queryMaterialByMaterialCode(baseMaterialModel.getMaterialCode(), loginUser);
        if (ObjectUtil.isNotNull(baseMaterial)) {
            materialModelVO.setUnit(baseMaterial.getUnit());
            materialModelVO.setMaterialName(baseMaterial.getMaterialName());
            materialModelVO.setMaterialType(baseMaterial.getMaterialType());
            materialModelVO.setVolume(baseMaterial.getVolume());
            materialModelVO.setWeight(baseMaterial.getWeight());
            materialModelVO.setWeightDiff(baseMaterial.getWeightDiff());
            BaseMaterialType materialType = baseMaterialTypeService.queryMaterialTypeByMaterialTypeCode(baseMaterial.getMaterialType(), loginUser);
            if (ObjectUtil.isNotNull(materialType)) {
                materialModelVO.setMaterialTypeName(materialType.getTypeName());
            }
        }

        return materialModelVO;
    }

    public BaseMaterialModel querySimplGoodsByGoodsCode(String goodsCode, LoginUser loginUser) {
        //查询规格是否存在
        BaseMaterialModel baseMaterialModel = baseMaterialModelService.queryMaterialModelByMaterialModelCode(goodsCode, loginUser);
        if (ObjectUtil.isNull(baseMaterialModel)) {
            return null;
        }
        return baseMaterialModel;
    }
}
