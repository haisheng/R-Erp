package com.ray.base.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.base.table.entity.BaseTechnology;
import com.ray.base.table.entity.BaseTechnologyRecord;
import com.ray.base.table.mapper.BaseTechnologyRecordMapper;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 技术工艺 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-27
 */
@Service
public class BaseTechnologyRecordService extends ServiceImpl<BaseTechnologyRecordMapper, BaseTechnologyRecord> {

    /**
     * 根据技术工艺编码删除
     *
     * @param technologyCode
     * @param loginUser
     * @return
     */
    public Boolean deleteByTechCode(String technologyCode, LoginUser loginUser) {
        BaseTechnologyRecord entity = new BaseTechnologyRecord();
        entity.setIsDeleted(YesOrNoEnum.YES.getValue());
        DataOPUtil.editUser(entity, loginUser);
        BaseTechnologyRecord query = new BaseTechnologyRecord();
        query.setTechnologyCode(technologyCode);
        UpdateWrapper<BaseTechnologyRecord> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询工艺对应原料
     *
     * @param technologyCode
     * @param loginUser
     * @return
     */
    public List<BaseTechnologyRecord> listByTechnologyCode(String technologyCode, LoginUser loginUser) {
        BaseTechnologyRecord query = new BaseTechnologyRecord();
        query.setTechnologyCode(technologyCode);
        QueryWrapper<BaseTechnologyRecord> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return list(queryWrapper);
    }

    public BaseTechnologyRecord queryRecord(String technologyCode, String modelType, LoginUser loginUser) {
        BaseTechnologyRecord query = new BaseTechnologyRecord();
        query.setTechnologyCode(technologyCode);
        query.setModelType(modelType);
        QueryWrapper<BaseTechnologyRecord> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }
}
