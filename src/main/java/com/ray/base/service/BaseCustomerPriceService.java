package com.ray.base.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.base.table.entity.BaseCustomerPrice;
import com.ray.base.table.entity.BaseMaterialExtend;
import com.ray.base.table.mapper.BaseCustomerPriceMapper;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 客户表加工费用配置 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-07-11
 */
@Service
public class BaseCustomerPriceService extends ServiceImpl<BaseCustomerPriceMapper, BaseCustomerPrice> {

    /**
     * 删除扩展信息
     *
     * @param customerCode
     * @param loginUser
     */
    public boolean deleteCustomerPrice(String customerCode, LoginUser loginUser) {
        BaseCustomerPrice entity = new BaseCustomerPrice();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        BaseCustomerPrice query = new BaseCustomerPrice();
        query.setCustomerCode(customerCode);
        UpdateWrapper<BaseCustomerPrice> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    public List<BaseCustomerPrice> listByCustomerPrice(String customerCode,LoginUser loginUser) {
        BaseCustomerPrice query = new BaseCustomerPrice();
        query.setCustomerCode(customerCode);
        QueryWrapper<BaseCustomerPrice> queryWrapper = new QueryWrapper<>(query);
        queryWrapper.orderByDesc("create_time");
        DataAuthUtil.addComapnyDataAuth(queryWrapper,loginUser);
        return list(queryWrapper);
    }

    public BaseCustomerPrice queryCustomerPrice(String customerCode, String stepCode,LoginUser loginUser) {
        BaseCustomerPrice query = new BaseCustomerPrice();
        query.setCustomerCode(customerCode);
        query.setStepCode(stepCode);
        QueryWrapper<BaseCustomerPrice> queryWrapper = new QueryWrapper<>(query);
        queryWrapper.orderByDesc("create_time");
        DataAuthUtil.addComapnyDataAuth(queryWrapper,loginUser);
        return getOne(queryWrapper);
    }

}
