package com.ray.base.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.base.table.dto.MaterialModelQueryDTO;
import com.ray.base.table.entity.BaseMaterialModel;
import com.ray.base.table.mapper.BaseMaterialModelMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import com.ray.woodencreate.page.CommonPage;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 规格 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-01
 */
@Service
public class BaseMaterialModelService extends ServiceImpl<BaseMaterialModelMapper, BaseMaterialModel> {

    /**
     * 编辑规格
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(BaseMaterialModel entity, LoginUser loginUser) {
        BaseMaterialModel query = new BaseMaterialModel();
        query.setModelCode(entity.getModelCode());
        UpdateWrapper<BaseMaterialModel> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询规格
     *
     * @param modelCode 规格编码
     * @param loginUser 当前操作员
     * @return
     */
    public BaseMaterialModel queryMaterialModelByMaterialModelCode(String modelCode, LoginUser loginUser) {
        if (StrUtil.isBlank(modelCode)) {
            return null;
        }
        BaseMaterialModel query = new BaseMaterialModel();
        query.setModelCode(modelCode);
        QueryWrapper<BaseMaterialModel> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<BaseMaterialModel> page(CommonPage<MaterialModelQueryDTO, Page<BaseMaterialModel>> queryParams, LoginUser loginUser) {
        MaterialModelQueryDTO params = queryParams.getEntity();
        BaseMaterialModel entity = BeanCreate.newBean(BaseMaterialModel.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<BaseMaterialModel> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.like(StrUtil.isNotBlank(params.getModelNameLike()), "model_name", params.getModelNameLike());
            queryWrapper.like(StrUtil.isNotBlank(params.getNameLike()), "name", params.getNameLike());
            queryWrapper.in(ObjectUtil.isNotEmpty(params.getModelCodes()), "model_code", params.getModelCodes());
        }
        queryWrapper.orderByDesc("create_time");
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<BaseMaterialModel> list(MaterialModelQueryDTO queryParams, LoginUser loginUser) {
        BaseMaterialModel entity = BeanCreate.newBean(BaseMaterialModel.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<BaseMaterialModel> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getModelNameLike()), "model_name", queryParams.getModelNameLike());
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getNameLike()), "name", queryParams.getNameLike());
        queryWrapper.in(ObjectUtil.isNotEmpty(queryParams.getModelCodes()), "model_code", queryParams.getModelCodes());
        queryWrapper.orderByDesc("create_time");
        return list(queryWrapper);
    }

    /**
     * 更新数据
     *
     * @param materialType
     * @param materialCode
     * @param loginUser
     * @return
     */
    public Boolean updateMaterialType(String materialType, String materialCode, LoginUser loginUser) {
        BaseMaterialModel entity = new BaseMaterialModel();
        entity.setMaterialType(materialType);
        DataOPUtil.editUser(entity, loginUser);
        BaseMaterialModel query = new BaseMaterialModel();
        query.setMaterialCode(materialCode);
        UpdateWrapper<BaseMaterialModel> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    public BaseMaterialModel queryMaterialModelByModelName(String materialCode, String modelName, LoginUser loginUser) {
        BaseMaterialModel query = new BaseMaterialModel();
        query.setMaterialCode(materialCode);
        query.setModelName(modelName);
        QueryWrapper<BaseMaterialModel> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }
}
