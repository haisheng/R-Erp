package com.ray.base.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.base.table.entity.BaseTemplateExtend;
import com.ray.base.table.mapper.BaseTemplateExtendMapper;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 物料扩展信息 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-01
 */
@Service
public class BaseTemplateExtendService extends ServiceImpl<BaseTemplateExtendMapper, BaseTemplateExtend> {

    /**
     * 删除扩展信息
     *
     * @param objectCode
     * @param loginUser
     */
    public boolean deleteTemplateExtend(String objectCode, LoginUser loginUser) {
        BaseTemplateExtend entity = new BaseTemplateExtend();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        BaseTemplateExtend query = new BaseTemplateExtend();
        query.setObjectCode(objectCode);
        UpdateWrapper<BaseTemplateExtend> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    public List<BaseTemplateExtend> listByObjectCode(String objectCode) {
        BaseTemplateExtend query = new BaseTemplateExtend();
        query.setObjectCode(objectCode);
        QueryWrapper<BaseTemplateExtend> queryWrapper = new QueryWrapper<>(query);
        queryWrapper.orderByDesc("create_time" );
        DataAuthUtil.notDelete(queryWrapper);
        return  list(queryWrapper);
    }
}
