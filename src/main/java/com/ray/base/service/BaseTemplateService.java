package com.ray.base.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.base.table.dto.TemplateQueryDTO;
import com.ray.base.table.entity.BaseTemplate;
import com.ray.base.table.mapper.BaseTemplateMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商品属性模板定义 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-01
 */
@Service
public class BaseTemplateService extends ServiceImpl<BaseTemplateMapper, BaseTemplate> {

    /**
     * 编辑属性
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(BaseTemplate entity, LoginUser loginUser) {
        BaseTemplate query = new BaseTemplate();
        query.setTemplateCode(entity.getTemplateCode());
        UpdateWrapper<BaseTemplate> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询属性
     *
     * @param templateCode 属性编码
     * @param loginUser    当前操作员
     * @return
     */
    public BaseTemplate queryTemplateByTemplateCode(String templateCode, LoginUser loginUser) {
        BaseTemplate query = new BaseTemplate();
        query.setTemplateCode(templateCode);
        QueryWrapper<BaseTemplate> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<BaseTemplate> page(CommonPage<TemplateQueryDTO, Page<BaseTemplate>> queryParams, LoginUser loginUser) {
        TemplateQueryDTO params = queryParams.getEntity();
        BaseTemplate entity = BeanCreate.newBean(BaseTemplate.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<BaseTemplate> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getNameLike()), "name", params.getNameLike());
        }
        queryWrapper.orderByDesc("create_time" );
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<BaseTemplate> list(TemplateQueryDTO queryParams, LoginUser loginUser) {
        BaseTemplate entity = BeanCreate.newBean(BaseTemplate.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<BaseTemplate> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper,loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getNameLike()), "name", queryParams.getNameLike());
        return list(queryWrapper);
    }


    /**
     *
     * @param loginUser
     * @return
     */
    public List<BaseTemplate> listByComapnyCode(String templateType, LoginUser loginUser) {
        BaseTemplate entity = BeanCreate.newBean(BaseTemplate.class);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        entity.setTemplateType(templateType);
        QueryWrapper<BaseTemplate> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper,loginUser);
        return list(queryWrapper);
    }


    /**
     * 查询属性
     *
     * @param prop
     * @param loginUser    当前操作员
     * @return
     */
    public BaseTemplate queryTemplateByProp(String prop, String templateType,LoginUser loginUser) {
        BaseTemplate query = new BaseTemplate();
        query.setProp(prop);
        query.setTemplateType(templateType);
        QueryWrapper<BaseTemplate> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

}
