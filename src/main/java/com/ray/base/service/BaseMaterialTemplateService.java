package com.ray.base.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.base.table.dto.MaterialTemplateQueryDTO;
import com.ray.base.table.entity.BaseMaterialTemplate;
import com.ray.base.table.mapper.BaseMaterialTemplateMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商品属性模板定义 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-01
 */
@Service
public class BaseMaterialTemplateService extends ServiceImpl<BaseMaterialTemplateMapper, BaseMaterialTemplate> {

    /**
     * 编辑产品属性
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(BaseMaterialTemplate entity, LoginUser loginUser) {
        BaseMaterialTemplate query = new BaseMaterialTemplate();
        query.setTemplateCode(entity.getTemplateCode());
        UpdateWrapper<BaseMaterialTemplate> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询产品属性
     *
     * @param templateCode 产品属性编码
     * @param loginUser    当前操作员
     * @return
     */
    public BaseMaterialTemplate queryMaterialTemplateByMaterialTemplateCode(String templateCode, LoginUser loginUser) {
        BaseMaterialTemplate query = new BaseMaterialTemplate();
        query.setTemplateCode(templateCode);
        QueryWrapper<BaseMaterialTemplate> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<BaseMaterialTemplate> page(CommonPage<MaterialTemplateQueryDTO, Page<BaseMaterialTemplate>> queryParams, LoginUser loginUser) {
        MaterialTemplateQueryDTO params = queryParams.getEntity();
        BaseMaterialTemplate entity = BeanCreate.newBean(BaseMaterialTemplate.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<BaseMaterialTemplate> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getNameLike()), "name", params.getNameLike());
        }
        queryWrapper.orderByDesc("create_time" );
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<BaseMaterialTemplate> list(MaterialTemplateQueryDTO queryParams, LoginUser loginUser) {
        BaseMaterialTemplate entity = BeanCreate.newBean(BaseMaterialTemplate.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<BaseMaterialTemplate> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper,loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getNameLike()), "name", queryParams.getNameLike());
        return list(queryWrapper);
    }


    /**
     *
     * @param loginUser
     * @return
     */
    public List<BaseMaterialTemplate> listByComapnyCode(LoginUser loginUser) {
        BaseMaterialTemplate entity = BeanCreate.newBean(BaseMaterialTemplate.class);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<BaseMaterialTemplate> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper,loginUser);
        return list(queryWrapper);
    }


    /**
     * 查询产品属性
     *
     * @param prop
     * @param loginUser    当前操作员
     * @return
     */
    public BaseMaterialTemplate queryMaterialTemplateByProp(String prop, LoginUser loginUser) {
        BaseMaterialTemplate query = new BaseMaterialTemplate();
        query.setProp(prop);
        QueryWrapper<BaseMaterialTemplate> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

}
