package com.ray.base.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.base.table.entity.BaseTechnology;
import com.ray.base.table.entity.BaseTechnology;
import com.ray.base.table.entity.BaseTechnologyRecord;
import com.ray.base.table.mapper.BaseTechnologyMapper;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 技术工艺 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-27
 */
@Service
public class BaseTechnologyService extends ServiceImpl<BaseTechnologyMapper, BaseTechnology> {

    /**
     * 删除技术工艺
     *
     * @param technologyCode
     * @param loginUser
     * @return
     */
    public Boolean deleteByCode(String technologyCode, LoginUser loginUser) {
        BaseTechnology entity = new BaseTechnology();
        entity.setIsDeleted(YesOrNoEnum.YES.getValue());
        DataOPUtil.editUser(entity, loginUser);
        BaseTechnology query = new BaseTechnology();
        query.setTechnologyCode(technologyCode);
        UpdateWrapper<BaseTechnology> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询物料
     *
     * @param technologyCode 工艺编码
     * @param loginUser    当前操作员
     * @return
     */
    public BaseTechnology queryTechnologyByTechnologyCode(String technologyCode, LoginUser loginUser) {
        BaseTechnology query = new BaseTechnology();
        query.setTechnologyCode(technologyCode);
        QueryWrapper<BaseTechnology> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 查询物料
     *
     * @param modelCode
     * @param loginUser    当前操作员
     * @return
     */
    public BaseTechnology queryTechnologyByModelCode(String modelCode, LoginUser loginUser) {
        BaseTechnology query = new BaseTechnology();
        query.setModelCode(modelCode);
        QueryWrapper<BaseTechnology> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

}
