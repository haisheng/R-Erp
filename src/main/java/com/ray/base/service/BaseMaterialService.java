package com.ray.base.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.table.dto.MaterialQueryDTO;
import com.ray.base.table.entity.BaseMaterial;
import com.ray.base.table.mapper.BaseMaterialMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 物料/产品 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-01
 */
@Service
public class BaseMaterialService extends ServiceImpl<BaseMaterialMapper, BaseMaterial> {
    /**
     * 编辑物料
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(BaseMaterial entity, LoginUser loginUser) {
        BaseMaterial query = new BaseMaterial();
        query.setMaterialCode(entity.getMaterialCode());
        UpdateWrapper<BaseMaterial> pdateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(pdateWrapper, loginUser);
        return update(entity, pdateWrapper);
    }

    /**
     * 查询物料
     *
     * @param materialCode 物料编码
     * @param loginUser    当前操作员
     * @return
     */
    public BaseMaterial queryMaterialByMaterialCode(String materialCode, LoginUser loginUser) {
        BaseMaterial query = new BaseMaterial();
        query.setMaterialCode(materialCode);
        QueryWrapper<BaseMaterial> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<BaseMaterial> page(CommonPage<MaterialQueryDTO, Page<BaseMaterial>> queryParams, LoginUser loginUser) {
        MaterialQueryDTO params = queryParams.getEntity();
        BaseMaterial entity = BeanCreate.newBean(BaseMaterial.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<BaseMaterial> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.like(StrUtil.isNotBlank(params.getMaterialNameLike()), "material_name", params.getMaterialNameLike());
        }
        queryWrapper.orderByDesc("create_time" );
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<BaseMaterial> list(MaterialQueryDTO queryParams, LoginUser loginUser) {
        BaseMaterial entity = BeanCreate.newBean(BaseMaterial.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<BaseMaterial> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getMaterialNameLike()), "material_name", queryParams.getMaterialNameLike());
        return list(queryWrapper);
    }

    /**
     * 通过物料名称查询物料
     *
     * @param materialName
     * @return
     */
    public BaseMaterial queryMaterialByMaterialName(String materialName,LoginUser loginUser) {
        BaseMaterial entity = BeanCreate.newBean(BaseMaterial.class);
        entity.setMaterialName(materialName);
        QueryWrapper<BaseMaterial> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper,loginUser);
        return getOne(queryWrapper);
    }


    /**
     * 批量获取物料信息
     *
     * @param materialCodes
     * @return
     */
    public List<BaseMaterial> queryMaterialByMaterialCodesForLogin(List<String> materialCodes) {
        BaseMaterial entity = BeanCreate.newBean(BaseMaterial.class);
        entity.setStatus(YesOrNoEnum.YES.getValue());
        QueryWrapper<BaseMaterial> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.notDelete(queryWrapper);
        queryWrapper.in("material_code", materialCodes);
        return list(queryWrapper);
    }
}
