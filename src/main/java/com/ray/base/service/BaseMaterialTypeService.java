package com.ray.base.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.base.table.dto.MaterialTypeQueryDTO;
import com.ray.base.table.entity.BaseMaterialType;
import com.ray.base.table.mapper.BaseMaterialTypeMapper;
import com.ray.woodencreate.beans.BeanCreate;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.page.CommonPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 物料类型 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-01
 */
@Service
public class BaseMaterialTypeService extends ServiceImpl<BaseMaterialTypeMapper, BaseMaterialType> {

    /**
     * 编辑物料类型类型
     *
     * @param entity    编辑对象
     * @param loginUser 当前操作员
     * @return
     */
    public boolean edit(BaseMaterialType entity, LoginUser loginUser) {
        BaseMaterialType query = new BaseMaterialType();
        query.setTypeCode(entity.getTypeCode());
        UpdateWrapper<BaseMaterialType> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.addDataAuth(updateWrapper, loginUser);
        return update(entity, updateWrapper);
    }

    /**
     * 查询物料类型类型
     *
     * @param typeCode 物料类型类型编码
     * @param loginUser    当前操作员
     * @return
     */
    public BaseMaterialType queryMaterialTypeByMaterialTypeCode(String typeCode, LoginUser loginUser) {
        BaseMaterialType query = new BaseMaterialType();
        query.setTypeCode(typeCode);
        QueryWrapper<BaseMaterialType> queryWrapper = new QueryWrapper<>(query);
        DataAuthUtil.addComapnyDataAuth(queryWrapper, loginUser);
        return getOne(queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public IPage<BaseMaterialType> page(CommonPage<MaterialTypeQueryDTO, Page<BaseMaterialType>> queryParams, LoginUser loginUser) {
        MaterialTypeQueryDTO params = queryParams.getEntity();
        BaseMaterialType entity = BeanCreate.newBean(BaseMaterialType.class);
        //查询对象存在
        if (ObjectUtil.isNotNull(params)) {
            BeanUtil.copyProperties(params, entity);
        }
        QueryWrapper<BaseMaterialType> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        if (ObjectUtil.isNotNull(params)) {
            queryWrapper.likeRight(StrUtil.isNotBlank(params.getTypeNameLike()), "type_name", params.getTypeNameLike());
        }
        queryWrapper.orderByDesc("create_time" );
        return page(queryParams.getPage(), queryWrapper);
    }

    /**
     * 列表查询
     *
     * @param queryParams
     * @param loginUser
     * @return
     */
    public List<BaseMaterialType> list(MaterialTypeQueryDTO queryParams, LoginUser loginUser) {
        BaseMaterialType entity = BeanCreate.newBean(BaseMaterialType.class);
        BeanUtil.copyProperties(queryParams, entity);
        QueryWrapper<BaseMaterialType> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addDataAuth(queryWrapper, loginUser);
        queryWrapper.likeRight(StrUtil.isNotBlank(queryParams.getTypeNameLike()), "type_name", queryParams.getTypeNameLike());
        return list(queryWrapper);
    }

    /**
     * 通过名称查询类型
     *
     * @param materialTypeName
     * @return
     */
    public BaseMaterialType queryMaterialTypeByMaterialTypeName(String materialTypeName,LoginUser user) {
        BaseMaterialType entity = BeanCreate.newBean(BaseMaterialType.class);
        entity.setTypeName(materialTypeName);
        QueryWrapper<BaseMaterialType> queryWrapper = new QueryWrapper<>(entity);
        DataAuthUtil.addComapnyDataAuth(queryWrapper,user);
        return getOne(queryWrapper);
    }

}
