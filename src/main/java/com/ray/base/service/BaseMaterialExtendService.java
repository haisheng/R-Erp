package com.ray.base.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ray.base.table.entity.BaseMaterialExtend;
import com.ray.base.table.mapper.BaseMaterialExtendMapper;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.DeleteEnum;
import com.ray.woodencreate.mybatis.DataAuthUtil;
import com.ray.woodencreate.mybatis.DataOPUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 物料扩展信息 服务实现类
 * </p>
 *
 * @author shenbo
 * @since 2020-06-01
 */
@Service
public class BaseMaterialExtendService extends ServiceImpl<BaseMaterialExtendMapper, BaseMaterialExtend> {

    /**
     * 删除扩展信息
     *
     * @param materialCode
     * @param loginUser
     */
    public boolean deleteMaterialExtend(String materialCode, LoginUser loginUser) {
        BaseMaterialExtend entity = new BaseMaterialExtend();
        entity.setIsDeleted(DeleteEnum.DELETE.getValue());
        DataOPUtil.editUser(entity, loginUser);
        BaseMaterialExtend query = new BaseMaterialExtend();
        query.setMaterialCode(materialCode);
        UpdateWrapper<BaseMaterialExtend> updateWrapper = new UpdateWrapper<>(query);
        DataAuthUtil.notDelete(updateWrapper);
        return update(entity, updateWrapper);
    }

    public List<BaseMaterialExtend> listByMaterialCode(String materialCode) {
        BaseMaterialExtend query = new BaseMaterialExtend();
        query.setMaterialCode(materialCode);
        QueryWrapper<BaseMaterialExtend> queryWrapper = new QueryWrapper<>(query);
        queryWrapper.orderByDesc("create_time" );
        DataAuthUtil.notDelete(queryWrapper);
        return  list(queryWrapper);
    }
}
