package com.ray.base.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseTechnology;
import com.ray.base.table.entity.BaseTechnologyRecord;
import com.ray.base.table.params.technology.TechnologyCreateParams;
import com.ray.base.table.params.technology.TechnologyGoodsParams;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: BaseMaterial 构造器
 * @Class: MaterialBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class TechnologyRecordBuilder extends AbstractBuilder<BaseTechnologyRecord, TechnologyRecordBuilder> {

    public TechnologyRecordBuilder() {
        super(new BaseTechnologyRecord());
        super.setBuilder(this);
    }

    @Override
    public BaseTechnologyRecord bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }



    /**
     * 添加状态
     *
     * @return
     */
    public TechnologyRecordBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }




    @Override
    public String getCode() {
        return entity.getTechnologyCode();
    }

    public TechnologyRecordBuilder appendCode(String code) {
        entity.setTechnologyCode(code);
        return this;
    }

    public TechnologyRecordBuilder append(TechnologyGoodsParams technologyGoodsParams) {
      if(ObjectUtil.isNotNull(technologyGoodsParams)){
          entity.setModelCode(technologyGoodsParams.getModelCode());
          entity.setModelType(technologyGoodsParams.getModelType());
          entity.setUnit(technologyGoodsParams.getUnit());
          entity.setUseQuantity(technologyGoodsParams.getUseQuantity());
      }
        return this;
    }
}
