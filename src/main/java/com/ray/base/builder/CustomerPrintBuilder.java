package com.ray.base.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.base.table.entity.BaseCustomerPrint;
import com.ray.base.table.params.customer.CustomerCreateParams;
import com.ray.base.table.params.customer.CustomerEditParams;
import com.ray.base.table.params.customer.CustomerPrintSaveParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: BaseCustomer 构造器
 * @Class: CustomerBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class CustomerPrintBuilder extends AbstractBuilder<BaseCustomerPrint, CustomerPrintBuilder> {

    public CustomerPrintBuilder() {
        super(new BaseCustomerPrint());
        super.setBuilder(this);
    }

    @Override
    public BaseCustomerPrint bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public CustomerPrintBuilder append(CustomerPrintSaveParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }


}
