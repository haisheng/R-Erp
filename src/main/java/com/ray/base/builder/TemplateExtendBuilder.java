package com.ray.base.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseMaterialExtend;
import com.ray.base.table.entity.BaseTemplateExtend;
import com.ray.base.table.params.material.PropParams;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: BaseMaterialExtend 构造器
 * @Class: MaterialExtendBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class TemplateExtendBuilder extends AbstractBuilder<BaseTemplateExtend, TemplateExtendBuilder> {

    public TemplateExtendBuilder() {
        super(new BaseTemplateExtend());
        super.setBuilder(this);
    }

    @Override
    public BaseTemplateExtend bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public TemplateExtendBuilder append(PropParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }


    /**
     * 添加编码
     *
     * @return
     */
    public TemplateExtendBuilder appendCode(String code) {
        entity.setObjectCode(code);
        return this;
    }

    public TemplateExtendBuilder appendCompanyCode(String companyCode) {
        entity.setCompanyCode(companyCode);
        return this;
    }

    @Override
    public String getCode() {
        return entity.getObjectCode();
    }


    public TemplateExtendBuilder appendName(String name) {
        entity.setName(name);
        return this;
    }
}
