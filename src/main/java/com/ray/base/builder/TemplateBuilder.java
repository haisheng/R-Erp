package com.ray.base.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.enums.TemplateTypeEnum;
import com.ray.base.table.entity.BaseTemplate;
import com.ray.base.table.params.template.TemplateCreateParams;
import com.ray.base.table.params.template.TemplateEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.template.Template;
import com.ray.util.CodeCreateUtil;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: BaseTemplate 构造器
 * @Class: TemplateBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class TemplateBuilder extends AbstractBuilder<BaseTemplate, TemplateBuilder> {

    public TemplateBuilder() {
        super(new BaseTemplate());
        super.setBuilder(this);
        entity.setTemplateCode(CodeCreateUtil.getCode(Perifx.CODE));
    }

    @Override
    public BaseTemplate bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public TemplateBuilder append(TemplateCreateParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            if(!TemplateTypeEnum.valueInEnum(createParams.getTemplateType())){
                throw BusinessExceptionFactory.newException("模板类型不存在");
            }
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    /**
     *  添加状态
     * @return
     */
    public TemplateBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }
    /**
     *  添加编码
     * @return
     */
    public TemplateBuilder appendCode(String code) {
        entity.setTemplateCode(code);
        return this;
    }
    /**
     *  添加编码
     * @return
     */
    public TemplateBuilder appendComapnyCode(String code) {
        entity.setCompanyCode(code);
        return this;
    }
    /**
     * 构建创建信息
     * @param editParams
     * @return
     */
    public TemplateBuilder append(TemplateEditParams editParams) {
        if(ObjectUtil.isNotNull(editParams)){
            if(!TemplateTypeEnum.valueInEnum(editParams.getTemplateType())){
                throw BusinessExceptionFactory.newException("模板类型不存在");
            }
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getTemplateCode();
    }



}
