package com.ray.base.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.base.table.entity.BaseMaterial;
import com.ray.base.table.params.material.MaterialCreateParams;
import com.ray.base.table.params.material.MaterialEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: BaseMaterial 构造器
 * @Class: MaterialBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class MaterialBuilder extends AbstractBuilder<BaseMaterial, MaterialBuilder> {

    public MaterialBuilder() {
        super(new BaseMaterial());
        super.setBuilder(this);
        entity.setMaterialCode(CodeCreateUtil.getCode(Perifx.CODE));
        log.info("获得物料编码:{}", entity.getMaterialCode());
    }

    @Override
    public BaseMaterial bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public MaterialBuilder append(MaterialCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public MaterialBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public MaterialBuilder appendCode(String code) {
        entity.setMaterialCode(code);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public MaterialBuilder append(MaterialEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getMaterialCode();
    }

}
