package com.ray.base.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseCustomerPrice;
import com.ray.base.table.params.customer.StepPriceParams;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: BaseCustomer 构造器
 * @Class: CustomerBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class CustomerPriceBuilder extends AbstractBuilder<BaseCustomerPrice, CustomerPriceBuilder> {

    public CustomerPriceBuilder() {
        super(new BaseCustomerPrice());
        super.setBuilder(this);
    }

    @Override
    public BaseCustomerPrice bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public CustomerPriceBuilder append(StepPriceParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getCustomerCode();
    }


    public CustomerPriceBuilder appendCustomerCode( String customerCode) {
        entity.setCustomerCode(customerCode);
        return this;
    }
}
