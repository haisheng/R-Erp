package com.ray.base.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseMaterialTemplate;
import com.ray.base.table.params.material.template.MaterialTemplateCreateParams;
import com.ray.base.table.params.material.template.MaterialTemplateEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: BaseMaterialTemplate 构造器
 * @Class: MaterialTemplateBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class MaterialTemplateBuilder extends AbstractBuilder<BaseMaterialTemplate, MaterialTemplateBuilder> {

    public MaterialTemplateBuilder() {
        super(new BaseMaterialTemplate());
        super.setBuilder(this);
        entity.setTemplateCode(CodeCreateUtil.getCode(Perifx.CODE));
    }

    @Override
    public BaseMaterialTemplate bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public MaterialTemplateBuilder append(MaterialTemplateCreateParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    /**
     *  添加状态
     * @return
     */
    public MaterialTemplateBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }
    /**
     *  添加编码
     * @return
     */
    public MaterialTemplateBuilder appendCode(String code) {
        entity.setTemplateCode(code);
        return this;
    }
    /**
     *  添加编码
     * @return
     */
    public MaterialTemplateBuilder appendComapnyCode(String code) {
        entity.setCompanyCode(code);
        return this;
    }
    /**
     * 构建创建信息
     * @param editParams
     * @return
     */
    public MaterialTemplateBuilder append(MaterialTemplateEditParams editParams) {
        if(ObjectUtil.isNotNull(editParams)){
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getTemplateCode();
    }



}
