package com.ray.base.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseMaterialExtend;
import com.ray.base.table.entity.BaseMaterialTemplate;
import com.ray.base.table.entity.BaseTemplate;
import com.ray.base.table.entity.BaseTemplateExtend;
import com.ray.base.table.vo.material.PropVO;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: PropVO 构造器
 * @Class: PropBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class PropBuilder extends AbstractBuilder<PropVO, PropBuilder> {

    public PropBuilder() {
        super(new PropVO());
        super.setBuilder(this);

    }

    @Override
    public PropVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param extend
     * @return
     */
    public PropBuilder append(BaseMaterialExtend extend) {
        if (ObjectUtil.isNotNull(extend)) {
            entity.setValue(extend.getValue());
        }
        return this;
    }



    /**
     * 构建创建信息
     *
     * @param template
     * @return
     */
    public PropBuilder append(BaseMaterialTemplate template) {
        if (ObjectUtil.isNotNull(template)) {
            entity.setName(template.getName());
            entity.setProp(template.getProp());
        }
        return this;
    }

    public PropBuilder append(BaseTemplate baseTemplate) {
        if (ObjectUtil.isNotNull(baseTemplate)) {
            entity.setName(baseTemplate.getName());
            entity.setProp(baseTemplate.getProp());
        }
        return this;
    }

    public PropBuilder append(BaseTemplateExtend baseTemplateExtend) {
        if (ObjectUtil.isNotNull(baseTemplateExtend)) {
            entity.setValue(baseTemplateExtend.getValue());
        }
        return this;
    }
}
