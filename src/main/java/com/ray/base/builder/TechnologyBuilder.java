package com.ray.base.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseMaterial;
import com.ray.base.table.entity.BaseTechnology;
import com.ray.base.table.params.material.MaterialCreateParams;
import com.ray.base.table.params.material.MaterialEditParams;
import com.ray.base.table.params.technology.TechnologyCreateParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: BaseMaterial 构造器
 * @Class: MaterialBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class TechnologyBuilder extends AbstractBuilder<BaseTechnology, TechnologyBuilder> {

    public TechnologyBuilder() {
        super(new BaseTechnology());
        super.setBuilder(this);
    }

    @Override
    public BaseTechnology bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public TechnologyBuilder append(TechnologyCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public TechnologyBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }




    @Override
    public String getCode() {
        return entity.getTechnologyCode();
    }

}
