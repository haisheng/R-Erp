package com.ray.base.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseMaterialType;
import com.ray.base.table.params.material.type.MaterialTypeCreateParams;
import com.ray.base.table.params.material.type.MaterialTypeEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: BaseMaterialType 构造器
 * @Class: MaterialTypeBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class MaterialTypeBuilder extends AbstractBuilder<BaseMaterialType, MaterialTypeBuilder> {

    public MaterialTypeBuilder() {
        super(new BaseMaterialType());
        super.setBuilder(this);
        entity.setTypeCode(CodeCreateUtil.getCode(Perifx.CODE));
        log.info("获得类型编码:{}", entity.getTypeCode());
    }

    @Override
    public BaseMaterialType bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public MaterialTypeBuilder append(MaterialTypeCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public MaterialTypeBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public MaterialTypeBuilder appendCode(String code) {
        entity.setTypeCode(code);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public MaterialTypeBuilder append(MaterialTypeEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getTypeCode();
    }


}
