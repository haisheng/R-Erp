package com.ray.base.builder;


import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseCustomerPrice;
import com.ray.base.table.entity.BaseMaterialTemplate;
import com.ray.base.table.entity.BaseTemplate;
import com.ray.base.table.entity.BaseTemplateExtend;
import com.ray.base.table.vo.customer.StepPriceVO;
import com.ray.business.table.entity.ProdStep;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: PropVO 构造器
 * @Class: PropBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class StepPriceBuilder extends AbstractBuilder<StepPriceVO, StepPriceBuilder> {

    public StepPriceBuilder() {
        super(new StepPriceVO());
        super.setBuilder(this);

    }

    @Override
    public StepPriceVO bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param price
     * @return
     */
    public StepPriceBuilder append(BaseCustomerPrice price) {
        if (ObjectUtil.isNotNull(price)) {
            entity.setPrice(price.getPrice());
        }
        return this;
    }



    /**
     * 构建创建信息
     *
     * @param step
     * @return
     */
    public StepPriceBuilder append(ProdStep step) {
        if (ObjectUtil.isNotNull(step)) {
            entity.setStepCode(step.getStepCode());
            entity.setStepName(step.getStepName());
        }
        return this;
    }


}
