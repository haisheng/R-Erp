package com.ray.base.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.base.table.params.customer.CustomerCreateParams;
import com.ray.base.table.params.customer.CustomerEditParams;
import com.ray.common.Perifx;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: BaseCustomer 构造器
 * @Class: CustomerBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class CustomerBuilder extends AbstractBuilder<BaseCustomer, CustomerBuilder> {

    public CustomerBuilder() {
        super(new BaseCustomer());
        super.setBuilder(this);
        entity.setCustomerCode(CodeCreateUtil.getCode(Perifx.CUSTOMER_CODE));
        log.info("获得客户编码:{}", entity.getCustomerCode());
    }

    @Override
    public BaseCustomer bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     * @param createParams
     * @return
     */
    public CustomerBuilder append(CustomerCreateParams createParams) {
        if(ObjectUtil.isNotNull(createParams)){
            BeanUtil.copyProperties(createParams,entity);
        }
        return this;
    }

    /**
     *  添加状态
     * @return
     */
    public CustomerBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }
    /**
     *  添加编码
     * @return
     */
    public CustomerBuilder appendCode(String code) {
        entity.setCustomerCode(code);
        return this;
    }
    /**
     * 构建创建信息
     * @param editParams
     * @return
     */
    public CustomerBuilder append(CustomerEditParams editParams) {
        if(ObjectUtil.isNotNull(editParams)){
            BeanUtil.copyProperties(editParams,entity);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getCustomerCode();
    }



}
