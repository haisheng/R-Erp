package com.ray.base.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ray.base.table.entity.BaseMaterial;
import com.ray.base.table.entity.BaseMaterialModel;
import com.ray.base.table.params.material.model.MaterialModelCreateParams;
import com.ray.base.table.params.material.model.MaterialModelEditParams;
import com.ray.common.builder.AbstractBuilder;
import com.ray.util.CodeCreateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: BaseMaterialModel 构造器
 * @Class: MaterialModelBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class MaterialModelBuilder extends AbstractBuilder<BaseMaterialModel, MaterialModelBuilder> {

    public MaterialModelBuilder() {
        super(new BaseMaterialModel());
        super.setBuilder(this);
    }

    @Override
    public BaseMaterialModel bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public MaterialModelBuilder append(MaterialModelCreateParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
            //设置
            entity.setModelCode(CodeCreateUtil.nextCode(createParams.getMaterialCode()));
        }
        return this;
    }

    /**
     * 添加状态
     *
     * @return
     */
    public MaterialModelBuilder appendStatus(Integer status) {
        entity.setStatus(status);
        return this;
    }

    /**
     * 添加编码
     *
     * @return
     */
    public MaterialModelBuilder appendCode(String code) {
        entity.setModelCode(code);
        return this;
    }

    /**
     * 构建创建信息
     *
     * @param editParams
     * @return
     */
    public MaterialModelBuilder append(MaterialModelEditParams editParams) {
        if (ObjectUtil.isNotNull(editParams)) {
            BeanUtil.copyProperties(editParams, entity);
        }
        return this;
    }

    public MaterialModelBuilder appendName(String materialName) {
        if (StrUtil.isBlank(entity.getName())) {
            entity.setName(materialName);
        }
        return this;
    }

    @Override
    public String getCode() {
        return entity.getModelCode();
    }


    public MaterialModelBuilder appendMaterialType(String materialType) {
       entity.setMaterialType(materialType);
        return this;
    }

    public MaterialModelBuilder append(BaseMaterial material) {
        if (ObjectUtil.isNotNull(material)) {
            entity.setMaterialCode(material.getMaterialCode());
            entity.setMaterialType(material.getMaterialType());
            entity.setName(material.getMaterialName());
            entity.setModelName("默认");
            //设置
            entity.setModelCode(CodeCreateUtil.nextCode(material.getMaterialCode()));
        }
        return this;
    }
}
