package com.ray.base.builder;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ray.base.table.entity.BaseMaterialExtend;
import com.ray.base.table.params.material.PropParams;
import com.ray.common.builder.AbstractBuilder;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bo shen
 * @Description: BaseMaterialExtend 构造器
 * @Class: MaterialExtendBuilder
 * @Package com.ray.base.builder
 * @date 2020/5/27 12:17
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
public class MaterialExtendBuilder extends AbstractBuilder<BaseMaterialExtend, MaterialExtendBuilder> {

    public MaterialExtendBuilder() {
        super(new BaseMaterialExtend());
        super.setBuilder(this);
    }

    @Override
    public BaseMaterialExtend bulid() {
        return super.bulid();
    }

    @Override
    public String bulidString() {
        return super.bulidString();
    }

    /**
     * 构建创建信息
     *
     * @param createParams
     * @return
     */
    public MaterialExtendBuilder append(PropParams createParams) {
        if (ObjectUtil.isNotNull(createParams)) {
            BeanUtil.copyProperties(createParams, entity);
        }
        return this;
    }


    /**
     * 添加编码
     *
     * @return
     */
    public MaterialExtendBuilder appendCode(String code) {
        entity.setMaterialCode(code);
        return this;
    }

    public MaterialExtendBuilder appendCompanyCode(String companyCode) {
        entity.setCompanyCode(companyCode);
        return this;
    }

    @Override
    public String getCode() {
        return entity.getMaterialCode();
    }


    public MaterialExtendBuilder appendName(String name) {
        entity.setName(name);
        return this;
    }
}
