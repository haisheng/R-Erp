package com.ray.base.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.builder.MaterialTypeBuilder;
import com.ray.base.check.MaterialTypeCheck;
import com.ray.base.service.BaseMaterialTypeService;
import com.ray.base.table.dto.MaterialTypeQueryDTO;
import com.ray.base.table.entity.BaseMaterialType;
import com.ray.base.table.params.material.type.MaterialTypeCreateParams;
import com.ray.base.table.params.material.type.MaterialTypeEditParams;
import com.ray.base.table.params.material.type.MaterialTypeQueryParams;
import com.ray.base.table.vo.material.type.MaterialTypeVO;
import com.ray.common.SysMsgCodeConstant;
import com.ray.system.builder.CommonPageBuilder;
import com.ray.validate.support.utils.ValidateUtil;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.Assert;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 产品类型相关服务
 * @Class: MaterialTypeApi
 * @Package com.ray.base.api
 * @date 2020/5/27 11:15
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class MaterialTypeApi {

    @Autowired
    private BaseMaterialTypeService baseMaterialTypeService;


    /**
     * 查询产品类型列表信息 分页  非删除的
     *
     * @param queryParams
     * @return
     */
    public Result<IPage<MaterialTypeVO>> pageMaterialTypes(CommonPage<MaterialTypeQueryParams, Page<MaterialTypeVO>> queryParams) {
        Assert.notNull(queryParams, "参数[queryParams]不存在");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        CommonPageBuilder<MaterialTypeQueryDTO, BaseMaterialType> commonPageBuilder = new CommonPageBuilder<>(MaterialTypeQueryDTO.class);
        commonPageBuilder.appendEntity(queryParams.getEntity()).appendQuery(queryParams.getQuery()).appendPage(queryParams.getPage());
        IPage<BaseMaterialType> page = baseMaterialTypeService.page(commonPageBuilder.bulid(), loginUser);
        List<BaseMaterialType> customers = page.getRecords();
        //结果对象
        IPage<MaterialTypeVO> pageList = new Page<>();
        pageList.setTotal(page.getTotal());
        pageList.setCurrent(page.getCurrent());
        pageList.setSize(page.getSize());
        //查询到结果 数据转换
        if (ObjectUtil.isNotNull(customers)) {
            pageList.setRecords(customers.stream().map(sysMaterialType -> {
                MaterialTypeVO customerVO = new MaterialTypeVO();
                BeanUtil.copyProperties(sysMaterialType, customerVO);
                return customerVO;
            }).collect(Collectors.toList()));
        } else {
            pageList.setRecords(new ArrayList<>());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, pageList);
    }

    /**
     * 查询产品类型列表信息--启用的产品类型
     *
     * @param queryParams
     * @return
     */
    public Result<List<MaterialTypeVO>> queryMaterialTypes(MaterialTypeQueryParams queryParams) {
        Assert.notNull(queryParams, "参数[queryParams]不存在");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        MaterialTypeQueryDTO queryDTO = new MaterialTypeQueryDTO();
        BeanUtil.copyProperties(queryParams, queryDTO);
        queryDTO.setStatus(YesOrNoEnum.YES.getValue());
        List<BaseMaterialType> customers = baseMaterialTypeService.list(queryDTO, loginUser);
        //查询对象
        List<MaterialTypeVO> list = new ArrayList<>();
        if (ObjectUtil.isNotNull(customers)) {
            list = customers.stream().map(sysMaterialType -> {
                MaterialTypeVO customerVO = new MaterialTypeVO();
                BeanUtil.copyProperties(sysMaterialType, customerVO);
                return customerVO;
            }).collect(Collectors.toList());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, list);
    }


    /**
     * 创建产品类型
     *
     * @param createParams 创建对象
     * @return Result
     */
    @Transactional
    public Result<String> createMaterialType(MaterialTypeCreateParams createParams) {
        ValidateUtil.validate(createParams);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        BaseMaterialType materialType = baseMaterialTypeService.queryMaterialTypeByMaterialTypeName(createParams.getTypeName(),loginUser);
        new MaterialTypeCheck(materialType).checkTypeName(null, "类型名称已经存在");
        MaterialTypeBuilder customerBuilder = new MaterialTypeBuilder();
        customerBuilder.append(createParams).appendStatus(YesOrNoEnum.YES.getValue()).appendCreate(loginUser);
        //保存产品类型信息
        if (!baseMaterialTypeService.save(customerBuilder.bulid())) {
            log.info("保存产品类型接口异常,参数:{}", JSON.toJSONString(customerBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, customerBuilder.getCode());
    }

    /**
     * 编辑产品类型
     *
     * @param editParams 编辑对象
     * @return Result
     */
    @Transactional
    public Result<String> editMaterialType(MaterialTypeEditParams editParams) {
        ValidateUtil.validate(editParams);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        BaseMaterialType materialType = baseMaterialTypeService.queryMaterialTypeByMaterialTypeName(editParams.getTypeName(),loginUser);
        new MaterialTypeCheck(materialType).checkTypeName(editParams.getTypeCode(), "类型名称已经存在");
        MaterialTypeBuilder customerBuilder = new MaterialTypeBuilder();
        customerBuilder.append(editParams).appendEdit(loginUser);
        //编辑产品类型信息
        if (!baseMaterialTypeService.edit(customerBuilder.bulid(), loginUser)) {
            log.info("编辑产品类型接口异常,参数:{}", JSON.toJSONString(customerBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, customerBuilder.getCode());
    }


    /**
     * 删除产品类型
     *
     * @param customerCode 产品类型编码
     * @return Result
     */
    @Transactional
    public Result<String> deleteMaterialType(String customerCode) {
        ValidateUtil.hasLength(customerCode, "参数[customerCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        BaseMaterialType sysMaterialType = baseMaterialTypeService.queryMaterialTypeByMaterialTypeCode(customerCode, loginUser);
        new MaterialTypeCheck(sysMaterialType).checkNull("类型不存在");
        MaterialTypeBuilder customerBuilder = new MaterialTypeBuilder();
        customerBuilder.appendCode(customerCode).appendEdit(loginUser).delete();
        //删除产品类型信息
        if (!baseMaterialTypeService.edit(customerBuilder.bulid(), loginUser)) {
            log.info("删除产品类型接口异常,参数:{}", JSON.toJSONString(customerBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, customerBuilder.getCode());
    }

    /**
     * 开启产品类型
     *
     * @param customerCode 产品类型编码
     * @return Result
     */
    @Transactional
    public Result<String> openMaterialType(String customerCode) {
        ValidateUtil.hasLength(customerCode, "参数[customerCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        BaseMaterialType sysMaterialType = baseMaterialTypeService.queryMaterialTypeByMaterialTypeCode(customerCode, loginUser);
        new MaterialTypeCheck(sysMaterialType).checkNull("类型不存在");
        MaterialTypeBuilder customerBuilder = new MaterialTypeBuilder();
        customerBuilder.appendCode(customerCode).appendEdit(loginUser).open();
        //开启产品类型信息
        if (!baseMaterialTypeService.edit(customerBuilder.bulid(), loginUser)) {
            log.info("开启产品类型接口异常,参数:{}", JSON.toJSONString(customerBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, customerBuilder.getCode());
    }

    /**
     * 开启产品类型
     *
     * @param customerCode 产品类型编码
     * @return Result
     */
    @Transactional
    public Result<String> closeMaterialType(String customerCode) {
        ValidateUtil.hasLength(customerCode, "参数[customerCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        BaseMaterialType sysMaterialType = baseMaterialTypeService.queryMaterialTypeByMaterialTypeCode(customerCode, loginUser);
        new MaterialTypeCheck(sysMaterialType).checkNull("类型不存在");
        MaterialTypeBuilder customerBuilder = new MaterialTypeBuilder();
        customerBuilder.appendCode(customerCode).appendEdit(loginUser).close();
        //关闭产品类型信息
        if (!baseMaterialTypeService.edit(customerBuilder.bulid(), loginUser)) {
            log.info("关闭产品类型接口异常,参数:{}", JSON.toJSONString(customerBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, customerBuilder.getCode());
    }

    /**
     * 产品类型详情
     *
     * @param customerCode 产品类型编码
     * @return Result
     */
    public Result<MaterialTypeVO> viewMaterialType(String customerCode) {
        ValidateUtil.hasLength(customerCode, "参数[customerCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        BaseMaterialType sysMaterialType = baseMaterialTypeService.queryMaterialTypeByMaterialTypeCode(customerCode, loginUser);
        new MaterialTypeCheck(sysMaterialType).checkNull("类型不存在");
        MaterialTypeVO customerVO = new MaterialTypeVO();
        BeanUtil.copyProperties(sysMaterialType, customerVO);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, customerVO);
    }

}
