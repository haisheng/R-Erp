package com.ray.base.api;

import com.ray.base.builder.CustomerPriceBuilder;
import com.ray.base.builder.StepPriceBuilder;
import com.ray.base.check.CustomerCheck;
import com.ray.base.service.BaseCustomerPriceService;
import com.ray.base.service.BaseCustomerService;
import com.ray.base.table.entity.BaseCustomer;
import com.ray.base.table.entity.BaseCustomerPrice;
import com.ray.base.table.params.customer.StepPriceQueryParams;
import com.ray.base.table.params.customer.price.CustomerPriceParams;
import com.ray.base.table.vo.customer.StepPriceVO;
import com.ray.base.table.vo.customer.price.CustomerPriceVO;
import com.ray.business.service.ProdStepService;
import com.ray.business.table.dto.StepQueryDTO;
import com.ray.business.table.entity.ProdStep;
import com.ray.common.check.AbstractCheck;
import com.ray.validate.support.utils.ValidateUtil;
import com.ray.woodencreate.annotation.ServiceLog;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description:
 * @Class: CustomerPriceApi
 * @Package com.ray.base.api
 * @date 2020/7/11 15:38
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class CustomerPriceApi {

    @Autowired
    private BaseCustomerPriceService baseCustomerPriceService;
    @Autowired
    private BaseCustomerService baseCustomerService;
    @Autowired
    private ProdStepService prodStepService;


    /**
     * 编辑客户
     *
     * @param editParams 编辑对象
     * @return Result
     */
    @Transactional
    @ServiceLog(name = "保存客户加工单价")
    public Result<String> saveCustomerPrice(CustomerPriceParams editParams) {
        ValidateUtil.validate(editParams);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();

        //获取权限信息
        BaseCustomer sysCustomer = baseCustomerService.queryCustomerByCustomerCode(editParams.getCustomerCode(), loginUser);
        new CustomerCheck(sysCustomer).checkNull("客户不存在");
        //删除信息
        baseCustomerPriceService.deleteCustomerPrice(editParams.getCustomerCode(), loginUser);
        //保存扩展信息
        baseCustomerPriceService.saveBatch(editParams.getProps().stream().map(stepPriceParams -> {
            CustomerPriceBuilder customerPriceBuilder = new CustomerPriceBuilder();
            customerPriceBuilder.append(stepPriceParams).appendCustomerCode(editParams.getCustomerCode())
                    .appendCreate(loginUser);
            return customerPriceBuilder.bulid();
        }).collect(Collectors.toList()));
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, editParams.getCustomerCode());
    }


    /**
     * 客户加工单价详情
     *
     * @param customerCode 客户编码
     * @return Result
     */
    @ServiceLog(name = "查询客户加工单价")
    public Result<CustomerPriceVO> viewCustomerPrice(String customerCode) {
        ValidateUtil.hasLength(customerCode, "参数[customerCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        BaseCustomer sysCustomer = baseCustomerService.queryCustomerByCustomerCode(customerCode, loginUser);
        new CustomerCheck(sysCustomer).checkNull("客户不存在");
        CustomerPriceVO customerPriceVO = new CustomerPriceVO();
        customerPriceVO.setCustomerCode(customerCode);
        //获取扩展信息
        Map<String, BaseCustomerPrice> extendMap = baseCustomerPriceService.listByCustomerPrice(customerCode, loginUser).stream()
                .collect(Collectors.toMap(item -> item.getStepCode(), item -> item));
        //获取属性信息
        List<StepPriceVO> propVOS = prodStepService.list(new StepQueryDTO(), loginUser).stream().map(baseTemplate -> {
            return new StepPriceBuilder().append(baseTemplate).append(extendMap.get(baseTemplate.getStepCode())).bulid();
        }).collect(Collectors.toList());
        customerPriceVO.setProps(propVOS);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, customerPriceVO);
    }

    /**
     * 查询客户价格
     *
     * @param queryParams
     * @return
     */
    @ServiceLog(name = "查询客户单价-步骤")
    public Result<BigDecimal> price(@Valid StepPriceQueryParams queryParams) {
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        BaseCustomerPrice price = baseCustomerPriceService.queryCustomerPrice(queryParams.getCustomerCode(), queryParams.getStepCode(), loginUser);

        new AbstractCheck<>(price).checkNull("工序单价未配置");
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, price.getPrice());
    }
}
