package com.ray.base.api;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import com.ray.base.table.vo.FileVO;
import com.ray.woodencreate.config.SysFileConfig;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.IOUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author bo shen
 * @Description:
 * @Class: UploadApi
 * @Package com.ray.base.api
 * @date 2020/7/13 15:38
 * @company <p></p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Slf4j
@Service
public class UploadApi {

    @Autowired
    private SysFileConfig sysFileConfig;

    /**
     * 上传文件
     * @param file
     * @return
     */
    public Result<FileVO> uploadFile(MultipartFile file) {
        String fileUrl = String.format("%s/%s.%s",DateUtil.format(new Date(),"yyyy/MM/dd"), UUID.randomUUID().toString(),file.getOriginalFilename());
        FileOutputStream fileOutputStream = null;
        InputStream inputStream = null;
        log.info("上传文件地址:{}",fileUrl);
        log.info("上传文件本地地址:{}{}",sysFileConfig.getDir(),fileUrl);
        File newFile  = new File(sysFileConfig.getDir()+fileUrl);
        if(!newFile.getParentFile().exists()){
            newFile.getParentFile().mkdirs();
        }
        try {
            fileOutputStream = new FileOutputStream(newFile);
            inputStream = file.getInputStream();
            IOUtils.copy(inputStream, fileOutputStream);
        } catch(IOException e) {
            log.error(e.getMessage());
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            }
        }
        FileVO fileVO = new FileVO();
        fileVO.setFileName(file.getOriginalFilename());
        fileVO.setFileUrl(fileUrl);
        fileVO.setFullUrl(sysFileConfig.getPrefix()+fileUrl);
        return ResultFactory.createSuccessResult("上传成功",fileVO);
    }

    /**
     * 上传文件
     * @param file
     * @return
     */
    public Result<FileVO> uploadImageFile(MultipartFile file) {
        String fileUrl = String.format("%s/%s.%s",DateUtil.format(new Date(),"yyyy/MM/dd"), UUID.randomUUID().toString(),file.getOriginalFilename());
        FileOutputStream fileOutputStream = null;
        InputStream inputStream = null;
        log.info("上传文件地址:{}",fileUrl);
        log.info("上传文件本地地址:{}{}",sysFileConfig.getImageDir(),fileUrl);
        File newFile  = new File(sysFileConfig.getImageDir()+fileUrl);
        if(!newFile.getParentFile().exists()){
            newFile.getParentFile().mkdirs();
        }
        try {
            fileOutputStream = new FileOutputStream(newFile);
            inputStream = file.getInputStream();
            IOUtils.copy(inputStream, fileOutputStream);
        } catch(IOException e) {
            log.error(e.getMessage());
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            }
        }
        FileVO fileVO = new FileVO();
        fileVO.setFileName(file.getOriginalFilename());
        fileVO.setFileUrl(fileUrl);
        fileVO.setFullUrl(sysFileConfig.getPrefix()+fileUrl);
        return ResultFactory.createSuccessResult("上传成功",fileVO);
    }

    public Result<List<FileVO>> listImages() {
        List<FileVO> list = new ArrayList<>();
        File newFile  = new File(sysFileConfig.getImageDir());
        readFile(newFile,list);
        return ResultFactory.createSuccessResult("查询成功",list);
    }

    /**
     * 读取文件
     * @param rootFile
     * @param list
     */
    private void readFile(File rootFile, List<FileVO> list) {
        for(File file :rootFile.listFiles()){
            if(file.isDirectory()){
                readFile(file,list);
            }else if(file.isFile()) {
                FileVO fileVO = new FileVO();
                String fileUrl = file.getPath().substring(sysFileConfig.getDir().length());
                fileVO.setFileName(file.getName());
                fileVO.setFileUrl(fileUrl);
                fileVO.setFullUrl(sysFileConfig.getPrefix()+fileUrl);
                list.add(fileVO);
            }
        }
    }
}
