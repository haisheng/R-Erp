package com.ray.base.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.builder.MaterialTemplateBuilder;
import com.ray.base.check.MaterialTemplateCheck;
import com.ray.base.service.BaseMaterialTemplateService;
import com.ray.base.table.dto.MaterialTemplateQueryDTO;
import com.ray.base.table.entity.BaseMaterialTemplate;
import com.ray.base.table.params.material.template.MaterialTemplateCreateParams;
import com.ray.base.table.params.material.template.MaterialTemplateEditParams;
import com.ray.base.table.params.material.template.MaterialTemplateQueryParams;
import com.ray.base.table.vo.material.template.MaterialTemplateVO;
import com.ray.system.builder.CommonPageBuilder;
import com.ray.validate.support.utils.ValidateUtil;
import com.ray.woodencreate.beans.LoginUser;
import com.ray.woodencreate.enums.YesOrNoEnum;
import com.ray.woodencreate.exception.BusinessExceptionFactory;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.MsgCodeConstant;
import com.ray.woodencreate.result.Result;
import com.ray.woodencreate.result.ResultFactory;
import com.ray.woodencreate.util.Assert;
import com.ray.woodencreate.util.LogInUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author bo shen
 * @Description: 产品扩展属性相关服务
 * @Class: MaterialTemplateApi
 * @Package com.ray.base.api
 * @date 2020/5/27 11:15
 * @company <p>Ray快速开发平台</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Service
@Slf4j
public class MaterialTemplateApi {

    @Autowired
    private BaseMaterialTemplateService baseMaterialTemplateService;


    /**
     * 查询扩展属性列表信息 分页  非删除的
     *
     * @param queryParams
     * @return
     */
    public Result<IPage<MaterialTemplateVO>> pageMaterialTemplates(CommonPage<MaterialTemplateQueryParams, Page<MaterialTemplateVO>> queryParams) {
        Assert.notNull(queryParams, "参数[queryParams]不存在");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        CommonPageBuilder<MaterialTemplateQueryDTO, BaseMaterialTemplate> commonPageBuilder = new CommonPageBuilder<>(MaterialTemplateQueryDTO.class);
        commonPageBuilder.appendEntity(queryParams.getEntity()).appendQuery(queryParams.getQuery()).appendPage(queryParams.getPage());
        IPage<BaseMaterialTemplate> page = baseMaterialTemplateService.page(commonPageBuilder.bulid(), loginUser);
        List<BaseMaterialTemplate> customers = page.getRecords();
        //结果对象
        IPage<MaterialTemplateVO> pageList = new Page<>();
        pageList.setTotal(page.getTotal());
        pageList.setCurrent(page.getCurrent());
        pageList.setSize(page.getSize());
        //查询到结果 数据转换
        if (ObjectUtil.isNotNull(customers)) {
            pageList.setRecords(customers.stream().map(sysMaterialTemplate -> {
                MaterialTemplateVO customerVO = new MaterialTemplateVO();
                BeanUtil.copyProperties(sysMaterialTemplate, customerVO);
                return customerVO;
            }).collect(Collectors.toList()));
        } else {
            pageList.setRecords(new ArrayList<>());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, pageList);
    }

    /**
     * 查询扩展属性列表信息--启用的扩展属性
     *
     * @param queryParams
     * @return
     */
    public Result<List<MaterialTemplateVO>> queryMaterialTemplates(MaterialTemplateQueryParams queryParams) {
        Assert.notNull(queryParams, "参数[queryParams]不存在");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        MaterialTemplateQueryDTO queryDTO = new MaterialTemplateQueryDTO();
        BeanUtil.copyProperties(queryParams, queryDTO);
        queryDTO.setStatus(YesOrNoEnum.YES.getValue());
        List<BaseMaterialTemplate> customers = baseMaterialTemplateService.list(queryDTO, loginUser);
        //查询对象
        List<MaterialTemplateVO> list = new ArrayList<>();
        if (ObjectUtil.isNotNull(customers)) {
            list = customers.stream().map(sysMaterialTemplate -> {
                MaterialTemplateVO customerVO = new MaterialTemplateVO();
                BeanUtil.copyProperties(sysMaterialTemplate, customerVO);
                return customerVO;
            }).collect(Collectors.toList());
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, list);
    }


    /**
     * 创建扩展属性
     *
     * @param createParams 创建对象
     * @return Result
     */
    @Transactional
    public Result<String> createMaterialTemplate(MaterialTemplateCreateParams createParams) {
        ValidateUtil.validate(createParams);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        BaseMaterialTemplate materialTemplate = baseMaterialTemplateService.queryMaterialTemplateByProp(createParams.getProp(),loginUser);
        new MaterialTemplateCheck(materialTemplate).checkTemplateProp(null,"属性已经存在");
        MaterialTemplateBuilder customerBuilder = new MaterialTemplateBuilder();
        customerBuilder.append(createParams).appendComapnyCode(loginUser.getCompanyCode())
                .appendStatus(YesOrNoEnum.YES.getValue()).appendCreate(loginUser);
        //保存扩展属性信息
        if (!baseMaterialTemplateService.save(customerBuilder.bulid())) {
            log.info("保存扩展属性接口异常,参数:{}", JSON.toJSONString(customerBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, customerBuilder.getCode());
    }

    /**
     * 编辑扩展属性
     *
     * @param editParams 编辑对象
     * @return Result
     */
    @Transactional
    public Result<String> editMaterialTemplate(MaterialTemplateEditParams editParams) {
        ValidateUtil.validate(editParams);
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        BaseMaterialTemplate materialTemplate = baseMaterialTemplateService.queryMaterialTemplateByProp(editParams.getProp(),loginUser);
        new MaterialTemplateCheck(materialTemplate).checkTemplateProp(editParams.getTemplateCode(),"属性已经存在");
        MaterialTemplateBuilder customerBuilder = new MaterialTemplateBuilder();
        customerBuilder.append(editParams).appendEdit(loginUser);
        //编辑扩展属性信息
        if (!baseMaterialTemplateService.edit(customerBuilder.bulid(), loginUser)) {
            log.info("编辑扩展属性接口异常,参数:{}", JSON.toJSONString(customerBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, customerBuilder.getCode());
    }


    /**
     * 删除扩展属性
     *
     * @param templateCode 扩展属性编码
     * @return Result
     */
    @Transactional
    public Result<String> deleteMaterialTemplate(String templateCode) {
        ValidateUtil.hasLength(templateCode, "参数[templateCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        BaseMaterialTemplate sysMaterialTemplate = baseMaterialTemplateService.queryMaterialTemplateByMaterialTemplateCode(templateCode, loginUser);
        new MaterialTemplateCheck(sysMaterialTemplate).checkNull("属性不存在");
        MaterialTemplateBuilder customerBuilder = new MaterialTemplateBuilder();
        customerBuilder.appendCode(templateCode).appendEdit(loginUser).delete();
        //删除扩展属性信息
        if (!baseMaterialTemplateService.edit(customerBuilder.bulid(), loginUser)) {
            log.info("删除扩展属性接口异常,参数:{}", JSON.toJSONString(customerBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, customerBuilder.getCode());
    }

    /**
     * 开启扩展属性
     *
     * @param templateCode 扩展属性编码
     * @return Result
     */
    @Transactional
    public Result<String> openMaterialTemplate(String templateCode) {
        ValidateUtil.hasLength(templateCode, "参数[templateCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        BaseMaterialTemplate sysMaterialTemplate = baseMaterialTemplateService.queryMaterialTemplateByMaterialTemplateCode(templateCode, loginUser);
        new MaterialTemplateCheck(sysMaterialTemplate).checkNull("属性不存在");
        MaterialTemplateBuilder customerBuilder = new MaterialTemplateBuilder();
        customerBuilder.appendCode(templateCode).appendEdit(loginUser).open();
        //开启扩展属性信息
        if (!baseMaterialTemplateService.edit(customerBuilder.bulid(), loginUser)) {
            log.info("开启扩展属性接口异常,参数:{}", JSON.toJSONString(customerBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, customerBuilder.getCode());
    }

    /**
     * 开启扩展属性
     *
     * @param templateCode 扩展属性编码
     * @return Result
     */
    @Transactional
    public Result<String> closeMaterialTemplate(String templateCode) {
        ValidateUtil.hasLength(templateCode, "参数[templateCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        BaseMaterialTemplate sysMaterialTemplate = baseMaterialTemplateService.queryMaterialTemplateByMaterialTemplateCode(templateCode, loginUser);
        new MaterialTemplateCheck(sysMaterialTemplate).checkNull("属性不存在");
        MaterialTemplateBuilder customerBuilder = new MaterialTemplateBuilder();
        customerBuilder.appendCode(templateCode).appendEdit(loginUser).close();
        //关闭扩展属性信息
        if (!baseMaterialTemplateService.edit(customerBuilder.bulid(), loginUser)) {
            log.info("关闭扩展属性接口异常,参数:{}", JSON.toJSONString(customerBuilder.bulid()));
            throw BusinessExceptionFactory.newException(MsgCodeConstant.Error.ERR00000001);
        }
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000001, customerBuilder.getCode());
    }

    /**
     * 扩展属性详情
     *
     * @param templateCode 扩展属性编码
     * @return Result
     */
    public Result<MaterialTemplateVO> viewMaterialTemplate(String templateCode) {
        ValidateUtil.hasLength(templateCode, "参数[templateCode]不能为空");
        //当前登录对象
        LoginUser loginUser = LogInUserUtil.get();
        //获取权限信息
        BaseMaterialTemplate sysMaterialTemplate = baseMaterialTemplateService.queryMaterialTemplateByMaterialTemplateCode(templateCode, loginUser);
        new MaterialTemplateCheck(sysMaterialTemplate).checkNull("属性不存在");
        MaterialTemplateVO customerVO = new MaterialTemplateVO();
        BeanUtil.copyProperties(sysMaterialTemplate, customerVO);
        return ResultFactory.createSuccessResult(MsgCodeConstant.Success.SUC00000002, customerVO);
    }

}
