package com.ray.base.controller;


import com.ray.base.api.TechnologyApi;
import com.ray.base.table.params.technology.TechnologyCreateParams;
import com.ray.base.table.vo.technology.TechnologyVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 系统基础服务-技术工艺管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/base/technology")
@Api("基础服务-技术工艺")
@Validated
public class TechnologyController {

    @Autowired
    private TechnologyApi technologyApi;


    @ApiOperation("技术工艺新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid TechnologyCreateParams createParams) {
        return technologyApi.saveTechnology(createParams);
    }


    @ApiOperation("技术工艺详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<TechnologyVO> view(@ApiParam(value = "规格编码", required = true) @RequestParam(required = true) String modelCode) {
        return technologyApi.viewTechnology(modelCode);
    }


}
