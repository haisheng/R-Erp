package com.ray.base.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.api.MaterialTemplateApi;
import com.ray.base.table.params.material.template.MaterialTemplateCreateParams;
import com.ray.base.table.params.material.template.MaterialTemplateEditParams;
import com.ray.base.table.params.material.template.MaterialTemplateQueryParams;
import com.ray.base.table.vo.material.template.MaterialTemplateVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-产品物料属性模板管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/base/material/template")
@Api("基础服务-产品物料属性模板")
@Validated
public class MaterialTemplateController {

    @Autowired
    private MaterialTemplateApi materialTemplateApi;

    @ApiOperation("查询产品物料属性模板列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<MaterialTemplateVO>> page(@RequestBody @Valid CommonPage<MaterialTemplateQueryParams, Page<MaterialTemplateVO>> queryParams) {
        return materialTemplateApi.pageMaterialTemplates(queryParams);
    }

    @ApiOperation("查询产品物料属性模板列表")
    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<MaterialTemplateVO>> list(@RequestBody @Valid MaterialTemplateQueryParams queryParams) {
        return materialTemplateApi.queryMaterialTemplates(queryParams);
    }


    @ApiOperation("产品物料属性模板新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid MaterialTemplateCreateParams createParams) {
        return materialTemplateApi.createMaterialTemplate(createParams);
    }

    @ApiOperation("产品物料属性模板编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid MaterialTemplateEditParams editParams) {
        return materialTemplateApi.editMaterialTemplate(editParams);
    }

    @ApiOperation("产品物料属性模板删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "产品物料属性模板编码", required = true) @RequestParam(required = true) String templateCode) {
        return materialTemplateApi.deleteMaterialTemplate(templateCode);
    }

    @ApiOperation("产品物料属性模板详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<MaterialTemplateVO> view(@ApiParam(value = "产品物料属性模板编码", required = true) @RequestParam(required = true) String templateCode) {
        return materialTemplateApi.viewMaterialTemplate(templateCode);
    }

    @ApiOperation("产品物料属性模板开启")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "产品物料属性模板编码", required = true) @RequestParam(required = true) String templateCode) {
        return materialTemplateApi.openMaterialTemplate(templateCode);
    }


    @ApiOperation("产品物料属性模板关闭")
    @PostMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "产品物料属性模板编码", required = true) @RequestParam(required = true) String templateCode) {
        return materialTemplateApi.closeMaterialTemplate(templateCode);
    }


}
