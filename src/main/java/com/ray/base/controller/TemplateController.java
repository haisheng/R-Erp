package com.ray.base.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.api.TemplateApi;
import com.ray.base.table.params.template.TemplateCreateParams;
import com.ray.base.table.params.template.TemplateEditParams;
import com.ray.base.table.params.template.TemplateQueryParams;
import com.ray.base.table.vo.template.TemplateVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-属性模板管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/base/template")
@Api("基础服务-属性模板")
@Validated
public class TemplateController {

    @Autowired
    private TemplateApi materialTemplateApi;

    @ApiOperation("查询属性模板列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<TemplateVO>> page(@RequestBody @Valid CommonPage<TemplateQueryParams, Page<TemplateVO>> queryParams) {
        return materialTemplateApi.pageTemplates(queryParams);
    }

    @ApiOperation("查询属性模板列表")
    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<TemplateVO>> list(@RequestBody @Valid TemplateQueryParams queryParams) {
        return materialTemplateApi.queryTemplates(queryParams);
    }


    @ApiOperation("属性模板新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid TemplateCreateParams createParams) {
        return materialTemplateApi.createTemplate(createParams);
    }

    @ApiOperation("属性模板编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid TemplateEditParams editParams) {
        return materialTemplateApi.editTemplate(editParams);
    }

    @ApiOperation("属性模板删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "属性模板编码", required = true) @RequestParam(required = true) String templateCode) {
        return materialTemplateApi.deleteTemplate(templateCode);
    }

    @ApiOperation("属性模板详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<TemplateVO> view(@ApiParam(value = "属性模板编码", required = true) @RequestParam(required = true) String templateCode) {
        return materialTemplateApi.viewTemplate(templateCode);
    }

    @ApiOperation("属性模板开启")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "属性模板编码", required = true) @RequestParam(required = true) String templateCode) {
        return materialTemplateApi.openTemplate(templateCode);
    }


    @ApiOperation("属性模板关闭")
    @PostMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "属性模板编码", required = true) @RequestParam(required = true) String templateCode) {
        return materialTemplateApi.closeTemplate(templateCode);
    }


}
