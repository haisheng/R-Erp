package com.ray.base.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.api.CustomerApi;
import com.ray.base.table.params.customer.CustomerCreateParams;
import com.ray.base.table.params.customer.CustomerEditParams;
import com.ray.base.table.params.customer.CustomerPrintSaveParams;
import com.ray.base.table.params.customer.CustomerQueryParams;
import com.ray.base.table.vo.customer.CustomerPrintVO;
import com.ray.base.table.vo.customer.CustomerVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-客户管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/base/customer")
@Api("基础服务-客户")
@Validated
public class CustomerController {

    @Autowired
    private CustomerApi customerApi;

    @ApiOperation("查询客户列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<CustomerVO>> page(@RequestBody @Valid CommonPage<CustomerQueryParams, Page<CustomerVO>> queryParams) {
        return customerApi.pageCustomers(queryParams);
    }

    @ApiOperation("查询客户列表")
    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<CustomerVO>> list(@RequestBody @Valid CustomerQueryParams queryParams) {
        return customerApi.queryCustomers(queryParams);
    }


    @ApiOperation("客户新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid CustomerCreateParams createParams) {
        return customerApi.createCustomer(createParams);
    }

    @ApiOperation("客户编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid CustomerEditParams editParams) {
        return customerApi.editCustomer(editParams);
    }

    @ApiOperation("客户删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "客户编码", required = true) @RequestParam(required = true) String customerCode) {
        return customerApi.deleteCustomer(customerCode);
    }

    @ApiOperation("客户详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<CustomerVO> view(@ApiParam(value = "客户编码", required = true) @RequestParam(required = true) String customerCode) {
        return customerApi.viewCustomer(customerCode);
    }

    @ApiOperation("客户开启")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "客户编码", required = true) @RequestParam(required = true) String customerCode) {
        return customerApi.openCustomer(customerCode);
    }


    @ApiOperation("客户关闭")
    @PostMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "客户编码", required = true) @RequestParam(required = true) String customerCode) {
        return customerApi.closeCustomer(customerCode);
    }

    @ApiOperation("客户模板新增")
    @PostMapping(value = "/template/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> templateAdd(@RequestBody @Valid CustomerPrintSaveParams createParams) {
        return customerApi.saveCustomerPrint(createParams);
    }

    @ApiOperation("客户模板新增")
    @PostMapping(value = "/template/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<CustomerPrintVO> templateView(@ApiParam(value = "客户编码", required = true) @RequestParam(required = true) String customerCode) {
        return customerApi.viewCustomerPrint(customerCode);
    }

}
