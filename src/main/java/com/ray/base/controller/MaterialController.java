package com.ray.base.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.api.MaterialApi;
import com.ray.base.table.params.material.MaterialCreateParams;
import com.ray.base.table.params.material.MaterialEditParams;
import com.ray.base.table.params.material.MaterialQueryParams;
import com.ray.base.table.vo.material.MaterialVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-产品/物料管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/base/material")
@Api("基础服务-产品/物料")
@Validated
public class MaterialController {

    @Autowired
    private MaterialApi materialApi;

    @ApiOperation("查询产品/物料列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<MaterialVO>> page(@RequestBody @Valid CommonPage<MaterialQueryParams, Page<MaterialVO>> queryParams) {
        return materialApi.pageMaterials(queryParams);
    }

    @ApiOperation("查询产品/物料列表")
    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<MaterialVO>> list(@RequestBody @Valid MaterialQueryParams queryParams) {
        return materialApi.queryMaterials(queryParams);
    }


    @ApiOperation("产品/物料新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid MaterialCreateParams createParams) {
        return materialApi.createMaterial(createParams);
    }

    @ApiOperation("产品/物料编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid MaterialEditParams editParams) {
        return materialApi.editMaterial(editParams);
    }

    @ApiOperation("产品/物料删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "产品/物料编码", required = true) @RequestParam(required = true) String materialCode) {
        return materialApi.deleteMaterial(materialCode);
    }

    @ApiOperation("产品/物料详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<MaterialVO> view(@ApiParam(value = "产品/物料编码", required = true) @RequestParam(required = true) String materialCode) {
        return materialApi.viewMaterial(materialCode);
    }

    @ApiOperation("产品/物料开启")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "产品/物料编码", required = true) @RequestParam(required = true) String materialCode) {
        return materialApi.openMaterial(materialCode);
    }


    @ApiOperation("产品/物料关闭")
    @PostMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "产品/物料编码", required = true) @RequestParam(required = true) String materialCode) {
        return materialApi.closeMaterial(materialCode);
    }


}
