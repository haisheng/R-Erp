package com.ray.base.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.api.MaterialTypeApi;
import com.ray.base.table.params.material.type.MaterialTypeCreateParams;
import com.ray.base.table.params.material.type.MaterialTypeEditParams;
import com.ray.base.table.params.material.type.MaterialTypeQueryParams;
import com.ray.base.table.vo.material.type.MaterialTypeVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-产品物料类型管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/base/material/type")
@Api("基础服务-产品物料类型")
@Validated
public class MaterialTypeController {

    @Autowired
    private MaterialTypeApi materialTypeApi;

    @ApiOperation("查询产品物料类型列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<MaterialTypeVO>> page(@RequestBody @Valid CommonPage<MaterialTypeQueryParams, Page<MaterialTypeVO>> queryParams) {
        return materialTypeApi.pageMaterialTypes(queryParams);
    }

    @ApiOperation("查询产品物料类型列表")
    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<MaterialTypeVO>> list(@RequestBody @Valid MaterialTypeQueryParams queryParams) {
        return materialTypeApi.queryMaterialTypes(queryParams);
    }


    @ApiOperation("产品物料类型新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid MaterialTypeCreateParams createParams) {
        return materialTypeApi.createMaterialType(createParams);
    }

    @ApiOperation("产品物料类型编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid MaterialTypeEditParams editParams) {
        return materialTypeApi.editMaterialType(editParams);
    }

    @ApiOperation("产品物料类型删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "产品物料类型编码", required = true) @RequestParam(required = true) String typeCode) {
        return materialTypeApi.deleteMaterialType(typeCode);
    }

    @ApiOperation("产品物料类型详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<MaterialTypeVO> view(@ApiParam(value = "产品物料类型编码", required = true) @RequestParam(required = true) String typeCode) {
        return materialTypeApi.viewMaterialType(typeCode);
    }

    @ApiOperation("产品物料类型开启")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "产品物料类型编码", required = true) @RequestParam(required = true) String typeCode) {
        return materialTypeApi.openMaterialType(typeCode);
    }


    @ApiOperation("产品物料类型关闭")
    @PostMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "产品物料类型编码", required = true) @RequestParam(required = true) String typeCode) {
        return materialTypeApi.closeMaterialType(typeCode);
    }


}
