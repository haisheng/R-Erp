package com.ray.base.controller;


import com.ray.base.api.CustomerPriceApi;
import com.ray.base.table.params.customer.StepPriceQueryParams;
import com.ray.base.table.params.customer.price.CustomerPriceParams;
import com.ray.base.table.vo.customer.price.CustomerPriceVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;

/**
 * <p>
 * 系统基础服务-客户管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/base/customer/price")
@Api("基础服务-客户加工价格")
@Validated
public class CustomerPriceController {

    @Autowired
    private CustomerPriceApi customerPriceApi;




    @ApiOperation("客户价格编辑")
    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> save(@RequestBody @Valid CustomerPriceParams editParams) {
        return customerPriceApi.saveCustomerPrice(editParams);
    }


    @ApiOperation("客户价格详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<CustomerPriceVO> view(@ApiParam(value = "客户编码", required = true) @RequestParam(required = true) String customerCode) {
        return customerPriceApi.viewCustomerPrice(customerCode);
    }


    @ApiOperation("客户工序单价")
    @PostMapping(value = "/price", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<BigDecimal> price(@RequestBody @Valid  StepPriceQueryParams queryParams) {
        return customerPriceApi.price(queryParams);
    }


}
