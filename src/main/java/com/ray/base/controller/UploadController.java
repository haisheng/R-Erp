package com.ray.base.controller;

import com.ray.base.api.UploadApi;
import com.ray.base.table.vo.FileVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.annotation.NoLogin;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author bo shen
 * @Description: 文件上传
 * @Class: UploadController
 * @Package com.ray.base.controller
 * @date 2020/7/13 15:25
 * @company <p>浙江数链科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@RestController
@RequestMapping("/upload")
@Api("基础服务-文件上传")
@Validated
public class UploadController {

    @Autowired
    private UploadApi uploadApi;

    @PostMapping("/file")
    @NoAuth
    @NoLogin
    public Result<FileVO> file(@RequestParam(value = "file", required = true) MultipartFile file) {
        return uploadApi.uploadFile(file);
    }
}
