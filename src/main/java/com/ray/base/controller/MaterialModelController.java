package com.ray.base.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ray.base.api.MaterialModelApi;
import com.ray.base.table.params.material.model.MaterialModelCreateParams;
import com.ray.base.table.params.material.model.MaterialModelEditParams;
import com.ray.base.table.params.material.model.MaterialModelQueryParams;
import com.ray.base.table.vo.material.model.MaterialModelVO;
import com.ray.woodencreate.annotation.NoAuth;
import com.ray.woodencreate.page.CommonPage;
import com.ray.woodencreate.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统基础服务-规格管理
 * </p>
 *
 * @author shenbo
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/base/material/model")
@Api("基础服务-规格")
@Validated
public class MaterialModelController {

    @Autowired
    private MaterialModelApi materialModelApi;

    @ApiOperation("查询规格列表查询")
    @PostMapping(value = "/page", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<IPage<MaterialModelVO>> page(@RequestBody @Valid CommonPage<MaterialModelQueryParams, Page<MaterialModelVO>> queryParams) {
        return materialModelApi.pageMaterialModels(queryParams);
    }

    @ApiOperation("查询规格列表")
    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<List<MaterialModelVO>> list(@RequestBody @Valid MaterialModelQueryParams queryParams) {
        return materialModelApi.queryMaterialModels(queryParams);
    }


    @ApiOperation("规格新增")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> add(@RequestBody @Valid MaterialModelCreateParams createParams) {
        return materialModelApi.createMaterialModel(createParams);
    }

    @ApiOperation("规格编辑")
    @PostMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> edit(@RequestBody @Valid MaterialModelEditParams editParams) {
        return materialModelApi.editMaterialModel(editParams);
    }

    @ApiOperation("规格删除")
    @PostMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> delete(@ApiParam(value = "规格编码", required = true) @RequestParam(required = true) String modelCode) {
        return materialModelApi.deleteMaterialModel(modelCode);
    }

    @ApiOperation("规格详情")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<MaterialModelVO> view(@ApiParam(value = "规格编码", required = true) @RequestParam(required = true) String modelCode) {
        return materialModelApi.viewMaterialModel(modelCode);
    }

    @ApiOperation("规格开启")
    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> open(@ApiParam(value = "规格编码", required = true) @RequestParam(required = true) String modelCode) {
        return materialModelApi.openMaterialModel(modelCode);
    }


    @ApiOperation("规格关闭")
    @PostMapping(value = "/close", consumes = MediaType.APPLICATION_JSON_VALUE)
    @NoAuth
    public Result<String> close(@ApiParam(value = "规格编码", required = true) @RequestParam(required = true) String modelCode) {
        return materialModelApi.closeMaterialModel(modelCode);
    }


}
