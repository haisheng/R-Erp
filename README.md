# 项目总述
项目 是一个Saas平台 可支持本地化部署
# 开发初衷
* 小皮R-ERP立志为中小企业提供开源好用的ERP软件，降低企业的信息化成本
* 个人开发者也可以使用小皮ERP进行二次开发，加快完成开发任务
* 初学JAVA的小伙伴可以下载源代码来进行学习交流
* 联系方式: qray686898@163.com QQ:236833129
* 对elementUI进行2次封装，简便前端开发


备注：com.ray下的依赖都都在我的开源代码中
https://gitee.com/rayDreams/magicTest
https://gitee.com/rayDreams/magicValidate
https://gitee.com/rayDreams/magicBlock

# 技术框架
* 核心框架：SpringBoot 2.0.5.RELEASE
* 持久层框架：Mybatis 1.3.2
* 持久层框架 mybatisPlus 3.1.0
* 日志管理：Log4j 2.10.0
* JS框架：Vue 2.0
* UI框架: ElementUi 1.13.1
* 项目管理框架: Maven 3.2.3

# 开发环境
建议开发者使用以下环境，可以避免版本带来的问题
* IDE: IntelliJ IDEA 2018+
* DB: Mysql5.7+
* JDK: JDK1.8
* Maven: Maven3.2.3+

# 服务器环境
* 数据库服务器：Mysql5.7+
* JAVA平台: JRE1.8
* 操作系统：Windows、Linux等

# 开源说明
* 本系统100%开源，遵守AP-2.0协议
# 演示环境 
http://erp.todaytop.com.cn   账号:17700000000 密码:000000

# 功能列表
![输入图片说明](https://images.gitee.com/uploads/images/2020/0621/195015_d82ba3ba_382827.png "屏幕截图.png")

# 系统美图
* 登陆
![输入图片说明](https://images.gitee.com/uploads/images/2020/0621/194410_5f61af6f_382827.png "屏幕截图.png")
*首页
![输入图片说明](https://images.gitee.com/uploads/images/2020/0621/194504_0900b915_382827.png "屏幕截图.png")
*仓库管理
![输入图片说明](https://images.gitee.com/uploads/images/2020/0621/194544_1562e388_382827.png "屏幕截图.png")
*基础管理
![输入图片说明](https://images.gitee.com/uploads/images/2020/0621/194614_7994a4d9_382827.png "屏幕截图.png")
*财务管理
![输入图片说明](https://images.gitee.com/uploads/images/2020/0621/194647_d64a28a0_382827.png "屏幕截图.png")
*系统管理
![输入图片说明](https://images.gitee.com/uploads/images/2020/0621/194836_aec57351_382827.png "屏幕截图.png")




